package ui.customWidgets;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Point;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import utils.global.AppUtils;

public class CustomScrollView extends NestedScrollView
{
	private boolean mIsScrolling;
	private boolean mIsTouching;
	private OnScrollListener mOnScrollListener;
	private Runnable mScrollingRunnable;
	
	public abstract static class OnScrollListener
	{
		void onScrollChanged(CustomScrollView scrollView, int x, int y, int oldX, int oldY)
		{
			//if you need just override this method
		}
		
		void onEndScroll(CustomScrollView scrollView)
		{
			//if you need just override this method
		}
		
		protected abstract void onGoUp();
		
		protected abstract void onGoDown();
	}
	
	public CustomScrollView(final Context context)
	{
		super(context);
	}
	
	public CustomScrollView(final Context context, final AttributeSet attrs)
	{
		super(context, attrs);
	}
	
	public CustomScrollView(final Context context, final AttributeSet attrs, final int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
	}
	
	private int y = 0;
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent iEv)
	{
		if (isEnabled())
		{
			processEvent(iEv);
			super.dispatchTouchEvent(iEv);
			return true; //to keep receive event that follow down event
		}
		return super.dispatchTouchEvent(iEv);
	}
	
	private void processEvent(final MotionEvent iEv)
	{
		switch (iEv.getAction())
		{
			case MotionEvent.ACTION_DOWN:
				y = (int) iEv.getY();
				break;
			
			case MotionEvent.ACTION_UP:
				y = (int) iEv.getY();
				
				if (mIsTouching && !mIsScrolling && mOnScrollListener != null)
				{
					mOnScrollListener.onEndScroll(this);
				}
				
				mIsTouching = false;
				break;
			case MotionEvent.ACTION_MOVE:
				mIsTouching = true;
				mIsScrolling = true;
				
				int newY = (int) iEv.getY();
				int difY = y - newY;
				
				int MAX_VALUE = 200;
				int MIN_VALUE = -200;
				if (difY > MAX_VALUE)
				{
					if (mOnScrollListener != null)
					{
						mOnScrollListener.onGoDown();
					}
					y = newY;
				}
				else if (difY < MIN_VALUE)
				{
					if (mOnScrollListener != null)
					{
						mOnScrollListener.onGoUp();
					}
					y = newY;
				}
				
				break;
		}
	}
	
	@Override
	protected void onScrollChanged(int iX, int iY, int iOldX, int iOldY)
	{
		super.onScrollChanged(iX, iY, iOldX, iOldY);
		
		if (Math.abs(iOldX - iX) > 0)
		{
			if (mScrollingRunnable != null)
			{
				removeCallbacks(mScrollingRunnable);
			}
			
			mScrollingRunnable = new Runnable()
			{
				public void run()
				{
					if (mIsScrolling && !mIsTouching && mOnScrollListener != null)
					{
						mOnScrollListener.onEndScroll(CustomScrollView.this);
					}
					
					mIsScrolling = false;
					mScrollingRunnable = null;
				}
			};
			
			postDelayed(mScrollingRunnable, 200);
		}
		
		if (mOnScrollListener != null)
		{
			mOnScrollListener.onScrollChanged(this, iX, iY, iOldX, iOldY);
		}
	}
	
	public void scrollToView(final View iV)
	{
		// Get deepChild Offset
		Point childOffset = new Point();
		getDeepChildOffset(CustomScrollView.this, iV.getParent(), iV, childOffset);
		// Scroll to child.
		
		CustomScrollView.this.scrollToY(childOffset.y);
	}
	
	/**
	 * Used to get deep child offset.
	 * <p/>
	 * 1. We need to scroll to child in scrollview, but the child may not the direct child to scrollview.
	 * 2. So to get correct child position to scroll, we need to iterate through all of its parent views till the main parent.
	 *
	 * @param mainParent        Main Top parent.
	 * @param parent            Parent.
	 * @param child             Child.
	 * @param accumulatedOffset Accumalated Offset.
	 */
	private void getDeepChildOffset(final ViewGroup mainParent, final ViewParent parent, final View child, final Point accumulatedOffset)
	{
		ViewGroup parentGroup = (ViewGroup) parent;
		accumulatedOffset.x += child.getLeft();
		accumulatedOffset.y += child.getTop();
		if (parentGroup.equals(mainParent))
		{
			return;
		}
		getDeepChildOffset(mainParent, parentGroup.getParent(), parentGroup, accumulatedOffset);
	}
	
	public void scrollToY(final int iY)
	{
		CustomScrollView.this.postDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				int x = 0;
				int y = iY;
				ObjectAnimator xTranslate = ObjectAnimator.ofInt(CustomScrollView.this, "scrollX", x);
				ObjectAnimator yTranslate = ObjectAnimator.ofInt(CustomScrollView.this, "scrollY", y);
				
				AnimatorSet animators = new AnimatorSet();
				animators.setDuration(500L);
				animators.playTogether(xTranslate, yTranslate);
				animators.addListener(new Animator.AnimatorListener()
				{
					
					@Override
					public void onAnimationStart(Animator arg0)
					{
						// noting
					}
					
					@Override
					public void onAnimationRepeat(Animator arg0)
					{
						// noting
						
					}
					
					@Override
					public void onAnimationEnd(Animator arg0)
					{
						// noting
						
					}
					
					@Override
					public void onAnimationCancel(Animator arg0)
					{
						// noting
						
					}
				});
				animators.start();
			}
		}, 300);
	}
	
	public void scrollToTop()
	{
		scrollToY(0);
	}
	
	
	public OnScrollListener getOnScrollListener()
	{
		return mOnScrollListener;
	}
	
	public void setOnScrollListener(OnScrollListener mOnEndScrollListener)
	{
		this.mOnScrollListener = mOnEndScrollListener;
	}
}
