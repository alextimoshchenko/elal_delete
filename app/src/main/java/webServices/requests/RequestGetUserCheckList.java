package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.UserData;

/**
 * Created with care by Shahar Ben-Moshe on 21/02/17.
 */

public class RequestGetUserCheckList
{
	@JsonProperty("UserId")
	private int mUserID;
	@JsonProperty("PNRNum")
	private String mPnrNum;
	@JsonProperty("FlightNumber")
	private String mFlightNumber;
	
	@JsonProperty("LanguageCode")
	private String mLanguageCode = UserData.getInstance().getLanguage().getLanguageCode();
	
	public RequestGetUserCheckList(final int iUserID, final String iPnrNum, final String iFlightNumber)
	{
		mUserID = iUserID;
		mPnrNum = iPnrNum;
		mFlightNumber = iFlightNumber;
	}
}
