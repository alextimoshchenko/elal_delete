package ui.activities;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import global.ElAlApplication;
import global.UserData;
import utils.errors.LocalError;
import utils.errors.ServerError;
import utils.global.AppUtils;
import utils.global.ElalPreferenceUtils;
import webServices.controllers.LoginController;
import webServices.controllers.LoginControllerMatmid;
import webServices.controllers.MyFlightsController;
import webServices.requests.RequestGetGroupPNRsData;
import webServices.requests.RequestGetUserActivePNRs;
import webServices.responses.ResponseGetGroupPNRs;
import webServices.responses.ResponseGetUserActivePNRs;
import webServices.responses.ResponseMatmidLogin;
import webServices.responses.ResponseUserLogin;

/**
 * Created by Avishay.Peretz on 11/07/2017.
 */

public abstract class EWBaseUserActivity extends EWBaseDrawerActivity
{
	private static final String TAG = EWBaseUserActivity.class.getSimpleName();
	
	public void performRegisteredUserLogin(final String iEmail, final String iPassword, final boolean iIsChecked, final Response.Listener<ResponseUserLogin> iListener,
	                                       final Response.ErrorListener iErrorListener)
	{
		if (isServiceConnected())
		{
			setProgressDialog(true);
			getService().getController(LoginController.class).LoginAsUser(iEmail, iPassword, iIsChecked, new Response.Listener<ResponseUserLogin>()
			{
				@Override
				public void onResponse(final ResponseUserLogin response)
				{
					if (iListener != null)
					{
						iListener.onResponse(response);
					}
					
					setDrawerAdapter();
				}
			}, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError error)
				{
					if (iErrorListener != null)
					{
						iErrorListener.onErrorResponse(error);
					}
					
					setProgressDialog(false);
				}
			});
		}
	}
	
	public void performMatmidUserLogin(final String iMemberNumber, final String iPassword, final Response.Listener<ResponseMatmidLogin> iListener, final Response.ErrorListener iErrorListener)
	{
		if (isServiceConnected())
		{
			setProgressDialog(true);
			
			try
			{
				getService().initQueues();
			}
			catch (Throwable iThrowable)
			{
				AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.GeneralConnectionError.getErrorMessage() + iThrowable);
			}
			
			getService().getController(LoginControllerMatmid.class).MatmidCAToken(new Response.Listener<String>()
			{
				@Override
				public void onResponse(final String token)
				{
					ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.CA_TOKEN, token);
					
					if (token != null && !TextUtils.isEmpty(token))
					{
						getService().getController(LoginControllerMatmid.class).MatmidLogin(iMemberNumber, iPassword, new Response.Listener<ResponseMatmidLogin>()
						{
							@Override
							public void onResponse(final ResponseMatmidLogin response)
							{
								if (response.getContent().getMatmidErrorCode() == ServerError.eServerError.Ok.getErrorCode())
								{
									registerMatmidKeepAlive();
								}
								
								if (iListener != null)
								{
									iListener.onResponse(response);
								}
								
								setProgressDialog(false);
							}
						}, new Response.ErrorListener()
						{
							@Override
							public void onErrorResponse(final VolleyError error)
							{
								if (iErrorListener != null)
								{
									iErrorListener.onErrorResponse(error);
								}
								
								setProgressDialog(false);
							}
						});
					}
					else
					{
						iErrorListener.onErrorResponse(new VolleyError(token));
						setProgressDialog(false);
					}
				}
			}, iErrorListener);
		}
	}
	
	public void performMatmidUserLoginWithExistingToken(final String iMemberNumber, final String iPassword, final Response.Listener<ResponseMatmidLogin> iListener,
	                                                    final Response.ErrorListener iErrorListener)
	{
		if (isServiceConnected())
		{
			setProgressDialog(true);
			
			getService().getController(LoginControllerMatmid.class).MatmidLogin(iMemberNumber, iPassword, new Response.Listener<ResponseMatmidLogin>()
			{
				@Override
				public void onResponse(final ResponseMatmidLogin response)
				{
					
					if (response.getContent().getMatmidErrorCode() == ServerError.eServerError.Ok.getErrorCode())
					{
						registerMatmidKeepAlive();
					}
					
					if (iListener != null)
					{
						iListener.onResponse(response);
					}
					
					setProgressDialog(false);
				}
			}, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError error)
				{
					if (iErrorListener != null)
					{
						iErrorListener.onErrorResponse(error);
					}
					
					setProgressDialog(false);
				}
			});
		}
	}
	
	public void performGetGroupPnrsData(final RequestGetGroupPNRsData iRequest, final Response.Listener<ResponseGetGroupPNRs> iListener, final Response.ErrorListener iErrorListener)
	{
		if (isServiceConnected())
		{
			setProgressDialog(true);
			
			getService().getController(LoginController.class).GetGroupPnrsData(iRequest, new Response.Listener<ResponseGetGroupPNRs>()
			{
				@Override
				public void onResponse(final ResponseGetGroupPNRs response)
				{
					if (iListener != null)
					{
						iListener.onResponse(response);
					}
				}
			}, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError error)
				{
					if (iErrorListener != null)
					{
						iErrorListener.onErrorResponse(error);
					}
					
				}
			});
		}
	}
	
	
	public void GetUserActivePNRs(final Response.Listener<ResponseGetUserActivePNRs> iResponseListener, final Response.ErrorListener iErrorListener)
	{
		if (isServiceConnected())
		{
			int userId = UserData.getInstance().getUserID();
			
			getService().getController(MyFlightsController.class).GetUserActivePNRs(new RequestGetUserActivePNRs(userId), new Response.Listener<ResponseGetUserActivePNRs>()
			{
				@Override
				public void onResponse(final ResponseGetUserActivePNRs iResponse)
				{
					iResponseListener.onResponse(iResponse);
				}
			}, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError iError)
				{
					iErrorListener.onErrorResponse(iError);
				}
			});
		}
	}
}
