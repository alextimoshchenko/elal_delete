package utils.global;

/**
 * Created with care by Shahar Ben-Moshe on 16/01/17.
 */

public class ParseUtils
{
	public static int tryParseStringToIntegerOrDefault(String iStringToParse, int iDefaultValue)
	{
		int result = iDefaultValue;
		
		try
		{
			result = Integer.parseInt(iStringToParse);
		}
		catch (Exception ignored)
		{
		}
		
		return result;
	}
}
