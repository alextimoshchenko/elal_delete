package utils.global.googleAnalitics.googleTagManager;

import com.google.android.gms.tagmanager.ContainerHolder;

/**
 * Created with care by Alexey.T on 27/08/2017.
 * <p>
 * TODO: Add a class header comment!
 */
public class ContainerHolderSingleton
{
	private static ContainerHolder containerHolder;
	
	/**
	 * Utility class; don't instantiate.
	 */
	private ContainerHolderSingleton()
	{
	}
	
	public static ContainerHolder getContainerHolder()
	{
		return containerHolder;
	}
	
	public static void setContainerHolder(ContainerHolder c)
	{
		containerHolder = c;
	}
}