package webServices.responses.deleteUserPushMessage;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with care by Alexey.T on 25/07/2017.
 */
public class ResponseDeleteUserPushMessageContent
{
	
	@JsonProperty("IsDeleted")
	private boolean mIsDeleted;
	@JsonProperty("Notifications")
	private List<NotificationInstance> mNotificationInstanceList = new ArrayList<>();
	
	public List<UserMessage> getNotificationInstanceList()
	{
		List<UserMessage> list = new ArrayList<>();
		list.addAll(mNotificationInstanceList);
		return (List<UserMessage>)(List<?>) mNotificationInstanceList  ;
	}
	
	public boolean isDeleted()
	{
		return mIsDeleted;
	}
}
