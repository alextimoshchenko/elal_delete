package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import global.UserData;

/**
 * Created by Avishay.Peretz on 27/08/2017.
 */

public class RequestGetGroupPNRsData
{
	
	@JsonProperty("LanguageCode")
	private String mLanguageCode = UserData.getInstance().getLanguage().getLanguageCode();
	
	@JsonProperty("GroupPNRs")
	private ArrayList<String> mGroupPNRs;
	
//	@JsonProperty("GroupPNRs")
//	private  String[] mGroupPNRs;
	
	public RequestGetGroupPNRsData(final ArrayList<String> iGroupPNRs)
	{
		mGroupPNRs = iGroupPNRs;
	}
	
//	public RequestGetGroupPNRsData(final  String[] iGroupPNRs)
//	{
//		mGroupPNRs = iGroupPNRs;
//	}
}
