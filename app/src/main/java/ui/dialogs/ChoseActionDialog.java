package ui.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import global.UserData;
import global.eLanguage;
import il.co.ewave.elal.R;
import interfaces.IChoseActionElement;
import utils.global.AppUtils;

public class ChoseActionDialog<T extends IChoseActionElement> extends Dialog
{
	private Context mContext;
	private IChoseActionListener mListener;
	private List<T> mChoseElement = new ArrayList<>();
	private View rootView;
	private ePositionState mState = ePositionState.NO_NEED;
	private eLanguage mLanguage;
	
	public interface IChoseActionListener
	{
		void onActionClick(IChoseActionElement iChoseElement);
	}
	
	public ChoseActionDialog(@NonNull final Context iContext, final IChoseActionListener iListener, List<T> iChoseElements)
	{
		super(iContext);
		
		mListener = iListener;
		mContext = iContext;
		
		if (iChoseElements != null && !iChoseElements.isEmpty())
		{
			mChoseElement = iChoseElements;
		}
		
		initDialog();
	}
	
	@SafeVarargs
	public ChoseActionDialog(@NonNull final Context iContext, final IChoseActionListener iListener, T... iChoseElement)
	{
		super(iContext);
		
		mListener = iListener;
		mContext = iContext;
		
		if (iChoseElement != null && iChoseElement.length > 0)
		{
			Collections.addAll(mChoseElement, iChoseElement);
		}
		
		initDialog();
	}
	
	public ChoseActionDialog(@NonNull final Context iContext, ePositionState iState, final IChoseActionListener iListener, List<T> iChoseElements)
	{
		super(iContext);
		
		mListener = iListener;
		mContext = iContext;
		
		if (iChoseElements != null && !iChoseElements.isEmpty())
		{
			mChoseElement = iChoseElements;
		}
		
		initDialog(iState);
	}
	
	@SafeVarargs
	public ChoseActionDialog(@NonNull final Context iContext, ePositionState iState, final IChoseActionListener iListener, T... iChoseElement)
	{
		super(iContext);
		
		mListener = iListener;
		mContext = iContext;
		
		if (iChoseElement != null && iChoseElement.length > 0)
		{
			Collections.addAll(mChoseElement, iChoseElement);
		}
		
		initDialog(iState);
	}
	
	private void initDialog()
	{
		initDialog(ePositionState.NO_NEED);
	}
	
	private void initDialog(ePositionState iState)
	{
		mLanguage = UserData.getInstance().getLanguage();
		mState = iState;
		initRootView();
		initDialogWindow();
	}
	
	private void initRootView()
	{
		rootView = LayoutInflater.from(mContext).inflate(R.layout.dialog_chose_action_layout, null, false);
		ViewGroup viewGroup = (ViewGroup) rootView.findViewById(R.id.ll_root_chose_action_dialog);
		
		for (int i = 0 ; i < mChoseElement.size() ; i++)
		{
			viewGroup.addView(getTextView(mChoseElement.get(i)));
			
			if (i != mChoseElement.size() - 1)
			{
				viewGroup.addView(getSeparator());
			}
		}
	}
	
	private View getSeparator()
	{
		View view = getLayoutInflater().inflate(R.layout.custom_separator, null, false);
		view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) AppUtils.convertPixelsToDp(1, mContext)));
		return view;
	}
	
	@NonNull
	private TextView getTextView(final IChoseActionElement element)
	{
		TextView textView = (TextView) getLayoutInflater().inflate(R.layout.custom_text_view, null, false);
		textView.setText(element.getElementName(getContext()));
		
		textView.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				ChoseActionDialog.this.dismiss();
				mListener.onActionClick(element);
			}
		});
		return textView;
	}
	
	private void initDialogWindow()
	{
		setContentView(rootView);
		if (this.getWindow() != null)
		{
			Window window = this.getWindow();
			WindowManager.LayoutParams params = window.getAttributes();
			params.width = (int) AppUtils.convertPixelsToDp(200, mContext);
			params.height = WindowManager.LayoutParams.WRAP_CONTENT;
			
			if (mState.isClasss())
			{
				if (mLanguage.isEnglish())
				{
					params.gravity = Gravity.END;
					params.y = 300;
				}
				else if (mLanguage.isHebrew())
				{
					params.gravity = Gravity.START;
					params.y = 300;
				}
			}
			else if (mState.isTravelRoute())
			{
				if (mLanguage.isEnglish())
				{
					params.gravity = Gravity.END;
				}
				else if (mLanguage.isHebrew())
				{
					params.gravity = Gravity.START;
				}
			}
			
			window.setAttributes(params);
			window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		}
	}
	
	public enum ePositionState
	{
		NO_NEED,
		TRAVEL_ROUTE, /*when user click on travel route field in search flight fragment*/
		CLASS; /*when user click on class field in search flight fragment*/
		
		ePositionState()
		{
		}
		
		public boolean isNoNeed()
		{
			return this == NO_NEED;
		}
		
		public boolean isTravelRoute()
		{
			return this == TRAVEL_ROUTE;
		}
		
		public boolean isClasss()
		{
			return this == CLASS;
		}
	}
}
