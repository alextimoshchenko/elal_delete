package ui.adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;

import global.ElAlApplication;
import il.co.ewave.elal.R;
import interfaces.IAppContentListener;
import ui.customWidgets.RoundedNetworkImageView;
import utils.global.CustomImageLoader;
import webServices.global.JacksonRequest;
import webServices.global.RequestStringBuilder;
import webServices.responses.getAppData.AppContent;

/**
 * Created with care by Shahar Ben-Moshe on 21/02/17.
 */

public class AppContentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
	private static final String TAG = AppContentAdapter.class.getSimpleName();
	
	private ArrayList<AppContent> mAppContent;
	private CustomImageLoader mImageLoader;
	private IAppContentListener mAppContentListener;
	private Context mContext;
	
	public AppContentAdapter(ArrayList<AppContent> iAppContent, @NonNull final CustomImageLoader iImageLoader, final IAppContentListener iAppContentListener, Context iContext)
	{
		mAppContent = iAppContent;
		mImageLoader = iImageLoader;
		mAppContentListener = iAppContentListener;
		mContext = iContext;
	}
	
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType)
	{
		RecyclerView.ViewHolder viewHolder;
		
		switch (viewType)
		{
			case AppContent.MEDIA_TYPE_VIDEO:
				viewHolder = new ViewHolderVideo(LayoutInflater.from(parent.getContext()).inflate(R.layout.app_content_video_layout_cell, parent, false));
				break;
			case AppContent.MEDIA_TYPE_IMAGE:
				viewHolder = new ViewHolderImage(LayoutInflater.from(parent.getContext()).inflate(R.layout.app_content_image_layout_cell, parent, false));
				break;
			case AppContent.MEDIA_TYPE_BUTTON:
			default:
				viewHolder = new ViewHolderButton(LayoutInflater.from(parent.getContext()).inflate(R.layout.app_content_button_layout_cell, parent, false));
				break;
		}
		
		return viewHolder;
	}
	
	@Override
	public void onBindViewHolder(final RecyclerView.ViewHolder iViewHolder, final int iPosition)
	{
		final AppContent currentItem = mAppContent.get(iPosition);
		
		if (currentItem != null)
		{
			
			switch (currentItem.getMediaTypeID())
			{
				case AppContent.MEDIA_TYPE_VIDEO:
					
					final ViewHolderVideo viewHolderVideo = (ViewHolderVideo) iViewHolder;
					if (currentItem.getLink() != null && currentItem.getLink().contains("http"))
					{
						viewHolderVideo.wvWebView.setFocusable(false);
						viewHolderVideo.tvTitle.setText(currentItem.getTitle());
						viewHolderVideo.wvWebView.setWebViewClient(new WebViewClient());
						viewHolderVideo.wvWebView.getSettings().setJavaScriptEnabled(true);
						viewHolderVideo.wvWebView.getSettings().setLoadWithOverviewMode(true);
						viewHolderVideo.wvWebView.getSettings().setUseWideViewPort(true);
						viewHolderVideo.wvWebView.getSettings().setDisplayZoomControls(false);
						viewHolderVideo.wvWebView.getSettings().setUserAgentString(JacksonRequest.USER_AGENT_CONTENT);
						viewHolderVideo.wvWebView.setWebChromeClient(new WebChromeClient()
						{
							@Override
							public void onProgressChanged(WebView view, int newProgress)
							{
								super.onProgressChanged(view, newProgress);
								
								// hide the progress bar if the loading is complete
								if (newProgress == 100)
								{
									viewHolderVideo.pbWebViewProgressBar.setVisibility(View.GONE);
								}
								else
								{
									viewHolderVideo.pbWebViewProgressBar.setVisibility(View.VISIBLE);
								}
							}
						});
						
						if (currentItem.getLink() == null || !currentItem.getLink().contains("http"))
						{
							viewHolderVideo.mRootView.setVisibility(View.GONE);
							viewHolderVideo.wvWebView.setVisibility(View.GONE);
						}
						else
						{
							// TODO: 08/03/17 Shahar change link from reuven
							viewHolderVideo.wvWebView.loadUrl(/*RequestStringBuilder.getYouTubeUrl(*/currentItem.getLink()/*)*/);
							
							viewHolderVideo.mRootView.setVisibility(View.VISIBLE);
							viewHolderVideo.wvWebView.setVisibility(View.VISIBLE);
						}
					}
					
					break;
				
				case AppContent.MEDIA_TYPE_BUTTON:
					
					final ViewHolderButton viewHolderButton = (ViewHolderButton) iViewHolder;
					viewHolderButton.nivImage.setFocusable(false);
					setButtonColorsByItem(viewHolderButton, currentItem.getButtonBackground(), currentItem.getTextColor());
					
					final String imgUrl = RequestStringBuilder.getAppContentsImageUrl(currentItem.getImage());
					if (!TextUtils.isEmpty(imgUrl))
					{
						viewHolderButton.nivImage.setImageUrl(RequestStringBuilder.getAppContentsImageUrl(currentItem.getImage()), mImageLoader);
						
						
						viewHolderButton.nivImage.setVisibility(View.VISIBLE);
					}
					else
					{
						viewHolderButton.nivImage.setVisibility(View.GONE);
					}
					
					viewHolderButton.tvTitle.setText(currentItem.getTitle());
					viewHolderButton.tvSubTitle.setText(currentItem.getDescription());
					
					viewHolderButton.btnLayoutClick.setOnClickListener(new View.OnClickListener()
					{
						@Override
						public void onClick(final View v)
						{
							if (mAppContentListener != null)
							{
								mAppContentListener.onButtonClick(currentItem);
							}
						}
					});
					break;
				
				case AppContent.MEDIA_TYPE_IMAGE:
				default:
					final ViewHolderImage viewHolderImage = (ViewHolderImage) iViewHolder;
					viewHolderImage.nivImage.setFocusable(false);
					final String iImgUrl = RequestStringBuilder.getAppContentsImageUrl(currentItem.getImage());
					Log.d("AppContents", "imgUrl: " + iImgUrl);
					
					viewHolderImage.tvTitle.setText(currentItem.getTitle());
					viewHolderImage.tvSubTitle.setText(currentItem.getDescription());
					
					if (mAppContentListener != null)
					{
						mAppContentListener.onLoadButtonImage(iImgUrl, viewHolderImage.nivImage, viewHolderImage.mFlImageFrame);
					}
					
					viewHolderImage.btnLayoutClick.setOnClickListener(new View.OnClickListener()
					{
						@Override
						public void onClick(final View v)
						{
							if (mAppContentListener != null)
							{
								mAppContentListener.onButtonClick(currentItem);
							}
						}
					});
					break;
			}
		}
	}
	
	private void setButtonColorsByItem(ViewHolderButton iButtonContainer, String iBackgroundColorHex, String iTextColorHex)
	{
		if (iBackgroundColorHex != null && !iBackgroundColorHex.isEmpty())
		{
			int bgColor = ElAlApplication.getInstance().getResources().getColor(R.color.pinkish_grey);
			
			if (iBackgroundColorHex.contains("#"))
			{
//				iButtonContainer.mButtonContainer.setBackground(null);
				try
				{
					bgColor = Color.parseColor(iBackgroundColorHex);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				
				if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP)
				{
					iButtonContainer.mButtonContainer.setBackgroundTintList(ColorStateList.valueOf(bgColor));
				}
				else
				{
//					iButtonContainer.mButtonContainer.setBackgroundColor(bgColor);
					
					/*
					 * ugly solution to get rounded colors according to fixed colors in CMS for Android APIs <= 5.1.+
					 * */
					
					// check if background equals CMS gray color & set gray background image from drawable
					if (bgColor == ElAlApplication.getInstance().getResources().getColor(R.color.button_gray))
					{
						iButtonContainer.mButtonContainer.setBackground(ElAlApplication.getInstance().getResources().getDrawable(R.drawable.button_gray));
					}
					else 
					{
						// set blue button background
						iButtonContainer.mButtonContainer.setBackground(ElAlApplication.getInstance().getResources().getDrawable(R.drawable.button_blue_content));
					}
				}
			}
		}
		
		if (iTextColorHex != null && !iTextColorHex.isEmpty())
		{
			int txtColor = ElAlApplication.getInstance().getResources().getColor(R.color.white);
			if (iTextColorHex.contains("#"))
			{
				try
				{
					txtColor = Color.parseColor(iTextColorHex);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			
			iButtonContainer.tvTitle.setTextColor(txtColor);
			iButtonContainer.tvSubTitle.setTextColor(txtColor);
		}
	}
	
	@Override
	public int getItemViewType(final int iPosition)
	{
		return mAppContent.get(iPosition).getMediaTypeID();
	}
	
	@Override
	public int getItemCount()
	{
		return mAppContent == null ? 0 : mAppContent.size();
	}
	
	private class ViewHolderVideo extends RecyclerView.ViewHolder
	{
		private TextView tvTitle;
		private RelativeLayout rlWebViewRoot;
		private WebView wvWebView;
		private ProgressBar pbWebViewProgressBar;
		private LinearLayout mRootView;
		
		ViewHolderVideo(final View itemView)
		{
			super(itemView);
			
			this.mRootView = (LinearLayout) itemView.findViewById(R.id.lil_appContentVideoItem_Root);
			this.tvTitle = (TextView) itemView.findViewById(R.id.tv_appContentVideoItem_Title);
			this.rlWebViewRoot = (RelativeLayout) itemView.findViewById(R.id.rl_appContentVideoItem_WebViewRoot);
			this.wvWebView = (WebView) itemView.findViewById(R.id.wv_appContentVideoItem_WebView);
			this.pbWebViewProgressBar = (ProgressBar) itemView.findViewById(R.id.pb_appContentVideoItem_WebViewProgressBar);
		}
	}
	
	private class ViewHolderButton extends RecyclerView.ViewHolder
	{
		private TextView tvTitle;
		private TextView tvSubTitle;
		private NetworkImageView nivImage;
		private Button btnLayoutClick;
		private LinearLayout mButtonContainer;
		
		ViewHolderButton(final View itemView)
		{
			super(itemView);
			
			this.mButtonContainer = (LinearLayout) itemView.findViewById(R.id.lil_buttonContainer);
			this.tvTitle = (TextView) itemView.findViewById(R.id.tv_appContentButtonItem_Title);
			this.tvSubTitle = (TextView) itemView.findViewById(R.id.tv_appContentButtonItem_SubTitle);
			this.nivImage = (NetworkImageView) itemView.findViewById(R.id.niv_appContentButtonItem_Image);
			this.btnLayoutClick = (Button) itemView.findViewById(R.id.btn_appContentButtonItem_LayoutClick);
		}
	}
	
	private class ViewHolderImage extends RecyclerView.ViewHolder
	{
		private TextView tvTitle;
		private TextView tvSubTitle;
//		private NetworkImageView nivImage;
		private RoundedNetworkImageView nivImage;
		private Button btnLayoutClick;
		private FrameLayout mFlImageFrame;
		
		ViewHolderImage(final View itemView)
		{
			super(itemView);
			
			this.tvTitle = (TextView) itemView.findViewById(R.id.tv_appContentImageItem_Title);
			this.tvSubTitle = (TextView) itemView.findViewById(R.id.tv_appContentImageItem_SubTitle);
//			this.nivImage = (NetworkImageView) itemView.findViewById(R.id.niv_appContentImageItem_Image);
			this.nivImage = (RoundedNetworkImageView) itemView.findViewById(R.id.niv_appContentImageItem_Image);
			this.btnLayoutClick = (Button) itemView.findViewById(R.id.btn_appContentImageItem_LayoutClick);
			this.mFlImageFrame = (FrameLayout) itemView.findViewById(R.id.flImageFrame);
		}
	}
}
