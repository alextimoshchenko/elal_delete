package ui.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import global.ElAlApplication;
import global.IntentExtras;
import global.UserData;
import global.eLanguage;
import il.co.ewave.elal.R;
import utils.global.AppUtils;
import utils.global.LocaleUtils;
import webServices.controllers.LoginController;
import webServices.responses.ResponseUserLogin;
import webServices.responses.promo.Promo;

/**
 * Created with care by Shahar Ben-Moshe on 20/12/16.
 */

public class LoginActivity extends EWBaseActivity implements Response.Listener<ResponseUserLogin>, Response.ErrorListener
{
	public static void tryStartLoginActivity(Activity iActivity, Promo iPromoData)
	{
		Intent intent = new Intent(iActivity, LoginActivity.class);
		intent.putExtra(IntentExtras.PROMO_DATA, iPromoData);
		iActivity.startActivity(intent);
	}
	
	private Promo mPromoData;
	private Toolbar mLlToolbar;
	private EditText mEtEmail;
	private EditText mEtPassword;
	private TextView mTvForgotMyPassword;
	private TextView mTvIncorrectUserNameOrPassword;
	private CheckBox mCbRememberMe;
	private Button mTvContinue;
	private Button mBtnRegister;
	private TextView mTvContinueAsAGuest;
	
	@Override
	protected void initScreen()
	{
		setContentView(R.layout.activity_login_layout);
		
		getIntentExtras();
		initReference();
		initToolbar();
		setListeners();
		setErrorMessage("");
	}
	
	private void getIntentExtras()
	{
		Bundle bundle = getIntent().getExtras();
		if (bundle != null)
		{
			mPromoData = bundle.getParcelable(IntentExtras.PROMO_DATA);
		}
	}
	
	private void initReference()
	{
		mLlToolbar = (Toolbar) findViewById(R.id.ll_login_Toolbar);
		mEtEmail = (EditText) findViewById(R.id.et_login_Email);
		mEtPassword = (EditText) findViewById(R.id.et_login_RegisteredAccountPassword);
		mEtPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
		mTvForgotMyPassword = (TextView) findViewById(R.id.tv_login_RegisteredAccountForgotMyPassword);
		mTvIncorrectUserNameOrPassword = (TextView) findViewById(R.id.tv_login_IncorrectUsernameOrPassword);
		mCbRememberMe = (CheckBox) findViewById(R.id.cb_login_RememberMe);
		mTvContinue = (Button) findViewById(R.id.tv_login_Continue);
		mBtnRegister = (Button) findViewById(R.id.btn_login_Register);
		mTvContinueAsAGuest = (TextView) findViewById(R.id.tv_login_ContinueAsAGuest);
	}
	
	private void initToolbar()
	{
		if (mLlToolbar != null)
		{
			mLlToolbar.findViewById(R.id.img_actionBar_drawer).setVisibility(View.GONE);
			//			mLlToolbar.findViewById(R.id.ll_actionBar_flightSchedule).setVisibility(View.GONE);
			((ImageView) mLlToolbar.findViewById(R.id.img_actionBar_logo)).setImageResource(UserData.getInstance().getLanguage() == eLanguage.Hebrew ? R.mipmap.elal_logo_he : R.mipmap.elal_logo_en);
		}
	}
	
	private void setListeners()
	{
		mTvForgotMyPassword.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View iView)
			{
				doOnForgotPasswordClick();
			}
		});
		
		mTvContinue.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View iView)
			{
				doOnContinueClick();
			}
		});
		
		mBtnRegister.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View iView)
			{
				doOnRegisterClick();
			}
		});
		
		mTvContinueAsAGuest.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View iView)
			{
				doOnContinueAsAGuestClick();
			}
		});
	}
	
	private void doOnForgotPasswordClick()
	{
		startActivity(new Intent(this, ForgotPasswordActivity.class));
	}
	
	private void doOnContinueClick()
	{
		if (isDataValidAndShowErrorMessageIfNeeded())
		{
			if (isServiceConnected())
			{
				setProgressDialog(true);
				setErrorMessage("");
				getService().getController(LoginController.class).LoginAsUser(mEtEmail.getText().toString(), mEtPassword.getText().toString(), mCbRememberMe.isChecked(), this, this);
			}
		}
	}
	
	private boolean isDataValidAndShowErrorMessageIfNeeded()
	{
		boolean result = false;
		
		if (TextUtils.isEmpty(mEtEmail.getText().toString()))
		{
			setErrorMessage(getString(R.string.empty_email_address));
		}
		else if (!AppUtils.isEmailValid(mEtEmail.getText().toString()))
		{
			setErrorMessage(getString(R.string.invalid_email_address));
		}
		else if (TextUtils.isEmpty(mEtPassword.getText().toString()))
		{
			setErrorMessage(getString(R.string.empty_password));
		}
		else
		{
			setErrorMessage("");
			result = true;
		}
		
		return result;
	}
	
	private void doOnRegisterClick()
	{
		startActivity(new Intent(this, RegistrationActivity.class));
	}
	
	private void doOnContinueAsAGuestClick()
	{
		if (isServiceConnected())
		{
			setProgressDialog(true);
			setErrorMessage("");
			getService().getController(LoginController.class).LoginAsGuest(this, this);
		}
	}
	
	private void goToPromoActivity()
	{
		PromoActivity.tryStartPromoActivity(this, mPromoData);
		finish();
	}
	
	private void setErrorMessage(final String iTextToShow)
	{
		mTvIncorrectUserNameOrPassword.setText(iTextToShow == null ? "" : iTextToShow);
	}
	
	@Override
	protected void onServiceConnected()
	{
	}
	
	@Override
	public void onResponse(final ResponseUserLogin response)
	{
		setProgressDialog(false);
		
		if (response != null && response.getContent() != null)
		{
			if (mCbRememberMe.isChecked())
			{
				UserData.getInstance().setEmailAndPassword(mEtEmail.getText().toString(), mEtPassword.getText().toString());
			}
			else
			{
				UserData.getInstance().setEmailAndPassword(null, null);
				//				UserData.getInstance().setTemporaryPassword(null);
				UserData.getInstance().setEmail(mEtEmail.getText().toString());
				UserData.getInstance().setTemporaryPassword(mEtPassword.getText().toString());
			}
			UserData.getInstance().setUserId(response.getContent().getUserID());
			UserData.getInstance().setUserObject(response.getContent().getUserObject());
			UserData.getInstance().saveUserData();
			
			eLanguage userLanguageInServer = eLanguage.getLanguageByCodeOrDefault(response.getContent().getUserObject().getLanguageID(), eLanguage.English);
			if (userLanguageInServer != UserData.getInstance().getLanguage())
			{
				UserData.getInstance().setLanguage(userLanguageInServer);
				UserData.getInstance().saveUserData();
				LocaleUtils.applyLocale(ElAlApplication.getInstance(), LocaleUtils.convertLanguageCodeToLocaleOrNull(userLanguageInServer.getLanguageCode()));
				
				AlertDialog dialog = AppUtils.createSimpleMessageDialog(this, getString(R.string.restart_after_language_change), null, null, getString(R.string.close), new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(final DialogInterface dialog, final int which)
					{
						LoginActivity.this.finish();
					}
				});
				dialog.setCancelable(false);
				dialog.show();
			}
			else
			{
				goToPromoActivity();
			}
		}
		else
		{
			// TODO: 05/01/17 Shahar handle case.
		}
	}
	
	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
	}
	
	@Override
	public void onErrorResponse(final VolleyError error)
	{
		setErrorMessage(getString(R.string.incorrect_username_or_password));
		setProgressDialog(false);
	}
	
}
