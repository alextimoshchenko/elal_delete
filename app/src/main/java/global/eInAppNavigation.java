package global;


import ui.activities.MainActivity;
import ui.activities.MyFlightsActivity;
import ui.activities.NotificationActivity;
import ui.activities.SearchFlightActivity;
import ui.fragments.AddNewFlightFragment;
import ui.fragments.ChangePasswordFragment;
import ui.fragments.details.DetailScreenFragment;
import ui.fragments.FamilyCardFragment;
import ui.fragments.ForgotPasswordFragment;
import ui.fragments.HomeFragment;
import ui.fragments.LoginFragment;
import ui.fragments.MyDocumentsFragment;
import ui.fragments.RegistrationFragment;
import ui.fragments.SettingsFragment;
import ui.fragments.WebViewFragment;
import ui.fragments.myFlights.MyFlightFragmentNew2;
import ui.fragments.myFlights.MyFlightsFragment;

/**
 * Created by Avishay.Peretz on 13/09/2017.
 */


public enum eInAppNavigation
{
	Error(0, null, null, false),
	Empty(1, null, null, false), // solution for same activities but need to reload - not the ideal, but works. because the "NotificationActivity" architecture not compatible with the
	// EBaseDrawerActivity...
	
	//Login module
	Login(100, MainActivity.class, LoginFragment.class, false),
	Registration(110, MainActivity.class, RegistrationFragment.class, false),
	FamilyScreen(130, MainActivity.class, FamilyCardFragment.class, true),
	
	//ForgotPassword(160, ForgotPasswordActivity.class, false),
	ForgotPassword(160, MainActivity.class, ForgotPasswordFragment.class, false),
	ChangePassword(230, MainActivity.class, ChangePasswordFragment.class, true),
	
	//Main module
	Main(170, MainActivity.class, HomeFragment.class, false),
	
	//MyFlights module
	MyFlights(180, MyFlightsActivity.class, MyFlightsActivity.class, false),
	MyFlightsDeck(190, MyFlightsActivity.class, MyFlightsFragment.class, false),
	MyFlight(200, MyFlightsActivity.class, MyFlightFragmentNew2.class, false),
	
	Settings(210, MainActivity.class, SettingsFragment.class, false),
	
	UserInfo(220, MainActivity.class, DetailScreenFragment.class, true),
	MyDocuments(250, MainActivity.class, MyDocumentsFragment.class, true),
	
	//Search module
	FlightSearch(260, SearchFlightActivity.class, SearchFlightActivity.class, false),
	
	//Notification module
	Notifications(270, NotificationActivity.class, NotificationActivity.class, false),
	
	AddNewFlightFragment(99, MyFlightsActivity.class, AddNewFlightFragment.class, false),
	
	//WebView
	WebView(9999, MainActivity.class, WebViewFragment.class, false);
	
	private final int mNavigationId;
	private final Class mNavigationActivityClass;
	private final Class mNavigationFragmentClass;
	private boolean mPersonal;
	
	eInAppNavigation(final int iNavigationId, final Class iNavigationActivityClass, final Class iNavigationFragmentClass, final boolean iPersonal)
	{
		mNavigationId = iNavigationId;
		mNavigationActivityClass = iNavigationActivityClass;
		mNavigationFragmentClass = iNavigationFragmentClass;
		mPersonal = iPersonal;
	}
	
	public int getNavigationId()
	{
		return mNavigationId;
	}
	
	public Class getNavigationActivityClass()
	{
		return mNavigationActivityClass;
	}
	
	public Class getNavigationFragmentClass()
	{
		return mNavigationFragmentClass;
	}
	
	public static eInAppNavigation getInAppNavigationByIdOrErrorDefault(int iNavigationId)
	{
		eInAppNavigation result = Error;
		
		for (eInAppNavigation inAppNavigation : values())
		{
			if (inAppNavigation != null && inAppNavigation.getNavigationId() == iNavigationId)
			{
				result = inAppNavigation;
				break;
			}
		}
		
		return result;
	}
	
	public boolean isWebView()
	{
		return eInAppNavigation.this == WebView;
	}
	
	public boolean isChangePassword()
	{
		return eInAppNavigation.this == ChangePassword;
	}
	
	public boolean isFamilyScreen()
	{
		return eInAppNavigation.this.getNavigationId() == FamilyScreen.getNavigationId();
	}
	
	public boolean isPersonal()
	{
		return mPersonal;
	}
	
	// This method works but before use it we need to solve navigation problems, because we need to separate all to moduls , now for swipe we in some cases we are using fragents and in some cases
	// activities, but we need to in all cases use activities
	public static int getClassIdByClass(Class iClass)
	{
		int result = -1;
		
		if (iClass != null)
		{
			String currentClassName = iClass.getSimpleName();
			eInAppNavigation[] navigations = values();
			
			for (eInAppNavigation tmpNav : navigations)
			{
				Class tpmClass = tmpNav.getNavigationFragmentClass();
				
				if (tpmClass != null)
				{
					String className = tmpNav.getNavigationFragmentClass().getSimpleName();
					
					if (className.equals(currentClassName))
					{
						result = tmpNav.getNavigationId();
					}
				}
			}
		}
		
		return result;
	}
}
