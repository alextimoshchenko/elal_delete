package utils.ssl;

import android.content.Context;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

/**
 * Created with care by Shahar Ben-Moshe on 7/16/15.<BR>
 * <BR>
 * <B>Requires an import of library 'org.apache.httpcomponents:httpclient-android'.</B><BR>
 * <B>Requires an implementation of the following in the gradle file:</B><BR>
 * <BR>
 * android{
 * <pre>
 * useLibrary 'org.apache.http.legacy'
 * </pre>
 * }<BR>
 */
public class SslUtils
{
	/**
	 * If you are adding variable here, also add this value in `dexguard-project.txt` under this configuration `-encryptstrings public class utils.ssl.SslUtils{ .... }`
	 */
	public static final String PROTOCOL_TLSv1 = "TLSv1";
	public static final String PROTOCOL_TLSv1_1 = "TLSv1.1";
	public static final String PROTOCOL_TLSv1_2 = "TLSv1.2";
	
	public static final String SECRET_KEY = "8e983237-0e7d-4084-b0be-bbd56bcc668b";
	static final String TRUST_MANAGER_X509 = "X509";
	static final String SSL_LOCAL_HOST_PROD = "dpel.elal.co.il";
	static final String SSL_LOCAL_HOST_DEV = "elal.co.il";
	static final String SSL_LOCAL_HOST_MATMID = "www.elal-matmid.com";
	
	//Certificate keys
	public static final String MATMID_CA_KEY = "3059301306072a8648ce3d020106082a8648ce3d03010703420004bc6bcf8966618247207d33701f766c1ea7ad89c54c343e8a1ebb58263578a5bdd4a3a9b6ac724540dc0a7bb07efd07b4543b2c69c7ec380b3c0717f3fe924b7c";
	public static final String MATMID_ROOT_KEY = "3059301306072a8648ce3d020106082a8648ce3d030107034200042f77f9a018352412f85e4fdfdab1cb70219a43417e760c13d9722121090af7eb0524a255cdb2480eaf7ba187d914649ea0adc57f39704c9a420001a3160788bd";
	
	public static final String MIZDAMEN_CA_KEY = "30820122300d06092a864886f70d01010105000382010f003082010a0282010100d34b9e882fbc993a57841adb2dd0e9f1222c2cc07ab637f2c8bf23dc5c46a9e130adc7643afff040178d74ea83368b4d29dd03d4db594dfa8e93423aaf59118e81bfb5d4816dc7e910f69a9956b2a40a134b23c9cbfdd44c3a248bbbc673c9754a4bb04e08b1ec8e15c865db9d296b6bdf91495e0c000ff69f1056c6019eea4bfce35a97e493ce7f4cc22ff431fe068a053ad01de283eaa9802cb90d168e1cb50a1000f38fc6321c9e64e3a67e49cd590fc2a649e027be66d84edb84f9159d9bf6c7d07f51f2aa81364f91c34000f2b3a188213458a882f6c02c9d589f47e02a86f2d4168cc2e6d17e4d95546009bff587abf0d521e511b11a756fa2f52945010203010001";
	public static final String MIZDAMEN_ROOT_KEY = "30820122300d06092a864886f70d01010105000382010f003082010a0282010100b2fc06fb0493d2ea59203b4485975239e710f07ae0b09440da46f80c28bbb9ce60383fd2d811421b91ad49ee8fc7de6cde376ffd8b203c6de774d3dcd52488418089ee36bec4d5be8d5313aae4a5b8930abeecdacd3cd43256efd04ea0b897bb39501e6e65c3fdb2cee059a94809c6febeaefc3e3b8120978b8f46df60640775bb1b86389f477b34cea1d197ad76d89fb726db79803648f2c537f8d932ae7ca45381c799a154382f4f75a0bb5aa5bbcdac025b1902d51318a7ceac745512058b9ba29546647238cd5a1b3a16a7be71998c5403b8966c01d33e06983f21813b027e004753011e0e4643fb4b2ddc0b1ae82f98f87ed199ab136ca417de6ff615f50203010001";
	
	static final String KEYSTORE_EXTENSION_BKS = "BKS";
	
	static final int COUNT_OF_CER_MATCHES = 2;
	static final int COUNT_OF_HOST_MATCHES = 1;
	
	public static final int SSL_PORT = 443;
	
	/**
	 * Creates a new {@link CloseableHttpClient} object with SSL capabilities.
	 *
	 * @param iContext           - This is used to load the certificates from the raw resource folder.
	 * @param iSslProtocol       - The SSL protocol to be used. The SSL protocols can be found in this class with the prefix <B>"PROTOCOL"</B> or can be entered manually.
	 * @param iClientCertificate - The client certificate to be used. Can be null if not needed.
	 * @param iServerCertificate - The server certificate to be used. <B>If null accepts ALL CONNECTIONS.</B>
	 *
	 * @return An {@link CloseableHttpClient} with SSL capabilities.
	 *
	 * @throws KeyManagementException
	 * @throws UnrecoverableKeyException
	 * @throws KeyStoreException
	 * @throws NoSuchAlgorithmException
	 * @throws CertificateException
	 * @throws IOException
	 */
	public static CloseableHttpClient createSslHttpClient(Context iContext, String iSslProtocol, SslCertificate iClientCertificate,
	                                                      SslCertificate iServerCertificate) throws KeyManagementException, UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException
	{
		CloseableHttpClient closeableHttpClient;
		
		SSLContext sslContext = SSLContext.getInstance(iSslProtocol);
		KeyManager[] keyManagers = iClientCertificate == null ? null : generateKeyManagersForCertificate(iContext, iClientCertificate);
		TrustManager[] trustManagers = iServerCertificate == null ? generateTrustManagersForAll() : generateTrustManagersForCertificate(iContext, iServerCertificate);
		sslContext.init(keyManagers, trustManagers, null);
		SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(sslContext);
		//Specify more details if needed
		//		SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(sslContext, new String[] {"TLSv1", "TLSv1.1", "TLSv1.2"}, null, null);
		
		closeableHttpClient = HttpClients.custom().setSSLSocketFactory(sslConnectionSocketFactory).build();
		
		return closeableHttpClient;
	}
	
	private static KeyManager[] generateKeyManagersForCertificate(Context iContext,
	                                                              SslCertificate iClientCertificate) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException, UnrecoverableKeyException
	{
		KeyManager[] result = null;
		
		//load certificate for client
		KeyStore keyStore = KeyStore.getInstance(iClientCertificate.getCertificateType());
		InputStream ksis = iContext.getResources().openRawResource(iClientCertificate.getCertificateResourceId());
		keyStore.load(ksis, iClientCertificate.getCertificatePassword().toCharArray());
		
		//load client keystore to key manager
		KeyManagerFactory kmf = KeyManagerFactory.getInstance(iClientCertificate.getCertificateAlgorithm());
		kmf.init(keyStore, iClientCertificate.getCertificatePassword().toCharArray());
		result = kmf.getKeyManagers();
		
		return result;
	}
	
	private static TrustManager[] generateTrustManagersForCertificate(Context iContext,
	                                                                  SslCertificate iServerCertificate) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException
	{
		TrustManager[] result;
		
		//load certificate for server
		KeyStore trustStore = KeyStore.getInstance(iServerCertificate.getCertificateType());
		InputStream tsis = iContext.getResources().openRawResource(iServerCertificate.getCertificateResourceId());
		trustStore.load(tsis, iServerCertificate.getCertificatePassword().toCharArray());
		
		//load client keystore to trust manager
		TrustManagerFactory tmf = TrustManagerFactory.getInstance(iServerCertificate.getCertificateAlgorithm());
		tmf.init(trustStore);
		result = tmf.getTrustManagers();
		
		return result;
	}
	
	private static TrustManager[] generateTrustManagersForAll()
	{
		return new TrustManager[] {new X509TrustManager()
		{
			public java.security.cert.X509Certificate[] getAcceptedIssuers()
			{
				return null;
			}
			
			public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType)
			{
			}
			
			public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)
			{
			}
		}};
	}
	
	/**
	 * This method for hashing the request body with the secret key
	 * This snippet calculate md5 for any given string
	 **/
	public static String doHashMd5(String s)
	{
		try
		{
			// Create MD5 Hash
			MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();
			
			// Create Hex String
			StringBuffer hexString = new StringBuffer();
			
			for (int i = 0 ; i < messageDigest.length ; i++)
			{
				hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
			}
			
			return hexString.toString();
			
		}
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}
		return "";
	}
	
	
	public static String doHashSha256(String base)
	{
		try
		{
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest(base.getBytes("UTF-8"));
			StringBuffer hexString = new StringBuffer();
			
			for (int i = 0 ; i < hash.length ; i++)
			{
				String hex = Integer.toHexString(0xff & hash[i]);
				if (hex.length() == 1)
				{
					hexString.append('0');
				}
				hexString.append(hex);
			}
			
			return hexString.toString();
		}
		catch (Exception ex)
		{
			throw new RuntimeException(ex);
		}
	}
}