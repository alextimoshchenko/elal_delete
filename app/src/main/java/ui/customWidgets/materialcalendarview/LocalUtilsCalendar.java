package ui.customWidgets.materialcalendarview;

import java.util.Locale;

import global.UserData;

/**
 * Created with care by Alexey.T on 19/10/2017.
 * <p>
 * TODO: Add a class header comment!
 */
public class LocalUtilsCalendar
{
	public static boolean isRTL()
	{
//		return isRTL(Locale.getDefault());
		return UserData.getInstance().getLanguage().isHebrew();
	}
	
	private static boolean isRTL(Locale locale)
	{
		final int directionality = Character.getDirectionality(locale.getDisplayName().charAt(0));
		return directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT || directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC;
	}
}
