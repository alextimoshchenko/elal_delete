package webServices.responses.UserCheckList;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import utils.global.DateTimeUtils;

/**
 * Created with care by Shahar Ben-Moshe on 21/02/17.
 */

public class UserCheckList implements Parcelable
{
	@JsonProperty("From24Hours")
	private ArrayList<CheckListItem> mToDo24HoursBefore;
	@JsonProperty("From7DaysTo24Hours")
	private ArrayList<CheckListItem> mToDoWeekBefore;
	@JsonProperty("FromReservationTo7Days") //to do Now
	private ArrayList<CheckListItem> mLessThan24Hours;
	
	public UserCheckList()
	{
	}
	
	public ArrayList<CheckListItem> getLessThan24Hours()
	{
		return mLessThan24Hours;
	}
	
	public ArrayList<CheckListItem> getToDo24HoursBefore()
	{
		return mToDo24HoursBefore;
	}
	
	public ArrayList<CheckListItem> getToDoWeekBefore()
	{
		return mToDoWeekBefore;
	}
	
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeTypedList(this.mLessThan24Hours);
		dest.writeTypedList(this.mToDo24HoursBefore);
		dest.writeTypedList(this.mToDoWeekBefore);
	}
	
	protected UserCheckList(Parcel in)
	{
		this.mLessThan24Hours = in.createTypedArrayList(CheckListItem.CREATOR);
		this.mToDo24HoursBefore = in.createTypedArrayList(CheckListItem.CREATOR);
		this.mToDoWeekBefore = in.createTypedArrayList(CheckListItem.CREATOR);
	}
	
	public static final Parcelable.Creator<UserCheckList> CREATOR = new Parcelable.Creator<UserCheckList>()
	{
		@Override
		public UserCheckList createFromParcel(Parcel source)
		{
			return new UserCheckList(source);
		}
		
		@Override
		public UserCheckList[] newArray(int size)
		{
			return new UserCheckList[size];
		}
	};
	
	public UserCheckList orderListByTimes(int iTimeDifference)
	{
		if (iTimeDifference < DateTimeUtils.HOURS_OF_A_DAY)
		{
			if (mToDoWeekBefore != null && !mToDoWeekBefore.isEmpty())
			{
				mLessThan24Hours.addAll(mToDoWeekBefore);
				mToDoWeekBefore.clear();
			}
			if (mToDo24HoursBefore != null && !mToDo24HoursBefore.isEmpty())
			{
				mLessThan24Hours.addAll(mToDo24HoursBefore);
				mToDo24HoursBefore.clear();
			}
		}
		else if (iTimeDifference < DateTimeUtils.HOURS_OF_A_DAY * 7)
		{
			if (mToDoWeekBefore != null && !mToDoWeekBefore.isEmpty())
			{
				mLessThan24Hours.addAll(mToDoWeekBefore);
				mToDoWeekBefore.clear();
			}
		}
		return this;
	}
	
}
