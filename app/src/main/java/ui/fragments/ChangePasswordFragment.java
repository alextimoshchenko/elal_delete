package ui.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import global.UserData;
import il.co.ewave.elal.R;

import ui.customWidgets.ChangeDirectionTextInputLayout;
import ui.customWidgets.ElalCustomTilLL;
import utils.errors.ErrorsHandler;
import utils.global.AppUtils;
import utils.global.CustomTextWatcher;
import webServices.controllers.LoginController;
import webServices.requests.RequestChangePassword;
import webServices.responses.ResponseChangePassword;


/**
 * Created by Avishay.Peretz on 20/06/2017.
 */

public class ChangePasswordFragment extends EWBaseFragment implements Response.ErrorListener
{
	private static final String TAG = ChangePasswordFragment.class.getSimpleName();
	
	private TextView mTvTitle;
	private ImageView mBtnBack;
	
	private ElalCustomTilLL mTilCurrentPassword;
	private EditText mEtCurrentPassword;
	private ElalCustomTilLL mTilNewPassword;
	private EditText mEtNewPassword;
	private ElalCustomTilLL mTilConfirmNewPassword;
	private EditText mEtConfirmNewPassword;
	
	private Button mBtnContinue;
	private ImageView mIvTooltipNewPassword;
	private ImageView mIvTooltipConfirmPassword;
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_change_password_layout);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		initReference();
		setGui();
		setListeners();
		pushScreenOpenEvent("Change Password");
	}
	
	
	private void initReference()
	{
		Activity act = getActivity();
		mTvTitle = (TextView) act.findViewById(R.id.tv_name_screen_title);
		mBtnBack = (ImageView) act.findViewById(R.id.iv_name_screen_back_arrow);
		
		mTilCurrentPassword = (ElalCustomTilLL) act.findViewById(R.id.til_current_password);
		mTilCurrentPassword.init(getString(R.string.current_password_asterisk));
		//		mTilCurrentPassword.setDefaultHintText(getString(R.string.current_password_asterisk));
		mTilCurrentPassword.setInputTypePassword();
		
		mEtCurrentPassword = mTilCurrentPassword.getEditText();
		
		mTilNewPassword = (ElalCustomTilLL) act.findViewById(R.id.til_new_password);
		mTilNewPassword.init(getString(R.string.new_password_asterisk));
		//		mTilNewPassword.setDefaultHintText(getString(R.string.new_password_asterisk));
		mTilNewPassword.setInputTypePassword();
		
		mEtNewPassword = mTilNewPassword.getEditText();
		
		mTilConfirmNewPassword = (ElalCustomTilLL) act.findViewById(R.id.til_confirm_new_password);
		mTilConfirmNewPassword.init(getString(R.string.confirm_new_password_asterisk));
		//		mTilConfirmNewPassword.setDefaultHintText(getString(R.string.confirm_new_password_asterisk));
		mTilConfirmNewPassword.setInputTypePassword();
		
		mEtConfirmNewPassword = mTilConfirmNewPassword.getEditText();
		mEtConfirmNewPassword.setImeOptions(EditorInfo.IME_ACTION_DONE);
		
		mIvTooltipNewPassword = (ImageView) act.findViewById(R.id.iv_tooltip_new_password);
		mIvTooltipConfirmPassword = (ImageView) act.findViewById(R.id.iv_tooltip_confirm_new_password);
		
		mBtnContinue = (Button) act.findViewById(R.id.btn_frag_reg_continue);
		
	}
	
	
	private void setGui()
	{
		mTvTitle.setText(getString(R.string.change_password));
	}
	
	
	private void setListeners()
	{
		mBtnBack.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				tryPopFragment();
			}
		});
		
		mEtCurrentPassword.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				showPasswordError(mTilCurrentPassword, false, null);
			}
		});
		
		mEtCurrentPassword.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus && !TextUtils.isEmpty(mEtCurrentPassword.getText().toString()) && !isPasswordValid(mEtCurrentPassword.getText().toString()))
				{
					showInvalidPasswordError(mTilCurrentPassword, true);
				}
			}
		});
		
		mEtNewPassword.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				showPasswordError(mTilNewPassword, false, null);
			}
		});
		
		mEtNewPassword.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus && !TextUtils.isEmpty(mEtNewPassword.getText().toString()) && !isPasswordValid(mEtNewPassword.getText().toString()))
				{
					showInvalidPasswordError(mTilNewPassword, true);
				}
			}
		});
		
		mEtConfirmNewPassword.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				showPasswordError(mTilConfirmNewPassword, false, null);
			}
		});
		
		mEtConfirmNewPassword.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus && !TextUtils.isEmpty(mEtConfirmNewPassword.getText().toString()))
				{
					if (!isPasswordValid(mEtConfirmNewPassword.getText().toString()))
					{
						showInvalidPasswordError(mTilConfirmNewPassword, true);
					}
					else if (!isNewPasswordsMatches())
					{
						showPasswordsNotMatchError(mTilConfirmNewPassword, true);
					}
				}
			}
		});
		
		mIvTooltipNewPassword.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View iV)
			{
				showToolTip(iV,  getString(R.string.frag_change_password_tooltip) );
			}
		});
		
		mIvTooltipConfirmPassword.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View iV)
			{
				showToolTip(iV,  getString(R.string.frag_change_password_tooltip) );
			}
		});
		
		mBtnContinue.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnContinue();
			}
		});
		
	}
	
	private boolean isPasswordValid(String iPassword)
	{
		return AppUtils.isPasswordStrong(iPassword, false);
	}
	
	private void showPasswordError(ChangeDirectionTextInputLayout iTilPassword, boolean iState, String iError)
	{
		if (iState)
		{
			iTilPassword.setError(iError);
		}
		else
		{
			iTilPassword.setError(null);
		}
	}
	
	private void showPasswordError(ElalCustomTilLL iTilPassword, boolean iState, String iError)
	{
		if (iState)
		{
			iTilPassword.setError(true, iError);
		}
		else
		{
			iTilPassword.setError(false, null);
		}
	}
	
	
	private void showInvalidPasswordError(ChangeDirectionTextInputLayout iTilPassword, boolean iState)
	{
		
		showPasswordError(iTilPassword, iState, getString(R.string.invalid_password));
		//		if (iState)
		//		{
		//			iTilPassword.setError(getActivity().getResources().getString(R.string.invalid_password));
		//		}
		//		else
		//		{
		//			iTilPassword.setError(null);
		//		}
	}
	
	
	private void showPasswordsNotMatchError(ChangeDirectionTextInputLayout iTilPassword, boolean iState)
	{
		showPasswordError(iTilPassword, iState, getString(R.string.password_not_match));
		//		if (iState)
		//		{
		//			iTilPassword.setError(getActivity().getResources().getString(R.string.password_not_match));
		//		}
		//		else
		//		{
		//			iTilPassword.setError(null);
		//		}
	}
	
	
	private void showInvalidPasswordError(ElalCustomTilLL iTilPassword, boolean iState)
	{
		
		showPasswordError(iTilPassword, iState, getString(R.string.invalid_password));
		
	}
	
	private void showPasswordsNotMatchError(ElalCustomTilLL iTilPassword, boolean iState)
	{
		showPasswordError(iTilPassword, iState, getString(R.string.password_not_match));
		
	}
	
	
	private void doOnContinue()
	{
		if (!isFieldsValid() || !isNewPasswordsMatches())
		{
			return;
		}
		
		AppUtils.hideKeyboard(getActivity(), getView());
		trySetProgressDialog(true);
		
		doOnChangePassword();
	}
	
	private void doOnChangePassword()
	{
		if (isServiceConnected())
		{
			trySetProgressDialog(true);
			
			final int userId = UserData.getInstance().getUserID();
			final String currentPassword = mEtCurrentPassword.getText().toString();
			final String newPassword = mEtNewPassword.getText().toString();
			
			getService().getController(LoginController.class).ChangePassword(new RequestChangePassword(userId, currentPassword, newPassword), new Response.Listener<ResponseChangePassword>()
			{
				@Override
				public void onResponse(final ResponseChangePassword response)
				{
					if (UserData.getInstance().isSavedPassword())
					{
						UserData.getInstance().setPassword(newPassword);
						UserData.getInstance().saveUserData();
					}
					else
					{
						UserData.getInstance().setTemporaryPassword(newPassword);
					}
					
					AlertDialog dialog = AppUtils.createSimpleMessageDialog(getActivity(), getString(R.string.password_changed_successfully), getString(R.string.close), new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(final DialogInterface dialog, final int which)
						{
							tryPopFragment();
						}
					}, null, null);
					dialog.setCancelable(false);
					dialog.show();
					trySetProgressDialog(false);
					
				}
			}, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError error)
				{
					doOnError(error);
					trySetProgressDialog(false);
				}
			});
		}
	}
	
	
	private void doOnError(final VolleyError iError)
	{
		AlertDialog dialog = AppUtils.createSimpleMessageDialog(getActivity(), getString(R.string.invalid_password), null, null, getString(R.string.close), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, final int which)
			{
				dialog.dismiss();
			}
		});
		dialog.setCancelable(false);
		dialog.show();
		//		ErrorsHandler.tryShowServiceErrorDialog(iError, getActivity());
	}
	
	
	//	private void goToLogin()
	//	{
	//		if (getActivity() != null && getActivity() instanceof MainActivity)
	//		{
	//			trySetProgressDialog(true);
	//
	//			UserData data = UserData.getInstance();
	//			String email = data.getEmail();
	//			String pass = data.getPassword();
	//
	//			((MainActivity) getActivity()).performRegisteredUserLogin(email, pass, true, new Response.Listener<ResponseUserLogin>()
	//			{
	//				@Override
	//				public void onResponse(final ResponseUserLogin response)
	//				{
	//					changeLocale(response);
	//				}
	//			}, this);
	//		}
	//	}
	//
	//	private void changeLocale(final ILoginResponse iResponse)
	//	{
	//		trySetProgressDialog(false);
	//
	//		if (iResponse != null && iResponse.getContent() != null)
	//		{
	//			UserData.getInstance().setEmailAndPassword(mEtEmail.getText().toString(), mEtMainTextPass.getText().toString());
	//			eLanguage userLanguageInServer = eLanguage.getLanguageByCodeOrDefault("", eLanguage.English);
	//
	//			if (iResponse.getContent() instanceof UserLogin)
	//			{
	//				UserData.getInstance().setUserId(((UserLogin) iResponse.getContent()).getUserID());
	//				UserData.getInstance().setUserObject(((UserLogin) iResponse.getContent()).getUserObject());
	//				userLanguageInServer = eLanguage.getLanguageByCodeOrDefault(((UserLogin) iResponse.getContent()).getUserObject().getLanguageID(), eLanguage.English);
	//			}
	//
	//			UserData.getInstance().saveUserData();
	//
	//			if (userLanguageInServer != UserData.getInstance().getLanguage())
	//			{
	//				UserData.getInstance().setLanguage(userLanguageInServer);
	//				UserData.getInstance().saveUserData();
	//				LocaleUtils.applyLocale(ElAlApplication.getInstance(), LocaleUtils.convertLanguageCodeToLocaleOrNull(userLanguageInServer.getLanguageCode()));
	//
	//				AlertDialog dialog = AppUtils.createSimpleMessageDialog(getActivity(), getString(R.string.restart_after_language_change), null, null, getString(R.string.close), new DialogInterface.OnClickListener()
	//				{
	//					@Override
	//					public void onClick(final DialogInterface dialog, final int which)
	//					{
	//						tryPopFragment();
	//					}
	//				});
	//				dialog.setCancelable(false);
	//				dialog.show();
	//			}
	//			else
	//			{
	//				goToHomeFragment();
	//			}
	//		}
	//	}
	//
	//	private void goToHomeFragment()
	//	{
	//		if (getActivity() != null && getActivity() instanceof MainActivity)
	//		{
	//			((MainActivity) getActivity()).loadHomeFragment();
	//		}
	//	}
	
	
	private boolean isFieldsValid()
	{
		boolean result = true;
		
		if (!isPasswordValid(mEtCurrentPassword.getText().toString()))
		{
			result = false;
			showInvalidPasswordError(mTilCurrentPassword, true);
		}
		if (!isPasswordValid(mEtNewPassword.getText().toString()))
		{
			result = false;
			showInvalidPasswordError(mTilNewPassword, true);
		}
		if (!isPasswordValid(mEtConfirmNewPassword.getText().toString()))
		{
			result = false;
			showInvalidPasswordError(mTilConfirmNewPassword, true);
		}
		
		return result;
	}
	
	
	private void showToolTip(View iV, String iText)
	{
		AppUtils.showToolTip(getContext(), iV, iText);
	}
	
	
	@Override
	public void onErrorResponse(final VolleyError error)
	{
		doOnError(error);
		trySetProgressDialog(false);
		AppUtils.printLog(Log.ERROR, TAG, error.getLocalizedMessage());
	}
	
	public boolean isNewPasswordsMatches()
	{
		boolean result = true;
		
		if (!mEtNewPassword.getText().toString().equals(mEtConfirmNewPassword.getText().toString()))
		{
			result = false;
			//			AppUtils.createSimpleMessageDialog(getActivity(), getString(R.string.password_not_match), null, null, getString(R.string.close), null).show();
			//			Toast.makeText(getActivity(), getString(R.string.password_not_match), Toast.LENGTH_SHORT).show();
			showPasswordError(mTilConfirmNewPassword, true, getString(R.string.password_not_match));
		}
		
		return result;
	}
}

