package ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tagmanager.DataLayer;

import global.AppData;
import global.IntentExtras;
import global.UserData;
import global.eActivityModule;
import il.co.ewave.elal.R;
import interfaces.IElalScreensNavigator;
import ui.fragments.details.DetailScreenFragment;
import ui.fragments.HomeFragment;
import ui.fragments.UserFamilyFragment;
import utils.errors.LocalError.eLocalError;
import utils.global.AppUtils;
import utils.managers.TabsBackStackManager;


/**
 * Created with care by Shahar Ben-Moshe on 15/12/16.
 */

public class MainActivity extends EWBaseUserActivity implements IElalScreensNavigator
{
	public static final int LOGIN_TYPE_REGISTERED_USER = 0;
	public static final int LOGIN_TYPE_MATMID_CLUB_MEMBER = 1;
	public static final String TAG = MainActivity.class.getSimpleName();
	
	private static final long EXIT_TIME = 2000;
	
	@Override
	protected void onServiceConnected()
	{
		initDrawer();
		handleInnerScreenLoad();
	}
	
	@Override
	protected void initScreen()
	{
		setContentView(R.layout.activity_main_layout);
		super.initScreen();
		
		pushCustomEvent(getString(R.string.sender_user_id), DataLayer.mapOf("userID", UserData.getInstance().getUserID()));
		
		//		setMainFragmentGui();
		

	}
	
	private void setCrashTestBtn()
	{
		Button crashButton = new Button(this);
		crashButton.setText("Crash!");
		crashButton.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View view)
			{
				Crashlytics.getInstance().crash(); // Force a crash
			}
		});
		addContentView(crashButton, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
	}
	
	@Override
	protected void setCurrentActivityModuleId()
	{
		AppData.getInstance().setModuleId(eActivityModule.Main.getActivityModuleId());
	}
	
	
//	public void goToLoginScreen()
//	{
//		UserData.getInstance().clearUserData();
//		//		getFragmentSwapper().clearFragments();
//		goToScreenById(eInAppNavigation.Login.getNavigationId(), null);
//	}
	
	public void loadHomeFragment()
	{
		if (getFragmentSwapper() != null && getFragmentSwapper().getTopFragment() != null && getFragmentSwapper().getTopFragment() instanceof HomeFragment)
		{
		}
		else
		{
			super.loadHomeFragment();
		}
	}
	
	
	//	@Override
	//	protected void handleLogoutResponse()
	//	{
	//		super.handleLogoutResponse();
	//		UserData.getInstance().clearUserData();
	//
	//		if (isServiceConnected())
	//		{
	//			getService().getController(LoginController.class).GetRepresentationByIp(new Response.Listener<ResponseGetRepresentationByIp>()
	//			{
	//				@Override
	//				public void onResponse(final ResponseGetRepresentationByIp iResponse)
	//				{
	//					if (iResponse != null && iResponse.getContent() != null && iResponse.getContent().size() > 0)
	//					{
	//						UserData.getInstance().setUserRepresentation(iResponse.getContent().get(0).getRepresentationId());
	//					}
	//
	//					startActivity(getIntent());
	//				}
	//			}, MainActivity.this);
	//		}
	//	}
	
	private void setMainFragmentGui()
	{
		loadHomeFragment();
	}
	
	private void handleInnerScreenLoad()
	{
		if (getFragmentSwapper().getTopFragment() instanceof UserFamilyFragment || getFragmentSwapper().getTopFragment() instanceof DetailScreenFragment) // if came from Scannig screen
		{
			AppUtils.printLog(Log.ERROR, TAG, eLocalError.NullPointerException.getErrorMessage());
			return;
		}

		Intent intent = getIntent();

		if (intent != null && intent.getExtras() != null && intent.getExtras().size() > 0)
		{
			Bundle bundle = intent.getExtras();

			if (bundle.getBoolean(IntentExtras.IS_FROM_PUSH_NOTIFICATION, false))
			{
				doOpenScreenFromPush(intent, bundle);
			}
			else if (bundle.containsKey(IntentExtras.IN_APP_NAVIGATION_ID))
			{
				/**TODO avishay 25/12/17 test it! else return the line openScreenFromBundle(bundle);**/
				goToScreenById(bundle.getInt(IntentExtras.IN_APP_NAVIGATION_ID),bundle);
//				openScreenFromBundle(bundle);
				/*****/
			}
			else if (  !TextUtils.isEmpty(bundle.getString(IntentExtras.URL))  ){
			
			{
				tryStartWebViewFragmentWithUrl(bundle.getString(IntentExtras.URL));
			}
		}
		}
		else if (getFragmentSwapper().isFragmentStackEmpty())
		{
			setMainFragmentGui();
		}
		
//		//clear the Navigation_ID and other params
//		intent.getExtras().clear();
		
		
//		if (!(getFragmentSwapper().getTopFragment() instanceof UserFamilyFragment) && !(getFragmentSwapper().getTopFragment() instanceof DetailScreenFragment)) // if came from Scannig screen
//		{
//			if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getBoolean(IntentExtras.IS_FROM_PUSH_NOTIFICATION, false))
//			{
//				String url = getIntent().getExtras().getString(IntentExtras.URL);
//				int inAppNavigationId = getIntent().getExtras().getInt(IntentExtras.IN_APP_NAVIGATION_ID);
//
//				if (inAppNavigationId > 0)
//				{
//					goToScreenById(inAppNavigationId, Bundle.EMPTY);
//
//					getIntent().replaceExtras(Bundle.EMPTY);
//					getIntent().setAction("");
//					getIntent().setData(null);
//					getIntent().setFlags(0);
//				}
//				else if (!TextUtils.isEmpty(url))
//				{
//					tryStartWebViewFragmentWithUrl(url);
//				}
//			}
//			//Avishay 14/09/17
//			else if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().size() > 0)
//			{
//				String url = !TextUtils.isEmpty(getIntent().getExtras().getString(IntentExtras.URL)) ? getIntent().getExtras().getString(IntentExtras.URL) : "";
//				int inAppNavigationId = getIntent().getExtras().getInt(IntentExtras.IN_APP_NAVIGATION_ID);
//
//				if (inAppNavigationId > 0)
//				{
//					goToScreenById(inAppNavigationId, null);
//				}
//				else if (!TextUtils.isEmpty(url))
//				{
//					tryStartWebViewFragmentWithUrl(url);
//				}
//			}
//			else if (getFragmentSwapper().isFragmentStackEmpty())
//			{
//				setMainFragmentGui();
//			}
//		}
	}
	
	private void openScreenFromBundle(final Bundle iBundle)
	{
		String url = TextUtils.isEmpty(iBundle.getString(IntentExtras.URL)) ? "" : iBundle.getString(IntentExtras.URL);
		int inAppNavigationId = iBundle.getInt(IntentExtras.IN_APP_NAVIGATION_ID);
		
		if (inAppNavigationId > 0)
		{
			goToScreenById(inAppNavigationId, null);
		}
		else if (!TextUtils.isEmpty(url))
		{
			tryStartWebViewFragmentWithUrl(url);
		}
	}
	
	private void doOpenScreenFromPush(final Intent iIntent, final Bundle iBundle)
	{
		String url = iBundle.getString(IntentExtras.URL);
		int inAppNavigationId = iBundle.getInt(IntentExtras.IN_APP_NAVIGATION_ID);
		
		if (inAppNavigationId > 0)
		{
			goToScreenById(inAppNavigationId, Bundle.EMPTY);
			
			//We need this method, because after that user clicked on push notification with internal link on status bar(for example SearchFlight) you are not successful go back, because
			// handleInnerScreenLoad() method every time is opening the same Intent with the same data
			AppUtils.releaseIntent(iIntent);
		}
		else if (!TextUtils.isEmpty(url))
		{
			tryStartWebViewFragmentWithUrl(url);
		}
	}
	
	@Override
	public void onBackPressed()
	{
		if (isDrawerOpen())
		{
			closeDrawer();
		}
		else if (!getFragmentSwapper().isTopFragmentOfTypeTabStackMaxOne())
		{
			((TabsBackStackManager.ITabsBackStackManagerHandler) getFragmentSwapper().getTopFragment()).popFragment();
		}
		else
		{
			if (getFragmentSwapper().isFragmentStackMaxOne())
			{
				//				Log.d("MainOnBackPressed", "Fragment stack max 1");
				if (mShouldExit && getFragmentSwapper().getTopFragment() instanceof HomeFragment)
				{
					getFragmentSwapper().clearFragments();
					super.onBackPressed();
				}
				else if (!mShouldExit && getFragmentSwapper().getTopFragment() instanceof HomeFragment)
				{
					Toast.makeText(getApplicationContext(), getString(R.string.press_again_to_exit), Toast.LENGTH_SHORT).show();
					startExitTimer();
				}
				else if (!(getFragmentSwapper().getTopFragment() instanceof HomeFragment) && !isTaskRoot())
				{
					finish();
				}
				else if ((getFragmentSwapper().getTopFragment() instanceof DetailScreenFragment) && UserData.getInstance().getUserObject() != null && UserData.getInstance()
				                                                                                                                                              .getUserObject()
				                                                                                                                                              .isMatmid() && UserData.getInstance()
				                                                                                                                                                                     .getUserObject()
				                                                                                                                                                                     .isMatmidFirstLogin())
				{
				}
				else
				{
					loadHomeFragment();
				}
			}
			else
			{
				getFragmentSwapper().popFragment();
			}
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
	}
	
}
