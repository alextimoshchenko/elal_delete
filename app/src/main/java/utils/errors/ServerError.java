package utils.errors;

import android.text.TextUtils;

import com.android.volley.VolleyError;

import global.ElAlApplication;
import il.co.ewave.elal.R;
import webServices.global.Response;

/**
 * Created by Shahar Ben-Moshe on 06/09/15.
 */
public class ServerError extends VolleyError
{
	public enum eServerError
	{
		Ok(0, R.string.test28),
		
		//Matmid Login Errors
		CredsNotFoundForUser(4/*5*/, R.string.incorrect_username_or_password_matmid),
		CredsNotFoundForUser2(5800, R.string.incorrect_username_or_password_matmid),
		IncorrectPasswordMatmid(3/*4*/, R.string.incorrect_username_or_password_matmid),
		IncorrectPasswordMatmid2(5707, R.string.incorrect_username_or_password_matmid),
		GeneralErrorMatmidLogin(5/*6*/, R.string.incorrect_username_or_password_matmid),
		GeneralErrorMatmidLogin2(555, R.string.incorrect_username_or_password_matmid),
		InvalidSystemCode(8/*9*/, R.string.incorrect_username_or_password_matmid),
		InvalidSystemCode2(7151, R.string.incorrect_username_or_password_matmid),
		IncorrectAnswer(10, R.string.incorrect_username_or_password_matmid),
		IncorrectAnswer2(558, R.string.incorrect_username_or_password_matmid),
		
		UserLocked(7/*8*/, R.string.test1),
		UserLocked2(557, R.string.test1),
		
		
		PasswordIsNotActive(11/*3*/, R.string.locked_user_matmid),
		PasswordIsNotActive2(5705/*557*/, R.string.locked_user_matmid),
		AttemptsNumberIsOver(1, R.string.locked_user_matmid),
		AttemptsNumberIsOver2(5700, R.string.locked_user_matmid),
		
		PasswordExpired(2, R.string.password_expired_matmid),
		PasswordExpired2(5704, R.string.password_expired_matmid),
		MustChangeTempPassword(5705, R.string.password_expired_matmid),
		MustChangeTempPassword2(556, R.string.password_expired_matmid),
		MustChangeTempPassword3(6/*7*/, R.string.password_expired_matmid),
		
		
		//global errors
	/*	LoginSucceeded(1, R.string.test2),*/
		Global(10, R.string.error_L2_no_response),
		/*RequestParseError(11, R.string.test4),
		ApplicationGlobalError(12, R.string.test1),*/
		MobileUserNotFound(13, R.string.error_S13_mobile_user_not_found),
		/*UserNotSaved(14, R.string.test5),
		PasswordNotFound(15, R.string.test6),
		DeviceNotFound(16, R.string.test7),
		SetDeviceError(17, R.string.test8),
		LoginFailed(18, R.string.test9),*/
		RequestDataNotAllowed(19, R.string.error_S19_request_data_not_allowed),
		/*	GUIDNotGenerated(20, R.string.test10),
			UrlNotExist(30, R.string.test11),
			EmailSent(31, R.string.test12),
			EmailNotSent(32, R.string.test13),
			UserSearchNotSet(33, R.string.test14),
			LogoutFailed(34, R.string.test15),*/
		EmailAlreadyExist(35, R.string.email_already_exist),
		PasswordAlreadyUsedBefore(39, R.string.password_allready_used_before),
		IncorrectPassword(46, R.string.incorrect_password),
		//db errors
		DBError(100, R.string.test16),
		
		//telemessage errors
		NoMessagesFound(200, R.string.test17),
		PNRNotSetToTM(201, R.string.test18),
		
		//lognet errors
		RetrievePNRError(300, R.string.test19),
		LognetPNRNotFound(301, R.string.error_S301_could_not_find_your_reservation_details),
		LognetPNRNotSaved(302, R.string.test20),
		PNRNotPairedToUser(303, R.string.error_S303_pnr_not_paired_to_user),
		LognetReservationIsNotSet(304, R.string.test22),
		LognetGetStatusInfoError(305, R.string.test23),
		RequestedPNRIsNotGroup(306, R.string.test24),
		TicketNumberNotFoundForGroupPNR(307, R.string.test25),
		NoFlightsFound(308, R.string.test26),
		NoTicketsFound(309, R.string.test27),
		
		NotActivePNR(310, R.string.error_S303_pnr_not_paired_to_user),
		
		MatmidFamilyNotSet(601, R.string.error_S12_member_number_or_name_not_much);
		
		// TODO: 13/12/16 Shahar set actual messages
		
		
		private int mErrorCode;
		private int mErrorMessageResourceId;
		
		eServerError(int iErrorCode, int iErrorMessageResourceId)
		{
			mErrorCode = iErrorCode;
			mErrorMessageResourceId = iErrorMessageResourceId;
		}
		
		public int getErrorCode()
		{
			return mErrorCode;
		}
		
		public String getErrorMessage()
		{
			String result = ElAlApplication.getInstance().getString(mErrorMessageResourceId);
			
			if (TextUtils.isEmpty(result))
			{
				result = "";
			}
			else
			{
				if (result.toLowerCase().contains("%s"))
				{
					result = String.format(result, getErrorCode());
				}
			}
			
			return result;
		}
		
		public static eServerError getServerErrorByErrorCodeOrNull(int iErrorCode)
		{
			eServerError result = null;
			
			for (eServerError serverError : eServerError.values())
			{
				if (serverError.getErrorCode() == iErrorCode)
				{
					result = serverError;
					break;
				}
			}
			
			return result;
		}
	}
	
	private int mErrorCode;
	private String mErrorMessage;
	
	public ServerError(Response iResponseObject)
	{
		if (iResponseObject != null)
		{
			eServerError eServerError = ServerError.eServerError.getServerErrorByErrorCodeOrNull(iResponseObject.getErrorCode());
			
			if (eServerError != null)
			{
				this.mErrorCode = eServerError.getErrorCode();
				this.mErrorMessage = eServerError.getErrorMessage();
			}
			else
			{
				this.mErrorCode = iResponseObject.getErrorCode();
				this.mErrorMessage = iResponseObject.getErrorMessage();
			}
		}
		else
		{
			this.mErrorCode = eServerError.Global.getErrorCode();
			this.mErrorMessage = eServerError.Global.getErrorMessage();
		}
	}
	
	public ServerError(eServerError iServerError)
	{
		this.mErrorCode = iServerError.getErrorCode();
		this.mErrorMessage = iServerError.getErrorMessage();
	}
	
	public ServerError(Exception e)
	{
		super(e);
		mErrorMessage = resolveExceptionMessage(e);
	}
	
	public ServerError(String iErrorMessage)
	{
		mErrorMessage = iErrorMessage;
	}
	
	private String resolveExceptionMessage(Exception e)
	{
		String result = ElAlApplication.getInstance().getString(R.string.error);
		
		if (e != null)
		{
			if (!TextUtils.isEmpty(e.getMessage()))
			{
				result = getMessage();
			}
			else if (e.getCause() != null && !TextUtils.isEmpty(e.getCause().getMessage()))
			{
				result = e.getCause().getMessage();
			}
		}
		
		return result;
	}
	
	@Override
	public String getMessage()
	{
		String message = "";
		
		if (mErrorCode > 0)
		{
			message += mErrorCode + ": ";
		}
		
		message += mErrorMessage;
		
		return message;
	}
	
	public int getErrorCode()
	{
		return mErrorCode;
	}
	
	public String getErrorMessage()
	{
		return mErrorMessage;
	}
	
	public boolean isErrorOfType(final eServerError iEServerError)
	{
		return iEServerError != null && mErrorCode == iEServerError.getErrorCode();
	}
}