package utils.global.inputFilters;

import android.text.InputFilter;
import android.text.Spanned;

import java.util.regex.Pattern;

/**
 * Created with care by Shahar Ben-Moshe on 24/01/17.
 */

public class HebrewInputFilter implements InputFilter
{
	private Pattern mPattern;
	
	public HebrewInputFilter()
	{
		mPattern = Pattern.compile("\\p{InHebrew}");
	}
	
	@Override
	public CharSequence filter(final CharSequence source, final int start, final int end, final Spanned dest, final int dstart, final int dend)
	{
		return mPattern.matcher(source).matches() ? null : "";
	}
}
