package webServices.responses.getAppData;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class GetAppDataContent
{
	@JsonProperty("Representaions")
	private ArrayList<Representation> mRepresentations;
	@JsonProperty("FamilyTypes")
	private ArrayList<FamilyType> mFamilyTypes;
	@JsonProperty("Languages")
	private ArrayList<Language> mLanguages;
	
	public ArrayList<Representation> getRepresentations()
	{
		return this.mRepresentations;
	}
	
	public ArrayList<FamilyType> getFamilyTypes()
	{
		return this.mFamilyTypes;
	}
	
	public ArrayList<Language> getLanguages()
	{
		return this.mLanguages;
	}
	
	public GetAppDataContent()
	{
	}
}
