package interfaces;

import com.android.volley.RequestQueue;

import services.EWService;

/**
 * Any class implementing this interface <B>Must not</B> override the default empty constructor in order for it to be used in the {@link EWService}.
 */
public interface IController
{
	/**
	 * Sets the requestQueue for the controller provided from the service.
	 *
	 * @param iRequestQueue
	 */
	void setVolleyRequestQueue(RequestQueue iRequestQueue);
}
