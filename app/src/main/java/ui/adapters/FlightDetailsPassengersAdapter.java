package ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import global.UserData;
import il.co.ewave.elal.R;
import utils.global.AppUtils;
import webServices.responses.responseGetUserActivePNRs.Passenger;

/**
 * Created with care by Shahar Ben-Moshe on 15/02/17.
 */

public class FlightDetailsPassengersAdapter extends RecyclerView.Adapter<FlightDetailsPassengersAdapter.ViewHolder>
{
	private ArrayList<Passenger> mPassengers;
	private String mPnr;
	
	public FlightDetailsPassengersAdapter(final ArrayList<Passenger> iPassengers, final String iPnr)
	{
		mPassengers = iPassengers;
		mPnr = iPnr;
	}
	
	@Override
	public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType)
	{
		return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.flight_details_passenger_item_cell_layout, parent, false));
	}
	
	@Override
	public void onBindViewHolder(final ViewHolder iViewHolder, final int iPosition)
	{
		final Passenger currentPassenger = mPassengers.get(iPosition);
		
		if (currentPassenger != null)
		{
//			iViewHolder.mTvPassengerName.setText(currentPassenger.getTitle() + " " + currentPassenger.getFirstName() + " " + currentPassenger.getLastName());
			iViewHolder.mTvPassengerName.setText(currentPassenger.getFirstName() + " " + currentPassenger.getLastName());
			iViewHolder.mTvPassengerSeat.setText(currentPassenger.getSeat());
			iViewHolder.mTvPassengerMeal.setText(currentPassenger.getMeal());
			
			
			//			iViewHolder.mTvTicketNumber.setText(currentPassenger.getTicketNumber());
//			iViewHolder.mTvReservationCode.setText(AppUtils.nullSafeCheckString(mPnr));
//
//			if (currentPassenger.isExpanded())
//			{
//				iViewHolder.mLlExpand.setVisibility(View.VISIBLE);
//				iViewHolder.mTvExpandArrow.setImageResource(R.mipmap.arrow_down_white);
//			}
//			else
//			{
//				iViewHolder.mLlExpand.setVisibility(View.GONE);
//				iViewHolder.mTvExpandArrow.setImageResource(/*AppUtils.isDefaultLocaleRTL() ? R.mipmap.arrow_left_white_full : */R.mipmap.arrow_right_white_full);
//			}
//
//			String matmidNumber = UserData.getInstance().getUserObject().getMatmidMemberID();
//			iViewHolder.mTvMatmidNumber.setText(AppUtils.nullSafeCheckString(matmidNumber));
//			iViewHolder.mLlMatmidNumber.setVisibility(TextUtils.isEmpty(matmidNumber) ? View.GONE : View.VISIBLE);
//
//			iViewHolder.mBtnExpand.setOnClickListener(new View.OnClickListener()
//			{
//				@Override
//				public void onClick(final View v)
//				{
//					currentPassenger.reverseExpand();
//					notifyItemChanged(iViewHolder.getAdapterPosition());
//				}
//			});
		}
	}
	
	@Override
	public int getItemCount()
	{
		return mPassengers == null ? 0 : mPassengers.size();
	}
	
	class ViewHolder extends RecyclerView.ViewHolder
	{
		TextView mTvPassengerName, mTvPassengerSeat, mTvPassengerMeal;
//		ImageView mTvExpandArrow;
//		Button mBtnExpand;
//		TextView mTvMatmidNumber;
//		LinearLayout mLlMatmidNumber;
//		TextView mTvTicketNumber;
//		TextView mTvReservationCode;
//		LinearLayout mLlExpand;
		
		
		ViewHolder(final View itemView)
		{
			super(itemView);
			
			mTvPassengerName = (TextView) itemView.findViewById(R.id.txv_passenger_name);
			mTvPassengerSeat = (TextView) itemView.findViewById(R.id.txv_passenger_seat);
			mTvPassengerMeal = (TextView) itemView.findViewById(R.id.txv_passenger_meal);
			
//			mTvExpandArrow = (ImageView) itemView.findViewById(R.id.iv_flightDetailsPassengerItem_ExpandArrow);
//			mBtnExpand = (Button) itemView.findViewById(R.id.btn_flightDetailsPassengerItem_Expand);
//			mTvMatmidNumber = (TextView) itemView.findViewById(R.id.tv_flightDetailsPassengerItem_MatmidNumber);
//			mLlMatmidNumber = (LinearLayout) itemView.findViewById(R.id.ll_flightDetailsPassengerItem_MatmidNumber);
//			mTvTicketNumber = (TextView) itemView.findViewById(R.id.tv_flightDetailsPassengerItem_TicketNumber);
//			mTvReservationCode = (TextView) itemView.findViewById(R.id.tv_flightDetailsPassengerItem_ReservationCode);
//			mLlExpand = (LinearLayout) itemView.findViewById(R.id.ll_flightDetailsPassengerItem_Expand);
		
		}
	}
}
