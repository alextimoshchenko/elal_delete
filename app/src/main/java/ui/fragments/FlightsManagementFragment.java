package ui.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;

import global.IntentExtras;
import global.UserData;
import il.co.ewave.elal.R;
import interfaces.IAddFlightListener;
import interfaces.IFlightsSwipeDeckListener;
import ui.activities.MyFlightsActivity;
import ui.adapters.MyFlightsSwipeDeckAdapter;
import ui.customWidgets.swipeDeck.SwipeDeck;
import ui.fragments.myFlights.MyFlightFragmentNew2;
import utils.errors.ErrorsHandler;
import utils.global.AppUtils;
import utils.global.myFlightsObjects.MyFlightsObject;
import webServices.controllers.MainController;
import webServices.global.RequestStringBuilder;
import webServices.requests.RequestGetMultiplePNRDestinations;
import webServices.requests.RequestGetUserActivePNRs;
import webServices.responses.ResponseGetPnrByNum;
import webServices.responses.ResponseGetUserActivePNRs;
import webServices.responses.getMultiplePNRDestinations.GetMultiplePNRDestinations;
import webServices.responses.getMultiplePNRDestinations.ResponseGetMultiplePNRDestinations;
import webServices.responses.responseGetUserActivePNRs.UserActivePnr;

/**
 * Created with care by Shahar Ben-Moshe on 09/01/17.
 */

public class FlightsManagementFragment extends EWBaseFragment implements IAddFlightListener, Response.Listener<ResponseGetUserActivePNRs>, Response.ErrorListener, IFlightsSwipeDeckListener
{
	private MyFlightsSwipeDeckAdapter mMyFlightsSwipeDeckAdapter;
	private SwipeDeck mSdFlightsDeck;
	private Button mBtnMyFlight;
	private TextView mBtnAddFlight;
	private TextView mTvBookHotel;
	private Button mBtnBookHotel;
	private TextView mTvRentCar;
	private Button mBtnRentCar;
	private Button mBtnMatmidClub;
	private ArrayList<UserActivePnr> mUserActivePnr;
	private int mCurrentItemPosition = -1;
	private TextView mTvTitle;
	private ImageView mBtnBackArrow;
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.fragment_flight_management_layout);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		getIntentExtras();
		initReference();
		setListeners();
		setGui();
	}
	
	private void getIntentExtras()
	{
		if (getArguments() != null)
		{
			mUserActivePnr = getArguments().getParcelableArrayList(IntentExtras.USER_ACTIVE_PNRS);
		}
		
		if (mUserActivePnr == null)
		{
			mUserActivePnr = new ArrayList<>();
		}
	}
	
	private void initReference()
	{
		mTvTitle = (TextView) getActivity().findViewById(R.id.tv_name_screen_title);
		mBtnBackArrow = (ImageView) getActivity().findViewById(R.id.iv_name_screen_back_arrow);
		mSdFlightsDeck = (SwipeDeck) getActivity().findViewById(R.id.sd_flightManagement_FlightsDeck);
		mBtnMyFlight = (Button) getActivity().findViewById(R.id.btn_flightManagement_ManageMyFlight);
		mBtnAddFlight = (TextView) getActivity().findViewById(R.id.btn_flightManagement_AddFlight);
		mTvBookHotel = (TextView) getActivity().findViewById(R.id.tv_flightManagement_BookHotel);
		mBtnBookHotel = (Button) getActivity().findViewById(R.id.btn_flightManagement_BookHotel);
		mTvRentCar = (TextView) getActivity().findViewById(R.id.tv_flightManagement_RentCar);
		mBtnRentCar = (Button) getActivity().findViewById(R.id.btn_flightManagement_RentCar);
		mBtnMatmidClub = (Button) getActivity().findViewById(R.id.btn_flightManagement_MatmidClub);
	}
	
	private void setListeners()
	{
		mBtnBackArrow.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				tryPopFragment();
			}
		});
		mBtnMyFlight.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				goToMyFlightFragment(mUserActivePnr.get(mCurrentItemPosition) );
			}
		});
		
		mBtnAddFlight.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				goToAddFlightFragment();
			}
		});
		
		mBtnBookHotel.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				onBookHotelClick();
			}
		});
		
		mBtnRentCar.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				onRentCarClick();
			}
		});
		
		mBtnMatmidClub.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				onMatmidClubClick();
			}
		});
	}
	
	private void setGui()
	{
		mTvTitle.setText(R.string.my_flights);
		
		if (mUserActivePnr.size() > 0)
		{
			setAdapter();
		}
		else
		{
			goToAddFlightFragment();
		}
	}
	
	private void onBookHotelClick()
	{
		if (mUserActivePnr != null && mUserActivePnr.size() > 0 && mUserActivePnr.size() > mCurrentItemPosition && mUserActivePnr.get(mCurrentItemPosition) != null
				/*&& mUserActivePnr.get(mCurrentItemPosition).getPNRFlights() .get(0) != null*/ && mUserActivePnr.get(mCurrentItemPosition).getLastDeparturePNRFlightOrNull() != null
			/*&& !TextUtils .isEmpty(mUserActivePnr.get(mCurrentItemPosition).getPNRFlights().get(0).getHotelLink()))*/ && !TextUtils.isEmpty(mUserActivePnr.get(mCurrentItemPosition).getLastDeparturePNRFlightOrNull().getHotelLink()))
		{
			//				tryGoToWebFragment(mUserActivePnr.get(mCurrentItemPosition).getPNRFlights().get(0).getHotelLink());
			tryGoToWebFragment(mUserActivePnr.get(mCurrentItemPosition).getLastDeparturePNRFlightOrNull().getHotelLink());
		}
		else
		{//TODO avishay 25/06/17 handle it.
			
		}
	}
	
	private void onRentCarClick()
	{
		if (mUserActivePnr != null && mUserActivePnr.size() > 0 && mUserActivePnr.size() > mCurrentItemPosition && mUserActivePnr.get(mCurrentItemPosition) != null
				/*&& mUserActivePnr.get(mCurrentItemPosition) .getPNRFlights() .get(0) != null*/ && mUserActivePnr.get(mCurrentItemPosition).getLastDeparturePNRFlightOrNull() != null && !TextUtils.isEmpty(mUserActivePnr.get(mCurrentItemPosition).getLastDeparturePNRFlightOrNull().getVehicleLink()))
		{
			//				tryGoToWebFragment(mUserActivePnr.get(mCurrentItemPosition).getPNRFlights().get(0).getVehicleLink());
			tryGoToWebFragment(mUserActivePnr.get(mCurrentItemPosition).getLastDeparturePNRFlightOrNull().getVehicleLink());
		}
		else
		{//TODO avishay 25/06/17 handle it.
			
		}
	}
	
	private void onMatmidClubClick()
	{
		tryGoToWebFragment(RequestStringBuilder.getMatmidUrlByLanguage());
	}
	
	private void setAdapter()
	{
		if (mMyFlightsSwipeDeckAdapter == null)
		{
			mMyFlightsSwipeDeckAdapter = new MyFlightsSwipeDeckAdapter(mUserActivePnr, this);
			mSdFlightsDeck.setAdapter(mMyFlightsSwipeDeckAdapter);
			mSdFlightsDeck.setCallback(new SwipeDeck.SwipeDeckCallback()
			{
				@Override
				public void cardSwipedLeft(final long stableId)
				{
					mCurrentItemPosition++;
					resetDeckIfNeeded();
				}
				
				@Override
				public void cardSwipedRight(final long stableId)
				{
					mCurrentItemPosition++;
					resetDeckIfNeeded();
				}
				
				@Override
				public void cardClick(final View card)
				{
				}
			});
		}
		else
		{
			mMyFlightsSwipeDeckAdapter.updateDataAndRefresh(mUserActivePnr);
		}
		
		mCurrentItemPosition = 0;
	}
	
	private void resetDeckIfNeeded()
	{
		if (mCurrentItemPosition == mMyFlightsSwipeDeckAdapter.getCount())
		{
			setAdapter();
		}
	}
	
	@Override
	public void addFlightListener(final boolean iDidAddFlight)
	{
		if (iDidAddFlight)
		{
			refreshUserActivePnrList();
		}
		else
		{
			if (mUserActivePnr.size() == 0)
			{
				tryPopFragment();
			}
		}
	}
	
	private void refreshUserActivePnrList()
	{
		if (isServiceConnected())
		{
			trySetProgressDialog(true);
			
			getService().getController(MainController.class).GetUserActivePNRs(new RequestGetUserActivePNRs(UserData.getInstance().getUserID()), this, this);
		}
	}
	
	@Override
	public void onResponse(final ResponseGetUserActivePNRs iResponse)
	{
		trySetProgressDialog(false);
		
		if (iResponse != null && iResponse.getContent() != null)
		{
			mUserActivePnr = iResponse.getContent();
			
			if (mUserActivePnr.size() > 0)
			{
				setAdapter();
			}
			else
			{
				showNoPnrsDialog();
			}
		}
	}
	
	private void showNoPnrsDialog()
	{
		AppUtils.createSimpleMessageDialog(getActivity(), getString(R.string.no_pnrs), getString(R.string.close), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, final int which)
			{
				dialog.dismiss();
				tryPopFragment();
			}
		}, null, null).show();
	}
	
	private void goToAddFlightFragment()
	{
		Bundle bundle = new Bundle();
		bundle.putSerializable(IntentExtras.ADD_FLIGHT_LISTENER, this);
		
		tryGoToFragmentByFragmentClass(AddNewFlightFragment.class, bundle, true);
	}
	
	@Override
	public void onErrorResponse(final VolleyError error)
	{
		ErrorsHandler.tryShowServiceErrorDialog(error, getActivity());
	}
	
	//	@Override
	//	public void cardClick(final int iPosition)
	//	{
	//		goToMyFlightFragment();
	//	}
	
	@Override
	public void cardClick()
	{
//		if (iUserActivePnr.getIsMultiple())
//		{
		UserActivePnr userActivePnr = MyFlightsObject.getInstance().getCurrentUserActivePnr();
		((MyFlightsActivity)getActivity()).GetPNRByNum(new Response.Listener<ResponseGetPnrByNum>()
		{
			@Override
			public void onResponse(final ResponseGetPnrByNum response)
			{
				handleResponseGetPnrByNum(response);
			}
		}, this, userActivePnr.getPnr(), null);
		
//		}
//		else
//		{
//			goToMyFlightFragment(iUserActivePnr);
//		}
	}
	
	private void handleResponseGetPnrByNum(ResponseGetPnrByNum response)
	{
		if (response.getContent() != null)
		{
			MyFlightsObject.getInstance().setCurrentUserActivePnr(response.getContent());
			getMultiplePNRDestinations(response.getContent());
		}
		else
		{
			getMultiplePNRDestinations(new UserActivePnr());
		}
	}
	
	private void goToMyFlightFragment(UserActivePnr iUserActivePnr)
	{
		tryGoToFragmentByFragmentClass(MyFlightFragmentNew2.class, MyFlightFragmentNew2.createBundleForFragment(iUserActivePnr, null), true);
//		tryGoToFragmentByFragmentClass(MyFlightFragmentNew2.class, MyFlightFragmentNew2.createBundleForFragment(mUserActivePnr.get(mCurrentItemPosition)), true);
	}
	
	private void goToMyFlightFragment(UserActivePnr iUserActivePnr, GetMultiplePNRDestinations iDestinationsList)
	{
		tryGoToFragmentByFragmentClass(MyFlightFragmentNew2.class, MyFlightFragmentNew2.createBundleForFragment(iUserActivePnr, iDestinationsList), true);
		//		tryGoToFragmentByFragmentClass(MyFlightFragmentNew2.class, MyFlightFragmentNew2.createBundleForFragment(mUserActivePnr.get(mCurrentItemPosition)), true);
	}
	
	private void getMultiplePNRDestinations(final UserActivePnr iUserActivePnr)
	{
		if (isServiceConnected())
		{
			trySetProgressDialog(true);
			
			getService().getController(MainController.class).GetMultiplePNRDestinations(new RequestGetMultiplePNRDestinations(UserData.getInstance().getUserID(), iUserActivePnr.getPnr()),
					new Response.Listener<ResponseGetMultiplePNRDestinations>()
			{
				@Override
				public void onResponse(ResponseGetMultiplePNRDestinations response)
				{
					trySetProgressDialog(false);
					
					if (response != null && response.getContent() != null)
					{
						GetMultiplePNRDestinations pnrDestinations = response.getContent();
						
						if (pnrDestinations != null && pnrDestinations.getmPNRDestinationsList() != null && pnrDestinations.getmPNRDestinationsList().size() > 0)
						{
							goToMyFlightFragment(iUserActivePnr, pnrDestinations);
						}
						else
						{
							goToMyFlightFragment(iUserActivePnr);
						}
					}
				}
			}, this);
		}
	}
}
