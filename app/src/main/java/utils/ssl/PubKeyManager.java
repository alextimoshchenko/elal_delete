package utils.ssl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.jetbrains.annotations.Contract;

import java.math.BigInteger;
import java.security.KeyStore;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

/**
 * Created with care by Alexey.T on 03/12/2017.
 * <p>
 * TODO: Add a class header comment!
 */

public final class PubKeyManager implements X509TrustManager
{
	private final static String TAG = PubKeyManager.class.getSimpleName();
	private List<String> mPublicKeyStore = new ArrayList<>();
	
	public PubKeyManager(String... iAssetNames)
	{
		setPublicStore(iAssetNames);
	}
	
	private void setPublicStore(final String[] iAssetNames)
	{
		Collections.addAll(mPublicKeyStore, iAssetNames);
	}
	
	@Override
	public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException
	{
		if (chain == null)
		{
			throw new IllegalArgumentException("checkServerTrusted: X509Certificate array is null");
		}
		
		if (!(chain.length > 0))
		{
			throw new IllegalArgumentException("checkServerTrusted: X509Certificate is empty");
		}
		
		// Perform customary SSL/TLS checks
		getTrustManagerFactory(chain, authType);
		
		List<String> keysFromServer = getServerKeys(chain);
		
		//We need to match here 2 keys
		int count = getCountOfMatches(keysFromServer);
		
		// fail if expected public key is different from our public key
		if (count != SslUtils.COUNT_OF_CER_MATCHES)
		{
			throw new CertificateException("Certificate not trusted");
		}
	}
	
	private int getCountOfMatches(final List<String> iKeysFromServer)
	{
		int count = 0;
		
		for (String localKey : mPublicKeyStore)
		{
			for (String serverKey : iKeysFromServer)
			{
				if (localKey.equalsIgnoreCase(serverKey))
				{
					count++;
					break;
				}
			}
		}
		return count;
	}
	
	private void getTrustManagerFactory(final X509Certificate[] chain, final String authType) throws CertificateException
	{
		TrustManagerFactory tmf;
		
		try
		{
			tmf = TrustManagerFactory.getInstance(SslUtils.TRUST_MANAGER_X509);
			tmf.init((KeyStore) null);
			
			for (TrustManager trustManager : tmf.getTrustManagers())
			{
				((X509TrustManager) trustManager).checkServerTrusted(chain, authType);
			}
		}
		catch (Exception e)
		{
			throw new CertificateException(e.toString());
		}
	}
	
	@NonNull
	private List<String> getServerKeys(final X509Certificate[] chain)
	{
		List<String> keysFromServer = new ArrayList<>();
		
		if (chain.length > 0)
		{
			PublicKey pubkeyCA = chain[0].getPublicKey();
			keysFromServer.add(new BigInteger(1 /* positive */, pubkeyCA.getEncoded()).toString(16));
			
			if (chain.length > 1)
			{
				PublicKey pubkeyRoot = chain[1].getPublicKey();
				keysFromServer.add(new BigInteger(1 /* positive */, pubkeyRoot.getEncoded()).toString(16));
			}
		}
		return keysFromServer;
	}
	
	@Override
	public void checkClientTrusted(X509Certificate[] xcs, String string)
	{
		// throw new
		// UnsupportedOperationException("checkClientTrusted: Not supported yet.");
	}
	
	@Override
	@Nullable
	@Contract(pure = true)
	public X509Certificate[] getAcceptedIssuers()
	{
		// throw new
		// UnsupportedOperationException("getAcceptedIssuers: Not supported yet.");
		return null;
	}
}