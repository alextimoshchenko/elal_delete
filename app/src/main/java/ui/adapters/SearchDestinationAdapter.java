package ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import global.UserData;
import global.eLanguage;
import il.co.ewave.elal.R;
import interfaces.ISearchElement;

/**
 * Created with care by Alexey.T on 10/07/2017.
 */

public class SearchDestinationAdapter<T extends ISearchElement> extends RecyclerView.Adapter<SearchDestinationAdapter.ViewHolder>
{
	private List<T> mSearchElements = new ArrayList<>();
	private IDestinationTypeListener mListener;
	private boolean isEnglishLocale = UserData.getInstance().getLanguage() == eLanguage.English;
	
	public interface IDestinationTypeListener
	{
		void onItemClicked(ISearchElement iElement);
	}
	
	public SearchDestinationAdapter(IDestinationTypeListener iListener)
	{
		mListener = iListener;
	}
	
	@Override
	public SearchDestinationAdapter.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType)
	{
		return new ViewHolder(LayoutInflater.from(parent.getContext())
											.inflate(R.layout.destination_search_layout, parent, false));
	}
	
	@Override
	public void onBindViewHolder(final SearchDestinationAdapter.ViewHolder holder, final int position)
	{
		final ISearchElement element = mSearchElements.get(position);
		String searchText;
		
		if (isEnglishLocale)
		{
			searchText = element.getElementNameEn();
		}
		else
		{
			searchText = element.getElementNameHe();
		}
		
		holder.mTvDestinationLayout.setText(searchText);
		
		holder.mTvDestinationLayout.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				mListener.onItemClicked(element);
			}
		});
		
	}
	
	public void updateData(List<T> iSearchElements)
	{
		mSearchElements.clear();
		
		if (iSearchElements != null && !iSearchElements.isEmpty())
		{
			mSearchElements.addAll(iSearchElements);
		}
	}
	
	@Override
	public int getItemCount()
	{
		return mSearchElements.isEmpty() ? 0 : mSearchElements.size();
	}
	
	class ViewHolder extends RecyclerView.ViewHolder
	{
		private TextView mTvDestinationLayout;
		
		ViewHolder(final View iV)
		{
			super(iV);
			
			this.mTvDestinationLayout = (TextView) iV.findViewById(R.id.tv_destination_layout);
		}
	}
}
