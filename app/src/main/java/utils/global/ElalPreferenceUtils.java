package utils.global;

/**
 * Created with care by Shahar Ben-Moshe on 15/12/16.
 */

public class ElalPreferenceUtils extends APreferencesUtils
{
	public static final String BOOT_COUNT = "bootCount";
	public static final String SECRET_COUNT = "secretCount";
	public static final String LANGUAGE = "language";
	public static final String REPRESENTATION = "representation";
	public static final String EMAIL = "email";
	public static final String DESCRIPTION = "description";
	public static final String FIRST_NAME_EN = "FirstNameEn";
	public static final String DATE = "Date";
	public static final String PASSPORT_NUMBER = "PassportNumber";
	public static final String EXPIRATION_DATE = "ExpirationDate";
	public static final String FAMILY_NAME_HE = "FamilyNameHe";
	public static final String FIRST_NAME_HE = "FirstNameHe";
	public static final String FAMILY_NAME_EN = "FamilyNameEn";
	public static final String IS_SAW_TUTORIAL = "isSawTutorial";
	public static final String PASSWORD = "password";
	public static final String FLIGHTS_INFO = "flightInfo";
	public static final String DEALS_INFO = "dealsInfo";
	public static final String REMIND_ME_DETAIL_SCREEN = "remindMeDetailScreen";
	public static final String REMIND_FAMILY_SCREEN = "remindFamilyScreen";
	public static final String PHONE_NUMBER = "phoneNumber";
	public static final String COUNTRY_CODE = "countryCode";
	public static final String IS_FINISH_TUTORIAL = "isFinishTutorial";
	public static final String SMS_SESSION = "SMSession";
	public static final String SESSION_COUNTER = "session_counter";
	public static final String SESSION_UPDATE_DATE = "session_update";
	public static final String ACTIVE_PNR = "active_pnr";
	public static final String CA_TOKEN = "ca_token";
//	public static final String CA_BASE_TOKEN = "ca_base_token";
	public static final String USER_ID = "userId";
	
	private ElalPreferenceUtils()
	{
		super();
	}
}
