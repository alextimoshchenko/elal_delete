package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.firebase.iid.FirebaseInstanceId;

import global.ElAlApplication;
import global.GlobalVariables;
import global.UserData;
import utils.global.AppUtils;

/**
 * Created with care by Shahar Ben-Moshe on 25/05/17.
 */

public class RequestMatmidLogin
{
	@JsonProperty("MemberID")
	private String mMatmidNumber;
//	@JsonProperty("Password")
//	private String mMatmidPassword;
	@JsonProperty("LanguageCode")
	private String mLanguageCode = UserData.getInstance().getLanguage().getLanguageCode();
	@JsonProperty("UDID")
	private String mUdid = UserData.getInstance().getUdid();
	@JsonProperty("OSID")
	private int mOSID = GlobalVariables.OS_ANDROID;
	@JsonProperty("Token")
	private String mToken = FirebaseInstanceId.getInstance().getToken();
	@JsonProperty("DeviceName")
	private String mDeviceName = "Android" + " " + AppUtils.getDeviceName();
	@JsonProperty("OSVersion")
	private String mOSVersion = AppUtils.getDeviceOsVersion();
	@JsonProperty("AppVersion")
	private String mAppVersion = String.valueOf(AppUtils.getApplicationVersion(ElAlApplication.getInstance()));
	@JsonProperty("RepresentationId")
	private int mRepresentationId = UserData.getInstance().getRepresentationId();
	
	public RequestMatmidLogin(final String iMatmidNumber, final String iMatmidPassword)
	{
		mMatmidNumber = iMatmidNumber;
//		mMatmidPassword = iMatmidPassword;
	}
}
