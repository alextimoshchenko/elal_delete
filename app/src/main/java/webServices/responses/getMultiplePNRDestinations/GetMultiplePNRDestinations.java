package webServices.responses.getMultiplePNRDestinations;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by erline.katz on 13/08/2017.
 */

public class GetMultiplePNRDestinations implements Parcelable
{
	@JsonProperty("PNRDestinationsList")
	private ArrayList<DestinationListItem> mPNRDestinationsList;
	
	public GetMultiplePNRDestinations()
	{
	}
	
	public GetMultiplePNRDestinations(final ArrayList<DestinationListItem> iDestinations)
	{
		mPNRDestinationsList = iDestinations;
	}
	
	
	protected GetMultiplePNRDestinations(Parcel in)
	{
		mPNRDestinationsList = in.createTypedArrayList(DestinationListItem.CREATOR);
	}
	
	public static final Creator<GetMultiplePNRDestinations> CREATOR = new Creator<GetMultiplePNRDestinations>()
	{
		@Override
		public GetMultiplePNRDestinations createFromParcel(Parcel in)
		{
			return new GetMultiplePNRDestinations(in);
		}
		
		@Override
		public GetMultiplePNRDestinations[] newArray(int size)
		{
			return new GetMultiplePNRDestinations[size];
		}
	};
	
	public ArrayList<DestinationListItem> getmPNRDestinationsList()
	{
		return mPNRDestinationsList;
	}
	
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeTypedList(this.mPNRDestinationsList);
	}
	
	
}