package ui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import global.AppData;
import global.UserData;
import global.eActivityModule;
import il.co.ewave.elal.R;
import ui.fragments.notification.NotificationListFragment;
import utils.errors.ErrorsHandler;
import utils.global.AppUtils;
import utils.global.notificationObjects.NotificationObject;
import webServices.controllers.NotificationController;
import webServices.requests.RequestDeleteUserPushMessage;
import webServices.requests.RequestGetPushNotifications;
import webServices.requests.RequestSetPushAsRead;
import webServices.responses.deleteUserPushMessage.NotificationInstance;
import webServices.responses.deleteUserPushMessage.ResponseDeleteUserPushMessage;
import webServices.responses.getPushNotifications.ResponseGetPushNotifications;
import webServices.responses.setPushAsRead.ResponseSetPushAsRead;

/**
 * Created with care by Alexey.T on 25/07/2017.
 */
public class NotificationActivity extends EWBaseUserActivity /*implements*/ /*IElalScreensNavigator, *//*Response.ErrorListener*/
{
	private static final String TAG = NotificationActivity.class.getSimpleName();
	
	@Override
	protected void initScreen()
	{
		setContentView(R.layout.activity_notification_layout);
		super.initScreen();
		NotificationObject.getInstance().cleanAllCurrentData();
	}
	
	@Override
	protected void setCurrentActivityModuleId()
	{
		AppData.getInstance().setModuleId(eActivityModule.Notifications.getActivityModuleId());
	}
	
	@Override
	protected void onServiceConnected()
	{
		initDrawer();
		
		if (NotificationObject.getInstance().getResponseGetPushNotifications() == null)
		{
			getPushNotifications();
		}
	}
	
	public void loadSearchNotificationFragment()
	{
		goToFragmentByFragmentClass(NotificationListFragment.class, Bundle.EMPTY, false);
	}
	
	public void deleteUserPushMessage(NotificationInstance iNotificationInstance, final Response.Listener<ResponseDeleteUserPushMessage> iListener, final Response.ErrorListener iErrorListener)
	{
		if (isServiceConnected())
		{
			setProgressDialog(true);
			
			UserData userData = UserData.getInstance();
			
			getService().getController(NotificationController.class)
			            .DeleteUserPushMessage(new RequestDeleteUserPushMessage(userData.getUserID(), iNotificationInstance.getCampaignID(), iNotificationInstance.isTelemessage() ?
			                                                                                                                                 iNotificationInstance.getMessageText() :
			                                                                                                                                 null), new Response.Listener<ResponseDeleteUserPushMessage>()
			            {
				            @Override
				            public void onResponse(final ResponseDeleteUserPushMessage response)
				            {
					            if (iListener != null)
					            {
						            iListener.onResponse(response);
					            }
					
					            setProgressDialog(false);
				            }
			            }, new Response.ErrorListener()
			            {
				            @Override
				            public void onErrorResponse(final VolleyError error)
				            {
					            if (iErrorListener != null)
					            {
						            iErrorListener.onErrorResponse(error);
					            }
					
					            setProgressDialog(false);
				            }
			            });
		}
	}
	
	public void setPushAsRead(NotificationInstance iNotificationInstance, final Response.Listener<ResponseSetPushAsRead> iListener, final Response.ErrorListener iErrorListener)
	{
		if (isServiceConnected())
		{
			setProgressDialog(true);
			UserData userData = UserData.getInstance();
			
			getService().getController(NotificationController.class)
			            .SetPushAsRead(new RequestSetPushAsRead(userData.getUserID(), iNotificationInstance.getCampaignID(), iNotificationInstance.isTelemessage() ?
			                                                                                                                 iNotificationInstance.getMessageText() :
			                                                                                                                 null), new Response.Listener<ResponseSetPushAsRead>()
			            {
				            @Override
				            public void onResponse(final ResponseSetPushAsRead response)
				            {
					            if (iListener != null)
					            {
						            iListener.onResponse(response);
					            }
					
					            setProgressDialog(false);
				            }
			            }, new Response.ErrorListener()
			            {
				            @Override
				            public void onErrorResponse(final VolleyError error)
				            {
					            if (iErrorListener != null)
					            {
						            iErrorListener.onErrorResponse(error);
					            }
					
					            setProgressDialog(false);
				            }
			            });
		}
	}
	
	public void getPushNotifications()
	{
		if (isServiceConnected())
		{
			setProgressDialog(true);
			UserData userData = UserData.getInstance();
			
			getService().getController(NotificationController.class).GetPushNotifications(new RequestGetPushNotifications(userData.getUserID()), new Response.Listener<ResponseGetPushNotifications>()
			{
				@Override
				public void onResponse(final ResponseGetPushNotifications iResponse)
				{
					if (iResponse != null && iResponse.getContent() != null && !iResponse.getContent().getNotificationInstanceList().isEmpty())
					{
						NotificationObject notificationObject = NotificationObject.getInstance();
						notificationObject.setResponseGetPushNotifications(iResponse);
						notificationObject.setCurrentUserMessageList(iResponse.getContent().getNotificationInstanceList());
					}
					
					loadSearchNotificationFragment();
					setProgressDialog(false);
				}
			}, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError iError)
				{
					
					AlertDialog dialog = AppUtils.createSimpleMessageDialog(NotificationActivity.this, getString(R.string.error_L2_no_response), getString(R.string.close), new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(final DialogInterface dialog, final int which)
						{
							dialog.dismiss();
							finish();
						}
					}, null, null);
					dialog.setCancelable(false);
					dialog.show();
					setProgressDialog(false);
					
//					ErrorsHandler.tryShowServiceErrorDialog(iError, NotificationActivity.this);
//					setProgressDialog(false);
//					loadSearchNotificationFragment();
				}
			});
		}
	}
	
/*	@Override
	public void onErrorResponse(final VolleyError iError)
	{
		ErrorsHandler.tryShowServiceErrorDialog(iError, NotificationActivity.this);
		setProgressDialog(false);
	}*/
	
	@Override
	public void onBackPressed()
	{
		if (isDrawerOpen())
		{
			closeDrawer();
		}
		else
		{
			super.onBackPressed();
		}
	}
}
