package webServices.responses.getSearchGlobalData;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class GetSearchGlobalDataContent
{
	@JsonProperty("UserFamily")
	private ArrayList<GetSearchGlobalDataContentUserFamily> mUserFamily;
	@JsonProperty("FlightClasses")
	private ArrayList<GetSearchGlobalDataContentFlightClasses> mFlightClasses;
	
	public ArrayList<GetSearchGlobalDataContentUserFamily> getUserFamily()
	{
		return this.mUserFamily;
	}
	
	public ArrayList<GetSearchGlobalDataContentFlightClasses> getFlightClasses()
	{
		return this.mFlightClasses;
	}
	
	public GetSearchGlobalDataContent()
	{
	}
}
