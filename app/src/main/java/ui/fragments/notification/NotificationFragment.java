package ui.fragments.notification;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.tagmanager.DataLayer;

import global.eInAppNavigation;
import il.co.ewave.elal.R;
import ui.activities.EWBaseActivity;
import ui.activities.EWBaseDrawerActivity;
import ui.activities.NotificationActivity;
import ui.fragments.EWBaseFragment;
import utils.errors.ErrorsHandler;
import utils.global.ParseUtils;
import utils.global.notificationObjects.NotificationObject;
import webServices.responses.deleteUserPushMessage.NotificationInstance;
import webServices.responses.deleteUserPushMessage.NotificationInstance.eKindOfLink;
import webServices.responses.deleteUserPushMessage.NotificationInstance.eOpenType;
import webServices.responses.deleteUserPushMessage.ResponseDeleteUserPushMessage;
import webServices.responses.deleteUserPushMessage.ResponseDeleteUserPushMessageContent;

/**
 * Created with care by Alexey.T on 25/07/2017.
 */
public class NotificationFragment extends EWBaseFragment implements Response.ErrorListener
{
	private LinearLayout mLlScreenName;
	private ImageView mIvBackArrow, mIvMessageHeaderIcon, mIvDeleteButton;
	private TextView mTvNotificationDate, mTvMessage;
	private NotificationInstance mNotificationMessage;
	private eKindOfLink mKindOfLink = eKindOfLink.NULL;
	private String mNotificationUrl;
	private eOpenType mTypeToOpen = eOpenType.WEB_VIEW;
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.notification_fragment_layout);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		initReference();
		getMessageData();
		setListeners();
		setGui();
		pushScreenOpenEvent("Notification Details");
		
	}
	
	private void getMessageData()
	{
		mNotificationMessage = NotificationObject.getInstance().getCurrentNotificationMessage();
		mKindOfLink = mNotificationMessage.getKindOfLink();
		mNotificationUrl = mNotificationMessage.getNotificationUrl();
		mTypeToOpen = mNotificationMessage.getOpenType();
	}
	
	private void initReference()
	{
		Activity act = getActivity();
		
		//header
		View header = act.findViewById(R.id.ff_notification_fragment_header);
		mLlScreenName = (LinearLayout) header.findViewById(R.id.ll_header_screen_name);
		mIvBackArrow = (ImageView) header.findViewById(R.id.iv_name_screen_back_arrow);
		
		//Message Header
		View messageHeader = act.findViewById(R.id.cl_notification_fragment_message_header);
		mIvMessageHeaderIcon = (ImageView) messageHeader.findViewById(R.id.iv_header_notification_icon);
		mTvNotificationDate = (TextView) messageHeader.findViewById(R.id.tv_header_notification_notification_date);
		mIvDeleteButton = (ImageView) messageHeader.findViewById(R.id.iv_header_notification_delete);
		
		//Message Text
		mTvMessage = (TextView) act.findViewById(R.id.tv_notification_fragment_message);
	}
	
	private void setListeners()
	{
		mIvBackArrow.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnBackArrowClick();
			}
		});
		
		mIvDeleteButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnDeleteClick();
			}
		});
		
		mTvMessage.setOnClickListener(mNotificationMessageListener);
	}
	
	private View.OnClickListener mNotificationMessageListener = new View.OnClickListener()
	{
		@Override
		public void onClick(final View v)
		{
			if (TextUtils.isEmpty(mNotificationUrl))
			{
				return;
			}
			
			switch (mKindOfLink)
			{
				case EXTERNAL:
				{
					switch (mTypeToOpen)
					{
						case BROWSER:
						{
							((EWBaseDrawerActivity) getActivity()).goToBrowserByUrl(mNotificationUrl);
							break;
						}
						case WEB_VIEW:
						default:
						{
							doOnExternalWebViewClick();
							break;
						}
						
						
					}
					break;
				}
				case INTERNAL:
				{
					doOnInternalMessageClick();
					break;
				}
			}
		}
	};
	
	private void doOnExternalWebViewClick()
	{
		tryGoToWebFragment(mNotificationUrl);
	}
	
	private void doOnInternalMessageClick()
	{
		int inAppNavigationId = ParseUtils.tryParseStringToIntegerOrDefault(mNotificationUrl, -1);
		
		if (inAppNavigationId > -1)
		{
			if (inAppNavigationId == eInAppNavigation.Notifications.getNavigationId())
			{
				tryPopFragment();
			}
			else
			{
				((NotificationActivity) getActivity()).goToScreenById(inAppNavigationId, null);
			}
		}
	}
	
	private void doOnDeleteClick()
	{
		NotificationInstance notificationMessage = NotificationObject.getInstance().getCurrentNotificationMessage();
		
		if (isServiceConnected() && (isResumed() || isVisible()) && notificationMessage != null)
		{
			int pushType = mNotificationMessage.getPushType();
			String mesName = mNotificationMessage.getMessageName();
			//event 114
			((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "My Massages", "Action", "Delit Massage - " + pushType, "Label", mesName));
			
			((NotificationActivity) getActivity()).deleteUserPushMessage(notificationMessage, new Response.Listener<ResponseDeleteUserPushMessage>()
			{
				@Override
				public void onResponse(final ResponseDeleteUserPushMessage response)
				{
					if (response != null && response.getContent() != null)
					{
						handleDeleteResponse(response.getContent());
					}
				}
			}, this);
		}
	}
	
	private void handleDeleteResponse(ResponseDeleteUserPushMessageContent iResponse)
	{
		updateMessageNumber();
		NotificationObject.getInstance().setCurrentUserMessageList(iResponse.getNotificationInstanceList());
		tryPopFragment();
	}
	
	private void updateMessageNumber()
	{
		((EWBaseActivity) getActivity()).getAndUpdateMessageNumber();
	}
	
	private void doOnBackArrowClick()
	{
		tryPopFragment();
	}
	
	private void setGui()
	{
		//header screen name
		mLlScreenName.setVisibility(View.INVISIBLE);
		
		//Message header icon
		Drawable messageHeaderIcon;
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
		{
			//			messageHeaderIcon = getResources().getDrawable(R.drawable.msg2, getActivity().getTheme());
			messageHeaderIcon = getResources().getDrawable(mNotificationMessage.isTelemessage() ? R.drawable.msg1 : R.drawable.msg2, getActivity().getTheme());
		}
		else
		{
			//			messageHeaderIcon = getResources().getDrawable(R.drawable.msg2);
			messageHeaderIcon = getResources().getDrawable(mNotificationMessage.isTelemessage() ? R.drawable.msg1 : R.drawable.msg2);
		}
		
		mIvMessageHeaderIcon.setImageDrawable(messageHeaderIcon);
		
		//notification date
		mTvNotificationDate.setText(mNotificationMessage.getNotificationDate());
		
		//notification message
		mTvMessage.setText(mNotificationMessage.getMessageText());
	}
	
	@Override
	public void onErrorResponse(final VolleyError error)
	{
		trySetProgressDialog(false);
		ErrorsHandler.tryShowServiceErrorDialog(error, getActivity());
	}
}
