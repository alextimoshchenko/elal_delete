package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.firebase.iid.FirebaseInstanceId;

import global.ElAlApplication;
import global.GlobalVariables;
import global.UserData;
import utils.global.AppUtils;

/**
 * Created by Aviv.Kabeli on 15/12/2016.
 */
class RequestDevice
{
	@JsonProperty("UDID")
	private String mUdid = UserData.getInstance().getUdid();
	@JsonProperty("OSID")
	private int mOSID = GlobalVariables.OS_ANDROID;
	@JsonProperty("Token")
	private String mToken = FirebaseInstanceId.getInstance().getToken();
	@JsonProperty("DeviceName")
	private String mDeviceName = AppUtils.getDeviceName();
	@JsonProperty("OSVersion")
	private String mOSVersion = AppUtils.getDeviceOsVersion();
	@JsonProperty("AppVersion")
	private String mAppVersion = String.valueOf(AppUtils.getApplicationVersion(ElAlApplication.getInstance()));
	@JsonProperty("PushEnabled")
	private boolean mPushEnabled = true;
//	@JsonProperty("PushFlightsInfo")
//	private boolean mPushFlightsInfo = true;
//	@JsonProperty("PushOffersInfo")
//	private boolean mPushOffersInfo = true;
//	@JsonProperty("RememberMe")
//	private boolean mRememberMe;
	
	public RequestDevice()
	{
	}
	
	public void setRememberMe(final boolean iRememberMe)
	{
		
//		mRememberMe = iRememberMe;
	}
}
