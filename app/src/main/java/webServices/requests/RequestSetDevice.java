package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.GlobalVariables;
import global.UserData;

public class RequestSetDevice
{
	@JsonProperty("AppVersion")
	private String mAppVersion;
	@JsonProperty("OSVersion")
	private String mOSVersion;
//	@JsonProperty("OSID")
//	private int OSID = GlobalVariables.OS_ANDROID;
	@JsonProperty("Token")
	private String mToken;
	@JsonProperty("UDID")
	private String UDID = UserData.getInstance().getUdid();
	@JsonProperty("DeviceName")
	private String mDeviceName;
	@JsonProperty("PushEnabled")
	private boolean mPushEnabled;
	
	public RequestSetDevice(String iAppVersion, String iOSVersion, String iToken, String iDeviceName, boolean iPushEnabled)
	{
		mAppVersion = iAppVersion;
		this.mOSVersion = iOSVersion;
		mToken = iToken;
		mDeviceName = iDeviceName;
		mPushEnabled = iPushEnabled;
	}
	
	public RequestSetDevice()
	{
	}
}
