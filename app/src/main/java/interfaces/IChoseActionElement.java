package interfaces;

import android.content.Context;

public interface IChoseActionElement
{
	/** I need this interface method, because in case when app in Hebrew some fields are presenting in English, because of this I need to pass `getContext()` inside instead of
	 * `getApplicationContext()`*/
	
	 String getElementName(Context iContext);
	
	String getElementName();
	
	int getElementId();
}
