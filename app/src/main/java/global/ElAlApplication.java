package global;

import android.Manifest;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.support.multidex.MultiDex;
import android.support.v4.content.ContextCompat;

import net.danlew.android.joda.JodaTimeAndroid;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;

import il.co.ewave.elal.R;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import utils.global.ActivityLifecycleHandler;
import utils.global.LocaleUtils;
import utils.permissions.PermissionManager;

/**
 * Created with care by Shahar Ben-Moshe on 13/12/16.
 */

public class ElAlApplication extends Application
{
	private static final String TAG = ElAlApplication.class.getSimpleName();
	
	private static ElAlApplication mInstance;
//	private int mModuleId;
	private static boolean mAppWasInBackground = false;
	
	@Override
	public void onCreate()
	{
		super.onCreate();
		
		JodaTimeAndroid.init(this);
		
		mInstance = this;
		
		CookieHandler.setDefault(new CookieManager());
		CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
		
		setUserDefinedLanguage();
		setActivityLifecycleHandler();
		initCalligraphy();
		Realm.init(this);
		RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().build();
		Realm.setDefaultConfiguration(realmConfiguration);
		
		
		//		Thread.setDefaultUncaughtExceptionHandler (new Thread.UncaughtExceptionHandler()
		//		{
		//			@Override
		//			public void uncaughtException (Thread thread, Throwable e)
		//			{
		//				handleUncaughtException (thread, e);
		//			}
		//		});
	}
	
	//	public void handleUncaughtException(Thread thread, Throwable e)
//	{
//		e.printStackTrace(); // not all Android versions will print the stack trace automatically
//
//		Intent intent = new Intent(this, SplashActivity.class);
//		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // required when starting from Application
//		startActivity(intent);
//
//		System.exit(1); // kill off the crashed app
//	}
	
	@Override
	protected void attachBaseContext(Context iContext)
	{
		super.attachBaseContext(iContext);
		MultiDex.install(this);
	}
	
	@Override
	public void onTrimMemory(final int level)
	{
		super.onTrimMemory(level);
		if (level == ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN)
		{
			// Get called every-time when application went to background.
			mAppWasInBackground = true;
		}
	}
	
	public static boolean isAppWasInBackground()
	{
		return mAppWasInBackground;
	}
	
	public static void setAppWasInBackground(boolean mAppWasInBackground)
	{
		ElAlApplication.mAppWasInBackground = mAppWasInBackground;
	}
	
	public static ElAlApplication getInstance()
	{
		return mInstance;
	}
	
	public void setUserDefinedLanguage()
	{
		LocaleUtils.applyLocaleFromLanguage(this, UserData.getInstance().getLanguage()/*!= null? UserData.getInstance().getLanguage(): eLanguage.English*/);
	}
	
	private void setActivityLifecycleHandler()
	{
		registerActivityLifecycleCallbacks(new ActivityLifecycleHandler());
	}
	
	private void initCalligraphy()
	{
		CalligraphyConfig.initDefault(new CalligraphyConfig.Builder().setDefaultFontPath(getString(R.string.font_type_light)).setFontAttrId(R.attr.fontPath).build());
	}
	
//	public int getModuleId()
//	{
//		return mModuleId;
//	}
//
//	public void setModuleId(final int iModuleId)
//	{
//		mModuleId = iModuleId;
//	}
}
