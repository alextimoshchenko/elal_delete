package ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import il.co.ewave.elal.R;
import interfaces.IFlightsSwipeDeckListener;
import utils.global.myFlightsObjects.MyFlightsObject;
import webServices.responses.responseGetUserActivePNRs.UserActivePnr;

public class MyFlightsSwipeDeckAdapter extends BaseAdapter
{
	private ArrayList<UserActivePnr> mUserActivePnrs = new ArrayList<>();
	private final IFlightsSwipeDeckListener mFlightsSwipeDeckListener;
	
	public MyFlightsSwipeDeckAdapter(final ArrayList<UserActivePnr> iUserActivePnrs, IFlightsSwipeDeckListener iFlightsSwipeDeckListener)
	{
		mFlightsSwipeDeckListener = iFlightsSwipeDeckListener;
		updateDataAndRefresh(iUserActivePnrs);
	}
	
	@Override
	public int getCount()
	{
		return mUserActivePnrs == null ? 0 : mUserActivePnrs.size();
	}
	
	@Override
	public Object getItem(final int iPosition)
	{
		return mUserActivePnrs.get(iPosition);
	}
	
	@Override
	public long getItemId(final int iPosition)
	{
		return iPosition;
	}
	
	@Override
	public View getView(final int iPosition, final View iConvertView, final ViewGroup iParent)
	{
		//Current UserActivePnr
		final UserActivePnr userActivePnr = (UserActivePnr) getItem(iPosition);
		
		CardViewHolder vH;
		View view;
		
		if (iConvertView == null)
		{
			view = LayoutInflater.from(iParent.getContext()).inflate(R.layout.my_flight_card_deck_item, iParent, false);
			vH = new CardViewHolder(view.findViewById(R.id.cv_my_flight_front_card_deck_item));
			view.setTag(vH);
		}
		else
		{
			view = iConvertView;
		}
		
		vH = (CardViewHolder) view.getTag();
		
		//Manage my flight button listener
		Button btnMenageMyFlight = (Button) view.findViewById(R.id.btn_my_flight_manage_my_flight);
		
		btnMenageMyFlight.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				if (mFlightsSwipeDeckListener != null)
				{
					MyFlightsObject.getInstance().setCurrentUserActivePnr(userActivePnr);
					mFlightsSwipeDeckListener.cardClick();
				}
			}
		});
		
		vH.mTvTimeTitle.setText(userActivePnr.getTimeTitleByFlightStatus());
		
		vH.mTvOrigin.setText(userActivePnr.getOriginDestination());
		
		vH.mTvDestination.setText(userActivePnr.getDepartureDestination());
		
		vH.mTvDepartureDateTime.setText(userActivePnr.getDepartureTime());
		
		vH.mTvArrivalDateTime.setText(userActivePnr.getArrivalTime());
		
		vH.mTvVia.setText(userActivePnr.getNumberOfStopsOrVia());
		
		return view;
	}
	
	public void updateDataAndRefresh(final ArrayList<UserActivePnr> iUserActivePnrs)
	{
		if (iUserActivePnrs != null)
		{
			mUserActivePnrs.clear();
			mUserActivePnrs.addAll(iUserActivePnrs);
			notifyDataSetChanged();
		}
	}
	
	private class CardViewHolder
	{
		TextView mTvTimeTitle, mTvOrigin, mTvDestination, mTvDepartureDateTime, mTvArrivalDateTime, mTvVia;
		
		CardViewHolder(final View iV)
		{
			mTvTimeTitle = (TextView) iV.findViewById(R.id.tv_flight_card_deck_item_time_title);
			mTvOrigin = (TextView) iV.findViewById(R.id.tv_flight_card_deck_item_origin);
			mTvVia = (TextView) iV.findViewById(R.id.tv_flight_card_deck_item_via);
			mTvDestination = (TextView) iV.findViewById(R.id.tv_flight_card_deck_item_destination);
			mTvArrivalDateTime = (TextView) iV.findViewById(R.id.tv_flight_card_deck_item_arrival_date_time);
			mTvDepartureDateTime = (TextView) iV.findViewById(R.id.tv_flight_card_deck_item_departure_date_time);
		}
	}
}

