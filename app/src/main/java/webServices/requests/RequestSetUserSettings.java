package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.UserData;

public class RequestSetUserSettings
{
	@JsonProperty("UserID")
	private int mUserID;
	@JsonProperty("RepresentationID")
	private int mRepresentationID;
	@JsonProperty("LanguageCode")
	private String mLanguageCode = UserData.getInstance().getLanguage().getLanguageCode();
	@JsonProperty("PushFlightsInfo")
	private boolean mPushFlightsInfo;
	@JsonProperty("PushOffersInfo")
	private boolean mPushOffersInfo;
	
	public RequestSetUserSettings(final int iUserID, final int iRepresentationID, final String iLanguageCode, final boolean iPushFlightsInfo, final boolean iPushOffersInfo)
	{
		mUserID = iUserID;
		mRepresentationID = iRepresentationID;
		mLanguageCode = iLanguageCode;
		mPushFlightsInfo = iPushFlightsInfo;
		mPushOffersInfo = iPushOffersInfo;
	}
}
