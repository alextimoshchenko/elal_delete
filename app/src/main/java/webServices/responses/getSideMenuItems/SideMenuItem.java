package webServices.responses.getSideMenuItems;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.eInAppNavigation;

public class SideMenuItem
{
	@JsonProperty("SideMenuCategoryID")
	private int mSideMenuCategoryID;
	@JsonProperty("SideMenuName")
	private String mSideMenuName;
	@JsonProperty("IsActive")
	private boolean mIsActive;
	@JsonProperty("ItemUrl")
	private String mItemUrl;
	@JsonProperty("SortOrder")
	private int mSortOrder;
	@JsonProperty("Icon")
	private String mIcon;
	@JsonProperty("SideMenuItemID")
	private int mSideMenuItemID;
	@JsonProperty("InApp")
	private boolean mIsInApp;
	@JsonProperty("IsNewWindow")
	private boolean mIsNewWindow;
	
	@JsonProperty("IsDeleted")
	private boolean mIsDeleted;
	@JsonProperty("MatmidInApp")
	private boolean mMatmidInApp;
	@JsonProperty("MatmidIsNewWindow")
	private boolean mMatmidIsNewWindow;
	@JsonProperty("MatmidItemUrl")
	private String mMatmidItemUrl;
	@JsonProperty("MatmidStatusID")
	private int mMatmidStatusID;
	
	public SideMenuItem()
	{
	}
	
	public SideMenuItem(eInAppNavigation iEInAppNavigation, String iItemName)
	{
		mSideMenuCategoryID = -1;
		mSideMenuName = iItemName;
		mIsActive = true;
		mItemUrl = String.valueOf(iEInAppNavigation.getNavigationId());
		mSortOrder = -1;
		mIcon = "";
		mSideMenuItemID = -1;
		mIsInApp = true;
	}
	
	private String mExternalUrl = "";
	
	public SideMenuItem(eInAppNavigation iEInAppNavigation, String iItemName, String iExternalUrl)
	{
		this(iEInAppNavigation, iItemName);
		mExternalUrl = iExternalUrl;
	}
	
	public String getExternalUrl()
	{
		return mExternalUrl;
	}
	
	public boolean isNewWindow()
	{
		return mIsNewWindow;
	}
	
	public boolean isDeleted()
	{
		return mIsDeleted;
	}
	
	public boolean isMatmidInApp()
	{
		return mMatmidInApp;
	}
	
	public boolean isMatmidIsNewWindow()
	{
		return mMatmidIsNewWindow;
	}
	
	public String getMatmidItemUrl()
	{
		return mMatmidItemUrl;
	}
	
	public int getMatmidStatusID()
	{
		return mMatmidStatusID;
	}
	
	public int getSideMenuCategoryID()
	{
		return mSideMenuCategoryID;
	}
	
	public String getSideMenuName()
	{
		return mSideMenuName;
	}
	
	public boolean isActive()
	{
		return mIsActive;
	}
	
	public String getItemUrl()
	{
		return mItemUrl;
	}
	
	public int getSortOrder()
	{
		return mSortOrder;
	}
	
	public String getIcon()
	{
		return mIcon;
	}
	
	public int getSideMenuItemID()
	{
		return mSideMenuItemID;
	}
	
	public boolean isInApp()
	{
		return mIsInApp;
	}
}
