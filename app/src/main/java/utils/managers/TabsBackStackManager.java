package utils.managers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import il.co.ewave.elal.R;
import ui.fragments.EWBaseFragment;

/**
 * Created with care by Shahar Ben-Moshe on 29/03/16.
 */
public class TabsBackStackManager implements TabHost.OnTabChangeListener
{
	public static final String CURRENT_TAB = "currentTab";
	private static final long FINISH_DELETE_DELAY = 1000;
	private final String TAG = TabsBackStackManager.class.getSimpleName();
	
	private Activity mActivity;
	private FrameLayout mFlContainer;
	private TabHost mTabHost;
	private ArrayList<TabItem> mTabItems;
	private HashMap<String, Stack<EWBaseFragment>> mTabsStacks;
	private String mCurrentTab;
	private FragmentManager mFragmentManager;
	private int mDefaultTabPosition = -1;
	private IOnTabChangeListener mOnTabChangeListener;
	private FrameLayout mFlTopContainer;
	private Context mContext;
	
	public TabsBackStackManager(final @NonNull Activity iActivity, final @NonNull FrameLayout iFlContainer, final @NonNull TabHost iTabHost, final @NonNull ArrayList<TabItem> iTabItems,
	                            final @NonNull FragmentManager iFragmentManager, final int iDefaultTabPosition, final IOnTabChangeListener iOnTabChangeListener, FrameLayout iFlTopContainer,
	                            Context iContext)
	{
		mActivity = iActivity;
		mFlContainer = iFlContainer;
		mTabHost = iTabHost;
		mTabItems = iTabItems;
		mFragmentManager = iFragmentManager;
		mDefaultTabPosition = iDefaultTabPosition;
		mOnTabChangeListener = iOnTabChangeListener;
		mCurrentTab = null;
		mFlTopContainer = iFlTopContainer;
		mContext = iContext;
		
		initTabs();
		setDefaultTab();
	}
	
	public void updateTabsBackStackManager(final @NonNull Activity iActivity, final @NonNull FrameLayout iFlContainer, final @NonNull TabHost iTabHost, final @NonNull ArrayList<TabItem> iTabItems,
	                                       final @NonNull FragmentManager iFragmentManager, final int iDefaultTabPosition, final IOnTabChangeListener iOnTabChangeListener, FrameLayout iFlTopContainer,
	                                       Context iContext, boolean iIsMultiple)
	{
		mActivity = iActivity;
		mFlContainer = iFlContainer;
		mTabHost = iTabHost;
		mTabItems = iTabItems;
		mFragmentManager = iFragmentManager;
		mDefaultTabPosition = iDefaultTabPosition;
		mOnTabChangeListener = iOnTabChangeListener;
		mCurrentTab = null;
		mFlTopContainer = iFlTopContainer;
		mContext = iContext;
		
		initTabs();
		if (iIsMultiple)
		{
			setDefaultTab();
		}
	}
	
	private void initTabs()
	{
		if (mTabsStacks != null)
		{
			mTabsStacks.clear();
		}
		
		mTabsStacks = new HashMap<>();
		
		if (mTabHost != null && mTabHost.getTabWidget() != null && mTabHost.getTabWidget().getTabCount() > 0)
		{
			mTabHost.clearAllTabs();
		}
		
		assert mTabHost != null;
		mTabHost.setup();
		
		TabHost.TabContentFactory tabContentFactory = new TabHost.TabContentFactory()
		{
			@Override
			public View createTabContent(String tag)
			{
				return mFlContainer;
			}
		};
		
		mFlContainer.setFocusable(false);
		mFlContainer.setFocusableInTouchMode(false);
		
		mTabHost.setOnTabChangedListener(this);
		mTabHost.getTabWidget().setShowDividers(TabWidget.SHOW_DIVIDER_MIDDLE);
		mTabHost.getTabWidget().setDividerDrawable(R.color.white);
		
		for (int i = 0 ; i < mTabItems.size() ; i++)
		{
			TabItem tabItem = mTabItems.get(i);
			mTabsStacks.put(tabItem.getTabName(), new Stack<EWBaseFragment>());
			mTabHost.addTab(mTabHost.newTabSpec(tabItem.getTabName()).setContent(tabContentFactory).setIndicator(tabItem.getTabView(mActivity)));
			final int finalI = i;
			
			mTabHost.getTabWidget().getChildTabViewAt(i).setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(final View v)
				{
					onTabClick(mTabItems.get(finalI));
					setCurrentTab(finalI);
				}
			});
		}
	}
	
	private void setDefaultTab()
	{
		if (mTabHost != null && mTabHost.getTabWidget() != null && mTabHost.getTabWidget().getChildCount() > 0)
		{
			if (mTabHost.getTabWidget().getChildAt(mDefaultTabPosition) != null)
			{
				mTabHost.getTabWidget().getChildAt(mDefaultTabPosition).setFocusable(false);
			}
			
			new Handler(mActivity.getMainLooper()).post(new Runnable()
			{
				@Override
				public void run()
				{
					if (mTabItems != null && mDefaultTabPosition <= 0 && mDefaultTabPosition >= mTabItems.size())
					{
						mDefaultTabPosition = 0;
					}
					
					if (mCurrentTab != null)
					{
						setCurrentTab(mDefaultTabPosition);
					}
					
					View focusPoint = new View(mActivity);
					focusPoint.setLayoutParams(new ViewGroup.LayoutParams(0, 0));
					mFlTopContainer.addView(focusPoint, 0);
					
					focusPoint.setFocusable(true);
					focusPoint.setFocusableInTouchMode(true);
					focusPoint.requestFocus();
					//					focusPoint.clearFocus();
					//					mTabHost.removeView(focusPoint);
				}
			});
		}
	}
	
	private void notifyFinishInitIfNeeded()
	{
		if (mDefaultTabPosition != -1 && mOnTabChangeListener != null && mTabItems.get(mDefaultTabPosition).getTabName().equals(mCurrentTab))
		{
			mDefaultTabPosition = -1;
			
			new Handler(mActivity.getMainLooper()).postDelayed(new Runnable()
			{
				@Override
				public void run()
				{
					mOnTabChangeListener.onFinishInit();
				}
			}, FINISH_DELETE_DELAY);
		}
	}
	
	private void onTabClick(final TabItem iTabItem)
	{
		if (mOnTabChangeListener != null && !TextUtils.isEmpty(mCurrentTab) && iTabItem != null && !TextUtils.isEmpty(iTabItem.getTabName()) && iTabItem.getTabName().equals(mCurrentTab))
		{
			mOnTabChangeListener.onTabReselected(mCurrentTab);
		}
	}
	
	@Override
	public void onTabChanged(String iTabId)
	{
		String unselectedTab = mCurrentTab;
		mCurrentTab = iTabId;
		
		mTabHost.clearFocus();
		
		if (mTabsStacks != null && mTabsStacks.get(iTabId) != null)
		{
			if (mTabsStacks.get(iTabId).size() == 0)
			{
				EWBaseFragment fragmentInstance = getTabItemBuyName(iTabId).getFragmentInstance();
				
				try
				{
					if (fragmentInstance != null)
					{
						pushFragment(iTabId, fragmentInstance, true, true);
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				pushFragment(iTabId, mTabsStacks.get(iTabId).lastElement(), true, false);
			}
		}
		
		if (mOnTabChangeListener != null)
		{
			mOnTabChangeListener.onTabSelected(mCurrentTab);
			
			if (!TextUtils.isEmpty(unselectedTab))
			{
				mOnTabChangeListener.onTabUnselected(unselectedTab);
			}
		}
		
		notifyFinishInitIfNeeded();
	}
	
	private void setCurrentTab(int iTabPosition)
	{
		mFlContainer.setFocusable(false);
		mTabHost.setFocusable(false);
		mTabHost.setCurrentTab(iTabPosition);
	}
	
	public void setCurrentTab(String iTabName)
	{
		setCurrentTab(getTabItemBuyName(iTabName));
	}
	
	private void setCurrentTab(TabItem iTabItem)
	{
		if (iTabItem != null)
		{
			setCurrentTab(mTabItems.indexOf(iTabItem));
		}
	}
	
	// TODO: 22/06/16 Shahar check possible crash on rapid ui.fragments swapping
	public void pushFragment(String iTag, EWBaseFragment iFragment, boolean iShouldAnimate, boolean iShouldAdd)
	{
		String finalTag = iTag.equals(CURRENT_TAB) ? mCurrentTab : iTag;
		
		if (iShouldAdd)
		{
			mTabsStacks.get(finalTag).push(iFragment);
		}
		
		FragmentTransaction ft = mFragmentManager.beginTransaction();
		if (iShouldAnimate)
		{
			ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
		}
		ft.replace(mFlContainer.getId(), iFragment);
		ft.commitAllowingStateLoss();
		
		if (mOnTabChangeListener != null)
		{
			if (iShouldAdd)
			{
				mOnTabChangeListener.onTabStackAdd(finalTag);
			}
		}
	}
	
	public void popFragment()
	{
	  /*
	   *    Select the second last fragment in current tab's stack..
       *    which will be shown after the fragment transaction given below
       */
		if (!isCurrentTabBackStackEmpty())
		{
			Fragment fragment = mTabsStacks.get(mCurrentTab).elementAt(mTabsStacks.get(mCurrentTab).size() - 2);
			
      /*pop current fragment from stack.. */
//			mFlContainer.removeView(fragment.getView());
//			((ViewGroup) fragment.getView().getParent()).removeView(fragment.getView());
			mTabsStacks.get(mCurrentTab).pop();

      /* We have the target fragment in hand.. Just show it.. Show a standard navigation animation*/
			FragmentTransaction fTransaction = mFragmentManager.beginTransaction();
			fTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
			fTransaction.replace(mFlContainer.getId(), fragment);
			fTransaction.commit();
			
			if (mOnTabChangeListener != null)
			{
				mOnTabChangeListener.onTabStackPop(mCurrentTab);
			}
		}
	}
	
	public boolean onBackPressed()
	{
		boolean result = false;
		
		Fragment fragment = mTabsStacks.get(mCurrentTab).lastElement();
		
		if (fragment != null)
		{
			result = ((EWBaseFragment) fragment).onBackPressed();
		}
		
		return result;
	}
	
	public void onTabRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults)
	{
		Fragment fragment = mTabsStacks.get(mCurrentTab).lastElement();
		
		if (fragment != null)
		{
			fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
		}
	}
	
	public boolean isCurrentTabBackStackEmpty()
	{
		return mTabsStacks.get(mCurrentTab).size() == 1;
	}
	
	public void onActivityResult(int iRequestCode, int iResultCode, Intent iData)
	{
		if (mTabsStacks.get(mCurrentTab).size() > 0)
		{
			mTabsStacks.get(mCurrentTab).lastElement().onActivityResult(iRequestCode, iResultCode, iData);
		}
	}
	
	private TabItem getTabItemBuyName(String iTabName)
	{
		TabItem result = null;
		
		if (!TextUtils.isEmpty(iTabName))
		{
			for (TabItem tabItem : mTabItems)
			{
				if (tabItem != null && tabItem.getTabName().equals(iTabName))
				{
					result = tabItem;
					break;
				}
			}
		}
		
		return result;
	}
	
	
	public static class TabItem
	{
		private String mTabName;
		private Class<? extends EWBaseFragment> mFragmentClass;
		private Bundle mBundle;
		private int mTabIconResourceId;
		private boolean mTabIsLast = false;
		
		public TabItem(String iTabName, Class<? extends EWBaseFragment> iFragmentClass, final Bundle iBundle, int iTabIconResourceId, boolean iTabIsLast)
		{
			mTabName = iTabName;
			mFragmentClass = iFragmentClass;
			mBundle = iBundle;
			mTabIconResourceId = iTabIconResourceId;
			mTabIsLast = iTabIsLast;
		}
		
		public TabItem(String iTabName, Class<? extends EWBaseFragment> iFragmentClass, final Bundle iBundle, int iTabIconResourceId)
		{
			mTabName = iTabName;
			mFragmentClass = iFragmentClass;
			mBundle = iBundle;
			mTabIconResourceId = iTabIconResourceId;
			mTabIsLast = false;
		}
		
		public String getTabName()
		{
			return mTabName;
		}
		
		public Class<? extends EWBaseFragment> getFragmentClass()
		{
			return mFragmentClass;
		}
		
		public EWBaseFragment getFragmentInstance()
		{
			EWBaseFragment result = null;
			
			try
			{
				result = mFragmentClass.newInstance();
				
				if (result != null && mBundle != null)
				{
					result.setArguments(mBundle);
				}
			}
			catch (Exception iE)
			{
				iE.printStackTrace();
			}
			
			return result;
		}
		
		private View getTabView(Context iContext)
		{
			View view = LayoutInflater.from(iContext).inflate(R.layout.custom_tab_layout, null, false);
			
			TextView tv = (TextView) view.findViewById(R.id.tv_bottomTabLayout_Title);
			tv.setText(mTabName);
			
			ImageView img = (ImageView) view.findViewById(R.id.img_bottomTabLayout_Icon);
			img.setImageResource(mTabIconResourceId > 0 ? mTabIconResourceId : android.R.color.transparent);
			
			View tabSeparator = view.findViewById(R.id.Tab_Separator);
			tabSeparator.setVisibility(mTabIsLast ? View.GONE : View.VISIBLE);
			
			return view;
		}
	}
	
	public String getCurrentFragmentTitle()
	{
		String result = "";
		
		if (mTabsStacks != null && mTabsStacks.get(mCurrentTab) != null && mTabsStacks.get(mCurrentTab).lastElement() != null)
		{
			result = mTabsStacks.get(mCurrentTab).lastElement().getFragmentTitle();
		}
		
		return result;
	}
	
	public interface ITabsBackStackManagerHandler
	{
		void pushFragmentToCurrentTab(EWBaseFragment iFragment, boolean iShouldAnimate, boolean iShouldAdd);
		
		void popFragment();
		
		boolean isFirstInTab();
		
		void onTabRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults);
	}
	
	public interface IOnTabChangeListener
	{
		void onFinishInit();
		
		void onTabSelected(final String iTabName);
		
		void onTabUnselected(final String iTabName);
		
		void onTabReselected(final String iTabName);
		
		void onTabStackAdd(final String iTabName);
		
		void onTabStackPop(final String iTabName);
	}
}
