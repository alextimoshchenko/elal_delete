package utils.global.enums;

import android.content.Context;
import android.text.TextUtils;

import global.ElAlApplication;
import il.co.ewave.elal.R;

/**
 * Created with care by Alexey.T on 10/07/2017.
 * <p>
 * TODO: Add a class header comment!
 */
public enum eTabStatement
{
	FLIGHT_TICKET("1", R.string.flight_ticket),
	AWARD_TICKET("2", R.string.award_ticket),
	POINTS_AND_MONEY("3", R.string.points_and_money);
	
	private String  mStatementCode = "-1";
	private int mResTabName = -1;
	
	public String getStatementCode()
	{
		return mStatementCode;
	}
	
	eTabStatement(String iStatementCode, int iResTabName)
	{
		mStatementCode = iStatementCode;
		mResTabName = iResTabName;
	}
	
//	public String getTabName()
//	{
//		String result = ElAlApplication.getInstance().getString(mResTabName);
//
//		if (TextUtils.isEmpty(result))
//		{
//			result = "";
//		}
//
//		return result;
//	}
	
	public String getTabName(Context iContext)
	{
		String result = iContext.getString(mResTabName);
		
		if (TextUtils.isEmpty(result))
		{
			result = "";
		}
		
		return result;
	}
	
	public boolean isFlightTicket()
	{
		return this == FLIGHT_TICKET;
	}
	
	public boolean isAwardTicket()
	{
		return this == AWARD_TICKET;
	}
	
	public boolean isPointsAndMoneyTicket()
	{
		return this == POINTS_AND_MONEY;
	}
}
