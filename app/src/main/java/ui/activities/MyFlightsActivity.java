package ui.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;

import global.AppData;
import global.ElAlApplication;
import global.IntentExtras;
import global.UserData;
import global.eActivityModule;
import global.eInAppNavigation;
import il.co.ewave.elal.R;
import interfaces.IAddOrEditCheckListItemListener;
import interfaces.IElalScreensNavigator;
import interfaces.IUpdateDataListener;
import ui.fragments.AddNewFlightFragment;
import ui.fragments.EWBaseFragment;
import ui.fragments.ToDoFragment;
import ui.fragments.myFlights.FlightDetailsFragment;
import ui.fragments.myFlights.MyFlightFragmentNew2;
import ui.fragments.myFlights.MyFlightsFragment;
import ui.fragments.myFlights.OrderDetailsFragment;
import utils.errors.ErrorsHandler;
import utils.errors.LocalError;
import utils.errors.ServerError;
import utils.global.AppUtils;
import utils.global.myFlightsObjects.MyFlightsObject;
import utils.managers.TabsBackStackManager;
import webServices.controllers.MyFlightsController;
import webServices.requests.RequestGetMultiplePNRDestinations;
import webServices.requests.RequestGetUserActivePNRs;
import webServices.requests.RequestPnrByNum;
import webServices.responses.ResponseGetPnrByNum;
import webServices.responses.ResponseGetUserActivePNRs;
import webServices.responses.getMultiplePNRDestinations.GetMultiplePNRDestinations;
import webServices.responses.getMultiplePNRDestinations.ResponseGetMultiplePNRDestinations;
import webServices.responses.responseGetUserActivePNRs.UserActivePnr;

/**
 * Created with care by Alexey.T on 13/08/2017.
 * <p>
 * This activity responsible for user flights, quantity of flights and more
 */
public class MyFlightsActivity extends EWBaseUserActivity implements IElalScreensNavigator, Response.ErrorListener, IUpdateDataListener, IAddOrEditCheckListItemListener
{
	private final String TAG = MyFlightsActivity.class.getSimpleName();
	public String mCurrentFlightPnr;
	private int mFragmentsCount = 0; //for certain use, because we don't have access to EWBaseFragment stack
	private boolean mShouldUpdateCheckList = false;
	
	@Override
	protected void initScreen()
	{
		setContentView(R.layout.activity_my_flights_layout);
		super.initScreen();
		
		MyFlightsObject.getInstance().clearObject();
		getIntentExtras();
	}
	
	@Override
	protected void setCurrentActivityModuleId()
	{
		AppData.getInstance().setModuleId(eActivityModule.MyFlights.getActivityModuleId());
	}
	
	private void getIntentExtras()
	{
		if (getIntent() != null && !TextUtils.isEmpty(getIntent().getStringExtra(IntentExtras.USER_ACTIVE_PNR)))
		{
			mCurrentFlightPnr = getIntent().getStringExtra(IntentExtras.USER_ACTIVE_PNR);
		}
	}
	
	public String getCurrentActiveFlightPnr()
	{
		return mCurrentFlightPnr;
	}
	
	@Override
	protected void onServiceConnected()
	{
		initDrawer();
		handleInnerScreenLoad();
	}
	
	private void handleInnerScreenLoad()
	{
		int inAppNavigationId = getIntent().getExtras().getInt(IntentExtras.IN_APP_NAVIGATION_ID);
		
		if (inAppNavigationId > 0 && inAppNavigationId == /*99*/eInAppNavigation.AddNewFlightFragment.getNavigationId())
		{
			loadMyFlightsAddFragment(false);
		}
		else
		{
			getUserActivePNRs();
		}
	}
	
	public boolean isShouldUpdateCheckList()
	{
		return mShouldUpdateCheckList;
	}
	
	public void setmShouldUpdateCheckList(boolean mShouldUpdateCheckList)
	{
		this.mShouldUpdateCheckList = mShouldUpdateCheckList;
	}
	
	private void getUserActivePNRs()
	{
		MyFlightsObject myFlightsObject = MyFlightsObject.getInstance();
		
		if (myFlightsObject.getUserActivePnrList().isEmpty())
		{
			GetUserActivePNRs();
		}
	}
	
	public void GetUserActivePNRs()
	{
		if (isServiceConnected())
		{
			setProgressDialog(true);
			
			int userId = UserData.getInstance().getUserID();
			
			getService().getController(MyFlightsController.class).GetUserActivePNRs(new RequestGetUserActivePNRs(userId), new Response.Listener<ResponseGetUserActivePNRs>()
			{
				@Override
				public void onResponse(final ResponseGetUserActivePNRs iResponse)
				{
					setProgressDialog(false);
					
					if (iResponse != null && iResponse.getContent() != null)
					{
						handleResponseGetUserActivePNRs(iResponse.getContent());
					}
				}
			}, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError iError)
				{
					setProgressDialog(false);
					//					goToFragmentByFragmentClass(MyFlightsFragment.class, Bundle.EMPTY, true);
					goToScreenById(eInAppNavigation.AddNewFlightFragment.getNavigationId(), Bundle.EMPTY);
					ErrorsHandler.tryShowServiceErrorDialog(iError, MyFlightsActivity.this);
				}
			});
		}
	}
	
	private void handleResponseGetUserActivePNRs(ArrayList<UserActivePnr> iUserActivePnrsList)
	{
		MyFlightsObject.getInstance().setUserActivePnrList(iUserActivePnrsList);
		
		if (!UserData.getInstance().isUserGuest() && !UserData.getInstance().getGroupPnrs().isEmpty())
		{
			MyFlightsObject.getInstance().getUserActivePnrList().addAll(UserData.getInstance().getGroupPnrs());
		}
		
		if (MyFlightsObject.getInstance().getUserActivePnrList().isEmpty())
		{
			loadMyFlightsAddFragment(false);
		}
		else if (MyFlightsObject.getInstance().getUserActivePnrList().size() == 1 && MyFlightsObject.getInstance().getUserActivePnrList().get(0) != null)
		{
			if (MyFlightsObject.getInstance().getUserActivePnrList().get(0).isGroupPnr() && UserData.getInstance().getGroupPnrs().contains(MyFlightsObject.getInstance().getUserActivePnrList().get(0)))
			{
				loadMyFlightsAddFragment(true);
			}
			else
			{
				getPnrByNum(MyFlightsObject.getInstance().getUserActivePnrList().get(0).getPnr());
			}
		}
		else
		{
			loadMyFlightsFragment();
		}
	}
	
	public void GetMultiplePNRDestinations(final Response.Listener<ResponseGetMultiplePNRDestinations> iResponseListener, final Response.ErrorListener iResponseErrorListener)
	{
		if (isServiceConnected())
		{
			setProgressDialog(true);
			
			int userId = UserData.getInstance().getUserID();
			String userActivePnr = MyFlightsObject.getInstance().getCurrentUserActivePnr().getPnr();
			
			getService().getController(MyFlightsController.class)
			            .GetMultiplePNRDestinations(new RequestGetMultiplePNRDestinations(userId, userActivePnr), new Response.Listener<ResponseGetMultiplePNRDestinations>()
			            {
				            @Override
				            public void onResponse(ResponseGetMultiplePNRDestinations response)
				            {
					            if (iResponseListener != null)
					            {
						            iResponseListener.onResponse(response);
					            }
					            setProgressDialog(false);
				            }
			            }, new Response.ErrorListener()
			            {
				            @Override
				            public void onErrorResponse(final VolleyError error)
				            {
					            if (iResponseErrorListener != null)
					            {
						            iResponseErrorListener.onErrorResponse(error);
					            }
					            setProgressDialog(false);
				            }
			            });
		}
	}
	
	public void GetPNRByNum(final Response.Listener<ResponseGetPnrByNum> iResponseListener, final Response.ErrorListener iResponseErrorListener, String iFlightPnr, String iFlightNumber)
	{
		//		if (isServiceConnected())
		//		{
		setProgressDialog(true);
		
		int userId = UserData.getInstance().getUserID();
		
		getService().getController(MyFlightsController.class).GetPnrByNum(new RequestPnrByNum(userId, iFlightPnr, iFlightNumber), new Response.Listener<ResponseGetPnrByNum>()
		{
			@Override
			public void onResponse(ResponseGetPnrByNum response)
			{
				setProgressDialog(false);
				if (iResponseListener != null)
				{
					iResponseListener.onResponse(response);
				}
			}
		}, new Response.ErrorListener()
		{
			@Override
			public void onErrorResponse(final VolleyError error)
			{
				if (iResponseErrorListener != null)
				{
					iResponseErrorListener.onErrorResponse(error);
				}
				
				setProgressDialog(false);
			}
		});
		//		}
	}
	
	private void handleResponseGetMultiplePNRDestinations(final ResponseGetMultiplePNRDestinations response)
	{
		if (response != null && response.getContent() != null)
		{
			GetMultiplePNRDestinations pnrDestinations = response.getContent();
			MyFlightsObject.getInstance().setCurrentPnrDestinations(pnrDestinations);
			
			loadMyFlightFragmentNew();
			mShouldExit = true;
			setProgressDialog(false);
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.GeneralError.getErrorMessage());
			setProgressDialog(false);
			goToFragmentByFragmentClass(MyFlightsFragment.class, Bundle.EMPTY, true);
		}
	}
	
	private void loadMyFlightFragmentNew()
	{
		goToFragmentByFragmentClass(MyFlightFragmentNew2.class, Bundle.EMPTY, false);
	}
	
	public void loadMyFlightsFragment()
	{
		if (!TextUtils.isEmpty(mCurrentFlightPnr))
		{
			getPnrByNum(mCurrentFlightPnr);
		}
		else
		{
			goToFragmentByFragmentClass(MyFlightsFragment.class, Bundle.EMPTY, true);
			setProgressDialog(false);
		}
	}
	
	public void getPnrByNum(String iCurrentFlightPnr)
	{
		if (isServiceConnected() && !TextUtils.isEmpty(iCurrentFlightPnr))
		{
			GetPNRByNum(new Response.Listener<ResponseGetPnrByNum>()
			{
				@Override
				public void onResponse(final ResponseGetPnrByNum response)
				{
					handleResponseGetPnrByNum(response);
				}
			}, this, iCurrentFlightPnr, null);
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.GeneralError.getErrorMessage());
			goToFragmentByFragmentClass(MyFlightsFragment.class, Bundle.EMPTY, true);
			setProgressDialog(false);
		}
	}
	
	private void handleResponseGetPnrByNum(ResponseGetPnrByNum response)
	{
		if (response != null && response.getContent() != null)
		{
			UserActivePnr activePnr = response.getContent();
			MyFlightsObject myFlightsObject = MyFlightsObject.getInstance();
			myFlightsObject.setCurrentUserActivePnr(activePnr);
			if (myFlightsObject.getUserActivePnrList() != null)
			{
				myFlightsObject.getUserActivePnrList().add(activePnr);
			}
			getMultiplePNRDestinations();
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.GeneralError.getErrorMessage());
			goToFragmentByFragmentClass(MyFlightsFragment.class, Bundle.EMPTY, true);
		}
	}
	
	private void getMultiplePNRDestinations()
	{
		if (isServiceConnected())
		{
			GetMultiplePNRDestinations(new Response.Listener<ResponseGetMultiplePNRDestinations>()
			{
				@Override
				public void onResponse(final ResponseGetMultiplePNRDestinations response)
				{
					handleResponseGetMultiplePNRDestinations(response);
				}
			}, this);
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.GeneralError.getErrorMessage());
			setProgressDialog(false);
		}
	}
	
	public void loadMyFlightsAddFragment(boolean iIsGroupPNR)
	{
		Bundle bndl = AddNewFlightFragment.createBundleForFragment(iIsGroupPNR);
		goToFragmentByFragmentClass(AddNewFlightFragment.class, bndl, false);
	}
	
	@Override
	public void onErrorResponse(final VolleyError iError)
	{
		//		ErrorsHandler.tryShowServiceErrorDialog(iError, this);
		if (iError != null && iError instanceof ServerError)
		{
			AppUtils.createSimpleMessageDialog(this, ((ServerError) iError).getErrorMessage(), null, null, getString(R.string.close), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					loadHomeFragment();
				}
			}).show();
		}
		else
		{
			ErrorsHandler.tryShowServiceErrorDialog(iError, this);
		}
		setProgressDialog(false);
		
		// TODO: 06/11/2017 - temporary test
		//		loadHomeFragment();
	}
	
	@Override
	public void onBackPressed()
	{
		if (isDrawerOpen())
		{
			closeDrawer();
		}
		else
		{
			android.support.v4.app.Fragment lastFragment = getFragmentSwapper().getTopFragment();
			if (getFragmentSwapper().getTopFragment().getClass().getSimpleName().equals(FlightDetailsFragment.class.getSimpleName()) || getFragmentSwapper().getTopFragment()
			                                                                                                                                                .getClass()
			                                                                                                                                                .getSimpleName()
			                                                                                                                                                .equals(OrderDetailsFragment.class.getSimpleName()))
			{
				getFragmentSwapper().popFragment();
			}
			else if (lastFragment instanceof EWBaseFragment && !getFragmentSwapper().isFragmentStackMaxOne())
			{
				((EWBaseFragment) lastFragment).tryPopFragment();
			}
			else if (getFragmentSwapper().isFragmentStackMaxOne())
			{
				int backStackEntryCount = getFragmentManager().getBackStackEntryCount();
				Log.d(TAG, "backstack count: " + backStackEntryCount);
				if (getFragmentSwapper().getTopFragment() instanceof TabsBackStackManager.ITabsBackStackManagerHandler)
				{
					if (getFragmentSwapper().getTopFragment().getClass().getSimpleName().equals(MyFlightFragmentNew2.class.getSimpleName()))
					{
						if (mShouldExit || backStackEntryCount < 1)
						{
							//							Log.d(TAG, "calling homeFrament");
							goToScreenById(eInAppNavigation.Main.getNavigationId(), Bundle.EMPTY);
						}
						else
						{
							//							Log.d(TAG, "calling super.onBackPressed for TabsBackStackManager");
							super.onBackPressed();
						}
					}
					else
					{
						if (mShouldExit)
						{
							goToScreenById(eInAppNavigation.Main.getNavigationId(), Bundle.EMPTY);
						}
						else
						{
							((TabsBackStackManager.ITabsBackStackManagerHandler) getFragmentSwapper().getTopFragment()).popFragment();
							mShouldExit = true;
						}
					}
				}
				else
				{
					//					Log.d(TAG, "not ITabsBackStackManagerHandler or FlightDetailsFragment or OrderDetailsFragment");
					if (mShouldExit || backStackEntryCount < 1)
					{
						goToScreenById(eInAppNavigation.Main.getNavigationId(), Bundle.EMPTY);
					}
					else
					{
						//						Log.d(TAG, "calling popFragment");
						getFragmentSwapper().popFragment();
						mShouldExit = true;
					}
				}
			}
			else
			{
				Log.d(TAG, "calling super.onBackPressed");
				super.onBackPressed();
			}
		}
	}
	
	public void loadMyFlightFragment(final String iPnr)
	{
		mCurrentFlightPnr = iPnr;
		getPnrByNum(mCurrentFlightPnr);
	}
	
	public void setFragmentsCount(int backStackEntryCount)
	{
		mFragmentsCount = backStackEntryCount;
	}
	
	@Override
	public void updateDataListener()
	{
		FlightDetailsFragment fragment = (FlightDetailsFragment) getFragmentSwapper().findFragmentByTag(FlightDetailsFragment.TAG);
		if (fragment != null)
		{
			fragment.getPnrByNum();
		}
		
	}
	
	@Override
	public void onSuccessfulAdd()
	{
		setmShouldUpdateCheckList(true);
	}
	
	
}
