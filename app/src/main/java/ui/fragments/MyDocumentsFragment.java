package ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.tagmanager.DataLayer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import global.IntentExtras;
import global.UserData;
import il.co.ewave.elal.R;
import interfaces.IMyDocumentsClickListener;
import ui.activities.EWBaseActivity;
import ui.activities.EWBaseDrawerActivity;
import ui.adapters.MyDocumentsAdapter;
import ui.fragments.myFlights.OrderDetailsFragment;
import ui.other.SpacesItemDecoration;
import utils.errors.ErrorsHandler;
import utils.global.dbObjects.Document.eDocumentType;
import utils.global.googleAnalitics.googleTagManager.TagManagerImplementation;
import utils.managers.TabsBackStackManager;
import webServices.controllers.MainController;
import webServices.global.ePnrFlightType;
import webServices.requests.RequestGetUserDestinations;
import webServices.responses.ResponseGetUserDestinations;
import webServices.responses.responseGetUserActivePNRs.UserActivePnr;

/**
 * Created with care by Shahar Ben-Moshe on 15/01/17.
 */

public class MyDocumentsFragment extends EWBaseFragment implements Response.Listener<ResponseGetUserDestinations>, Response.ErrorListener, IMyDocumentsClickListener
{
	private static final String TAG = MyDocumentsFragment.class.getSimpleName();
	//	private TextView mTvAllPnrsTitle;
	private TextView mTvSpecificPnrTitle;
	private TextView mTvNoDocsTitle;
	private RecyclerView mRvList;
	private Button mBtnAddNewDocument;
	private MyDocumentsAdapter mAdapter;
	private List<UserActivePnr> mUserActivePnrs = new ArrayList<>();
	private UserActivePnr mUserActivePnr;
	private Boolean mIsFlightDocs = false;
	private TextView mTvHeaderTitle;
	private View mLlHeaderTitleRoot;
	private View mBtnBack;
	private int mNumFlightDocs;
	
	public static Bundle createBundleForFragment(@NonNull final UserActivePnr iUserActivePnr, boolean isFlightDocs, int iNumOfDocs)
	{
		Bundle result = new Bundle();
		
		result.putParcelable(IntentExtras.USER_ACTIVE_PNR, iUserActivePnr);
		result.putBoolean(IntentExtras.FLIGHT_DOCS, isFlightDocs);
		result.putInt(IntentExtras.NUM_FLIGHT_DOCS, iNumOfDocs);
		
		return result;
	}
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_my_documents_layout_new);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		getIntentExtras();
		initReference();
		setListeners();
		setGui();
		pushScreenOpenEvent("My Documents");
	}
	
	private void getIntentExtras()
	{
		if (getArguments() != null)
		{
			mUserActivePnr = getArguments().getParcelable(IntentExtras.USER_ACTIVE_PNR);
			mIsFlightDocs = getArguments().getBoolean(IntentExtras.FLIGHT_DOCS, false);
			mNumFlightDocs = getArguments().getInt(IntentExtras.NUM_FLIGHT_DOCS, 1);
		}
	}
	
	@Override
	public void onServiceConnected()
	{
		if (isResumed() || isVisible())
		{
			if (mUserActivePnr == null && isServiceConnected())
			{
				trySetProgressDialog(true);
				
				getService().getController(MainController.class).getUserDestinations(new RequestGetUserDestinations(UserData.getInstance().getUserID()), this, this);
			}
			else
			{
				setAdapter();
			}
		}
	}
	
	private void initReference()
	{
		mBtnBack = getActivity().findViewById(R.id.iv_name_screen_back_arrow);
		mLlHeaderTitleRoot = getActivity().findViewById(R.id.ll_name_screen_root);
		mTvHeaderTitle = (TextView) getActivity().findViewById(R.id.tv_name_screen_title);
		//		mTvAllPnrsTitle = (TextView) getActivity().findViewById(R.id.tv_myDocuments_AllPnrsTitle);
		mTvSpecificPnrTitle = (TextView) getActivity().findViewById(R.id.tv_myDocuments_SpecificPnrTitle);
		mTvNoDocsTitle = (TextView) getActivity().findViewById(R.id.tv_myDocuments_NoDocs);
		mRvList = (RecyclerView) getActivity().findViewById(R.id.rv_myDocuments_List);
		mBtnAddNewDocument = (Button) getActivity().findViewById(R.id.btn_myDocuments_AddNewDocument);
	}
	
	private void setListeners()
	{
		mBtnAddNewDocument.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				if (mUserActivePnr != null)
				{
					goToAddDocument(mUserActivePnr, -1);
				}
			}
		});
		
		mBtnBack.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				getActivity().onBackPressed();
			}
		});
	}
	
	private void setGui()
	{
		mTvHeaderTitle.setText(getResources().getString(R.string.my_documents));
		
		if (mUserActivePnr == null)
		{
			mLlHeaderTitleRoot.setVisibility(View.VISIBLE);
			//			mTvAllPnrsTitle.setVisibility(View.VISIBLE);
			//			mBtnAddNewDocument.setVisibility(View.GONE);
			mTvSpecificPnrTitle.setVisibility(View.GONE);
			//			mBtnAddNewDocument.setVisibility(View.VISIBLE);
		}
		else
		{
			mLlHeaderTitleRoot.setVisibility(View.GONE);
			//			mTvAllPnrsTitle.setVisibility(View.GONE);
			//			mBtnAddNewDocument.setVisibility(View.GONE);
			//			mBtnAddNewDocument.setVisibility(View.VISIBLE);
			mTvSpecificPnrTitle.setVisibility(View.VISIBLE);
		}
	}
	
	private void setAdapter()
	{
		if (mUserActivePnrs != null)
		{
			Collections.sort(mUserActivePnrs, new Comparator<UserActivePnr>()
			{
				@Override
				public int compare(final UserActivePnr o1, final UserActivePnr o2)
				{
					int result;
					
					Date date1 = o1 != null && o1.getCurrentItemFlightAtPositionDepartureOrReurnOrNull(0, ePnrFlightType.DEPARTURE) != null ?
					             o1.getCurrentItemFlightAtPositionDepartureOrReurnOrNull(0, ePnrFlightType.DEPARTURE).getDepartureDate() :
					             null;
					Date date2 = o2 != null && o2.getCurrentItemFlightAtPositionDepartureOrReurnOrNull(0, ePnrFlightType.DEPARTURE) != null ?
					             o2.getCurrentItemFlightAtPositionDepartureOrReurnOrNull(0, ePnrFlightType.DEPARTURE).getDepartureDate() :
					             null;
					
					if (date1 == null)
					{
						result = -1;
					}
					else if (date2 == null)
					{
						result = 1;
					}
					else
					{
						result = date2.compareTo(date1);
					}
					
					return result;
				}
			});
		}
		
		if (mAdapter == null)
		{
			//			mRvList.setHasFixedSize(false);
			if ((mUserActivePnrs == null || mUserActivePnrs.size() == 0) && mUserActivePnr != null)
			{
				mUserActivePnrs = new ArrayList<>();
				mUserActivePnrs.add(mUserActivePnr);
				
				mTvNoDocsTitle.setVisibility(View.GONE);
			}
			else
			{
				mTvNoDocsTitle.setVisibility(View.VISIBLE);
			}
			
			if (mUserActivePnrs != null && mUserActivePnrs.size() > 0)
			{
				mTvNoDocsTitle.setVisibility(View.GONE);
			}
			else
			{
				mTvNoDocsTitle.setVisibility(View.VISIBLE);
			}
			
			
			mAdapter = new MyDocumentsAdapter(getActivity(), mUserActivePnrs, this, mIsFlightDocs, mNumFlightDocs);
			mRvList.setLayoutManager(mAdapter.getLayoutManager());
			mRvList.addItemDecoration(new SpacesItemDecoration(getResources().getDimensionPixelOffset(R.dimen.grid_spacing)));
			ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) mRvList.getLayoutParams();
			marginLayoutParams.setMargins(25, 0, 25, 0);
			mRvList.setLayoutParams(marginLayoutParams);
			
			mRvList.setAdapter(mAdapter);
		}
		else
		{
			mAdapter.notifyDataSetChanged();
		}
	}
	
	
	@Override
	public void onResponse(final ResponseGetUserDestinations response)
	{
		if (response != null && response.getContent() != null)
		{
			mUserActivePnrs.clear();
			mUserActivePnrs.addAll(response.getContent());
			
			if (isAdded() && isAttachedToActivity())
			{
				setAdapter();
			}
		}
		
		trySetProgressDialog(false);
	}
	
	@Override
	public void onErrorResponse(final VolleyError error)
	{
		ErrorsHandler.tryShowServiceErrorDialog(error, getActivity());
		trySetProgressDialog(false);
	}
	
	@Override
	public void onChildClick(final int iParentPosition, final int iChildPosition, final eDocumentType iDocumentType, final long iCategoryItemsCount)
	{
		String activePnr = UserData.getInstance().getActivePnr();
		int docType = -1;
		
		if (iDocumentType != null)
		{
			docType = iDocumentType.getDocumentTypeId();
		}
		
		if (iDocumentType == eDocumentType.FLIGHT_DETAILS)
		{
			//event 61
			((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight" + TagManagerImplementation.TIME_SCOPE, "Action", TagManagerImplementation.TIME_SCOPE + activePnr + " >> Save", "Label", docType + " > View"));
		}
		else
		{
			//event 62
			((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight" + TagManagerImplementation.TIME_SCOPE, "Action", TagManagerImplementation.TIME_SCOPE + activePnr + " >> Save", "Label", docType + " > Add"));
		}
		
		UserActivePnr currentUserActivePnr = mUserActivePnr;
		
		if (currentUserActivePnr == null)
		{
			currentUserActivePnr = mUserActivePnrs.get(iParentPosition);
		}
		
		if (iDocumentType != null && iDocumentType == eDocumentType.FLIGHT_DETAILS)
		{
			showFlightDetailsDialog(currentUserActivePnr);
		}
		else
		{
			int documentTypeId = iDocumentType != null ? iDocumentType.getDocumentTypeId() : -1;
			
			if (iCategoryItemsCount > 0)
			{
				goToDocumentsDetailsForType(currentUserActivePnr, documentTypeId, mUserActivePnr != null);
			}
			else if (mUserActivePnr != null)
			{
				if (UserData.getInstance().isUserGuest())
				{
					((EWBaseDrawerActivity) getActivity()).openAskLoginDialog();
				}
				else
				{
					goToAddDocument(mUserActivePnr, documentTypeId);
				}
			}
		}
	}
	
	
	private void showFlightDetailsDialog(final UserActivePnr iCurrentUserActivePnr)
	{
		Bundle bundle = OrderDetailsFragment.createBundleForFragment(iCurrentUserActivePnr);
		if (getParentFragment() != null && getParentFragment() instanceof TabsBackStackManager.ITabsBackStackManagerHandler)
		{
			OrderDetailsFragment orderDetailsFragment = new OrderDetailsFragment();
			orderDetailsFragment.setArguments(bundle);
			
			((TabsBackStackManager.ITabsBackStackManagerHandler) getParentFragment()).pushFragmentToCurrentTab(orderDetailsFragment, true, true);
		}
		else
		{
			tryGoToFragmentByFragmentClass(OrderDetailsFragment.class, bundle, true);
		}
		//		AppUtils.createFlightDetailsDialog(getActivity(), iCurrentUserActivePnr).show();
	}
	
	private void goToDocumentsDetailsForType(final UserActivePnr iUserActivePnr, final int iDocumentTypeId, final boolean iShouldBeAbleToAdd)
	{
		Bundle bundle = DocumentsForTypeFragment.createBundleForFragment(iUserActivePnr, iDocumentTypeId, iShouldBeAbleToAdd);
		
		if (getParentFragment() != null && getParentFragment() instanceof TabsBackStackManager.ITabsBackStackManagerHandler)
		{
			DocumentsForTypeFragment documentsForTypeFragment = new DocumentsForTypeFragment();
			documentsForTypeFragment.setArguments(bundle);
			
			((TabsBackStackManager.ITabsBackStackManagerHandler) getParentFragment()).pushFragmentToCurrentTab(documentsForTypeFragment, true, true);
		}
		else
		{
			tryGoToFragmentByFragmentClass(DocumentsForTypeFragment.class, bundle, true);
		}
	}
	
	private void goToAddDocument(final UserActivePnr iUserActivePnr, final int iDocumentTypeId)
	{
		Bundle bundle = AddNewDocumentFragment.createBundleForFragment(iUserActivePnr, null, iDocumentTypeId, mUserActivePnrs.size() > 1);
		
		if (getParentFragment() != null && getParentFragment() instanceof TabsBackStackManager.ITabsBackStackManagerHandler)
		{
			AddNewDocumentFragment addNewDocumentFragment = new AddNewDocumentFragment();
			addNewDocumentFragment.setArguments(bundle);
			
			((TabsBackStackManager.ITabsBackStackManagerHandler) getParentFragment()).pushFragmentToCurrentTab(addNewDocumentFragment, true, true);
		}
		else
		{
			tryGoToFragmentByFragmentClass(AddNewDocumentFragment.class, bundle, true);
		}
	}
	
}
