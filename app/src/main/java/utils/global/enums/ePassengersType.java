package utils.global.enums;

import utils.global.searchFlightObjects.SearchObject;

/**
 * Created with care by Alexey.T on 10/07/2017.
 */
public enum ePassengersType
{
//	ADULTS(new SearchObject.AgeRange(28, 120)),
//	CHILDREN(new SearchObject.AgeRange(2, 11)),
//	INFANTS(new SearchObject.AgeRange(0, 2)),
//	TEENAGERS(new SearchObject.AgeRange(12, 14)),
//	YOUTH(new SearchObject.AgeRange(15, 27)),
	
	INFANTS(new SearchObject.AgeRange(0, 1)),
	CHILDREN(new SearchObject.AgeRange(2, 11)),
	TEENAGERS(new SearchObject.AgeRange(12, 13)),
	YOUTH(new SearchObject.AgeRange(14, 27)),
	ADULTS(new SearchObject.AgeRange(28, 120)),
	FAMILY(null),
	NO_SUCH_ITEM(null);
	
	private SearchObject.AgeRange mMemberRangeAge;
	
	ePassengersType(final SearchObject.AgeRange iMemberRangeAge)
	{
		mMemberRangeAge = iMemberRangeAge;
	}
	
	public SearchObject.AgeRange getMemberRangeAge()
	{
		return mMemberRangeAge;
	}
	
	public boolean isInfants()
	{
		return this == INFANTS;
	}
	
	public boolean isAdults()
	{
		return this == ADULTS;
	}
	
	public boolean isYouth()
	{
		return this == YOUTH;
	}
	
	public boolean isTeenagers()
	{
		return this == TEENAGERS;
	}
	
	public boolean isChildren()
	{
		return this == CHILDREN;
	}
	
	public boolean isFamily()
	{
		return this == FAMILY;
	}
	
	public boolean isNoSuchItem()
	{
		return this == NO_SUCH_ITEM;
	}
}
