package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with care by Alexey.T on 26/07/2017.
 */
public class RequestSetPushAsRead
{
	@JsonProperty("UserID")
	private int mUserID;
	
	@JsonProperty("CampaignID")
	private int mCampaignID;
	
	@JsonProperty("Message")
	private String mMessage;
 
//	public RequestSetPushAsRead()
//	{
//	}
	
//	public RequestSetPushAsRead(final int iUserID, final int iCampaignID)
//	{
//		mUserID = iUserID;
//		mCampaignID = iCampaignID;
//	}
	
	public RequestSetPushAsRead(final int iUserID, final int iCampaignID, final String iMessage)
	{
		mUserID = iUserID;
		mCampaignID = iCampaignID;
		mMessage = iMessage;
	}
}
