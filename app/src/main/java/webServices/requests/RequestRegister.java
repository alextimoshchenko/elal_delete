package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.UserData;

/**
 * Created by Aviv.Kabeli on 15/12/2016.
 */

public class RequestRegister
{
	@JsonProperty("Email")
	private String mEmail;
	
	@JsonProperty("Mobile")
	private String mMobile;
	
	@JsonProperty("Password")
	private String mPassword;
	
	
	@JsonProperty("RepresentationId")
	private int mRepresentationId = UserData.getInstance().getRepresentationId();
	
	@JsonProperty("Device")
	private RequestDevice mDevice = new RequestDevice();
	
	@JsonProperty("LanguageCode")
	private String mLanguageCode = UserData.getInstance().getLanguage().getLanguageCode();
	
	public RequestRegister(final String iEmail, final String iMobile, final String iPassword)
	{
		mEmail = iEmail;
		mMobile = iMobile;
		mPassword = iPassword;
	}
}
