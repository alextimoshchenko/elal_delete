package ui.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;

import global.ElAlApplication;
import global.IntentExtras;
import global.UserData;
import global.eLanguage;
import il.co.ewave.elal.R;
import interfaces.IFamilyMemberListener;
import interfaces.ILoginResponse;
import jp.wasabeef.recyclerview.animators.FadeInLeftAnimator;
import ui.activities.EWBaseActivity;
import ui.activities.EWBaseDrawerActivity;
import ui.activities.EWBaseUserActivity;
import ui.adapters.FamilyMembersAdapter;
import ui.customWidgets.swipeLayout.SwipeLayout;
import utils.errors.ErrorsHandler;
import utils.errors.LocalError.eLocalError;
import utils.global.AppUtils;
import utils.global.DividerItemDecoration;
import utils.global.ElalPreferenceUtils;
import utils.global.LocaleUtils;
import utils.global.googleAnalitics.googleTagManager.TagManagerImplementation.ePushEvents;
import webServices.controllers.LoginController;
import webServices.global.Mapper;
import webServices.global.eActionType;
import webServices.requests.RequestGetUserFamily;
import webServices.requests.RequestSetMatmidUserFamily;
import webServices.requests.RequestSetRegularUserFamily;
import webServices.requests.RequestSetUserFamilyMember;
import webServices.responses.ResponseGetUserFamily;
import webServices.responses.ResponseSetUserFamily.ResponseSetMatmidUserFamily;
import webServices.responses.ResponseSetUserFamily.ResponseSetRegularUserFamily;
import webServices.responses.ResponseUserLogin;
import webServices.responses.responseGetUserFamily.GetUserFamilyContent;
import webServices.responses.responseUserLogin.UserLogin;

public class FamilyCardFragment extends EWBaseFragment implements Response.ErrorListener
{
	//	If you will have some issue with animation try this post https://github.com/daimajia/AndroidSwipeLayout/issues/250
	
	private static final String TAG = FamilyCardFragment.class.getSimpleName();
	private RecyclerView mRvFamilyMembers;
	private FamilyMembersAdapter mAdapter;
	private ArrayList<GetUserFamilyContent> mFamilyMembers = new ArrayList<>();
	private int mUserId = -1;
	private TextView mTvSkip, mBtnAddMember, mTvNameScreenTitle, mTvHeaderSkipLayoutTitle;
	private ImageView mIvBackArrow, mIvNameScreenBackArrow;
	private Button mBtnCardDone;
	private boolean mIsRegistration;
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_family_card_layout);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		initReference();
		setGui();
		setListeners();
		pushScreenOpenEvent("Family Account");
	}
	
	public void getIntentExtras()
	{
		if (getArguments() != null)
		{
			mIsRegistration = getArguments().getBoolean(IntentExtras.IS_REGISTRATION, false);
			mUserId = getArguments().getInt(IntentExtras.USER_ID, -1);
		}
		
		if (mUserId <= 0)
		{
			mUserId = UserData.getInstance().getUserID();
		}
	}
	
	private void setGui()
	{
		mTvHeaderSkipLayoutTitle.setText("");
		mTvNameScreenTitle.setText(getActivity().getResources().getString(R.string.family_account));
		mIvNameScreenBackArrow.setVisibility(View.GONE);
	}
	
	private void initReference()
	{
		Activity act = getActivity();
		mRvFamilyMembers = act.findViewById(R.id.rv_frag_family_card);
		mIvBackArrow = act.findViewById(R.id.iv_skip_layout_back_arrow);
		mBtnAddMember = act.findViewById(R.id.btn_frag_family_card_add_member);
		mBtnCardDone = act.findViewById(R.id.btn_frag_family_card_done);
		
		//header
		View header = act.findViewById(R.id.ll_family_card_header);
		mTvHeaderSkipLayoutTitle = header.findViewById(R.id.tv_header_skip_layout_title);
		mTvSkip = header.findViewById(R.id.tv_skip_layout_skip);
		
		//layout name
		View layoutName = act.findViewById(R.id.fl_family_card_layout_name);
		mIvNameScreenBackArrow = layoutName.findViewById(R.id.iv_name_screen_back_arrow);
		mTvNameScreenTitle = layoutName.findViewById(R.id.tv_name_screen_title);
	}
	
	private void setListeners()
	{
		mTvSkip.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnSkip();
			}
		});
		
		mIvBackArrow.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnSkip();
			}
		});
		
		mBtnAddMember.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnAddMember();
			}
		});
		
		mBtnCardDone.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				//successful set family. cancel the reminder dialog
				ElalPreferenceUtils.setSharedPreference(getActivity(), AppUtils.getRemindMeFamilyKey(UserData.getInstance().getUserID()), false);
				doOnDone();
			}
		});
	}
	
	private void doOnDone()
	{
		goToLogin();
	}
	
	private void doOnAddMember()
	{
		goToUserFamilyFragmentCreateNewMember();
		((EWBaseActivity) getActivity()).pushEvent(ePushEvents.ADD_FAMILY_MEMBER);
	}
	
	private void doOnSkip()
	{
		goToLogin();
	}
	
	private void goToLogin()
	{
		if (getActivity() != null && getActivity() instanceof EWBaseUserActivity)
		{
			if (UserData.getInstance().isUserGuest() || (mIsRegistration && !UserData.getInstance().isUserGuest()))
			{
				trySetProgressDialog(true);
				
				UserData data = UserData.getInstance();
				String email = data.getEmail();
				String pass = data.getPassword();
				
				((EWBaseUserActivity) getActivity()).performRegisteredUserLogin(email, pass, true, new Response.Listener<ResponseUserLogin>()
				{
					@Override
					public void onResponse(final ResponseUserLogin response)
					{
						doOnSuccessfulRegisteredUserLogin(response);
					}
				}, this);
			}
			else
			{
				goToHomeFragment();
				//				tryGoToFragmentByFragmentClass(HomeFragment.class, null, false);
			}
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, eLocalError.GeneralError.getErrorMessage());
		}
	}
	
	private void doOnSuccessfulRegisteredUserLogin(final ILoginResponse iResponse)
	{
		trySetProgressDialog(false);
		
		if (iResponse != null && iResponse.getContent() != null)
		{
			eLanguage userLanguageInServer = eLanguage.getLanguageByCodeOrDefault("", eLanguage.English);
			
			if (iResponse.getContent() instanceof UserLogin)
			{
				UserData.getInstance().setUserId(((UserLogin) iResponse.getContent()).getUserID());
				UserData.getInstance().setUserObject(((UserLogin) iResponse.getContent()).getUserObject());
				userLanguageInServer = eLanguage.getLanguageByCodeOrDefault(((UserLogin) iResponse.getContent()).getUserObject().getLanguageID(), eLanguage.English);
			}
			if (!mIsRegistration)
			{
				UserData.getInstance().saveUserData();
			}
			
			if (userLanguageInServer != UserData.getInstance().getLanguage())
			{
				UserData.getInstance().setLanguage(userLanguageInServer);
				if (!mIsRegistration)
				{
					UserData.getInstance().saveUserData();
				}
				LocaleUtils.applyLocale(ElAlApplication.getInstance(), LocaleUtils.convertLanguageCodeToLocaleOrNull(userLanguageInServer.getLanguageCode()));
				
				AlertDialog dialog = AppUtils.createSimpleMessageDialog(getActivity(), getString(R.string.restart_after_language_change), null, null, getString(R.string.close), new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(final DialogInterface dialog, final int which)
					{
						tryPopFragment();
					}
				});
				dialog.setCancelable(false);
				dialog.show();
			}
			else
			{
				goToHomeFragment();
			}
		}
	}
	
	private void goToHomeFragment()
	{
		if (getActivity() != null && getActivity() instanceof EWBaseUserActivity)
		{
			((EWBaseUserActivity) getActivity()).loadHomeFragment();
		}
	}
	
	private void setAdapter()
	{
		if (mFamilyMembers == null || mFamilyMembers.isEmpty())
		{
			AppUtils.printLog(Log.ERROR, TAG, eLocalError.SomeProblemWithArray.getErrorMessage());
			tryGoToFragmentByFragmentClass(UserFamilyFragment.class, null, false);
		}
		
		if (mAdapter == null)
		{
			mAdapter = new FamilyMembersAdapter(getContext(), mFamilyMembers, new IFamilyMemberListener()
			{
				@Override
				public void onDeleteItem(final int iPosition)
				{
					openDeleteItemDialog(iPosition);
				}
				
				@Override
				public void onEditItem(final int iPosition)
				{
					goToUserFamilyFragmentEditCurrentMember(iPosition);
				}
				
				@Override
				public void onItemClick(final int iPosition, final SwipeLayout iSwipeLayout)
				{
					doOnItemClick(iPosition, iSwipeLayout);
				}
				
				@Override
				public void onOptionClick(final int iPosition)
				{
					doOnOptionClick(iPosition);
				}
				
				@Override
				public void unlinkMatMidMember(SwipeLayout iSwipeLayout, final int iPosition)
				{
					AppUtils.printLog(Log.ERROR, TAG, "unlinkMatMidMember");
					iSwipeLayout.close();
					openUnlinkMatMidMemberDialog(iPosition);
				}
			});
			
			mRvFamilyMembers.setLayoutManager(new LinearLayoutManager(getActivity()));
			mRvFamilyMembers.addItemDecoration(new DividerItemDecoration(getActivity().getResources().getDrawable(R.drawable.divider)));
			mRvFamilyMembers.setItemAnimator(new FadeInLeftAnimator());
			mRvFamilyMembers.setAdapter(mAdapter);
		}
		else
		{
			mAdapter.closeAllItems();
			mAdapter.notifyDataSetChanged();
		}
	}
	
	private void openUnlinkMatMidMemberDialog(final int iPosition)
	{
		AlertDialog dialog = AppUtils.createSimpleMessageDialog(getActivity(), getActivity().getString(R.string.delete_family_matmid_member_message_dialog),
				
				getActivity().getString(R.string._continue), new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(final DialogInterface dialog, final int which)
					{
						doOnDeleteItem(iPosition);
					}
				},
				
				getActivity().getString(R.string.cancel), new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(final DialogInterface dialog, final int which)
					{
						dialog.dismiss();
					}
				});
		dialog.setCancelable(false);
		dialog.show();
	}
	
	private void openDeleteItemDialog(final int iPosition)
	{
		AlertDialog dialog = AppUtils.createSimpleMessageDialog(getActivity(), getActivity().getString(R.string.delete_family_member_message_dialog),
				
				getActivity().getString(R.string.approve), new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(final DialogInterface dialog, final int which)
					{
						doOnDeleteItem(iPosition);
					}
				},
				
				getActivity().getString(R.string.cancel), new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(final DialogInterface dialog, final int which)
					{
						dialog.dismiss();
					}
				});
		dialog.setCancelable(false);
		dialog.show();
		
	}
	
	private void doOnDeleteItem(final int iPosition)
	{
		GetUserFamilyContent userContent = mFamilyMembers.get(iPosition);
		
		if (iPosition >= 0 && userContent != null)
		{
			if (!isServiceConnected())
			{
				return;
			}
			
			trySetProgressDialog(true);
			
			if (userContent.getMatmidMemberID() == 0)
			{
				doOnDeleteRegularUserFamily(iPosition);
			}
			else if (userContent.getMatmidMemberID() > 0)
			{
				doOnDeleteMatmidUserFamily(userContent);
			}
		}
	}
	
	private void doOnDeleteMatmidUserFamily(final GetUserFamilyContent iUserContent)
	{
		if (isServiceConnected())
		{
			trySetProgressDialog(true);
			getService().getController(LoginController.class).SetMatmidUserFamily(new RequestSetMatmidUserFamily(UserData.getInstance().getUserObject().getSessionID(),
								String.valueOf(iUserContent.getMatmidMemberID()), String.valueOf(iUserContent.getFamilyTypeID()), iUserContent.getFirstNameEng(),
								iUserContent.getLastNameEng(), eActionType.DELETE, UserData.getInstance().getUserObject().getMatmidMemberID(), iUserContent.getFamilyID()), new Response.Listener<ResponseSetMatmidUserFamily>()
			            {
				            @Override
				            public void onResponse(final ResponseSetMatmidUserFamily response)
				            {
					            trySetProgressDialog(false);
					            onSuccessfulDelete();
				            }
			            }, this);
		}
	}
	
	private void doOnDeleteRegularUserFamily(final int iPosition)
	{
		if (isServiceConnected())
		{
			getService().getController(LoginController.class)
			            .SetRegularUserFamily(new RequestSetRegularUserFamily(mUserId, new RequestSetUserFamilyMember(mFamilyMembers.get(iPosition), false)), new Response.Listener<ResponseSetRegularUserFamily>()
			            {
				            @Override
				            public void onResponse(final ResponseSetRegularUserFamily iResponse)
				            {
					            onSuccessfulDelete();
				            }
			            }, this);
		}
	}
	
	@Override
	public void onErrorResponse(final VolleyError error)
	{
		trySetProgressDialog(false);
		ErrorsHandler.tryShowServiceErrorDialog(error, getActivity());
	}
	
	private void onSuccessfulDelete()
	{
		refreshUserFamily();
	}
	
	private void doOnOptionClick(int iPosition)
	{
		
	}
	
	private void doOnItemClick(int iPosition, SwipeLayout iSwipeLayout)
	{
		//TODO IMPORTANT
		//if you will have a problem with click to swipe, check this method `getOpenStatus()` there is return statement from several places, try to set it consistently, I leave the comment inside.
		// But then check in NotificationListFragment in Hebrew if you get click reflection
		if (iSwipeLayout.getOpenStatus().isClosed())
		{
			iSwipeLayout.open();
		}
		else
		{
			iSwipeLayout.close();
		}
	}
	
	public void goToUserFamilyFragmentEditCurrentMember(int iPosition)
	{
		Bundle bundle = new Bundle();
		String familyObj = Mapper.string(mFamilyMembers.get(iPosition));
		bundle.putString(IntentExtras.FAMILY_OBJ, familyObj);
		openUserFamilyFragment(bundle);
	}
	
	public void goToUserFamilyFragmentCreateNewMember()
	{
		Bundle bundle = new Bundle();
		bundle.putBoolean(IntentExtras.CREATE_NEW_MEMBER, true);
		openUserFamilyFragment(bundle);
	}
	
	private void openUserFamilyFragment(Bundle iBundle)
	{
		tryGoToFragmentByFragmentClass(UserFamilyFragment.class, iBundle, true);
	}
	
	@Override
	public void setUserVisibleHint(final boolean isVisibleToUser)
	{
		super.setUserVisibleHint(isVisibleToUser);
		
		if (isVisibleToUser)
		{
			refreshLayout();
		}
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		refreshLayout();
		((EWBaseDrawerActivity)getActivity()).enableLogoBtn(!mIsRegistration);
	}
	
	
	@Override
	public void onPause()
	{
		((EWBaseDrawerActivity)getActivity()).enableLogoBtn(mIsRegistration);
		super.onPause();
	}
	
	
	private void switchToBackMode(boolean iIsRegistration)
	{
		mIvBackArrow.setVisibility(iIsRegistration ? View.GONE : View.VISIBLE);
		mTvSkip.setVisibility(iIsRegistration ? View.VISIBLE : View.GONE);
	}
	
	private void refreshLayout()
	{
		try
		{
			getIntentExtras();
			switchToBackMode(mIsRegistration);
			refreshUserFamily();
		}
		catch (Exception ignored)
		{
		}
	}
	
	private void refreshUserFamily()
	{
		if (isServiceConnected() && mUserId > 0)
		{
			trySetProgressDialog(true);
			
			getService().getController(LoginController.class).GetUserFamily(new RequestGetUserFamily(mUserId), new Response.Listener<ResponseGetUserFamily>()
			{
				@Override
				public void onResponse(final ResponseGetUserFamily iResponseGetUserFamily)
				{
					trySetProgressDialog(false);
					doOnResponse(iResponseGetUserFamily);
				}
			}, this);
		}
		else
		{
			setAdapter();
		}
	}
	
	private void doOnResponse(final ResponseGetUserFamily iResponseGetUserFamily)
	{
		if (iResponseGetUserFamily != null)
		{
			mFamilyMembers.clear();
			
			if (iResponseGetUserFamily.getContent().getFamily() != null)
			{
				mFamilyMembers.addAll(iResponseGetUserFamily.getContent().getFamily());
			}
			
			setAdapter();
		}
	}
}
