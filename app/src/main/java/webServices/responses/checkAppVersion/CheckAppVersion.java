package webServices.responses.checkAppVersion;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with care by Shahar Ben-Moshe on 13/12/16.
 */

public class CheckAppVersion
{
	@JsonProperty("AppID")
	private int mAppID;
	@JsonProperty("OsID")
	private int mOsID;
	@JsonProperty("Version")
	private double mAppVer;
	@JsonProperty("IsMandatory")
	private boolean mIsMandatory;
	
	public double getAppVer()
	{
		return mAppVer;
	}
	
	public boolean isMandatory()
	{
		return mIsMandatory;
	}
	
	public CheckAppVersion()
	{
	}
}
