package ui.fragments.myFlights;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.tagmanager.DataLayer;

import java.util.ArrayList;

import global.IntentExtras;
import global.UserData;
import il.co.ewave.elal.R;
import ui.activities.EWBaseActivity;
import ui.activities.MyFlightsActivity;
import ui.adapters.FlightDetailsPassengersAdapter;
import ui.fragments.EWBaseFragment;
import utils.global.DateTimeUtils;
import utils.global.googleAnalitics.googleTagManager.TagManagerImplementation;
import utils.global.myFlightsObjects.MyFlightsObject;
import webServices.global.RequestStringBuilder;
import webServices.global.ePnrFlightType;
import webServices.responses.ResponseGetPnrByNum;
import webServices.responses.getMultiplePNRDestinations.DestinationListItem;
import webServices.responses.responseGetUserActivePNRs.Passenger;
import webServices.responses.responseGetUserActivePNRs.PnrFlight;
import webServices.responses.responseGetUserActivePNRs.UserActivePnr;

public class FlightDetailsFragment extends EWBaseFragment
{
	public static final String TAG = FlightDetailsFragment.class.getSimpleName();
	private ImageView mIvClose;
	private TextView mTvOrigin, mTvConnectionOrigin;
	private TextView mTvDestination, mTvConnectionDestination;
	private TextView mTvFlightNumber, mTvConnectionFlightNumber;
	private TextView mTvDeparture, mTvConnectionDeparture;
	private TextView mTvArrival, mTvConnectionArrival;
	private TextView mTvPlaneType, mTvConnectionPlaneType;
	private TextView mTvFlightDuration, mTvConnectionFlightDuration;
	private TextView mTvConnectionLayover;
	private RecyclerView mRvPassengers, mRvConnectionFlightPassengers;
	private UserActivePnr mUserActivePnr;
	//	private DestinationListItem mDestinationFlight;
	private LinearLayout mLilConnectionFlightContainer;
	private Button mBtnChangeSeats, mBtnChangeMeals;
	private PnrFlight mCurrentFlight;
	
	public FlightDetailsFragment()
	{
		// Required empty public constructor
	}
	
	public static Bundle createBundleForFragment(@NonNull final UserActivePnr iUserActivePnr, DestinationListItem iDestinationList)
	{
		Bundle result = new Bundle();
		
		result.putParcelable(IntentExtras.USER_ACTIVE_PNR, iUserActivePnr);
		result.putParcelable(IntentExtras.USER_FLIGHT_DESTINATION, iDestinationList);
		
		return result;
	}
	
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		Log.d(TAG, "calling OnCreate");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_flight_details);
		
		((MyFlightsActivity) getActivity()).setFragmentsCount(getFragmentManager().getBackStackEntryCount());
	}
	
	@Override
	public void onViewCreatedOnce(View view, @Nullable Bundle savedInstanceState)
	{
		Log.d(TAG, "calling onViewCreatedOnce");
		getIntentExtras();
		initReference(view);
		setListeners();
		setData();
		
		pushScreenOpenEvent("More About The Flight");
	}
	
	@Override
	public void onResume()
	{
		Log.d(TAG, "calling onResume");
		super.onResume();
		
		// TODO: 04/12/2017 - get pnr by num
	}
	
	private void getIntentExtras()
	{
		if (getArguments() != null)
		{
			mUserActivePnr = getArguments().getParcelable(IntentExtras.USER_ACTIVE_PNR);
			//			mDestinationFlight = getArguments().getParcelable(IntentExtras.USER_FLIGHT_DESTINATION);
		}
	}
	
	private void initReference(View view)
	{
		mIvClose = view.findViewById(R.id.iv_flightDetails_Close);
		mTvOrigin = view.findViewById(R.id.tv_flightDetails_Origin);
		mTvDestination = view.findViewById(R.id.tv_flightDetails_Destination);
		mTvFlightNumber = view.findViewById(R.id.tv_flightDetails_FlightNumber);
		mTvFlightDuration = view.findViewById(R.id.tv_flightDetails_FlightDuration);
		mTvDeparture = view.findViewById(R.id.tv_flightDetails_Departure);
		mTvArrival = view.findViewById(R.id.tv_flightDetails_Arrival);
		mTvPlaneType = view.findViewById(R.id.txv_plane_type);
		mRvPassengers = view.findViewById(R.id.rv_flightDetails_Passengers);
		mTvConnectionLayover = view.findViewById(R.id.txv_ConnectionFlight_layover);
		mTvConnectionOrigin = view.findViewById(R.id.tv_ConnectionFlight_Origin);
		mTvConnectionDestination = view.findViewById(R.id.tv_ConnectionFlight_Destination);
		mTvConnectionFlightNumber = view.findViewById(R.id.tv_ConnectionFlight_FlightNumber);
		mTvConnectionFlightDuration = view.findViewById(R.id.tv_ConnectionFlight_FlightDuration);
		mTvConnectionDeparture = view.findViewById(R.id.tv_ConnectionFlight_Departure);
		mTvConnectionArrival = view.findViewById(R.id.tv_ConnectionFlight_Arrival);
		mTvConnectionPlaneType = view.findViewById(R.id.txv_connection_plane_type);
		mRvConnectionFlightPassengers = view.findViewById(R.id.rv_ConnectionFlight_Passengers);
		mLilConnectionFlightContainer = view.findViewById(R.id.lil_ConnectionFlight_container);
		
		mBtnChangeSeats = view.findViewById(R.id.btn_myFlightTop_ChangeSeats);
		mBtnChangeMeals = view.findViewById(R.id.btn_myFlightTop_ChangeMeals);
	}
	
	private void setListeners()
	{
		mIvClose.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				popFragment();
			}
		});
		
		mBtnChangeSeats.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				doOnChangeSeatsClick(mUserActivePnr.getPnr(), mUserActivePnr.getLastName());
				
				//event 45
				String activePnr = UserData.getInstance().getActivePnr();
				((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight" + TagManagerImplementation.TIME_SCOPE, "Action", TagManagerImplementation.TIME_SCOPE + activePnr + ">> More about the flight", "Label", "Change seating"));
			}
		});
		
		mBtnChangeMeals.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				doOnChangeMealsClick(mUserActivePnr.getPnr(), mUserActivePnr.getLastName());
				
				//event 46
				String activePnr = UserData.getInstance().getActivePnr();
				((EWBaseActivity) getActivity()).pushCustomEvent(getString(R.string.auto_event), DataLayer.mapOf("Category", "Manage Flight" + TagManagerImplementation.TIME_SCOPE, "Action", TagManagerImplementation.TIME_SCOPE + activePnr + ">> More about the " + "flight", "Label", "Change meal"));
			}
		});
	}
	
	private void setData()
	{
		PnrFlight currentFlight = null;
		PnrFlight viaFlight = null;
		
		mCurrentFlight = mUserActivePnr.getCurrentFlight();
		
		if (mCurrentFlight.geteNumDirection() == ePnrFlightType.DEPARTURE)
		{
			if (mUserActivePnr.getDeparturePNRFlights() != null && mUserActivePnr.getDeparturePNRFlights().size() > 1)
			{
				viaFlight = mUserActivePnr.getCurrentItemFlightAtPositionDepartureOrReurnOrNull(0, ePnrFlightType.DEPARTURE);
				currentFlight = mUserActivePnr.getCurrentItemFlightAtPositionDepartureOrReurnOrNull(1, ePnrFlightType.DEPARTURE);
			}
			else
			{
				currentFlight = mUserActivePnr.getCurrentItemFlightAtPositionDepartureOrReurnOrNull(0, ePnrFlightType.DEPARTURE);
			}
		}
		else
		{
			if (isReturnFlightWithConnection())
			{
				viaFlight = mUserActivePnr.getCurrentItemFlightAtPositionDepartureOrReurnOrNull(0, ePnrFlightType.RETURN);
				currentFlight = mUserActivePnr.getCurrentItemFlightAtPositionDepartureOrReurnOrNull(1, ePnrFlightType.RETURN);
			}
			else
			{
				currentFlight = mUserActivePnr.getCurrentItemFlightAtPositionDepartureOrReurnOrNull(0, ePnrFlightType.RETURN);
			}
		}
		
		// set via flight details if exist.
		if (viaFlight != null && currentFlight != null)
		{
			if (mLilConnectionFlightContainer != null)
			{
				mLilConnectionFlightContainer.setVisibility(View.VISIBLE);
			}
			
			// set via data
			long timeDifference = DateTimeUtils.getTimeDifferenceInMilliseconds(viaFlight.getArrivalDate(), currentFlight.getDepartureDate());
			if (timeDifference > 0)
			{
				mTvConnectionLayover.setVisibility(View.VISIBLE);
				mTvConnectionLayover.setText(getResources().getString(R.string.layover_) + " " + DateTimeUtils.convertDDeltaInMillisecondsToTimeStringWithoutSeconds(timeDifference));
			}
			else
			{
				mTvConnectionLayover.setVisibility(View.GONE);
			}
			mTvConnectionOrigin.setText(viaFlight.getOrigination());
			mTvConnectionDestination.setText(viaFlight.getDestination());
			mTvConnectionFlightNumber.setText(viaFlight.getFlightNumber());
			mTvConnectionDeparture.setText(DateTimeUtils.convertDateToDayMonthHourMinuteString(viaFlight.getDepartureDate()));
			mTvConnectionArrival.setText(DateTimeUtils.convertDateToDayMonthHourMinuteString(viaFlight.getArrivalDate()));
			mTvConnectionFlightDuration.setText(!TextUtils.isEmpty(viaFlight.getFlightDuration()) ? viaFlight.getFlightDuration() : "");
//			getActivity().findViewById(R.id.ll_ConnectionFlight_FlightDuration).setVisibility(!TextUtils.isEmpty(viaFlight.getFlightDuration()) ? View.VISIBLE : View.INVISIBLE);
			
			
			mTvConnectionPlaneType.setText(!TextUtils.isEmpty(viaFlight.getAircraftType()) ? viaFlight.getAircraftType() : "");
			
			mRvConnectionFlightPassengers.setLayoutManager(new LinearLayoutManager(getContext()));
			ArrayList<Passenger> viaPassengers = new ArrayList<>();
			//			UserActivePnr pnrByNum = MyFlightsObject.getInstance().getCurrentActivePnrData();
			if (viaFlight.getFlightNumber() != null && !TextUtils.isEmpty(viaFlight.getFlightNumber()))
			{
				//				Log.d(TAG, "getting passengers for via flight: " + viaFlight.getFlightNumber());
				viaPassengers = mUserActivePnr.getPassengersByFlightNum(viaFlight.getFlightNumber());
			}
			else
			{
				//				Log.d(TAG, "getting passengers from mUserActivePnr");
				viaPassengers = mUserActivePnr.getPassengers();
			}
			mRvConnectionFlightPassengers.setAdapter(new FlightDetailsPassengersAdapter(viaPassengers, mUserActivePnr.getPnr()));
			mRvConnectionFlightPassengers.setNestedScrollingEnabled(false);
		}
		else
		{
			if (mLilConnectionFlightContainer != null)
			{
				mLilConnectionFlightContainer.setVisibility(View.GONE);
			}
			
			if (mTvConnectionLayover != null)
			{
				mTvConnectionLayover.setVisibility(View.GONE);
			}
		}
		
		
		// set flight data to destination
		if (currentFlight != null)
		{
			mTvOrigin.setText(currentFlight.getOrigination());
			mTvDestination.setText(currentFlight.getDestination());
			mTvFlightNumber.setText(currentFlight.getFlightNumber());
			mTvDeparture.setText(DateTimeUtils.convertDateToDayMonthHourMinuteString(currentFlight.getDepartureDate()));
			mTvArrival.setText(DateTimeUtils.convertDateToDayMonthHourMinuteString(currentFlight.getArrivalDate()));
			mTvFlightDuration.setText(!TextUtils.isEmpty(currentFlight.getFlightDuration()) ? currentFlight.getFlightDuration() : "");
//			getActivity().findViewById(R.id.ll_flightDetails_FlightDuration).setVisibility(!TextUtils.isEmpty(currentFlight.getFlightDuration()) ? View.VISIBLE : View.INVISIBLE);
			mTvPlaneType.setText(!TextUtils.isEmpty(currentFlight.getAircraftType()) ? currentFlight.getAircraftType() : "");
			
			mRvPassengers.setLayoutManager(new LinearLayoutManager(getContext()));
			ArrayList<Passenger> passengers = new ArrayList<>();
			//			UserActivePnr pnrByNum = MyFlightsObject.getInstance().getCurrentActivePnrData();
			if (currentFlight.getFlightNumber() != null && !TextUtils.isEmpty(currentFlight.getFlightNumber()))
			{
				passengers = mUserActivePnr.getPassengersByFlightNum(currentFlight.getFlightNumber());
			}
			else
			{
				passengers = mUserActivePnr.getPassengers();
			}
			
			mRvPassengers.setAdapter(new FlightDetailsPassengersAdapter(passengers, mUserActivePnr.getPnr()));
			mRvPassengers.setNestedScrollingEnabled(false);
		}
		else
		{
			mTvOrigin.setText("");
			mTvDestination.setText("");
			mTvFlightNumber.setText("");
			mTvDeparture.setText("");
			mTvArrival.setText("");
			mTvFlightDuration.setText("");
//			getActivity().findViewById(R.id.ll_flightDetails_FlightDuration).setVisibility(View.INVISIBLE);
			mTvPlaneType.setText("");
		}
		
	}
	
	private void popFragment()
	{
		tryPopFragment();
	}
	
	private void doOnChangeSeatsClick(String iPnr, String iLastName)
	{
		tryGoToWebFragment(RequestStringBuilder.getChangeSeatMealUrlByLanguage(iPnr, iLastName));
	}
	
	private void doOnChangeMealsClick(String iPnr, String iLastName)
	{
		String url = RequestStringBuilder.getChangeSeatMealUrlByLanguage(iPnr, iLastName);
		tryGoToWebFragment(url);
	}
	
	private boolean isReturnFlightWithConnection()
	{
		return mUserActivePnr.getReturnPNRFlights() != null && !mUserActivePnr.getReturnPNRFlights().isEmpty() && mUserActivePnr.getReturnPNRFlights().size() > 1;
	}
	
	public void getPnrByNum()
	{
		String pnr = mCurrentFlight.getPNR();
		String flightNumber = mCurrentFlight.getFlightNumber();
		
		trySetProgressDialog(true);
		((MyFlightsActivity) getActivity()).GetPNRByNum(new Response.Listener<ResponseGetPnrByNum>()
		{
			@Override
			public void onResponse(final ResponseGetPnrByNum response)
			{
				handleResponseGetPnrByNum(response);
			}
		}, new Response.ErrorListener()
		{
			@Override
			public void onErrorResponse(final VolleyError error)
			{
				trySetProgressDialog(false);
			}
		}, pnr, flightNumber);
	}
	
	private void handleResponseGetPnrByNum(ResponseGetPnrByNum response)
	{
		if (response != null && response.getContent() != null)
		{
			trySetProgressDialog(false);
			UserActivePnr activePnr = response.getContent();
			MyFlightsObject myFlightsObject = MyFlightsObject.getInstance();
			myFlightsObject.setCurrentUserActivePnr(activePnr);
			
			mUserActivePnr = activePnr;
			
			//			mCurrentFlight = response.getContent().getCurrentFlight();
			
			setData();
		}
	}
}
