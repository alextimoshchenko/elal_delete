# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/Shahar/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

#If you not sure about proguard process so uncomment next line
#-dontobfuscate

## view res/layout/design_layout_snackbar_include.xml #generated:18
#-keep class android.support.design.internal.SnackbarContentLayout { <init>(...); }
#
## view res/layout/activity_main.xml #generated:11
#-keep class android.support.design.widget.AppBarLayout { <init>(...); }
#
## view AndroidManifest.xml #generated:19
#-keep class ru.jollydroid.athdemo.MainActivity { <init>(...); }
#
##
#
## Proguard configuration for Jackson 2.x (fasterxml package instead of codehaus package)
#-keep class com.fasterxml.jackson.databind.ObjectMapper {
#    public <methods>;
#    protected <methods>;
#}
#-keep class com.fasterxml.jackson.databind.ObjectWriter {
#    public ** writeValueAsString(**);
#}
#-keepnames class com.fasterxml.jackson.** { *; }
#
#-dontwarn com.fasterxml.jackson.databind.**
#
#-dontwarn com.squareup.okhttp.**
#
#-dontwarn cryptix.util.test.**
#
#-dontwarn jp.wasabeef.recyclerview.animators.**
#
#-dontwarn cryptix.util.gui.**
#
#-dontwarn ui.activities.**
#
#-dontwarn ui.adapters.**
#
#-dontwarn ui.fragments.**
#
#-dontwarn webServices.controllers.**

-ignorewarnings

-keep class * {
    public private *;
}


