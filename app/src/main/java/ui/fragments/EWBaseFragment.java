package ui.fragments;

import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import il.co.ewave.elal.BuildConfig;
import interfaces.IElalScreensNavigator;
import interfaces.IEwOnServiceConnectedListener;
import services.EWSSLService;
import ui.activities.EWBaseActivity;
import ui.activities.MyFlightsActivity;
import utils.global.AppUtils;
import utils.managers.TabsBackStackManager;

/**
 * Created with care by Shahar Ben-Moshe on 28-Dec-2015.
 */
public abstract class EWBaseFragment extends Fragment implements IEwOnServiceConnectedListener, Parcelable
{
	private static final String TAG = EWBaseFragment.class.getSimpleName();
	
	private int mContentViewResourceId;
	private boolean mIsFirstLoad = true;
	private View mRootView;
	
	public EWBaseFragment()
	{
		setArguments(new Bundle());
	}
	
	@Override
	public final void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		
		if (isActivityIsOfTypeEwBaseActivity())
		{
			((EWBaseActivity) getActivity()).addEwOnServiceConnectedListener(this);
		}
	}
	
	@Override
	public final void onViewCreated(View view, @Nullable Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		
		if (mIsFirstLoad)
		{
			mIsFirstLoad = false;
			onViewCreatedOnce(mRootView, savedInstanceState);
		}
	}
	
	public void onViewCreatedOnce(View view, @Nullable Bundle savedInstanceState)
	{
	}
	
	@Override
	public void onPause()
	{
		super.onPause();
		hideKeyboard();
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		
		if (BuildConfig.DEBUG)
		{
			AppUtils.printLog(Log.DEBUG, TAG, this.getClass().getCanonicalName());
		}
	}
	
	@Override
	public void onDestroy()
	{
		if (isActivityIsOfTypeEwBaseActivity())
		{
			((EWBaseActivity) getActivity()).removeEwOnServiceConnectedListener(this);
		}
		
		super.onDestroy();
	}
	
	@Override
	public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		if (mRootView == null)
		{
			mRootView = (inflater == null ? getActivity().getLayoutInflater() : inflater).inflate(mContentViewResourceId, container, false);
		}
		
		/**avishay 05/07/17
		 *the "else" segment resolve "IllegalStateException: The specified child already has a parent. You must call removeView() on the child's parent first."
		 */
		else
		{
			if (mRootView.getParent() != null)
			{
				((ViewGroup) mRootView.getParent()).removeView(mRootView);
			}
		}
		
		hideKeyboard();
		
		return mRootView;
	}
	
	public void setContentView(int iContentViewResourceId)
	{
		mContentViewResourceId = iContentViewResourceId;
	}
	
	/**
	 * Shows or hides a progress dialog, <B>IF the activity is of type {@link EWBaseActivity}</B>
	 */
	protected void trySetProgressDialog(boolean iShouldShow)
	{
		if(isVisible() || isResumed()) // For situation with parallels requests in diff fragments
		{
			if (isActivityIsOfTypeEwBaseActivity())
			{
				((EWBaseActivity) getActivity()).setProgressDialog(iShouldShow);
			}
		}
	}
	
	protected boolean isActivityIsOfTypeEwBaseActivity()
	{
		return getActivity() != null && getActivity() instanceof EWBaseActivity;
	}
	
	@Override
	public void onServiceConnected()
	{
	}
	
	protected boolean isAttachedToActivity()
	{
		return getActivity() != null;
	}
	
	public EWSSLService getService()
	{
		EWSSLService result = null;
		
		if (isActivityIsOfTypeEwBaseActivity())
		{
			result = ((EWBaseActivity) getActivity()).getService();
		}
		
		return result;
	}
	
	public boolean isServiceConnected()
	{
		boolean result = false;
		
		if (isActivityIsOfTypeEwBaseActivity())
		{
			result = ((EWBaseActivity) getActivity()).isServiceConnected();
		}
		
		return result;
	}
	
	/**
	 * @return true if event is consumed by fragment, else activity will handle event.
	 */
	public boolean onBackPressed()
	{
		return false;
	}
	
	public void pushScreenOpenEvent(String iScreenName)
	{
		((EWBaseActivity) getActivity()).pushScreenOpenEvent(iScreenName);
	}
	
	public String setFragmentTitle()
	{
		return null;
	}
	
	public String getFragmentTitle()
	{
		String fragmentTitle = setFragmentTitle();
		
		return TextUtils.isEmpty(fragmentTitle) ? "" : fragmentTitle;
	}
	
	public void tryClearFragments()
	{
		if (getActivity() instanceof EWBaseActivity)
		{
			((EWBaseActivity) getActivity()).getFragmentSwapper().clearFragments();
		}
	}
	
	public void tryPopFragment()
	{
		try
		{
			if (getActivity() != null)
			{
				if (getParentFragment() != null && getParentFragment() instanceof TabsBackStackManager.ITabsBackStackManagerHandler && !((TabsBackStackManager.ITabsBackStackManagerHandler) getParentFragment())
						.isFirstInTab())
				{
					((TabsBackStackManager.ITabsBackStackManagerHandler) getParentFragment()).popFragment();
				}
				else if (getActivity() instanceof TabsBackStackManager.ITabsBackStackManagerHandler)
				{
					((TabsBackStackManager.ITabsBackStackManagerHandler) getActivity()).popFragment();
				}
				else if (getActivity() instanceof EWBaseActivity)
				{
					int backStackEntryCount = getFragmentManager().getBackStackEntryCount();
					if (backStackEntryCount > 0)
					//					if (getParentFragment() != null)
					{
						((EWBaseActivity) getActivity()).getFragmentSwapper().popFragment();
					}
					else
					{
						getActivity().onBackPressed();
					}
				}
				
				if (getActivity() instanceof MyFlightsActivity)
				{
					((MyFlightsActivity) getActivity()).setFragmentsCount(getFragmentManager().getBackStackEntryCount());
				}
			}
		}
		catch (Exception iE)
		{
			iE.printStackTrace();
		}
	}
	
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
	}
	
	public static final Parcelable.Creator<EWBaseFragment> CREATOR = new Parcelable.Creator<EWBaseFragment>()
	{
		@Override
		public EWBaseFragment createFromParcel(Parcel source)
		{
			return null;
		}
		
		@Override
		public EWBaseFragment[] newArray(int size)
		{
			return new EWBaseFragment[size];
		}
	};
	
	private boolean isActivityImplementsElalScreensNavigator()
	{
		return getActivity() != null && getActivity() instanceof IElalScreensNavigator;
	}
	
	protected void tryGoToScreenById(final int iScreenId, final Bundle iBundle)
	{
		if (isActivityImplementsElalScreensNavigator())
		{
			((IElalScreensNavigator) getActivity()).goToScreenById(iScreenId, iBundle);
		}
	}
	
	protected void tryGoToScreenById(final int iScreenId, final Class iActivity, final Bundle iBundle)
	{
		if (isActivityImplementsElalScreensNavigator())
		{
			((IElalScreensNavigator) getActivity()).goToScreenById(iScreenId, iActivity, iBundle);
		}
	}
	
	protected void tryGoToWebFragment(final String iUrl)
	{
		if (isActivityImplementsElalScreensNavigator())
		{
			((IElalScreensNavigator) getActivity()).goToWebFragment(iUrl);
		}
	}
	
	protected void tryGoToFragmentByFragmentClass(Class<? extends EWBaseFragment> iFragmentClass, final Bundle iBundle, final boolean iAddToBackStack)
	{
		if (isActivityImplementsElalScreensNavigator())
		{
			((IElalScreensNavigator) getActivity()).goToFragmentByFragmentClass(iFragmentClass, iBundle, iAddToBackStack);
		}
	}
	
	@Override
	public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults)
	{
		if (this instanceof TabsBackStackManager.ITabsBackStackManagerHandler)
		{
			((TabsBackStackManager.ITabsBackStackManagerHandler) this).onTabRequestPermissionsResult(requestCode, permissions, grantResults);
		}
		else
		{
			super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		}
	}
	
	
	//-----------------------
	private ViewGroup mViewGroupRootLayout;
	
	private boolean keyboardListenersAttached = false;
	
	protected void onShowKeyboard(int keyboardHeight)
	{
	}
	
	protected void onHideKeyboard()
	{
	}
	
	private ViewTreeObserver.OnGlobalLayoutListener keyboardLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener()
	{
		@Override
		public void onGlobalLayout()
		{
			if (getActivity() != null && getActivity().getResources() != null)
			{
				// navigation bar height
				int navigationBarHeight = 0;
				
				int resourceId = getActivity().getResources().getIdentifier("navigation_bar_height", "dimen", "android");
				if (resourceId > 0)
				{
					navigationBarHeight = getActivity().getResources().getDimensionPixelSize(resourceId);
				}
				
				// status bar height
				int statusBarHeight = 0;
				resourceId = getActivity().getResources().getIdentifier("status_bar_height", "dimen", "android");
				if (resourceId > 0)
				{
					statusBarHeight = getActivity().getResources().getDimensionPixelSize(resourceId);
				}
				
				// display window size for the app layout
				Rect rect = new Rect();
				getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
				
				// screen height - (user app height + status + nav) ..... if non-zero, then there is a soft keyboard
				int keyboardHeight = mViewGroupRootLayout.getRootView().getHeight() - (statusBarHeight + navigationBarHeight + rect.height());
				
				if (keyboardHeight <= 0)
				{
					onHideKeyboard();
				}
				else
				{
					onShowKeyboard(keyboardHeight);
				}
			}
		}
	};
	
	protected void attachKeyboardListeners(ViewGroup iViewGroupRootLayout)
	{
		if (keyboardListenersAttached)
		{
			return;
		}
		
		mViewGroupRootLayout = iViewGroupRootLayout;
		iViewGroupRootLayout.getViewTreeObserver().addOnGlobalLayoutListener(keyboardLayoutListener);
		
		keyboardListenersAttached = true;
	}
	
	protected void hideKeyboard()
	{
		AppUtils.hideKeyboard(getActivity(), mRootView);
	}
	
	@Override
	public void onDestroyView()
	{
		if (keyboardListenersAttached)
		{
			mViewGroupRootLayout.getViewTreeObserver().removeOnGlobalLayoutListener(keyboardLayoutListener);
		}
		
		if (mRootView != null)
		{
			ViewGroup parentViewGroup = (ViewGroup) mRootView.getParent();

			if (parentViewGroup != null)
			{
				parentViewGroup.removeAllViews();
			}
		}
		
		super.onDestroyView();
	}
}
