package ui.adapters;

import android.content.res.Resources;
import android.support.annotation.DrawableRes;
import android.support.v4.widget.Space;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;

import global.UserData;
import il.co.ewave.elal.R;
import ui.activities.EWBaseActivity;
import ui.customWidgets.AnimatedExpandableListView;
import webServices.global.RequestStringBuilder;
import webServices.responses.getSideMenuItems.SideMenuCategory;
import webServices.responses.getSideMenuItems.SideMenuItem;

/**
 * Created with care by Shahar Ben-Moshe on 7/7/15.
 */
public class MainDrawerAdapter extends AnimatedExpandableListView.AnimatedExpandableListAdapter
{
	private LayoutInflater mInflater;
	private EWBaseActivity mEWBaseActivity;
	private ArrayList<SideMenuCategory> mSideMenuCategories, mPermanentSideMenuCategories;
	private View mHeaderView, mFooterView;
	private int mUnReadPushes;
	private boolean isHebrewLanguage;
	
	public MainDrawerAdapter(EWBaseActivity iEWBaseActivity)
	{
		this.mEWBaseActivity = iEWBaseActivity;
		this.mSideMenuCategories = new ArrayList<>();
		this.mInflater = LayoutInflater.from(mEWBaseActivity);
		isHebrewLanguage = UserData.getInstance().getLanguage().isHebrew();
	}
	
	@Override
	public int getGroupCount()
	{
		return mSideMenuCategories == null ? 0 : mSideMenuCategories.size();
	}
	
	@Override
	public int getRealChildrenCount(int groupPosition)
	{
		SideMenuCategory sideMenuCategory = getGroup(groupPosition);
		return (sideMenuCategory == null || sideMenuCategory.getSideMenuItems() == null) ? 0 : sideMenuCategory.getSideMenuItems().size();
	}
	
	@Override
	public SideMenuCategory getGroup(int groupPosition)
	{
		return mSideMenuCategories.get(groupPosition);
	}
	
	@Override
	public SideMenuItem getChild(int groupPosition, int childPosition)
	{
		return mSideMenuCategories.get(groupPosition).getSideMenuItems().get(childPosition);
	}
	
	@Override
	public long getGroupId(int groupPosition)
	{
		return groupPosition;
	}
	
	@Override
	public long getChildId(int groupPosition, int childPosition)
	{
		return childPosition;
	}
	
	@Override
	public boolean hasStableIds()
	{
		return false;
	}
	
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent)
	{
		GroupViewHolder vH;
		View view;
		SideMenuCategory currentSideMenuCategory = getGroup(groupPosition);
		
		if (convertView == null)
		{
			view = LayoutInflater.from(parent.getContext()).inflate(R.layout.side_menu_category_layout_cell, parent, false);
			vH = new GroupViewHolder(view);
			view.setTag(vH);
		}
		else
		{
			view = convertView;
			vH = (GroupViewHolder) view.getTag();
		}
		
		boolean isItCategoryWithoutSubcategories = currentSideMenuCategory.isConst();
		
		if (isItCategoryWithoutSubcategories)
		{
			vH.mTvGroupName.setVisibility(View.GONE);
			vH.mImgIcon.setVisibility(View.GONE);
			vH.mSpcSpace.setVisibility(View.GONE);
			vH.mImgGroupIndicator.setVisibility(View.GONE);
		}
		else
		{
			//ViewGroup arrow(indicator)
			vH.mImgGroupIndicator.setVisibility(View.VISIBLE);
			@DrawableRes int drawableRightLeft = isHebrewLanguage ? R.mipmap.arrow_small_left_white : R.mipmap.arrow_small_right_white;
			@DrawableRes int currentDrawable = isExpanded ? R.mipmap.arrow_small_down_white : drawableRightLeft;
			vH.mImgGroupIndicator.setImageResource(currentDrawable);
			
			//Cell background
//			vH.vCategoryCellRoot.setBackgroundColor(mEWBaseActivity.getResources().getColor(isExpanded ? R.color.black_opacity_30_side_menu : R.color.sideMenuDarkBlue));
			vH.vCategoryCellRoot.setBackgroundColor(mEWBaseActivity.getResources().getColor(R.color.sideMenuDarkBlue));
			
			//Group name
			vH.mTvGroupName.setVisibility(View.VISIBLE);
			
			if (isHebrewLanguage)
			{
				vH.mTvGroupName.setTextSize(TypedValue.COMPLEX_UNIT_PX, parent.getResources().getDimension(R.dimen.text_size_20sp));
			}
			
			vH.mTvGroupName.setText(currentSideMenuCategory.getCategoryName());
			
			//Category icon
			boolean isNeedToShowCategoryIcon = currentSideMenuCategory.getIconResourceId() > 0;
			
			if (isNeedToShowCategoryIcon)
			{
				vH.mImgIcon.setVisibility(View.VISIBLE);
				vH.mImgIcon.setImageResource(currentSideMenuCategory.getIconResourceId());
				vH.mSpcSpace.setVisibility(View.VISIBLE);
			}
			else
			{
				vH.mImgIcon.setVisibility(View.INVISIBLE);
				vH.mSpcSpace.setVisibility(View.INVISIBLE);
			}
		}
		
		return view;
	}
	
	@Override
	public View getRealChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent)
	{
		ChildViewHolder vH;
		View view;
		SideMenuItem currentSubCategory = getChild(groupPosition, childPosition);
		boolean isChildViewConstant = mSideMenuCategories.get(groupPosition).isConst();
		
		if (convertView == null)
		{
			view = mInflater.inflate(R.layout.side_menu_item_layout_cell, parent, false);
			
			vH = new ChildViewHolder(view);
			view.setTag(vH);
		}
		else
		{
			view = convertView;
			vH = (ChildViewHolder) view.getTag();
		}
		
		if (currentSubCategory.getIcon() != null && !TextUtils.isEmpty(currentSubCategory.getIcon()))
		{
			vH.mImgNetworkIcon.setVisibility(View.VISIBLE);
			vH.mImgIcon.setVisibility(View.GONE);
			String imageUrl = RequestStringBuilder.getSideMenuImageUrl(currentSubCategory.getIcon());
			vH.mImgNetworkIcon.setImageUrl(imageUrl, mEWBaseActivity.getService().getImageLoader());
		}
		else
		{
			vH.mImgNetworkIcon.setVisibility(View.GONE);
			vH.mImgIcon.setVisibility(isChildViewConstant ? View.GONE : View.VISIBLE);
		}
		
		
		LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) vH.mTvChildName.getLayoutParams();
		
		if (isChildViewConstant)
		{
			if (isHebrewLanguage)
			{
				vH.mTvChildName.setTextSize(TypedValue.COMPLEX_UNIT_PX, parent.getResources().getDimension(R.dimen.text_size_20sp));
			}
			else
			{
				vH.mTvChildName.setTextSize(TypedValue.COMPLEX_UNIT_PX, parent.getResources().getDimension(R.dimen.text_size_16sp));
			}
			params.setMargins(0, 50, 0, 50);
		}
		else
		{
			vH.mTvChildName.setTextSize(TypedValue.COMPLEX_UNIT_PX, parent.getResources().getDimension(R.dimen.text_size_16sp));
			params.setMargins(0, 20, 0, 20);
		}
		
		vH.mTvChildName.setLayoutParams(params);
		
		vH.mTvChildName.setText(currentSubCategory.getSideMenuName());
		
		Resources res = parent.getResources();
		vH.mVCellDivider.setBackgroundColor(isChildViewConstant ? res.getColor(R.color.divider_color) : res.getColor(R.color.black_opacity_30_side_menu));
		
		view.setBackgroundColor(parent.getResources().getColor(isChildViewConstant ? R.color.sideMenuDarkBlue : R.color.black_opacity_30_side_menu));
		
		return view;
	}
	
	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition)
	{
		return true;
	}
	
	@Override
	public boolean areAllItemsEnabled()
	{
		return true;
	}
	
	public void setSideMenuCategories(ArrayList<SideMenuCategory> iSideMenuCategories)
	{
		if (iSideMenuCategories != null)
		{
			mSideMenuCategories.clear();
			
			if (mPermanentSideMenuCategories != null)
			{
				mSideMenuCategories.addAll(mPermanentSideMenuCategories);
			}
			
			mSideMenuCategories.addAll(iSideMenuCategories);
		}
		this.notifyDataSetChanged();
	}
	
	public void setUnReadPushes(final int iUnReadPushes)
	{
		mUnReadPushes = iUnReadPushes;
	}
	
	public void notifyPushesIndicator()
	{
		View v = mHeaderView.findViewById(R.id.cl_side_menu_notification);
		TextView tvUnreadMessagesQuantity = (TextView) v.findViewById(R.id.tv_side_menu_notification_unread_messages_quantity);
		
		if (mUnReadPushes > 0)
		{
			tvUnreadMessagesQuantity.setVisibility(View.VISIBLE);
			tvUnreadMessagesQuantity.setText(String.valueOf(mUnReadPushes));
		}
		else
		{
			tvUnreadMessagesQuantity.setVisibility(View.GONE);
		}
	}
	
	public void setPermanentSideMenuCategories(final ArrayList<SideMenuCategory> iPermanentSideMenuCategories)
	{
		if (mPermanentSideMenuCategories == null)
		{
			mPermanentSideMenuCategories = new ArrayList<>();
		}
		else
		{
			mPermanentSideMenuCategories.clear();
		}
		mPermanentSideMenuCategories.addAll(iPermanentSideMenuCategories);
		this.notifyDataSetChanged();
	}
	
	public void expandPermanentGroups(final ExpandableListView iExpandableListView)
	{
		try
		{
			if (iExpandableListView != null/* && iExpandableListView.getAdapter() != null && iExpandableListView.getAdapter().equals(this)*/)
			{
				for (int i = 0 ; i < mSideMenuCategories.size() ; i++)
				{
					SideMenuCategory sideMenuCategory = mSideMenuCategories.get(i);
					
					if (sideMenuCategory != null && sideMenuCategory.isConst())
					{
						iExpandableListView.expandGroup(i);
					}
				}
			}
		}
		catch (Exception ignored)
		{
		}
	}
	
	public View setHeaderView(final View iHeaderView)
	{
		return mHeaderView = iHeaderView;
	}
	
	public View setFooterView(final View iFooterView)
	{
		return mFooterView = iFooterView;
	}
	
	public View getHeaderView()
	{
		return mHeaderView;
	}
	
	public View getFooterView()
	{
		return mFooterView;
	}
	
	private class GroupViewHolder
	{
		View vCategoryCellRoot;
		TextView mTvGroupName;
		ImageView mImgIcon;
		Space mSpcSpace;
		ImageView mImgGroupIndicator;
		
		GroupViewHolder(View iV)
		{
			vCategoryCellRoot = iV.findViewById(R.id.side_menu_category_cell_root);
			mTvGroupName = (TextView) iV.findViewById(R.id.tv_side_menu_category_name);
			mImgIcon = (ImageView) iV.findViewById(R.id.img_side_menu_category_icon);
			mSpcSpace = (Space) iV.findViewById(R.id.spc_sideMenuCategory_Space);
			mImgGroupIndicator = (ImageView) iV.findViewById(R.id.img_side_menu_category_group_indicator);
		}
	}
	
	private class ChildViewHolder
	{
		LinearLayout mLlRoot;
		ImageView mImgIcon;
		NetworkImageView mImgNetworkIcon;
		TextView mTvChildName;
		View mVCellDivider;
		
		ChildViewHolder(View iV)
		{
			mLlRoot = (LinearLayout) iV.findViewById(R.id.ll_subCategory_Root);
			mImgIcon = (ImageView) iV.findViewById(R.id.img_subCategory_Icon);
			mImgNetworkIcon = (NetworkImageView) iV.findViewById(R.id.img_subCategory_NetworkIcon);
			mTvChildName = (TextView) iV.findViewById(R.id.tv_subCategory_Name);
			mVCellDivider = iV.findViewById(R.id.v_side_menu_layout_cell_divider);
		}
	}
}
