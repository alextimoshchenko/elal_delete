package webServices.responses.getPnrByNum;

public class GetPnrByNumContentPNRFlights
{
	private String Destination;
	private String DestinationInfo;
	private String FlightDuration;
	private String ClassOfService;
	private String DestinationImage;
	private String Origination;
	private String Direction;
	private String CreateDate;
	private int Segment;
	private String LastModified;
	private String OriginationInfo;
	private String DepartTerminal;
	private String FlightStatus;
	private String ReferenceId;
	private String PNR;
	private String ArrivalDate;
	private String OriginationImage;
	private String FlightNumber;
	private String FlightCompany;
	private String DepartureDate;
	private String Carrier;
	private String AircraftType;
	
	public String getDestination()
	{
		return this.Destination;
	}
	
	public void setDestination(String Destination)
	{
		this.Destination = Destination;
	}
	
	public String getDestinationInfo()
	{
		return this.DestinationInfo;
	}
	
	public void setDestinationInfo(String DestinationInfo)
	{
		this.DestinationInfo = DestinationInfo;
	}
	
	public String getFlightDuration()
	{
		return this.FlightDuration;
	}
	
	public void setFlightDuration(String FlightDuration)
	{
		this.FlightDuration = FlightDuration;
	}
	
	public String getClassOfService()
	{
		return this.ClassOfService;
	}
	
	public void setClassOfService(String ClassOfService)
	{
		this.ClassOfService = ClassOfService;
	}
	
	public String getDestinationImage()
	{
		return this.DestinationImage;
	}
	
	public void setDestinationImage(String DestinationImage)
	{
		this.DestinationImage = DestinationImage;
	}
	
	public String getOrigination()
	{
		return this.Origination;
	}
	
	public void setOrigination(String Origination)
	{
		this.Origination = Origination;
	}
	
	public String getDirection()
	{
		return this.Direction;
	}
	
	public void setDirection(String Direction)
	{
		this.Direction = Direction;
	}
	
	public String getCreateDate()
	{
		return this.CreateDate;
	}
	
	public void setCreateDate(String CreateDate)
	{
		this.CreateDate = CreateDate;
	}
	
	public int getSegment()
	{
		return this.Segment;
	}
	
	public void setSegment(int Segment)
	{
		this.Segment = Segment;
	}
	
	public String getLastModified()
	{
		return this.LastModified;
	}
	
	public void setLastModified(String LastModified)
	{
		this.LastModified = LastModified;
	}
	
	public String getOriginationInfo()
	{
		return this.OriginationInfo;
	}
	
	public void setOriginationInfo(String OriginationInfo)
	{
		this.OriginationInfo = OriginationInfo;
	}
	
	public String getDepartTerminal()
	{
		return this.DepartTerminal;
	}
	
	public void setDepartTerminal(String DepartTerminal)
	{
		this.DepartTerminal = DepartTerminal;
	}
	
	public String getFlightStatus()
	{
		return this.FlightStatus;
	}
	
	public void setFlightStatus(String FlightStatus)
	{
		this.FlightStatus = FlightStatus;
	}
	
	public String getReferenceId()
	{
		return this.ReferenceId;
	}
	
	public void setReferenceId(String ReferenceId)
	{
		this.ReferenceId = ReferenceId;
	}
	
	public String getPNR()
	{
		return this.PNR;
	}
	
	public void setPNR(String PNR)
	{
		this.PNR = PNR;
	}
	
	public String getArrivalDate()
	{
		return this.ArrivalDate;
	}
	
	public void setArrivalDate(String ArrivalDate)
	{
		this.ArrivalDate = ArrivalDate;
	}
	
	public String getOriginationImage()
	{
		return this.OriginationImage;
	}
	
	public void setOriginationImage(String OriginationImage)
	{
		this.OriginationImage = OriginationImage;
	}
	
	public String getFlightNumber()
	{
		return this.FlightNumber;
	}
	
	public void setFlightNumber(String FlightNumber)
	{
		this.FlightNumber = FlightNumber;
	}
	
	public String getFlightCompany()
	{
		return this.FlightCompany;
	}
	
	public void setFlightCompany(String FlightCompany)
	{
		this.FlightCompany = FlightCompany;
	}
	
	public String getDepartureDate()
	{
		return this.DepartureDate;
	}
	
	public void setDepartureDate(String DepartureDate)
	{
		this.DepartureDate = DepartureDate;
	}
	
	public String getCarrier()
	{
		return this.Carrier;
	}
	
	public void setCarrier(String Carrier)
	{
		this.Carrier = Carrier;
	}
	
	public String getAircraftType()
	{
		return this.AircraftType;
	}
	
	public void setAircraftType(String AircraftType)
	{
		this.AircraftType = AircraftType;
	}
}
