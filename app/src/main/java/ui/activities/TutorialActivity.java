package ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RadioGroup;

import com.google.android.gms.tagmanager.DataLayer;

import java.util.ArrayList;

import global.IntentExtras;
import global.UserData;
import il.co.ewave.elal.R;
import utils.global.AppUtils;
import webServices.responses.promo.Promo;

/**
 * Created with care by Shahar Ben-Moshe on 05/08/15.
 */
public class TutorialActivity extends EWBaseActivity
{
	private int mCurrentPosition;
	
	public static void tryStartTutorialActivity(SplashActivity iActivity, Promo iPromoData)
	{
		Intent intent = new Intent(iActivity, TutorialActivity.class);
		intent.putExtra(IntentExtras.PROMO_DATA, iPromoData);
		iActivity.startActivity(intent);
	}
	
	private ImageView mImgSkip;
	private RadioGroup mRgIndicator;
	private ViewPager mVpContainer;
	private Promo mPromoData;
	
	@Override
	protected void initScreen()
	{
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_tutorial_layout);
		
		initReference();
		setListeners();
		getIntentExtras();
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		setViewPagerRTL();
	}
	
	private void initReference()
	{
		mImgSkip = (ImageView) findViewById(R.id.img_tutorial_Skip);
		mRgIndicator = (RadioGroup) findViewById(R.id.rg_tutorial_Indicator);
		mVpContainer = (ViewPager) findViewById(R.id.vp_tutorial_Container);
	}
	
	private void setListeners()
	{
		mImgSkip.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				// TODO: 15/12/16 Shahar GA location.
				//                TagManagerHandler.getInstance().tryPushEvent(GtmStrings.PUSH_BUTTON, DataLayer.mapOf(GtmStrings.BUTTON_NAME, GtmStrings.getTutorialSkipInStepWithLanguage(mVpContainer.getCurrentItem())));
				handleSkipOrClose();
				continueToPromoActivity();
			}
		});
		
		mVpContainer.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
		{
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
			{
			}
			
			@Override
			public void onPageSelected(int position)
			{
				//event 116
				pushCustomEvent(DataLayer.mapOf("Category", "Tutelary", "Action", position, "Label", "Skip"));
				
				//                if (position == 0)
				//                {
				//                    mRgIndicator.setVisibility(View.INVISIBLE);
				//                }
				//                else
				//                {
				//                    mRgIndicator.setVisibility(View.VISIBLE);
				//
				//                    ((RadioButton) mRgIndicator.getChildAt(position - 1)).setChecked(true);
				//                }
				//
				//                mImgSkip.setVisibility((position == mRgIndicator.getChildCount()) ? View.GONE : View.VISIBLE);
				
				// TODO: 15/12/16 Shahar GA location.
				//                TagManagerHandler.getInstance().tryPushEvent(GtmStrings.OPEN_SCREEN, DataLayer.mapOf(GtmStrings.SCREEN_NAME, GtmStrings.getTutorialStepWithLanguage(position)));
			}
			
			@Override
			public void onPageScrollStateChanged(int state)
			{
			}
		});
	}
	
	private void handleSkipOrClose()
	{
		if (AppUtils.isDefaultLocaleRTL())
		{
			if (mCurrentPosition == 0)
			{
				UserData.getInstance().setIsFinishTutorial(true);
			}
		}
		else
		{
			if (mCurrentPosition == (mVpContainer.getAdapter().getCount() - 1))
			{
				UserData.getInstance().setIsFinishTutorial(true);
			}
		}
		
		//event 116
		pushCustomEvent(DataLayer.mapOf("Category", "Tutelary", "Action", "Screen No", "Label", "Skip"));
	}
	
	private void getIntentExtras()
	{
		Bundle bundle = getIntent().getExtras();
		
		if (bundle != null)
		{
			mPromoData = bundle.getParcelable(IntentExtras.PROMO_DATA);
		}
	}
	
	private void setViewPagerRTL()
	{
		final ArrayList<ImageView> images = new ArrayList<>();
		
		try
		{
			if (AppUtils.isDefaultLocaleRTL())
			{
				//				images.add(generateImageViewWithResource(R.mipmap.tutorial_he_12));
				//				images.add(generateImageViewWithResource(R.mipmap.tutorial_he_11));
				//				images.add(generateImageViewWithResource(R.mipmap.tutorial_he_10));
				//				images.add(generateImageViewWithResource(R.mipmap.tutorial_he_9));
				//				images.add(generateImageViewWithResource(R.mipmap.tutorial_he_8));
				//				images.add(generateImageViewWithResource(R.mipmap.tutorial_he_7));
				//				images.add(generateImageViewWithResource(R.mipmap.tutorial_he_6));
				images.add(generateImageViewWithResource(R.mipmap.tutorial_he_5));
				images.add(generateImageViewWithResource(R.mipmap.tutorial_he_4));
				images.add(generateImageViewWithResource(R.mipmap.tutorial_he_3));
				images.add(generateImageViewWithResource(R.mipmap.tutorial_he_2));
				images.add(generateImageViewWithResource(R.mipmap.tutorial_he_1));
			}
			else
			{
				images.add(generateImageViewWithResource(R.mipmap.tutorial_en_1));
				images.add(generateImageViewWithResource(R.mipmap.tutorial_en_2));
				images.add(generateImageViewWithResource(R.mipmap.tutorial_en_3));
				images.add(generateImageViewWithResource(R.mipmap.tutorial_en_4));
				images.add(generateImageViewWithResource(R.mipmap.tutorial_en_5));
				//				images.add(generateImageViewWithResource(R.mipmap.tutorial_en_6));
				//				images.add(generateImageViewWithResource(R.mipmap.tutorial_en_7));
				//				images.add(generateImageViewWithResource(R.mipmap.tutorial_en_8));
				//				images.add(generateImageViewWithResource(R.mipmap.tutorial_en_9));
				//				images.add(generateImageViewWithResource(R.mipmap.tutorial_en_10));
				//				images.add(generateImageViewWithResource(R.mipmap.tutorial_en_11));
				//				images.add(generateImageViewWithResource(R.mipmap.tutorial_en_12));
			}
			
			images.get(AppUtils.isDefaultLocaleRTL() ? 0 : images.size() - 1).setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					UserData.getInstance().setSawTutorial();
					UserData.getInstance().saveUserData();
					continueToPromoActivity();
				}
			});
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		mVpContainer.setAdapter(new PagerAdapter()
		{
			@Override
			public int getCount()
			{
				return images.size();
			}
			
			@Override
			public boolean isViewFromObject(View view, Object o)
			{
				return o == view;
			}
			
			@Override
			public Object instantiateItem(ViewGroup container, int position)
			{
				mCurrentPosition = position;
				container.addView(images.get(position));
				return images.get(position);
			}
			
			@Override
			public void destroyItem(ViewGroup container, int position, Object object)
			{
				container.removeView(images.get(position));
			}
		});
		
		mVpContainer.setCurrentItem(AppUtils.isDefaultLocaleRTL() ? images.size() - 1 : 0);
		mRgIndicator.setVisibility(View.INVISIBLE);
		
		// TODO: 15/12/16 Shahar GA location.
		//        TagManagerHandler.getInstance().tryPushEvent(GtmStrings.OPEN_SCREEN, DataLayer.mapOf(GtmStrings.SCREEN_NAME, GtmStrings.getTutorialStepWithLanguage(0)));
	}
	
	private void continueToNeededActivity()
	{
		if (UserData.getInstance().shouldShowRegistrationActivity())
		{
			continueToRegistrationActivity();
		}
		else
		{
			continueToPromoActivity();
		}
	}
	
	private void continueToRegistrationActivity()
	{
		LoginActivity.tryStartLoginActivity(this, mPromoData);
		this.finish();
	}
	
	private void continueToPromoActivity()
	{
		PromoActivity.tryStartPromoActivity(this, mPromoData);
		this.finish();
	}
	
	private ImageView generateImageViewWithResource(int iDrawableResourceId)
	{
		ImageView imageView = null;
		
		if (iDrawableResourceId > 0)
		{
			imageView = new ImageView(this);
			imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
			imageView.setImageResource(iDrawableResourceId);
			imageView.setScaleType(ImageView.ScaleType.FIT_XY);
		}
		
		return imageView;
	}
	
	@Override
	protected void onServiceConnected()
	{
	}
}
