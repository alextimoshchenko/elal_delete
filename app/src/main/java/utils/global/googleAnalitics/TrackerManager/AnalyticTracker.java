package utils.global.googleAnalitics.TrackerManager;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;

import global.ElAlApplication;

/**
 * Created with care by Alexey.T on 28/08/2017.
 * <p>
 * TODO: Add a class header comment!
 */
public class AnalyticTracker
{
	private Tracker mTracker;
	
	// The following line should be changed to include the correct property id.
	private static final String PROPERTY_ID = "UA-XXXXX-Y";
	private HashMap<TrackerName, Tracker> mTrackers = new HashMap<>();
	
	private synchronized Tracker getTracker(TrackerName trackerId)
	{
		if (!mTrackers.containsKey(trackerId))
		{
			GoogleAnalytics analytics = GoogleAnalytics.getInstance(ElAlApplication.getInstance());
			
			Tracker t = analytics.newTracker(PROPERTY_ID);
			
			mTrackers.put(trackerId, t);
			
		}
		
		return mTrackers.get(trackerId);
	}
	
	public void sendNotFatalException(String iDescription)
	{
		sendException(iDescription, false);
	}
	
	private void sendException(String iDescription, boolean iIsFatal)
	{
		Tracker tracker = getTracker(TrackerName.APP_TRACKER);
		tracker.send(new HitBuilders.ExceptionBuilder().setDescription(iDescription).setFatal(iIsFatal).build());
	}
	
	public enum TrackerName
	{
		// Tracker used only in this app.
		APP_TRACKER,
		//		// Tracker used by all the apps from a company. eg: roll-up tracking.
		//		GLOBAL_TRACKER,
		//		// Tracker used by all ecommerce transactions from a company.
		//		ECOMMERCE_TRACKER
	}
}
