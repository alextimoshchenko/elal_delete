package interfaces;

import utils.global.dbObjects.Document;

/**
 * Created with care by Shahar Ben-Moshe on 08/03/17.
 */

public interface IDocumentsForTypeClickListener
{
	void onImageClick(Document iCurrentDocument);
	
	void onRemoveClick(Document iCurrentDocument, int iAdapterPosition);
}
