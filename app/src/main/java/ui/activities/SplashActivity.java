package ui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;

import global.AppData;
import global.ElAlApplication;
import global.IntentExtras;
import global.UserData;
import il.co.ewave.elal.R;
import utils.errors.ErrorsHandler;
import utils.global.ActivityLifecycleHandler;
import utils.global.AppUtils;
import utils.global.ElalPreferenceUtils;
import utils.global.FlightUtils;
import webServices.controllers.LoginController;
import webServices.controllers.MainController;
import webServices.global.RequestStringBuilder;
import webServices.requests.RequestGetAppData;
import webServices.responses.ResponseCheckAppVersion;
import webServices.responses.ResponseGetAppData;
import webServices.responses.ResponseGetHomeScreenContent;
import webServices.responses.ResponseGetRepresentationByIp;
import webServices.responses.ResponsePromo;
import webServices.responses.ResponseUserLogin;
import webServices.responses.promo.Promo;
import webServices.responses.responseGetUserActivePNRs.UserActivePnr;

/**
 * Created with care by Shahar Ben-Moshe on 12/12/16.
 */

public class SplashActivity extends EWBaseUserActivity
{
	private String TAG = SplashActivity.class.getSimpleName();
	
	private static final long SPLASH_DELAY = 3000;
	
	private TextView mTvDebug;
	private TextView mTvVersion;
	private TextView mTvVersionCode;
	private long mInitTime;
	private Promo mPromoData;
	private boolean mIsFromNotification;
	
	
	@Override
	protected void initScreen()
	{
		setContentView(R.layout.activity_splash_layout);
		setGoogleTagManager();
		initTimer();
		initReference();
		//		handleNotificationClick();
		setGui();
		//		runGIF();
	}
	
	@Override
	protected void setCurrentActivityModuleId()
	{
		
	}
	
	//	private void runGIF()
	//	{
	//		final ImageView animImageView = (ImageView) findViewById(R.id.iv_splash);
	//		animImageView.setBackgroundResource(R.drawable.splash_animation);
	//		animImageView.post(new Runnable()
	//		{
	//			@Override
	//			public void run()
	//			{
	//				AnimationDrawable frameAnimation = (AnimationDrawable) animImageView.getBackground();
	//				frameAnimation.start();
	//			}
	//		});
	//	}
	
	private void initTimer()
	{
		mInitTime = System.currentTimeMillis();
	}
	
	private void initReference()
	{
		mTvDebug = (TextView) findViewById(R.id.tv_splash_Debug);
		mTvVersion = (TextView) findViewById(R.id.tv_splash_Version);
		mTvVersion.setVisibility(View.VISIBLE);
		mTvVersionCode = (TextView) findViewById(R.id.tv_splash_versionCode);
		mTvVersionCode.setVisibility(View.GONE);
		
	}
	
	private void handleNotificationClick()
	{
		if (getIntent().getExtras() != null)
		{
			//				todo avishay 16/11/2017
			mIsFromNotification = getIntent().getExtras().getBoolean(IntentExtras.IS_FROM_PUSH_NOTIFICATION, false);
			
			if (mIsFromNotification && ElAlApplication.isAppWasInBackground())
			{
				ContinueToMainActivity();
			}
			
			else
			{
				if (isServiceConnected())
				{
					Response.Listener<ResponseUserLogin> listener = new Response.Listener<ResponseUserLogin>()
					{
						@Override
						public void onResponse(final ResponseUserLogin response)
						{
							if (response != null && response.getContent() != null)
							{
								UserData.getInstance().setUserId(response.getContent().getUserID());
								UserData.getInstance().setGuestUserId(response.getContent().getUserID());
								UserData.getInstance().setUserObject(response.getContent().getUserObject());
								ElAlApplication.getInstance().setUserDefinedLanguage();
								
								ContinueToMainActivity();
							}
							else
							{
								// TODO: 05/01/17 Shahar handle case.
							}
						}
					};
					Response.ErrorListener errorListener = new Response.ErrorListener()
					{
						@Override
						public void onErrorResponse(final VolleyError error)
						{
							// TODO: 04/01/17 Shahar handle error somehow?
							Log.e(TAG, "performLogin");
							//							ErrorsHandler.tryShowServiceErrorDialog(error, SplashActivity.this);
							showServiceUnavailableDialog();
						}
					};
					
					if (UserData.getInstance().isUserLoginDataAvailable())
					{
						getService().getController(LoginController.class).LoginAsUser(UserData.getInstance().getEmail(), UserData.getInstance().getPassword(), true, listener, errorListener);
					}
					else
					{
						getService().getController(LoginController.class).LoginAsGuest(listener, errorListener);
					}
				}
				
				else
				{
					ContinueToMainActivity();
				}
				
			}
		}
	}
	
	private void setGui()
	{
		mTvDebug.setText(RequestStringBuilder.getEnvironmentName());
		mTvVersion.setText(String.format("v%s", String.valueOf(AppUtils.getApplicationVersion(this))));
		mTvVersionCode.setText(String.format("v%s", String.valueOf(AppUtils.getApplicationVersionCode(this))));
	}
	
	@Override
	protected void onServiceConnected()
	{
		
		if (getIntent().getExtras() != null)
		{
			if (mIsFromNotification = getIntent().getExtras().getBoolean(IntentExtras.IS_FROM_PUSH_NOTIFICATION, false))
			{
				handleNotificationClick();
			}
			else
			{
				tryCheckVersion();
			}
		}
		else
		{
			tryCheckVersion();
		}
		
	}
	
	private void tryCheckVersion()
	{
		if (isNetworkAvailable())
		{
			checkAppVersion();
		}
		else
		{
			AlertDialog dialog = AppUtils.createSimpleMessageDialog(this, getResources().getString(R.string.error_L1_no_internet_connection), getResources().getString(R.string.try_again), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(final DialogInterface dialog, final int which)
				{
					tryCheckVersion();
				}
			}, null, null);
			dialog.setCancelable(false);
			dialog.show();
		}
	}
	
	private void checkAppVersion()
	{
		if (isServiceConnected())
		{
			getService().getController(LoginController.class).CheckAppVersion(new Response.Listener<ResponseCheckAppVersion>()
			{
				@Override
				public void onResponse(final ResponseCheckAppVersion response)
				{
					doOnCheckVersionResponse(response);
				}
			}, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError error)
				{
					loadPromo();
				}
			});
		}
	}
	
	private void doOnCheckVersionResponse(final ResponseCheckAppVersion iResponse)
	{
		if (iResponse != null && iResponse.getContent() != null && iResponse.getContent().getAppVer() > AppUtils.getApplicationVersion(this))
		{
			AlertDialog dialog = AppUtils.createSimpleMessageDialog(this, getString(R.string.app_version_update), getString(R.string.download), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					AppUtils.openAppInMarket(SplashActivity.this);
				}
			}, getString(R.string.cancel), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					if (iResponse.getContent().isMandatory())
					{
						finish();
					}
					else
					{
						loadPromo();
					}
				}
			});
			
			dialog.setCancelable(false);
			dialog.show();
		}
		else
		{
			loadPromo();
		}
	}
	
	private void loadPromo()
	{
		if (isServiceConnected())
		{
			getService().getController(LoginController.class).getPromoByUdid(new Response.Listener<ResponsePromo>()
			{
				@Override
				public void onResponse(final ResponsePromo response)
				{
					if (response != null && response.getContent() != null && !TextUtils.isEmpty(response.getContent().getImage()))
					{
						mPromoData = response.getContent();
					}
					
					getAppData();
				}
			}, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(VolleyError error)
				{
					getAppData();
				}
			});
		}
	}
	
	private void getAppData()
	{
		if (isServiceConnected())
		{
			getService().getController(LoginController.class).getAppData(new RequestGetAppData(), new Response.Listener<ResponseGetAppData>()
			{
				@Override
				public void onResponse(final ResponseGetAppData iResponse)
				{
					AppData.getInstance().setAppData(iResponse);
					getRepresentationByIp();
				}
			}, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError error)
				{
					// TODO: 04/01/17 Shahar handle error somehow?
					
					// TODO: 11/07/17 avishay dialog
					showServiceUnavailableDialog();
					
				}
			});
		}
	}
	
	private void getRepresentationByIp()
	{
		if (isServiceConnected())
		{
			getService().getController(LoginController.class).GetRepresentationByIp(new Response.Listener<ResponseGetRepresentationByIp>()
			{
				@Override
				public void onResponse(final ResponseGetRepresentationByIp iResponse)
				{
					if (iResponse != null && iResponse.getContent() != null && iResponse.getContent().size() > 0)
					{
						UserData.getInstance().setUserRepresentation(iResponse.getContent().get(0).getRepresentationId());
					}
					
					continueToNeededActivity();
				}
			}, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError error)
				{
					// TODO: 04/01/17 Shahar handle error somehow?
					//TODO avishay 13/06/17 show popup to try again!
					Log.e(TAG, "onErrorResponse");
					
					showServiceUnavailableDialog();
					
				}
			});
		}
	}
	
	private void showServiceUnavailableDialog()
	{
		AlertDialog dialog = AppUtils.createSimpleMessageDialog(SplashActivity.this, getResources().getString(R.string.error_L2_no_response), getResources().getString(R.string.close), null, null, null);
		dialog.setCancelable(false);
		dialog.show();
	}
	
	private void continueToNeededActivity()
	{
		long delta = System.currentTimeMillis() - mInitTime;
		delta = SPLASH_DELAY - delta;
		delta = delta < 0 ? 0 : delta;
		
		new Handler().postDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				if (!mIsFromNotification)
				{
					UserData.getInstance().updateAndSaveBootCount();
					
					performLogin();
				}
			}
		}, delta);
	}
	
	private void performLogin()
	{
		if (isServiceConnected())
		{
			Response.Listener<ResponseUserLogin> listener = new Response.Listener<ResponseUserLogin>()
			{
				@Override
				public void onResponse(final ResponseUserLogin response)
				{
					if (response != null && response.getContent() != null)
					{
						UserData.getInstance().setUserId(response.getContent().getUserID());
						UserData.getInstance().setGuestUserId(response.getContent().getUserID());
						UserData.getInstance().setUserObject(response.getContent().getUserObject());
						ElAlApplication.getInstance().setUserDefinedLanguage();
						
						getService().getController(MainController.class).getHomeScreenContent(new Response.Listener<ResponseGetHomeScreenContent>()
						{
							@Override
							public void onResponse(final ResponseGetHomeScreenContent iResponse)
							{
								if (iResponse != null && iResponse.getContent() != null)
								{
									AppData.getInstance().setHomeItems(iResponse.getContent());
								}
							}
						}, null);
						
						
						String pntForNext48Hours = response.getContent().getPNRNum();
						if (pntForNext48Hours != null && !TextUtils.isEmpty(pntForNext48Hours))
						{
							ContinueToMyFlightActivity(pntForNext48Hours);
						}
						else
						{
							setProgressDialog(false);
							continueToTutorialoOrPromo();
						}
						
					}
					else
					{
						// TODO: 05/01/17 Shahar handle case.
					}
				}
			};
			Response.ErrorListener errorListener = new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError error)
				{
					// TODO: 04/01/17 Shahar handle error somehow?
					Log.e(TAG, "performLogin");
					//					ErrorsHandler.tryShowServiceErrorDialog(error, SplashActivity.this);
					showServiceUnavailableDialog();
				}
			};
			
			if (UserData.getInstance().isUserLoginDataAvailable())
			{
				getService().getController(LoginController.class).LoginAsUser(UserData.getInstance().getEmail(), UserData.getInstance().getPassword(), true, listener, errorListener);
			}
			else
			{
				getService().getController(LoginController.class).LoginAsGuest(listener, errorListener);
			}
		}
	}
	
	private void continueToTutorialoOrPromo()
	{
		if (	/*ElalPreferenceUtils.getSharedPreferenceBooleanOrFalse(ElAlApplication.getInstance(), ElalPreferenceUtils.IS_FINISH_TUTORIAL)*/
				ElalPreferenceUtils.getSharedPreferenceBooleanOrFalse(ElAlApplication.getInstance(), "isFinishTutorial") || UserData.getInstance().isSawTutorial())
		{
			continueToPromoActivity();
		}
		else
		{
			continueToTutorialActivity();
		}
	}
	
	
	private void handleResponseGetUserActivePNRs(ArrayList<UserActivePnr> iUserActivePnrsList)
	{
		if (iUserActivePnrsList.isEmpty()) // user has no flights
		{
			continueToTutorialoOrPromo();
			//			continueToPromoActivity();
		}
		else
		{
			//check if there is an active flight in 24h from now
			UserActivePnr flight = FlightUtils.getActiveFlight(iUserActivePnrsList);
			//			if (flight != null)
			//			{
			//				//check if there is an active flight in 48h from now
			//				flight = FlightUtils.getActiveFlightForNext48Houres(iUserActivePnrsList);
			//			}
			//
			
			if (flight != null)
			{
				ContinueToMyFlightActivity(flight.getPnr());
			}
			else
			{
				continueToTutorialoOrPromo();
				//				continueToPromoActivity();
			}
		}
	}
	
	private void ContinueToMyFlightActivity(String iPnr)
	{
		Intent intent = new Intent(SplashActivity.this, MyFlightsActivity.class);
		//		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra(IntentExtras.USER_ACTIVE_PNR, iPnr);
		startActivity(intent);
		finish();
		
		
		//		Bundle bndle = new Bundle();
		//		bndle.putString(IntentExtras.USER_ACTIVE_PNR, iPnr);
		//		goToScreenById(eInAppNavigation.MyFlights.getNavigationId(), bndle);
		//
		//		finish();
	}
	
	
	private void ContinueToMainActivity()
	{
		long delta = 0;
		if (!ActivityLifecycleHandler.isApplicationVisible())
		{
			delta = System.currentTimeMillis() - mInitTime;
			delta = SPLASH_DELAY - delta;
			delta = delta < 0 ? 0 : delta;
		}
		
		new Handler().postDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				Intent intent = new Intent(SplashActivity.this, MainActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtras(getIntent().getExtras());
				startActivity(intent);
				finish();
			}
		}, delta);
	}
	
	private void continueToLoginActivity()
	{
		LoginActivity.tryStartLoginActivity(this, mPromoData);
		this.finish();
	}
	
	private void continueToPromoActivity()
	{
		PromoActivity.tryStartPromoActivity(this, mPromoData);
		this.finish();
	}
	
	private void continueToTutorialActivity()
	{
		UserData.getInstance().addSawTutorialCount();
		UserData.getInstance().saveUserData();
		
		TutorialActivity.tryStartTutorialActivity(this, mPromoData);
		this.finish();
	}
	
	@Override
	public void onBackPressed()
	{
		//		do nothing...
	}
}
