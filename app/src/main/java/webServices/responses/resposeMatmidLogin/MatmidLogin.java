package webServices.responses.resposeMatmidLogin;

import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Date;

import il.co.ewave.elal.R;
import webServices.responses.responseUserLogin.UserObject;

/**
 * Created with care by Shahar Ben-Moshe on 25/05/17.
 */

public class MatmidLogin
{
	@JsonProperty("UserID")
	private int mUserID;
	@JsonProperty("FirstName")
	private String mFirstName;
	@JsonProperty("LastName")
	private String mLastName;
	@JsonProperty("FirstNameHeb")
	private String mFirstNameHeb;
	@JsonProperty("LastNameHeb")
	private String mLastNameHeb;
	@JsonProperty("FirstNameEng")
	private String mFirstNameEng;
	@JsonProperty("LastNameEng")
	private String mLastNameEng;
	@JsonProperty("SessionID")
	private String mSessionID;
	@JsonProperty("CreatedDate")
	private String mCreatedDate;
	@JsonProperty("DOB")
	private Date mDateOfBirth;
	@JsonProperty("Email")
	private String mEmail;
	@JsonProperty("FamilyMembers")
	private ArrayList<FamilyMember> mFamilyMembers;
	@JsonProperty("IDNumber")
	private String mIdNumber;
	@JsonProperty("IsFirstLogin")
	private boolean mIsFirstLogin;
	@JsonProperty("IsGuest")
	private boolean mIsGuest;
	@JsonProperty("LanguageID")
	private String mLanguageID;
	@JsonProperty("MatmidMemberID")
	private String mMatmidMemberID;
	@JsonProperty("ModifiedDate")
	private String mModifiedDate;
	@JsonProperty("PassportExpiration")
	private Date mPassportExpiration;
	@JsonProperty("PassportNum")
	private String mPassportNum;
	@JsonProperty("PasswordGUID")
	private String mPasswordGUID;
	@JsonProperty("PhoneNumber")
	private String mPhoneNumber;
//	@JsonProperty("CountryCode")
//	private String mCountryCode;
	@JsonProperty("PushFlightsInfo")
	private boolean mPushFlightsInfo;
	@JsonProperty("PushOffersInfo")
	private boolean mPushOffersInfo;
	@JsonProperty("RepresentationID")
	private int mRepresentationID;
	@JsonProperty("Title")
	private String mTitle;
	@JsonProperty("UserPassword")
	private String mUserPassword;
	@JsonProperty("MatmidPoints")
	private String mMatmidPoints;
	@JsonProperty("MatmidTypeID")
	private int mMatmidTypeID;
	@JsonProperty("IsMatmidFirstLogin")
	private boolean mIsMatmidFirstLogin;
	@JsonProperty("SMSession")
	private String mSMSession;
	@JsonProperty("MatmidErrorCode")
	private int mMatmidErrorCode;
	@JsonProperty("GroupPNRs")
	private ArrayList<String> mGroupPNRs;
	@JsonProperty("MatmidCountryCellCode")
	private String mMatmidCountryCellCode;
	
	@JsonProperty("PNRNum") // returns PNR num if there is a flight coming next 48 hours
	private String mPNRNum;
	
	
	private MatmidLogin()
	{
	}
	
	public enum eMatmidType
	{
		NO_FLY_CARD(1, R.mipmap.matmid3),
		FLY_CARD(2, R.mipmap.matmid1),
		FLY_CARD_PREMIUM(3, R.mipmap.matmid2);
		
		private final int mMatmidTypeId;
		private final int mIconResourceId;
		
		eMatmidType(final int iMatmidTypeId, final int iIconResourceId)
		{
			mMatmidTypeId = iMatmidTypeId;
			mIconResourceId = iIconResourceId;
		}
		
		public static eMatmidType getMatmidTypeById(int iDocumentTypeId)
		{
			eMatmidType result = null;
			
			for (eMatmidType eMatmidType : values())
			{
				if (eMatmidType.mMatmidTypeId == iDocumentTypeId)
				{
					result = eMatmidType;
					break;
				}
			}
			return result;
		}
		
		public int getDocumentTypeId()
		{
			return mMatmidTypeId;
		}
		
		public int getIconResourceId()
		{
			return mIconResourceId;
		}
		
		public static int getIconResourceIdByMatmidTypeId(int iMatmidTypeId)
		{
			
			if (iMatmidTypeId == FLY_CARD.mMatmidTypeId)
			{
				return FLY_CARD.getIconResourceId();
			}
			else if (iMatmidTypeId == FLY_CARD_PREMIUM.mMatmidTypeId)
			{
				return FLY_CARD_PREMIUM.getIconResourceId();
			}
			else
			{ //default: NO_FLY_CARD
				return NO_FLY_CARD.getIconResourceId();
			}
		}
		
	}
	
	public int getUserID()
	{
		return mUserID;
	}
	
	public String getLanguageID()
	{
		return mLanguageID;
	}
	
	public UserObject getUserObject()
	{
		String firstName, lastName;
		if (!TextUtils.isEmpty(mFirstNameEng))
		{
			firstName = mFirstNameEng.substring(0, 1).toUpperCase() + mFirstNameEng.substring(1).toLowerCase();
		}
		else
		{
			firstName = mFirstName;
		}
		if (!TextUtils.isEmpty(mLastNameEng))
		{
			lastName = mLastNameEng;
		}
		else
		{
			lastName = mLastName;
		}
		
		
		return new UserObject(mUserID, mPhoneNumber, mMatmidCountryCellCode, mEmail, mUserPassword, mTitle, mFirstNameHeb, mLastNameHeb, firstName, lastName, "", "", mDateOfBirth, mPassportNum, mPassportExpiration, mMatmidMemberID, mIdNumber, null, mLanguageID, mRepresentationID, mModifiedDate, mCreatedDate,
				mIsGuest,
				mPasswordGUID, mPushFlightsInfo, mPushOffersInfo, mSessionID, mMatmidPoints, mMatmidTypeID, mIsMatmidFirstLogin);
	}
	
	public String getSessionID()
	{
		return mSessionID;
	}
	
	public int getMatmidErrorCode()
	{
		return mMatmidErrorCode;
	}
	
	public ArrayList<String> getGroupPNRs()
	{
		return mGroupPNRs;
	}
	
	public String getmPNRNum()
	{
		return mPNRNum;
	}
	
	public void setmPNRNum(String mPNRNum)
	{
		this.mPNRNum = mPNRNum;
	}
	
	public String getmSMSession()
	{
		return mSMSession;
	}
	
	public String getMatmidCountryCellCode()
	{
		return mMatmidCountryCellCode;
	}
}
