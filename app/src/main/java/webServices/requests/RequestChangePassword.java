package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Avishay.Peretz on 21/06/2017.
 */

public class RequestChangePassword
{
	@JsonProperty("UserID")
	private int mUserID;
	
	@JsonProperty("CurrentPassword")
	private String mCurrentPassword;
	
	@JsonProperty("NewPassword")
	private String mNewPassword;
	
	public RequestChangePassword(final int iUserID, final String iCurrentPassword, final String iNewPassword)
	{
		mUserID = iUserID;
		mCurrentPassword = iCurrentPassword;
		mNewPassword = iNewPassword;
	}
}
