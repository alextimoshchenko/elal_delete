package ui.fragments.search;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import il.co.ewave.elal.R;
import interfaces.ISearchElement;
import ui.activities.EWBaseActivity;
import ui.activities.SearchFlightActivity;
import ui.adapters.SearchDestinationAdapter;
import ui.fragments.EWBaseFragment;
import utils.errors.ErrorsHandler;
import utils.errors.LocalError;
import utils.global.AppUtils;
import utils.global.CustomTextWatcher;
import utils.global.FilterList;
import utils.global.enums.eCities;
import utils.global.enums.eListType;
import utils.global.enums.eSearchDestinationType;
import utils.global.enums.eTabStatement;
import utils.global.enums.eTypesOfTrip;
import utils.global.googleAnalitics.googleTagManager.TagManagerImplementation.ePushEvents;
import utils.global.searchFlightObjects.SearchObject;
import webServices.responses.ResponseClearUserHistory;
import webServices.responses.ResponseGetSearchHistory;
import webServices.responses.getSearchGlobalData.GetSearchGlobalDataContentElalCities;
import webServices.responses.getSearchHistory.ResponseGetSearchHistoryContent;

/**
 * Created with care by Alexey.T on 10/07/2017.
 */

public class SearchDestinationFragment extends EWBaseFragment implements Response.ErrorListener
{
	private final String TAG = SearchDestinationFragment.class.getSimpleName();
	
	private ImageView mIvBackArrow;
	private TextView mTvTitle, mTvCleanHistory, mTvBellowSearchText;
	private EditText mEtSearchText;
	private RecyclerView mRvDestination;
	private SearchDestinationAdapter<ISearchElement> mAdapter;
	private List<ResponseGetSearchHistoryContent> mHistoryElementsList = new ArrayList<>();
	private FilterList<ISearchElement> mSearchElementsFilterList = new FilterList<>();
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_seach_distination_layoyut);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		initReference();
		setListeners();
		setFilterLists();
	}
	
	private void setScreenTitleSize(final boolean iIsSmall)
	{
		int resId = iIsSmall ? R.style.Text_16sp_Topaz_Light : R.style.Text_18sp_Topaz_Light;
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
		{
			mTvTitle.setTextAppearance(resId);
		}
		else
		{
			mTvTitle.setTextAppearance(getActivity(), resId);
		}
		
		Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Rubik-Light.ttf");
		mTvTitle.setTypeface(face);
	}
	
	@Override
	public void onResume()
	{
		setGui();
		super.onResume();
	}
	
	private void setFilterLists()
	{
		SearchObject searchObject = SearchObject.getInstance();
		eSearchDestinationType destinationType = searchObject.getCurrentDestinationType();
		List<ISearchElement> elements = new ArrayList<>();
		
		List<GetSearchGlobalDataContentElalCities> destinationCitiesList = new ArrayList<>(searchObject.getElalDestinationCitiesList());
		List<GetSearchGlobalDataContentElalCities> departureCitiesList = new ArrayList<>(searchObject.getElalDepartureCitiesList());
		
		switch (destinationType)
		{
			case FIND_ORIGIN_DESTINATION:
			{
				elements.addAll(departureCitiesList);
				break;
			}
			case FIND_TO_DESTINATION:
			{
				elements.addAll(destinationCitiesList);
				break;
			}
			case FIND_ADDITIONAL_ORIGIN_DESTINATION:
			{
				List<GetSearchGlobalDataContentElalCities> cities = searchObject.getCityListFromListByCode(eListType.DEPARTURE_CITIES_LIST, eCities.TLV);
				
				if (searchObject.isOriginDestinationTLV())
				{
					for (GetSearchGlobalDataContentElalCities tmpCity : cities)
					{
						if (departureCitiesList.contains(tmpCity))
						{
							departureCitiesList.remove(tmpCity);
						}
					}
				}
				else if (searchObject.isToDestinationTLV())
				{
					departureCitiesList.clear();
					departureCitiesList.addAll(cities);
				}
				
				elements.addAll(departureCitiesList);
				break;
			}
			case FIND_ADDITIONAL_TO_DESTINATION:
			{
				List<GetSearchGlobalDataContentElalCities> cities = searchObject.getCityListFromListByCode(eListType.DESTINATION_CITIES_LIST, eCities.TLV);
				
				if (searchObject.isOriginDestinationTLV())
				{
					destinationCitiesList.clear();
					destinationCitiesList.addAll(cities);
				}
				else if (searchObject.isToDestinationTLV())
				{
					for (GetSearchGlobalDataContentElalCities tmpCity : cities)
					{
						if (destinationCitiesList.contains(tmpCity))
						{
							destinationCitiesList.remove(tmpCity);
						}
					}
				}
				
				elements.addAll(destinationCitiesList);
				
				break;
			}
		}
		
		mSearchElementsFilterList.startNewList(elements);
	}
	
	private void setRvDestinationAdapter(List<ISearchElement> iSearchElementsSet)
	{
		if (mAdapter == null)
		{
			mAdapter = new SearchDestinationAdapter<>(new SearchDestinationAdapter.IDestinationTypeListener()
			{
				@Override
				public void onItemClicked(final ISearchElement iElement)
				{
					doOnRecyclerViewItemClick(iElement);
				}
			});
			
			mAdapter.updateData(iSearchElementsSet);
			mRvDestination.setLayoutManager(new LinearLayoutManager(getContext()));
			mRvDestination.setAdapter(mAdapter);
		}
		else
		{
			mAdapter.updateData(iSearchElementsSet);
			mAdapter.notifyDataSetChanged();
		}
	}
	
	private void setRvDestinationAdapter(TreeSet<ISearchElement> iSearchElementsSet)
	{
		List<ISearchElement> elements = new ArrayList<>();
		elements.addAll(iSearchElementsSet);
		setRvDestinationAdapter(elements);
	}
	
	private void doOnRecyclerViewItemClick(final ISearchElement iElement)
	{
		if (iElement instanceof GetSearchGlobalDataContentElalCities)
		{
			doOnSearchGlobalDataClick(iElement);
		}
		else if (iElement instanceof ResponseGetSearchHistoryContent)
		{
			doOnSearchHistoryClick(iElement);
			((EWBaseActivity) getActivity()).pushEvent(ePushEvents.SEARCH_HISTORY_CLICK);
		}
		
		tryPopFragment();
	}
	
	private void doOnSearchHistoryClick(final ISearchElement iElement)
	{
		SearchObject searchObject = SearchObject.getInstance();
		
		ResponseGetSearchHistoryContent clickedSearchHistoryItem = (ResponseGetSearchHistoryContent) iElement;
		
		eTypesOfTrip typeOfTripOfClickedItem = eTypesOfTrip.getTypeById(clickedSearchHistoryItem.getFlightSearchTypeID());
		
		if (!typeOfTripOfClickedItem.isNoSuchItem() && typeOfTripOfClickedItem.isReturnFromAnotherDestination())
		{
			//Because if type ReturnFromAnotherDestination I need to switch to eTabStatement.FLIGHT_TICKET because only in this type available ReturnFromAnotherDestination
			searchObject.setCurrentTabStatement(eTabStatement.FLIGHT_TICKET);
		}
		
		searchObject.bindByHistoryObject(clickedSearchHistoryItem);
		//		searchObject.bindByHistoryObject((ResponseGetSearchHistoryContent) iElement);
	}
	
	private void doOnSearchGlobalDataClick(final ISearchElement iElement)
	{
		SearchObject searchObject = SearchObject.getInstance();
		eSearchDestinationType destinationType = searchObject.getCurrentDestinationType();
		
		switch (destinationType)
		{
			case FIND_ORIGIN_DESTINATION:
			{
				searchObject.setCurrentOriginDestination(iElement);
				searchObject.setCurrentToDestination(null);
				searchObject.setAdditionalCurrentOriginDestination(null);
				searchObject.setAdditionalCurrentToDestination(null);
				break;
			}
			case FIND_TO_DESTINATION:
			{
				searchObject.setCurrentToDestination(iElement);
				searchObject.setAdditionalCurrentOriginDestination(null);
				searchObject.setAdditionalCurrentToDestination(null);
				break;
			}
			case FIND_ADDITIONAL_ORIGIN_DESTINATION:
			{
				searchObject.setAdditionalCurrentToDestination(null);
				searchObject.setAdditionalCurrentOriginDestination(iElement);
				break;
			}
			case FIND_ADDITIONAL_TO_DESTINATION:
			{
				searchObject.setAdditionalCurrentToDestination(iElement);
				break;
			}
		}
	}
	
	private void initReference()
	{
		Activity act = getActivity();
		
		mTvBellowSearchText = (TextView) act.findViewById(R.id.tv_frag_search_destination_bellow_search_text);
		mRvDestination = (RecyclerView) act.findViewById(R.id.rv_frag_search_destination);
		mTvCleanHistory = (TextView) act.findViewById(R.id.tv_frag_search_destination_clean_history);
		
		//Edit Text field search destination
		View destinationInputField = act.findViewById(R.id.cdll_frag_search_destination_input);
		mEtSearchText = (EditText) destinationInputField.findViewById(R.id.et_main_text);
		
		//header
		View searchDestinationHeader = act.findViewById(R.id.ll_frag_search_destination_header);
		mIvBackArrow = (ImageView) searchDestinationHeader.findViewById(R.id.iv_name_screen_back_arrow);
		mTvTitle = (TextView) searchDestinationHeader.findViewById(R.id.tv_name_screen_title);
	}
	
	private void setVisibilityCleanHistoryText(boolean isVisible)
	{
		mTvCleanHistory.setVisibility(isVisible ? View.VISIBLE : View.GONE);
	}
	
	private void setVisibilityTextBellowSearchText(boolean isVisible)
	{
		mTvBellowSearchText.setVisibility(isVisible ? View.VISIBLE : View.GONE);
	}
	
	private void setGui()
	{
		String header = getString(R.string.some_error);
		SearchObject searchObject = SearchObject.getInstance();
		eSearchDestinationType destinationType = searchObject.getCurrentDestinationType();
		
		if (destinationType.isOriginDestinationType() || destinationType.isAdditionalOriginDestinationType())
		{
			header = getString(R.string.from_where_are_you_going);
			setScreenTitleSize(true);
		}
		else if (destinationType.isToDestinationType() || destinationType.isAdditionalToDestinationType())
		{
			header = getString(R.string.where_are_you_going);
			setScreenTitleSize(false);
		}
		
		mTvTitle.setText(header);
	}
	
	private void setListeners()
	{
		mTvCleanHistory.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnCleanHistoryClick();
			}
		});
		mIvBackArrow.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnBackClick();
			}
		});
		
		mEtSearchText.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				if (s.length() > 0)
				{
					doOnSearchByInput();
					setHistoryAttributes(false);
				}
				else
				{
					setSearchHistoryDataAndNotifyAdapter();
					
					if (mHistoryElementsList != null && !mHistoryElementsList.isEmpty())
					{
						setHistoryAttributes(true);
					}
				}
			}
		});
	}
	
	private void doOnCleanHistoryClick()
	{
		if (isServiceConnected() && (isResumed() || isVisible()))
		{
			((SearchFlightActivity) getActivity()).clearUserHistory(new Response.Listener<ResponseClearUserHistory>()
			{
				@Override
				public void onResponse(final ResponseClearUserHistory response)
				{
					setRvDestinationAdapter(new ArrayList<ISearchElement>());
					setHistoryAttributes(false);
				}
			}, this);
		}
		
		//event 26
		((EWBaseActivity) getActivity()).pushEvent(ePushEvents.CLEAR_SEARCH_HISTORY);
	}
	
	private void doOnSearchByInput()
	{
		sortByAlphabetic();
	}
	
	private void sortByAlphabetic()
	{
		String sortText = mEtSearchText.getText().toString();
		
		mSearchElementsFilterList.sortAlphabeticAsync(sortText, new FilterList.IOnFilterCompleteListener()
		{
			@Override
			public void onFilterComplete()
			{
				new Handler(getContext().getMainLooper()).post(new Runnable()
				{
					@Override
					public void run()
					{
						setRvDestinationAdapter(mSearchElementsFilterList.getFilteredList());
						
						if (mSearchElementsFilterList.getFilteredList().size() <= 0)
						{
							AppUtils.printLog(Log.ERROR, TAG, getString(R.string.no_such_items));
						}
					}
				});
			}
		});
	}
	
	@Override
	public void onServiceConnected()
	{
		getSearchHistory();
	}
	
	private void getSearchHistory()
	{
		if (isServiceConnected() && (isResumed() || isVisible()))
		{
			((SearchFlightActivity) getActivity()).getSearchHistory(new Response.Listener<ResponseGetSearchHistory>()
			{
				@Override
				public void onResponse(final ResponseGetSearchHistory response)
				{
					if (response != null && response.getContent() != null && !response.getContent().isEmpty())
					{
						List<ResponseGetSearchHistoryContent> mHistoryElementsListResult = findIfNeedToJoinReturnFromAnotherDestinationType(response);
						
						mHistoryElementsList = new ArrayList<>(mHistoryElementsListResult);
						setSearchHistoryDataAndNotifyAdapter();
						setHistoryAttributes(true);
					}
					else
					{
						AppUtils.showKeyboard(mEtSearchText);
						setHistoryAttributes(false);
					}
				}
			}, this);
		}
	}
	
	private void setHistoryAttributes(boolean iState)
	{
		setVisibilityCleanHistoryText(iState);
		setVisibilityTextBellowSearchText(iState);
	}
	
	/**
	 * I get from WS response list of elements if one of the elements has `RETURN_FROM_ANOTHER_DESTINATION` type it means that additional {@params mDepartureCityCode, mArrivalCityCode,
	 * mDepartureDate, mArrivalDate} I need to get from next element, and than delete this element
	 */
	@NonNull
	private List<ResponseGetSearchHistoryContent> findIfNeedToJoinReturnFromAnotherDestinationType(final ResponseGetSearchHistory response)
	{
		List<ResponseGetSearchHistoryContent> mHistoryElementsListResult = new ArrayList<>();
		List<ResponseGetSearchHistoryContent> mHistoryElementsListFromResponse = response.getContent();
		SearchObject searchObject = SearchObject.getInstance();
		
		for (int i = 0 ; i < mHistoryElementsListFromResponse.size() ; i++)
		{
			ResponseGetSearchHistoryContent currentHistoryElement = mHistoryElementsListFromResponse.get(i);
			
			if (currentHistoryElement.getFlightSearchTypeID() == eTypesOfTrip.RETURN_FROM_ANOTHER_DESTINATION.getTypeOfTripId())
			{
				int flightSearchId = currentHistoryElement.getFilghtSearchID();
				
				/*
				  I am using `++i` because of if current item has type of trip `return from another destination` it means that I need to take additional params from next element in
				  list and delete it after this. Instead of this I am using `++i` and literally skip this element in range.
				 */
				ResponseGetSearchHistoryContent nextHistoryElement = mHistoryElementsListFromResponse.get(++i);
				
				if (flightSearchId == nextHistoryElement.getReference())
				{
					String originDestinationDepartureCityCode = currentHistoryElement.getDepartureCityCode();
					String originDestinationArrivalCityCode = currentHistoryElement.getArrivalCityCode();
					String additionalDestinationDepartureCityCode = nextHistoryElement.getDepartureCityCode();
					String additionalDestinationArrivalCityCode = nextHistoryElement.getArrivalCityCode();
					
					String departureDestinationCityName = searchObject.getCityListFromListByCode(eListType.DEPARTURE_CITIES_LIST, originDestinationDepartureCityCode).get(0).getCityName();
					String arrivalDestinationCityName = searchObject.getCityListFromListByCode(eListType.DESTINATION_CITIES_LIST, originDestinationArrivalCityCode).get(0).getCityName();
					String additionalDepartureDestinationCityName = searchObject.getCityListFromListByCode(eListType.DEPARTURE_CITIES_LIST, additionalDestinationDepartureCityCode)
					                                                            .get(0)
					                                                            .getCityName();
					String additionalArrivalDestinationCityName = searchObject.getCityListFromListByCode(eListType.DESTINATION_CITIES_LIST, additionalDestinationArrivalCityCode).get(0).getCityName();
					
					currentHistoryElement.setAdditionalDepartureCityCode(additionalDestinationDepartureCityCode);
					currentHistoryElement.setAdditionalArrivalCityCode(additionalDestinationArrivalCityCode);
					currentHistoryElement.setAdditionalDepartureDate(nextHistoryElement.getDepartureDate());
					currentHistoryElement.setAdditionalArrivalDate(nextHistoryElement.getArrivalDate());
					
					currentHistoryElement.setSearchTitle(departureDestinationCityName + getString(R.string.dash_sing) + arrivalDestinationCityName + getString(R.string.dash_sing) + additionalDepartureDestinationCityName + getString(R.string.dash_sing) + additionalArrivalDestinationCityName);
				}
				else
				{
					AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.NoMatchingReturnFromAnotherDestinationItem.getErrorMessage());
				}
			}
			
			mHistoryElementsListResult.add(currentHistoryElement);
		}
		
		return mHistoryElementsListResult;
	}
	
	private void setSearchHistoryDataAndNotifyAdapter()
	{
		List<ISearchElement> historyElements = new ArrayList<>();
		historyElements.addAll(mHistoryElementsList);
		setRvDestinationAdapter(historyElements);
	}
	
	private void doOnBackClick()
	{
		tryPopFragment();
	}
	
	@Override
	public void onErrorResponse(final VolleyError iError)
	{
		ErrorsHandler.tryShowServiceErrorDialog(iError, getActivity());
	}
}
