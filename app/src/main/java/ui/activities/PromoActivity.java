package ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;


import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import global.IntentExtras;
import il.co.ewave.elal.R;
import webServices.global.RequestStringBuilder;
import webServices.responses.promo.Promo;

public class PromoActivity extends EWBaseActivity
{
	private static final String TAG = PromoActivity.class.getSimpleName();
	
	public static void tryStartPromoActivity(Activity iActivity, Promo iPromoData)
	{
		Intent intent = new Intent(iActivity, PromoActivity.class);
		intent.putExtra(IntentExtras.PROMO_DATA, iPromoData);
		iActivity.startActivity(intent);
	}
	
	private static final long PROMO_DELAY = 3000;
	
	private ImageView mImgBackground;
	private ImageView mImgDismiss;
	private Promo mPromoData;
	private boolean mIsUserInterrupt;
	
	@Override
	protected void initScreen()
	{
		setContentView(R.layout.activity_promo_layout);
		
		initReference();
		setListeners();
		setPromoVisibility(false);
		
		pushScreenOpenEvent("Promo");
		
		getIntentExtras();
		continueOrFinish();
		//		showPromo();
	}
	
	private void continueOrFinish()
	{
		if (mPromoData == null || TextUtils.isEmpty(mPromoData.getImage()))
		{
			goToMainActivity();
		}
	}
	
	@Override
	protected void onServiceConnected()
	{
		showPromo();
	}
	
	private void initReference()
	{
		mImgBackground = findViewById(R.id.img_promo_Background);
		mImgDismiss = findViewById(R.id.img_promo_Dismiss);
	}
	
	private void setListeners()
	{
		mImgBackground.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (mPromoData != null && mPromoData.getUrl() != null)
				{
					goToMainActivity(mPromoData.getUrl());
				}
			}
		});
		
		mImgDismiss.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				goToMainActivity();
			}
		});
	}
	
	private void getIntentExtras()
	{
		Bundle bundle = getIntent().getExtras();
		if (bundle != null)
		{
			mPromoData = bundle.getParcelable(IntentExtras.PROMO_DATA);
		}
	}
	
	private synchronized void goToMainActivity(String iUrl)
	{
		if (!mIsUserInterrupt)
		{
			mIsUserInterrupt = true;
			
			Intent intent = new Intent(this, MainActivity.class);
			if (!TextUtils.isEmpty(iUrl))
			{
				intent.putExtra(IntentExtras.URL, iUrl);
			}
			//			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(intent);
			finish();
		}
	}
	
	private void goToMainActivity()
	{
		goToMainActivity(null);
	}
	
	private void continueToMainAfterDelay()
	{
		new Handler().postDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				goToMainActivity();
			}
		}, PROMO_DELAY);
	}
	
	@Override
	public void onBackPressed()
	{
		// nothing... disable user dismissal.
	}
	
	private void showPromo()
	{
		if (mPromoData != null && mPromoData.getImage() != null)
		{
			if (isServiceConnected())
			{
				try
				{
					
					Picasso.Builder builder = new Picasso.Builder(this);
					builder.listener(new Picasso.Listener()
					{
						@Override
						public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception)
						{
							Log.d(TAG, "Picasso: " + exception.toString());
						}
					});
					builder.build().load(RequestStringBuilder.getPromoImageUrl(mPromoData.getImage())).into(mImgBackground, new Callback()
					{
						@Override
						public void onSuccess()
						{
							setPromoVisibility(true);
							continueToMainAfterDelay();
						}
						
						@Override
						public void onError()
						{
							goToMainActivity();
						}
						
					});
				}
				catch (Exception iE)
				{
					iE.printStackTrace();
					goToMainActivity();
				}
				//				getService().getController(LoginController.class).loadImage(RequestStringBuilder.getPromoImageUrl(mPromoData.getImage()), new CustomImageLoader.ImageListener()
				//				{
				//					@Override
				//					public void onResponse(CustomImageLoader.ImageContainer response, boolean isImmediate)
				//					{
				//						if (!isImmediate)
				//						{
				//							if (response.getBitmap() != null)
				//							{
				//								mImgBackground.setImageBitmap(response.getBitmap());
				//
				//								setPromoVisibility(true);
				//								continueToMainAfterDelay();
				//							}
				//							else
				//							{
				//								goToMainActivity();
				//							}
				//						}
				//					}
				//
				//					@Override
				//					public void onErrorResponse(VolleyError error)
				//					{
				//						goToMainActivity();
				//					}
				//				});
			}
			else
			{
				goToMainActivity();
			}
		}
		else
		{
			goToMainActivity();
		}
	}
	
	private void setPromoVisibility(boolean iIsVisible)
	{
		mImgBackground.setVisibility(iIsVisible ? View.VISIBLE : View.GONE);
		mImgDismiss.setVisibility(iIsVisible ? View.VISIBLE : View.GONE);
	}
}