package utils.ssl;

/**
 * Created with care by Shahar Ben-Moshe on 7/16/15.
 */
public class SslCertificate
{
	public static final String FILE_TYPE_PFX = "PKCS12";
	
	public static final String ALGORITHM_X509 = "X509";
	
	private int mCertificateResourceId;
	private String mCertificatePassword;
	private String mCertificateFileType;
	private String mCertificateAlgorithm;
	
	/**
	 * An SslCertificate holds the necessary information in order to use a certificate successfully.<BR>
	 * This sslCertificate can be used either for a client or a server certificate.
	 *
	 * @param iCertificateResourceId - The resource id of the certificate. The certificate must be in the <B><U>raw</U></B> resource folder.
	 * @param iCertificatePassword   - The password to be used to load the certificate.
	 * @param iCertificateFileType   - The Certificate file types can be found in this class with the prefix <B>"FILE_TYPE"</B> or can be entered manually.
	 * @param iCertificateAlgorithm  - The Certificate algorithm types can be found in this class with the prefix <B>"ALGORITHM"</B> or can be entered manually.
	 */
	public SslCertificate(int iCertificateResourceId, String iCertificatePassword, String iCertificateFileType, String iCertificateAlgorithm)
	{
		this.mCertificateResourceId = iCertificateResourceId;
		this.mCertificatePassword = iCertificatePassword;
		this.mCertificateFileType = iCertificateFileType;
		this.mCertificateAlgorithm = iCertificateAlgorithm;
	}
	
	public int getCertificateResourceId()
	{
		return mCertificateResourceId;
	}
	
	public String getCertificatePassword()
	{
		return mCertificatePassword;
	}
	
	public String getCertificateType()
	{
		return mCertificateFileType;
	}
	
	public String getCertificateAlgorithm()
	{
		return mCertificateAlgorithm;
	}
}