package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.UserData;

public class RequestGetAppData
{
	@JsonProperty("LanguageCode")
	private String mLanguageCode = UserData.getInstance().getLanguage().getLanguageCode();
	
	public RequestGetAppData()
	{
	}
}
