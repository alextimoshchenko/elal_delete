package interfaces;

import ui.customWidgets.swipeLayout.SwipeLayout;

/**
 * Created with care by Shahar Ben-Moshe on 04/01/17.
 */

public interface IFamilyMemberListener
{
	void onDeleteItem(int iPosition);
	
	void onEditItem(int iPosition);
	
	void onItemClick(int iPosition, SwipeLayout iSwipeLayout);
	
	void onOptionClick(int iPosition);
	
	void unlinkMatMidMember(SwipeLayout swipeLayout, int iPosition);
}
