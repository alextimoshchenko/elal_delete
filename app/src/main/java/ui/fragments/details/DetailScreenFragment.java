package ui.fragments.details;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import global.AppData;
import global.ElAlApplication;
import global.GlobalVariables;
import global.IntentExtras;
import global.UserData;
import global.eLanguage;
import global.ocr.OcrIsraelDriversLicense;
import global.ocr.OcrPassport;
import il.co.ewave.elal.R;
import interfaces.ILoginResponse;
import interfaces.IOcrResultObject;
import interfaces.IOnCancelClick;
import scanovate.ocr.common.OCRManager;
import ui.activities.DocumentScanActivity;
import ui.activities.EWBaseActivity;
import ui.activities.EWBaseDrawerActivity;
import ui.activities.EWBaseUserActivity;
import ui.activities.MainActivity;
import ui.adapters.SpinnerHintAdapter;
import ui.customWidgets.CustomScrollView;
import ui.customWidgets.ElalCustomAutoComplete;
import ui.customWidgets.ElalCustomTilLL;
import ui.customWidgets.ExpandCollapseAnimation;
import ui.fragments.EWBaseScrollFragment;
import ui.fragments.UserFamilyFragment;
import ui.fragments.WebViewFragment;
import utils.errors.ErrorsHandler;
import utils.global.AppUtils;
import utils.global.CustomTextWatcher;
import utils.global.DateTimeUtils;
import utils.global.ElalPreferenceUtils;
import utils.global.googleAnalitics.googleTagManager.TagManagerImplementation.ePushEvents;
import utils.permissions.IPermissionsListener;
import utils.permissions.PermissionManager;
import webServices.controllers.LoginController;
import webServices.global.RequestStringBuilder;
import webServices.requests.RequestSetUserDetails;
import webServices.requests.RequestSetUserMatmidDetails;
import webServices.responses.ResponseMatmidLogin;
import webServices.responses.ResponseSetUserDetails;
import webServices.responses.ResponseSetUserMatmidDetails;
import webServices.responses.ResponseUserLogin;
import webServices.responses.getMobileCountryCodes.MobileCountryCode;
import webServices.responses.responseUserLogin.UserLogin;
import webServices.responses.responseUserLogin.UserObject;

public class DetailScreenFragment extends EWBaseScrollFragment implements Response.ErrorListener, IPermissionsListener
{
	private static final String TAG = DetailScreenFragment.class.getSimpleName();
	private static final int EXPAND_DURATION = 500;
	private ElalCustomTilLL mTilLastNameHe, mTilFirstNameEn, mTilLastNameEn, mTilFirstNameHe, mTilIdNumber;
	private TextView mTvScreenSkip;
	private LinearLayout mLlPassportScan, mLlTitle, mLlKeyboardContainer;
	private ElalCustomTilLL mTilTitle, mTilDOB, mTilPassportExpiration;
	private EditText mEtTitle;
	private Spinner mSpnrDescription;
	private EditText mEtFirstNameEn, mEtLastNameEn, mEtFirstNameHe, mEtLastNameHe, mEtPassportNum, mEtIdNumber, mEtDOB, mEtPassportExpiration;
	private ImageView mIvBackArrow;
	private List<String> mDescriptionList = new ArrayList<>();
	private CustomScrollView mSvMainContainer;
	private ExpandCollapseAnimation mTitleExpandCollapseAnimation, mKeyboardReflectorExpandCollapseAnimation;
	private Button mBtnContinue, mBtnDOB, mBtnExpirationDate;
	private Calendar mCalendarDOB;
	private Calendar mCalendarExpirationDate;
	private boolean mIsMatmid;
	private TextView mTvMakeSureDetails;
	private ElalCustomTilLL mTilEmail;
	private EditText mEtEmail;
	private ElalCustomTilLL mTilPhoneNumber;
	private ElalCustomAutoComplete mTilCountryCode;
	private EditText mEtPhone;
	private AutoCompleteTextView mEtCountryCode;
	private TextView mTvLinkToRules;
	private CheckBox mCbAgreeRules;
	private TextView mTvAgreeRulesError;
	
	private TextView mTvMatmidName;
	private TextView mTvMatmidNumber;
	private TextView mTvMatmidId;
	private TextView mTvMatmidDOB;
	private boolean mIsRegistration;
	private ElalCustomTilLL mTilPassportNum;
	
	private ImageView mIvCleanExpirationDate, mIvCleanDob;
	private ArrayList<MobileCountryCode> mCountryCodesArray = new ArrayList<>();
	
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		getIntentExtras();
		
		if (UserData.getInstance().getUserObject() != null)
		{
			mIsMatmid = UserData.getInstance().getUserObject().isMatmid();
		}
		
		getCountryCodesList();
		
		initReference();
		setListeners();
		setGui();
		setDescriptionList();
		setSpinnerDescriptionAdapter();
		setExpandableViews();
		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		attachKeyboardListeners((ViewGroup) view.findViewById(R.id.ll_frag_detail_root_view_group));
		
		if (mIsMatmid)
		{
			setMatmidFields();
			pushScreenOpenEvent("Matmid Details");
		}
		else
		{
			setUserFields();
			pushScreenOpenEvent("My Details");
		}
	}
	
	@Override
	public void onPause()
	{
		((EWBaseDrawerActivity) getActivity()).enableLogoBtn(mIsRegistration);
		super.onPause();
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		((EWBaseDrawerActivity) getActivity()).enableLogoBtn(!mIsRegistration);
		
	}
	
	protected void onShowKeyboard(int keyboardHeight)
	{
		animateCollapseExpandOfKeyboardReflector(false);
	}
	
	protected void onHideKeyboard()
	{
		animateCollapseExpandOfKeyboardReflector(true);
	}
	
	private void animateCollapseExpandOfKeyboardReflector(final boolean iShouldExpand)
	{
		if (mKeyboardReflectorExpandCollapseAnimation == null)
		{
			return;
		}
		
		if ((iShouldExpand && !mKeyboardReflectorExpandCollapseAnimation.isExpand()) || (!iShouldExpand && mKeyboardReflectorExpandCollapseAnimation.isExpand()))
		{
			mKeyboardReflectorExpandCollapseAnimation.reverseAndAnimate();
		}
	}
	
	private void initReference()
	{
		Activity act = getActivity();
		mLlKeyboardContainer = act.findViewById(R.id.ll_frag_detail_keyboard_container);
		
		mTvScreenSkip = act.findViewById(R.id.tv_skip_layout_skip);
		mLlPassportScan = act.findViewById(R.id.ll_frag_detail_passport_scan);
		mTvMakeSureDetails = act.findViewById(R.id.tv_make_sure_details);
		
		mSpnrDescription = act.findViewById(R.id.spnr_frag_detail_description);
		
		
		mBtnExpirationDate = act.findViewById(R.id.btn_frag_detail_expiration_date);
		
		mIvBackArrow = act.findViewById(R.id.iv_skip_layout_back_arrow);
		mSvMainContainer = act.findViewById(R.id.scroll_frag_detail);
		mLlTitle = act.findViewById(R.id.ll_frag_detail_title);
		mBtnContinue = act.findViewById(R.id.btn_frag_detail_continue);
		
		mTvMatmidName = act.findViewById(R.id.tv_matmid_member_name);
		mTvMatmidNumber = act.findViewById(R.id.tv_matmid_member_number);
		mTvMatmidId = act.findViewById(R.id.tv_matmid_member_id);
		mTvMatmidDOB = act.findViewById(R.id.tv_matmid_member_DOB);
		
		mCbAgreeRules = act.findViewById(R.id.cb_agree_rules);
		mTvLinkToRules = act.findViewById(R.id.tv_frag_detail_link_to_rules);
		mTvAgreeRulesError = act.findViewById(R.id.tv_agree_rules_error);
		
		//Expiration date
		mTilPassportExpiration = act.findViewById(R.id.til_frag_detail_expiration_date);
		mTilPassportExpiration.init(getString(R.string.expiration_date));
		//		mTilPassportExpiration.setDefaultHintText(getString(R.string.expiration_date));
		mEtPassportExpiration = mTilPassportExpiration.getEditText();
		mIvCleanExpirationDate = act.findViewById(R.id.iv_frag_detail_clean_expiration_date);
		
		//DOB
		mTilDOB = act.findViewById(R.id.til_frag_detail_date_of_birth);
		mTilDOB.init(getString(R.string.date_of_birth_asterisk));
		//		mTilDOB.setDefaultHintText(getString(R.string.date_of_birth_asterisk));
		mEtDOB = mTilDOB.getEditText();
		
		mIvCleanDob = act.findViewById(R.id.iv_frag_detail_clean_dob);
		mBtnDOB = act.findViewById(R.id.btn_frag_detail_date_of_birht);
		
		//Title
		mTilTitle = act.findViewById(R.id.til_frag_detail_title);
		mTilTitle.init(getString(R.string.title_asterisk));
		//		mTilTitle.setDefaultHintText(getString(R.string.title_asterisk));
		mEtTitle = mTilTitle.getEditText();
		mEtTitle.setHint("");
		mEtTitle.setCursorVisible(false);
		mEtTitle.setClickable(false);
		mEtTitle.setFocusable(false);
		
		//Email
		mTilEmail = act.findViewById(R.id.til_frag_detail_email);
		mTilEmail.init(getString(R.string.email_asterisk));
		//		mTilEmail.setDefaultHintText(getString(R.string.email_asterisk));
		mTilEmail.setInputTypeEmail();
		mEtEmail = mTilEmail.getEditText();
		
		//Phone prefix
		FrameLayout flCountryCode = act.findViewById(R.id.fl_frag_detail_country_code);
		mTilCountryCode = act.findViewById(R.id.til_frag_detail_country_code);
		mTilCountryCode.init(getString(R.string.mobile_country_code_asterisk));
		
		mEtCountryCode = mTilCountryCode.getAutoComplete();
		if (mIsMatmid)
		{
			flCountryCode.setVisibility(View.VISIBLE);
			mTilCountryCode.setInputTypePhone();
			if (mCountryCodesArray != null)
			{
				mTilCountryCode.setCountryCodesData(mCountryCodesArray);
			}
		}
		else
		{
			flCountryCode.setVisibility(View.GONE);
		}
		
		//Phone
		mTilPhoneNumber = act.findViewById(R.id.til_frag_detail_phone_number);
		mTilPhoneNumber.init(getString(R.string.mobile_phone_asterisk));
		//		mTilPhoneNumber.setDefaultHintText(getString(R.string.mobile_phone_asterisk));
		mTilPhoneNumber.setInputTypePhone();
		mEtPhone = mTilPhoneNumber.getEditText();
		//		mEtPhone.setInputType(InputType.TYPE_CLASS_PHONE);
		
		//FirstNameEn
		mTilFirstNameEn = act.findViewById(R.id.til_frag_detail_first_name_en);
		mTilFirstNameEn.init(getString(R.string.first_name_english));
		//		mTilFirstNameEn.setDefaultHintText(getString(R.string.first_name_english));
		mTilFirstNameEn.setInputTypeTextOnly();
		mEtFirstNameEn = mTilFirstNameEn.getEditText();
		
		
		//LastNameEn
		mTilLastNameEn = act.findViewById(R.id.til_frag_detail_family_name_en);
		mTilLastNameEn.init(getString(R.string.family_name_english));
		//		mTilLastNameEn.setDefaultHintText(getString(R.string.family_name_english));
		mTilLastNameEn.setInputTypeTextOnly();
		mEtLastNameEn = mTilLastNameEn.getEditText();
		
		//FirstNameHe
		mTilFirstNameHe = act.findViewById(R.id.til_frag_detail_first_name_he);
		mTilFirstNameHe.init(getString(R.string.first_name_hebrew));
		//		mTilFirstNameHe.setDefaultHintText(getString(R.string.first_name_hebrew));
		mTilFirstNameHe.setInputTypeTextOnly();
		mEtFirstNameHe = mTilFirstNameHe.getEditText();
		
		//LastNameHe
		mTilLastNameHe = act.findViewById(R.id.til_frag_detail_family_name_he);
		mTilLastNameHe.init(getString(R.string.family_name_hebrew));
		//		mTilLastNameHe.setDefaultHintText(getString(R.string.family_name_hebrew));
		mTilLastNameHe.setInputTypeTextOnly();
		mEtLastNameHe = mTilLastNameHe.getEditText();
		
		//PassportNum
		mTilPassportNum = act.findViewById(R.id.til_frag_detail_passport_number);
		mTilPassportNum.init(getString(R.string.passport_number));
		//		mTilPassportNum.setDefaultHintText(getString(R.string.passport_number));
		mEtPassportNum = mTilPassportNum.getEditText();
		mEtPassportNum.setImeOptions(EditorInfo.IME_ACTION_DONE);
		
		//IdNumber
		mTilIdNumber = act.findViewById(R.id.til_frag_detail_id_number);
		mTilIdNumber.init(getString(R.string.id_number));
		mTilIdNumber.setEditTextInputType(InputType.TYPE_CLASS_NUMBER, InputType.TYPE_NUMBER_FLAG_DECIMAL);
		
		//		mTilIdNumber.setDefaultHintText(getString(R.string.id_number));
		mEtIdNumber = mTilIdNumber.getEditText();
		mEtIdNumber.setImeOptions(EditorInfo.IME_ACTION_DONE);
	}
	
	private void setTextToEtPhone(String iText)
	{
		mEtPhone.setText(!TextUtils.isEmpty(iText) ? iText : "");
		//		tryToSetGravityIfHebrewAndNumericOnly(mEtPhone, iText);
	}
	
	private void setTextToEtCountryCode(String iText)
	{
		// TODO: 01/03/2018 - get default country code from PM
		if (mEtCountryCode != null)
		{
			mEtCountryCode.setText(!TextUtils.isEmpty(iText) ? iText : "");
		}
	}
	
	private void switchToBackMode(boolean iIsRegistration)
	{
		mIvBackArrow.setVisibility(iIsRegistration ? View.GONE : View.VISIBLE);
		mTvScreenSkip.setVisibility(iIsRegistration ? View.VISIBLE : View.GONE);
	}
	
	private void setGui()
	{
		//		UserObject user = UserData.getInstance().getUserObject();
		setScreenTitle();
		
		getActivity().findViewById(R.id.iv_name_screen_back_arrow).setVisibility(View.GONE);
		
		setScanViewVisibility();
		
		getActivity().findViewById(R.id.ll_matmid_details).setVisibility(mIsMatmid ? View.VISIBLE : View.GONE);
		getActivity().findViewById(R.id.ll_spnr_frag_detail_description).setVisibility(mIsMatmid ? View.GONE : View.VISIBLE);
		
		mTilLastNameHe.setVisibility(mIsMatmid ? View.GONE : View.VISIBLE);
		//		mTvScreenSkip.setVisibility(mIsMatmid ? View.GONE : View.VISIBLE);
		//		mLlPassportScan.setVisibility(mIsMatmid ? View.GONE : View.VISIBLE);
		mTvMakeSureDetails.setText(getString(mIsMatmid ? R.string.confirm_the_following_or_update_if_needed : R.string.make_sure_details_correct));
		mTilTitle.setVisibility(mIsMatmid ? View.GONE : View.VISIBLE);
		mSpnrDescription.setVisibility(mIsMatmid ? View.GONE : View.VISIBLE);
		mTilFirstNameEn.setVisibility(mIsMatmid ? View.GONE : View.VISIBLE);
		mEtFirstNameEn.setVisibility(mIsMatmid ? View.GONE : View.VISIBLE);
		mTilLastNameEn.setVisibility(mIsMatmid ? View.GONE : View.VISIBLE);
		mTilFirstNameHe.setVisibility(mIsMatmid ? View.GONE : View.VISIBLE);
		mTilDOB.setVisibility(mIsMatmid ? View.GONE : View.VISIBLE);
		mIvCleanDob.setVisibility(mIsMatmid ? View.GONE : View.VISIBLE);
		mTilIdNumber.setVisibility(mIsMatmid ? View.GONE : View.VISIBLE);
		mEtLastNameEn.setVisibility(mIsMatmid ? View.GONE : View.VISIBLE);
		mEtFirstNameHe.setVisibility(mIsMatmid ? View.GONE : View.VISIBLE);
		mEtLastNameHe.setVisibility(mIsMatmid ? View.GONE : View.VISIBLE);
		mBtnDOB.setVisibility(mIsMatmid ? View.GONE : View.VISIBLE);
		//		mBtnExpirationDate.setVisibility(mIsMatmid ? View.VISIBLE : View.VISIBLE);
		
		setEtPassportNum(!TextUtils.isEmpty(UserData.getInstance().getPassportNumber()) ? UserData.getInstance().getPassportNumber() : "");
		//		String expirationDate = DateTimeUtils.convertDateToSlashSeparatedDayMonthYearString(user == null ? new Date() : user.getPassportExpiration());
		//		setTextToEtPassportExpiration(!TextUtils.isEmpty(expirationDate) ? expirationDate : "");
		setTextToEtEmail(!TextUtils.isEmpty(UserData.getInstance().getEmail()) ? UserData.getInstance().getEmail() : "");
		setTextToEtPhone(!TextUtils.isEmpty(UserData.getInstance().getPhoneNumber()) ? UserData.getInstance().getPhoneNumber() : "");
		//		setTextToEtCountryCode(!TextUtils.isEmpty(UserData.getInstance().getCountryCode()) ? UserData.getInstance().getCountryCode() : "");
		setTextToEtCountryCode(UserData.getInstance().getCountryCode());
		
		mEtIdNumber.setVisibility(mIsMatmid ? View.GONE : View.VISIBLE);
		//		mIvBackArrow .setVisibility(mIsMatmid ? View.GONE : View.VISIBLE);
		//		mSvMainContainer = (CustomScrollView) act.findViewById(R.id.scroll_frag_detail);
		//		mLlTitle = (LinearLayout) act.findViewById(R.id.ll_frag_detail_title);
		//		mBtnContinue = (Button) act.findViewById(R.id.btn_frag_detail_continue);
		mEtDOB.setVisibility(mIsMatmid ? View.GONE : View.VISIBLE);
		setEmailPhoneVisibility();
		//		mTilEmail.setVisibility(mIsMatmid ? View.VISIBLE : View.GONE);
		//		mTilPhoneNumber.setVisibility(mIsMatmid ? View.VISIBLE : View.GONE);
		
		getActivity().findViewById(R.id.ll_agree_rules).setVisibility((mIsMatmid && UserData.getInstance().getUserObject().isMatmidFirstLogin()) ? View.VISIBLE : View.GONE);
		getActivity().findViewById(R.id.tv_header_skip_layout_title).setVisibility(View.INVISIBLE);
		
		if (mIsMatmid)
		{
			getActivity().findViewById(R.id.ll_skip_top_bar_root).setVisibility(UserData.getInstance().getUserObject().isMatmidFirstLogin() ? View.GONE : View.VISIBLE);
			mIvBackArrow.setVisibility(UserData.getInstance().getUserObject().isMatmidFirstLogin() ? View.GONE : View.VISIBLE);
			mTvScreenSkip.setVisibility(View.GONE);
		}
		else
		{
			getActivity().findViewById(R.id.ll_skip_top_bar_root).setVisibility(View.VISIBLE);
			
			switchToBackMode(mIsRegistration);
		}
		setCalendarExpirationDate(UserData.getInstance().getUserObject() != null ? UserData.getInstance().getUserObject().getPassportExpiration() : null);
		
		
		mBtnContinue.setText(mIsRegistration ? getText(R.string._continue) : getText(R.string.done));
		
	}
	
	private void setEmailPhoneVisibility()
	{
		if (!mIsMatmid && mIsRegistration)
		{
			mTilEmail.setVisibility(View.GONE);
			mTilPhoneNumber.setVisibility(View.GONE);
			mTilCountryCode.setVisibility(View.GONE);
		}
		else
		{
			mTilEmail.setVisibility(View.VISIBLE);
			mTilPhoneNumber.setVisibility(View.VISIBLE);
			mTilCountryCode.setVisibility(View.VISIBLE);
		}
	}
	
	private void setScanViewVisibility()
	{
		if (!mIsMatmid && mIsRegistration)
		{
			getActivity().findViewById(R.id.ll_scan_passport).setVisibility(View.VISIBLE);
		}
		else
		{
			getActivity().findViewById(R.id.ll_scan_passport).setVisibility(View.GONE);
		}
	}
	
	private void setScreenTitle()
	{
		String title = "";
		if (mIsMatmid && UserData.getInstance().getUserObject() != null && UserData.getInstance().getUserObject().isMatmidFirstLogin())
		{
			title = getString(R.string.personal_details);
		}
		else if (!mIsMatmid && mIsRegistration)
		{
			title = getString(R.string.personal_details);
		}
		else
		{
			title = getString(R.string.personal_info_update);
		}
		if (!TextUtils.isEmpty(title))
		{
			((TextView) getActivity().findViewById(R.id.tv_name_screen_title)).setText(title);
			getActivity().findViewById(R.id.tv_name_screen_title).setVisibility(View.VISIBLE);
		}
		else
		{
			getActivity().findViewById(R.id.tv_name_screen_title).setVisibility(View.INVISIBLE);
		}
		
	}
	
	private void setTextToEtEmail(String iText)
	{
		mEtEmail.setText(!TextUtils.isEmpty(iText) ? iText : "");
		//		tryToSetGravityIfHebrewAndNumericOnly(mEtEmail, iText);
	}
	
	private void setUserFields()
	{
		UserObject userObject = UserData.getInstance().getUserObject();
		
		if (userObject == null)
		{
			return;
		}
		if (!TextUtils.isEmpty(userObject.getTitle()))
		{
			
			mSpnrDescription.setSelection(AppUtils.getSelectionInt(mDescriptionList, userObject.getTitle()));
		}
		else
		{
			//			mSpnrDescription.setSelection(0);
		}
		setFirstNameEn(userObject.getFirstNameEng());
		setLastNameEn(userObject.getLastNameEng());
		setFirstNameHe(userObject.getFirstNameHeb());
		setLastNameHe(userObject.getLastNameHeb());
		setCalendarDOB(userObject.getDateOfBirth());
		//		if (userObject.getDateOfBirth() != null)
		//		{
		//			if (mCalendarDOB == null)
		//			{
		//				mCalendarDOB = Calendar.getInstance();
		//			}
		//			mCalendarDOB.setTime(userObject.getDateOfBirth());
		//
		//			setEtDOBText(DateTimeUtils.convertCalendarToSlashSeparatedDayMonthYearString(mCalendarDOB));
		//		}
		
		setEtPassportNum(userObject.getPassportNum());
		
		if (userObject.getPassportExpiration() != null)
		{
			if (mCalendarExpirationDate == null)
			{
				mCalendarExpirationDate = Calendar.getInstance();
			}
			
			mCalendarExpirationDate.setTime(userObject.getPassportExpiration());
			
			setTextToEtPassportExpiration(DateTimeUtils.convertCalendarToSlashSeparatedDayMonthYearString(mCalendarExpirationDate));
		}
		
		setEtIdNumberText(userObject.getIdNumber());
	}
	
	private void tryToSetGravityIfHebrewAndNumericOnly(EditText iEt, String iText)
	{
		try
		{
			String text = iText.replace("/", "").replace("@", "").replace(".", "");
			
			if (UserData.getInstance().getLanguage() == eLanguage.Hebrew && AppUtils.isNumeric(text))
			{
				iEt.setGravity(Gravity.END);
			}
			else if (UserData.getInstance().getLanguage() == eLanguage.Hebrew && AppUtils.isStringContainsLatinCharactersOnly(text))
			{
				iEt.setGravity(Gravity.END);
			}
		}
		catch (Exception e)
		{
		
		}
		
	}
	
	private void setMatmidFields()
	{
		UserObject user = UserData.getInstance().getUserObject();
		
		disableHeaderButtonsAsMatmidFirstLogin();
		
		String username = "";
		if (UserData.getInstance().getLanguage() == eLanguage.Hebrew && (!TextUtils.isEmpty(user.getFirstNameHeb()) && !TextUtils.isEmpty(user.getLastNameHeb())))
		{
			username = user.getFirstNameHeb() + " " + user.getLastNameHeb();
		}
		else
		{
			//			if(!TextUtils.isEmpty(user.getFirstNameEng())&&!TextUtils.isEmpty(user.getLastNameEng()))
			username = user.getFirstNameEng() + " " + user.getLastNameEng();
		}
		
		mTvMatmidName.setText(username);
		mTvMatmidNumber.setText(user.getMatmidMemberID());
		
		if (TextUtils.isEmpty(user.getIdNumber()))
		{
			getActivity().findViewById(R.id.ll_matmid_member_id).setVisibility(View.INVISIBLE);
		}
		else if (user.getIdNumber().length() < 5) // no valid id
		{
			getActivity().findViewById(R.id.ll_matmid_member_id).setVisibility(View.INVISIBLE);
		}
		else
		{
			mTvMatmidId.setText(user.getIdNumber());
			getActivity().findViewById(R.id.ll_matmid_member_id).setVisibility(View.VISIBLE);
		}
		
		
		if (user.getDateOfBirth() != null)
		{
			if (mCalendarDOB == null)
			{
				mCalendarDOB = Calendar.getInstance();
			}
			
			mCalendarDOB.setTime(user.getDateOfBirth());
			mTvMatmidDOB.setText(DateTimeUtils.convertCalendarToSlashSeparatedDayMonthYearString(mCalendarDOB));
			getActivity().findViewById(R.id.ll_matmid_dob).setVisibility(View.VISIBLE);
		}
		else
		{
			getActivity().findViewById(R.id.ll_matmid_dob).setVisibility(View.INVISIBLE);
		}
		
	}
	
	private void disableHeaderButtonsAsMatmidFirstLogin()
	{
		if (UserData.getInstance().getUserObject().isMatmid() && UserData.getInstance().getUserObject().isMatmidFirstLogin())
		{
			//			((EWBaseDrawerActivity)getActivity()).	handleHeaderButtonsAsMatmidFirstLogin();
		}
	}
	
	public void getIntentExtras()
	{
		if (getArguments() != null)
		{
			mIsRegistration = getArguments().getBoolean(IntentExtras.IS_REGISTRATION, false);
			mIsMatmid = getArguments().getBoolean(IntentExtras.IS_MATMID, false);
		}
	}
	
	private void setExpandableViews()
	{
		mLlTitle.post(new Runnable()
		{
			@Override
			public void run()
			{
				int expandedViewHeight = mLlTitle.getHeight();
				mTitleExpandCollapseAnimation = new ExpandCollapseAnimation(mLlTitle, true, EXPAND_DURATION, expandedViewHeight);
			}
		});
		
		mLlKeyboardContainer.post(new Runnable()
		{
			@Override
			public void run()
			{
				int expandedViewHeight = mLlKeyboardContainer.getHeight();
				mKeyboardReflectorExpandCollapseAnimation = new ExpandCollapseAnimation(mLlKeyboardContainer, true, EXPAND_DURATION, expandedViewHeight);
			}
		});
	}
	
	private void setDescriptionList()
	{
		String[] list = getResources().getStringArray(R.array.description_values);
		mDescriptionList = Arrays.asList(list);
	}
	
	private void setSpinnerDescriptionAdapter()
	{
		//		ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), R.layout.custom_spinner_view, android.R.id.text1, mDescriptionList);
		//		adapter.setDropDownViewResource(R.layout.custom_drop_down);
		//		mSpnrDescription.setAdapter(adapter);
		
		SpinnerHintAdapter adapter = new SpinnerHintAdapter(getContext(), R.layout.custom_spinner_settings_layout_new,  /*android.R.id.text1, */mDescriptionList, 0);
		adapter.setDropDownViewResource(R.layout.custom_drop_down);
		mSpnrDescription.setPrompt(getString(R.string.title_asterisk));
		mSpnrDescription.setAdapter(adapter);
	}
	
	private void setListeners()
	{
		mIvCleanDob.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnCleanDOBClick();
			}
		});
		
		mIvCleanExpirationDate.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnCleanExpirationDateClick();
			}
		});
		
		mSpnrDescription.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
		{
			@Override
			public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id)
			{
				showTitleError(false);
			}
			
			@Override
			public void onNothingSelected(final AdapterView<?> parent)
			{
			
			}
		});
		
		mEtPassportExpiration.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (hasFocus)
				{
					openDateOfExpirationPickerDialog();
				}
			}
		});
		
		mEtIdNumber.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				showIdError(false);
			}
		});
		
		mEtIdNumber.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus && !TextUtils.isEmpty(mEtIdNumber.getText().toString()) && !isIdNumberValid(mEtIdNumber.getText().toString()))
				{
					showIdError(true);
				}
			}
		});
		
		mBtnExpirationDate.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				openDateOfExpirationPickerDialog();
			}
		});
		
		mBtnDOB.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				openDateOfBirthPickerDialog();
			}
		});
		
		mEtFirstNameEn.addTextChangedListener(new CustomTextWatcher()
		{
			//			@Override
			//			public void onTextChanged(final CharSequence s, final int start, final int before, final int count)
			//			{
			//				if (!AppUtils.isStringContainsLatinCharactersOnly(s.toString())){
			//
			//				}
			//			}
			
			@Override
			public void afterTextChanged(final Editable s)
			{
				if (!TextUtils.isEmpty(s.toString()) && !AppUtils.isStringContainsLatinCharactersOnly(s.toString()))
				{
					showFirstNameEnError(true);
				}
				else
				{
					showFirstNameEnError(false);
				}
			}
		});
		
		mEtFirstNameEn.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus && !TextUtils.isEmpty(mEtFirstNameEn.getText().toString()) && !isFirstNameEnValid())
				{
					showFirstNameEnError(true);
				}
			}
		});
		
		mEtFirstNameHe.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				if (!TextUtils.isEmpty(s.toString()) && !AppUtils.isStringContainsHebrewCharactersOnly(s.toString()))
				{
					showFirstNameHeError(true);
				}
				else
				{
					showFirstNameHeError(false);
				}
				
			}
		});
		
		mEtFirstNameHe.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus && !TextUtils.isEmpty(mEtFirstNameHe.getText().toString()) && !isFirstNameHeValid())
				{
					showFirstNameHeError(true);
				}
			}
		});
		
		mEtDOB.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				showDOBError(false);
			}
		});
		
		mEtDOB.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus && !TextUtils.isEmpty(mEtDOB.getText().toString()) && !isDOBValid())
				{
					showDOBError(true);
				}
				else if (hasFocus)
				{
					openDateOfBirthPickerDialog();
				}
			}
		});
		
		mEtLastNameEn.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				if (!TextUtils.isEmpty(s.toString()) && !AppUtils.isStringContainsLatinCharactersOnly(s.toString()))
				{
					showFamilyNameEnError(true, 1);
				}
				else
				{
					showFamilyNameEnError(false, 1);
				}
			}
		});
		
		mEtLastNameEn.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus && !TextUtils.isEmpty(mEtLastNameEn.getText().toString()))
				{
					if (!isLastNameEnContainsLatinCharactersOnly())
					{
						showFamilyNameEnError(true, 1);
					}
					else if (!isLastNameEnContainsAtLeastTwoChar())
					{
						showFamilyNameEnError(true, 2);
					}
				}
			}
		});
		
		mEtLastNameHe.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				if (!TextUtils.isEmpty(s.toString()) && !AppUtils.isStringContainsHebrewCharactersOnly(s.toString()))
				{
					showLastNameHeError(true);
				}
				else
				{
					showLastNameHeError(false);
				}
				
			}
		});
		
		mEtLastNameHe.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus && !TextUtils.isEmpty(mEtLastNameHe.getText().toString()) && !isLastNameHeValid())
				{
					showLastNameHeError(true);
				}
			}
		});
		
		mEtEmail.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				showEmailError(false);
			}
		});
		
		mEtEmail.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus && !TextUtils.isEmpty(mEtEmail.getText().toString()) && !isEmailValid())
				{
					showEmailError(true);
				}
			}
		});
		
		mEtPhone.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				showPhoneNumberError(false);
			}
		});
		
		mEtPhone.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus && !TextUtils.isEmpty(mEtPhone.getText().toString()) && !isPhoneNumberValid())
				{
					showPhoneNumberError(true);
				}
			}
		});
		
		if (mEtCountryCode != null)
		{
			//			mEtCountryCode.addTextChangedListener(new CustomTextWatcher()
			//			{
			//				@Override
			//				public void afterTextChanged(final Editable s)
			//				{
			//					showCountryCodeError(false);
			//				}
			//			});
			
			mEtCountryCode.setGravity(Gravity.LEFT);
			mEtCountryCode.setOnFocusChangeListener(new View.OnFocusChangeListener()
			{
				@Override
				public void onFocusChange(final View v, final boolean hasFocus)
				{
					if (!hasFocus && !TextUtils.isEmpty(mEtCountryCode.getText().toString()) && !isCountryCodeValid())
					{
						showCountryCodeError(true);
					}
				}
			});
		}
		
		mEtPassportNum.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				showPassportNumberError(false);
			}
		});
		
		mEtPassportNum.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus && !TextUtils.isEmpty(mEtPassportNum.getText().toString()) && !isPassportNumberValid())
				{
					showPassportNumberError(true, getActivity().getResources().getString(R.string.invalid_passport_number));
				}
			}
		});
		
		mEtPassportExpiration.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				showPassportExpirationError(false);
				
			}
		});
		
		mEtPassportExpiration.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus && !TextUtils.isEmpty(mEtPassportExpiration.getText().toString()) && !isPassportExpirationValid())
				{
					showPassportExpirationError(true, getActivity().getResources().getString(R.string.invalid_expiration_date));
				}
			}
		});
		
		
		mCbAgreeRules.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked)
			{
				showAgreeRulesError(false);
			}
		});
		
		mTvScreenSkip.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnSkip();
			}
		});
		
		mIvBackArrow.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				if (mIsRegistration)
				{
					doOnSkip();
				}
				else
				{
					getActivity().onBackPressed();
				}
			}
		});
		
		mLlPassportScan.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnScan();
			}
		});
		
		mSvMainContainer.setOnScrollListener(new CustomScrollView.OnScrollListener()
		{
			@Override
			public void onGoUp()
			{
				animateCollapseExpandOfTitle(true);
			}
			
			@Override
			public void onGoDown()
			{
				try
				{
					animateCollapseExpandOfTitle(false);
				}
				catch (Exception e)
				{
				
				}
			}
		});
		
		mTvLinkToRules.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnReadRules();
			}
		});
		
		mBtnContinue.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnContinue();
			}
		});
	}
	
	private void doOnCleanDOBClick()
	{
		setCalendarDOB(null);
	}
	
	private void doOnCleanExpirationDateClick()
	{
		setCalendarExpirationDate(null);
	}
	
	private void showPassportExpirationError(boolean iState)
	{
		showPassportExpirationError(iState, null);
	}
	
	private void showPassportNumberError(boolean iState)
	{
		showPassportNumberError(iState, null);
	}
	
	private void doOnReadRules()
	{
		Bundle bundle = new Bundle();
		bundle.putString(IntentExtras.URL, RequestStringBuilder.getElalTermsOfUseUrl());
		tryGoToFragmentByFragmentClass(WebViewFragment.class, bundle, true);
	}
	
	private void openDateOfExpirationPickerDialog()
	{
		Calendar calendarMore10Years = Calendar.getInstance();
		calendarMore10Years.set(Calendar.YEAR, calendarMore10Years.get(Calendar.YEAR) + 10);
		AppUtils.createSimpleDatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener()
		{
			@Override
			public void onDateSet(final DatePicker view, final int year, final int month, final int dayOfMonth)
			{
				setCalendarExpirationDate(DateTimeUtils.convertYearMonthDayToCalendar(year, month, dayOfMonth).getTime());
			}
		}, mCalendarExpirationDate, Calendar.getInstance(), calendarMore10Years, new IOnCancelClick()
		{
			@Override
			public void cancelClick()
			{
				mEtPassportExpiration.clearFocus();
			}
		}).show();
	}
	
	private void openDateOfBirthPickerDialog()
	{
		if (mCalendarDOB == null)
		{
			mCalendarDOB = Calendar.getInstance();
			
			mCalendarDOB.set(Calendar.YEAR, 1986);
			mCalendarDOB.set(Calendar.MONTH, Calendar.JANUARY);
			mCalendarDOB.set(Calendar.DAY_OF_MONTH, 1);
		}
		
		DatePickerDialog dialog = AppUtils.createSimpleDatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener()
		{
			@Override
			public void onDateSet(final DatePicker view, final int year, final int month, final int dayOfMonth)
			{
				Date selectedDate = DateTimeUtils.convertYearMonthDayToCalendar(year, month, dayOfMonth).getTime();
				if (Calendar.getInstance().getTime().after(selectedDate))
				{
					setCalendarDOB(selectedDate);
				}
			}
		}, mCalendarDOB, null, Calendar.getInstance(), new IOnCancelClick()
		{
			@Override
			public void cancelClick()
			{
				mEtDOB.clearFocus();
			}
		});
		dialog.setCancelable(false);
		dialog.show();
	}
	
	private void setCalendarExpirationDate(final Date iCalendarExpirationDate)
	{
		if (iCalendarExpirationDate != null)
		{
			if (mCalendarExpirationDate == null)
			{
				mCalendarExpirationDate = Calendar.getInstance();
			}
			
			mCalendarExpirationDate.setTime(iCalendarExpirationDate);
			setTextToEtPassportExpiration(DateTimeUtils.convertCalendarToSlashSeparatedDayMonthYearString(mCalendarExpirationDate));
		}
		else
		{
			mCalendarExpirationDate = null;
			setTextToEtPassportExpiration("");
		}
	}
	
	private void setCalendarDOB(final Date iCalendarDOB)
	{
		if (iCalendarDOB != null)
		{
			if (mCalendarDOB == null)
			{
				mCalendarDOB = Calendar.getInstance();
			}
			
			mCalendarDOB.setTime(iCalendarDOB);
			setEtDOBText(DateTimeUtils.convertCalendarToSlashSeparatedDayMonthYearString(mCalendarDOB));
		}
		else
		{
			mCalendarDOB = null;
			setEtDOBText("");
		}
	}
	
	private void doOnContinue()
	{
		if ((mIsMatmid && !isFieldsMatmidValid()) || (!mIsMatmid && !isFieldsValid()))
		{
			return;
		}
		
		AppUtils.hideKeyboard(getActivity(), getView());
		doOnSetUserDetails();
		((EWBaseActivity) getActivity()).pushEvent(ePushEvents.MORE_DETAILS_CONTINUE);
	}
	
	private void doOnSetUserDetails()
	{
		if (isServiceConnected())
		{
			trySetProgressDialog(true);
			
			
			if (mIsMatmid && UserData.getInstance().getUserObject() != null && !TextUtils.isEmpty(UserData.getInstance().getUserObject().getSessionID()))
			{
				doOnSetUserMatmidDetails();
			}
			else if (!mIsMatmid)
			{
				doOnSetRegularUserDetails(UserData.getInstance().getUserObject());
			}
		}
	}
	
	private void doOnSetRegularUserDetails(final UserObject iUser)
	{
		final String email, phoneNumber, countryCode;
		final String firstNameHe, lastNameHe;
		final String passportNumber/*, expirationDate*/;
		final String idNumber;
		
		//		if (iUser != null)
		//		{
		//			email = !TextUtils.isEmpty(mEtEmail.getText().toString()) ? mEtEmail.getText().toString() : (!TextUtils.isEmpty(iUser.getEmail()) ? iUser.getEmail() : "");
		//			phoneNumber = !TextUtils.isEmpty(mEtPhone.getText().toString()) ? mEtPhone.getText().toString() : (!TextUtils.isEmpty(iUser.getPhoneNumber()) ? iUser.getPhoneNumber() : "");
		//			firstNameHe = !TextUtils.isEmpty(mEtFirstNameHe.getText().toString()) ? mEtFirstNameHe.getText().toString() : "";
		//			lastNameHe = !TextUtils.isEmpty(mEtLastNameHe.getText().toString()) ? mEtLastNameHe.getText().toString() : "";
		//			passportNumber = !TextUtils.isEmpty(mEtPassportNum.getText().toString()) ? mEtPassportNum.getText().toString() : (!TextUtils.isEmpty(iUser.getPassportNum()) ? iUser.getPassportNum() : "");
		//			//			expirationDate = !TextUtils.isEmpty(mEtPassportExpiration.getText().toString()) ?
		//			//			                 mEtPassportExpiration.getText().toString() :
		//			//			                 (iUser.getPassportExpiration() != null && !TextUtils.isEmpty(iUser.getPassportExpiration().toString()) ? iUser.getPassportExpiration().toString() : "");
		//			idNumber = !TextUtils.isEmpty(mEtIdNumber.getText().toString()) ? mEtIdNumber.getText().toString() : (!TextUtils.isEmpty(iUser.getIdNumber()) ? iUser.getIdNumber() : "");
		//		}
		//		else
		//		{
		email = mEtEmail.getText().toString();
		phoneNumber = mEtPhone.getText().toString();
		countryCode = mEtCountryCode.getText().toString();
		firstNameHe = mEtFirstNameHe.getText().toString();
		lastNameHe = mEtLastNameHe.getText().toString();
		passportNumber = mEtPassportNum.getText().toString();
		//			expirationDate = mEtPassportExpiration.getText().toString();
		idNumber = mEtIdNumber.getText().toString();
		//		}
		
		if (isServiceConnected())
		{
			if (mCalendarDOB != null)
			{
				mCalendarDOB.setTimeZone(TimeZone.getTimeZone("Etc/GMT"));
				mCalendarDOB.clear(Calendar.ZONE_OFFSET);//working!!1
			}
			
			if (mCalendarExpirationDate != null)
			{
				mCalendarExpirationDate.setTimeZone(TimeZone.getTimeZone("Etc/GMT"));
				mCalendarExpirationDate.clear(Calendar.ZONE_OFFSET);
			}
			getService().getController(LoginController.class)
			            .SetUserDetails(new RequestSetUserDetails(email, "", mCalendarExpirationDate, mEtLastNameEn.getText()
			                                                                                                       .toString(), passportNumber, mDescriptionList.get(mSpnrDescription.getSelectedItemPosition()), phoneNumber, countryCode, mEtFirstNameEn
					            .getText()
					            .toString(), UserData.getInstance().getUserID(), mCalendarDOB, idNumber, UserData.getInstance().getRepresentationId(), firstNameHe, lastNameHe, UserData.getInstance().getLanguage()
			                                                                                                                                                                            .getLanguageCode(), UserData
					            .getInstance()
					            .getPassword(), ""), new Response.Listener<ResponseSetUserDetails>()
			            {
				            @Override
				            public void onResponse(final ResponseSetUserDetails response)
				            {
					            UserData.getInstance().setEmail(email);
					            UserData.getInstance().setPhoneNumber(phoneNumber);
					            UserData.getInstance().setCountryCode(countryCode);
					            if (!mIsRegistration)
					            {
						            /**TODO Avishay 23/11/17 this line save the user data afterthe change details.
						             *I think we need check if(Prefs.isRememberMe) then save, else don't save!
						             * */
						            UserData.getInstance().saveUserData();
					            }
					
					            trySetProgressDialog(false);
					            doOnUpdateResponse(response);
				            }
			            }, this);
		}
	}
	
	private void doOnSetUserMatmidDetails()
	{
		final String email = mEtEmail.getText().toString();
		final String phoneNumber = mEtPhone.getText().toString();
		final String countryCode = mEtCountryCode.getText().toString();
		final String passportNum = mEtPassportNum.getText().toString();
		final String passportExpiration = mEtPassportExpiration.getText().toString();
		//		final String languageCode =  UserData.getInstance().getLanguage().getLanguageCode();
		final String memberId = UserData.getInstance().getUserObject().getMatmidMemberID();
		
		if (!TextUtils.isEmpty(memberId))
		{
			getService().getController(LoginController.class)
			            .SetUserMatmidDetails(new RequestSetUserMatmidDetails(RequestStringBuilder.isEnvironmentProd() ? null : UserData.getInstance().getUserObject().getSessionID()  /*UserData.getInstance()
			                                                                          .getUserObject()
			                                                                          .getSessionID()*/, email, phoneNumber, countryCode, passportNum, mCalendarExpirationDate, memberId), new Response.Listener<ResponseSetUserMatmidDetails>()
			            {
				            @Override
				            public void onResponse(final ResponseSetUserMatmidDetails response)
				            {
					            UserData.getInstance().getUserObject().setEmail(email);
					            UserData.getInstance().getUserObject().setPhoneNumber(phoneNumber);
					            UserData.getInstance().getUserObject().setCountryCode(countryCode);
					            UserData.getInstance().getUserObject().setPassportNum(passportNum);
					
					            UserData.getInstance().setEmail(email);
					            UserData.getInstance().setPhoneNumber(phoneNumber);
					            UserData.getInstance().setCountryCode(countryCode);
					            UserData.getInstance().setPassportNumber(passportNum);
					
					            if (mCalendarExpirationDate != null)
					            {
						            UserData.getInstance().getUserObject().setPassportExpiration(new Date(mCalendarExpirationDate.getTimeInMillis()));
					            }
					            trySetProgressDialog(false);
					            doOnUpdateMatmidResponse(response);
				            }
			            }, this);
		}
		else
		{//TODO avishay 25/06/17 handle it.
		
		}
	}
	
	private void doOnUpdateResponse(final ResponseSetUserDetails iResponse)
	{
		//successful set details. cancel the reminder dialog
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), AppUtils.getRemindMeDetailsKey(UserData.getInstance().getUserID()), false);
		if (mIsRegistration)
		{
			doOnOpenLetsContinueDialog();
		}
		else
		{
			goToLogin();
		}
	}
	
	private void doOnUpdateMatmidResponse(final ResponseSetUserMatmidDetails iResponse)
	{
		//successful set details. cancel the reminder dialog
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), AppUtils.getRemindMeDetailsKey(UserData.getInstance().getUserID()), false);
		
		
		if (mIsMatmid && UserData.getInstance().getUserObject().isMatmidFirstLogin())
		{
			doOnOpenLetsContinueDialog();
		}
		else
		{
			//			((EWBaseDrawerActivity) getActivity()).setDrawerAdapter();
			//			goToHomeFragment();
			goToLogin();
		}
	}
	
	private void doOnOpenLetsContinueDialog()
	{
		AppUtils.setSawReminderDialogFamily(true);
		String title = getString(R.string.almost_done);
		String message = getString(R.string.add_family_members);
		
		if (mIsMatmid)
		{
			title = getString(R.string.thanks_for_update_details);
			message = getString(R.string.you_can_also_update_family_members);
		}
		
		AlertDialog dialog = AppUtils.createSimpleMessageDialog(getActivity(), title, message, getString(R.string.let_me_continue), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, final int which)
			{
				
				Bundle bundle = new Bundle();
				bundle.putBoolean(IntentExtras.IS_REGISTRATION, mIsRegistration);
				bundle.putBoolean(IntentExtras.CREATE_NEW_MEMBER, true);
				
				tryGoToFragmentByFragmentClass(UserFamilyFragment.class, bundle, !isMatmidFirstLogin());
				
				//event 14
				if (mIsMatmid)
				{
					((EWBaseActivity) getActivity()).pushEvent(ePushEvents.LETS_CONTINUE_REGISTER_CLUB);
				}
				else
				{
					((EWBaseActivity) getActivity()).pushEvent(ePushEvents.LETS_CONTINUE_REGISTER_CUSTOMER);
				}
			}
		}, getString(R.string.remind_me_later), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, final int which)
			{
				ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), AppUtils.getRemindMeFamilyKey(UserData.getInstance().getUserID()), true);
				
				if (mIsMatmid)
				{
					//event 15
					((EWBaseActivity) getActivity()).pushEvent(ePushEvents.REMIND_ME_LATTER_REGISTER_CLUB);
					UserData.getInstance().getUserObject().setIsMatmidFirstLogin(false);
					goToHomeFragment();
				}
				else
				{
					//event 15
					((EWBaseActivity) getActivity()).pushEvent(ePushEvents.REMIND_ME_LATTER_REGISTER_CUSTOMER);
					goToLogin();
				}
			}
		}, getString(R.string.skip_underline), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, final int which)
			{
				ElalPreferenceUtils.setSharedPreference(getActivity(), AppUtils.getRemindMeFamilyKey(UserData.getInstance().getUserID()), false);
				
				if (mIsMatmid)
				{
					((EWBaseActivity) getActivity()).pushEvent(ePushEvents.NO_THANKS_REGISTER_CLUB);
					UserData.getInstance().getUserObject().setIsMatmidFirstLogin(false);
					goToHomeFragment();
				}
				else
				{
					((EWBaseActivity) getActivity()).pushEvent(ePushEvents.NO_THANKS_REGISTER);
					goToLogin();
				}
			}
		});
		dialog.setCancelable(false);
		dialog.show();
	}
	
	private boolean isMatmidFirstLogin()
	{
		return UserData.getInstance().getUserObject() != null && UserData.getInstance().getUserObject().isMatmid() && UserData.getInstance().getUserObject().isMatmidFirstLogin();
	}
	
	private void goToLogin()
	{
		if (getActivity() != null && getActivity() instanceof EWBaseUserActivity)
		{
			if (mIsMatmid/*((EWBaseUserActivity)getActivity()).isMatmidUser()*/)
			{
				((EWBaseUserActivity) getActivity()).performMatmidUserLoginWithExistingToken(UserData.getInstance().getUserObject().getMatmidMemberID(), UserData.getInstance()
				                                                                                                                                                 .getPassword(), new Response.Listener<ResponseMatmidLogin>()
				{
					@Override
					public void onResponse(final ResponseMatmidLogin response)
					{
						doOnSuccessfulRegisteredUserLogin(response);
					}
				}, this);
			}
			else
			{
				((EWBaseUserActivity) getActivity()).performRegisteredUserLogin(UserData.getInstance().getEmail()/*userObject.getEmail()*/, UserData.getInstance()
				                                                                                                                                    .getPassword()/*userObject.getUserPassword()*/, true, new Response.Listener<ResponseUserLogin>()
				{
					@Override
					public void onResponse(final ResponseUserLogin response)
					{
						doOnSuccessfulRegisteredUserLogin(response);
					}
				}, this);
			}
		}
	}
	
	private void doOnSuccessfulRegisteredUserLogin(final ILoginResponse iResponse)
	{
		trySetProgressDialog(false);
		
		if (iResponse != null && iResponse.getContent() != null)
		{
			eLanguage userLanguageInServer = eLanguage.getLanguageByCodeOrDefault("", eLanguage.English);
			
			if (iResponse.getContent() instanceof UserLogin)
			{
				UserData.getInstance().setUserId(((UserLogin) iResponse.getContent()).getUserID());
				UserData.getInstance().setUserObject(((UserLogin) iResponse.getContent()).getUserObject());
				userLanguageInServer = eLanguage.getLanguageByCodeOrDefault(((UserLogin) iResponse.getContent()).getUserObject().getLanguageID(), eLanguage.English);
			}
			if (!mIsRegistration)
			{
				UserData.getInstance().saveUserData();
			}
			
			//			if (userLanguageInServer != UserData.getInstance().getLanguage())
			//			{
			//				UserData.getInstance().setLanguage(userLanguageInServer);
			//				if (!mIsRegistration)
			//				{
			//					UserData.getInstance().saveUserData();
			//				}
			//				LocaleUtils.applyLocale(ElAlApplication.getInstance(), LocaleUtils.convertLanguageCodeToLocaleOrNull(userLanguageInServer.getLanguageCode()));
			//
			//				AlertDialog dialog = AppUtils.createSimpleMessageDialog(getActivity(), getString(R.string.restart_after_language_change), null, null, getString(R.string.close), new DialogInterface.OnClickListener()
			//				{
			//					@Override
			//					public void onClick(final DialogInterface dialog, final int which)
			//					{
			//						tryPopFragment();
			//					}
			//				});
			//				dialog.setCancelable(false);
			//				dialog.show();
			//			}
			//			else
			//			{
			((EWBaseDrawerActivity) getActivity()).setDrawerAdapter();
			goToHomeFragment();
			//			}
		}
	}
	
	private void goToHomeFragment()
	{
		if (getActivity() != null && getActivity() instanceof EWBaseUserActivity)
		{
			((EWBaseUserActivity) getActivity()).loadHomeFragment();
		}
	}
	
	private boolean isFirstNameEnValid()
	{
		return AppUtils.isStringContainsLatinCharactersOnly(mEtFirstNameEn.getText().toString());
	}
	
	private boolean isLastNameEnContainsLatinCharactersOnly()
	{
		return AppUtils.isStringContainsLatinCharactersOnly(mEtLastNameEn.getText().toString());
	}
	
	private boolean isLastNameEnContainsAtLeastTwoChar()
	{
		return AppUtils.isStringContainsAtLeastTwoChar(mEtLastNameEn.getText().toString());
	}
	
	private boolean isFirstNameHeValid()
	{
		//		return AppUtils.isStringContainsHebrewCharactersAndNumbersOnly(mEtFirstNameHe.getText().toString());
		return AppUtils.isStringContainsHebrewCharactersOnly(mEtFirstNameHe.getText().toString());
	}
	
	private boolean isLastNameHeValid()
	{
		//		return AppUtils.isStringContainsHebrewCharactersAndNumbersOnly(mEtLastNameHe.getText().toString());
		return AppUtils.isStringContainsHebrewCharactersOnly(mEtLastNameHe.getText().toString());
	}
	
	private boolean isDOBValid()
	{
		return !TextUtils.isEmpty(mEtDOB.getText().toString());
	}
	
	private boolean isEmailValid()
	{
		return AppUtils.isEmailValid(mEtEmail.getText().toString());
	}
	
	private boolean isPhoneNumberValid()
	{
		return AppUtils.isPhoneNumberValid(mEtPhone.getText().toString());
	}
	
	private boolean isCountryCodeValid()
	{
		return AppUtils.isCountryCodeValid(mEtCountryCode.getText().toString());
	}
	
	private boolean isFieldsValid()
	{
		boolean result = true;
		if (!isIdNumberValid(mEtIdNumber.getText().toString()))
		{
			result = false;
			showIdError(true);
		}
		
		if (!isPassportDetailsValid())
		{
			result = false;
		}
		
		if (!isDOBValid())
		{
			result = false;
			showDOBError(true);
			focusOnView(mSvMainContainer, mTilLastNameHe);
		}
		if (!mIsRegistration && !isPhoneNumberValid())
		{
			result = false;
			showPhoneNumberError(true);
			focusOnView(mSvMainContainer, mTilPhoneNumber);
		}
		
		if (!mIsRegistration && !isCountryCodeValid())
		{
			result = false;
			showCountryCodeError(true);
			focusOnView(mSvMainContainer, mTilCountryCode);
		}
		
		if (!isFirstNameEnValid())
		{
			result = false;
			showFirstNameEnError(true);
			focusOnView(mSvMainContainer, mTilFirstNameEn);
		}
		
		if (!isLastNameEnContainsLatinCharactersOnly())
		{
			result = false;
			showFamilyNameEnError(true, 1);
			focusOnView(mSvMainContainer, mTilLastNameEn);
		}
		
		if (!isLastNameEnContainsAtLeastTwoChar())
		{
			result = false;
			showFamilyNameEnError(true, 2);
			focusOnView(mSvMainContainer, mTilLastNameEn);
		}
		if (!isLastNameHeValid())
		{
			result = false;
			showLastNameHeError(true);
			focusOnView(mSvMainContainer, mTilLastNameHe);
		}
		
		if (!isFirstNameHeValid())
		{
			result = false;
			showFirstNameHeError(true);
			focusOnView(mSvMainContainer, mTilFirstNameHe);
		}
		
		if (!isTitleValid())
		{
			result = false;
			showTitleError(true);
			focusOnView(mSvMainContainer, mTilTitle);
		}
		
		
		if (!mIsRegistration && !isEmailValid())
		{
			result = false;
			showEmailError(true);
			focusOnView(mSvMainContainer, mTilEmail);
			
		}
		
		return result;
	}
	
	public boolean isTitleValid()
	{
		return !mDescriptionList.get(mSpnrDescription.getSelectedItemPosition()).toString().equals(getString(R.string.title_asterisk));
	}
	
	private boolean isPassportDetailsValid()
	{
		boolean result = true;
		if (!isPassportNumberEmpty())
		{
			if (!isPassportNumberValid())
			{
				showPassportNumberError(true, getActivity().getResources().getString(R.string.invalid_passport_number));
				result = false;
				focusOnView(mSvMainContainer, mTilPassportNum);
			}
			else
			{
				if (isPassportExpirationEmpty())
				{
					showPassportExpirationError(true, getActivity().getResources().getString(R.string.enter_expiration_date));
					result = false;
					//					focusOnViewBottomView(mSvMainContainer, mTilPassportExpiration);
					focusOnView(mSvMainContainer, mTilPassportNum);
				}
				else if (!isPassportExpirationValid())
				{
					showPassportExpirationError(true, getActivity().getResources().getString(R.string.invalid_expiration_date));
					result = false;
					//					focusOnViewBottomView(mSvMainContainer, mTilPassportExpiration);
					focusOnView(mSvMainContainer, mTilPassportNum);
				}
			}
			
		}
		else if (!isPassportExpirationEmpty())
		{
			if (!isPassportExpirationValid())
			{
				showPassportExpirationError(true, getActivity().getResources().getString(R.string.invalid_expiration_date));
				result = false;
				//				focusOnViewBottomView(mSvMainContainer, mTilPassportExpiration);
				focusOnView(mSvMainContainer, mTilPassportNum);
			}
			else
			{
				if (isPassportNumberEmpty())
				{
					showPassportNumberError(true, getActivity().getResources().getString(R.string.enter_passport_number));
					result = false;
					focusOnView(mSvMainContainer, mTilPassportNum);
				}
				
				else if (!isPassportNumberValid())
				{
					showPassportNumberError(true, getActivity().getResources().getString(R.string.invalid_passport_number));
					result = false;
					focusOnView(mSvMainContainer, mTilPassportNum);
				}
			}
			
		}
		
		return result;
	}
	
	private boolean isPassportNumberValid()
	{
		return AppUtils.isPassportNumberValid(mEtPassportNum.getText().toString());
	}
	
	public boolean isPassportNumberEmpty()
	{
		return TextUtils.isEmpty(mEtPassportNum.getText().toString());
	}
	
	private boolean isPassportExpirationValid()
	{
		if (mCalendarExpirationDate == null)
		{
			return false;
		}
		return AppUtils.isPassportExpirationValid(mCalendarExpirationDate.getTime());
	}
	
	public boolean isPassportExpirationEmpty()
	{
		return TextUtils.isEmpty(mEtPassportExpiration.getText().toString());
	}
	
	private boolean isFieldsMatmidValid()
	{
		boolean result = true;
		
		if (UserData.getInstance().getUserObject() != null && UserData.getInstance().getUserObject().isMatmidFirstLogin() && getActivity().findViewById(R.id.ll_agree_rules)
		                                                                                                                                  .getVisibility() == View.VISIBLE)
		{
			if (!mCbAgreeRules.isChecked())
			{
				result = false;
				showAgreeRulesError(true);
				focusOnView(mSvMainContainer, mCbAgreeRules);
			}
		}
		if (!isPassportDetailsValid())
		{
			result = false;
		}
		
		if (!isPhoneNumberValid())
		{
			result = false;
			showPhoneNumberError(true);
			focusOnView(mSvMainContainer, mTilPhoneNumber);
		}
		if (!isCountryCodeValid())
		{
			result = false;
			showCountryCodeError(true);
			focusOnView(mSvMainContainer, mTilCountryCode);
		}
		if (!isEmailValid())
		{
			result = false;
			showEmailError(true);
			focusOnView(mSvMainContainer, mTilEmail);
		}
		
		return result;
	}
	
	private void showTitleError(boolean iState)
	{
		if (iState)
		{
			mTilTitle.setError(true, mTilFirstNameEn.getOriginHintText());
		}
		else
		{
			mTilTitle.setError(false, null);
		}
	}
	
	private void showFirstNameEnError(boolean iState)
	{
		if (iState)
		{
			mTilFirstNameEn.setError(true, getActivity().getResources().getString(R.string.only_latin_alphabet));
		}
		else
		{
			mTilFirstNameEn.setError(false, null);
		}
	}
	
	private void showLastNameHeError(final boolean iState)
	{
		if (iState)
		{
			mTilLastNameHe.setError(true, getActivity().getResources().getString(R.string.only_hebrew_alphabet));
		}
		else
		{
			mTilLastNameHe.setError(false, null);
		}
	}
	
	private void showFirstNameHeError(boolean iState)
	{
		if (iState)
		{
			mTilFirstNameHe.setError(true, getActivity().getResources().getString(R.string.only_hebrew_alphabet));
		}
		else
		{
			mTilFirstNameHe.setError(false, null);
		}
	}
	
	private void showEmailError(boolean iState)
	{
		if (iState)
		{
			mTilEmail.setError(true, getActivity().getResources().getString(R.string.email_not_match));
		}
		else
		{
			mTilEmail.setError(false, null);
		}
	}
	
	private void showPhoneNumberError(boolean iState)
	{
		if (iState)
		{
			mTilPhoneNumber.setError(true, getActivity().getResources().getString(R.string.mobile_phone_number_incorrect));
		}
		else
		{
			mTilPhoneNumber.setError(false, null);
		}
	}
	
	private void showCountryCodeError(boolean iState)
	{
		if (iState)
		{
			mTilCountryCode.setError(true, getActivity().getResources().getString(R.string.mobile_country_code_incorrect));
		}
		else
		{
			mTilCountryCode.setError(false, null);
		}
	}
	
	private void showAgreeRulesError(boolean iState)
	{
		mTvAgreeRulesError.setVisibility(iState ? View.VISIBLE : View.INVISIBLE);
	}
	
	private void showPassportNumberError(boolean iState, String iText)
	{
		if (iState)
		{
			mTilPassportNum.setError(true, iText);
		}
		else
		{
			mTilPassportNum.setError(false, null);
		}
	}
	
	private void showPassportExpirationError(boolean iState, String iText)
	{
		if (iState)
		{
			mTilPassportExpiration.setError(true, iText);
		}
		else
		{
			mTilPassportExpiration.setError(false, null);
		}
	}
	
	private void showDOBError(boolean iState)
	{
		if (iState)
		{
			mTilDOB.setError(true, getActivity().getResources().getString(R.string.required_field));
		}
		else
		{
			mTilDOB.setError(false, null);
		}
	}
	
	private void showIdError(boolean iState)
	{
		if (iState)
		{
			mTilIdNumber.setError(true, getActivity().getResources().getString(R.string.id_number_error));
		}
		else
		{
			mTilIdNumber.setError(false, null);
		}
	}
	
	private void showFamilyNameEnError(boolean iState, int iType)
	{
		if (iState)
		{
			if (iType == 1)
			{
				mTilLastNameEn.setError(true, getActivity().getResources().getString(R.string.only_latin_alphabet));
			}
			else if (iType == 2)
			{
				mTilLastNameEn.setError(true, getActivity().getResources().getString(R.string.at_least_two_char_long));
			}
		}
		else
		{
			mTilLastNameEn.setError(false, null);
		}
	}
	
	private void animateCollapseExpandOfTitle(final boolean iShouldExpand)
	{
		if ((iShouldExpand && mTitleExpandCollapseAnimation != null && !mTitleExpandCollapseAnimation.isExpand()) || (!iShouldExpand && mTitleExpandCollapseAnimation.isExpand()))
		{
			mTitleExpandCollapseAnimation.reverseAndAnimate();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void onActivityResult(final int iRequestCode, final int iResultCode, final Intent data)
	{
		if (iResultCode == OCRManager.SCAN_STATUS.SCAN_STATUS_SUCCESS.getValue() && data != null && data.getExtras() != null && data.getExtras().containsKey(IntentExtras.DOCUMENT_SCAN_RESULTS) && data
				.getExtras()
				.get(IntentExtras.DOCUMENT_SCAN_RESULTS) instanceof IOcrResultObject)
		{
			doOnOcrDataReceived((IOcrResultObject) data.getExtras().get(IntentExtras.DOCUMENT_SCAN_RESULTS));
		}
	}
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_detail_screen_layout);
	}
	
	private void getCountryCodesList()
	{
		mCountryCodesArray = AppData.getInstance().getMobileCountryCodes();
	}
	
	private void doOnOcrDataReceived(final IOcrResultObject iOcrResultObject)
	{
		if (iOcrResultObject != null)
		{
			if (iOcrResultObject instanceof OcrPassport)
			{
				setPassportData(((OcrPassport) iOcrResultObject));
			}
			else if (iOcrResultObject instanceof OcrIsraelDriversLicense)
			{
				setIsraelDriversLicenseData(((OcrIsraelDriversLicense) iOcrResultObject));
			}
		}
	}
	
	public void setPassportData(final OcrPassport iPassportData)
	{
		if (!TextUtils.isEmpty(iPassportData.getGender()))
		{
			mSpnrDescription.setSelection(AppUtils.getSelectionInt(mDescriptionList, iPassportData.getGender()));
		}
		if (!TextUtils.isEmpty(iPassportData.getFirstName()))
		{
			setFirstNameEn(iPassportData.getFirstName());
		}
		if (!TextUtils.isEmpty(iPassportData.getLastName()))
		{
			setLastNameEn(iPassportData.getLastName());
		}
		if (mCalendarDOB == null)
		{
			mCalendarDOB = Calendar.getInstance();
		}
		if (iPassportData.getDateOfBirth() != null)
		{
			mCalendarDOB.setTime(iPassportData.getDateOfBirth());
		}
		if (!TextUtils.isEmpty(DateTimeUtils.convertCalendarToSlashSeparatedDayMonthYearString(iPassportData.getDateOfBirth())))
		{
			setEtDOBText(DateTimeUtils.convertCalendarToSlashSeparatedDayMonthYearString(iPassportData.getDateOfBirth()));
		}
		if (!TextUtils.isEmpty(iPassportData.getPassportNumber()))
		{
			setEtPassportNum(iPassportData.getPassportNumber());
		}
		if (mCalendarExpirationDate == null)
		{
			mCalendarExpirationDate = Calendar.getInstance();
		}
		if (iPassportData.getDateOfExpiration() != null)
		{
			mCalendarExpirationDate.setTime(iPassportData.getDateOfExpiration());
		}
		if (!TextUtils.isEmpty(DateTimeUtils.convertCalendarToSlashSeparatedDayMonthYearString(iPassportData.getDateOfExpiration())))
		{
			setTextToEtPassportExpiration(DateTimeUtils.convertCalendarToSlashSeparatedDayMonthYearString(iPassportData.getDateOfExpiration()));
		}
		if (!TextUtils.isEmpty(iPassportData.getIdNumber()) && isIdNumberValid((iPassportData.getIdNumber()).trim().replace(" ", "")))
		{
			setEtIdNumberText((iPassportData.getIdNumber()).trim().replace(" ", ""));
		}
	}
	
	public void setIsraelDriversLicenseData(final OcrIsraelDriversLicense iIsraelDriversLicenseData)
	{
		setFirstNameEn(iIsraelDriversLicenseData.getFirstNameEnglish());
		setLastNameEn(iIsraelDriversLicenseData.getLastNameEnglish());
		setFirstNameHe(iIsraelDriversLicenseData.getFirstNameHebrew());
		setLastNameHe(iIsraelDriversLicenseData.getLastNameHebrew());
		setEtDOBText(iIsraelDriversLicenseData.getDateOfBirth().toString());
		setTextToEtPassportExpiration(iIsraelDriversLicenseData.getDateOfExpiration());
		setEtIdNumberText(iIsraelDriversLicenseData.getIdNumber());
	}
	
	private void setFirstNameEn(String iText)
	{
		mEtFirstNameEn.setText(!TextUtils.isEmpty(iText) ? iText : "");
		//		tryToSetGravityIfHebrewAndNumericOnly(mEtFirstNameEn, iText);
	}
	
	private void setLastNameEn(String iText)
	{
		mEtLastNameEn.setText(!TextUtils.isEmpty(iText) ? iText : "");
		//		tryToSetGravityIfHebrewAndNumericOnly(mEtLastNameEn, iText);
	}
	
	private void setEtDOBText(String iText)
	{
		mEtDOB.setText(!TextUtils.isEmpty(iText) ? iText : "");
		//		tryToSetGravityIfHebrewAndNumericOnly(mEtDOB, iText);
	}
	
	private void setEtPassportNum(String iText)
	{
		mEtPassportNum.setText(!TextUtils.isEmpty(iText) ? iText : "");
		//		tryToSetGravityIfHebrewAndNumericOnly(mEtPassportNum, iText);
	}
	
	private void setTextToEtPassportExpiration(String iText)
	{
		mEtPassportExpiration.setText(!TextUtils.isEmpty(iText) ? iText : "");
		//		tryToSetGravityIfHebrewAndNumericOnly(mEtPassportExpiration, iText);
	}
	
	private boolean isIdNumberValid(String id)
	{
		return AppUtils.isIdValid(id);
	}
	
	private void setEtIdNumberText(String iText)
	{
		mEtIdNumber.setText(!TextUtils.isEmpty(iText) ? iText : "");
		//		tryToSetGravityIfHebrewAndNumericOnly(mEtIdNumber, iText);
	}
	
	private void setFirstNameHe(String iText)
	{
		mEtFirstNameHe.setText(!TextUtils.isEmpty(iText) ? iText : "");
		//		tryToSetGravityIfHebrewAndNumericOnly(mEtFirstNameHe, iText);
	}
	
	private void setLastNameHe(String iText)
	{
		mEtLastNameHe.setText(!TextUtils.isEmpty(iText) ? iText : "");
		//		tryToSetGravityIfHebrewAndNumericOnly(mEtLastNameHe, iText);
	}
	
	private void doOnSkip()
	{
		goToLogin();
	}
	
	@Override
	public void onErrorResponse(final VolleyError error)
	{
		doOnError(error);
		trySetProgressDialog(false);
		AppUtils.printLog(Log.ERROR, TAG, error.getLocalizedMessage());
	}
	
	private void doOnError(final VolleyError iError)
	{
		ErrorsHandler.tryShowServiceErrorDialog(iError, getActivity());
	}
	
	@Override
	public void doOnPermissionsGranted(final String[] iPermissions)
	{
		if (PermissionManager.isPermissionInArray(iPermissions, Manifest.permission.CAMERA))
		{
			doOnScan();
		}
	}
	
	private void doOnScan()
	{
		
		if (PermissionManager.isPermissionGranted(getActivity(), Manifest.permission.CAMERA))
		{
			if (getActivity() != null && getActivity() instanceof MainActivity)
			{
				goToScanDocumentActivityByType(GlobalVariables.OCR_TYPE_PASSPORT);
			}
		}
		else
		{
			PermissionManager.requestApplicationPermissionIfNeeded(getActivity(), Manifest.permission.CAMERA, this);
		}
		
		((EWBaseActivity) getActivity()).pushEvent(ePushEvents.SCAN_PASSPORT_SCAN);
	}
	
	private void goToScanDocumentActivityByType(@GlobalVariables.OcrType int iOcrType)
	{
		DocumentScanActivity.tryStartDocumentScanActivityForResult(getActivity(), DetailScreenFragment.this, iOcrType);
	}
	
	@Override
	public void doOnPermissionsDenied(final String[] iPermissions)
	{
		
	}
}
 