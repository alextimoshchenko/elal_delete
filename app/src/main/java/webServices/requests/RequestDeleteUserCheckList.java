package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with care by Shahar Ben-Moshe on 21/02/17.
 */

public class RequestDeleteUserCheckList
{
	@JsonProperty("UserItemID")
	private int mUserItemID;
	
	public RequestDeleteUserCheckList(final int iUserItemID)
	{
		mUserItemID = iUserItemID;
	}
}
