package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with care by Alexey.T on 25/07/2017.
 * <p>
 * TODO: Add a class header comment!
 */
public class RequestDeleteUserPushMessage
{
	@JsonProperty("UserID")
	private int mUserID;
	
	@JsonProperty("CampaignID")
	private int mCampaignID;
	
	@JsonProperty("Message")
	private String mMessage;
	
//	public RequestDeleteUserPushMessage()
//	{
//	}
	
//	public RequestDeleteUserPushMessage(final int iUserID, final int iCampaignID)
//	{
//		mUserID = iUserID;
//		mCampaignID = iCampaignID;
//	}
	
	
	public RequestDeleteUserPushMessage(final int iUserID, final int iCampaignID, final String iMessage)
	{
		mUserID = iUserID;
		mCampaignID = iCampaignID;
		mMessage = iMessage;
	}
}
