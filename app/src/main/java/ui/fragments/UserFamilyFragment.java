package ui.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import global.AppData;
import global.ElAlApplication;
import global.GlobalVariables;
import global.IntentExtras;
import global.UserData;
import global.eLanguage;
import global.ocr.OcrIsraelDriversLicense;
import global.ocr.OcrPassport;
import il.co.ewave.elal.R;
import interfaces.ILoginResponse;
import interfaces.IOcrResultObject;
import interfaces.IOnCancelClick;
import scanovate.ocr.common.OCRManager;
import ui.activities.DocumentScanActivity;
import ui.activities.EWBaseActivity;
import ui.activities.EWBaseDrawerActivity;
import ui.activities.EWBaseUserActivity;
import ui.activities.MainActivity;
import ui.adapters.SpinnerHintAdapter;
import ui.customWidgets.CustomScrollView;
import ui.customWidgets.ElalCustomTilLL;
import ui.customWidgets.ExpandCollapseAnimation;
import utils.errors.LocalError;
import utils.errors.ServerError;
import utils.global.AppUtils;
import utils.global.CustomTextWatcher;
import utils.global.DateTimeUtils;
import utils.global.LocaleUtils;
import utils.global.googleAnalitics.googleTagManager.TagManagerImplementation.ePushEvents;
import utils.permissions.IPermissionsListener;
import utils.permissions.PermissionManager;
import webServices.controllers.LoginController;
import webServices.global.Mapper;
import webServices.global.eActionType;
import webServices.requests.RequestSetMatmidUserFamily;
import webServices.requests.RequestSetRegularUserFamily;
import webServices.requests.RequestSetUserFamilyMember;
import webServices.responses.ResponseSetUserFamily.ResponseSetMatmidUserFamily;
import webServices.responses.ResponseSetUserFamily.ResponseSetRegularUserFamily;
import webServices.responses.ResponseUserLogin;
import webServices.responses.getAppData.FamilyType;
import webServices.responses.responseGetUserFamily.GetUserFamilyContent;
import webServices.responses.responseUserLogin.UserLogin;

public class UserFamilyFragment extends EWBaseScrollFragment implements Response.ErrorListener, IPermissionsListener
{
	private static final String TAG = UserFamilyFragment.class.getSimpleName();
	private static final int EXPAND_DURATION = 500;
	private TextView mTvSkip;
	private ImageView mIvBackArrow, mIvMatmidDrawableAccNumber;
	private LinearLayout mLlTitle, mLlPassportScan, mLlKeyboardReflector;
	private View mFlNumberAcc;
	private Spinner mSpnrRelation, mSpnrDescription;
	private ElalCustomTilLL mTilFirstNameEn, mTilLastNameEn, mTilDOB, mTilRelation, mTilTitle;
	private EditText mEtFirstNameEn, mEtLastNameEn, mEtDOB, mEtMatmidMainTextAccNumber, mEtRelation, mEtTitle;
	private Button mBtnDOB, mBtnAddMember;
	private List<String> mDescriptionList = new ArrayList<>();
	private ArrayList<FamilyType> mRelationList = new ArrayList<>();
	private CustomScrollView mScrollView;
	private ExpandCollapseAnimation mTitleExpandCollapseAnimation, mKeyboardReflectorExpandCollapseAnimation;
	private Calendar mCalendarDOB;
	private int mFamilyMemberId = 0;
	private TextView /*mTvMatmidFloatingHintAccNumber,*/ mTvMatmidErrorAccNumber;
	private boolean mIsRegistration;
	private boolean mIsNewMember;
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		initReference();
		setGui();
		setListeners();
		setDescriptionList();
		setRelationList();
		setSpinnerDescriptionAdapter();
		setSpinnerRelationAdapter();
		setExpandableViews();
		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		attachKeyboardListeners((ViewGroup) view.findViewById(R.id.ll_frag_user_family_root_view_group));
		pushScreenOpenEvent("Add Family Member");
		//		setDebugData();
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		getIntentExtras();
		
		((EWBaseDrawerActivity) getActivity()).enableLogoBtn(!mIsRegistration);
		
		if (mIsRegistration || (UserData.getInstance().getUserObject() != null && UserData.getInstance().getUserObject().isMatmid() && UserData.getInstance().getUserObject().isMatmidFirstLogin()))
		{
			switchToBackMode(false);
		}
		else
		{
			switchToBackMode(true);
		}
	}
	
	@Override
	public void onPause()
	{
		((EWBaseDrawerActivity) getActivity()).enableLogoBtn(mIsRegistration);
		super.onPause();
	}
	
	protected void onShowKeyboard(int keyboardHeight)
	{
		animateCollapseExpandOfKeyboardReflector(false);
	}
	
	protected void onHideKeyboard()
	{
		animateCollapseExpandOfKeyboardReflector(true);
	}
	
	private void animateCollapseExpandOfKeyboardReflector(final boolean iShouldExpand)
	{
		if (mKeyboardReflectorExpandCollapseAnimation == null)
		{
			return;
		}
		
		if ((iShouldExpand && !mKeyboardReflectorExpandCollapseAnimation.isExpand()) || (!iShouldExpand && mKeyboardReflectorExpandCollapseAnimation.isExpand()))
		{
			mKeyboardReflectorExpandCollapseAnimation.reverseAndAnimate();
		}
	}
	
	public void getIntentExtras()
	{
		if (getArguments() != null)
		{
			mIsRegistration = getArguments().getBoolean(IntentExtras.IS_REGISTRATION, false);
			mIsNewMember = getArguments().getBoolean(IntentExtras.CREATE_NEW_MEMBER, false);
			getActivity().findViewById(R.id.ll_scan_passport).setVisibility(mIsNewMember ? View.VISIBLE : View.GONE);
			
			if (!TextUtils.isEmpty(getArguments().getString(IntentExtras.FAMILY_OBJ)))
			{
				try
				{
					setFieldsByEdit(Mapper.objectOrThrow(getArguments().getString(IntentExtras.FAMILY_OBJ), GetUserFamilyContent.class));
					//					switchToBackMode(true);
					mBtnAddMember.setText(getActivity().getString(R.string.done));
				}
				catch (IOException iE)
				{
					AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.GeneralError + iE.getLocalizedMessage());
				}
			}
			else if (getArguments().getBoolean(IntentExtras.CREATE_NEW_MEMBER))
			{
				//				switchToBackMode(true);
				
			}
		}
	}
	
	private void switchToBackMode(boolean isBackMode)
	{
		mIvBackArrow.setVisibility(isBackMode ? View.VISIBLE : View.GONE);
		mTvSkip.setVisibility(isBackMode ? View.GONE : View.VISIBLE);
	}
	
	private void setFieldsByEdit(GetUserFamilyContent iFamilyMember)
	{
		mSpnrRelation.setSelection(iFamilyMember.getFamilyTypeID());
		mSpnrDescription.setSelection(getSelectionInt(mDescriptionList, iFamilyMember.getTitle()));
		mEtFirstNameEn.setText(iFamilyMember.getFirstNameEng());
		mEtLastNameEn.setText(iFamilyMember.getLastNameEng());
		setCalendarDOB(iFamilyMember.getDateOfBirth());
		mFamilyMemberId = iFamilyMember.getFamilyID();
	}
	
	private int getSelectionInt(List<String> arrayList, String str)
	{
		int result = 0;
		
		if (arrayList != null)
		{
			for (int i = 0 ; i < arrayList.size() ; i++)
			{
				String item = arrayList.get(i);
				
				if (!TextUtils.isEmpty(item))
				{
					if (item.trim().equalsIgnoreCase(str.trim()))
					{
						result = i;
						break;
					}
				}
				else
				{
					AppUtils.printLog(Log.ERROR, TAG, "eElectraError.DevelopmentError.name()");
				}
			}
		}
		
		return result;
	}
	
	private void setCalendarDOB(final Date iCalendarDOB)
	{
		if (iCalendarDOB != null)
		{
			if (mCalendarDOB == null)
			{
				mCalendarDOB = Calendar.getInstance();
			}
			
			mCalendarDOB.setTime(iCalendarDOB);
			mEtDOB.setText(DateTimeUtils.convertCalendarToSlashSeparatedDayMonthYearString(mCalendarDOB));
		}
	}
	
	private void setDebugData()
	{
		if (GlobalVariables.isDebugVersion)
		{
			mEtMatmidMainTextAccNumber.setText("109585836");
			mEtFirstNameEn.setText("ddd");
			mEtLastNameEn.setText("eee");
			mEtDOB.setText("10/06/2017");
		}
	}
	
	private void initReference()
	{
		mLlKeyboardReflector = getActivity().findViewById(R.id.ll_frag_user_family_keyboard_reflector);
		mTvSkip = getActivity().findViewById(R.id.tv_skip_layout_skip);
		mIvBackArrow = getActivity().findViewById(R.id.iv_skip_layout_back_arrow);
		mLlTitle = getActivity().findViewById(R.id.ll_frag_user_family_title);
		mLlPassportScan = getActivity().findViewById(R.id.ll_frag_user_family_passport_scan);
		mSpnrRelation = getActivity().findViewById(R.id.spnr_frag_user_family_relation);
		mSpnrDescription = getActivity().findViewById(R.id.spnr_frag_user_family_description);
		
		mTilFirstNameEn = getActivity().findViewById(R.id.til_frag_user_family_first_name_en);
		mTilFirstNameEn.init(getString(R.string.first_name_english));
		mTilFirstNameEn.setInputTypeTextOnly();
		mEtFirstNameEn = mTilFirstNameEn.getEditText();
		
		mTilLastNameEn = getActivity().findViewById(R.id.til_frag_user_family_last_name_en);
		mTilLastNameEn.init(getString(R.string.family_name_english));
		mTilLastNameEn.setInputTypeTextOnly();
		mEtLastNameEn = mTilLastNameEn.getEditText();
		mEtLastNameEn.setImeOptions(EditorInfo.IME_ACTION_DONE);
		
		mTilRelation = getActivity().findViewById(R.id.til_frag_user_family_relation);
		mTilRelation.init(getString(R.string.relation_asterisk));
		mEtRelation = mTilRelation.getEditText();
		mEtRelation.setHint("");
		mEtRelation.setCursorVisible(false);
		mEtRelation.setClickable(false);
		mEtRelation.setFocusable(false);
		
		
		mTilTitle = getActivity().findViewById(R.id.til_frag_user_family_title);
		mTilTitle.init(getString(R.string.title_asterisk));
		mEtTitle = mTilTitle.getEditText();
		mEtTitle.setHint("");
		mEtTitle.setCursorVisible(false);
		mEtTitle.setClickable(false);
		mEtTitle.setFocusable(false);
		
		mTilDOB = getActivity().findViewById(R.id.til_frag_user_family_date_of_birth);
		mTilDOB.init(getString(R.string.date_of_birth_asterisk));
		mEtDOB = mTilDOB.getEditText();
		
		mBtnDOB = getActivity().findViewById(R.id.btn_frag_user_family_date_of_birht);
		
		mBtnAddMember = getActivity().findViewById(R.id.btn_frag_user_family_add_member);
		mScrollView = getActivity().findViewById(R.id.scroll_frag_user_family);
		mFlNumberAcc = getActivity().findViewById(R.id.ll_frag_user_family_number_acc);
		//		mTvMatmidFloatingHintAccNumber = (TextView) mFlNumberAcc.findViewById(R.id.tv_floating_hint);
		mEtMatmidMainTextAccNumber = (TextInputEditText) mFlNumberAcc.findViewById(R.id.et_main_text);
		mIvMatmidDrawableAccNumber = mFlNumberAcc.findViewById(R.id.iv_drawable);
		mTvMatmidErrorAccNumber = mFlNumberAcc.findViewById(R.id.tv_error);
	}
	
	private void setGui()
	{
		((TextView) getActivity().findViewById(R.id.tv_header_skip_layout_title)).setText("");
		((TextView) getActivity().findViewById(R.id.tv_name_screen_title)).setText(getActivity().getResources().getString(R.string.family_account));
		getActivity().findViewById(R.id.iv_name_screen_back_arrow).setVisibility(View.GONE);
		
		if (UserData.getInstance().getUserObject() != null && UserData.getInstance().getUserObject().isMatmid())
		{
			mFlNumberAcc.setVisibility(View.VISIBLE);
			//			mTvMatmidFloatingHintAccNumber.setText(getString(R.string.member_number));
			//			mTvMatmidFloatingHintAccNumber.setVisibility(View.INVISIBLE);
			mEtMatmidMainTextAccNumber.setHint(getString(R.string.matmid_member_number));
			mEtMatmidMainTextAccNumber.setInputType(InputType.TYPE_CLASS_NUMBER);
			mEtMatmidMainTextAccNumber.setImeOptions(EditorInfo.IME_ACTION_DONE);
			mTvMatmidErrorAccNumber.setText(getString(R.string.matmid_acc_number_error));
			mTvMatmidErrorAccNumber.setVisibility(View.INVISIBLE);
			
			boolean firstLogin = UserData.getInstance().getUserObject().isMatmidFirstLogin();
			
			switchToBackMode(!UserData.getInstance().getUserObject().isMatmidFirstLogin());
		}
		else
		{
			mFlNumberAcc.setVisibility(View.GONE);
		}
		
		//		if (mIsRegistration)
		//		{
		//			switchToBackMode(false);
		//		}
		//		else
		//		{
		//			switchToBackMode(true);
		//		}
	}
	
	private void setListeners()
	{
		mSpnrRelation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
		{
			@Override
			public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id)
			{
				//				mEtRelation.setText(mSpnrRelation.getSelectedItem().toString()/*mRelationList.get(position)*/);
				showRelationError(false);
			}
			
			@Override
			public void onNothingSelected(final AdapterView<?> parent)
			{
			
			}
		});
		
		mSpnrDescription.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
		{
			@Override
			public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id)
			{
				//				mEtTitle.setText(mDescriptionList.get(position));
				showTitleError(false);
			}
			
			@Override
			public void onNothingSelected(final AdapterView<?> parent)
			{
			
			}
		});
		
		mIvMatmidDrawableAccNumber.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View iV)
			{
				AppUtils.showToolTip(getContext(), iV, getString(R.string.matmid_membership_number_tooltip));
			}
		});
		
		mTvSkip.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				if (UserData.getInstance().getUserObject() != null && UserData.getInstance().getUserObject().isMatmid())
				{
					UserData.getInstance().getUserObject().setIsMatmidFirstLogin(false);
					goToHomeFragment();
				}
				else
				{
					goToLogin();
				}
			}
		});
		
		mIvBackArrow.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				tryPopFragment();
			}
		});
		
		mBtnAddMember.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnAddMember();
			}
		});
		mBtnDOB.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				openDateOfBirthPickerDialog();
			}
		});
		mLlPassportScan.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnScan();
			}
		});
		mScrollView.setOnScrollListener(new CustomScrollView.OnScrollListener()
		{
			@Override
			public void onGoUp()
			{
				animateCollapseExpandOfTitle(true);
			}
			
			@Override
			public void onGoDown()
			{
				animateCollapseExpandOfTitle(false);
			}
		});
		mEtFirstNameEn.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				if (!TextUtils.isEmpty(s.toString()) && !AppUtils.isStringContainsLatinCharactersOnly(s.toString()))
				{
					showFirstNameEnError(true);
				}
				else
				{
					showFirstNameEnError(false);
				}
				
			}
		});
		mEtFirstNameEn.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus && !TextUtils.isEmpty(mEtFirstNameEn.getText().toString()) && !isFirstNameEnValid())
				{
					showFirstNameEnError(true);
				}
			}
		});
		
		mEtLastNameEn.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				if (!TextUtils.isEmpty(s.toString()) && !AppUtils.isStringContainsLatinCharactersOnly(s.toString()))
				{
					showFamilyNameEnError(true, 1);
				}
				else
				{
					showFamilyNameEnError(false, 1);
				}
				
			}
		});
		
		mEtLastNameEn.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus && !TextUtils.isEmpty(mEtLastNameEn.getText().toString()))
				{
					if (!isLastNameEnContainsLatinCharactersOnly())
					{
						showFamilyNameEnError(true, 1);
					}
					else if (!isLastNameEnContainsAtLeastTwoChar())
					{
						showFamilyNameEnError(true, 2);
					}
				}
			}
		});
		
		mEtDOB.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				showDOBError(false);
			}
		});
		
		mEtDOB.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus && !TextUtils.isEmpty(mEtDOB.getText().toString()) && !isDOBValid())
				{
					showDOBError(true);
				}
				else if (hasFocus)
				{
					openDateOfBirthPickerDialog();
				}
			}
		});
	}
	
	private void setSpinnerDescriptionAdapter()
	{
		//		ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), R.layout.custom_spinner_view, android.R.id.text1, mDescriptionList);
		//		adapter.setDropDownViewResource(R.layout.custom_drop_down);
		//		mSpnrDescription.setPrompt(getString(R.string.title_asterisk));
		//		mSpnrDescription.setAdapter(adapter);
		
		
		SpinnerHintAdapter adapter = new SpinnerHintAdapter(getContext(), R.layout.custom_spinner_details_layout,  /*android.R.id.text1, */mDescriptionList, 0/*, getString(R.string
		.title_asterisk)*/);
		adapter.setDropDownViewResource(R.layout.custom_drop_down);
		mSpnrDescription.setPrompt(getString(R.string.title_asterisk));
		mSpnrDescription.setAdapter(adapter);
		
	}
	
	private void setSpinnerRelationAdapter()
	{
		//				ArrayAdapter<FamilyType> adapter = new ArrayAdapter<>(getContext(), R.layout.custom_spinner_settings_layout_new, android.R.id.text1, mRelationList);
		//				adapter.setDropDownViewResource(R.layout.custom_drop_down);
		//				mSpnrRelation.setAdapter(adapter);
		
		
		SpinnerHintAdapter adapter = new SpinnerHintAdapter(getContext(), R.layout.custom_spinner_details_layout, /* android.R.id.text1,*/ mRelationList, 0/*, getString(R.string.relation_asterisk)*/);
		adapter.setDropDownViewResource(R.layout.custom_drop_down);
		mSpnrRelation.setPrompt(getString(R.string.relation_asterisk));
		mSpnrRelation.setAdapter(adapter);
	}
	
	private void setDescriptionList()
	{
		String[] list = getResources().getStringArray(R.array.description_values);
		mDescriptionList = Arrays.asList(list);
	}
	
	private void setRelationList()
	{
		
		ArrayList<FamilyType> familyTypes = AppData.getInstance().getFamilyTypes();
		if (familyTypes != null && familyTypes.size() > 0)
		{
			mRelationList.addAll(familyTypes);
			mRelationList.add(0, new FamilyType(getActivity().getString(R.string.relation_asterisk), -1));
		}
	}
	
	private void goToLogin()
	{
		if (getActivity() != null && getActivity() instanceof EWBaseUserActivity)
		{
			//			UserObject userObject = UserData.getInstance().getUserObject();
			//			if (UserData.getInstance().getUserObject() != null && UserData.getInstance().getUserObject().isMatmid())
			//			{
			//				((MainActivity) getActivity()).performMatmidUserLogin(UserData.getInstance().getUserObject().getMatmidMemberID(), UserData.getInstance()
			//				                                                                                                                          .getTemporaryPassword(), new Response.Listener<ResponseMatmidLogin>()
			//				{
			//					@Override
			//					public void onResponse(final ResponseMatmidLogin response)
			//					{
			//						doOnSuccessfulRegisteredUserLogin(response);
			//					}
			//				}, this);
			//			}
			//			else
			//			{
			((EWBaseUserActivity) getActivity()).performRegisteredUserLogin(UserData.getInstance().getEmail(), UserData.getInstance().getPassword(), true, new Response.Listener<ResponseUserLogin>()
			{
				@Override
				public void onResponse(final ResponseUserLogin response)
				{
					doOnSuccessfulRegisteredUserLogin(response);
				}
			}, this);
			//			}
		}
	}
	
	private void doOnSuccessfulRegisteredUserLogin(final ILoginResponse iResponse)
	{
		trySetProgressDialog(false);
		
		if (iResponse != null && iResponse.getContent() != null)
		{
			eLanguage userLanguageInServer = eLanguage.getLanguageByCodeOrDefault("", eLanguage.English);
			
			if (iResponse.getContent() instanceof UserLogin)
			{
				UserData.getInstance().setUserId(((UserLogin) iResponse.getContent()).getUserID());
				UserData.getInstance().setUserObject(((UserLogin) iResponse.getContent()).getUserObject());
				userLanguageInServer = eLanguage.getLanguageByCodeOrDefault(((UserLogin) iResponse.getContent()).getUserObject().getLanguageID(), eLanguage.English);
			}
			
			UserData.getInstance().saveUserData();
			
			if (userLanguageInServer != UserData.getInstance().getLanguage())
			{
				UserData.getInstance().setLanguage(userLanguageInServer);
				UserData.getInstance().saveUserData();
				LocaleUtils.applyLocale(ElAlApplication.getInstance(), LocaleUtils.convertLanguageCodeToLocaleOrNull(userLanguageInServer.getLanguageCode()));
				
				AlertDialog dialog = AppUtils.createSimpleMessageDialog(getActivity(), getString(R.string.restart_after_language_change), null, null, getString(R.string.close), new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(final DialogInterface dialog, final int which)
					{
						tryPopFragment();
					}
				});
				dialog.setCancelable(false);
				dialog.show();
			}
			else
			{
				goToHomeFragment();
			}
		}
	}
	
	private void goToHomeFragment()
	{
		if (getActivity() != null && getActivity() instanceof MainActivity)
		{
			((MainActivity) getActivity()).loadHomeFragment();
		}
	}
	
	private void openDateOfBirthPickerDialog()
	{
		if (mCalendarDOB == null)
		{
			mCalendarDOB = Calendar.getInstance();
			
			mCalendarDOB.set(Calendar.YEAR, 1986);
			mCalendarDOB.set(Calendar.MONTH, Calendar.JANUARY);
			mCalendarDOB.set(Calendar.DAY_OF_MONTH, 1);
		}
		
		AppUtils.createSimpleDatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener()
		{
			@Override
			public void onDateSet(final DatePicker view, final int year, final int month, final int dayOfMonth)
			{
				Date selectedDate = DateTimeUtils.convertYearMonthDayToCalendar(year, month, dayOfMonth).getTime();
				if (Calendar.getInstance().getTime().after(selectedDate))
				{
					setCalendarDOB(selectedDate);
				}
				mEtMatmidMainTextAccNumber.requestFocus();
			}
		}, mCalendarDOB, null, Calendar.getInstance(), new IOnCancelClick()
		{
			@Override
			public void cancelClick()
			{
				mEtDOB.clearFocus();
			}
		}).show();
	}
	
	private void showDOBError(boolean iState)
	{
		if (iState)
		{
			mTilDOB.setError(true, getActivity().getResources().getString(R.string.required_field));
		}
		else
		{
			mTilDOB.setError(false, null);
		}
	}
	
	//	private void showMatmidNumberError(boolean iState)
	//	{
	//		if (iState)
	//		{
	//			mTilMatmidMemberNumber.setHintTextAppearance(R.style.EditText_Login_Error);//Error(getActivity().getResources().getString(R.string.invalid_password));
	//			mTilMatmidMemberNumber.setError(" ");
	//		}
	//		else
	//		{
	//			mTilMatmidMemberNumber.setHintTextAppearance(R.style.EditText_Login_Hint);//setError(null);
	//			mTilMatmidMemberNumber.setError(null);
	//		}
	//	}
	
	
	@Override
	public void onDestroy()
	{
		if (UserData.getInstance().getUserObject() != null && UserData.getInstance().getUserObject().isMatmid())
		{
			UserData.getInstance().getUserObject().setIsMatmidFirstLogin(false);
		}
		super.onDestroy();
	}
	
	private void doOnAddMember()
	{
		if (!isFieldsValid())
		{
			return;
		}
		
		AppUtils.hideKeyboard(getActivity(), getView());
		doOnAddFamilyMember();
		((EWBaseActivity) getActivity()).pushEvent(ePushEvents.FAMILY_MEMBERS_DETAILS);
	}
	
	private boolean isMatmidNumberExist()
	{
		return mFlNumberAcc.getVisibility() == View.VISIBLE && !TextUtils.isEmpty(mEtMatmidMainTextAccNumber.getText().toString());
	}
	
	private void doOnAddFamilyMember()
	{
		if (!isServiceConnected())
		{
			return;
		}
		
		if (isMatmidNumberExist())
		{
			doOnSetMatmidUserFamily();
		}
		else
		{
			doOnSetRegularUserFamily();
		}
	}
	
	private void doOnSetMatmidUserFamily()
	{
		RequestSetMatmidUserFamily requestSetMatmidUserFamily = new RequestSetMatmidUserFamily(UserData.getInstance().getUserObject().getSessionID(), mEtMatmidMainTextAccNumber.getText()
		                                                                                                                                                                        .toString(), String.valueOf(mRelationList
				.get(mSpnrRelation.getSelectedItemPosition())
				.getFamilyTypeID()), mEtFirstNameEn.getText().toString(), mEtLastNameEn.getText().toString(), eActionType.ADD, UserData.getInstance().getUserObject().getMatmidMemberID(), 0);
		
		trySetProgressDialog(true);
		getService().getController(LoginController.class).SetMatmidUserFamily(requestSetMatmidUserFamily, new Response.Listener<ResponseSetMatmidUserFamily>()
		{
			@Override
			public void onResponse(final ResponseSetMatmidUserFamily response)
			{
				trySetProgressDialog(false);
				goToFamilyCardFragment();
			}
		}, new Response.ErrorListener()
		{
			@Override
			public void onErrorResponse(final VolleyError error)
			{
				doOnErrorTryAgain(ServerError.eServerError.MatmidFamilyNotSet.getErrorMessage());
				trySetProgressDialog(false);
				AppUtils.printLog(Log.ERROR, TAG, ServerError.eServerError.MatmidFamilyNotSet.getErrorMessage());
			}
		});
	}
	
	private boolean isFieldsValid()
	{
		boolean result = true;
		
		if (!isDOBValid())
		{
			result = false;
			showDOBError(true);
			focusOnViewBottomView(mScrollView, mTilDOB);
		}
		if (!isLastNameEnContainsLatinCharactersOnly())
		{
			result = false;
			showFamilyNameEnError(true, 1);
			focusOnView(mScrollView, mTilLastNameEn);
		}
		
		if (!isLastNameEnContainsAtLeastTwoChar())
		{
			result = false;
			showFamilyNameEnError(true, 2);
			focusOnView(mScrollView, mTilLastNameEn);
		}
		
		if (!isFirstNameEnValid())
		{
			result = false;
			showFirstNameEnError(true);
			focusOnView(mScrollView, mTilFirstNameEn);
		}
		
		if (!isTitleValid())
		{
			result = false;
			showTitleError(true);
			focusOnView(mScrollView, mTilTitle);
		}
		
		if (!isRelationValid())
		{
			result = false;
			showRelationError(true);
			focusOnView(mScrollView, mTilRelation);
		}
		
		return result;
	}
	
	public boolean isMatmidNumberValid(String iMatmidNumber)
	{
		return AppUtils.isMatmidNumberValid(iMatmidNumber);
	}
	
	private void doOnSetRegularUserFamily()
	{
		if (isServiceConnected())
		{
			trySetProgressDialog(true);
			
			if (mCalendarDOB != null)
			{
				//				mCalendarDOB.set(Calendar.HOUR_OF_DAY, 14);//just handle in GMT +02:00 issue. not finally solution
				//				mCalendarDOB.setTimeZone(TimeZone.getTimeZone("GMT0"));
				mCalendarDOB.setTimeZone(TimeZone.getTimeZone("Etc/GMT"));
				//				mCalendarDOB.setTimeZone(TimeZone.getTimeZone("Etc/GMT+0"));
				//				mCalendarDOB.setTimeZone(TimeZone.getTimeZone("Greenwich"));
				//				mCalendarDOB.setTimeZone(TimeZone.getTimeZone("Universal"));
				//
				//				mCalendarDOB.setTimeZone(TimeZone.getTimeZone("America/Caracas"));
				
				mCalendarDOB.clear(Calendar.ZONE_OFFSET);//working!!1
				
				//				TimeZone zone = TimeZone.getTimeZone("Etc/GMT");
				//				Calendar cal = Calendar.getInstance(zone);
				//				cal.set(Calendar.DAY_OF_MONTH, mCalendarDOB.get(Calendar.DAY_OF_MONTH));
				//				cal.set(Calendar.MONTH, mCalendarDOB.get(Calendar.MONTH));
				//				cal.set(Calendar.YEAR, mCalendarDOB.get(Calendar.YEAR));
				//
				//				mCalendarDOB = cal;
				
			}
			getService().getController(LoginController.class)
			            .SetRegularUserFamily(new RequestSetRegularUserFamily(UserData.getInstance().getUserID(), new RequestSetUserFamilyMember("", mEtFirstNameEn.getText()
			                                                                                                                                                       .toString(), mFamilyMemberId, mCalendarDOB, mEtLastNameEn
							            .getText()
							            .toString(), mRelationList.get(mSpnrRelation.getSelectedItemPosition()).getFamilyTypeID(), mDescriptionList.get(mSpnrDescription.getSelectedItemPosition())))
					
					            , new Response.Listener<ResponseSetRegularUserFamily>()
					            {
						            @Override
						            public void onResponse(final ResponseSetRegularUserFamily iResponse)
						            {
							            trySetProgressDialog(false);
							            goToFamilyCardFragment();
						            }
					            }, this);
		}
	}
	
	private boolean isDOBValid()
	{
		return !TextUtils.isEmpty(mEtDOB.getText().toString());
	}
	
	private void goToFamilyCardFragment()
	{
		Bundle bundle = new Bundle();
		bundle.putInt(IntentExtras.USER_ID, UserData.getInstance().getUserID());
		bundle.putBoolean(IntentExtras.IS_REGISTRATION, mIsRegistration);
		
		tryGoToFragmentByFragmentClass(FamilyCardFragment.class, bundle, false);
		//		if (mIsRegistration)
		//		{
		//			tryClearFragments();
		//		}
	}
	
	private boolean isLastNameEnContainsLatinCharactersOnly()
	{
		return AppUtils.isStringContainsLatinCharactersOnly(mEtLastNameEn.getText().toString());
	}
	
	private boolean isLastNameEnContainsAtLeastTwoChar()
	{
		return AppUtils.isStringContainsAtLeastTwoChar(mEtLastNameEn.getText().toString());
	}
	
	private void showFamilyNameEnError(boolean iState, int iType)
	{
		if (iState)
		{
			if (iType == 1)
			{
				mTilLastNameEn.setError(true, getActivity().getResources().getString(R.string.only_latin_alphabet));
			}
			else if (iType == 2)
			{
				mTilLastNameEn.setError(true, getActivity().getResources().getString(R.string.at_least_two_char_long));
			}
		}
		else
		{
			mTilLastNameEn.setError(false, null);
		}
	}
	
	public boolean isRelationValid()
	{
		if (mRelationList != null)
		{
			if (!mRelationList.isEmpty())
			{
				return !mRelationList.get(mSpnrRelation.getSelectedItemPosition()).toString().equals(getString(R.string.relation_asterisk));
			}
//			else
//			{
//				return true;
//			}
		}
		return false;
	}
	
	public boolean isTitleValid()
	{
		return !mDescriptionList.get(mSpnrDescription.getSelectedItemPosition()).toString().equals(getString(R.string.title_asterisk));
	}
	
	private boolean isFirstNameEnValid()
	{
		return AppUtils.isStringContainsLatinCharactersOnly(mEtFirstNameEn.getText().toString());
	}
	
	private void showFirstNameEnError(boolean iState)
	{
		if (iState)
		{
			mTilFirstNameEn.setError(true, getActivity().getResources().getString(R.string.only_latin_alphabet));
		}
		else
		{
			mTilFirstNameEn.setError(false, null);
		}
	}
	
	private void showTitleError(boolean iState)
	{
		if (iState)
		{
			mTilTitle.setError(true, mTilFirstNameEn.getOriginHintText());
		}
		else
		{
			mTilTitle.setError(false, null);
		}
	}
	
	private void showRelationError(boolean iState)
	{
		if (iState)
		{
			mTilRelation.setError(true, mTilRelation.getOriginHintText());
		}
		else
		{
			mTilRelation.setError(false, null);
		}
	}
	
	private void animateCollapseExpandOfTitle(final boolean iShouldExpand)
	{
		if ((iShouldExpand && !mTitleExpandCollapseAnimation.isExpand()) || (!iShouldExpand && mTitleExpandCollapseAnimation.isExpand()))
		{
			mTitleExpandCollapseAnimation.reverseAndAnimate();
		}
	}
	
	private void setExpandableViews()
	{
		mLlTitle.post(new Runnable()
		{
			@Override
			public void run()
			{
				int expandedViewHeight = mLlTitle.getHeight();
				mTitleExpandCollapseAnimation = new ExpandCollapseAnimation(mLlTitle, true, EXPAND_DURATION, expandedViewHeight);
			}
		});
		
		mLlKeyboardReflector.post(new Runnable()
		{
			@Override
			public void run()
			{
				int expandedViewHeight = mLlKeyboardReflector.getHeight();
				mKeyboardReflectorExpandCollapseAnimation = new ExpandCollapseAnimation(mLlKeyboardReflector, true, EXPAND_DURATION, expandedViewHeight);
			}
		});
	}
	
	private void onScanDocumentsClick()
	{
		if (getActivity() != null && getActivity() instanceof MainActivity)
		{
			//			((RegistrationActivity) getActivity()).loadFragmentToContainer(DocumentScanSelectionFragment.class, DocumentScanSelectionFragment.createBundleForFragmentResult(this));
			tryGoToFragmentByFragmentClass(DocumentScanSelectionFragment.class, DocumentScanSelectionFragment.createBundleForFragmentResult(this), true);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void onActivityResult(final int iRequestCode, final int iResultCode, final Intent data)
	{
		//		if (iRequestCode == DocumentScanActivity.DOCUMENT_SCAN_REQUEST_CODE)
		//		{
		//			if (iResultCode == OCRManager.SCAN_STATUS.SCAN_STATUS_SUCCESS.getValue() && data != null && data.getExtras() != null && data.getExtras()
		//			                                                                                                                            .containsKey(IntentExtras.DOCUMENT_SCAN_RESULTS) && data.getExtras()
		//			                                                                                                                                                                                    .get(IntentExtras.DOCUMENT_SCAN_RESULTS) instanceof IOcrResultObject)
		//			{
		//				doOnOcrDataReceived((IOcrResultObject) data.getExtras().get(IntentExtras.DOCUMENT_SCAN_RESULTS));
		//			}
		//		}
		if (iResultCode == OCRManager.SCAN_STATUS.SCAN_STATUS_SUCCESS.getValue() && data != null && data.getExtras() != null && data.getExtras().containsKey(IntentExtras.DOCUMENT_SCAN_RESULTS) && data
				.getExtras()
				.get(IntentExtras.DOCUMENT_SCAN_RESULTS) instanceof IOcrResultObject)
		{
			doOnOcrDataReceived((IOcrResultObject) data.getExtras().get(IntentExtras.DOCUMENT_SCAN_RESULTS));
		}
	}
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_user_family_layout);
	}
	
	private void doOnOcrDataReceived(final IOcrResultObject iOcrResultObject)
	{
		if (iOcrResultObject != null)
		{
			if (iOcrResultObject instanceof OcrPassport)
			{
				setPassportData(((OcrPassport) iOcrResultObject));
			}
			else if (iOcrResultObject instanceof OcrIsraelDriversLicense)
			{
				setIsraelDriversLicenseData(((OcrIsraelDriversLicense) iOcrResultObject));
			}
		}
	}
	
	public void setPassportData(final OcrPassport iPassportData)
	{
		
		if (!TextUtils.isEmpty(iPassportData.getGender()))
		{
			mSpnrDescription.setSelection(AppUtils.getSelectionInt(mDescriptionList, iPassportData.getGender()));
		}
		if (!TextUtils.isEmpty(iPassportData.getFirstName()))
		{
			mEtFirstNameEn.setText(iPassportData.getFirstName());
		}
		if (!TextUtils.isEmpty(iPassportData.getLastName()))
		{
			mEtLastNameEn.setText(iPassportData.getLastName());
		}
		if (mCalendarDOB == null)
		{
			mCalendarDOB = Calendar.getInstance();
		}
		if (iPassportData.getDateOfBirth() != null)
		{
			mCalendarDOB.setTime(iPassportData.getDateOfBirth());
		}
		if (!TextUtils.isEmpty(DateTimeUtils.convertCalendarToSlashSeparatedDayMonthYearString(iPassportData.getDateOfBirth())))
		{
			mEtDOB.setText(DateTimeUtils.convertCalendarToSlashSeparatedDayMonthYearString(iPassportData.getDateOfBirth()));
		}
		
	}
	
	//	private void doOnScan()
	//	{
	//		onScanDocumentsClick();
	//	}
	
	public void setIsraelDriversLicenseData(final OcrIsraelDriversLicense iIsraelDriversLicenseData)
	{
		mEtFirstNameEn.setText(iIsraelDriversLicenseData.getFirstNameEnglish());
		mEtLastNameEn.setText(iIsraelDriversLicenseData.getLastNameEnglish());
		mEtDOB.setText(iIsraelDriversLicenseData.getDateOfBirth().toString());
	}
	
	@Override
	public void doOnPermissionsGranted(final String[] iPermissions)
	{
		if (PermissionManager.isPermissionInArray(iPermissions, Manifest.permission.CAMERA))
		{
			doOnScan();
		}
	}
	
	private void doOnScan()
	{
		if (PermissionManager.isPermissionGranted(getActivity(), Manifest.permission.CAMERA))
		{
			if (getActivity() != null && getActivity() instanceof MainActivity)
			{
				goToScanDocumentActivityByType(GlobalVariables.OCR_TYPE_PASSPORT);
			}
		}
		else
		{
			PermissionManager.requestApplicationPermissionIfNeeded(getActivity(), Manifest.permission.CAMERA, this);
		}
		
		((EWBaseActivity) getActivity()).pushEvent(ePushEvents.SCAN_PASSPORT);
	}
	
	private void goToScanDocumentActivityByType(@GlobalVariables.OcrType int iOcrType)
	{
		DocumentScanActivity.tryStartDocumentScanActivityForResult(getActivity(), UserFamilyFragment.this, iOcrType);
	}
	
	@Override
	public void doOnPermissionsDenied(final String[] iPermissions)
	{
	
	}
	
	@Override
	public void onErrorResponse(final VolleyError error)
	{
		//		doOnError(((ServerError) error).getErrorMessage());
		AppUtils.createSimpleMessageDialog(getActivity(), getContext().getString(R.string.error_L3_timeout), getString(R.string._continue), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, final int which)
			{
				doOnSetRegularUserFamily();
			}
		}, getString(R.string.close), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, final int which)
			{
				dialog.dismiss();
			}
		}).show();
		trySetProgressDialog(false);
		AppUtils.printLog(Log.ERROR, TAG, error.getLocalizedMessage());
	}
	
	private void doOnError(final String iErrorMsg)
	{
		AppUtils.createSimpleMessageDialog(getActivity(), iErrorMsg, getString(R.string._continue), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, final int which)
			{
				doOnSetRegularUserFamily();
			}
		}, getString(R.string.close), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, final int which)
			{
				dialog.dismiss();
			}
		}).show();
	}
	
	private void doOnErrorTryAgain(final String iErrorMsg)
	{
		AppUtils.createSimpleMessageDialog(getActivity(), iErrorMsg, getString(R.string.try_again), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, final int which)
			{
				dialog.dismiss();
			}
		}, getString(R.string._continue), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, final int which)
			{
				doOnSetRegularUserFamily();
			}
		}).show();
	}
	
}
