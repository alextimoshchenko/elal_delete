package ui.fragments;

import android.Manifest;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tagmanager.DataLayer;

import java.io.File;
import java.util.ArrayList;

import global.IntentExtras;
import global.UserData;
import il.co.ewave.elal.R;
import io.realm.RealmObject;
import ui.activities.EWBaseActivity;
import utils.global.AppUtils;
import utils.global.CameraUtil;
import utils.global.dbObjects.Document;
import utils.global.googleAnalitics.googleTagManager.TagManagerImplementation;
import utils.managers.RealmManager;
import utils.managers.TabsBackStackManager;
import utils.permissions.IPermissionsListener;
import utils.permissions.PermissionManager;
import webServices.responses.responseGetUserActivePNRs.UserActivePnr;

/**
 * Created with care by Shahar Ben-Moshe on 27/02/17.
 */

public class AddNewDocumentFragment extends EWBaseFragment implements RealmManager.IRealmListener<RealmObject>, IPermissionsListener
{
	private static final String TAG = AddNewDocumentFragment.class.getSimpleName();
	
	private static final int HINT_MARGIN = -30;
	private static final int HINT_MARGIN_ZERO = 0;
	private static final int HINT_ANIMATION_DURATION = 800;
	
	private ImageView mImvDocumentSaved;
	private Spinner mSpnrDocumentType;
	private EditText mEtDocumentTitle;
	private FrameLayout mBtnFromGallery;
	private FrameLayout mBtnPhotoDocument;
	private Button mBtnSave;
	private Bitmap mSelectedImage;
	private Document mDocument;
	private UserActivePnr mUserActivePnr;
	private int mDocumentTypeId = -1;
	private String mImgPath;
	private TextView mTxvDocTypeTitle;
	private TextView mTxvDocumentTitle;
	private LinearLayout mLilDocumentType;
	private boolean mShowSelectType;
	private ImageView mIvClose;
	private ImageView mBtnClearName;
	private boolean mAlreadySaved = false;
	
	public static Bundle createBundleForFragment(@NonNull final UserActivePnr iUserActivePnr, final Parcelable iDocument, final int iDocumentTypeId, boolean iShouldSelectType)
	{
		Bundle result = new Bundle();
		
		result.putParcelable(IntentExtras.USER_ACTIVE_PNR, iUserActivePnr);
		result.putParcelable(IntentExtras.DOCUMENT, iDocument);
		result.putInt(IntentExtras.DOCUMENT_TYPE, iDocumentTypeId);
		result.putBoolean(IntentExtras.SELECT_TYPE, iShouldSelectType);
		
		return result;
	}
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_add_new_document);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		getIntentExtras();
		initReference();
		setListeners();
		
		setDocumentTypeSpinnerAdapter();
	}
	
	private void hideDocumentTypeSpinner()
	{
		if (mLilDocumentType != null)
		{
			mLilDocumentType.setVisibility(View.GONE);
		}
	}
	
	private void getIntentExtras()
	{
		if (getArguments() != null)
		{
			mUserActivePnr = getArguments().getParcelable(IntentExtras.USER_ACTIVE_PNR);
			mDocument = getArguments().getParcelable(IntentExtras.DOCUMENT);
			mDocumentTypeId = getArguments().getInt(IntentExtras.DOCUMENT_TYPE);
			mShowSelectType = getArguments().getBoolean(IntentExtras.SELECT_TYPE, true);
		}
	}
	
	private void initReference()
	{
		mIvClose = (ImageView) getActivity().findViewById(R.id.iv_addDocument_Close);
		mLilDocumentType = (LinearLayout) getActivity().findViewById(R.id.lil_document_type_holder);
		mSpnrDocumentType = (Spinner) getActivity().findViewById(R.id.spnr_addNewDocument_DocumentType);
		mTxvDocTypeTitle = (TextView) getActivity().findViewById(R.id.txv_doc_type_title);
		mImvDocumentSaved = (ImageView) getActivity().findViewById(R.id.imv_DocumentSaved);
		mTxvDocumentTitle = (TextView) getActivity().findViewById(R.id.txv_addNewDocument_DocumentTitle);
		mEtDocumentTitle = (EditText) getActivity().findViewById(R.id.et_addNewDocument_DocumentTitle);
		mBtnClearName = (ImageView) getActivity().findViewById(R.id.btnClearName);
		mBtnFromGallery = (FrameLayout) getActivity().findViewById(R.id.btn_addNewDocument_FromGallery);
		mBtnPhotoDocument = (FrameLayout) getActivity().findViewById(R.id.btn_addNewDocument_PhotoDocument);
		mBtnSave = (Button) getActivity().findViewById(R.id.btn_addNewDocument_Save);
		
		FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) mBtnClearName.getLayoutParams();
		if (AppUtils.isDefaultLocaleRTL())
		{
			params.gravity = Gravity.LEFT;
		}
		else
		{
			params.gravity = Gravity.RIGHT;
		}
		
		mBtnClearName.setLayoutParams(params);
	}
	
	private void setListeners()
	{
		mBtnFromGallery.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				onFromGalleryClick();
			}
		});
		
		mBtnPhotoDocument.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				onPhotoDocumentClick();
			}
		});
		
		mBtnSave.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				onSaveClick();
			}
		});
		
		mIvClose.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				tryPopFragment();
				
				//event 56
				((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight" + TagManagerImplementation.TIME_SCOPE, "Action", TagManagerImplementation.TIME_SCOPE + UserData.getInstance()
				                                                                                                                                                                                            .getActivePnr() + " >> Save", "Label", "All travel documents"));
			}
		});
		
		mEtDocumentTitle.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(View view, boolean hasFocus)
			{
				animateTitle(hasFocus);
			}
		});
		
		mBtnClearName.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				if (mEtDocumentTitle != null)
				{
					mEtDocumentTitle.setText("");
					mEtDocumentTitle.clearFocus();
				}
			}
		});
	}
	
	private void animateTitle(boolean hasFocus)
	{
		int currentValue = hasFocus ? HINT_MARGIN_ZERO : HINT_MARGIN;
		int margin = hasFocus ? HINT_MARGIN : HINT_MARGIN_ZERO;
		
		ValueAnimator vAnim = ValueAnimator.ofInt(margin, currentValue);
		vAnim.setDuration(HINT_ANIMATION_DURATION);
		vAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
		{
			@Override
			public void onAnimationUpdate(ValueAnimator animation)
			{
				LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) mTxvDocumentTitle.getLayoutParams();
				lp.setMargins(0, 0, 0, (Integer) animation.getAnimatedValue());
				mTxvDocumentTitle.setLayoutParams(lp);
			}
		});
		vAnim.start();
	}
	
	private void setDocumentTypeSpinnerAdapter()
	{
		if (!mShowSelectType)
		{
			hideDocumentTypeSpinner();
		}
		
		final Document.eDocumentType[] titlesArray = Document.eDocumentType.values();
		final ArrayAdapter<Document.eDocumentType> spinnerAdapter = new ArrayAdapter<>(getActivity(), R.layout.custom_light_gray_spinner_layout, android.R.id.text1, titlesArray);
		spinnerAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
		mSpnrDocumentType.setAdapter(spinnerAdapter);
		
		if (mDocumentTypeId > -1)
		{
			for (int i = 0 ; i < titlesArray.length ; i++)
			{
				Document.eDocumentType documentType = titlesArray[i];
				if (documentType != null && documentType.getDocumentTypeId() == mDocumentTypeId)
				{
					setDocTypeTitle(documentType.getNameResourceId());
					mSpnrDocumentType.setSelection(i);
					break;
				}
			}
		}
		
		mSpnrDocumentType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
		{
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l)
			{
				Document.eDocumentType documentType = titlesArray[position];
				if (documentType != null)
				{
					setDocTypeTitle(documentType.getNameResourceId());
				}
			}
			
			@Override
			public void onNothingSelected(AdapterView<?> adapterView)
			{
			}
		});
	}
	
	private void setDocTypeTitle(int nameResourceId)
	{
		if (mTxvDocTypeTitle != null)
		{
			mTxvDocTypeTitle.setText(getResources().getString(R.string.add_document) + " " + getResources().getString(nameResourceId));
		}
	}
	
	private void onFromGalleryClick()
	{
		try
		{
			File fileToSave = CameraUtil.createImageFile(getContext());
			mImgPath = fileToSave.getAbsolutePath();
			
			CameraUtil.startGalleryIntent(this);
			
			//event 59
			String activePnr = UserData.getInstance().getActivePnr();
			((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight" + TagManagerImplementation.TIME_SCOPE, "Action", TagManagerImplementation.TIME_SCOPE + activePnr + " >> Save", "Label",
					"Document Type > Add " + "from gallery"));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Toast.makeText(getContext(), getContext().getString(R.string.error_L3_timeout), Toast.LENGTH_SHORT).show();
		}
	}
	
	private void onPhotoDocumentClick()
	{
		int permissionReadImages = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
		int permissionWrite = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
		int permissionCamera = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA);
		ArrayList<String> listPermissionsNeeded = new ArrayList<>();
		if (permissionReadImages != PackageManager.PERMISSION_GRANTED && permissionWrite != PackageManager.PERMISSION_GRANTED && permissionCamera != PackageManager.PERMISSION_GRANTED)
		{
			listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
			listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
			listPermissionsNeeded.add(Manifest.permission.CAMERA);
			
			PermissionManager.requestApplicationPermissionsIfNeeded(getActivity(), listPermissionsNeeded, this);
		}
		else
		{
			try
			{
				File fileToSave = CameraUtil.createImageFile(getContext());
				mImgPath = fileToSave.getAbsolutePath();
				
				CameraUtil.startCameraIntent(this, fileToSave);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				Toast.makeText(getContext(), getContext().getString(R.string.error_L3_timeout), Toast.LENGTH_SHORT).show();
			}
		}
		
		//event 58
		String activePnr = UserData.getInstance().getActivePnr();
		((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight" + TagManagerImplementation.TIME_SCOPE, "Action", TagManagerImplementation.TIME_SCOPE + activePnr + " >> Save", "Label", "Document " +
				"Type > Take a picture"));
	}
	
	@Override
	public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults)
	{
		PermissionManager.handlePermissionResponse(requestCode, permissions, grantResults, this);
	}
	
	@Override
	public void doOnPermissionsGranted(final String[] iPermissions)
	{
		if (PermissionManager.isPermissionInArray(iPermissions, Manifest.permission.CAMERA))
		{
			onPhotoDocumentClick();
		}
	}
	
	@Override
	public void doOnPermissionsDenied(final String[] iPermissions)
	{
	}
	
	private void onSaveClick()
	{
		if (isAllFieldsValid())
		{
			trySetProgressDialog(true);
			if (!mAlreadySaved)
			{
				saveImageInDb();
			}
			
			//event 60
			String activePnr = UserData.getInstance().getActivePnr();
			int docType = mDocument.getType();
			((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight" + TagManagerImplementation.TIME_SCOPE, "Action", TagManagerImplementation.TIME_SCOPE +
					activePnr + " >> " + "Save", "Label", docType + " > Save"));
		}
		else
		{
			showErrorMessage();
		}
	}
	
	private boolean isAllFieldsValid()
	{
		return mSelectedImage != null;
	}
	
	private void saveImageInDb()
	{
		mAlreadySaved = true;
		int documentType = Document.eDocumentType.values()[mSpnrDocumentType.getSelectedItemPosition()].getDocumentTypeId();
		String documentName = mEtDocumentTitle.getText().toString();
		String documentImageBase64 = AppUtils.convertBitmapToBase64(mSelectedImage);
		
		if (mDocument == null)
		{
			mDocument = new Document(mUserActivePnr.getPnr(), UserData.getInstance().getUserID(), documentType, documentName, documentImageBase64);
		}
		else
		{
			mDocument.setType(documentType);
			mDocument.setName(documentName);
			mDocument.setData(documentImageBase64);
		}
		
		RealmManager.getInstance().insertDocument(mDocument, this);
	}
	
	private void showErrorMessage()
	{
		AppUtils.createSimpleMessageDialog(getActivity(), getString(R.string.no_document_is_attached), getString(R.string.confirm), null, null, null).show();
	}
	
	@Override
	public void onActivityResult(final int iRequestCode, final int iResultCode, final Intent iData)
	{
		mSelectedImage = CameraUtil.resolveSelectedPhotoToBitmap(getActivity(), iRequestCode, iResultCode, iData, mImgPath);
		if (mImvDocumentSaved != null)
		{
			mImvDocumentSaved.setVisibility(View.VISIBLE);
		}
	}
	
	@Override
	public void onSuccess(final ArrayList<RealmObject> iResultObjects)
	{
		trySetProgressDialog(false);
		//		tryPopFragment();
		goToMyDocsFragment();
	}
	
	@Override
	public void onFailed()
	{
		mAlreadySaved = false;
		trySetProgressDialog(false);
		showErrorMessage();
		Log.d(TAG, "save document failed");
	}
	
	private void goToMyDocsFragment()
	{
		Bundle bundle = MyDocumentsFragment.createBundleForFragment(mUserActivePnr, true, mUserActivePnr.getFlightsCount());
		
		if (getParentFragment() != null && getParentFragment() instanceof TabsBackStackManager.ITabsBackStackManagerHandler)
		{
			MyDocumentsFragment myDocumentsFragment = new MyDocumentsFragment();
			myDocumentsFragment.setArguments(bundle);
			
			((TabsBackStackManager.ITabsBackStackManagerHandler) getParentFragment()).pushFragmentToCurrentTab(myDocumentsFragment, true, true);
		}
		else
		{
			tryGoToFragmentByFragmentClass(MyDocumentsFragment.class, bundle, true);
		}
	}
}
