package ui.customWidgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import il.co.ewave.elal.R;
import utils.global.AppUtils;

/**
 * Created by Avishay.Peretz on 19/07/2017.
 */

public class ChangeDirectionTextInputLayout extends TextInputLayout
{
	
	private final Context mContext;
	private TextView mTvErrorText;
	private boolean mIsRTL = false;
 	
	
	public ChangeDirectionTextInputLayout(final Context context)
	{
		super(context);
		mContext = context;
		initTvErrorText(context);
 	}
	
	public ChangeDirectionTextInputLayout(final Context context, final AttributeSet attrs)
	{
		super(context, attrs);
		mContext = context;
		handleAttributes(context, attrs);
		initTvErrorText(context);
 	}
	
	public ChangeDirectionTextInputLayout(final Context context, final AttributeSet attrs, final int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
		mContext = context;
		initTvErrorText(context);
 	}
	
	private void initTvErrorText(Context iContext)
	{
		try
		{
			Field fErrorView = TextInputLayout.class.getDeclaredField("mErrorView");
			fErrorView.setAccessible(true);
			mTvErrorText = (TextView) fErrorView.get(this);
			mTvErrorText.setTextColor(Color.GREEN);
			LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f);
			//			params.gravity = Gravity.CENTER_HORIZONTAL;
			mTvErrorText.setLayoutParams(params);
			
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
			{
				mTvErrorText.setTextAppearance(R.style.Text_14sp_Red_Regular);
			}
			else
			{
				mTvErrorText.setTextAppearance(iContext, R.style.Text_14sp_Red_Regular);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		
		//		mTvErrorText = new TextView(iContext);
	
		//		mTvErrorText.setVisibility(INVISIBLE);
		//
		//		this.addView(mTvErrorText);
		
		
		//TODO avishay 30/07/17 or test
//		setActivated(true);
	}
	
	@Override
	public void setError(@Nullable final CharSequence error)
	{
		//		super.setError(error);
		
		
		//					if (getEditText() != null && TextUtils.isEmpty(getEditText().getText().toString()))
		//					{
		//						super.setError("");
		//					}
		//					else{
		//						super.setError(error);
		//					}
		
		
		if (error != null)
		{
			if (getEditText() != null && !TextUtils.isEmpty(getEditText().getText().toString()))
			{
				super.setError(error);
				//						setCustomError(error.toString());
			}
			setUnderlineColor(mContext.getResources().getColor(R.color.red));
//						setEditTextHint();
//						setHintTextAppearance(R.style.TextAppearance_Error);
//			setEditTextHint();
			
			
			
			
//			setHint(error);
			setHintTextAppearance(R.style.Text_12sp_Red_Regular);
		}
		else
		{
			clearUnderlineColor();
//			setUnderlineColor(mContext.getResources().getColor(R.color.ElalDarkBlue));
						setHintTextAppearance(R.style.Text_12sp_DarkBlue_Regular);
			//					setCustomError(null);
			super.setError(error);
			
			
			
			
//			setHint(mHintTextOrigin);
//			setHintTextAppearance(R.style.EditText_Login);
		}
//		mTvErrorText.setVisibility(GONE);// TODO avishay 26/07/17 just for test
	}
	
	private void clearUnderlineColor()
	{
		if (getEditText() != null)
		{
			getEditText().getBackground().clearColorFilter();
		}
	}
	
	
	
	private void setEditTextHint()
	{
		if (getEditText() != null )
		{
			getEditText().setHintTextColor(Color.GREEN);
		}
	}
	
	public void clearEditTextHint()
	{
		if (getEditText() != null && !TextUtils.isEmpty(getEditText().getText().toString()) && !TextUtils.isEmpty(this.getHint()) && getEditText().getText().toString().equals(this.getHint()))
		{
			getEditText().setText("");
		}
	}
	
	
	public void setEmptyError()
	{
		setUnderlineColor(mContext.getResources().getColor(R.color.red));
		//		setHintTextAppearance(R.style.TextAppearance_Error);
		//		setEditTextHint();
	}
	
	
	protected void setErrorTextRTL(final boolean iIsRTL)
	{
		if (mTvErrorText != null)
		{
			if (iIsRTL)
			{
				mTvErrorText.setGravity(Gravity.RIGHT);
				//				mTvErrorText.setPadding(0, 0, 15, 30);
			}
			else
			{
				mTvErrorText.setGravity(Gravity.LEFT);
				//				mTvErrorText.setPadding(15, 0, 0, 30);
			}
		}
	}
	
	public void setCustomError(final String iError)
	{
		if(mTvErrorText!=null)
		{
			if (!TextUtils.isEmpty(iError))
			{
				mTvErrorText.setText(iError);
				mTvErrorText.setVisibility(VISIBLE);
			}
			else
			{
				mTvErrorText.setVisibility(INVISIBLE);
			}
		}
	}
	
	
	@SuppressLint("ObsoleteSdkInt")
	@Override
	protected void onFinishInflate()
	{
		super.onFinishInflate();
		
		boolean isRtl = !isInEditMode() && AppUtils.isDefaultLocaleRTL();
		setErrorTextRTL(isRtl);
		setEditTextGravity(isRtl);
	}
	
	private void handleAttributes(Context iContext, AttributeSet attrs)
	{
		TypedArray typedArray = iContext.obtainStyledAttributes(attrs, R.styleable.ChangeDirectionTextInputLayout);
		
		final int N = typedArray.getIndexCount();
		
		for (int i = 0 ; i < N ; ++i)
		{
			int attr = typedArray.getIndex(i);
			switch (attr)
			{
				case R.styleable.ChangeDirectionTextInputLayout_isTilRTL:
					mIsRTL = typedArray.getBoolean(attr, false);
					break;
			}
		}
		typedArray.recycle();
	}
	
	
	
	
	public void setUnderlineColor(final int iUnderlineColor)
	{
		if (getEditText() != null)
		{
//			ColorStateList colorStateList = ColorStateList.valueOf(iUnderlineColor);
//			ViewCompat.setBackgroundTintList(getEditText(), colorStateList);
			getEditText().getBackground().setColorFilter(iUnderlineColor, PorterDuff.Mode.SRC_ATOP);
			
		}
	}
	
	public void setEditTextGravity(final boolean iIsRTL)
	{
		if (getEditText() != null)
		{
			getEditText().setGravity(iIsRTL ? Gravity.RIGHT : Gravity.LEFT);
		}
	}
	
	
}
