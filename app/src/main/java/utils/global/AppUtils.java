package utils.global;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaScannerConnection;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tooltip.Tooltip;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Pattern;

import javax.crypto.NoSuchPaddingException;

import global.AppData;
import global.ElAlApplication;
import global.GlobalVariables;
import global.UserData;
import global.eLanguage;
import il.co.ewave.elal.BuildConfig;
import il.co.ewave.elal.R;
import interfaces.IOnCancelClick;
import ui.activities.MainActivity;
import ui.activities.SplashActivity;
import ui.adapters.FlightDetailsPassengersAdapter;
import ui.fragments.EWBaseFragment;
import utils.AESEncryption;
import utils.global.googleAnalitics.GoogleAnalyticsManager;
import utils.global.googleAnalitics.TrackerManager.AnalyticTracker;
import webServices.global.ePnrFlightType;
import webServices.responses.getMobileCountryCodes.MobileCountryCode;
import webServices.responses.responseGetUserActivePNRs.PnrFlight;
import webServices.responses.responseGetUserActivePNRs.UserActivePnr;

import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * Created with care by Shahar Ben-Moshe on 13/12/16.
 */

public class AppUtils
{
	public static final String DEFAULT_ELAL_CAMERA_FOLDER = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath() + "/ELAL";
	private static final int DEFAULT_NOTIFICATION_ID = 1;
	
	public static final int SCREEN_WAKE_TIME = 3 * 60 * 1000;
	
	private static final String GOOGLE_PLAY_STORE_PACKAGE_NAME_OLD = "com.google.market";
	private static final String GOOGLE_PLAY_STORE_PACKAGE_NAME_NEW = "com.android.vending";
	//	private static final String PASSWORD_STRENGTH_CHECK_REGEX = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&])(?=\\S+$).{8,}$";
	//	private static final String PASSWORD_STRENGTH_CHECK_REGEX = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[><+-|%=&!*_.?:])(?=\\S+$).{8,}$";
	public static final String EMAIL_CHECK_REGEX = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[a-zA-Z]{2,6}$";
	
	private static final String CAPITAL_LETTERS_GROUP = "(?=.*[A-Z])";
	private static final String SMALL_LETTERS_GROUP = "(?=.*[a-z])";
	private static final String SPECIAL_CHARS_GROUP = "-|%=&!*_.?:"/*"(?=.*[>+\\|&!*\\-%_<\\?:=\\.])"*/;
	private static final String NUMBERS_GROUP = "(?=.*[0-9])";
	
	//	private static final String CAPITAL_LETTERS_GROUP = "?=.*[A-Z]";
	//	private static final String SMALL_LETTERS_GROUP = "?=.*[a-z]";
	//	private static final String SPECIAL_CHARS_GROUP = "?=.*[>+\\|&!*\\-%_<\\?:=\\.]";
	//	private static final String NUMBERS_GROUP = "?=.*[0-9]";
	
	//	private static final String PASSWORD_STRENGTH_CHECK_REGEX = "^" + "(" + SMALL_LETTERS_GROUP + CAPITAL_LETTERS_GROUP + NUMBERS_GROUP +        // # must contain a-z, A-Z and 0-9
	//			"|" +                                                                    // # OR
	//			SMALL_LETTERS_GROUP + CAPITAL_LETTERS_GROUP + SPECIAL_CHARS_GROUP +    // # must contain a-z, A-Z and special
	//			"|" +                                                                    // # OR
	//			SMALL_LETTERS_GROUP + NUMBERS_GROUP + SPECIAL_CHARS_GROUP +            // # must contain a-z, 0-9 and special
	//			"|" +                                                                    // # OR
	//			CAPITAL_LETTERS_GROUP + NUMBERS_GROUP + SPECIAL_CHARS_GROUP +            // # must contain A-Z, 0-9 and special
	//			")" + "(?=\\S+$).{8,}$";
	
	public static final String PASSWORD_STRENGTH_CHECK_REGEX = "^(?=.*[0-9])(?=.*[A-Za-z])(?=\\S+$).{6,}$";
	
	
	//	private static final String PASSWORD_ILLEGAL_CHECK_REGEX = "@";
	private static final String MATMID_PASSWORD_STRENGTH_CHECK_REGEX = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[#!_.?])(?=\\S+$).{7,12}$";
	private static final String PASSPORT_NUMBER_CHECK_REGEX = "^(?=.*[0-9])(?=.*[a-zA-Z0-9])(?=\\S+$).{6,20}$";
	private static final String MATMID_NUMBER_CHECK_REGEX = "^(?=.*[0-9])(?=\\S+$).{7,10}$";
	
	private static final String HTTP = "http://";
	private static final String HTTPS = "https://";
	private static final String TEXT_PLAIN = "text/plain";
	private static final String JPG_SUFFIX = ".jpg";
	
	private static final int IMAGE_SAVE_QUALITY = 70;
	private static final String TAG = AppUtils.class.getSimpleName();
	private static boolean mIsSawReminderDialogDetails;
	private static boolean mIsSawReminderDialogFamily;
	
	
	public static double getApplicationVersion(final Context iContext)
	{
		double result = 0;
		
		try
		{
			result = Double.parseDouble(iContext.getPackageManager().getPackageInfo(iContext.getPackageName(), 0).versionName);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return result;
	}
	
	public static int getApplicationVersionCode(final Context iContext)
	{
		int result = 0;
		
		try
		{
			result = iContext.getPackageManager().getPackageInfo(iContext.getPackageName(), 0).versionCode;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return result;
	}
	
	public static void copyToClipBoard(String iLabel, String iStr)
	{
		Context context = ElAlApplication.getInstance();
		
		ClipboardManager clipboard = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
		ClipData clip = ClipData.newPlainText(iLabel, iStr);
		
		if (clipboard != null)
		{
			clipboard.setPrimaryClip(clip);
		}
		
		Toast.makeText(context, "Nice choice!", Toast.LENGTH_SHORT).show();
	}
	
	public static String getApplicationVersion()
	{
		if (!TextUtils.isEmpty(BuildConfig.VERSION_NAME))
		{
			return BuildConfig.VERSION_NAME;
		}
		return "";
	}
	
	public static void printLog(int iLogType, String iTag, String iString)
	{
		int CHAR_LIMITS = 3500;
		String dataToPrint = iString;
		
		if (!TextUtils.isEmpty(dataToPrint))
		{
			while (dataToPrint.length() > 0)
			{
				String currentPrint;
				
				if (dataToPrint.length() > CHAR_LIMITS)
				{
					currentPrint = dataToPrint.substring(0, CHAR_LIMITS);
					dataToPrint = dataToPrint.substring(CHAR_LIMITS);
					printLog(Log.DEBUG, TAG, "exceeded char limits : " + CHAR_LIMITS + " chars");
				}
				else
				{
					currentPrint = dataToPrint;
					dataToPrint = "";
				}
				
				switch (iLogType)
				{
					case Log.VERBOSE:
						Log.v(iTag, getLogLocation() + currentPrint);
						break;
					case Log.DEBUG:
						Log.d(iTag, getLogLocation() + currentPrint);
						break;
					case Log.INFO:
						Log.i(iTag, getLogLocation() + currentPrint);
						break;
					case Log.WARN:
						Log.w(iTag, getLogLocation() + currentPrint);
						break;
					case Log.ERROR:
						Log.e(iTag, getLogLocation() + currentPrint);
						sendNotFatalException(getLogLocation() + currentPrint);
						break;
					default:
						Log.wtf(iTag, getLogLocation() + currentPrint);
						break;
				}
			}
		}
	}
	
	private static void sendNotFatalException(String iDescription)
	{
		GoogleAnalyticsManager analyticsManager = GoogleAnalyticsManager.getAnalyticsManagerObject();
		analyticsManager.init(ElAlApplication.getInstance());
		AnalyticTracker analyticTracker = analyticsManager.getTracker();
		analyticTracker.sendNotFatalException(iDescription);
	}
	
	private static String getLogLocation()
	{
		final String tmpClassName = AppUtils.class.getName();
		final StackTraceElement[] traces = Thread.currentThread().getStackTrace();
		boolean found = false;
		
		for (StackTraceElement trace : traces)
		{
			try
			{
				if (found)
				{
					if (!trace.getClassName().startsWith(tmpClassName))
					{
						Class<?> clazz = Class.forName(trace.getClassName());
						String className = getClassName(clazz);
						String methodName = trace.getMethodName();
						return "[" + className + "::" + methodName + "::" + trace.getLineNumber() + "]:: ";
					}
				}
				else if (trace.getClassName().startsWith(tmpClassName))
				{
					found = true;
				}
			}
			catch (ClassNotFoundException ignored)
			{
			}
		}
		
		return "[]: ";
	}
	
	private static String getClassName(Class<?> clazz)
	{
		if (clazz != null)
		{
			if (!TextUtils.isEmpty(clazz.getSimpleName()))
			{
				return clazz.getSimpleName();
			}
			return getClassName(clazz.getEnclosingClass());
		}
		return "";
	}
	
	@SuppressLint("HardwareIds")
	public static String getUserUdid()
	{
		return Settings.Secure.getString(ElAlApplication.getInstance().getContentResolver(), Settings.Secure.ANDROID_ID);
	}
	
	public static AlertDialog createRemindMeDialog(final Context iContext, final String iTitle, final String iMessage, /*final String iButton1Text,*/
	                                               final DialogInterface.OnClickListener iOnButton1Click, /*final String iButton2Text,*/ final DialogInterface.OnClickListener iOnButton2Click,
													   /* final String iButton3Text,*/ final DialogInterface.OnClickListener iOnButton3Click)
	{
		return createSimpleMessageDialog(iContext, iTitle, iMessage, iContext.getResources().getString(R.string.let_me_continue), iOnButton1Click, iContext.getResources()
		                                                                                                                                                   .getString(R.string.remind_me_later), iOnButton2Click, iContext
				.getResources()
				.getString(R.string.skip_underline), iOnButton3Click, false, true);
	}
	
	public static AlertDialog createSimpleMessageDialog(final Context iContext, final String iTitle, final String iMessage, final String iButton1Text,
	                                                    final DialogInterface.OnClickListener iOnButton1Click, final String iButton2Text, final DialogInterface.OnClickListener iOnButton2Click,
	                                                    final String iButton3Text, final DialogInterface.OnClickListener iOnButton3Click, final boolean iShowXBtn, final boolean iCancelable)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(iContext);
		
		View customDialogView = LayoutInflater.from(iContext).inflate(R.layout.custom_dialog, null, false);
		builder.setView(customDialogView);
		final AlertDialog dialog = builder.create();
		
		dialog.setCancelable(iCancelable);
		Window w = dialog.getWindow();
		if (w != null)
		{
			w.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		}
		
		
		if (iShowXBtn)
		{
			customDialogView.findViewById(R.id.iv_customDialog_close_btn).setVisibility(View.VISIBLE);
			customDialogView.findViewById(R.id.iv_customDialog_close_btn).setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					dialog.dismiss();
				}
			});
		}
		else
		{
			customDialogView.findViewById(R.id.iv_customDialog_close_btn).setVisibility(View.GONE);
		}
		
		if (TextUtils.isEmpty(iTitle))
		{
			((TextView) customDialogView.findViewById(R.id.tv_customDialog_Title)).setText("");
			customDialogView.findViewById(R.id.tv_customDialog_Title).setVisibility(View.GONE);
		}
		else
		{
			((TextView) customDialogView.findViewById(R.id.tv_customDialog_Title)).setText(iTitle);
			customDialogView.findViewById(R.id.tv_customDialog_Title).setVisibility(View.VISIBLE);
			if (iShowXBtn)
			{
				((TextView) customDialogView.findViewById(R.id.tv_customDialog_Title)).setGravity(View.TEXT_ALIGNMENT_TEXT_START);
			}
		}
		
		if (TextUtils.isEmpty(iMessage))
		{
			((TextView) customDialogView.findViewById(R.id.tv_customDialog_Message)).setText("");
			customDialogView.findViewById(R.id.tv_customDialog_Message).setVisibility(View.GONE);
		}
		else
		{
			((TextView) customDialogView.findViewById(R.id.tv_customDialog_Message)).setText(iMessage);
			customDialogView.findViewById(R.id.tv_customDialog_Message).setVisibility(View.VISIBLE);
			if (iShowXBtn)
			{
				((TextView) customDialogView.findViewById(R.id.tv_customDialog_Message)).setGravity(View.TEXT_ALIGNMENT_TEXT_START);
			}
		}
		
		if (TextUtils.isEmpty(iButton1Text))
		{
			((TextView) customDialogView.findViewById(R.id.btn_customDialog_Button1)).setText("");
			customDialogView.findViewById(R.id.btn_customDialog_Button1).setVisibility(View.GONE);
		}
		else
		{
			((TextView) customDialogView.findViewById(R.id.btn_customDialog_Button1)).setText(iButton1Text);
			customDialogView.findViewById(R.id.btn_customDialog_Button1).setVisibility(View.VISIBLE);
			customDialogView.findViewById(R.id.btn_customDialog_Button1).setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(final View v)
				{
					if (iOnButton1Click != null)
					{
						iOnButton1Click.onClick(dialog, android.R.id.button1);
					}
					
					dialog.dismiss();
				}
			});
			
			if (TextUtils.isEmpty(iButton2Text))
			{
				LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
				(customDialogView.findViewById(R.id.btn_customDialog_Button1)).setLayoutParams(p);
				((Button) customDialogView.findViewById(R.id.btn_customDialog_Button1)).setMaxWidth(dp2px(200));
				
				
			}
		}
		
		if (TextUtils.isEmpty(iButton2Text))
		{
			((TextView) customDialogView.findViewById(R.id.btn_customDialog_Button2)).setText("");
			customDialogView.findViewById(R.id.btn_customDialog_Button2).setVisibility(View.GONE);
		}
		else
		{
			((TextView) customDialogView.findViewById(R.id.btn_customDialog_Button2)).setText(iButton2Text);
			customDialogView.findViewById(R.id.btn_customDialog_Button2).setVisibility(View.VISIBLE);
			customDialogView.findViewById(R.id.btn_customDialog_Button2).setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(final View v)
				{
					if (iOnButton2Click != null)
					{
						iOnButton2Click.onClick(dialog, android.R.id.button2);
					}
					
					dialog.dismiss();
				}
			});
		}
		if (customDialogView.findViewById(R.id.btn_customDialog_Button1).getVisibility() == View.GONE && customDialogView.findViewById(R.id.btn_customDialog_Button2).getVisibility() == View.GONE)
		{
			customDialogView.findViewById(R.id.ll_customDialog_Button1_Button2).setVisibility(View.GONE);
		}
		else
		{
			customDialogView.findViewById(R.id.ll_customDialog_Button1_Button2).setVisibility(View.VISIBLE);
		}
		if (TextUtils.isEmpty(iButton3Text))
		{
			((TextView) customDialogView.findViewById(R.id.btn_customDialog_Button3)).setText("");
			customDialogView.findViewById(R.id.btn_customDialog_Button3).setVisibility(View.GONE);
			customDialogView.findViewById(R.id.ll_customDialog_Button3).setVisibility(View.GONE);
		}
		else
		{
			((TextView) customDialogView.findViewById(R.id.btn_customDialog_Button3)).setText(iButton3Text);
			((TextView) customDialogView.findViewById(R.id.btn_customDialog_Button3)).setPaintFlags(((TextView) customDialogView.findViewById(R.id.btn_customDialog_Button3)).getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
			customDialogView.findViewById(R.id.btn_customDialog_Button3).setVisibility(View.VISIBLE);
			customDialogView.findViewById(R.id.btn_customDialog_Button3).setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(final View v)
				{
					if (iOnButton3Click != null)
					{
						iOnButton3Click.onClick(dialog, android.R.id.button2);
					}
					
					dialog.dismiss();
				}
			});
		}
		
		return dialog;
	}
	
	private static int dp2px(float dp)
	{
		return (int) (dp * ElAlApplication.getInstance().getResources().getDisplayMetrics().density + 0.5f);
	}
	
	public static AlertDialog createSimpleMessageDialog(final Context iContext, final String iTitle, final String iMessage, final String iButton1Text,
	                                                    final DialogInterface.OnClickListener iOnButton1Click, final String iButton2Text, final DialogInterface.OnClickListener iOnButton2Click,
	                                                    final String iButton3Text, final DialogInterface.OnClickListener iOnButton3Click)
	{
		return createSimpleMessageDialog(iContext, iTitle, iMessage, iButton1Text, iOnButton1Click, iButton2Text, iOnButton2Click, iButton3Text, iOnButton3Click, false, true);
	}
	
	public static AlertDialog createSimpleMessageDialog(final Context iContext, final String iMessage, final String iButton1Text, final DialogInterface.OnClickListener iOnButton1Click,
	                                                    final String iButton2Text, final DialogInterface.OnClickListener iOnButton2Click, final String iButton3Text,
	                                                    final DialogInterface.OnClickListener iOnButton3Click)
	{
		return createSimpleMessageDialog(iContext, null, iMessage, iButton1Text, iOnButton1Click, iButton2Text, iOnButton2Click, iButton3Text, iOnButton3Click, false, true);
	}
	
	public static AlertDialog createSimpleMessageDialog(final Context iContext, final String iTitle, final String iMessage, final String iButton1Text,
	                                                    final DialogInterface.OnClickListener iOnButton1Click, final String iButton2Text, final DialogInterface.OnClickListener iOnButton2Click)
	{
		return createSimpleMessageDialog(iContext, iTitle, iMessage, iButton1Text, iOnButton1Click, iButton2Text, iOnButton2Click, null, null, false, true);
	}
	
	public static AlertDialog createSimpleMessageDialogWithXButton(final Context iContext, final String iTitle, final String iMessage, final String iButton1Text,
	                                                               final DialogInterface.OnClickListener iOnButton1Click, final String iButton2Text,
	                                                               final DialogInterface.OnClickListener iOnButton2Click, final String iButton3Text,
	                                                               final DialogInterface.OnClickListener iOnButton3Click)
	{
		return createSimpleMessageDialog(iContext, iTitle, iMessage, iButton1Text, iOnButton1Click, iButton2Text, iOnButton2Click, iButton3Text, iOnButton3Click, true, true);
	}
	
	public static void showToolTip(Context iContext, View iV, String iText)
	{
		showToolTip(iContext, iV, UserData.getInstance().getLanguage() == eLanguage.English ? Gravity.START : Gravity.END, iText);
	}
	
	public static void showToolTip(Context iContext, View iV, int iGravity, String iText)
	{
		new Tooltip.Builder(iV).setBackgroundColor(iContext.getResources().getColor(R.color.ElalTooltipGrey))
		                       .setTextColor(iContext.getResources().getColor(R.color.dark_blue_grey))
		                       .setCornerRadius(15f)
		                       .setText(iText)
		                       .setGravity(iGravity)
		                       .setCancelable(true)
		                       .setDismissOnClick(true)
		                       .show();
	}
	
	public static DatePickerDialog createSimpleDatePickerDialog(final Context iContext, final DatePickerDialog.OnDateSetListener iOnDateSetListener, final Calendar iCurrentDate,
	                                                            final Calendar iMinDate, final Calendar iMaxDate)
	{
		return createSimpleDatePickerDialog(iContext, iOnDateSetListener, iCurrentDate, iMinDate, iMaxDate, null);
	}
	
	public static DatePickerDialog createSimpleDatePickerDialog(final Context iContext, final DatePickerDialog.OnDateSetListener iOnDateSetListener, final Calendar iCurrentDate,
	                                                            final Calendar iMinDate, final Calendar iMaxDate, final IOnCancelClick iCancelClick)
	{
		DatePickerDialog result = null;
		
		if (iContext != null && iOnDateSetListener != null)
		{
			Calendar currentDate = iCurrentDate != null ? iCurrentDate : Calendar.getInstance();
			result = new DatePickerDialog(iContext, R.style.AlertDialogTheme, iOnDateSetListener, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DAY_OF_MONTH));
			
			if (iMinDate != null)
			{
				result.getDatePicker().setMinDate(iMinDate.getTimeInMillis());
			}
			
			if (iMaxDate != null)
			{
				result.getDatePicker().setMaxDate(iMaxDate.getTimeInMillis());
			}
			
			if (iCancelClick != null)
			{
				result.setButton(DialogInterface.BUTTON_NEGATIVE, iContext.getString(R.string.cancel), new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface iDialog, int iWhich)
					{
						if (iWhich == DialogInterface.BUTTON_NEGATIVE)
						{
							iCancelClick.cancelClick();
						}
					}
				});
			}
			
			result.setCancelable(false);
		}
		
		return result;
	}
	
	public static <T> int getSelectionInt(List<T> list, String str)
	{
		
		int result = 0;
		
		if (list == null || list.isEmpty())
		{
			return result;
		}
		
		for (int i = 0 ; i < list.size() ; i++)
		{
			if (list.get(i) instanceof String)
			{
				String obj = (String) list.get(i);
				if (obj.trim().equalsIgnoreCase(str.trim()))
				{
					result = i;
				}
			}
		}
		
		return result;
	}
	
	public static float convertPixelsToDp(float iPx, Context iContext)
	{
		Resources res = iContext.getResources();
		return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, iPx, res.getDisplayMetrics());
	}
	
	public static void openAppInMarket(final Context iContext)
	{
		iContext.startActivity(getAppInMarketIntent());
	}
	
	public static Intent getAppInMarketIntent()
	{
		return getAppInMarketIntent(ElAlApplication.getInstance().getPackageName());
	}
	
	public static Intent getAppInMarketIntent(final String iPackageName)
	{
		Intent intent = new Intent(Intent.ACTION_VIEW);
		
		if (isPlayStoreAppAvailable(ElAlApplication.getInstance()))
		{
			intent.setData(Uri.parse("market://details?id=" + iPackageName));
		}
		else
		{
			intent.setData(Uri.parse("http://play.google.com/store/apps/details?id=" + iPackageName));
		}
		
		return intent;
	}
	
	private static boolean isPlayStoreAppAvailable(final Context iContext)
	{
		boolean result = false;
		
		if (iContext != null)
		{
			PackageManager packageManager = iContext.getPackageManager();
			List<PackageInfo> packages = packageManager.getInstalledPackages(PackageManager.GET_UNINSTALLED_PACKAGES);
			for (PackageInfo packageInfo : packages)
			{
				if (packageInfo.packageName.equals(GOOGLE_PLAY_STORE_PACKAGE_NAME_OLD) || packageInfo.packageName.equals(GOOGLE_PLAY_STORE_PACKAGE_NAME_NEW))
				{
					result = true;
					break;
				}
			}
		}
		
		return result;
	}
	
	public static boolean isDefaultLocaleRTL()
	{
		//		return isRTL(Locale.getDefault());
		if (UserData.getInstance().getLanguage().getLanguageCode().equalsIgnoreCase(eLanguage.Hebrew.getLanguageCode()/*"he"*/)) // test
		{
			return true;
		}
		else
		{
			return isRTL(LocaleUtils.convertLanguageCodeToLocaleOrNull(UserData.getInstance().getLanguage().getLanguageCode()));
		}
	}
	
	public static boolean isRTL(Locale iLocale)
	{
		if (iLocale == null)
		{
			iLocale = Locale.getDefault();
		}
		final int directionality = Character.getDirectionality(iLocale.getDisplayName().charAt(0));
		return directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT || directionality == Character.DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC;
	}
	
	public static boolean isPasswordStrong(final String iPasswordToCheck, final boolean iIsMatmid)
	{
		if (iIsMatmid)
		{
			return !TextUtils.isEmpty(iPasswordToCheck) && isMatmidPasswordValid(iPasswordToCheck);
		}
		else
		{
			return !TextUtils.isEmpty(iPasswordToCheck) && /*checkRegex(iPasswordToCheck, PASSWORD_STRENGTH_CHECK_REGEX)*/
					isPasswordValid(iPasswordToCheck);
		}
	}
	
	private static boolean isMatmidPasswordValid(final String iPasswordToCheck)
	{
		if (iPasswordToCheck.length() < 7 || iPasswordToCheck.length() > 12)
		{
			return false;
		}
		
		String matchesStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "abcdefghijklmnopqrstuvwxyz" + "0123456789" + ".?!#_";
		char[] chars = iPasswordToCheck.toCharArray();
		for (char tmpChar : chars)
		{
			if (!matchesStr.contains(Character.toString(tmpChar)))
			{
				return false;
			}
		}
		
		return true;
	}
	
	
	public static boolean isPasswordValid(final String iPasswordToCheck)
	{
		if (iPasswordToCheck.length() < 6)
		{
			return false;
		}
		
		String PASSWORD_CHECK = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "abcdefghijklmnopqrstuvwxyz" + "0123456789" + "-|%=&!*_.?:";
		
		char[] chars = iPasswordToCheck.toCharArray();
		for (char tmpChar : chars)
		{
			if (!PASSWORD_CHECK.contains(Character.toString(tmpChar)))
			{
				return false;
			}
		}
		
		return true;
	}
	
	public static boolean checkRegex(final String iPasswordToCheck, final String iPasswordStrengthCheckRegex)
	{
		boolean result = false;
		
		try
		{
			result = Pattern.compile(iPasswordStrengthCheckRegex).matcher(iPasswordToCheck).matches();
		}
		catch (RuntimeException ignored)
		{
		}
		
		return result;
	}
	
	private static boolean checkRegexForChar(final String iPasswordToCheck, final String iPasswordStrengthCheckRegex)
	{
		boolean result = false;
		
		try
		{
			result = Pattern.compile(iPasswordStrengthCheckRegex).matcher(iPasswordToCheck).find();
		}
		catch (RuntimeException ignored)
		{
		}
		
		return result;
	}
	
	public static boolean isMatmidNumberValid(final String iMatmidNumber)
	{
		boolean result = true;
		
		if (TextUtils.isEmpty(iMatmidNumber) || !checkRegex(iMatmidNumber, MATMID_NUMBER_CHECK_REGEX))
		{
			result = false;
		}
		
		
		return result;
	}
	
	public static boolean isPhoneNumberValid(final String iPhoneNumber)
	{
		boolean result = true;
		
		if (iPhoneNumber == null || TextUtils.isEmpty(iPhoneNumber) || iPhoneNumber.length() < 10 || iPhoneNumber.length() > 18 || !Patterns.PHONE.matcher(iPhoneNumber).matches())
		{
			result = false;
		}
		
		return result;
	}
	
	public static boolean isCountryCodeValid(final String iCountryCode)
	{
		boolean result = true;
		
		if (iCountryCode == null || TextUtils.isEmpty(iCountryCode) /*|| iCountryCode.length() < 2*/ || iCountryCode.length() > 4
				||!isCountryCodeExist(iCountryCode))
		{
			result = false;
		}
		
		return result;
	}
	
	public static boolean isCountryCodeExist(String iCountryCode){
		ArrayList<MobileCountryCode> list = AppData.getInstance().getMobileCountryCodes();
		for (int i = 0; i< list.size();i++){
			if (list.get(i).getPrefix().equalsIgnoreCase(iCountryCode)){
				return true;
			}
		}
		return false;
	}
	
	public static boolean isStringContainsAtLeastTwoChar(String iStringToCheck)
	{
		return iStringToCheck.toCharArray().length >= 2;
	}
	
	public static boolean isStringContainsLatinCharactersOnly(final String iStringToCheck)
	{
		return iStringToCheck.matches("^[a-zA-Z0-9. ']+$");
	}
	
	public static boolean isStringContainsLatinCharactersOnlyLetters(final String iStringToCheck)
	{
		return iStringToCheck.matches("^[a-zA-Z. ']+$");
	}
	
	public static boolean isStringContainsHebrewCharactersOnly(final String iStringToCheck)
	{
		boolean result = true;
		
		String matchesStr = "פםןוטארקףךלחיעכגדשץתצמנהבסז " + "\u0020" + "'";
		char[] chars = iStringToCheck.replace(" ", "").toCharArray();
		
		for (char tmpChar : chars)
		{
			if (!matchesStr.contains(Character.toString(tmpChar)))
			{
				result = false;
			}
		}
		
		return result;
	}
	
	public static boolean isPassportNumberValid(String iPassportNumber)
	{
		return !TextUtils.isEmpty(iPassportNumber) && checkRegex(iPassportNumber, PASSPORT_NUMBER_CHECK_REGEX) && iPassportNumber.matches("^[a-zA-Z0-9]+$");
	}
	
	public static boolean isPassportExpirationValid(Date iPassportExpirationDate)
	{
		if (iPassportExpirationDate == null)
		{
			return false;
		}
		else
		{
			Calendar today = Calendar.getInstance();
			today.set(Calendar.HOUR_OF_DAY, 0);
			today.set(Calendar.MINUTE, 0);
			today.set(Calendar.SECOND, 0);
			today.set(Calendar.MILLISECOND, 0);
			
			if (iPassportExpirationDate.before(today.getTime()))
			{
				return false;
			}
		}
		return true;
	}
	
	
	public static boolean isTicketNumberValid(String iTicketNumber)
	{
		
		if (!TextUtils.isEmpty(iTicketNumber) && iTicketNumber.length() != 13)
		{
			return false;
		}
		return true;
	}
	
	public static boolean isIdValid(String tz)
	{
		if (tz != null && tz.length() > 0)
		{
			try
			{
				if (tz.length() > 9 || tz.length() < 5)
				{
					return false;
				}
				while (tz.length() < 9)
				{
					tz = "0" + tz;
				}
				if (tz.equals("000000000"))
				{
					return false;
				}
				
				int tmpTZ = Integer.valueOf(tz);
				int[] digits = new int[9];
				int[] oneTwo = {1, 2, 1, 2, 1, 2, 1, 2, 1};
				int[] result = new int[9];
				
				// Start filling array from the end
				
				int i = 8;
				
				while (tmpTZ > 0)
				{
					digits[i] = tmpTZ % 10;
					
					tmpTZ /= 10;
					
					i--;
				}
				
				for (int j = 0 ; j < digits.length ; j++)
				{
					int tmpResult = (digits[j] * oneTwo[j]);
					
					// If number is greater than 9, add both digits
					
					if (tmpResult > 9)
					{
						tmpResult -= 9;
					}
					
					result[j] = tmpResult;
				}
				
				int iResult = 0;
				
				for (int aResult : result)
				{
					iResult += aResult;
				}
				
				return (iResult % 10) == 0;
			}
			catch (Exception exception)
			{
				if (BuildConfig.DEBUG)
				{
					Log.e(TAG, exception.toString());
				}
				return false;
			}
		}
		else
		{
			return true;
		}
	}
	
	public static void changeLocale(final EWBaseFragment iFragment, eLanguage iELanguage)
	{
		//only for test method
		if (GlobalVariables.isDebugVersion)
		{
			if (UserData.getInstance().getLanguage() == iELanguage)
			{
				return;
			}
			
			UserData.getInstance().setLanguage(iELanguage);
			UserData.getInstance().saveUserData();
			LocaleUtils.applyLocale(ElAlApplication.getInstance(), LocaleUtils.convertLanguageCodeToLocaleOrNull(iELanguage.getLanguageCode()));
			
			AlertDialog dialog = AppUtils.createSimpleMessageDialog(iFragment.getContext(), iFragment.getString(R.string.restart_after_language_change), null, null, iFragment.getString(R.string.close), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(final DialogInterface dialog, final int which)
				{
					iFragment.tryPopFragment();
				}
			});
			dialog.setCancelable(false);
			dialog.show();
		}
	}
	
	public static AlertDialog createSimpleMessageDialog(final Context iContext, final String iMessage, final String iButton1Text, final DialogInterface.OnClickListener iOnButton1Click,
	                                                    final boolean iCancelable)
	{
		return createSimpleMessageDialog(iContext, null, iMessage, iButton1Text, iOnButton1Click, null, null, null, null, false, iCancelable);
	}
	
	public static AlertDialog createSimpleMessageDialog(final Context iContext, final String iMessage, final String iButton1Text, final DialogInterface.OnClickListener iOnButton1Click,
	                                                    final String iButton2Text, final DialogInterface.OnClickListener iOnButton2Click)
	{
		return createSimpleMessageDialog(iContext, null, iMessage, iButton1Text, iOnButton1Click, iButton2Text, iOnButton2Click, null, null, false, true);
	}
	
	public static String getDate(long date)
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
		return dateFormat.format(date);
	}
	
	public static void hideKeyboard(Context context, View v)
	{
		if (context != null && v != null)
		{
			((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(v.getWindowToken(), 0);
		}
	}
	
	public static void showKeyboard(final EditText iEditText)
	{
		new Handler().postDelayed(new Runnable()
		{
			public void run()
			{
				iEditText.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
				iEditText.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0));
			}
		}, 1000);
	}
	
	public static int getScreenWidth(Context iContext)
	{
		WindowManager wm = (WindowManager) iContext.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		
		Point size = new Point();
		display.getSize(size);
		
		return size.x;
	}
	
	public static String removeWhiteSpace(String iStringToRemove)
	{
		return iStringToRemove == null ? "" : iStringToRemove.replace(" ", "");
	}
	
	public static String getDeviceName()
	{
		String result;
		
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		
		if (model.startsWith(manufacturer))
		{
			result = capitalizeFirstLetter(model);
		}
		else
		{
			result = capitalizeFirstLetter(manufacturer) + " " + model;
		}
		
		return result;
	}
	
	public static String capitalizeFirstLetter(String iStringToCapitalize)
	{
		String result = "";
		
		if (!TextUtils.isEmpty(iStringToCapitalize))
		{
			char firstLetter = iStringToCapitalize.charAt(0);
			
			if (Character.isUpperCase(firstLetter))
			{
				result = iStringToCapitalize;
			}
			else
			{
				result = Character.toUpperCase(firstLetter) + iStringToCapitalize.substring(1);
			}
		}
		
		return result;
	}
	
	public static String getDeviceOsVersion()
	{
		return Build.VERSION.RELEASE;
	}
	
	public static String addHttpToStringIfNeed(String iStringToAdd)
	{
		String result = iStringToAdd;
		
		if (!TextUtils.isEmpty(result))
		{
			if (!result.startsWith(HTTP) && !result.startsWith(HTTPS))
			{
				result = HTTP + result;
			}
		}
		
		return result;
	}
	
	public static int generateRandomNumberInRange(final int iMin, final int iMax)
	{
		return iMin + (int) (Math.random() * ((iMax - iMin) + 1));
	}
	
	public static void sendSimpleNotification(Context iContext, String iTitle, String iBigTextMessage, String iContentMessage, int iIconResource, Intent iIntent)
	{
		sendSimpleNotification(iContext, iTitle, iBigTextMessage, iContentMessage, iIconResource, iIntent, -1);
	}
	
	public static void sendSimpleNotification(Context iContext, String iTitle, String iBigTextMessage, String iContentMessage, int iIconResource, Intent iIntent, int iUniqueId)
	{
		
		
		if (iContext != null && iIntent != null)
		{
			NotificationManager notificationManager = (NotificationManager) iContext.getSystemService(Context.NOTIFICATION_SERVICE);
			//
			//            iIntent.setAction("android.intent.action.MAIN");
			//            iIntent.addCategory("android.intent.category.LAUNCHER");
			//
			int finalId = DEFAULT_NOTIFICATION_ID;
			String CHANNEL_ID = "ElalFlights_notifications_channel_01";// The id of the channel.
			if (iUniqueId > 0)
			{
				finalId = iUniqueId;
			}

			PendingIntent contentIntent = PendingIntent.getActivity(iContext, iUniqueId, iIntent, PendingIntent.FLAG_UPDATE_CURRENT);

			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(iContext, CHANNEL_ID).setSmallIcon(iIconResource > 0 ? iIconResource : getApplicationIconId(iContext))
			                                                                                          .setContentTitle(iTitle)
			                                                                                          .setStyle(new NotificationCompat.BigTextStyle().bigText(iBigTextMessage))
			                                                                                          .setContentText(iContentMessage);

			int importance = NotificationManager.IMPORTANCE_HIGH;
			CharSequence name = iContext.getString(R.string.channel_name);// The user-visible name of the channel.


			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
			{
				try
				{
					NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
					notificationManager.createNotificationChannel(channel);
//					mBuilder.setChannelId(CHANNEL_ID);
				}
				catch (Exception iE)
				{
					iE.toString();
				}
			}

			mBuilder.setAutoCancel(true);
			mBuilder.setContentIntent(contentIntent);
			try
			{
				if (mBuilder != null)
				{
					notificationManager.notify(finalId, mBuilder.build());
					tryPlayNotificationSound(iContext);
				}
			}
			catch (Exception iE)
			{
				iE.printStackTrace();
			}

		}
	}
	
	private static int getApplicationIconId(Context iContext)
	{
		return iContext.getApplicationInfo().icon;
	}
	
	private static void tryPlayNotificationSound(Context iContext)
	{
		try
		{
			Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);//play sound
			Ringtone ringtone = RingtoneManager.getRingtone(iContext.getApplicationContext(), notification);
			ringtone.play();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static Intent getSplashIntent(Context iContext)
	{
		return new Intent(iContext, SplashActivity.class);
	}
	
	public static Intent getMainIntent(Context iContext)
	{
		return new Intent(iContext, MainActivity.class);
	}
	
	public static Uri convertBase64ImageToUri(String iBase64Image)
	{
		Uri result = null;
		
		if (!TextUtils.isEmpty(iBase64Image))
		{
			try
			{
				String path = MediaStore.Images.Media.insertImage(ElAlApplication.getInstance().getContentResolver(), convertBase64ImageToBitmap(iBase64Image), "", null);
				result = Uri.parse(path);
			}
			catch (Exception ignored)
			{
			}
		}
		
		return result;
	}
	
	public static Bitmap convertBase64ImageToBitmap(String iBase64Image)
	{
		Bitmap result = null;
		
		if (!TextUtils.isEmpty(iBase64Image))
		{
			try
			{
				byte[] parsedImageByteArray = Base64.decode(iBase64Image, Base64.DEFAULT);
				result = BitmapFactory.decodeByteArray(parsedImageByteArray, 0, parsedImageByteArray.length);
			}
			catch (Exception ignored)
			{
			}
		}
		
		return result;
	}
	
	public static String convertBitmapToBase64(Bitmap iBitmap)
	{
		String result = "";
		
		if (iBitmap != null)
		{
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			iBitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
			byte[] byteArray = byteArrayOutputStream.toByteArray();
			result = Base64.encodeToString(byteArray, Base64.DEFAULT);
		}
		
		return result;
	}
	
	public static Intent getEmailIntent(String iEmailAddress, String iSubject, String iBody, ArrayList<Uri> iAttachmentsUris)
	{
		Intent intent = null;
		
		try
		{
			//            Uri emailAddressUri = Uri.fromParts(sf_MAIL_TO, isEmailValid(iEmailAddress) ? iEmailAddress : "", null);
			
			Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
			emailIntent.setType(TEXT_PLAIN);
			
			if (isEmailValid(iEmailAddress))
			{
				emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {iEmailAddress});
			}
			
			if (!TextUtils.isEmpty(iSubject))
			{
				emailIntent.putExtra(Intent.EXTRA_SUBJECT, iSubject);
			}
			
			if (!TextUtils.isEmpty(iBody))
			{
				emailIntent.putExtra(Intent.EXTRA_TEXT, iBody);
			}
			
			if (iAttachmentsUris != null)
			{
				emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, iAttachmentsUris);
			}
			
			intent = Intent.createChooser(emailIntent, null);
		}
		catch (Exception ignored)
		{
		}
		
		return intent;
	}
	
	public static boolean isEmailValid(final String iEmailToValidate)
	{
		//		return !TextUtils.isEmpty(iEmailToValidate) && Patterns.EMAIL_ADDRESS.matcher(iEmailToValidate).matches();
		return !TextUtils.isEmpty(iEmailToValidate) && Pattern.compile(EMAIL_CHECK_REGEX).matcher(iEmailToValidate).matches();
	}
	
	public static boolean isEmailValid(CharSequence iEmailToValidate)
	{
		return !TextUtils.isEmpty(iEmailToValidate) && Patterns.EMAIL_ADDRESS.matcher(iEmailToValidate).matches();
	}
	
	public static boolean saveImageToDevice(Bitmap iImageToSave, String iPathOfDirectoryToSaveImage)
	{
		boolean result = false;
		
		if (iImageToSave != null && !TextUtils.isEmpty(iPathOfDirectoryToSaveImage))
		{
			try
			{
				File folderToSaveImage = new File(iPathOfDirectoryToSaveImage);
				if (!folderToSaveImage.exists())
				{
					folderToSaveImage.mkdirs();
				}
				
				File fileToSave = new File(folderToSaveImage, getCurrentDateAndTimeInFileSaveFormat() + JPG_SUFFIX);
				
				FileOutputStream fos = new FileOutputStream(fileToSave);
				iImageToSave.compress(Bitmap.CompressFormat.JPEG, IMAGE_SAVE_QUALITY, fos);
				fos.flush();
				fos.close();
				
				notifyMediaScannerService(fileToSave.getAbsolutePath());
				result = true;
			}
			catch (Exception ignored)
			{
			}
		}
		
		return result;
	}
	
	private static String getCurrentDateAndTimeInFileSaveFormat()
	{
		return DateTimeUtils.convertDateToFileSaveDateString(new Date());
	}
	
	public static void notifyMediaScannerService(String iPathToScan)
	{
		MediaScannerConnection.scanFile(ElAlApplication.getInstance(), new String[] {iPathToScan}, null, new MediaScannerConnection.OnScanCompletedListener()
		{
			public void onScanCompleted(String path, Uri uri)
			{
			}
		});
	}
	
	public static float resolveActionBarHeight(@NonNull final Context iContext)
	{
		float result = 0f;
		
		TypedValue tv = new TypedValue();
		if (iContext.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
		{
			result = TypedValue.complexToDimensionPixelSize(tv.data, iContext.getResources().getDisplayMetrics());
		}
		
		return result;
	}
	
	public static Rect getVisibleRectOfView(View iViewToCheck)
	{
		Rect result = null;
		
		if (iViewToCheck != null)
		{
			result = new Rect();
			
			iViewToCheck.getGlobalVisibleRect(result);
		}
		
		return result;
	}
	
	public static float resolveDrawableHeight(@NonNull final Context iContext, @DrawableRes int iDrawableResourceid)
	{
		float result = 0f;
		
		try
		{
			//			BitmapFactory.Options o = new BitmapFactory.Options();
			//			o.inJustDecodeBounds = true;
			//			BitmapFactory.decodeResource(iContext.getResources(), iDrawableResourceid , o);
			//			result = o.outHeight;
			result = ((BitmapDrawable) iContext.getResources().getDrawable(iDrawableResourceid)).getBitmap().getHeight();
		}
		catch (Exception ignored)
		{
		}
		
		return result;
	}
	
	public static boolean isSawReminderDialogDetails()
	{
		return mIsSawReminderDialogDetails;
	}
	
	public static void setSawReminderDialogDetails(final boolean iSawReminderDialogDetails)
	{
		mIsSawReminderDialogDetails = iSawReminderDialogDetails;
	}
	
	public static boolean isSawReminderDialogFamily()
	{
		return mIsSawReminderDialogFamily;
	}
	
	public static void setSawReminderDialogFamily(final boolean iSawReminderDialogFamily)
	{
		mIsSawReminderDialogFamily = iSawReminderDialogFamily;
	}
	
	public static String getRemindMeDetailsKey(final int iUserID)
	{
		return ElalPreferenceUtils.REMIND_ME_DETAIL_SCREEN + "_" + String.valueOf(iUserID);
	}
	
	public static String getRemindMeFamilyKey(final int iUserID)
	{
		return ElalPreferenceUtils.REMIND_FAMILY_SCREEN + "_" + String.valueOf(iUserID);
	}
	
	//	public static eFlightStatus getFlightStatusByDestination(final DestinationListItem iCurrentDestination, DestinationListItem iNextDestination)
	//	{
	//		eFlightStatus result = eFlightStatus.Error;
	//
	//		if (iCurrentDestination != null)
	//		{
	//			//			GetPNRFlightsContent currentFlight = iCurrentDestination.getmVia() != null ? iCurrentDestination.getmVia() : iCurrentDestination.getmDestination();
	//			//			GetPNRFlightsContent lastFlight = iNextDestination.getmVia() != null ? iNextDestination.getmVia() : iNextDestination.getmDestination();
	//
	//			Date currentFlightDepartureDate = null;
	//			Date currentFlightArrivalDate = null;
	//
	//			//			Date nextFlightDepartureDate = null;
	//			//			Date nextFlightArrivalDate = null;
	//
	//			if (iCurrentDestination != null)
	//			{
	//				currentFlightDepartureDate = iCurrentDestination.getmVia() != null ? iCurrentDestination.getmVia().getDepartureDate() : iCurrentDestination.getmDestination().getDepartureDate();
	//				currentFlightArrivalDate = iCurrentDestination.getmDestination().getArrivalDate();
	//			}
	//
	//			//			if (iNextDestination != null)
	//			//			{
	//			//				nextFlightDepartureDate = iNextDestination.getmVia() != null ? iNextDestination.getmVia().getDepartureDate() : iNextDestination.getmDestination().getDepartureDate();
	//			//				nextFlightArrivalDate = iNextDestination.getmDestination().getArrivalDate();
	//			//			}
	//
	//			if (currentFlightDepartureDate != null && currentFlightArrivalDate != null)
	//			{
	//				if (iCurrentDestination.getmFlightType() == ePnrFlightType.RETURN) // return flight
	//				{
	//					Date currentDate = new Date();
	//					Integer returnFlightDepartureDateTimeDifferenceInHours = DateTimeUtils.getTimeDifferenceInHoursOrNull(currentDate, currentFlightDepartureDate);
	//
	//					if (returnFlightDepartureDateTimeDifferenceInHours > DateTimeUtils.HOURS_OF_A_DAY)
	//					{
	//						result = MoreThanDayToOrigination;
	//					}
	//					else
	//					{
	//						result = LessThanDayToOrigination;
	//
	//						Integer returnFlightArrivalDateTimeDifferenceInHours = DateTimeUtils.getTimeDifferenceInHoursOrNull(currentDate, currentFlightArrivalDate);
	//						if (returnFlightArrivalDateTimeDifferenceInHours <= 0)
	//						{
	//							result = AfterBackToOrigination;
	//						}
	//					}
	//				}
	//				else // departure to destination
	//				{
	//					Date currentDate = new Date();
	//					Integer currentFlightDepartureDateTimeDifferenceInHours = DateTimeUtils.getTimeDifferenceInHoursOrNull(currentDate, currentFlightDepartureDate);
	//
	//					if (currentFlightDepartureDateTimeDifferenceInHours > DateTimeUtils.HOURS_OF_A_DAY * 2)
	//					{
	//						result = MoreThan48HoursToDeparture;
	//					}
	//					else
	//					{
	//						result = LessThan48HoursToDeparture;
	//
	//						if (currentFlightDepartureDateTimeDifferenceInHours < DateTimeUtils.HOURS_OF_A_DAY)
	//						{
	//							result = LessThanDayToDeparture;
	//
	//							if (currentFlightDepartureDateTimeDifferenceInHours < 0)
	//							{
	//								result = DepartedNotArrivedToDestination;
	//
	//								Integer currentFlightArrivalDateTimeDifferenceInHours = DateTimeUtils.getTimeDifferenceInHoursOrNull(currentDate, currentFlightArrivalDate);
	//								if (currentFlightArrivalDateTimeDifferenceInHours <= 0) // Arrived to destination
	//								{
	//									result = MoreThanDayToOrigination;
	//								}
	//							}
	//						}
	//					}
	//				}
	//			}
	//			else
	//			{
	//				result = eFlightStatus.Error;
	//			}
	//
	//
	//			//			if (currentFlightDepartureDate != null)
	//			//			{
	//			//				Date currentDate = new Date();
	//			//				Integer firstFlightDepartureDateTimeDifferenceInHours = DateTimeUtils.getTimeDifferenceInHoursOrNull(currentDate, firstFlightDepartureDate);
	//			//
	//			//				if (firstFlightDepartureDateTimeDifferenceInHours != null)
	//			//				{
	//			//					if (firstFlightDepartureDateTimeDifferenceInHours > 0)
	//			//					{
	//			//						if (firstFlightDepartureDateTimeDifferenceInHours <= DateTimeUtils.HOURS_OF_A_DAY)
	//			//						{
	//			//							if (iDestination.getmFlightType() == ePnrFlightType.RETURN)
	//			//							{
	//			//								result = LessThanDayToOrigination;
	//			//							}
	//			//							else
	//			//							{
	//			//								result = LessThanDayToDeparture;
	//			//							}
	//			//						}
	//			//						else if (firstFlightDepartureDateTimeDifferenceInHours <= DateTimeUtils.HOURS_OF_A_DAY * 2 && iDestination.getmFlightType() == ePnrFlightType.DEPARTURE)
	//			//						{
	//			//							result = LessThan48HoursToDeparture;
	//			//						}
	//			//						else
	//			//						{
	//			//							if (iDestination.getmFlightType() == ePnrFlightType.RETURN)
	//			//							{
	//			//								result = MoreThanDayToOrigination;
	//			//							}
	//			//							else
	//			//							{
	//			//								result = MoreThan48HoursToDeparture;
	//			//							}
	//			//						}
	//			//					}
	//			//					else
	//			//					{
	//			//						Integer firstFlightArrivalDateTimeDifferenceInHours = DateTimeUtils.getTimeDifferenceInHoursOrNull(currentDate, firstFlightArrivalDate);
	//			//
	//			//						if (firstFlightArrivalDateTimeDifferenceInHours != null && firstFlightArrivalDateTimeDifferenceInHours > 0)
	//			//						{
	//			//							result = DepartedNotArrivedToDestination;
	//			//						}
	//			//						else if (lastFlightDepartureDate != null)
	//			//						{
	//			//							Integer lastFlightDepartureDateTimeDifferenceInHours = DateTimeUtils.getTimeDifferenceInHoursOrNull(currentDate, lastFlightDepartureDate);
	//			//
	//			//							if (lastFlightDepartureDateTimeDifferenceInHours != null)
	//			//							{
	//			//								if (lastFlightDepartureDateTimeDifferenceInHours >= DateTimeUtils.HOURS_OF_A_DAY)
	//			//								{
	//			//									result = MoreThanDayToOrigination;
	//			//								}
	//			//								else if (lastFlightArrivalDate != null)
	//			//								{
	//			//									Integer lastFlightArrivalDateTimeDifferenceInHours = DateTimeUtils.getTimeDifferenceInHoursOrNull(currentDate, lastFlightArrivalDate);
	//			//
	//			//									if (lastFlightArrivalDateTimeDifferenceInHours != null)
	//			//									{
	//			//										if (lastFlightArrivalDateTimeDifferenceInHours < DateTimeUtils.HOURS_OF_A_DAY && lastFlightArrivalDateTimeDifferenceInHours > 0)
	//			//										{
	//			//											result = LessThanDayToOrigination;
	//			//										}
	//			//										else
	//			//										{
	//			//											result = AfterBackToOrigination;
	//			//										}
	//			//									}
	//			//								}
	//			//
	//			//							}
	//			//						}
	//			//						else
	//			//						{
	//			//							result = NoToOrigination;
	//			//						}
	//			//					}
	//			//
	//			//				}
	//			//			}
	//		}
	//		return result;
	//	}
	
	public static ePnrFlightType getPnrFlightTypeByFlightStatus(eFlightStatus iFlightStatus)
	{
		if (isFlightStatusAtDestination(iFlightStatus))
		{
			return ePnrFlightType.RETURN;
		}
		else
		{
			return ePnrFlightType.DEPARTURE;
		}
		//		if (iFlightStatus == AppUtils.eFlightStatus.MoreThanDayToDestination)
		//		{
		//			return ePnrFlightType.DEPARTURE;
		//		}
		//		else if (iFlightStatus == AppUtils.eFlightStatus.LessThanDayToDestination)
		//		{
		//			return ePnrFlightType.DEPARTURE;
		//		}
		//		else if (iFlightStatus == AppUtils.eFlightStatus.MoreThanDayToOrigination)
		//		{
		//			//TODO avishay 06/07/17 ask tal and aviram about this
		//			return ePnrFlightType.DEPARTURE;
		//		}
		//		else if (iFlightStatus == AppUtils.eFlightStatus.LessThanDayToOrigination)
		//		{
		//			return ePnrFlightType.RETURN;
		//		}
		//		else if (iFlightStatus == AppUtils.eFlightStatus.AfterBackToOrigination)
		//		{
		//			return ePnrFlightType.RETURN;
		//		}
		//		else if (iFlightStatus == AppUtils.eFlightStatus.NoToOrigination)
		//		{
		//			//TODO avishay 06/07/17 ask tal and aviram about this
		//			return ePnrFlightType.DEPARTURE;
		//		}
		//
		//		//else iFlightStatus == AppUtils.eFlightStatus.Error
		//		//TODO avishay 06/07/17 ask tal and aviram about this
		//		return ePnrFlightType.DEPARTURE;
		
	}
	
	public static boolean isFlightStatusAtDestination(eFlightStatus iFlightStatus)
	{
		return iFlightStatus != null && (iFlightStatus == eFlightStatus.MoreThanDayToOrigination || iFlightStatus == eFlightStatus.NoToOrigination || iFlightStatus == eFlightStatus.AfterBackToOrigination);
	}
	
	public static String nullSafeCheckString(final String iStringToCheck)
	{
		String result = "";
		
		if (!TextUtils.isEmpty(iStringToCheck))
		{
			result = iStringToCheck;
		}
		
		return result;
	}
	
	@SuppressLint("InflateParams")
	public static AlertDialog createFlightDetailsDialog(Activity iActivity, UserActivePnr iUserActivePnr)
	{
		AlertDialog result = null;
		
		if (iActivity != null && iUserActivePnr != null)
		{
			View rootView = LayoutInflater.from(iActivity).inflate(R.layout.flight_details_dialog, null, false);
			
			ImageView ivClose = (ImageView) rootView.findViewById(R.id.iv_flightDetails_Close);
			TextView tvOrigin = (TextView) rootView.findViewById(R.id.tv_flightDetails_Origin);
			//			ImageView ivPlane = (ImageView) rootView.findViewById(R.id.iv_flightDetails_Plane);
			TextView tvDestination = (TextView) rootView.findViewById(R.id.tv_myFlightTop_Destination);
			TextView originName = (TextView) rootView.findViewById(R.id.tv_flightDetails_OriginName);
			TextView tvFlightNumber = (TextView) rootView.findViewById(R.id.tv_flightDetails_FlightNumber);
			TextView tvDestinationName = (TextView) rootView.findViewById(R.id.tv_flightDetails_DestinationName);
			TextView tvFlightDate = (TextView) rootView.findViewById(R.id.tv_flightDetails_FlightDate);
			TextView tvDeparture = (TextView) rootView.findViewById(R.id.tv_flightDetails_Departure);
			TextView tvArrival = (TextView) rootView.findViewById(R.id.tv_flightDetails_Arrival);
			TextView tvClass = (TextView) rootView.findViewById(R.id.tv_flightDetails_Class);
			TextView tvStatus = (TextView) rootView.findViewById(R.id.tv_flightDetails_Status);
			RecyclerView rvPassengers = (RecyclerView) rootView.findViewById(R.id.rv_flightDetails_Passengers);
			TextView tvOriginTerminal = (TextView) rootView.findViewById(R.id.tv_flightDetails_OriginTerminal);
			TextView tvDestinationTerminal = (TextView) rootView.findViewById(R.id.tv_flightDetails_DestinationTerminal);
			
			/*PnrFlight currentFlight = iUserActivePnr.getCurrentItemFlightAtPositionOrNull(0);*/
			/**Avishay 25/06/17 solution just for now */
			PnrFlight currentFlight = iUserActivePnr.getCurrentItemFlightAtPositionOrNull(0, ePnrFlightType.DEPARTURE);
			if (currentFlight == null)
			{
				currentFlight = iUserActivePnr.getCurrentItemFlightAtPositionOrNull(0, ePnrFlightType.RETURN);
			}
			/*****/
			
			//			ivPlane.setImageResource(AppUtils.isDefaultLocaleRTL() ? R.mipmap.plane_blue_to_left : R.mipmap.plane_blue_to_right);
			
			if (currentFlight != null)
			{
				if (tvOrigin != null)
				{
					tvOrigin.setText(currentFlight.getOrigination());
				}
				tvDestination.setText(currentFlight.getDestination());
				originName.setText(currentFlight.getOriginationCityName());
				tvFlightNumber.setText(currentFlight.getFlightNumber());
				tvDestinationName.setText(currentFlight.getDestinationCityName());
				tvFlightDate.setText(DateTimeUtils.convertDateToSlashSeparatedDayMonthYearString(currentFlight.getDepartureDate()));
				tvDeparture.setText(DateTimeUtils.convertDateToHourMinuteString(currentFlight.getDepartureDate()));
				tvArrival.setText(DateTimeUtils.convertDateToHourMinuteString(currentFlight.getArrivalDate()));
				tvClass.setText(currentFlight.getClassOfService());
				tvStatus.setText(currentFlight.getFlightStatus());
				tvOriginTerminal.setText(currentFlight.getDepartTerminal());
				tvDestinationTerminal.setText("");
			}
			else
			{
				tvOrigin.setText("");
				tvDestination.setText("");
				originName.setText("");
				tvFlightNumber.setText("");
				tvDestinationName.setText("");
				tvFlightDate.setText("");
				tvDeparture.setText("");
				tvArrival.setText("");
				tvClass.setText("");
				tvOriginTerminal.setText("");
				tvDestinationTerminal.setText("");
			}
			
			rvPassengers.setLayoutManager(new LinearLayoutManager(iActivity));
			rvPassengers.setAdapter(new FlightDetailsPassengersAdapter(iUserActivePnr.getPassengers(), iUserActivePnr.getPnr()));
			
			result = new AlertDialog.Builder(iActivity).setView(rootView).create();
			
			//			Rect displayRectangle = new Rect();
			//			Window window = iActivity.getWindow();
			//			window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
			//			rootView.setMinimumWidth((int) (displayRectangle.width() * 0.95f));
			//			rootView.setMinimumHeight((int) (displayRectangle.height() * 0.95f));
			
			final AlertDialog finalResult = result;
			ivClose.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(final View v)
				{
					finalResult.dismiss();
				}
			});
		}
		
		return result;
	}
	
	public static void tryToSetTextGravityIfNeed(TextView iTv, final String iText)
	{
		if (UserData.getInstance().getLanguage().isHebrew() && AppUtils.isNumeric(iText.replace("-", "").replace("/", "").replaceAll("\\s+", "")))
		{
			iTv.setGravity(Gravity.END);
		}
		else if (UserData.getInstance().getLanguage().isHebrew() && !AppUtils.isStringContainsHebrewCharactersAndNumbersOnly(iText))
		{
			iTv.setGravity(Gravity.START);
		}
	}
	
	public static boolean isNumeric(String iStr)
	{
		return iStr.matches("-?\\d+(\\.\\d+)?");
	}
	
	public static boolean isStringContainsHebrewCharactersAndNumbersOnly(final String iStringToCheck)
	{
		boolean result = true;
		
		String matchesStr = "פםןוטארקףךלחיעכגדשץתצמנהבסז " + "\u0020" + "0123456789";
		char[] chars = iStringToCheck.replace("(", "").replace(")", "").replace(" ", "").replace(",", "").toCharArray();
		
		for (char tmpChar : chars)
		{
			if (!matchesStr.contains(Character.toString(tmpChar)))
			{
				result = false;
			}
		}
		
		return result;
	}
	
	public static String getXMLFromUrl(String url)
	{
		BufferedReader br = null;
		try
		{
			HttpURLConnection conn = (HttpURLConnection) (new URL(url)).openConnection();
			br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			
			String line;
			final StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null)
			{
				sb.append(line).append("\n");
			}
			
			return sb.toString();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			try
			{
				if (br != null)
				{
					br.close();
				}
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public static void setEdittextUnderlineColor(final EditText iEditText, final int iUnderlineColor)
	{
		ColorStateList colorStateList = ColorStateList.valueOf(iUnderlineColor);
		ViewCompat.setBackgroundTintList(iEditText, colorStateList);
	}
	
	public static String encrypt(String strENC)
	{
		try
		{
			System.out.println("Original Params:" + strENC);
			
			AESEncryption tcaseb = new AESEncryption();
			String encrypted = tcaseb.encrypt(strENC);
			System.out.println("Params Encrypted:" + encrypted);
			
			String decrypted = tcaseb.decrypt(encrypted);
			System.out.println("Params decrypted:" + decrypted);
			
			return encrypted;
		}
		catch (NoSuchProviderException e)
		{
			e.printStackTrace();
		}
		catch (java.security.KeyException e)
		{
			e.printStackTrace();
		}
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}
		catch (NoSuchPaddingException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return "";
	}
	
	public static boolean checkValidChar(String iChar)
	{
		return checkRegexForChar(iChar, "[A-Za-z0-9]") || SPECIAL_CHARS_GROUP.contains(iChar);
	}
	
	private static String getRegexFor(String iCharsGroup)
	{
		return "[^" + iCharsGroup + "]";
	}
	
	public static void releaseIntent(Intent iIntent)
	{
		iIntent.replaceExtras(Bundle.EMPTY);
		iIntent.setAction("");
		iIntent.setData(null);
		iIntent.setFlags(0);
	}
	
	public static String generateRandomId(int aStart, long aEnd)
	{
		StringBuilder randomId = new StringBuilder();
		Random aRandom = new Random();
		
		for (int i = 0 ; i < 10 ; i++)
		{
			//get the range, casting to long to avoid overflow problems
			long range = aEnd - (long) aStart + 1;
			// compute a fraction of the range, 0 <= frac < range
			long fraction = (long) (range * aRandom.nextDouble());
			long randomNumber = fraction + (long) aStart;
			
			randomId.append(randomNumber);
		}
		
		return randomId.toString();
	}
	
	public enum eFlightStatus
	{
		Error,
		MoreThan48HoursToDeparture,
		LessThan48HoursToDeparture,
		LessThanDayToDeparture,
		DepartedNotArrivedToDestination,
		MoreThanDayToOrigination,
		LessThanDayToOrigination,
		AfterBackToOrigination,
		NoToOrigination;
	}
}
