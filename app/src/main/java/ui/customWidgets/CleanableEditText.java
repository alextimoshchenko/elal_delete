package ui.customWidgets;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created with care by Shahar Ben-Moshe on 03/12/15.
 */
// TODO: 19/03/17 Shahar change occurrences of "2" position to 2 or 0 depending on rtl.
public class CleanableEditText extends android.support.v7.widget.AppCompatAutoCompleteTextView implements View.OnTouchListener, View.OnFocusChangeListener
{
    private Drawable mClearTextDrawable;

    public CleanableEditText(Context context)
    {
        super(context);
        init();
    }

    public CleanableEditText(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    public CleanableEditText(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        init();
    }

    @Override
    public void setOnTouchListener(OnTouchListener l)
    {
        this.l = l;
    }

    @Override
    public void setOnFocusChangeListener(OnFocusChangeListener f)
    {
        this.f = f;
    }

    private OnTouchListener l;
    private OnFocusChangeListener f;

    @Override
    public boolean onTouch(View v, MotionEvent event)
    {
        if (getCompoundDrawables()[2] != null)
        {
            boolean tappedX = event.getX() > (getWidth() - getPaddingRight() - mClearTextDrawable.getIntrinsicWidth());
	        
            if (tappedX)
            {
                if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    setText("");
                }
                
                return true;
            }
        }

        return l != null && l.onTouch(v, event);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus)
    {
        if (hasFocus)
        {
            setClearIconVisible(!TextUtils.isEmpty(getText()));
        }
        else
        {
            setClearIconVisible(false);
        }
        if (f != null)
        {
            f.onFocusChange(v, hasFocus);
        }
    }

    @SuppressWarnings("deprecated")
    private void init()
    {
        mClearTextDrawable = getCompoundDrawables()[2];

        if (mClearTextDrawable == null)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                mClearTextDrawable = getResources().getDrawable(android.R.drawable.presence_offline, getContext().getTheme());
            }
            else
            {
                mClearTextDrawable = getResources().getDrawable(android.R.drawable.presence_offline);
            }
        }

        if (mClearTextDrawable != null)
        {
            mClearTextDrawable.setBounds(0, 0, mClearTextDrawable.getIntrinsicWidth(), mClearTextDrawable.getIntrinsicHeight());
        }

        setClearIconVisible(false);
        super.setOnTouchListener(this);
        super.setOnFocusChangeListener(this);
        setTextWatcher();
    }

    private void setTextWatcher()
    {
        addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (isFocused())
                {
                    setClearIconVisible(!TextUtils.isEmpty(CleanableEditText.this.getText().toString()));
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {
            }
        });
    }

    protected void setClearIconVisible(boolean visible)
    {
        boolean wasVisible = (getCompoundDrawables()[2] != null);
        if (visible != wasVisible)
        {
            Drawable x = visible ? mClearTextDrawable : null;
            setCompoundDrawables(getCompoundDrawables()[0], getCompoundDrawables()[1], x, getCompoundDrawables()[3]);
        }
    }
}