package webServices.responses.responseUserLogin;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserLogin
{
	@JsonProperty("UserID")
	private int mUserID;
	@JsonProperty("UserObject")
	private UserObject mUserObject;
	@JsonProperty("PNRNum") // returns PNR num if there is a flight coming next 48 hours
	private String mPNRNum;
	
	public UserLogin()
	{
	}
	
	public String getPNRNum()
	{
		return mPNRNum;
	}
	
	public int getUserID()
	{
		return mUserID;
	}
	
	public UserObject getUserObject()
	{
		return mUserObject;
	}
}
