package global;

import android.content.Context;

import interfaces.IChoseActionElement;

public class TypeOfTripElement implements IChoseActionElement
{
	private String mActionName;
	private int mActionId;
	
	public TypeOfTripElement(String iActionName, int iActionId)
	{
		mActionName = iActionName;
		mActionId = iActionId;
	}
	
	@Override
	public String getElementName(final Context iContext)
	{
		return mActionName;
	}
	
	@Override
	public String getElementName()
	{
		return mActionName;
	}
	
	@Override
	public int getElementId()
	{
		return mActionId;
	}
}
