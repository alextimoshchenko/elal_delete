package webServices.responses.getSearchDestiationCities;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import webServices.responses.getSearchGlobalData.GetSearchGlobalDataContentElalCities;

/**
 * Created with care by Alexey.T on 17/07/2017.
 */
public class ResponseSearchDestinationCitiesContainer
{
	@JsonProperty("DestinationCities")
	private List<GetSearchGlobalDataContentElalCities> mDestinationCities;
	
	public List<GetSearchGlobalDataContentElalCities> getDestinationCities()
	{
		return mDestinationCities;
	}
}
