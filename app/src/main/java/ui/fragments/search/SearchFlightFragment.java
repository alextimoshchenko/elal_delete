package ui.fragments.search;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.tagmanager.DataLayer;

import java.util.Date;
import java.util.List;
import java.util.Map;

import global.ElAlApplication;
import global.IntentExtras;
import global.UserData;
import il.co.ewave.elal.R;
import interfaces.IChoseActionElement;
import ui.activities.EWBaseActivity;
import ui.activities.SearchFlightActivity;
import ui.customWidgets.ChangeDirectionLinearLayout;
import ui.customWidgets.CustomScrollView;
import ui.customWidgets.ExpandCollapseAnimation;
import ui.dialogs.ChoseActionDialog;
import ui.fragments.EWBaseFragment;
import ui.fragments.LoginFragment;
import ui.fragments.WebViewFragment;
import utils.errors.ErrorsHandler;
import utils.errors.LocalError.eLocalError;
import utils.global.AppUtils;
import utils.global.DateTimeUtils;
import utils.global.ElalPreferenceUtils;
import utils.global.enums.eIntention;
import utils.global.enums.ePassengersType;
import utils.global.enums.eSearchDestinationType;
import utils.global.enums.eTabStatement;
import utils.global.enums.eTypesOfTrip;
import utils.global.searchFlightObjects.SearchObject;
import webServices.responses.ResponseSearchFlight;
import webServices.responses.getSearchDestiationCities.ResponseSearchDestinationCities;
import webServices.responses.getSearchDestiationCities.ResponseSearchDestinationCitiesContainer;
import webServices.responses.getSearchGlobalData.GetSearchGlobalDataContentElalCities;
import webServices.responses.getSearchGlobalData.GetSearchGlobalDataContentFlightClasses;
import webServices.responses.responseUserLogin.UserObject;

public class SearchFlightFragment extends EWBaseFragment implements Response.ErrorListener
{
	private final String TAG = SearchFlightFragment.class.getSimpleName();
	
	private ImageView mIvBackArrow;
	private TextView mTvMainIcon, mTvTypeOfClassMainText, mTvTypeOfClassHint, mTvFlightTicket, mTvNumberOfPassengersMainText, mTvNumberOfPassengersHint, mTvDatesMainText, mTvDatesError, mTvDatesHint, mTvAdditionalToDestinationMainText, mTvAdditionalToDestinationError, mTvAdditionalToDestinationHint, mTvAdditionalOriginDestinationMainText, mTvAdditionalOriginDestinationError, mTvAdditionalOriginDestinationHint, mTvOriginDestinationMainText, mTvOriginDestinationHint, mTvOriginDestinationError, mTvTypeOfTripMainText, mTvTypeOfTripHint, mTvAwardTicket, mTvPointsAndMoney, mTvToDestinationHint, mTvToDestinationError, mTvToDestinationMainText;
	private Button mBtnSearch;
	private ChangeDirectionLinearLayout mCdllAdditionalToDestinationMain, mCdllAdditionalOriginalDestinationMain, mCdllToDestinationIconContainer, mCdllToDestinationRootMain, mCdllOriginDestinationRootMain, mCdllDatesMain;
	private LinearLayout mLlFromAdditionalDestinationContainer;
	private ExpandCollapseAnimation mToDestinationIconContainerExpandCollapseAnimation, mFromAdditionalDestinationExpandCollapseAnimation;
	private CustomScrollView mSvSearchFlight;
	private static final int EXPAND_DURATION = 500;
	private int mCount = 0;
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_search_flights_layout);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		initReference();
		setListeners();
		setExpandableViews();
		pushScreenOpenEvent("Book a Flight");
	}
	
	private void setExpandableViews()
	{
		mToDestinationIconContainerExpandCollapseAnimation = new ExpandCollapseAnimation(mCdllToDestinationIconContainer, false, EXPAND_DURATION);
		mFromAdditionalDestinationExpandCollapseAnimation = new ExpandCollapseAnimation(mLlFromAdditionalDestinationContainer, false, EXPAND_DURATION);
	}
	
	private void animateCollapseExpandOfToDestinationIconContainer(final boolean iShouldExpand)
	{
		boolean isExpand = mToDestinationIconContainerExpandCollapseAnimation.isExpand();
		
		if ((iShouldExpand && !isExpand) || (!iShouldExpand && isExpand))
		{
			mToDestinationIconContainerExpandCollapseAnimation.reverseAndAnimate();
		}
	}
	
	private void animateCollapseExpandOfFromAdditionalDestination(final boolean iShouldExpand)
	{
		boolean isExpand = mFromAdditionalDestinationExpandCollapseAnimation.isExpand();
		
		if ((iShouldExpand && !isExpand) || (!iShouldExpand && isExpand))
		{
			mFromAdditionalDestinationExpandCollapseAnimation.reverseAndAnimate();
		}
	}
	
	private void setOffFlightFragmentErrors()
	{
		setErrorOriginDestination(false);
		setErrorToDestination(false);
		setErrorDates(false);
		setErrorAdditionalToDestination(false);
	}
	
	private void setTextOriginDestinationMainText(String iText)
	{
		mTvOriginDestinationMainText.setText(TextUtils.isEmpty(iText) ? "" : iText);
		AppUtils.tryToSetTextGravityIfNeed(mTvOriginDestinationMainText, iText);
	}
	
	private void initReference()
	{
		Activity act = getActivity();
		mBtnSearch = act.findViewById(R.id.btn_search_flight_search);
		mIvBackArrow = act.findViewById(R.id.iv_name_screen_back_arrow);
		mTvFlightTicket = act.findViewById(R.id.tv_search_flight_flight_ticket);
		mTvAwardTicket = act.findViewById(R.id.tv_search_flight_award_ticket);
		mTvPointsAndMoney = act.findViewById(R.id.tv_search_flight_points_and_money);
		mCdllToDestinationIconContainer = act.findViewById(R.id.cdll_search_flight_to_destination_icon_container);
		mLlFromAdditionalDestinationContainer = act.findViewById(R.id.ll_search_flight_from_additional_destination_container);
		mSvSearchFlight = act.findViewById(R.id.sv_search_flight);
		
		//Number of passenger
		View numberOfPassengers = act.findViewById(R.id.ll_search_flight_number_of_passengers);
		mTvNumberOfPassengersHint = numberOfPassengers.findViewById(R.id.tv_custom_tv_error_hint);
		mTvNumberOfPassengersMainText = numberOfPassengers.findViewById(R.id.tv_custom_tv_error_main_text);
		
		//Dates field
		View dates = act.findViewById(R.id.ll_search_flight_dates);
		mTvDatesHint = dates.findViewById(R.id.tv_custom_tv_error_hint);
		mTvDatesError = dates.findViewById(R.id.tv_custom_tv_error_error);
		mCdllDatesMain = dates.findViewById(R.id.cdll_custom_tv_error_root_main);
		mTvDatesMainText = dates.findViewById(R.id.tv_custom_tv_error_main_text);
		
		//Origin destination field
		View originDestination = act.findViewById(R.id.ll_search_flight_origin_destination);
		mTvOriginDestinationHint = originDestination.findViewById(R.id.tv_custom_tv_error_hint);
		mTvOriginDestinationError = originDestination.findViewById(R.id.tv_custom_tv_error_error);
		mTvOriginDestinationMainText = originDestination.findViewById(R.id.tv_custom_tv_error_main_text);
		mCdllOriginDestinationRootMain = originDestination.findViewById(R.id.cdll_custom_tv_error_root_main);
		
		//To destination field
		View toDestinationLayout = act.findViewById(R.id.fl_search_flight_to_destination);
		mTvToDestinationHint = toDestinationLayout.findViewById(R.id.tv_custom_tv_error_hint);
		mTvToDestinationError = toDestinationLayout.findViewById(R.id.tv_custom_tv_error_error);
		mCdllToDestinationRootMain = toDestinationLayout.findViewById(R.id.cdll_custom_tv_error_root_main);
		mTvToDestinationMainText = toDestinationLayout.findViewById(R.id.tv_custom_tv_error_main_text);
		
		//Type of trip
		View typeOfTrip = act.findViewById(R.id.ll_search_flight_type_of_trip);
		mTvTypeOfTripHint = typeOfTrip.findViewById(R.id.tv_custom_tv_error_hint);
		mTvTypeOfTripMainText = typeOfTrip.findViewById(R.id.tv_custom_tv_error_main_text);
		
		//Type of class
		mTvTypeOfClassHint = act.findViewById(R.id.tv_header_type_of_flight);
		mTvTypeOfClassMainText = act.findViewById(R.id.tv_search_flight_type_of_flight_main);
		
		//Additional origin destination
		View additionalOriginDestination = act.findViewById(R.id.ll_search_flight_additional_origin_destination);
		mTvAdditionalOriginDestinationHint = additionalOriginDestination.findViewById(R.id.tv_custom_tv_error_hint);
		mTvAdditionalOriginDestinationError = additionalOriginDestination.findViewById(R.id.tv_custom_tv_error_error);
		mTvAdditionalOriginDestinationMainText = additionalOriginDestination.findViewById(R.id.tv_custom_tv_error_main_text);
		mCdllAdditionalOriginalDestinationMain = additionalOriginDestination.findViewById(R.id.cdll_custom_tv_error_root_main);
		
		//Additional to destination
		View additionalToDestination = act.findViewById(R.id.ll_search_flight_additional_to_destination);
		mTvAdditionalToDestinationHint = additionalToDestination.findViewById(R.id.tv_custom_tv_error_hint);
		mTvAdditionalToDestinationError = additionalToDestination.findViewById(R.id.tv_custom_tv_error_error);
		mTvAdditionalToDestinationMainText = additionalToDestination.findViewById(R.id.tv_custom_tv_error_main_text);
		mCdllAdditionalToDestinationMain = additionalToDestination.findViewById(R.id.cdll_custom_tv_error_root_main);
		
		//Main Icon
		mTvMainIcon = act.findViewById(R.id.include).findViewById(R.id.tv_name_screen_title);
	}
	
	private void setGui()
	{
		SearchObject searchObject = SearchObject.getInstance();
		eTypesOfTrip typeOfTrip = searchObject.getCurrentTypeOfTrip();
		
		((TextView) getActivity().findViewById(R.id.tv_name_screen_title)).setText(getString(R.string.book_flight));
		
		//Tab names
		mTvFlightTicket.setText(eTabStatement.FLIGHT_TICKET.getTabName(getActivity()));
		mTvAwardTicket.setText(eTabStatement.AWARD_TICKET.getTabName(getActivity()));
		mTvPointsAndMoney.setText(eTabStatement.POINTS_AND_MONEY.getTabName(getActivity()));
		
		//Number of passenger
		mTvNumberOfPassengersHint.setText(R.string.passengers);
		String passengersNumber = String.valueOf(searchObject.getSelectedPassengersQuantity());
		mTvNumberOfPassengersMainText.setText(passengersNumber);
		AppUtils.tryToSetTextGravityIfNeed(mTvNumberOfPassengersMainText, passengersNumber);
		
		//Origin destination field
		mTvOriginDestinationHint.setText(R.string.origin_);
		
		String originDestinationCityName = searchObject.getCurrentOriginDestination().getCityName() == null ?
		                                   getString(R.string.from_where_are_you_going) :
		                                   searchObject.getCurrentOriginDestination().getCityName();
		
		if (TextUtils.isEmpty(originDestinationCityName))
		{
			mTvOriginDestinationMainText.setHint(R.string.from_where_are_you_going);
		}
		else
		{
			setTextOriginDestinationMainText(originDestinationCityName);
		}
		
		//To destination field
		mTvToDestinationHint.setText(R.string.destination_);
		String toDestinationCityName = searchObject.getCurrentToDestination().getCityName();
		if (TextUtils.isEmpty(toDestinationCityName))
		{
			setTextToDestinationMainText(toDestinationCityName);
			mTvToDestinationMainText.setHint(R.string.where_are_you_going);
		}
		else
		{
			setTextToDestinationMainText(toDestinationCityName);
		}
		
		//Additional origin destination
		mTvAdditionalOriginDestinationHint.setText(R.string.origin_);
		String additionalOriginDestinationCityName = searchObject.getAdditionalCurrentOriginDestination().getCityName();
		if (TextUtils.isEmpty(additionalOriginDestinationCityName))
		{
			mTvAdditionalOriginDestinationMainText.setText(null);
			mTvAdditionalOriginDestinationMainText.setHint(R.string.from_where_are_you_going);
		}
		else
		{
			mTvAdditionalOriginDestinationMainText.setText(additionalOriginDestinationCityName);
			AppUtils.tryToSetTextGravityIfNeed(mTvAdditionalOriginDestinationMainText, additionalOriginDestinationCityName);
		}
		
		//Additional to destination
		mTvAdditionalToDestinationHint.setText(R.string.destination_);
		String additionalToDestinationCityName = searchObject.getAdditionalCurrentToDestination().getCityName();
		
		if (TextUtils.isEmpty(additionalToDestinationCityName))
		{
			mTvAdditionalToDestinationMainText.setText(null);
			mTvAdditionalToDestinationMainText.setHint(R.string.where_are_you_going);
		}
		else
		{
			mTvAdditionalToDestinationMainText.setText(additionalToDestinationCityName);
			AppUtils.tryToSetTextGravityIfNeed(mTvAdditionalToDestinationMainText, additionalToDestinationCityName);
		}
		
		//Type of class
		mTvTypeOfClassHint.setText(R.string.class_);
		GetSearchGlobalDataContentFlightClasses item = searchObject.getCurrentFlightClass();
		
		String currentFlightClassName = searchObject.getCurrentFlightClass() == null ? "" : item.getElementName(getContext());
		
		mTvTypeOfClassMainText.setText(currentFlightClassName);
		AppUtils.tryToSetTextGravityIfNeed(mTvTypeOfClassMainText, currentFlightClassName);
		
		//Calendar drawable
		if (UserData.getInstance().getLanguage().isHebrew())
		{
			mTvDatesMainText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.search_calendar, 0);
		}
		else if (UserData.getInstance().getLanguage().isEnglish())
		{
			mTvDatesMainText.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.search_calendar, 0, 0, 0);
		}
		
		//Dates main text
		mTvDatesHint.setText(R.string.dates);
		Date currentFirstDate = searchObject.getCurrentFirstDateInSelection();
		Date currentLastDate = searchObject.getCurrentLastDateInSelection();
		
		eTypesOfTrip typesOfTrip = searchObject.getCurrentTypeOfTrip();
		
		if (typesOfTrip.isReturnFromAnotherDestination() || typesOfTrip.isRoundTrip())
		{
			if (currentLastDate == null)
			{
				searchObject.setCurrentFirstDateInSelection(null);
			}
		}
		
		String firstInSelection = currentFirstDate == null ? "" : DateTimeUtils.convertDateToSlashSeparatedDayMonthYearTwoNumbersString(currentFirstDate);
		String lastInSelection = currentLastDate == null ? "" : DateTimeUtils.convertDateToSlashSeparatedDayMonthYearTwoNumbersString(currentLastDate);
		boolean isOneWayTicket = typeOfTrip.isOneWayTicket();
		
		String datesResult = firstInSelection + (isOneWayTicket ? "" : getString(R.string.dash_sing)) + lastInSelection;
		
		if ((isOneWayTicket && !TextUtils.isEmpty(firstInSelection)) || (!TextUtils.isEmpty(firstInSelection) && !TextUtils.isEmpty(lastInSelection)))
		{
			mTvDatesMainText.setText(" " + datesResult + " ");
		}
		else
		{
			mTvDatesMainText.setText(null);
			
			//I need set datesResult " - " , because of AppUtils.tryToSetTextGravityIfNeed method. Without it if app run in Hebrew and user select dates from round trip and after selection changing
			// tipe of trip from "round trip" to "one way ticket" hint word of dates goes to the left side.
			datesResult = " - ";
			
			if (typeOfTrip.isOneWayTicket())
			{
				mTvDatesMainText.setHint(" " + getString(R.string.depart) + " ");
			}
			else
			{
				mTvDatesMainText.setHint(" " + getString(R.string.depart_and_return) + " ");
			}
		}
		
		AppUtils.tryToSetTextGravityIfNeed(mTvDatesMainText, datesResult);
		
		//Type of trip
		mTvTypeOfTripHint.setText(R.string.travel_route);
		mTvTypeOfTripMainText.setText(typeOfTrip.getElementName(getContext()));
		
		if (typeOfTrip.isReturnFromAnotherDestination())
		{
			animateCollapseExpandOfFromAdditionalDestination(true);
			animateCollapseExpandOfToDestinationIconContainer(true);
		}
		else
		{
			animateCollapseExpandOfFromAdditionalDestination(false);
			animateCollapseExpandOfToDestinationIconContainer(false);
			searchObject.setAdditionalCurrentOriginDestination(null);
		}
		
		//Tabs
		eTabStatement statement = searchObject.getCurrentTabStatement();
		if (statement.isFlightTicket())
		{
			setUiTabFlightTicket();
		}
		else if (statement.isAwardTicket())
		{
			setUiTabAwardTicket();
		}
		else if (statement.isPointsAndMoneyTicket())
		{
			setUiTabPointsAndMoney();
		}
	}
	
	private void setListeners()
	{
		mTvMainIcon.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnMainIconClick();
			}
		});
		mTvAdditionalToDestinationMainText.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnAdditionalToDestinationMainTextClick();
			}
		});
		mTvNumberOfPassengersMainText.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnNumberOfPassengersMainTextClick();
			}
		});
		mTvTypeOfClassMainText.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnClickTypeOfClass();
			}
		});
		mBtnSearch.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnClickSearch();
			}
		});
		mIvBackArrow.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnBackClick();
			}
		});
		mTvFlightTicket.setOnClickListener(tabListener);
		mTvAwardTicket.setOnClickListener(tabListener);
		mTvPointsAndMoney.setOnClickListener(tabListener);
		mTvOriginDestinationMainText.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnClickOriginDestinationMainText();
			}
		});
		mTvAdditionalOriginDestinationMainText.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnClickAdditionalOriginDestination();
			}
		});
		mTvToDestinationMainText.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnClickToDestinationMainText();
			}
		});
		mTvDatesMainText.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnClickDatesMainText();
			}
		});
		mTvTypeOfTripMainText.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnClickTypeOfTrip();
			}
		});
	}
	
	private void doOnMainIconClick()
	{
		mCount++;
		
		if (mCount == 5)
		{
			ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.SECRET_COUNT, true);
			Toast.makeText(getActivity(), "What do you think? Book a flight", Toast.LENGTH_SHORT).show();
			mCount = 0;
			
			new Handler().postDelayed(new Runnable()
			{
				@Override
				public void run()
				{
					ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.SECRET_COUNT, false);
				}
			}, 2000);
		}
	}
	
	private void doOnAdditionalToDestinationMainTextClick()
	{
		SearchObject searchObject = SearchObject.getInstance();
		
		if (TextUtils.isEmpty(searchObject.getAdditionalCurrentOriginDestination().getCityCode()))
		{
			setErrorAdditionalOriginDestination(true);
			return;
		}
		
		if (isServiceConnected() && (isResumed() || isVisible()))
		{
			String cityCode = "";
			GetSearchGlobalDataContentElalCities city = searchObject.getAdditionalCurrentOriginDestination();
			
			if (city != null)
			{
				cityCode = city.getCityCode();
			}
			
			((SearchFlightActivity) getActivity()).getSearchDestinationCities(cityCode, new Response.Listener<ResponseSearchDestinationCities>()
			{
				@Override
				public void onResponse(final ResponseSearchDestinationCities response)
				{
					trySetProgressDialog(false);
					
					if (response != null && response.getContent() != null)
					{
						setSearchDestinationCitiesResponse(response.getContent());
						setErrorToDestination(false);
						openFragmentDestination(eSearchDestinationType.FIND_ADDITIONAL_TO_DESTINATION);
					}
				}
			}, this);
		}
	}
	
	private void doOnNumberOfPassengersMainTextClick()
	{
		Date firstDateInSelection = SearchObject.getInstance().getCurrentFirstDateInSelection();
		
		if (firstDateInSelection != null || UserData.getInstance().isUserGuest())
		{
			tryGoToFragmentByFragmentClass(SearchPassengersFragment.class, Bundle.EMPTY, true);
		}
		else
		{
			setErrorDates(true);
		}
	}
	
	private void doOnClickTypeOfClass()
	{
		new ChoseActionDialog<>(getContext(), ChoseActionDialog.ePositionState.CLASS, new ChoseActionDialog.IChoseActionListener()
		{
			@Override
			public void onActionClick(final IChoseActionElement iChoseElement)
			{
				SearchObject.getInstance().setCurrentFlightClass(iChoseElement);
				setGui();
			}
		}, SearchObject.getInstance().getFlightClassesList()).show();
	}
	
	private void doOnClickSearch()
	{
		if (isFieldsValid())
		{
			doOnSaveFlightInHistoryRequest();
		}
		else
		{
			AppUtils.printLog(Log.DEBUG, TAG, eLocalError.SomeFieldAreNotValid.getErrorMessage());
		}
		
		SearchObject searchObject = SearchObject.getInstance();
		if (searchObject != null)
		{
			String originCityCode = searchObject.getCurrentOriginDestination().getCityCode();
			String destinationCityCode = searchObject.getCurrentToDestination().getCityCode();
			String firstInSelection = DateTimeUtils.convertDateToSlashSeparatedDayMonthYearString(searchObject.getCurrentFirstDateInSelection());
			String lastInSelection = DateTimeUtils.convertDateToSlashSeparatedDayMonthYearString(searchObject.getCurrentLastDateInSelection());
			int pasQuantity = searchObject.getSelectedPassengersQuantity();
			String flightClass = searchObject.getCurrentFlightClass() != null && getContext() != null ? searchObject.getCurrentFlightClass().getElementName(getContext()) : "";
			
			//event 24
			Map<String, Object> dataLayer = DataLayer.mapOf("Category", "Flight Search Details", "Action", originCityCode + " > " + destinationCityCode + "<> " + searchObject.getCurrentOriginDestination() + " > " + searchObject.getCurrentToDestination().getCityCode(), "Label", firstInSelection + " - " + lastInSelection + " : Pr(" + pasQuantity + ") : " + flightClass);
			
			((EWBaseActivity) getActivity()).pushCustomEvent(dataLayer);
		}
	}
	
	private void doOnSaveFlightInHistoryRequest()
	{
		if (isServiceConnected() && (isResumed() || isVisible()))
		{
			((SearchFlightActivity) getActivity()).saveFlightInHistory(new Response.Listener<ResponseSearchFlight>()
			{
				@Override
				public void onResponse(final ResponseSearchFlight response)
				{
					trySetProgressDialog(false);
					if (response != null && response.getContent() != null && response.getContent().getSuccess())
					{
						doOnOpenSearchFlightWebView();
					}
					else
					{
						AppUtils.printLog(Log.ERROR, TAG, eLocalError.GeneralError.getErrorMessage());
					}
				}
			}, this);
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, eLocalError.GeneralError.getErrorMessage());
		}
	}
	
	private void doOnOpenSearchFlightWebView()
	{
		Bundle bundle = new Bundle();
		bundle.putString(IntentExtras.URL, SearchObject.getInstance().getSearchFlightUrlNoENC());
		bundle.putString(IntentExtras.ENC_PARAMETERS, SearchObject.getInstance().getSearchFlightParameters());
		bundle.putString(IntentExtras.SEARCH_VIEW_WEB_VIEW_TITLE, getString(R.string.start_over));
		tryGoToFragmentByFragmentClass(WebViewFragment.class, bundle, true);
	}
	
	private boolean isFieldsValid()
	{
		boolean result = true;
		
		SearchObject searchObject = SearchObject.getInstance();
		eTypesOfTrip currentTypeOfTrip = searchObject.getCurrentTypeOfTrip();
		
		//Origin Destination
		if (TextUtils.isEmpty(mTvOriginDestinationMainText.getText().toString()))
		{
			setErrorOriginDestination(true);
			result = false;
		}
		
		//To Destination
		if (TextUtils.isEmpty(mTvToDestinationMainText.getText().toString()))
		{
			setErrorToDestination(true);
			result = false;
		}
		
		//Dates
		if (TextUtils.isEmpty(mTvDatesMainText.getText().toString()))
		{
			setErrorDates(true);
			result = false;
		}
		
		//Additional Origin Destination
		if (currentTypeOfTrip.isReturnFromAnotherDestination() && TextUtils.isEmpty(mTvAdditionalOriginDestinationMainText.getText().toString()))
		{
			setErrorAdditionalOriginDestination(true);
			result = false;
		}
		
		//Additional To Destination
		if (currentTypeOfTrip.isReturnFromAnotherDestination() && TextUtils.isEmpty(mTvAdditionalToDestinationMainText.getText().toString()))
		{
			setErrorAdditionalToDestination(true);
			result = false;
		}
		
		//Passengers Quantity
		if (searchObject.getSelectedPassengersQuantity() < 1)
		{
			setErrorTvPassengersQuantity();
			result = false;
		}
		
		//Passengers exist adults
		if (!searchObject.isContainsUserFamilyListSelectedAdultsOrYouthAtLeastOne())
		{
			if (searchObject.isUserHimselfIs(ePassengersType.INFANTS))
			{
				setErrorTvPassengersInfants();
			}
			else if (searchObject.isUserHimselfIs(ePassengersType.CHILDREN))
			{
				setErrorTvPassengersChildren();
			}
			else if (searchObject.isUserHimselfIs(ePassengersType.TEENAGERS))
			{
				setErrorTvPassengersTeenagers();
			}
			
			result = false;
		}
		else if (!searchObject.isEnoughAdultsMembersForType(ePassengersType.INFANTS, eIntention.DECREASE))
		{
			setErrorTvPassengersInfants();
			result = false;
		}
		else if (!searchObject.isEnoughAdultsMembersForType(ePassengersType.CHILDREN, eIntention.DECREASE))
		{
			setErrorTvPassengersChildren();
			result = false;
		}
		else if (!searchObject.isEnoughAdultsMembersForType(ePassengersType.TEENAGERS, eIntention.DECREASE))
		{
			setErrorTvPassengersTeenagers();
			result = false;
		}
		
		return result;
	}
	
	private void setErrorTvPassengersTeenagers()
	{
		doOnNumberOfPassengersMainTextClick();
	}
	
	private void setErrorTvPassengersChildren()
	{
		doOnNumberOfPassengersMainTextClick();
	}
	
	private void setErrorTvPassengersInfants()
	{
		doOnNumberOfPassengersMainTextClick();
	}
	
	private void setErrorTvPassengersQuantity()
	{
		doOnNumberOfPassengersMainTextClick();
	}
	
	private void setErrorAdditionalToDestination(boolean iState)
	{
		mTvAdditionalToDestinationError.setText(R.string.please_select_origin_destination);
		mTvAdditionalToDestinationError.setVisibility(iState ? View.VISIBLE : View.INVISIBLE);
		mCdllAdditionalToDestinationMain.setEnabled(!iState);
	}
	
	private void setErrorAdditionalOriginDestination(boolean iState)
	{
		mTvAdditionalOriginDestinationError.setText(R.string.please_select_origin);
		mTvAdditionalOriginDestinationError.setVisibility(iState ? View.VISIBLE : View.INVISIBLE);
		mCdllAdditionalOriginalDestinationMain.setEnabled(!iState);
	}
	
	private void setErrorDates(boolean iState)
	{
		mTvDatesError.setText(R.string.please_select_dates);
		mTvDatesError.setVisibility(iState ? View.VISIBLE : View.INVISIBLE);
		mCdllDatesMain.setEnabled(!iState);
	}
	
	private void setErrorToDestination(boolean iState)
	{
		mTvToDestinationError.setText(R.string.please_select_to_destination);
		mTvToDestinationError.setVisibility(iState ? View.VISIBLE : View.INVISIBLE);
		mCdllToDestinationRootMain.setEnabled(!iState);
		
		if (iState && SearchObject.getInstance().getCurrentTypeOfTrip().isReturnFromAnotherDestination())
		{
			mSvSearchFlight.scrollToTop();
		}
	}
	
	private void setErrorOriginDestination(boolean iState)
	{
		mTvOriginDestinationError.setText(R.string.please_select_origin_destination);
		mTvOriginDestinationError.setVisibility(iState ? View.VISIBLE : View.INVISIBLE);
		mCdllOriginDestinationRootMain.setEnabled(!iState);
	}
	
	private void doOnClickTypeOfTrip()
	{
		final SearchObject searchObject = SearchObject.getInstance();
		
		new ChoseActionDialog<>(getContext(), ChoseActionDialog.ePositionState.TRAVEL_ROUTE, new ChoseActionDialog.IChoseActionListener()
		{
			@Override
			public void onActionClick(final IChoseActionElement iChoseElement)
			{
				searchObject.setCurrentFirstDateInSelection(null);
				searchObject.setCurrentLastDateInSelection(null);
				searchObject.setCurrentTypeOfTrip((eTypesOfTrip) iChoseElement);
				
				setGui();
			}
		}, getChoseActionElementsList()).show();
	}
	
	private List<IChoseActionElement> getChoseActionElementsList()
	{
		final SearchObject searchObject = SearchObject.getInstance();
		
		List<IChoseActionElement> elementList;
		
		//		if (searchObject.getCurrentTabStatement().isFlightTicket())
		if (false)
		{
			elementList = searchObject.getTypeOfTripList();
		}
		else
		{
			elementList = searchObject.getTypeOfTripListWithout(eTypesOfTrip.RETURN_FROM_ANOTHER_DESTINATION);
		}
		
		return elementList;
	}
	
	private void doOnClickDatesMainText()
	{
		setErrorDates(false);
		tryGoToFragmentByFragmentClass(SearchDatesFragment.class, null, true);
	}
	
	private void doOnClickOriginDestinationMainText()
	{
		setErrorOriginDestination(false);
		openFragmentDestination(eSearchDestinationType.FIND_ORIGIN_DESTINATION);
	}
	
	private void doOnClickToDestinationMainText()
	{
		SearchObject searchObject = SearchObject.getInstance();
		
		if (TextUtils.isEmpty(searchObject.getCurrentOriginDestination().getCityCode()))
		{
			setErrorOriginDestination(true);
			return;
		}
		
		if (isServiceConnected() && (isResumed() || isVisible()))
		{
			String cityCode = "";
			
			GetSearchGlobalDataContentElalCities city = searchObject.getCurrentOriginDestination();
			
			if (city != null)
			{
				cityCode = city.getCityCode();
			}
			
			((SearchFlightActivity) getActivity()).getSearchDestinationCities(cityCode, new Response.Listener<ResponseSearchDestinationCities>()
			{
				@Override
				public void onResponse(final ResponseSearchDestinationCities response)
				{
					trySetProgressDialog(false);
					
					if (response != null && response.getContent() != null)
					{
						setSearchDestinationCitiesResponse(response.getContent());
						setErrorToDestination(false);
						openFragmentDestination(eSearchDestinationType.FIND_TO_DESTINATION);
					}
				}
			}, this);
		}
	}
	
	private void setSearchDestinationCitiesResponse(final ResponseSearchDestinationCitiesContainer iContent)
	{
		SearchObject searchObject = SearchObject.getInstance();
		searchObject.setElalDestinationCitiesList(iContent.getDestinationCities());
	}
	
	private void doOnClickAdditionalOriginDestination()
	{
		setErrorAdditionalOriginDestination(false);
		openFragmentDestination(eSearchDestinationType.FIND_ADDITIONAL_ORIGIN_DESTINATION);
	}
	
	private void openFragmentDestination(eSearchDestinationType iType)
	{
		SearchObject.getInstance().setCurrentDestinationType(iType);
		tryGoToFragmentByFragmentClass(SearchDestinationFragment.class, null, true);
	}
	
	private View.OnClickListener tabListener = new View.OnClickListener()
	{
		@Override
		public void onClick(final View iV)
		{
			eTabStatement currentTab = SearchObject.getInstance().getCurrentTabStatement();
			
			//Here I just check if user really switch the tab, or maybe he clicked on the same tab
			if (currentTab.getTabName(getActivity()).equals(((TextView) iV).getText()))
			{
				return;
			}
			
			//If user switch between the tabs, he will get different destination list according to another tab.
			SearchObject searchObject = SearchObject.getInstance();
			searchObject.setCurrentToDestination(null);
			
			int viewId = iV.getId();
			
			if (viewId == R.id.tv_search_flight_flight_ticket)
			{
				doOnClickTabFlightTicket();
			}
			else
			{
				UserObject user = UserData.getInstance().getUserObject();
				boolean isMatmid = user != null && user.isMatmid();
				
				if (isMatmid)
				{
					if (viewId == R.id.tv_search_flight_award_ticket)
					{
						doOnClickTabAwardTicket();
					}
					else if (viewId == R.id.tv_search_flight_points_and_money)
					{
						doOnClickTabPointsAndMoney();
					}
				}
				else
				{
					openLoginDialog();
				}
			}
			
			setGui();
		}
	};
	
	private void doOnClickTabFlightTicket()
	{
		SearchObject.getInstance().setCurrentTabStatement(eTabStatement.FLIGHT_TICKET);
	}
	
	private void doOnClickTabPointsAndMoney()
	{
		SearchObject searchObject = SearchObject.getInstance();
		searchObject.setCurrentTabStatement(eTabStatement.POINTS_AND_MONEY);
		searchObject.setCurrentTypeOfTrip(eTypesOfTrip.ROUND_TRIP);
		
		
	}
	
	private void doOnClickTabAwardTicket()
	{
		SearchObject searchObject = SearchObject.getInstance();
		searchObject.setCurrentTabStatement(eTabStatement.AWARD_TICKET);
		searchObject.setCurrentTypeOfTrip(eTypesOfTrip.ROUND_TRIP);
		
		clearSelectedData(searchObject);

	}
	
	private void clearSelectedData(SearchObject iSearchObject)
	{
		// clear dates
		iSearchObject.setCurrentFirstDateInSelection(null);
		iSearchObject.setCurrentLastDateInSelection(null);
		//avishay : must be before the iSearchObject.resetUserFamilyList()!!
		iSearchObject.clearAllNotFamilyByType(ePassengersType.ADULTS);
		iSearchObject.clearAllNotFamilyByType(ePassengersType.CHILDREN);
		iSearchObject.clearAllNotFamilyByType(ePassengersType.INFANTS);
		iSearchObject.clearAllNotFamilyByType(ePassengersType.TEENAGERS);
		iSearchObject.clearAllNotFamilyByType(ePassengersType.YOUTH);
		// clear family members
		iSearchObject.resetUserFamilyList();
	}
	
	private void setUiTabPointsAndMoney()
	{
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
		{
			mTvFlightTicket.setBackground(getResources().getDrawable(R.drawable.text_view_tab_disabled, getActivity().getTheme()));
			mTvAwardTicket.setBackground(getResources().getDrawable(R.drawable.text_view_tab_disabled, getActivity().getTheme()));
			mTvPointsAndMoney.setBackground(getResources().getDrawable(R.drawable.text_view_tab_enabled, getActivity().getTheme()));
		}
		else
		{
			mTvFlightTicket.setBackground(getResources().getDrawable(R.drawable.text_view_tab_disabled));
			mTvAwardTicket.setBackground(getResources().getDrawable(R.drawable.text_view_tab_disabled));
			mTvPointsAndMoney.setBackground(getResources().getDrawable(R.drawable.text_view_tab_enabled));
		}
	}
	
	private void setUiTabFlightTicket()
	{
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
		{
			mTvFlightTicket.setBackground(getResources().getDrawable(R.drawable.text_view_tab_enabled, getActivity().getTheme()));
			mTvAwardTicket.setBackground(getResources().getDrawable(R.drawable.text_view_tab_disabled, getActivity().getTheme()));
			mTvPointsAndMoney.setBackground(getResources().getDrawable(R.drawable.text_view_tab_disabled, getActivity().getTheme()));
		}
		else
		{
			mTvFlightTicket.setBackground(getResources().getDrawable(R.drawable.text_view_tab_enabled));
			mTvAwardTicket.setBackground(getResources().getDrawable(R.drawable.text_view_tab_disabled));
			mTvPointsAndMoney.setBackground(getResources().getDrawable(R.drawable.text_view_tab_disabled));
		}
	}
	
	private void setUiTabAwardTicket()
	{
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
		{
			mTvFlightTicket.setBackground(getResources().getDrawable(R.drawable.text_view_tab_disabled, getActivity().getTheme()));
			mTvAwardTicket.setBackground(getResources().getDrawable(R.drawable.text_view_tab_enabled, getActivity().getTheme()));
			mTvPointsAndMoney.setBackground(getResources().getDrawable(R.drawable.text_view_tab_disabled, getActivity().getTheme()));
		}
		else
		{
			mTvFlightTicket.setBackground(getResources().getDrawable(R.drawable.text_view_tab_disabled));
			mTvAwardTicket.setBackground(getResources().getDrawable(R.drawable.text_view_tab_enabled));
			mTvPointsAndMoney.setBackground(getResources().getDrawable(R.drawable.text_view_tab_disabled));
		}
	}
	
	private void openLoginDialog()
	{
		AppUtils.createSimpleMessageDialog(getContext(), null, getString(R.string.login_as_a_matmid), getString(R.string.log_in), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, final int which)
			{
				doOnLoginAsMatmid();
			}
		}, getString(R.string.back), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, final int which)
			{
				//back, just do nothing
			}
		}).show();
	}
	
	private void doOnBackClick()
	{
		getActivity().onBackPressed();
	}
	
	@Override
	public void onResume()
	{
		SearchObject.getInstance().updateSelectedStatusInMainList();
		setGui();
		AppUtils.hideKeyboard(getContext(), getView());
		setOffFlightFragmentErrors();
		super.onResume();
	}
	
	@Override
	public void onErrorResponse(final VolleyError iError)
	{
		ErrorsHandler.tryShowServiceErrorDialog(iError, getActivity());
	}
	
	private void doOnLoginAsMatmid()
	{
		Bundle bundle = new Bundle();
		bundle.putBoolean(IntentExtras.LOGIN_AS_MATMID, true);
		tryGoToFragmentByFragmentClass(LoginFragment.class, bundle, true);
	}
	
	public void setTextToDestinationMainText(final String iText)
	{
		String text = TextUtils.isEmpty(iText) ? "" : iText;
		mTvToDestinationMainText.setText(text);
		AppUtils.tryToSetTextGravityIfNeed(mTvToDestinationMainText, text);
	}
}
