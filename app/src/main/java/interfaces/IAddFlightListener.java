package interfaces;

import java.io.Serializable;

/**
 * Created with care by Shahar Ben-Moshe on 22/01/17.
 */

public interface IAddFlightListener extends Serializable
{
	void addFlightListener(boolean iDidAddFlight);
}
