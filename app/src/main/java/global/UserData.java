package global;

import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;

import utils.global.AppUtils;
import utils.global.DateTimeUtils;
import utils.global.ElalPreferenceUtils;
import utils.global.LocaleUtils;
import webServices.responses.responseGetUserActivePNRs.UserActivePnr;
import webServices.responses.responseUserLogin.UserObject;

public class UserData
{
	private static final int BOOT_COUNT_SHOW_REGISTRATION_MULTIPLIER = 5;
	private static final int MAX_TUTORIAL_SHOW_COUNT = 2;
	
	private static UserData mInstance = tryLoadUserDataFromPreferences();
	
	private String mUdid = AppUtils.getUserUdid();
	private int mBootCount;
	private String mEmail;
	private String mPassword;
	private eLanguage mLanguage;
	private int mRepresentationId;
	private int mIsSawTutorial;
	private int mUserID/* = -1*/;
	private UserObject mUserObject;
	private String mTemporaryPassword;
	private String mDescription;
	private String mFirstNameEn;
	private String mFamilyNameEn;
	private String mFirstNameHe;
	private String mFamilyNameHe;
	private String mDateOfBirth;
	private String mExpirationDate;
	private String mPassportNumber;
	private String mPhoneNumber;
	private String mCountryCode;
	private int mGuestUserId;
	private String mSMSession;
	private String mActivePnr;
	private int mSessionUpdateCounter = 0;
	private Date mLastSessionUpdate;
	
	private ArrayList<UserActivePnr> mGroupPnrs = new ArrayList<>();
	private boolean mIsFinishTutorial;
	
	private UserData()
	{
	}
	
	public static UserData getInstance()
	{
		if (mInstance == null)
		{
			mInstance = tryLoadUserDataFromPreferences();
		}
		
		return mInstance;
	}
	
	private static UserData tryLoadUserDataFromPreferences()
	{
		UserData userData = new UserData();
		try
		{
			
			userData.mBootCount = ElalPreferenceUtils.getSharedPreferenceIntOrDefault(ElAlApplication.getInstance(), ElalPreferenceUtils.BOOT_COUNT, 0);
			userData.mEmail = ElalPreferenceUtils.getSharedPreferenceStringOrNull(ElAlApplication.getInstance(), ElalPreferenceUtils.EMAIL);
			userData.mPassword = ElalPreferenceUtils.getSharedPreferenceStringOrNull(ElAlApplication.getInstance(), ElalPreferenceUtils.PASSWORD);
			userData.mLanguage = eLanguage.getLanguageByCodeOrDefault(ElalPreferenceUtils.getSharedPreferenceStringOrDefault(ElAlApplication.getInstance(), ElalPreferenceUtils.LANGUAGE, LocaleUtils.getDefaultLocaleLanguageCode()), null);
			userData.mRepresentationId = ElalPreferenceUtils.getSharedPreferenceIntOrDefault(ElAlApplication.getInstance(), ElalPreferenceUtils.REPRESENTATION, -1);
			userData.mIsSawTutorial = ElalPreferenceUtils.getSharedPreferenceIntOrDefault(ElAlApplication.getInstance(), ElalPreferenceUtils.IS_SAW_TUTORIAL, 0);
			userData.mDescription = ElalPreferenceUtils.getSharedPreferenceStringOrNull(ElAlApplication.getInstance(), ElalPreferenceUtils.DESCRIPTION);
			userData.mFirstNameEn = ElalPreferenceUtils.getSharedPreferenceStringOrNull(ElAlApplication.getInstance(), ElalPreferenceUtils.FIRST_NAME_EN);
			userData.mDateOfBirth = ElalPreferenceUtils.getSharedPreferenceStringOrNull(ElAlApplication.getInstance(), ElalPreferenceUtils.DATE);
			userData.mPassportNumber = ElalPreferenceUtils.getSharedPreferenceStringOrNull(ElAlApplication.getInstance(), ElalPreferenceUtils.PASSPORT_NUMBER);
			userData.mExpirationDate = ElalPreferenceUtils.getSharedPreferenceStringOrNull(ElAlApplication.getInstance(), ElalPreferenceUtils.EXPIRATION_DATE);
			userData.mFamilyNameHe = ElalPreferenceUtils.getSharedPreferenceStringOrNull(ElAlApplication.getInstance(), ElalPreferenceUtils.FAMILY_NAME_HE);
			userData.mFirstNameHe = ElalPreferenceUtils.getSharedPreferenceStringOrNull(ElAlApplication.getInstance(), ElalPreferenceUtils.FIRST_NAME_HE);
			userData.mFamilyNameEn = ElalPreferenceUtils.getSharedPreferenceStringOrNull(ElAlApplication.getInstance(), ElalPreferenceUtils.FAMILY_NAME_EN);
			userData.mPhoneNumber = ElalPreferenceUtils.getSharedPreferenceStringOrNull(ElAlApplication.getInstance(), ElalPreferenceUtils.PHONE_NUMBER);
			userData.mCountryCode = ElalPreferenceUtils.getSharedPreferenceStringOrNull(ElAlApplication.getInstance(), ElalPreferenceUtils.COUNTRY_CODE);
			userData.mSMSession = ElalPreferenceUtils.getSharedPreferenceStringOrNull(ElAlApplication.getInstance(), ElalPreferenceUtils.SMS_SESSION);
			userData.mActivePnr = ElalPreferenceUtils.getSharedPreferenceStringOrNull(ElAlApplication.getInstance(), ElalPreferenceUtils.ACTIVE_PNR);
			userData.mSessionUpdateCounter = ElalPreferenceUtils.getSharedPreferenceIntOrDefault(ElAlApplication.getInstance(), ElalPreferenceUtils.SESSION_COUNTER, 0);
			userData.mLastSessionUpdate = DateTimeUtils.convertStringToFileSaveDate(ElalPreferenceUtils.getSharedPreferenceStringOrDefault(ElAlApplication.getInstance(), ElalPreferenceUtils.SESSION_UPDATE_DATE, ""));
		}
		catch (Exception iE)
		{
			iE.printStackTrace();
		}
		return userData;
	}
	
	public void saveUserData()
	{
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.BOOT_COUNT, mBootCount);
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.EMAIL, mEmail);
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.DESCRIPTION, mDescription);
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.FIRST_NAME_EN, mFirstNameEn);
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.DATE, mDateOfBirth);
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.PASSPORT_NUMBER, mPassportNumber);
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.EXPIRATION_DATE, mExpirationDate);
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.FAMILY_NAME_HE, mFamilyNameHe);
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.FIRST_NAME_HE, mFirstNameHe);
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.FAMILY_NAME_EN, mFamilyNameEn);
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.PASSWORD, mPassword);
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.LANGUAGE, mLanguage.getLanguageCode());
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.REPRESENTATION, mRepresentationId);
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.IS_SAW_TUTORIAL, mIsSawTutorial);
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.PHONE_NUMBER, mPhoneNumber);
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.COUNTRY_CODE, mCountryCode);
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.SMS_SESSION, mSMSession);
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.ACTIVE_PNR, mActivePnr);
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.SESSION_COUNTER, mSessionUpdateCounter);
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.SESSION_UPDATE_DATE, DateTimeUtils.convertDateToFileSaveDateString(mLastSessionUpdate));
	}
	
	public String getUdid()
	{
		return mUdid;
	}
	
	public boolean shouldShowRegistrationActivity()
	{
		boolean result = true;
		
		//if user registered no need to show registration activity.
		if (!TextUtils.isEmpty(mEmail) && !TextUtils.isEmpty(mPassword))
		{
			result = false;
		}
		//if not registered show on first time, second time and every fifth time since then.
		else if (mBootCount > 2 && (mBootCount - 2) % BOOT_COUNT_SHOW_REGISTRATION_MULTIPLIER != 0)
		{
			result = false;
		}
		
		return result;
	}
	
	public void updateAndSaveBootCount()
	{
		mBootCount++;
		
		//to prevent getting to max int.
		if (mBootCount == BOOT_COUNT_SHOW_REGISTRATION_MULTIPLIER * 2)
		{
			mBootCount = BOOT_COUNT_SHOW_REGISTRATION_MULTIPLIER;
		}
		
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.BOOT_COUNT, mBootCount);
	}
	
	public boolean isSawTutorial()
	{
		return mIsSawTutorial >= MAX_TUTORIAL_SHOW_COUNT;
	}
	
	public void addSawTutorialCount()
	{
		mIsSawTutorial++;
	}
	
	public void setSawTutorial()
	{
		mIsSawTutorial = MAX_TUTORIAL_SHOW_COUNT;
	}
	
	public void setEmailAndPassword(final String iIEmail, final String iIPassword)
	{
		mEmail = iIEmail;
		mPassword = iIPassword;
	}
	
	public void setLanguage(final eLanguage iLanguage)
	{
		mLanguage = iLanguage;
	}
	
	public eLanguage getLanguage()
	{
		return mLanguage;
	}
	
	public int getRepresentationId()
	{
		return mRepresentationId;
	}
	
	public void setUserRepresentation(final int iRepresentationId)
	{
		mRepresentationId = iRepresentationId;
	}
	
	public void setUserId(final int iUserID)
	{
		mUserID = iUserID;
//		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.USER_ID, mUserID);
	}
	
	public int getUserID()
	{
		//		if (isUserGuest())
		//		{
		//			return getGuestUserId();
		//		}
		//		else
		//		{
		
//		if (mUserID <= 0)
//		{
//			mUserID = ElalPreferenceUtils.getSharedPreferenceIntOrDefault(ElAlApplication.getInstance(), ElalPreferenceUtils.USER_ID, mUserID);
//		}
		return mUserID;
		//		}
	}
	
	public UserObject getUserObject()
	{
		return mUserObject;
	}
	
	public void setUserObject(final UserObject iIUserObject)
	{
		mUserObject = iIUserObject;
		
		if (mUserObject != null)
		{
			mRepresentationId = mUserObject.getRepresentationID();
		}
	}
	
	public boolean isUserLoginDataAvailable()
	{
		return !TextUtils.isEmpty(mEmail) && !TextUtils.isEmpty(mPassword);
	}
	
	public boolean isUserGuest()
	{
		return mUserObject == null || mUserObject.isGuest();
	}
	
	public String getEmail()
	{
		if (mUserObject != null && !TextUtils.isEmpty(mUserObject.getEmail()))
		//		if (TextUtils.isEmpty(mEmail) && mUserObject != null && !TextUtils.isEmpty(mUserObject.getEmail()))
		{
			return mUserObject.getEmail();
		}
		return mEmail;
	}
	
	public void setDescription(final String iDescription)
	{
		mDescription = iDescription;
	}
	
	public String getDescription()
	{
		return mDescription;
	}
	
	public String getPassword()
	{
		String result = mPassword;
		
		if (TextUtils.isEmpty(result))
		{
			result = mTemporaryPassword;
		}
		
		return result;
	}
	
	public void clearUserData()
	{
		mBootCount = BOOT_COUNT_SHOW_REGISTRATION_MULTIPLIER - 1;
		mEmail = null;
		mPassword = null;
		mLanguage = eLanguage.getLanguageByCodeOrDefault(LocaleUtils.getDefaultLocaleLanguageCode(), eLanguage.English);
		//		mRepresentationId = -1;
		//		mUserID = -1;
		//		mUserID = mGuestUserId;
		
		mUserObject = null;
//		mUserObject = new UserObject();
		
		mDescription = null;
		mFirstNameEn = null;
		mDateOfBirth = null;
		mPassportNumber = null;
		mExpirationDate = null;
		mFamilyNameHe = null;
		mFamilyNameEn = null;
		mFirstNameHe = null;
		mPhoneNumber = null;
		mCountryCode = null;
		mActivePnr = null;
		mSMSession = null;
		mSessionUpdateCounter = 0;
		mLastSessionUpdate = null;
		
		mGroupPnrs = new ArrayList<>();
		
		saveUserData();
	}
	
	public String getDateOfBirth()
	{
		return mDateOfBirth;
	}
	
	public void setDateOfBirth(final String iDateOfBirth)
	{
		mDateOfBirth = iDateOfBirth;
	}
	
	public String getFamilyNameHe()
	{
		return mFamilyNameHe;
	}
	
	public void setFamilyNameHe(final String iFamilyNameHe)
	{
		mFamilyNameHe = iFamilyNameHe;
	}
	
	public String getFamilyNameEn()
	{
		return mFamilyNameEn;
	}
	
	public String getFirstNameHe()
	{
		return mFirstNameHe;
	}
	
	public void setFirstNameHe(final String iFirstNameHe)
	{
		mFirstNameHe = iFirstNameHe;
	}
	
	public void setFamilyNameEn(final String iFamilyNameEn)
	{
		mFamilyNameEn = iFamilyNameEn;
	}
	
	public String getFirstNameEn()
	{
		return mFirstNameEn;
	}
	
	public void setFirstNameEn(final String iFirstNameEn)
	{
		mFirstNameEn = iFirstNameEn;
	}
	
	public String getExpirationDate()
	{
		return mExpirationDate;
	}
	
	public String getPassportNumber()
	{
		if (mUserObject != null && !TextUtils.isEmpty(mUserObject.getPassportNum()))
		{
			return mUserObject.getPassportNum();
		}
		return mPassportNumber;
	}
	
	public void setPassportNumber(final String iPassportNumber)
	{
		mPassportNumber = iPassportNumber;
	}
	
	public void setExpirationDate(final String iExpirationDate)
	{
		mExpirationDate = iExpirationDate;
	}
	
	public String getPhoneNumber()
	{
		//		if (mUserObject != null && !TextUtils.isEmpty(mUserObject.getPhoneNumber()))
		if (TextUtils.isEmpty(mPhoneNumber) && mUserObject != null && !TextUtils.isEmpty(mUserObject.getPhoneNumber()))
		{
			return mUserObject.getPhoneNumber();
		}
		return mPhoneNumber;
	}
	
	public void setPhoneNumber(final String iPhoneNumber)
	{
		mPhoneNumber = iPhoneNumber;
	}
	
	public void setTemporaryPassword(final String iTemporaryPassword)
	{
		mTemporaryPassword = iTemporaryPassword;
		mBootCount = BOOT_COUNT_SHOW_REGISTRATION_MULTIPLIER - 1;
	}
	
	public String getTemporaryPassword()
	{
		return mTemporaryPassword;
	}
	
	public void setEmail(final String iEmail)
	{
		mEmail = iEmail;
	}
	
	public void setPassword(final String iPassword)
	{
		mPassword = iPassword;
	}
	
	public boolean isSavedPassword()
	{
		return !TextUtils.isEmpty(mPassword);
	}
	
	public void setGuestUserId(final int iGuestUserId)
	{
		mGuestUserId = iGuestUserId;
	}
	
	public int getGuestUserId()
	{
		//		if (mGuestUserId <= 0)
		//		{
		//			return getUserID();
		//		}
		return mGuestUserId;
	}
	
	
	public ArrayList<UserActivePnr> getGroupPnrs()
	{
		return mGroupPnrs;
	}
	
	public void setGroupPnrs(final ArrayList<UserActivePnr> iGroupPnrs)
	{
		mGroupPnrs = iGroupPnrs;
	}
	
	public void setIsFinishTutorial(final boolean iIsFinishTutorial)
	{
		mIsFinishTutorial = iIsFinishTutorial;
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.IS_FINISH_TUTORIAL, mIsFinishTutorial);
	}
	
	public String getActivePnr()
	{
		return mActivePnr;
	}
	
	public void setActivePnr(String mActivePnr)
	{
		this.mActivePnr = mActivePnr;
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.ACTIVE_PNR, mActivePnr);
	}
	
	public boolean isFinishTutorial()
	{
		return mIsFinishTutorial;
	}
	
	public void setSMSession(String mSMSession)
	{
		Log.d("UserData", "saving smSession: " + mSMSession);
		this.mSMSession = mSMSession;
	}
	
	public String getSMSession()
	{
		Log.d("UserData", "getting smSession: " + mSMSession);
		if (mSMSession != null && !TextUtils.isEmpty(mSMSession))
		{
			return mSMSession;
		}
		else
		{
			return "";
		}
	}
	
	public void updateSessionCounter()
	{
		mSessionUpdateCounter++;
	}
	
	public int getSessionUpdateCounter()
	{
		return mSessionUpdateCounter;
	}
	
	public void resetSessionCounter()
	{
		mSessionUpdateCounter = 0;
	}
	
	public Date getLastSessionUpdateDate()
	{
		return mLastSessionUpdate;
	}
	
	public void setLastSessionUpdate(/*Date mLastSessionUpdate*/)
	{
		this.mLastSessionUpdate = new Date()/*mLastSessionUpdate*/;
	}
	
	
	@Override
	public String toString()
	{
		return "UserData{" + "mUdid='" + mUdid + '\'' + ", mBootCount=" + mBootCount + ", mEmail='" + mEmail + '\'' + ", mPassword='" + mPassword + '\'' + ", mLanguage=" + mLanguage + ", mRepresentationId=" + mRepresentationId + ", mIsSawTutorial=" + mIsSawTutorial + ", mUserID=" + mUserID + ", mUserObject=" + mUserObject + ", mTemporaryPassword='" + mTemporaryPassword + '\'' + ", mDescription='" + mDescription + '\'' + ", mFirstNameEn='" + mFirstNameEn + '\'' + ", mFamilyNameEn='" + mFamilyNameEn + '\'' + ", mFirstNameHe='" + mFirstNameHe + '\'' + ", mFamilyNameHe='" + mFamilyNameHe + '\'' + ", mDateOfBirth='" + mDateOfBirth + '\'' + ", mExpirationDate='" + mExpirationDate + '\'' + ", mPassportNumber='" + mPassportNumber + '\'' + ", mPhoneNumber='" + mPhoneNumber + '\'' + ", mGuestUserId=" + mGuestUserId + ", mSMSession='" + mSMSession + '\'' + ", mActivePnr='" + mActivePnr + '\'' + ", mSessionUpdateCounter=" + mSessionUpdateCounter + ", mLastSessionUpdate=" + mLastSessionUpdate + ", mGroupPnrs=" + mGroupPnrs + ", mIsFinishTutorial=" + mIsFinishTutorial + '}';
	}
	
	public String getCountryCode()
	{
		if (TextUtils.isEmpty(mCountryCode) && mUserObject != null && !TextUtils.isEmpty(mUserObject.getCountryCode()))
		{
			return mUserObject.getCountryCode();
		}
		return mCountryCode;
	}
	
	public void setCountryCode(String iCountryCode)
	{
		this.mCountryCode = iCountryCode;
	}
	
	
}
