package ui.customWidgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;

import java.util.Locale;

import il.co.ewave.elal.R;

/**
 * Created by Avishay.Peretz on 17/07/2017.
 */

public class ChangeDirectionTooltipEditText extends TooltipEditText
{
	public ChangeDirectionTooltipEditText(final Context context)
	{
		super(context);
	}
	
	public ChangeDirectionTooltipEditText(final Context context, final AttributeSet attrs)
	{
		super(context, attrs);
		handleAttributes(context, attrs, 0);
	}
	
	public ChangeDirectionTooltipEditText(final Context context, final AttributeSet attrs, final int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
		handleAttributes(context, attrs, defStyleAttr);
	}
	
	
	private boolean mIsTooltipAutoDetectRtlByValue = true;
	private boolean mIsRTL = false;
//	private Drawable mDrawable;
	
	
//	@Override
//	public void setButtonDrawable(Drawable drawable)
//	{
//		super.setButtonDrawable(drawable);
//
//		if (drawable != null)
//		{
//			mDrawable = drawable;
//			setButtonDrawable(null);
//			super.setButtonDrawable(getResources().getDrawable(R.drawable.transparent_shape));
//
//			if (isCheckboxTextRtl())
//			{
//				setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
//			}
//			else
//			{
//				setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
//			}
//			setCompoundDrawablePadding(15);
//		}
//	}
	
	private boolean isCheckboxTextRtl()
	{
		boolean result;
		
		if (mIsTooltipAutoDetectRtlByValue)
		{
			char charRtlEvaluation;
			
			if (TextUtils.isEmpty(this.getText().toString()))
			{
				charRtlEvaluation = Locale.getDefault().getDisplayName().charAt(0);
			}
			else
			{
				charRtlEvaluation = getText().toString().charAt(0);
			}
			
			result = Character.getDirectionality(charRtlEvaluation) == Character.DIRECTIONALITY_RIGHT_TO_LEFT;
		}
		else
		{
			result = mIsRTL;
		}
		
		return result;
	}
	
	private void handleAttributes(Context context, AttributeSet attrs, int defStyle)
	{
		TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ChangeDirectionTooltipEditText, defStyle, 0);
		
		mIsTooltipAutoDetectRtlByValue = typedArray.getBoolean(R.styleable.ChangeDirectionTooltipEditText_isTooltipRTL, true);
		mIsRTL = typedArray.getBoolean(R.styleable.ChangeDirectionTooltipEditText_isTooltipRTL, false);
		
//		setButtonDrawable(mDrawable);
		if (isCheckboxTextRtl())
		{
			setClearTextDrawableRTL(true);
		}
		else
		{
			setClearTextDrawableRTL(false);
		}
		
		typedArray.recycle();
	}
	
	
}
