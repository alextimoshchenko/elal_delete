package webServices.responses.getPnrByNum;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import webServices.responses.getMultiplePNRDestinations.DestinationListItem;
import webServices.responses.responseGetUserActivePNRs.PnrMeals;
import webServices.responses.responseGetUserActivePNRs.PnrTicket;

public class GetPnrByNum
{
	@JsonProperty("PNRMeals")
	private ArrayList<PnrMeals> mPNRMeals = new ArrayList<>();
	
	@JsonProperty("PNRCheckins")
	private GetPnrByNumPnrCheckins[]  mPNRCheckins;
	
	@JsonProperty("IsActive")
	private Object mIsActive;
	
	@JsonProperty("PNRSeats")
	private GetPnrByNumContentPNRSeats[]  mPNRSeats;
	
	@JsonProperty("IsGroupPnr")
	private boolean mIsGroupPnr;
	
	@JsonProperty("PNRFlights")
	private GetPnrByNumContentPNRFlights[]  mPNRFlights;
	
	@JsonProperty("CreateDate")
	private String  mCreateDate;
	
	@JsonProperty("CurrencyCode")
	private Object mCurrencyCode;
	
	@JsonProperty("LastModified")
	private String mLastModified;
	
	@JsonProperty("TicketingDate")
	private Object mTicketingDate;
	
	@JsonProperty("PNR")
	private String mPNR;
	
	@JsonProperty("PNRTickets")
	private PnrTicket[] mPNRTickets;
	
	@JsonProperty("TotalFarePaid")
	private double mTotalFarePaid;
	
	@JsonProperty("LastName")
	private String mLastName;
	
	public GetPnrByNum()
	{
	}
	
	public GetPnrByNumPnrCheckins[] getmPNRCheckins()
	{
		return mPNRCheckins;
	}
	
	public Object getmIsActive()
	{
		return mIsActive;
	}
	
	public GetPnrByNumContentPNRSeats[] getmPNRSeats()
	{
		return mPNRSeats;
	}
	
	public boolean ismIsGroupPnr()
	{
		return mIsGroupPnr;
	}
	
	public GetPnrByNumContentPNRFlights[] getmPNRFlights()
	{
		return mPNRFlights;
	}
	
	public String getmCreateDate()
	{
		return mCreateDate;
	}
	
	public Object getmCurrencyCode()
	{
		return mCurrencyCode;
	}
	
	public String getmLastModified()
	{
		return mLastModified;
	}
	
	public Object getmTicketingDate()
	{
		return mTicketingDate;
	}
	
	public String getmPNR()
	{
		return mPNR;
	}
	
	public PnrTicket[] getmPNRTickets()
	{
		return mPNRTickets;
	}
	
	public double getmTotalFarePaid()
	{
		return mTotalFarePaid;
	}
	
	public String getmLastName()
	{
		return mLastName;
	}
	
	public ArrayList<PnrMeals> getmPNRMeals()
	{
		return mPNRMeals;
	}
}
