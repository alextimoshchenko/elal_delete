package webServices.controllers;

import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import utils.global.CustomImageLoader;
import webServices.global.JacksonRequest;
import webServices.responses.ResponseIpInfo;

/**
 * Created with care by Shahar Ben-Moshe on 13/12/16.
 */

public abstract class AController
{
	private static final String IP_INFO_URL = "http://ipinfo.io/json";
	private static final int IMAGE_LOADER_CACHE_SIZE = 30;
	private static final int CONNECTION_TIME_OUT = 1500;
	
	private RequestQueue mRequestQueue;
	private CustomImageLoader mImageLoader;
	
	/**
	 * Sets the requestQueue for the controller provided from the service.
	 *
	 * @param iRequestQueue
	 */
	public void setVolleyRequestQueue(RequestQueue iRequestQueue)
	{
		mRequestQueue = iRequestQueue;
	}
	
	RequestQueue getRequestQueue()
	{
		return mRequestQueue;
	}
	
	boolean isRequestQueueNull()
	{
		return mRequestQueue == null;
	}
	
	public final void loadImage(String iImageUrl, CustomImageLoader.ImageListener iImageListener)
	{
		if (!isRequestQueueNull() && iImageListener != null)
		{
			try {
				getImageLoader().get(URLEncoder.encode(iImageUrl, "utf-8"), iImageListener);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
	}
	
	private CustomImageLoader getImageLoader()
	{
		
		if (mImageLoader == null)
		{
			mImageLoader = new CustomImageLoader(mRequestQueue, new CustomImageLoader.ImageCache()
			{
				private final LruCache<String, Bitmap> mCache = new LruCache<>(IMAGE_LOADER_CACHE_SIZE);
				
				@Override
				public Bitmap getBitmap(String url)
				{
					return mCache.get(url);
				}
				
				@Override
				public void putBitmap(String url, Bitmap bitmap)
				{
					mCache.put(url, bitmap);
				}
			});
		}
		
		return mImageLoader;
	}
	
	void getIpAddress(Response.Listener<ResponseIpInfo> iListener, Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull())
		{
			JacksonRequest<ResponseIpInfo> request = new JacksonRequest<>(Request.Method.GET, IP_INFO_URL, null, ResponseIpInfo.class, iListener, iErrorListener);
			request.setRetryPolicy(new DefaultRetryPolicy(CONNECTION_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
}
