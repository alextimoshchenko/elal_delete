package utils.global.notificationObjects;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import webServices.responses.deleteUserPushMessage.NotificationInstance;
import webServices.responses.deleteUserPushMessage.UserMessage;
import webServices.responses.getPushNotifications.ResponseGetPushNotifications;

/**
 * Created with care by Alexey.T on 25/07/2017.
 * <p>
 * TODO: Add a class header comment!
 */
public class NotificationObject
{
	private static final String TAG = NotificationObject.class.getSimpleName();
	
	private static NotificationObject mObject;
	
	//This field I use for checking if it is null so need to execute request to server otherwise no
	private ResponseGetPushNotifications mResponseGetPushNotifications;
	
	private List<UserMessage> mCurrentUserMessageList = new ArrayList<>();
	private NotificationInstance currentNotificationMessage = new NotificationInstance();
	
	private NotificationObject()
	{
	}
	
	public static NotificationObject getInstance()
	{
		if (mObject == null)
		{
			mObject = new NotificationObject();
		}
		
		return mObject;
	}
	
	public ResponseGetPushNotifications getResponseGetPushNotifications()
	{
		return mResponseGetPushNotifications;
	}
	
	public void setResponseGetPushNotifications(final ResponseGetPushNotifications iResponseGetPushNotifications)
	{
		mResponseGetPushNotifications = iResponseGetPushNotifications;
	}
	
	public List<UserMessage> getCurrentUserMessageList()
	{
		return mCurrentUserMessageList;
	}
	
	//	//TODO test delete it, above is correct
	//	public List<NotificationInstance> getCurrentUserMessageList()
	//	{
	//		return new ArrayList<NotificationInstance>()
	//		{{
	//			add(new NotificationInstance(getText(), DateTimeUtils.getDate(eCalendarUnit.DAY_OF_WEEK, 0), true));
	//			add(new NotificationInstance(getText(), DateTimeUtils.getDate(eCalendarUnit.DAY_OF_WEEK, -1), false));
	//			add(new NotificationInstance(getText(), DateTimeUtils.getDate(eCalendarUnit.DAY_OF_WEEK, -2), false));
	//			add(new NotificationInstance(getText(), DateTimeUtils.getDate(eCalendarUnit.DAY_OF_WEEK, -3), true));
	//			add(new NotificationInstance(getText(), DateTimeUtils.getDate(eCalendarUnit.DAY_OF_WEEK, -4), true));
	//			add(new NotificationInstance(getText(), DateTimeUtils.getDate(eCalendarUnit.DAY_OF_WEEK, -5), false));
	//			add(new NotificationInstance(getText(), DateTimeUtils.getDate(eCalendarUnit.DAY_OF_WEEK, -6), true));
	//			add(new NotificationInstance(getText(), DateTimeUtils.getDate(eCalendarUnit.DAY_OF_WEEK, -7), false));
	//			add(new NotificationInstance(getText(), DateTimeUtils.getDate(eCalendarUnit.DAY_OF_WEEK, -8), false));
	//			add(new NotificationInstance(getText(), DateTimeUtils.getDate(eCalendarUnit.DAY_OF_WEEK, -9), true));
	//		}};
	//	}
	//
	//for test
	private String getText()
	{
		String result = "";
		
		for (int i = 0 ; i < 10 ; i++)
		{
			result += UUID.randomUUID().toString();
		}
		return result;
	}
	
	public void setCurrentUserMessageList(final List<UserMessage> iCurrentUserMessageList)
	{
		mCurrentUserMessageList = iCurrentUserMessageList;
	}
	
	public void cleanAllCurrentData()
	{
		mResponseGetPushNotifications = null;
		mCurrentUserMessageList.clear();
	}
	
	public NotificationInstance getCurrentNotificationMessage()
	{
		return currentNotificationMessage;
	}
	
	public void setCurrentNotificationMessage(final NotificationInstance iCurrentNotificationMessage)
	{
		currentNotificationMessage = iCurrentNotificationMessage;
	}
}
