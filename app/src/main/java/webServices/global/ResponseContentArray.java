package webServices.global;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ResponseContentArray<T>
{
	@JsonProperty("Status")
	private boolean mStatus;
	@JsonProperty("ErrorCode")
	private int mErrorCode;
	@JsonProperty("ErrorMessage")
	private String mErrorMessage;
	@JsonProperty("Content")
	private List <T> mContent;
	
	public boolean isSuccess()
	{
		return mStatus;
	}
	
	public int getErrorCode()
	{
		return mErrorCode;
	}
	
	public String getErrorMessage()
	{
		return mErrorMessage;
	}
	
	public List<T> getContent()
	{
		return mContent;
	}
}