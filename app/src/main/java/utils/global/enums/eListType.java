package utils.global.enums;

/**
 * Created with care by Alexey.T on 19/07/2017.
 */
public enum eListType
{
	DEPARTURE_CITIES_LIST,
	DESTINATION_CITIES_LIST;
	
	eListType()
	{
	}
	
	public boolean isDepartureList()
	{
		return this == DEPARTURE_CITIES_LIST;
	}
	
	public boolean isDestinationList()
	{
		return this == DESTINATION_CITIES_LIST;
	}
}


