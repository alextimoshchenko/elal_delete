package ui.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.NetworkImageView;

import global.AppData;
import global.IntentExtras;
import global.UserData;
import global.eActivityModule;
import global.eInAppNavigation;
import global.eLanguage;
import il.co.ewave.elal.R;
import ui.activities.EWBaseActivity;
import ui.activities.EWBaseUserActivity;
import ui.customWidgets.ExpandCollapseAnimation;
import ui.customWidgets.ExpandCollapseAnimationRelativeLayout;
import ui.customWidgets.TypewriterTextView;
import utils.errors.ErrorsHandler;
import utils.global.AppUtils;
import utils.global.ElalPreferenceUtils;
import utils.global.googleAnalitics.googleTagManager.TagManagerImplementation.ePushEvents;
import webServices.controllers.LoginController;
import webServices.controllers.MainController;
import webServices.responses.ResponseGetHomeScreenContent;
import webServices.responses.getHomeScreenContent.HomeScreenContent;
import webServices.responses.getHomeScreenContent.HomeScreenItem;
import webServices.responses.resposeMatmidLogin.MatmidLogin;

/**
 * Created with care by Shahar Ben-Moshe on 05/01/17.
 */

public class HomeFragment extends EWBaseFragment implements Response.Listener<ResponseGetHomeScreenContent>, Response.ErrorListener
{
	private static final String TAG = HomeFragment.class.getSimpleName();
	private TextView mTvTopButton;
	private NetworkImageView mIvTopButton;
	private FrameLayout mBtnTopButton;
	private TextView mTvTopBadge;
	private TextView mTvMiddleLeftButton;
	private NetworkImageView mIvMiddleLeftButton;
	private TextView mTvMiddleRightButton;
	private NetworkImageView mIvMiddleRightButton;
	private TextView mTvBottomButton;
	private NetworkImageView mIvBottomButton;
	private HomeScreenContent mHomeItems;
	
	private TypewriterTextView mTvUsername;
	
	//Guest bar
	private View mLlGuestBar;
	private Button mBtnRegister;
	private Button mBtnLogin;
	
	//Matmid bar
	
	private View mLlMatmidBar;
	private TextView mTvMatmidPoints;
	private ImageView mIvMatmidLogo;
	private ExpandCollapseAnimation mMatmidBarExpandCollapseAnimation;
	private TextView mTvTopRightButton;
	private FrameLayout mBtnTopRightButton, mBtnBottomButton, mBtnMiddleLeftButton, mBtnMiddleRightButton;
	
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_home_layout_new);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		initReference();
		setListeners();
		//		setGui();
		setButtonsContent();
		
		setUserData();
		setReminderDialog();
	}
	
	
	private void initReference()
	{
		mTvUsername = getActivity().findViewById(R.id.tv_hello_username);
		
		
		mLlGuestBar = getActivity().findViewById(R.id.ll_guest_bar);
		mBtnRegister = getActivity().findViewById(R.id.btn_register);
		mBtnLogin = getActivity().findViewById(R.id.btn_login);
		
		mLlMatmidBar = getActivity().findViewById(R.id.ll_matmid_bar);
		mTvMatmidPoints = getActivity().findViewById(R.id.tv_matmid_points);
		mIvMatmidLogo = getActivity().findViewById(R.id.iv_matmid_logo);
		
		//Top left button
		mTvTopButton = getActivity().findViewById(R.id.tv_home_TopButton);
		mIvTopButton = getActivity().findViewById(R.id.iv_home_TopButton);
		mBtnTopButton = getActivity().findViewById(R.id.btn_home_TopButton);
		mTvTopBadge = getActivity().findViewById(R.id.tv_home_TopBadge);
		//Top right button
		mTvTopRightButton = getActivity().findViewById(R.id.tv_home_TopRightButton);
		mBtnTopRightButton = getActivity().findViewById(R.id.btn_home_TopRightButton);
		//Middle left button
		mTvMiddleLeftButton = getActivity().findViewById(R.id.tv_home_MiddleLeftButton);
		mIvMiddleLeftButton = getActivity().findViewById(R.id.iv_home_MiddleLeftButton);
		mBtnMiddleLeftButton = getActivity().findViewById(R.id.btn_home_MiddleLeftButton);
		//Middle right button
		mTvMiddleRightButton = getActivity().findViewById(R.id.tv_home_MiddleRightButton);
		mIvMiddleRightButton = getActivity().findViewById(R.id.iv_home_MiddleRightButton);
		mBtnMiddleRightButton = getActivity().findViewById(R.id.btn_home_MiddleRightButton);
		//Bottom button
		mTvBottomButton = getActivity().findViewById(R.id.tv_home_BottomButton);
		mIvBottomButton = getActivity().findViewById(R.id.iv_home_BottomButton);
		mBtnBottomButton = getActivity().findViewById(R.id.btn_home_BottomButton);
	}
	
	private void setListeners()
	{
		mBtnRegister.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				//				goToRegisterFragment();
				if (getActivity() != null && getActivity() instanceof EWBaseUserActivity)
				{
					((EWBaseUserActivity) getActivity()).goToRegistrationActivity();/*goToRegisterFragment()*/;
					((EWBaseActivity) getActivity()).pushEvent(ePushEvents.CREATE_ACCOUNT);
				}
			}
		});
		
		mBtnLogin.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				//				tryGoToFragmentByFragmentClass(LoginFragment.class, null, true);
				if (getActivity() != null && getActivity() instanceof EWBaseUserActivity)
				{
					((EWBaseUserActivity) getActivity()).goToLoginScreen();
					
					//event 106
					((EWBaseActivity) getActivity()).pushEvent(ePushEvents.REGISTER_CUSTOMER);
				}
			}
		});
		
		mBtnTopButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				onTopClick();
			}
		});
		
		mBtnTopRightButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				//TODO avishy 25/06/17 handle top-right button click
				onTopRightClick();
			}
		});
		
		mBtnMiddleLeftButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				onMiddleLeftClick();
			}
		});
		
		mBtnMiddleRightButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				onMiddleRightClick();
			}
		});
		
		mBtnBottomButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				//				onBottomClick();
				//				SearchFlightActivity.tryToStartSearchFlightActivity((EWBaseActivity) getActivity());
				((EWBaseActivity) getActivity()).pushEvent(ePushEvents.FLIGHT_SEARCH);
				tryGoToScreenById(eInAppNavigation.FlightSearch.getNavigationId(), eActivityModule.Main.getNavigationActivityClass(), Bundle.EMPTY);
			}
		});
	}
	
	private void onTopRightClick()
	{
		if (mHomeItems != null && mHomeItems.getTop2Item() != null)
		{
			onHomeScreenItemClick(mHomeItems.getTop2Item());
		}
		
		((EWBaseActivity) getActivity()).pushEvent(ePushEvents.TOP_TWO);
	}
	
	private void setReminderDialog()
	{
		if (UserData.getInstance().isUserLoginDataAvailable() || !UserData.getInstance().isUserGuest())
		{
			if (ElalPreferenceUtils.getSharedPreferenceBooleanOrFalse(getActivity(), AppUtils.getRemindMeDetailsKey(UserData.getInstance().getUserID())))
			{
				if (!AppUtils.isSawReminderDialogDetails())
				
				{
					AppUtils.setSawReminderDialogDetails(true);
					
					AlertDialog dialog = AppUtils.createRemindMeDialog(getActivity(), getString(R.string.is_now_good_time_update_personal_details), null, new DialogInterface.OnClickListener()
					{
						
						@Override
						public void onClick(final DialogInterface dialog, final int which)
						{
							Bundle bundle = new Bundle();
							bundle.putSerializable(IntentExtras.IS_REGISTRATION, true);
							tryGoToScreenById(eInAppNavigation.UserInfo.getNavigationId(), bundle);
						}
					}, new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(final DialogInterface dialog, final int which)
						{
							ElalPreferenceUtils.setSharedPreference(getActivity(), AppUtils.getRemindMeDetailsKey(UserData.getInstance().getUserID()), true);
							
						}
					}, new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(final DialogInterface dialog, final int which)
						{
							ElalPreferenceUtils.setSharedPreference(getActivity(), AppUtils.getRemindMeDetailsKey(UserData.getInstance().getUserID()), false);
							
						}
					});
					
					dialog.setCancelable(false);
					dialog.show();
				}
			}
			else if (ElalPreferenceUtils.getSharedPreferenceBooleanOrFalse(getActivity(), AppUtils.getRemindMeFamilyKey(UserData.getInstance().getUserID())))
			{
				if (!AppUtils.isSawReminderDialogFamily())
				{
					AppUtils.setSawReminderDialogFamily(true);
					
					AlertDialog dialog = AppUtils.createRemindMeDialog(getActivity(), getString(R.string.is_now_good_time_update_family_members), null, new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(final DialogInterface dialog, final int which)
						{
							tryGoToFragmentByFragmentClass(UserFamilyFragment.class, null, false);
						}
					}, new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(final DialogInterface dialog, final int which)
						{
							ElalPreferenceUtils.setSharedPreference(getContext(), AppUtils.getRemindMeFamilyKey(UserData.getInstance().getUserID()), true);
							
						}
					}, new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(final DialogInterface dialog, final int which)
						{
							ElalPreferenceUtils.setSharedPreference(getContext(), AppUtils.getRemindMeFamilyKey(UserData.getInstance().getUserID()), false);
							
						}
					});
					dialog.setCancelable(false);
					dialog.show();
				}
			}
		}
	}
	
	private void goToRegisterFragment()
	{
		//		tryGoToFragmentByFragmentClass(RegistrationFragment.class, null, true);
		
		tryGoToFragmentByFragmentClass(eInAppNavigation.Registration.getNavigationFragmentClass(), null, true);
		//		tryGoToScreenById(eInAppNavigation.Registration.getNavigationId(), null);
	}
	
	private void onTopClick()
	{
		getUserActivePnrsAndGoToFlightManagement();
		((EWBaseActivity) getActivity()).pushEvent(ePushEvents.MANAGE_FLIGHTS_AND_CHECK_IN);
	}
	
	public void getUserActivePnrsAndGoToFlightManagement()
	{
		tryGoToScreenById(eInAppNavigation.MyFlights.getNavigationId(), Bundle.EMPTY);
	}
	
	private void onMiddleLeftClick()
	{
		if (mHomeItems != null && mHomeItems.getMiddle1Item() != null)
		{
			onHomeScreenItemClick(mHomeItems.getMiddle1Item());
		}
		
		((EWBaseActivity) getActivity()).pushEvent(ePushEvents.MIDDLE_ONE);
	}
	
	private void onMiddleRightClick()
	{
		if (mHomeItems != null && mHomeItems.getMiddle2Item() != null)
		{
			onHomeScreenItemClick(mHomeItems.getMiddle2Item());
		}
		
		((EWBaseActivity) getActivity()).pushEvent(ePushEvents.MIDDLE_TWO);
	}
	
	private void onBottomClick()
	{
		if (mHomeItems != null && mHomeItems.getBottomItem() != null)
		{
			onHomeScreenItemClick(mHomeItems.getBottomItem());
		}
	}
	
	private void onHomeScreenItemClick(final HomeScreenItem iHomeScreenItem)
	{
		if (iHomeScreenItem != null)
		{
			if (iHomeScreenItem.isInApp())
			{
				tryGoToScreenById(iHomeScreenItem.getLinkInt(), null);
			}
			else
			{
				tryGoToWebFragment(iHomeScreenItem.getLink());
			}
		}
	}
	
	
	@Override
	public void onServiceConnected()
	{
		try
		{
			//TODO Avishay 19/11/17 check it! if userId is not valid, like <= 0, then perform logout. else do getHomeScreenContent
			getService().getController(MainController.class).getHomeScreenContent(this, this);
		}
		catch (Exception iE)
		{
			iE.printStackTrace();
		}
	}
	
	
	@Override
	public void onResponse(final ResponseGetHomeScreenContent iResponse)
	{
		trySetProgressDialog(false);
		
		if (iResponse != null && iResponse.getContent() != null)
		{
			AppData.getInstance().setHomeItems(iResponse.getContent());
			
			mHomeItems = iResponse.getContent();
		}
		
		setButtonsContent();
		//		setGui();
	}
	
	private void setButtonsContent()
	{
		mHomeItems = AppData.getInstance().getHomeItems();
		if (mHomeItems != null /*&& isServiceConnected()*/)
		{
			if (mHomeItems.getTop1Item() != null)
			{
				//				mIvTopButton.setImageUrl(RequestStringBuilder.getHomeContentImageUrl(mHomeItems.getTop1Item().getIcon()), getService().getImageLoader());
			}
			else
			{
				mIvTopButton.setImageResource(R.color.transparent);
			}
			
			if (mHomeItems.getMiddle1Item() != null)
			{
				//				mIvMiddleLeftButton.setImageUrl(RequestStringBuilder.getHomeContentImageUrl(mHomeItems.getMiddle1Item().getIcon()), getService().getImageLoader());
			}
			else
			{
				mIvMiddleLeftButton.setImageResource(R.color.transparent);
			}
			
			if (mHomeItems.getTop2Item() != null)
			{
				//				mIvMiddleRightButton.setImageUrl(RequestStringBuilder.getHomeContentImageUrl(mHomeItems.getRightItem().getIcon()), getService().getImageLoader());
			}
			else
			{
				mIvMiddleRightButton.setImageResource(R.color.transparent);
			}
			
			if (mHomeItems.getBottomItem() != null)
			{
				//				mIvBottomButton.setImageUrl(RequestStringBuilder.getHomeContentImageUrl(mHomeItems.getBottomItem().getIcon()), getService().getImageLoader());
			}
			else
			{
				mIvBottomButton.setImageResource(R.color.transparent);
			}
		}
		else
		{
			mIvTopButton.setImageResource(R.color.transparent);
			mIvMiddleLeftButton.setImageResource(R.color.transparent);
			mIvMiddleRightButton.setImageResource(R.color.transparent);
			mIvBottomButton.setImageResource(R.color.transparent);
		}
		
		//		mTvTopButton.setText(mHomeItems != null && mHomeItems.getTop1Item() != null ? mHomeItems.getTop1Item().getHomeContentName() : "");
		//		mTvTopRightButton.setText(mHomeItems != null && mHomeItems.getTop2Item() != null ? mHomeItems.getTop2Item().getHomeContentName() : "");
		//		mTvMiddleLeftButton.setText(mHomeItems != null && mHomeItems.getMiddle1Item() != null ? mHomeItems.getMiddle1Item().getHomeContentName() : "");
		//		mTvMiddleRightButton.setText(mHomeItems != null && mHomeItems.getMiddle2Item() != null ? mHomeItems.getMiddle2Item().getHomeContentName() : "");
		//		mTvBottomButton.setText(mHomeItems != null && mHomeItems.getBottomItem() != null ? mHomeItems.getBottomItem().getHomeContentName() : "");
		
		if (mHomeItems != null)
		{
			if (mHomeItems.getTop1Item() != null && mHomeItems.getTop1Item().getHomeContentName() != null)
			{
				mTvTopButton.setText(mHomeItems.getTop1Item().getHomeContentName());
				mTvTopButton.setContentDescription(mHomeItems.getTop1Item().getHomeContentName());
			}
			if (mHomeItems.getTop2Item() != null && mHomeItems.getTop2Item().getHomeContentName() != null)
			{
				mTvTopRightButton.setText(mHomeItems.getTop2Item().getHomeContentName());
				mTvTopRightButton.setContentDescription(mHomeItems.getTop2Item().getHomeContentName());
			}
			if (mHomeItems.getMiddle1Item() != null && mHomeItems.getMiddle1Item().getHomeContentName() != null)
			{
				mTvMiddleLeftButton.setText(mHomeItems.getMiddle1Item().getHomeContentName());
				mTvMiddleLeftButton.setContentDescription(mHomeItems.getMiddle1Item().getHomeContentName());
			}
			if (mHomeItems.getMiddle2Item() != null && mHomeItems.getMiddle2Item().getHomeContentName() != null)
			{
				mTvMiddleRightButton.setText(mHomeItems.getMiddle2Item().getHomeContentName());
				mTvMiddleRightButton.setContentDescription(mHomeItems.getMiddle2Item().getHomeContentName());
			}
			if (mHomeItems.getBottomItem() != null && mHomeItems.getBottomItem().getHomeContentName() != null)
			{
				mTvBottomButton.setText(mHomeItems.getBottomItem().getHomeContentName());
				mTvBottomButton.setContentDescription(mHomeItems.getBottomItem().getHomeContentName());
			}
		}
		
		mTvTopBadge.setVisibility(View.GONE);
		setPnrBadge();
	}
	
	private void setGui()
	{
		if (mHomeItems != null && isServiceConnected())
		{
			if (mHomeItems.getTop1Item() != null)
			{
				//				mIvTopButton.setImageUrl(RequestStringBuilder.getHomeContentImageUrl(mHomeItems.getTop1Item().getIcon()), getService().getImageLoader());
			}
			else
			{
				mIvTopButton.setImageResource(R.color.transparent);
			}
			
			if (mHomeItems.getMiddle1Item() != null)
			{
				//				mIvMiddleLeftButton.setImageUrl(RequestStringBuilder.getHomeContentImageUrl(mHomeItems.getMiddle1Item().getIcon()), getService().getImageLoader());
			}
			else
			{
				mIvMiddleLeftButton.setImageResource(R.color.transparent);
			}
			
			if (mHomeItems.getTop2Item() != null)
			{
				//				mIvMiddleRightButton.setImageUrl(RequestStringBuilder.getHomeContentImageUrl(mHomeItems.getRightItem().getIcon()), getService().getImageLoader());
			}
			else
			{
				mIvMiddleRightButton.setImageResource(R.color.transparent);
			}
			
			if (mHomeItems.getBottomItem() != null)
			{
				//				mIvBottomButton.setImageUrl(RequestStringBuilder.getHomeContentImageUrl(mHomeItems.getBottomItem().getIcon()), getService().getImageLoader());
			}
			else
			{
				mIvBottomButton.setImageResource(R.color.transparent);
			}
		}
		else
		{
			mIvTopButton.setImageResource(R.color.transparent);
			mIvMiddleLeftButton.setImageResource(R.color.transparent);
			mIvMiddleRightButton.setImageResource(R.color.transparent);
			mIvBottomButton.setImageResource(R.color.transparent);
		}
		
		mTvTopButton.setText(mHomeItems != null && mHomeItems.getTop1Item() != null ? mHomeItems.getTop1Item().getHomeContentName() : "");
		mTvTopRightButton.setText(mHomeItems != null && mHomeItems.getTop2Item() != null ? mHomeItems.getTop2Item().getHomeContentName() : "");
		mTvMiddleLeftButton.setText(mHomeItems != null && mHomeItems.getMiddle1Item() != null ? mHomeItems.getMiddle1Item().getHomeContentName() : "");
		mTvMiddleRightButton.setText(mHomeItems != null && mHomeItems.getMiddle2Item() != null ? mHomeItems.getMiddle2Item().getHomeContentName() : "");
		mTvBottomButton.setText(mHomeItems != null && mHomeItems.getBottomItem() != null ? mHomeItems.getBottomItem().getHomeContentName() : "");
		
		mTvTopBadge.setVisibility(View.GONE);
		setPnrBadge();
	}
	
	private void setUserData()
	{
		if (!UserData.getInstance().isUserGuest() && UserData.getInstance().getUserObject() != null)
		{
			setUserName();
			setMatmidBar();
		}
		else
		{
			setGuestView();
		}
		
	}
	
	
	private void setGuestView()
	{
		mLlGuestBar.setVisibility(View.VISIBLE);
		mTvUsername.setVisibility(View.GONE);
		mLlMatmidBar.setVisibility(View.GONE);
	}
	
	private static final int MATMID_BAR_LAYOUT_DURATION = 750;
	
	private void setMatmidBar()
	{
		
		if (UserData.getInstance().getUserObject().isMatmid())
		{
			mLlMatmidBar.setVisibility(View.VISIBLE);
			mTvMatmidPoints.setText(String.valueOf(UserData.getInstance().getUserObject().getMatmidPoints()));
			mIvMatmidLogo.setImageResource(MatmidLogin.eMatmidType.getIconResourceIdByMatmidTypeId(UserData.getInstance().getUserObject().getMatmidTypeID()));
			
			ExpandCollapseAnimationRelativeLayout.expand(mLlMatmidBar, MATMID_BAR_LAYOUT_DURATION);//work good!!
		}
		else
		{
			mLlMatmidBar.setVisibility(View.GONE);
		}
		
		
		//		/***** avishay test only****/
		//		mLlMatmidBar.setVisibility(View.VISIBLE);
		//		ExpandCollapseAnimationRelativeLayout.expand(mLlMatmidBar, MATMID_BAR_LAYOUT_DURATION);//work good!!
		//		/*****/
		
	}
	
	
	private void setUserName()
	{
		mLlGuestBar.setVisibility(View.GONE);
		
		
		String username = "";
		if (UserData.getInstance().getLanguage() == eLanguage.Hebrew)
		{
			if (!TextUtils.isEmpty(UserData.getInstance().getUserObject().getFirstNameHeb()))
			{
				username = UserData.getInstance().getUserObject().getFirstNameHeb();
			}
			else
			{
				username = UserData.getInstance().getUserObject().getFirstNameEng();
			}
		}
		else
		{
			username = UserData.getInstance().getUserObject().getFirstNameEng();
		}
		if (!TextUtils.isEmpty(username))
		{
			mTvUsername.setVisibility(View.VISIBLE);
			mTvUsername.animateText(getResources().getString(R.string.hello_username, username));
		}
		else
		{
			mTvUsername.setVisibility(View.GONE);
		}
	}
	
	private void setPnrBadge()
	{
		int badgeCount = mHomeItems != null ? mHomeItems.getPNRCount() : -1;
		
		mTvTopBadge.setText(badgeCount > 0 ? String.valueOf(badgeCount) : "");
		mTvTopBadge.setVisibility(badgeCount > 0 ? View.VISIBLE : View.GONE);
	}
	
	@Override
	public void onErrorResponse(final VolleyError iError)
	{
		 
			trySetProgressDialog(false);
		ErrorsHandler.tryShowServiceErrorDialog(iError, getActivity());
	}
	
	//Just for test push
//	private void sentTestPush()
//	{
//		if (!GlobalVariables.isDebugVersion)
//		{
//			return;
//		}
//
//		new Handler().postDelayed(new Runnable()
//		{
//			@Override
//			public void run()
//			{
//				AppUtils.printLog(Log.ERROR, TAG, "Internal test from device");
//
//				eInAppNavigation inAppNavigation = eInAppNavigation.FlightSearch;
//				Intent intent = AppUtils.getSplashIntent(getContext());
//				intent.putExtra(IntentExtras.IN_APP_NAVIGATION_ID, inAppNavigation.getNavigationId());
//				intent.putExtra(IntentExtras.IS_FROM_PUSH_NOTIFICATION, true);
//
//				PushObject pushObject = new PushObject("Internal test from device");
//
//				final int NOTIFICATION_ID_MIN = 100000;
//				final int NOTIFICATION_ID_MAX = Integer.MAX_VALUE;
//
//				AppUtils.sendSimpleNotification(
//						getContext(),
//						getString(R.string.app_name),
//						pushObject.getMessageText(),
//						pushObject.getMessageText(),
//						0, intent, AppUtils.generateRandomNumberInRange
//								(NOTIFICATION_ID_MIN, NOTIFICATION_ID_MAX));
//			}
//		}, 3000);
//	}
}
