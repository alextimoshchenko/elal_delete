package ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

import il.co.ewave.elal.R;
import utils.global.CustomImageLoader;
import webServices.global.RequestStringBuilder;
import webServices.responses.getWeather.ForecastItem;
import webServices.responses.getWeather.Weather;

/**
 * Created with care by Shahar Ben-Moshe on 28/02/17.
 */

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.ViewHolder>
{
	private CustomImageLoader mImageLoader;
	private Weather mWeather;
	private Context mContext;
	
	public WeatherAdapter(@NonNull final CustomImageLoader iImageLoader, final Weather iWeather, Context iContext)
	{
		mImageLoader = iImageLoader;
		mWeather = iWeather;
		mContext = iContext;
	}
	
	@Override
	public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType)
	{
		return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_layout_cell_new, parent, false));
	}
	
	@Override
	public void onBindViewHolder(final ViewHolder iViewHolder, final int iPosition)
	{
		ForecastItem currentItem = mWeather.getForecastItems().get(iPosition);
		
		if (currentItem != null)
		{
			iViewHolder.mNivWImage.setImageUrl(RequestStringBuilder.getWeatherImageUrl(currentItem.getImage()), mImageLoader);
			
//			OkHttpClient client = new OkHttpClient.Builder()
//					.addInterceptor(new Interceptor() {
//						@Override
//						public Response intercept(Chain chain) throws IOException
//						{
//							Request newRequest = chain.request().newBuilder()
//													  .addHeader(JacksonRequest.USER_AGENT, JacksonRequest.USER_AGENT_CONTENT)
//													  .build();
//							return chain.proceed(newRequest);
//						}
//					})
//					.build();
//
//			Picasso picasso = new Picasso.Builder(mContext)
//					.downloader(new OkHttp3Downloader(client))
//					.build();
//
//			final String imgUrl = RequestStringBuilder.getWeatherImageUrl(currentItem.getImage());
//			if (imgUrl.contains("http") || imgUrl.contains("https"))
//			{
//				picasso.load(imgUrl)
//					   .into(iViewHolder.mNivWImage, new Callback()
//					   {
//						   @Override
//						   public void onSuccess()
//						   {
//							   iViewHolder.mNivWImage.setVisibility(View.VISIBLE);
//						   }
//
//						   @Override
//						   public void onError()
//						   {
//							   Log.e("picasso", "error loading " + imgUrl);
////							   iViewHolder.mNivWImage.setVisibility(View.GONE);
//						   }
//					   });
//			}
			
			iViewHolder.mTvWTitle.setText(currentItem.getDay());
//			iViewHolder.mTvWCurrentTemperature.setText(String.format("%s - %s", currentItem.getMinTempC(), currentItem.getMaxTempC()));
//			iViewHolder.mTvWCurrentTemperatureAlternative.setText(String.format("%s - %s F", currentItem.getMinTempF(), currentItem.getMaxTempF()));
			iViewHolder.mTvWCurrentTemperature.setText(currentItem.getMaxTempC() + "°");
			iViewHolder.mTvWCurrentTemperatureAlternative.setText(currentItem.getMaxTempF() + "°F");
			iViewHolder.mTvDescription.setText(currentItem.getDescription());
		}
	}
	
	@Override
	public int getItemCount()
	{
		return mWeather == null || mWeather.getForecastItems() == null ? 0 : mWeather.getForecastItems().size();
	}
	
	class ViewHolder extends RecyclerView.ViewHolder
	{
		TextView mTvWTitle;
		NetworkImageView mNivWImage;
		TextView mTvWCurrentTemperature;
		TextView mTvWCurrentTemperatureAlternative;
		TextView mTvDescription;
		
		ViewHolder(final View itemView)
		{
			super(itemView);
			
			mTvWTitle = (TextView) itemView.findViewById(R.id.tv_WeatherCell_Title);
			mNivWImage = (NetworkImageView) itemView.findViewById(R.id.niv_WeatherCell_Image);
			mTvWCurrentTemperature = (TextView) itemView.findViewById(R.id.tv_WeatherCell_CurrentTemperature);
			mTvWCurrentTemperatureAlternative = (TextView) itemView.findViewById(R.id.tv_WeatherCell_CurrentTemperatureAlternative);
			mTvDescription = (TextView) itemView.findViewById(R.id.tv_WeatherCell_Description);
		}
	}
}
