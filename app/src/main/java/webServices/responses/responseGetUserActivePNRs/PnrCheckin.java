package webServices.responses.responseGetUserActivePNRs;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Avishay.Peretz on 22/06/2017.
 */
public class PnrCheckin implements Parcelable
{
	
	@JsonProperty("PNR")
	private String mPNR;
	@JsonProperty("TicketNumber")
	private String mTicketNumber;
	@JsonProperty("FlightNumber")
	private String mFlightNumber;
	@JsonProperty("HasCheckin")
	private boolean mHasCheckin;
//	@JsonProperty("CheckInEligible")
//	private boolean mCheckInEligible;
	@JsonProperty("isCheckinButtonValid")
	private boolean mCheckInEligible;
	
	@JsonProperty("CategoryId")
	private int mCategoryId;
	@JsonProperty("SubText")
	private String mSubText;
	@JsonProperty("Icon")
	private String mIcon;
	@JsonProperty("URL")
	private String mURL;
	
	
	public PnrCheckin()
	{
	}
	
	
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeString(this.mPNR);
		dest.writeString(this.mTicketNumber);
		dest.writeString(this.mFlightNumber);
		dest.writeByte(this.mHasCheckin ? (byte) 1 : (byte) 0);
		dest.writeByte(this.mCheckInEligible ? (byte) 1 : (byte) 0);
		dest.writeInt(this.mCategoryId);
		dest.writeString(this.mSubText);
		dest.writeString(this.mIcon);
		dest.writeString(this.mURL);
	}
	
	protected PnrCheckin(Parcel in)
	{
		this.mPNR = in.readString();
		this.mTicketNumber = in.readString();
		this.mFlightNumber = in.readString();
		this.mHasCheckin = in.readByte() != 0;
		this.mCheckInEligible = in.readByte() != 0;
		this.mCategoryId = in.readInt();
		this.mSubText = in.readString();
		this.mIcon = in.readString();
		this.mURL = in.readString();
	}
	
	public static final Parcelable.Creator<PnrCheckin> CREATOR = new Parcelable.Creator<PnrCheckin>()
	{
		@Override
		public PnrCheckin createFromParcel(Parcel source)
		{
			return new PnrCheckin(source);
		}
		
		@Override
		public PnrCheckin[] newArray(int size)
		{
			return new PnrCheckin[size];
		}
	};
	
	public String getPNR()
	{
		return mPNR;
	}
	
	public String getTicketNumber()
	{
		return mTicketNumber;
	}
	
	public String getFlightNumber()
	{
		return mFlightNumber;
	}
	
	public boolean isHasCheckin()
	{
		return mHasCheckin;
	}
	
	public boolean isCheckInEligible()
	{
		return mCheckInEligible;
	}
	
	public int getCategoryId()
	{
		return mCategoryId;
	}
	
	public String getSubText()
	{
		return mSubText;
	}
	
	public String getIcon()
	{
		return mIcon;
	}
	
	public String getURL()
	{
		return mURL;
	}
	
	public static Creator<PnrCheckin> getCREATOR()
	{
		return CREATOR;
	}
}
