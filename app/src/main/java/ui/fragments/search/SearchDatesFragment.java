package ui.fragments.search;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

import global.UserData;
import global.eLanguage;
import il.co.ewave.elal.R;
import ui.customWidgets.materialcalendarview.CalendarDay;
import ui.customWidgets.materialcalendarview.CalendarMode;
import ui.customWidgets.materialcalendarview.DayViewDecorator;
import ui.customWidgets.materialcalendarview.DayViewFacade;
import ui.customWidgets.materialcalendarview.MaterialCalendarView;
import ui.customWidgets.materialcalendarview.OnDateSelectedListener;
import ui.customWidgets.materialcalendarview.OnMonthChangedListener;
import ui.customWidgets.materialcalendarview.OnRangeSelectedListener;
import ui.customWidgets.materialcalendarview.format.ArrayWeekDayFormatter;
import ui.fragments.EWBaseFragment;
import utils.errors.ErrorsHandler;
import utils.errors.LocalError;
import utils.global.AppUtils;
import utils.global.DateTimeUtils;
import utils.global.enums.eEditAction;
import utils.global.enums.eTypesOfTrip;
import utils.global.searchFlightObjects.SearchObject;

/**
 * Created with care by Alexey.T on 10/07/2017.
 */

public class SearchDatesFragment extends EWBaseFragment implements Response.ErrorListener, OnRangeSelectedListener, OnDateSelectedListener, OnMonthChangedListener
{
	private final String TAG = SearchDatesFragment.class.getSimpleName();
	
	private MaterialCalendarView mCalendarView;
	private ImageView mIvBackArrow, mIvLeftArrow, mIvRightArrow;
	private TextView mTvTitle, mTvSearchDatesRange, mTvMonth;
	private Button mBtnSearchDatesOk;
	private Calendar mMaxCalendarDate = Calendar.getInstance();
	private Calendar mMinCalendarDate = Calendar.getInstance();
	private CalendarDay mCurrentCalendarDayDate = CalendarDay.today();
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fargment_search_dates_layout);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		initReference();
		setListeners();
		setMaxMinCalendarDates();
		setMaterialCalendarSetting();
		setFocusOnSpecificDateIfNeed();
		setGui();
	}
	
	private void initReference()
	{
		Activity act = getActivity();
		mBtnSearchDatesOk = (Button) act.findViewById(R.id.btn_search_dates_ok);
		mCalendarView = (MaterialCalendarView) act.findViewById(R.id.cv_frag_search_dates);
		mTvSearchDatesRange = (TextView) act.findViewById(R.id.tv_search_dates_range);
		
		//header
		View searchDatesHeader = act.findViewById(R.id.cl_search_dates_header);
		mIvBackArrow = (ImageView) searchDatesHeader.findViewById(R.id.iv_name_screen_back_arrow);
		mTvTitle = (TextView) searchDatesHeader.findViewById(R.id.tv_name_screen_title);
		
		//header of calendar
		mIvLeftArrow = (ImageView) act.findViewById(R.id.iv_search_dates_left_arrow);
		mIvRightArrow = (ImageView) act.findViewById(R.id.iv_search_dates_right_arrow);
		mTvMonth = (TextView) act.findViewById(R.id.tv_search_dates_month);
	}
	
	private void setListeners()
	{
		mIvLeftArrow.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnLeftArrowClick();
			}
		});
		
		mIvRightArrow.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnRightArrowClick();
			}
		});
		
		mBtnSearchDatesOk.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnOkClick();
			}
		});
		
		mIvBackArrow.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnBackClick();
			}
		});
		
		mCalendarView.setOnRangeSelectedListener(this);
		
		mCalendarView.setOnDateChangedListener(this);
		
		mCalendarView.setOnTitleClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				AppUtils.printLog(Log.DEBUG, TAG, "setOnTitleClickListener");
			}
		});
		
		mCalendarView.setOnMonthChangedListener(this);
	}
	
	private void setMaxMinCalendarDates()
	{
		mMaxCalendarDate.add(Calendar.YEAR, 1);
		mMinCalendarDate = CalendarDay.today().getCalendar();
	}
	
	private void setMaterialCalendarSetting()
	{
		int calendarMode;
		
		if (SearchObject.getInstance().getCurrentTypeOfTrip().isOneWayTicket())
		{
			calendarMode = MaterialCalendarView.SELECTION_MODE_SINGLE;
		}
		else
		{
			calendarMode = MaterialCalendarView.SELECTION_MODE_RANGE;
		}
		
		if (UserData.getInstance().getLanguage().isHebrew())
		{
			mCalendarView.setWeekDayFormatter(new ArrayWeekDayFormatter(getResources().getTextArray(R.array.custom_weekdays)));
		}
		
		mCalendarView.setSelectionMode(calendarMode);
		mCalendarView.setTopbarVisible(false);
		mCalendarView.setSelectionColor(Color.parseColor("#ffffff"));
		mCalendarView.setShowOtherDates(MaterialCalendarView.SHOW_ALL);
		mCalendarView.state()
		             .edit()
		             .setCalendarDisplayMode(CalendarMode.MONTHS)
		             .setFirstDayOfWeek(getFirstDayOfTheWeekByLanguage())
		             .setMinimumDate(mMinCalendarDate)
		             .setMaximumDate(mMaxCalendarDate)
		             .commit();
	}
	
	private void setFocusOnSpecificDateIfNeed()
	{
		SearchObject searchObject = SearchObject.getInstance();
		eTypesOfTrip currentTypeOfTrip = searchObject.getCurrentTypeOfTrip();
		
		Date currentFirstDateInSelection = searchObject.getCurrentFirstDateInSelection();
		Date currentLastDateInSelection = searchObject.getCurrentLastDateInSelection();
		
		if (currentTypeOfTrip.isRoundTrip() || currentTypeOfTrip.isReturnFromAnotherDestination())
		{
			if (currentFirstDateInSelection != null && currentLastDateInSelection != null)
			{
				mCalendarView.selectRange(CalendarDay.from(currentFirstDateInSelection), CalendarDay.from(currentLastDateInSelection));
				mCalendarView.setCurrentDate(CalendarDay.from(currentFirstDateInSelection), true);
			}
		}
		else if (currentTypeOfTrip.isOneWayTicket())
		{
			if (currentFirstDateInSelection != null)
			{
				mCalendarView.setSelectedDate(CalendarDay.from(currentFirstDateInSelection));
				addDecoratorForOneSelectedDate(CalendarDay.from(currentFirstDateInSelection), eRoundSide.EMPTY);
				mCalendarView.setCurrentDate(CalendarDay.from(currentFirstDateInSelection), true);
			}
		}
	}
	
	private void setGui()
	{
		DayViewDecorator oneDayDecorator = new OneDayDecorator(CalendarDay.today(), eRoundSide.EMPTY, null, new StyleSpan(Typeface.BOLD));
		mCalendarView.addDecorator(oneDayDecorator);
		mCalendarView.invalidateDecorators();
		mTvTitle.setText(R.string.select_dates);
		setTvMonth();
		setOkButtonEnable();
		setTvSearchDatesRange();
	}
	
	private void setTvMonth()
	{
		String date = DateTimeUtils.convertDateToMonthNameAndYearOrString(mCurrentCalendarDayDate.getDate());
		mTvMonth.setText(date);
	}
	
	private void setTvSearchDatesRange()
	{
		SearchObject searchObject = SearchObject.getInstance();
		
		String firstInSelection = DateTimeUtils.convertDateToSlashSeparatedDayMonthYearTwoNumbersString(searchObject.getCurrentFirstDateInSelection());
		String lastInSelection = DateTimeUtils.convertDateToSlashSeparatedDayMonthYearTwoNumbersString(searchObject.getCurrentLastDateInSelection());
		mTvSearchDatesRange.setText(firstInSelection + (searchObject.getCurrentTypeOfTrip().isOneWayTicket() ? "" : getString(R.string.dash_sing)) + lastInSelection);
	}
	
	private void doOnOkClick()
	{
		SearchObject searchObject = SearchObject.getInstance();
		searchObject.setCurrentDateSelected(true);
		
		if (searchObject.getCurrentLastDateInSelection() == null && !searchObject.getCurrentTypeOfTrip().isOneWayTicket())
		{
			searchObject.setCurrentFirstDateInSelection(null);
		}
		
		searchObject.editFamilyList(true, eEditAction.ADD_AGE);
		
		tryPopFragment();
		tryClearFragments();
	}
	
	private void doOnRightArrowClick()
	{
		if (mCalendarView == null)
		{
			return;
		}
		
		if (UserData.getInstance().getLanguage() == eLanguage.English)
		{
			if (mCalendarView.canGoForward())
			{
				mCalendarView.goToNext();
			}
		}
		else
		{
			if (mCalendarView.canGoBack())
			{
				mCalendarView.goToPrevious();
			}
		}
	}
	
	private void doOnLeftArrowClick()
	{
		if (mCalendarView == null)
		{
			return;
		}
		
		if (UserData.getInstance().getLanguage() == eLanguage.English)
		{
			mCalendarView.goToPrevious();
		}
		else
		{
			mCalendarView.goToNext();
		}
	}
	
	private void addDecoratorForSelectedDates(List<CalendarDay> mDates)
	{
		if (mCalendarView != null && mDates != null && !mDates.isEmpty())
		{
			mCalendarView.addDecorator(new EventRangeSelectionDecorator(mDates));
			mCalendarView.invalidateDecorators();
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.SomeProblemWithArray.getErrorMessage());
		}
	}
	
	private void addDecoratorForOneSelectedDate(CalendarDay mDate, eRoundSide iSide)
	{
		if (mCalendarView != null && mDate != null)
		{
			Drawable background = getResources().getDrawable(R.drawable.background_dark_blue_calendar_date_decorator);
			Object colorNumber = new ForegroundColorSpan(getResources().getColor(R.color.topaz));
			DayViewDecorator decorator = new OneDayDecorator(mDate, iSide, background, colorNumber);
			mCalendarView.addDecorator(decorator);
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.SomeProblemWithArray.getErrorMessage());
		}
	}
	
	private int getFirstDayOfTheWeekByLanguage()
	{
		int firstDayOfTheWeek;
		
		if (UserData.getInstance().getLanguage() == eLanguage.Hebrew)
		{
			firstDayOfTheWeek = Calendar.SUNDAY;
		}
		else
		{
			firstDayOfTheWeek = Calendar.MONDAY;
		}
		
		return firstDayOfTheWeek;
	}
	
	private void setOkButtonEnable()
	{
		SearchObject searchObject = SearchObject.getInstance();
		eTypesOfTrip typeOfTrip = SearchObject.getInstance().getCurrentTypeOfTrip();
		mBtnSearchDatesOk.setEnabled(false);
		
		if (typeOfTrip.isOneWayTicket() && searchObject.getCurrentFirstDateInSelection() != null)
		{
			mBtnSearchDatesOk.setEnabled(true);
		}
		else if ((typeOfTrip.isReturnFromAnotherDestination() || typeOfTrip.isRoundTrip()) && searchObject.getCurrentFirstDateInSelection() != null && searchObject.getCurrentLastDateInSelection() != null)
		{
			mBtnSearchDatesOk.setEnabled(true);
		}
	}
	
	@Override
	public void onRangeSelected(@NonNull final MaterialCalendarView widget, @NonNull final List<CalendarDay> mDates)
	{
		handleRangeSelectedClick(mDates);
		setGui();
	}
	
	private void handleRangeSelectedClick(final @NonNull List<CalendarDay> mDates)
	{
		if (mDates.isEmpty())
		{
			AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.SomeProblemWithArray.getErrorMessage());
		}
		
		removeCalendarDecorators();
		
		SearchObject searchObject = SearchObject.getInstance();
		
		CalendarDay firstDateInSelection = mDates.get(0);
		CalendarDay lastDateInSelection = mDates.get(mDates.size() - 1);
		
		mDates.remove(firstDateInSelection);
		mDates.remove(lastDateInSelection);
		
		if (UserData.getInstance().getLanguage().isEnglish())
		{
			addDecoratorForOneSelectedDate(firstDateInSelection, eRoundSide.LEFT);
			addDecoratorForOneSelectedDate(lastDateInSelection, eRoundSide.RIGHT);
		}
		else
		{
			addDecoratorForOneSelectedDate(firstDateInSelection, eRoundSide.RIGHT);
			addDecoratorForOneSelectedDate(lastDateInSelection, eRoundSide.LEFT);
		}
		
		addDecoratorForSelectedDates(mDates);
		
		searchObject.setCurrentFirstDateInSelection(firstDateInSelection.getDate());
		searchObject.setCurrentLastDateInSelection(lastDateInSelection.getDate());
	}
	
	@Override
	public void onDateSelected(@NonNull final MaterialCalendarView widget, @NonNull final CalendarDay iDate, final boolean iSelected)
	{
		handleOnDateSelected(iDate, iSelected);
		setGui();
	}
	
	private void handleOnDateSelected(final @NonNull CalendarDay iDate, final boolean iSelected)
	{
		SearchObject searchObject = SearchObject.getInstance();
		
		//it mean that user start to select dates, this flag will be changed when user click "OK" button
		searchObject.setCurrentDateSelected(false);
		
		if (iSelected)
		{
			setFirstDateWithDecorator(iDate);
		}
		else
		{
			searchObject.setCurrentFirstDateInSelection(null);
			searchObject.setCurrentLastDateInSelection(null);
			removeCalendarDecorators();
		}
	}
	
	private void setFirstDateWithDecorator(CalendarDay iCalendarDay)
	{
		SearchObject searchObject = SearchObject.getInstance();
		searchObject.setCurrentFirstDateInSelection(iCalendarDay.getDate());
		searchObject.setCurrentLastDateInSelection(null);
		removeCalendarDecorators();
		addDecoratorForOneSelectedDate(iCalendarDay, eRoundSide.EMPTY);
	}
	
	private void removeCalendarDecorators()
	{
		if (mCalendarView != null)
		{
			mCalendarView.removeDecorators();
		}
	}
	
	@Override
	public void onMonthChanged(final MaterialCalendarView widget, final CalendarDay iDate)
	{
		mCurrentCalendarDayDate = iDate;
		setTvMonth();
	}
	
	private class EventRangeSelectionDecorator implements DayViewDecorator
	{
		private HashSet<CalendarDay> mDates = new HashSet<>();
		
		EventRangeSelectionDecorator(Collection<CalendarDay> iDates)
		{
			mDates = new HashSet<>(iDates);
		}
		
		@Override
		public boolean shouldDecorate(CalendarDay day)
		{
			return mDates.contains(day);
		}
		
		@Override
		public void decorate(DayViewFacade view)
		{
			view.setSelectionDrawable(getGreyDrawable(eRoundSide.NORMAL));
		}
	}
	
	private class OneDayDecorator implements DayViewDecorator
	{
		private CalendarDay mDate;
		private Object[] mSpan;
		private Drawable mBackgroundDrawable;
		private eRoundSide mSide;
		
		OneDayDecorator(CalendarDay iDate, eRoundSide iSide, Drawable iBackgroundDrawable, @NonNull Object... iSpans)
		{
			mDate = iDate;
			mSpan = iSpans;
			mBackgroundDrawable = iBackgroundDrawable;
			mSide = iSide;
		}
		
		@Override
		public boolean shouldDecorate(CalendarDay day)
		{
			return mDate != null && day.equals(mDate);
		}
		
		@Override
		public void decorate(DayViewFacade view)
		{
			if (mSpan != null && mSpan.length > 0)
			{
				for (Object span : mSpan)
				{
					view.addSpan(span);
				}
			}
			
			if (mBackgroundDrawable != null)
			{
				view.setBackgroundDrawable(mBackgroundDrawable);
			}
			
			if (mSide != null && !mSide.isEmpty())
			{
				view.setSelectionDrawable(getGreyDrawable(mSide));
			}
		}
		
		/**
		 * We're changing the internals, so make sure to call {@linkplain MaterialCalendarView#invalidateDecorators()}
		 */
		public void setDate(CalendarDay iDate)
		{
			mDate = iDate;
		}
		
		public void setSpan(final Objects... iSpans)
		{
			mSpan = iSpans;
		}
	}
	
	private enum eRoundSide
	{
		NORMAL,
		LEFT,
		RIGHT,
		EMPTY;
		
		public boolean isNormal()
		{
			return this == NORMAL;
		}
		
		public boolean isLeft()
		{
			return this == LEFT;
		}
		
		public boolean isRight()
		{
			return this == RIGHT;
		}
		
		public boolean isEmpty()
		{
			return this == EMPTY;
		}
	}
	
	private Drawable getGreyDrawable(eRoundSide iSide)
	{
		Drawable drawable = null;
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
		{
			if (iSide.isNormal())
			{
				drawable = getResources().getDrawable(R.drawable.calendar_selection, getContext().getTheme());
			}
			else if (iSide.isLeft())
			{
				drawable = getResources().getDrawable(R.drawable.calendar_selection_rounded_left, getContext().getTheme());
			}
			else if (iSide.isRight())
			{
				drawable = getResources().getDrawable(R.drawable.calendar_selection_rounded_right, getContext().getTheme());
			}
		}
		else
		{
			if (iSide.isNormal())
			{
				drawable = getResources().getDrawable(R.drawable.calendar_selection);
			}
			else if (iSide.isLeft())
			{
				drawable = getResources().getDrawable(R.drawable.calendar_selection_rounded_left);
			}
			else if (iSide.isRight())
			{
				drawable = getResources().getDrawable(R.drawable.calendar_selection_rounded_right);
			}
		}
		
		return drawable;
	}
	
	private void doOnBackClick()
	{
		getActivity().onBackPressed();
	}
	
	@Override
	public void onErrorResponse(final VolleyError iError)
	{
		ErrorsHandler.tryShowServiceErrorDialog(iError, getActivity());
	}
}
