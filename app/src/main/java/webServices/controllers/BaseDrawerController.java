package webServices.controllers;

import com.android.volley.Request;
import com.android.volley.Response;

import webServices.global.JacksonRequest;
import webServices.global.RequestStringBuilder;
import webServices.requests.RequestLogout;
import webServices.responses.ResponseLogout;

/**
 * Created with care by Alexey.T on 16/07/2017.
 */
public class BaseDrawerController extends AController implements MizdamenInterfave
{
	public void Logout(final RequestLogout iRequest, final Response.Listener<ResponseLogout> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseLogout> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getLogoutUrl(), iRequest, ResponseLogout.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
}
