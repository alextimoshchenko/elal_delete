package webServices.global;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Header;
import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import global.ElAlApplication;
import global.UserData;
import il.co.ewave.elal.BuildConfig;
import utils.HashUtilsManager;
import utils.errors.ServerError;
import utils.global.AppUtils;
import utils.global.ElalPreferenceUtils;

/**
 * Created by erline.katz on 27/09/2017.
 */

public class JacksonMatmidLoginRequest<T> extends JsonRequest<T>
{
	private static final String TAG = JacksonRequest.class.getSimpleName();
	private static final String CONTENT = "Content-Type";
	private static final String JSON = "application/json";
	private static final String USER_AGENT = "User-Agent";
	private static final String COOKIE = "Cookie";
	private static final String SET_COOKIE = "Set-Cookie";
	private static final String SMSESSION_TITLE = "SMSESSION";
	
	private static final String PASSWORD = "Password";
	//	private static final String SESSION_ID = "SessionID";
	
	private static final int REQUEST_TIMEOUT = 40 * 1000;
	private static final int REQUEST_MAX_RETRIES = 0;
	
	private Class<T> responseType;
	private String mToken;
	private String mSession = "";
	private HashMap<String, String> mHeaders;
	private String mPassword;
	private String mUrl;
	
	/**
	 * Creates a new request.
	 *
	 * @param method        the HTTP method to use
	 * @param url           URL to fetch the JSON from
	 * @param requestData   A {@link Object} to post and convert into json as the request. Null is allowed and indicates no parameters will be posted along with request.
	 * @param listener      Listener to receive the JSON response
	 * @param errorListener Error listener, or null to ignore errors.
	 */
	public JacksonMatmidLoginRequest(int method, String url, Object requestData, String iPassword, Class<T> responseType, com.android.volley.Response.Listener<T> listener, com.android.volley.Response.ErrorListener errorListener)
	{
		super(method, url, (requestData == null) ? null : Mapper.string(requestData), listener, errorListener);
		this.responseType = responseType;
		//		this.mToken = iToken;
		this.mToken = ElalPreferenceUtils.getSharedPreferenceStringOrDefault(ElAlApplication.getInstance(), ElalPreferenceUtils.CA_TOKEN, "");
		if (BuildConfig.DEBUG)
		{
			this.mSession = UserData.getInstance().getSMSession();
		}
		this.mPassword = iPassword;
		this.mUrl = url;
		
		if (BuildConfig.DEBUG)
		{
			AppUtils.printLog(Log.DEBUG, TAG, "RequestJsonString = " + ((requestData == null) ? "" : Mapper.prettifyString(Mapper.string(requestData))));
			AppUtils.printLog(Log.DEBUG, TAG, "RequestWsUrl = " + (getUrl() == null ? "" : getUrl()));
		}
		
		CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
		
		setRetryPolicy(new DefaultRetryPolicy(REQUEST_TIMEOUT, REQUEST_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
	}
	
	@Override
	protected com.android.volley.Response<T> parseNetworkResponse(NetworkResponse response)
	{
		try
		{
			if (!mUrl.contains(RequestStringBuilder.MATMID_LOGIN))
			{
				getSmSessionFromHeaders(response.allHeaders);
			}
			
			String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
			
			if (BuildConfig.DEBUG)
			{
				AppUtils.printLog(Log.DEBUG, TAG, "ResponseJsonString = " + Mapper.prettifyString(jsonString));
				AppUtils.printLog(Log.DEBUG, TAG, "ResponseWsUrl = " + (getUrl() == null ? "" : getUrl()));
			}
			
			T responseObject = Mapper.objectOrThrow(jsonString, responseType);
			
			if (responseObject != null && responseObject instanceof webServices.global.Response)
			{
				if (((webServices.global.Response) responseObject).getErrorCode() != ServerError.eServerError.Ok.getErrorCode())
				{
					throw new ServerError((webServices.global.Response) responseObject);
				}
			}
			
			if (HashUtilsManager.getInstance().isHashService(responseType.getSimpleName()))
			{
				if (response.headers.get(JacksonRequest.EWAVE_HASH) != null)
				{
					if (!TextUtils.isEmpty(response.headers.get(JacksonRequest.EWAVE_HASH)))
					{
						if (!TextUtils.isEmpty(jsonString))
						{
							if (HashUtilsManager.getInstance().isAuthenticHash(jsonString, response.headers.get(JacksonRequest.EWAVE_HASH)))
							{
								//do nothing
								AppUtils.printLog(Log.WARN, TAG, "isAuthenticHash: **true**");
								return com.android.volley.Response.success(responseObject, HttpHeaderParser.parseCacheHeaders(response));
							}
							else
							{
								throw new ServerError((webServices.global.Response) responseObject);
							}
						}
						else
						{
							throw new ServerError((webServices.global.Response) responseObject);
						}
					}
					else
					{
						throw new ServerError((webServices.global.Response) responseObject);
					}
				}
				else
				{
					throw new ServerError((webServices.global.Response) responseObject);
				}
			}
			else
			{
				return com.android.volley.Response.success(responseObject, HttpHeaderParser.parseCacheHeaders(response));
			}
		}
		catch (Exception iE)
		{
			return com.android.volley.Response.error(iE instanceof ServerError ? (ServerError) iE : new VolleyError(iE));
		}
	}
	
	private void getSmSessionFromHeaders(List<Header> allHeaders)
	{
		Log.d(TAG, "allHeaders: " + allHeaders);
		String newSMSession = findInCookiesList(allHeaders, SMSESSION_TITLE);
		
		if (newSMSession != null && !TextUtils.isEmpty(newSMSession))
		{
			/**
			 * substring every following cookie attached in header
			 * **/
			int iend = newSMSession.indexOf(";");
			if (iend != -1)
			{
				newSMSession = newSMSession.substring(0, iend); //this will give abc
			}
			Log.d("EWBaseDrawerActivity", "new smsession: " + newSMSession);
			
			ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.SMS_SESSION, newSMSession);
			UserData.getInstance().setSMSession(newSMSession);
		}
	}
	
	private String findInCookiesList(List<Header> allHeaders, String name)
	{
		for (Header header : allHeaders)
		{
			if (header.getName().equals(SET_COOKIE) && header.getValue().contains(name))
			{
				return header.getValue().replace(name + "=", "");
			}
		}
		return null;
		
	}
	
	public void setSecretHashHeader(final byte[] iBody)
	{
		String hash = HashUtilsManager.getInstance().getSecretHashHeader(iBody);
		if (!TextUtils.isEmpty(hash))
		{
			getHeaders().put(JacksonRequest.EWAVE_HASH, hash);
		}
	}
	
	@Override
	public final Map<String, String> getHeaders()
	{
		if (mHeaders == null)
		{
			mHeaders = new HashMap<>();
		}
		
		if (mUrl.contains("MatmidLogin"))
		{
			mHeaders.put(COOKIE, RequestStringBuilder.getMatmidCookieString());
		}
		else
		{
			mHeaders.put(COOKIE, RequestStringBuilder.getMatmidCookieStringForWeb());
		}
//		mHeaders.put(CONTENT, JSON);
		mHeaders.put(JacksonRequest.USER_AGENT, JacksonRequest.USER_AGENT_CONTENT);
		if (mPassword != null)
		{
			mHeaders.put(PASSWORD, mPassword);
		}
		
		//mHeaders.put("accept-encoding", "gzip, deflate");
		
		AppUtils.printLog(Log.DEBUG, TAG, "headers: " + mHeaders.toString());
		
		return mHeaders;
	}
	
	@Override
	protected VolleyError parseNetworkError(VolleyError volleyError)
	{
		if (BuildConfig.DEBUG)
		{
			AppUtils.printLog(Log.DEBUG, TAG, "ResponseErrorCode = " + String.valueOf((volleyError != null && volleyError.networkResponse != null) ? volleyError.networkResponse.statusCode : -1));
			AppUtils.printLog(Log.DEBUG, TAG, "ResponseErrorMessage = " + ((volleyError != null && !TextUtils.isEmpty(volleyError.toString())) ? volleyError.toString() : ""));
			AppUtils.printLog(Log.DEBUG, TAG, "ResponseWsUrl = " + (getUrl() == null ? "" : getUrl()));
		}
		
		return super.parseNetworkError(volleyError);
	}
	
	public void setSecretHashHeader(eRequestItem iERequestItem, final byte[] iBody)
	{
		String hash = null;
		
		if (iERequestItem != null)
		{
			
			//			String body = HashUtilsManager.getInstance().convertByteArrayToString(iBody);
			hash = handleSpecialRequests(iBody, iERequestItem.getKeysArr());
		}
		
		if (TextUtils.isEmpty(hash))
		{
			hash = HashUtilsManager.getInstance().getSecretHashHeader(iBody);
		}
		
		if (!TextUtils.isEmpty(hash))
		{
			getHeaders().put(JacksonRequest.EWAVE_HASH, hash);
		}
	}
	
	private String handleSpecialRequests(final byte[] iBody, final String[] iKeysArr)
	{
		String body = HashUtilsManager.getInstance().convertByteArrayToString(iBody);
		JSONObject jsnBody;
		
		try
		{
			JSONObject jsonObject = new JSONObject(body);
			jsnBody = new JSONObject();
			for (String anIKeysArr : iKeysArr)
			{
				jsnBody.put(anIKeysArr, jsonObject.get(anIKeysArr));
			}
			return HashUtilsManager.getInstance().getStringHashByString(jsnBody.toString());
		}
		catch (JSONException iE)
		{
			iE.printStackTrace();
		}
		
		return "";
	}
}
