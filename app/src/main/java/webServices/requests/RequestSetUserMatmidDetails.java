package webServices.requests;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Calendar;
import java.util.Date;

import global.UserData;
import utils.global.DateTimeUtils;
import webServices.global.RequestStringBuilder;

/**
 * Created by Avishay.Peretz on 07/06/2017.
 */

//@JsonIgnoreProperties("SessionID" )
public class RequestSetUserMatmidDetails
{
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("SessionID")
	private String mSessionID;
	@JsonProperty("Email")
	private String mEmail;
	@JsonProperty("Mobile")
	private String mMobile;
	@JsonProperty("PassportNum")
	private String mPassportNum;
	@JsonProperty("CountryCode")
	private String mCountryCode;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeUtils.DATE_FORMAT_yyyy_MM_ddHHmmSS)
	@JsonProperty("PassportExp")
	private Date mPassportExpiration;
	@JsonProperty("LanguageCode")
	private String mLanguageCode = UserData.getInstance().getLanguage().getLanguageCode();
	@JsonProperty("MemberID")
	private String mMemberID;
	@JsonProperty("MobileCountryPrefix")
	private String mMobileCountryPrefix;
	
	
	public RequestSetUserMatmidDetails(String iSessionID, String iEmail, String iMobile, String iMobileCountryPrefix, String iPassportNum, Calendar iPassportExpiration, /*String iLanguageCode,*/ String iMemberID)
	{
		/**
		 * session has to be sent only in test
		 */
		
		mSessionID = iSessionID;
		mEmail = iEmail;
		mMobile = iMobile;
		mMobileCountryPrefix = iMobileCountryPrefix;
		mPassportNum = iPassportNum;
		mPassportExpiration = iPassportExpiration == null ? null : iPassportExpiration.getTime();
		mMemberID = iMemberID;
	}
	
}
