package ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tagmanager.DataLayer;

import java.util.ArrayList;

import global.IntentExtras;
import global.UserData;
import il.co.ewave.elal.R;
import interfaces.IDocumentsForTypeClickListener;
import ui.activities.EWBaseActivity;
import ui.activities.EWBaseDrawerActivity;
import ui.adapters.DocumentsForTypeAdapter;
import utils.global.AppUtils;
import utils.global.dbObjects.Document;
import utils.global.googleAnalitics.googleTagManager.TagManagerImplementation;
import utils.managers.RealmManager;
import utils.managers.TabsBackStackManager;
import webServices.responses.responseGetUserActivePNRs.UserActivePnr;

//import ui.customWidgets.gestureImageView.GestureImageView;

/**
 * Created with care by Shahar Ben-Moshe on 06/03/17.
 */

public class DocumentsForTypeFragment extends EWBaseFragment implements IDocumentsForTypeClickListener, RealmManager.IRealmListener<Document>
{
	private TextView mTvDocumentsType;
	private RecyclerView mRvDocuments;
	private Button mBtnAddNewDocument;
	private UserActivePnr mUserActivePnr;
	private int mDocumentTypeId;
	private boolean mShouldBeAbleToAdd;
	private DocumentsForTypeAdapter mAdapter;
	private ArrayList<Document> mDocuments;
	private ImageView mBtnClose;
	private ImageView mBtnCloseZoomable;
	private FrameLayout mFlZoomableContainer;
	private ImageView mImgZoomable;
	
	public static Bundle createBundleForFragment(final UserActivePnr iUserActivePnr, final int iDocumentTypeId, final boolean iShouldBeAbleToAdd)
	{
		Bundle result = new Bundle();
		
		result.putParcelable(IntentExtras.USER_ACTIVE_PNR, iUserActivePnr);
		result.putInt(IntentExtras.DOCUMENT_TYPE, iDocumentTypeId);
		result.putBoolean(IntentExtras.SHOULD_BE_ABLE_TO_ADD, iShouldBeAbleToAdd);
		
		return result;
	}
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_documents_for_type_layout);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		getIntentExtras();
		initReference();
		setListeners();
		setGui();
		loadDataAndSetAdapter();
	}
	
	private void getIntentExtras()
	{
		if (getArguments() != null)
		{
			mUserActivePnr = getArguments().getParcelable(IntentExtras.USER_ACTIVE_PNR);
			mDocumentTypeId = getArguments().getInt(IntentExtras.DOCUMENT_TYPE);
			mShouldBeAbleToAdd = getArguments().getBoolean(IntentExtras.SHOULD_BE_ABLE_TO_ADD, false);
		}
	}
	
	private void initReference()
	{
		mTvDocumentsType = (TextView) getActivity().findViewById(R.id.tv_documentsForType_DocumentsType);
		mRvDocuments = (RecyclerView) getActivity().findViewById(R.id.rv_documentsForType_Documents);
		mBtnAddNewDocument = (Button) getActivity().findViewById(R.id.btn_documentsForType_AddNewDocument);
		mBtnClose = (ImageView) getActivity().findViewById(R.id.iv_documentForType_Close);
		
		mBtnCloseZoomable = (ImageView) getActivity().findViewById(R.id.btnHideEnalrgedImage);
		mImgZoomable = (ImageView) getActivity().findViewById(R.id.img_zoomable);
		mFlZoomableContainer = (FrameLayout) getActivity().findViewById(R.id.fl_zoomable_container);
		
	}
	
	private void setListeners()
	{
		mBtnAddNewDocument.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				
				if (UserData.getInstance().isUserGuest())
				{
					((EWBaseDrawerActivity) getActivity()).openAskLoginDialog();
				}
				else
				{
					goToAddDocument(mUserActivePnr, mDocumentTypeId);
					
					//event 57
					String activePnr = UserData.getInstance().getActivePnr();
					((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight" + TagManagerImplementation.TIME_SCOPE, "Action", TagManagerImplementation.TIME_SCOPE + activePnr + " >> Save", "Label", "Add a document"));
				}
			}
		});
		mBtnClose.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				//				tryPopFragment();
				goToMyDocsFragment();
				
				//event 56
				String activePnr = UserData.getInstance().getActivePnr();
				((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight" + TagManagerImplementation.TIME_SCOPE, "Action", TagManagerImplementation.TIME_SCOPE + activePnr + " >> Save", "Label", "All travel " +
						"documents"));
			}
		});
	}
	
	private void goToMyDocsFragment()
	{
		Bundle bundle = MyDocumentsFragment.createBundleForFragment(mUserActivePnr, true, mUserActivePnr.getFlightsCount());
		
		if (getParentFragment() != null && getParentFragment() instanceof TabsBackStackManager.ITabsBackStackManagerHandler)
		{
			MyDocumentsFragment myDocumentsFragment = new MyDocumentsFragment();
			myDocumentsFragment.setArguments(bundle);
			
			((TabsBackStackManager.ITabsBackStackManagerHandler) getParentFragment()).pushFragmentToCurrentTab(myDocumentsFragment, true, true);
		}
		else
		{
			tryGoToFragmentByFragmentClass(MyDocumentsFragment.class, bundle, true);
		}
	}
	
	private void setGui()
	{
		mBtnAddNewDocument.setVisibility(mShouldBeAbleToAdd ? View.VISIBLE : View.GONE);
		
		Document.eDocumentType documentType = Document.eDocumentType.getDocumentTypeById(mDocumentTypeId);
		mTvDocumentsType.setText(documentType == null ? "" : getString(documentType.getNameResourceId()));
	}
	
	private void loadDataAndSetAdapter()
	{
		String pnr = mUserActivePnr != null ? AppUtils.nullSafeCheckString(mUserActivePnr.getPnr()) : null;
		Document.eDocumentType documentType = Document.eDocumentType.getDocumentTypeById(mDocumentTypeId);
		
		if (!TextUtils.isEmpty(pnr) && documentType != null)
		{
			RealmManager.getInstance().getDocumentsForCategory(pnr, UserData.getInstance().getUserID(), documentType, this);
		}
	}
	
	private void setAdapter()
	{
		setAdapter(RecyclerView.NO_POSITION);
	}
	
	private void setAdapter(final int iPosition)
	{
		if (mAdapter == null)
		{
			mAdapter = new DocumentsForTypeAdapter(mDocuments, this);
			mRvDocuments.setLayoutManager(new LinearLayoutManager(getActivity()));
			mRvDocuments.setAdapter(mAdapter);
		}
		if (iPosition > RecyclerView.NO_POSITION)
		{
			mAdapter.notifyItemRemoved(iPosition);
			mAdapter.notifyItemRangeChanged(iPosition, mDocuments.size());
		}
		else
		{
			mAdapter.notifyDataSetChanged();
		}
	}
	
	private void goToAddDocument(final UserActivePnr iUserActivePnr, final int iDocumentTypeId)
	{
		Bundle bundle = AddNewDocumentFragment.createBundleForFragment(iUserActivePnr, null, iDocumentTypeId, true);
		
		if (getParentFragment() != null && getParentFragment() instanceof TabsBackStackManager.ITabsBackStackManagerHandler)
		{
			AddNewDocumentFragment addNewDocumentFragment = new AddNewDocumentFragment();
			addNewDocumentFragment.setArguments(bundle);
			
			((TabsBackStackManager.ITabsBackStackManagerHandler) getParentFragment()).pushFragmentToCurrentTab(addNewDocumentFragment, true, true);
		}
		else
		{
			tryGoToFragmentByFragmentClass(AddNewDocumentFragment.class, bundle, true);
		}
	}
	
	@Override
	public void onImageClick(final Document iCurrentDocument)
	{
		if (mImgZoomable != null)
		{
			mImgZoomable.setImageBitmap(iCurrentDocument.getDocumentImage());
		}
		
		if (mFlZoomableContainer != null)
		{
			mFlZoomableContainer.setVisibility(View.VISIBLE);
		}
		
		mBtnCloseZoomable.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				if (mFlZoomableContainer != null)
				{
					mFlZoomableContainer.setVisibility(View.GONE);
				}
			}
		});
	}
	
	@Override
	public void onRemoveClick(final Document iCurrentDocument, final int iAdapterPosition)
	{
		RealmManager.getInstance().deleteDocument(iCurrentDocument);
		if (!mDocuments.isEmpty())
		{
			//			mDocuments.remove(iAdapterPosition);
			mAdapter.notifyItemRemoved(iAdapterPosition);
			//			setAdapter(iAdapterPosition);
		}
		else
		{
			if (mDocuments != null)
			{
				mDocuments.clear();
				mAdapter.notifyDataSetChanged();
			}
		}
		
	}
	
	@Override
	public void onSuccess(final ArrayList<Document> iResultObjects)
	{
		mDocuments = iResultObjects;
		
		setAdapter();
	}
	
	@Override
	public void onFailed()
	{
		setAdapter();
	}
}
