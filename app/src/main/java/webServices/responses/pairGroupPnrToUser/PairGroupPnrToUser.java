package webServices.responses.pairGroupPnrToUser;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PairGroupPnrToUser
{
	@JsonProperty("Success")
	private boolean mSuccess;
	
	public boolean getSuccess()
	{
		return this.mSuccess;
	}
	
	public PairGroupPnrToUser()
	{
	}
}
