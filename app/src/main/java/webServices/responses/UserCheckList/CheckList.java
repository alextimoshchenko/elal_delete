package webServices.responses.UserCheckList;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;

import il.co.ewave.elal.R;
import utils.global.DateTimeUtils;

/**
 * Created with care by Shahar Ben-Moshe on 22/02/17.
 */

public class CheckList implements Parcelable
{
	ArrayList<CheckListParent> mCheckListParents;
	
	public CheckList(UserCheckList iUserCheckList, Date iFlightTime, String iTimezoneOffset)
	{
		mCheckListParents = new ArrayList<>();
		
		if (iUserCheckList != null)
		{
//			Date today = new Date();
//			today = DateTimeUtils.convertDateToDateIgnoreGMT(today);
			iFlightTime = DateTimeUtils.convertDateToDateIgnoreGMT(iFlightTime);
			Date today = DateTimeUtils.getCurrentDateByGMT(iTimezoneOffset);
			
			Integer timeDifferenceInHoursInteger = DateTimeUtils.getTimeDifferenceInHoursOrNull(today, iFlightTime);
			
			if (timeDifferenceInHoursInteger != null)
			{
				int timeDifferenceInHours = timeDifferenceInHoursInteger;
				
				CheckListParent toDoNowParent = null;
				CheckListParent toDo24HoursBefore = null;
				CheckListParent toDoWeekBefore = null;
				
				if (iUserCheckList.getLessThan24Hours() != null && iUserCheckList.getLessThan24Hours().size() > 0)
				{
					toDoNowParent = new CheckListParent(R.string.to_do_now);
					toDoNowParent.addCheckListItems(iUserCheckList.getLessThan24Hours());
				}
				
				if (iUserCheckList.getToDoWeekBefore() != null && iUserCheckList.getToDoWeekBefore().size() > 0)
				{
					toDoWeekBefore = new CheckListParent(R.string.to_do_week_before);
					toDoWeekBefore.addCheckListItems(iUserCheckList.getToDoWeekBefore());
				}
				
				if (iUserCheckList.getToDo24HoursBefore() != null && iUserCheckList.getToDo24HoursBefore().size() > 0)
				{
					toDo24HoursBefore = new CheckListParent(R.string.to_do_24_hours_before);
					toDo24HoursBefore.addCheckListItems(iUserCheckList.getToDo24HoursBefore());
				}
				
				if (timeDifferenceInHours < DateTimeUtils.HOURS_OF_A_DAY)
				{
					if (toDoWeekBefore != null && toDoWeekBefore.isHasItems())
					{
						if (toDoNowParent == null)
						{
							toDoNowParent = new CheckListParent(R.string.to_do_now);
						}
						
						toDoNowParent.addCheckListItems(toDoWeekBefore.getCheckListItems());
						toDoWeekBefore = null;
					}
					
					if (toDo24HoursBefore != null && toDo24HoursBefore.isHasItems())
					{
						if (toDoNowParent == null)
						{
							toDoNowParent = new CheckListParent(R.string.to_do_now);
						}
						
						toDoNowParent.addCheckListItems(toDo24HoursBefore.getCheckListItems());
						toDo24HoursBefore = null;
					}
				}
				else if (timeDifferenceInHours == DateTimeUtils.HOURS_OF_A_DAY)
				{
					if (toDoWeekBefore != null && toDoWeekBefore.isHasItems())
					{
						if (toDo24HoursBefore == null)
						{
							toDo24HoursBefore = new CheckListParent(R.string.to_do_now);
						}
						
						toDo24HoursBefore.addCheckListItems(toDoWeekBefore.getCheckListItems());
						toDoWeekBefore = null;
					}
				}
				
				if (toDoNowParent != null)
				{
					mCheckListParents.add(toDoNowParent);
				}
				
				if (toDoWeekBefore != null)
				{
					mCheckListParents.add(toDoWeekBefore);
				}
				
				if (toDo24HoursBefore != null)
				{
					mCheckListParents.add(toDo24HoursBefore);
				}
			}
		}
	}
	
	public ArrayList<CheckListParent> getCheckListParents()
	{
		return mCheckListParents;
	}
	
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeList(this.mCheckListParents);
	}
	
	protected CheckList(Parcel in)
	{
		this.mCheckListParents = new ArrayList<CheckListParent>();
		in.readList(this.mCheckListParents, CheckListParent.class.getClassLoader());
	}
	
	public static final Parcelable.Creator<CheckList> CREATOR = new Parcelable.Creator<CheckList>()
	{
		@Override
		public CheckList createFromParcel(Parcel source)
		{
			return new CheckList(source);
		}
		
		@Override
		public CheckList[] newArray(int size)
		{
			return new CheckList[size];
		}
	};
}
