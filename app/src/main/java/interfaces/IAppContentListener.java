package interfaces;

import android.widget.FrameLayout;

import ui.customWidgets.RoundedNetworkImageView;
import webServices.responses.getAppData.AppContent;

/**
 * Created with care by Shahar Ben-Moshe on 21/02/17.
 */

public interface IAppContentListener
{
	void onButtonClick(final AppContent iCurrentAppContentItem);
	void onLoadButtonImage(final String iImgUrl, RoundedNetworkImageView iRoundedImageView, FrameLayout iFlImageFrame);
}
