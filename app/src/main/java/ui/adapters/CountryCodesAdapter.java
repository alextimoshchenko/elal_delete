package ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;

import il.co.ewave.elal.R;
import webServices.responses.getMobileCountryCodes.MobileCountryCode;

public class CountryCodesAdapter extends ArrayAdapter<MobileCountryCode>
{
	private Context context;
	private int resource;
	private ArrayList<MobileCountryCode> items, tempItems, suggestions;
	
	public CountryCodesAdapter(Context context, int resource, ArrayList<MobileCountryCode> items)
	{
		super(context, resource, items);
		this.context = context;
		this.resource = resource;
		this.items = items;
		tempItems = new ArrayList<MobileCountryCode>(items);
		suggestions = new ArrayList<MobileCountryCode>();
	}
	
	
	/**
	 * Custom Filter implementation for custom suggestions we provide.
	 */
	private Filter codeFilter = new Filter()
	{
		@Override
		public CharSequence convertResultToString(Object resultValue)
		{
			return ((MobileCountryCode) resultValue).getPrefix();
		}
		
		@Override
		protected FilterResults performFiltering(CharSequence constraint)
		{
			if (constraint != null)
			{
				suggestions.clear();
				for (MobileCountryCode countryCode : tempItems)
				{
					if (countryCode.getPrefix().toLowerCase().contains(constraint.toString().toLowerCase()))
					{
						suggestions.add(countryCode);
					}
				}
				FilterResults filterResults = new FilterResults();
				filterResults.values = suggestions;
				filterResults.count = suggestions.size();
				return filterResults;
			}
			else
			{
				return new FilterResults();
			}
		}
		
		@Override
		protected void publishResults(CharSequence constraint, FilterResults results)
		{
			ArrayList<MobileCountryCode> filterList = null;
			try
			{
				filterList = (ArrayList<MobileCountryCode>) results.values;
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			
			if (filterList != null && results.count > 0)
			{
				clear();
				for (MobileCountryCode countryCode : filterList)
				{
					add(countryCode);
					notifyDataSetChanged();
				}
			}
		}
	};
	
	
	@NonNull
	@Override
	public View getView(int position, View convertView, @NonNull ViewGroup parent)
	{
		View view = convertView;
		if (convertView == null)
		{
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.destination_search_layout, parent, false);
		}
		MobileCountryCode countryCode = items.get(position);
		if (countryCode != null)
		{
			TextView lblName = (TextView) view.findViewById(R.id.tv_destination_layout);
			if (lblName != null)
			{
				lblName.setText(countryCode.getPrefix() + " (" + countryCode.getCountryName() + ")");
			}
		}
		return view;
	}
	
	@Override
	public Filter getFilter()
	{
		return codeFilter;
	}
}