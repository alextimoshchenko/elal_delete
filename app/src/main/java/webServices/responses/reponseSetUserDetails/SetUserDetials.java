package webServices.responses.reponseSetUserDetails;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SetUserDetials
{
	@JsonProperty("Success")
	private boolean mSuccess;
	
	public boolean getSuccess()
	{
		return this.mSuccess;
	}
	
	public SetUserDetials()
	{
	}
}
