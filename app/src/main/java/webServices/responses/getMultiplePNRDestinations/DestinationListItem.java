package webServices.responses.getMultiplePNRDestinations;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

import webServices.global.ePnrFlightType;
import webServices.responses.responseGetPNRFlights.GetPNRFlightsContent;
import webServices.responses.responseGetUserActivePNRs.PnrFlight;

/**
 * Created by erline.katz on 13/08/2017.
 */

public class DestinationListItem implements Parcelable
{
	@JsonProperty("Destination") // describes the whole flight, ignoring connections if exist
	private GetPNRFlightsContent mDestination;
	
	@JsonProperty("Via") // describes the period of connection (may be null)
	private GetPNRFlightsContent mVia;
	
	@JsonProperty("IsCurrentFlight")
	private boolean mIsCurrentFlight;
	
	private ePnrFlightType mFlightType = ePnrFlightType.DEPARTURE;
	
	
	public DestinationListItem()
	{
	}
	
	public DestinationListItem(GetPNRFlightsContent mDestination, GetPNRFlightsContent mVia)
	{
		this.mDestination = mDestination;
		this.mVia = mVia;
	}
	
	public ePnrFlightType getmFlightType()
	{
		return mFlightType;
	}
	
	public void setmFlightType(ePnrFlightType mFlightType)
	{
		this.mFlightType = mFlightType;
	}
	
	public static final Creator<DestinationListItem> CREATOR = new Creator<DestinationListItem>()
	{
		@Override
		public DestinationListItem createFromParcel(Parcel in)
		{
			return new DestinationListItem(in);
		}
		
		@Override
		public DestinationListItem[] newArray(int size)
		{
			return new DestinationListItem[size];
		}
	};
	
	public boolean isCurrentFlight()
	{
		return mIsCurrentFlight;
	}
	
	public GetPNRFlightsContent getmDestination()
	{
		return mDestination;
	}
	
	public void setmDestination(GetPNRFlightsContent mDestination)
	{
		this.mDestination = mDestination;
	}
	
	public GetPNRFlightsContent getmVia()
	{
		return mVia;
	}
	
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeParcelable(this.mDestination, flags);
		dest.writeParcelable(this.mVia, flags);
		dest.writeByte(this.mIsCurrentFlight ? (byte) 1 : (byte) 0);
	}
	
	private DestinationListItem(Parcel in)
	{
		this.mDestination = in.readParcelable(getClass().getClassLoader());
		this.mVia = in.readParcelable(getClass().getClassLoader());
		this.mIsCurrentFlight = in.readByte() != 0;
	}
	
}
