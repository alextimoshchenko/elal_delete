package webServices.controllers;

import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import global.AppData;
import global.UserData;
import webServices.responses.ResponseGetAppContents;
import webServices.global.JacksonRequest;
import webServices.global.RequestStringBuilder;
import webServices.requests.RequestDeleteUserCheckList;
import webServices.requests.RequestDoCheckList;
import webServices.requests.RequestGetAppContents;
import webServices.requests.RequestGetHomeScreenContent;
import webServices.requests.RequestGetMultiplePNRDestinations;
import webServices.requests.RequestGetToSeeLinkByDestination;
import webServices.requests.RequestGetUserActivePNRs;
import webServices.requests.RequestGetUserCheckList;
import webServices.requests.RequestGetUserDestinations;
import webServices.requests.RequestGetUserSettings;
import webServices.requests.RequestGetWeather;
import webServices.requests.RequestPairGroupPNRToUser;
import webServices.requests.RequestRetrievePNR;
import webServices.requests.RequestSetUserCheckList;
import webServices.requests.RequestSetUserSettings;
import webServices.requests.RequestUpdateUserCheckList;
import webServices.responses.ResponseDeleteUserCheckList;
import webServices.responses.ResponseDoCheckList;
import webServices.responses.ResponseGetHomeScreenContent;
import webServices.responses.ResponseGetToSeeLinkByDestination;
import webServices.responses.ResponseGetUserActivePNRs;
import webServices.responses.ResponseGetUserCheckList;
import webServices.responses.ResponseGetUserDestinations;
import webServices.responses.ResponseGetUserSettings;
import webServices.responses.ResponseGetWeather;
import webServices.responses.ResponsePairGroupPnrToUser;
import webServices.responses.ResponseRetrievePNR;
import webServices.responses.ResponseSetOrUpdateUserCheckList;
import webServices.responses.ResponseSetUserSettings;
import webServices.responses.getMultiplePNRDestinations.ResponseGetMultiplePNRDestinations;

/**
 * Created with care by Shahar Ben-Moshe on 08/01/17.
 */

public class MainController extends AController implements MizdamenInterfave
{
//	/**
//	 * avishay 27/09/17
//	 * unused!
//	 * move this method to LoginController
//	 */
//	public void GetHomeScreenContent(final RequestGetHomeScreenContent iRequest, final Response.Listener<ResponseGetHomeScreenContent> iListener, final Response.ErrorListener iErrorListener)
//	{
//		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
//		{
//			JacksonRequest<ResponseGetHomeScreenContent> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getGetHomeScreenContentUrl(), iRequest, ResponseGetHomeScreenContent.class, iListener, iErrorListener);
//			request.setSecretHashHeader(request.getBody());
//			getRequestQueue().add(request);
//		}
//	}
	
//	public void GetSideMenuItems(final RequestGetSideMenuItems iRequest, final Response.Listener<ResponseGetSideMenuItems> iListener, final Response.ErrorListener errorListener)
//	{
//		if (!isRequestQueueNull() && iListener != null && errorListener != null)
//		{
//			JacksonRequest<ResponseGetSideMenuItems> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getGetSideMenuItemsUrl(), iRequest, ResponseGetSideMenuItems.class, iListener, errorListener);
//			request.setSecretHashHeader(request.getBody());
//			getRequestQueue().add(request);
//		}
//	}
	
	public void GetUserActivePNRs(final RequestGetUserActivePNRs iRequest, final Response.Listener<ResponseGetUserActivePNRs> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseGetUserActivePNRs> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getUserActivePnrsUrl(), iRequest, ResponseGetUserActivePNRs.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void GetMultiplePNRDestinations(final RequestGetMultiplePNRDestinations iRequest, final Response.Listener<ResponseGetMultiplePNRDestinations> iListener,
	                                       final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseGetMultiplePNRDestinations> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getMultiplePnrDestinationsUrl(), iRequest, ResponseGetMultiplePNRDestinations.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void GetUserSettings(final RequestGetUserSettings iRequest, final Response.Listener<ResponseGetUserSettings> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseGetUserSettings> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getUserSettingsUrl(), iRequest, ResponseGetUserSettings.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void SetUserSettings(final RequestSetUserSettings iRequest, final Response.Listener<ResponseSetUserSettings> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseSetUserSettings> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getSetUserSettingsUrl(), iRequest, ResponseSetUserSettings.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void RetrievePnr(final RequestRetrievePNR iRequest, final Response.Listener<ResponseRetrievePNR> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseRetrievePNR> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getRetrievePnrUrl(), iRequest, ResponseRetrievePNR.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void PairGroupPnrToUser(final RequestPairGroupPNRToUser iRequest, final Response.Listener<ResponsePairGroupPnrToUser> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponsePairGroupPnrToUser> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getPairGroupPnrToUserUrl(), iRequest, ResponsePairGroupPnrToUser.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void getUserDestinations(final RequestGetUserDestinations iRequest, final Response.Listener<ResponseGetUserDestinations> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseGetUserDestinations> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getGetUserDestinationsUrl(), iRequest, ResponseGetUserDestinations.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void getGetAppContents(final RequestGetAppContents iRequest, final Response.Listener<ResponseGetAppContents> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseGetAppContents> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getGetAppContentsUrl(), iRequest, ResponseGetAppContents.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void getGetToSeeLinkByDestination(final RequestGetToSeeLinkByDestination iRequest, final Response.Listener<ResponseGetToSeeLinkByDestination> iListener,
	                                         final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseGetToSeeLinkByDestination> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getGetToSeeLinkByDestinationUrl(), iRequest, ResponseGetToSeeLinkByDestination.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void GetWeather(final RequestGetWeather iRequest, final Response.Listener<ResponseGetWeather> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseGetWeather> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getWeatherUrl(), iRequest, ResponseGetWeather.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void getUserCheckList(final RequestGetUserCheckList iRequest, final Response.Listener<ResponseGetUserCheckList> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseGetUserCheckList> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getGetUserCheckListUrl(), iRequest, ResponseGetUserCheckList.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void setUserCheckList(final RequestSetUserCheckList iRequest, final Response.Listener<ResponseSetOrUpdateUserCheckList> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseSetOrUpdateUserCheckList> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getSetUserCheckListUrl(), iRequest, ResponseSetOrUpdateUserCheckList.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void updateUserCheckList(final RequestUpdateUserCheckList iRequest, final Response.Listener<ResponseSetOrUpdateUserCheckList> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseSetOrUpdateUserCheckList> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getSetUserCheckListUrl(), iRequest, ResponseSetOrUpdateUserCheckList.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void deleteUserCheckList(final RequestDeleteUserCheckList iRequest, final Response.Listener<ResponseDeleteUserCheckList> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseDeleteUserCheckList> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getDeleteUserCheckListUrl(), iRequest, ResponseDeleteUserCheckList.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void doCheckList(final RequestDoCheckList iRequest, final Response.Listener<ResponseDoCheckList> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseDoCheckList> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getDoCheckListUrl(), iRequest, ResponseDoCheckList.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	
	public void getHomeScreenContent(final Response.Listener<ResponseGetHomeScreenContent> iResponseListener, final Response.ErrorListener iErrorListener)
	{
		String sessionId = "";
		String matmidMemberId = "";
		if (UserData.getInstance().getUserObject() != null)
		{
			if (!TextUtils.isEmpty(UserData.getInstance().getUserObject().getSessionID()))
			{
				sessionId = UserData.getInstance().getUserObject().getSessionID();
			}
			if (!TextUtils.isEmpty(UserData.getInstance().getUserObject().getMatmidMemberID()))
			{
				matmidMemberId = UserData.getInstance().getUserObject().getMatmidMemberID();
			}
		}
		
		Response.Listener<ResponseGetHomeScreenContent> responseListener;
		Response.ErrorListener errorListener;
		
		if (iResponseListener != null)
		{
			responseListener = iResponseListener;
		}
		else
		{
			responseListener = new Response.Listener<ResponseGetHomeScreenContent>()
			{
				@Override
				public void onResponse(final ResponseGetHomeScreenContent iResponse)
				{
					
					if (iResponse != null && iResponse.getContent() != null)
					{
						AppData.getInstance().setHomeItems(iResponse.getContent());
					}
				}
			};
		}
		if (iErrorListener != null)
		{
			errorListener = iErrorListener;
		}
		else
		{
			errorListener = new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError error)
				{
				
				
				}
			};
		}
		
		GetHomeScreenContent(new RequestGetHomeScreenContent(UserData.getInstance().isUserGuest() ?
		                                                     UserData.getInstance().getGuestUserId() :
		                                                     UserData.getInstance().getUserID(), sessionId, matmidMemberId), responseListener, errorListener);
		
	}
	
	private void GetHomeScreenContent(final RequestGetHomeScreenContent iRequest, final Response.Listener<ResponseGetHomeScreenContent> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseGetHomeScreenContent> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getGetHomeScreenContentUrl(), iRequest, ResponseGetHomeScreenContent.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
}
