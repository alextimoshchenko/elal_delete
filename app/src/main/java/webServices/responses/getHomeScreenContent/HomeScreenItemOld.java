package webServices.responses.getHomeScreenContent;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

import utils.global.ParseUtils;



public class HomeScreenItemOld
{
	@JsonProperty("IsActive")
	private boolean mIsActive;
	@JsonProperty("HomeContentNameEng")
	private String mHomeContentNameEng;
	@JsonProperty("InApp")
	private boolean mIsInApp;
	@JsonProperty("CreateDate")
	private Date mCreateDate;
	@JsonProperty("HomeContentName")
	private String mHomeContentName;
	@JsonProperty("LastModified")
	private String mLastModified;
	@JsonProperty("HomeContentNameHeb")
	private String mHomeContentNameHeb;
	@JsonProperty("IsConst")
	private boolean mIsConst;
	@JsonProperty("LanguageCode")
	private String mLanguageCode;
	@JsonProperty("ContentLocation")
	private int mContentLocation;
	@JsonProperty("Icon")
	private String mIcon;
	@JsonProperty("AppContentTypeId")
	private int mAppContentTypeId;
	@JsonProperty("HomeContentId")
	private int mHomeContentId;
	@JsonProperty("Link")
	private String mLink;
	
	public HomeScreenItemOld()
	{
	}
	
	public boolean isInApp()
	{
		return mIsInApp;
	}
	
	public String getHomeContentName()
	{
		return this.mHomeContentName;
	}
	
	public String getIcon()
	{
		return this.mIcon;
	}
	
	public int getAppContentTypeId()
	{
		return this.mAppContentTypeId;
	}
	
	public String getLink()
	{
		return this.mLink;
	}
	
	public int getLinkInt()
	{
		return ParseUtils.tryParseStringToIntegerOrDefault(mLink, 0);
	}
}

