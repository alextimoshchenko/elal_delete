//package ui.fragments;
//
//import android.content.Intent;
//import android.graphics.Point;
//import android.graphics.Rect;
//import android.os.Bundle;
//import android.os.CountDownTimer;
//import android.os.Handler;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.text.TextUtils;
//import android.view.View;
//import android.view.ViewTreeObserver;
//import android.widget.Button;
//import android.widget.FrameLayout;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.ScrollView;
//import android.widget.TabHost;
//import android.widget.TextView;
//
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.NetworkImageView;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Date;
//
//import global.ElAlApplication;
//import global.IntentExtras;
//import il.co.ewave.elal.R;
//import ui.adapters.PassengersAdapter;
//import ui.customWidgets.ExpandCollapseAnimation;
//import utils.errors.ErrorsHandler;
//import utils.global.AppUtils;
//import utils.global.DateTimeUtils;
//import utils.global.dbObjects.Document;
//import utils.managers.RealmManager;
//import utils.managers.TabsBackStackManager;
//import webServices.ResponseGetAppContents;
//import webServices.controllers.MainController;
//import webServices.global.RequestStringBuilder;
//import webServices.requests.RequestGetAppContents;
//import webServices.responses.ResponseGetUserCheckList;
//import webServices.responses.ResponseGetWeather;
//import webServices.responses.UserCheckList.UserCheckList;
//import webServices.responses.getAppData.AppContent;
//import webServices.responses.getWeather.Weather;
//import webServices.responses.responseGetUserActivePNRs.PnrFlight;
//import webServices.responses.responseGetUserActivePNRs.UserActivePnr;
//
//import static utils.global.AppUtils.eFlightStatus.LessThanDayToDestination;
//
///**
// * Created with care by Shahar Ben-Moshe on 30/01/17.
// */
//public class MyFlightFragment extends EWBaseFragment implements TabsBackStackManager.ITabsBackStackManagerHandler, TabsBackStackManager.IOnTabChangeListener, Response.ErrorListener
//{
//	private static final int DETAILS_EXPAND_COLLAPSE_ANIMATION_SPEED = 500;
//
//	private ScrollView mSvRoot;
//	private FrameLayout mFlTopContainer;
//	private TabHost mTabHost;
//	private FrameLayout mFlTabsContainer;
//	private TabsBackStackManager mTabsBackStackManager;
//	private NetworkImageView mNivTopRootBackground;
//	private Button mBtnFlightStatusReady;
//	private Button mBtnFlightStatusAtDestination;
//	private TextView mTvTimeTitle;
//	private TextView mTvTimeLeftCounter;
//	private TextView mTvOrigin;
//	private ImageView mIvPlane;
//	private TextView mTvDestination;
//	private TextView mTvDepartureDateTime;
//	private TextView mTvFlightDuration;
//	private TextView mTvArrivalDateTime;
//	private TextView mTvBoardingTime;
//	private TextView mTvGate;
//	private Button mBtnCheckIn;
//	private LinearLayout mLllExpandButtonRoot;
//	private TextView mTvExpandButtonName;
//	private ImageView mIvExpandButtonArrow;
//	private Button mBtnExpandButton;
//	private LinearLayout mLlDetails;
//	private TextView mTvFlightDetailsNumber;
//	private TextView mTvFlightDetailsPlaneType;
//	private Button mBtnFlightDetails;
//	private PassengersAdapter mPassengersAdapter;
//	private RecyclerView mRvPassengers;
//	private Button mBtnBoardingPass;
//	private Button mBtnChangeSeats;
//	private Button mBtnChangeMeals;
//	private ExpandCollapseAnimation mExpandCollapseAnimation;
//	private UserActivePnr mUserActivePnr;
//	private ArrayList<AppContent> mAppContent;
//	private AppUtils.eFlightStatus mFlightStatus;
//	private UserCheckList mUserCheckList;
//	private Weather mWeather;
//	private boolean mShouldScroll = true;
//	private boolean mDidRequestData = false;
//
//	public static Bundle createBundleForFragment(@NonNull final UserActivePnr iUserActivePnr)
//	{
//		Bundle result = new Bundle();
//
//		result.putParcelable(IntentExtras.USER_ACTIVE_PNR, iUserActivePnr);
//
//		return result;
//	}
//
//	private static final ArrayList<TabsBackStackManager.TabItem> mPrePostFlightTabItems = new ArrayList<>();
//
//	static
//	{
//		mPrePostFlightTabItems.add(new TabsBackStackManager.TabItem(ElAlApplication.getInstance().getString(R.string.to_know), ToKnowFragment.class, null, R.mipmap.icon_to_know));
//		mPrePostFlightTabItems.add(new TabsBackStackManager.TabItem(ElAlApplication.getInstance().getString(R.string.to_see), ToSeeFragment.class, null, R.mipmap.icon_to_see));
//		mPrePostFlightTabItems.add(new TabsBackStackManager.TabItem(ElAlApplication.getInstance().getString(R.string.to_do), ToDoFragment.class, null, R.mipmap.icon_to_do));
//		mPrePostFlightTabItems.add(new TabsBackStackManager.TabItem(ElAlApplication.getInstance().getString(R.string.to_save), ToSaveFragment.class, null, R.mipmap.icon_to_save));
//	}
//
//	/**
//	 * eFlightStatus.MoreThanDayToOrigination and AppUtils.utils.global.AppUtils.eFlightStatus.NoToOrigination
//	 */
//	private static final ArrayList<TabsBackStackManager.TabItem> mAtDestinationFlightTabItems = new ArrayList<>();
//
//	static
//	{
//		mAtDestinationFlightTabItems.add(new TabsBackStackManager.TabItem(ElAlApplication.getInstance().getString(R.string.useful_info), UsefulInfoFragment.class, null, R.mipmap.icon_to_know));
//		mAtDestinationFlightTabItems.add(new TabsBackStackManager.TabItem(ElAlApplication.getInstance().getString(R.string.to_see), ToSeeFragment.class, null, R.mipmap.icon_to_see));
//		mAtDestinationFlightTabItems.add(new TabsBackStackManager.TabItem(ElAlApplication.getInstance().getString(R.string.to_save), ToSaveFragment.class, null, R.mipmap.icon_to_save));
//	}
//
//	@Override
//	public void onCreate(@Nullable final Bundle savedInstanceState)
//	{
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.fragment_my_flight_layout);
//	}
//
//	@Override
//	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
//	{
//		trySetProgressDialog(true);
//		getIntentExtras();
//		setFlightStatus();
//		init();
//		setListeners();
////		initTabs();
//		setTopElementData();
//		initGui();
//	}
//
//	private void setFlightStatus()
//	{
//		mFlightStatus = AppUtils.getFlightStatusByUserActivePnr(mUserActivePnr);
//	}
//
//	private void getIntentExtras()
//	{
//		if (getArguments() != null)
//		{
//			mUserActivePnr = getArguments().getParcelable(IntentExtras.USER_ACTIVE_PNR);
//		}
//	}
//
//	private void init()
//	{
//		mSvRoot = (ScrollView) getActivity().findViewById(R.id.sv_myFlight_Root);
//		mFlTopContainer = (FrameLayout) getActivity().findViewById(R.id.fl_myFlight_TopContainer);
//		mTabHost = (TabHost) getActivity().findViewById(android.R.id.tabhost);
//		mFlTabsContainer = (FrameLayout) getActivity().findViewById(R.id.fl_myFlight_TabsContainer);
//
//		mNivTopRootBackground = (NetworkImageView) getActivity().findViewById(R.id.niv_myFlightTop_RootBackground);
//		mBtnFlightStatusReady = (Button) getActivity().findViewById(R.id.btn_myFlightTop_FlightStatusReady);
//		mBtnFlightStatusAtDestination = (Button) getActivity().findViewById(R.id.btn_myFlightTop_FlightStatusAtDestination);
//		mTvTimeTitle = (TextView) getActivity().findViewById(R.id.tv_myFlightTop_TimeTitle);
//		mTvTimeLeftCounter = (TextView) getActivity().findViewById(R.id.tv_myFlightTop_TimeLeftCounter);
//		mTvOrigin = (TextView) getActivity().findViewById(R.id.tv_myFlightTop_Origin);
//		mIvPlane = (ImageView) getActivity().findViewById(R.id.iv_myFlightTop_Plane);
//		mTvDestination = (TextView) getActivity().findViewById(R.id.tv_myFlightTop_Destination);
//		mTvDepartureDateTime = (TextView) getActivity().findViewById(R.id.tv_myFlightTop_DepartureDateTime);
//		mTvFlightDuration = (TextView) getActivity().findViewById(R.id.tv_myFlightTop_FlightDuration);
//		mTvArrivalDateTime = (TextView) getActivity().findViewById(R.id.tv_myFlightTop_ArrivalDateTime);
//		mTvBoardingTime = (TextView) getActivity().findViewById(R.id.tv_myFlightTop_BoardingTime);
//		mTvGate = (TextView) getActivity().findViewById(R.id.tv_myFlightTop_Gate);
//		mBtnCheckIn = (Button) getActivity().findViewById(R.id.btn_myFlightTop_CheckIn);
//		mLllExpandButtonRoot = (LinearLayout) getActivity().findViewById(R.id.ll_myFlightTop_ExpandButtonRoot);
//		mTvExpandButtonName = (TextView) getActivity().findViewById(R.id.tv_myFlightTop_ExpandButtonName);
//		mIvExpandButtonArrow = (ImageView) getActivity().findViewById(R.id.iv_myFlightTop_ExpandButtonArrow);
//		mBtnExpandButton = (Button) getActivity().findViewById(R.id.btn_myFlightTop_ExpandButton);
//		mLlDetails = (LinearLayout) getActivity().findViewById(R.id.ll_myFlightTop_Details);
//		mTvFlightDetailsNumber = (TextView) getActivity().findViewById(R.id.tv_myFlightTop_FlightDetailsNumber);
//		mTvFlightDetailsPlaneType = (TextView) getActivity().findViewById(R.id.tv_myFlightTop_FlightDetailsPlaneType);
//		mBtnFlightDetails = (Button) getActivity().findViewById(R.id.btn_myFlightTop_FlightDetails);
//		mRvPassengers = (RecyclerView) getActivity().findViewById(R.id.rv_myFlightTop_Passengers);
//		mBtnBoardingPass = (Button) getActivity().findViewById(R.id.btn_myFlightTop_BoardingPass);
//		mBtnChangeSeats = (Button) getActivity().findViewById(R.id.btn_myFlightTop_ChangeSeats);
//		mBtnChangeMeals = (Button) getActivity().findViewById(R.id.btn_myFlightTop_ChangeMeals);
//	}
//
//	private void setListeners()
//	{
//		mBtnCheckIn.setOnClickListener(new View.OnClickListener()
//		{
//			@Override
//			public void onClick(final View v)
//			{
//				onCheckinClick();
//			}
//		});
//
//		mBtnExpandButton.setOnClickListener(new View.OnClickListener()
//		{
//			@Override
//			public void onClick(final View v)
//			{
//				animateDetails();
//			}
//		});
//
//		mBtnFlightDetails.setOnClickListener(new View.OnClickListener()
//		{
//			@Override
//			public void onClick(final View v)
//			{
//				onFlightDetailsClick();
//			}
//		});
//
//		mBtnBoardingPass.setOnClickListener(new View.OnClickListener()
//		{
//			@Override
//			public void onClick(final View v)
//			{
//				onBoardingPassClick();
//			}
//		});
//
//		mBtnChangeSeats.setOnClickListener(new View.OnClickListener()
//		{
//			@Override
//			public void onClick(final View v)
//			{
//				onChangeSeatsClick();
//			}
//		});
//
//		mBtnChangeMeals.setOnClickListener(new View.OnClickListener()
//		{
//			@Override
//			public void onClick(final View v)
//			{
//				onChangeMealsClick();
//			}
//		});
//
//		mBtnBoardingPass.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
//		{
//			@Override
//			public void onGlobalLayout()
//			{
//				updateBoardingPassGui();
//			}
//		});
//	}
//
//	private void updateBoardingPassGui()
//	{
//		mBtnBoardingPass.setEnabled(RealmManager.getInstance().getDocumentCountForCategory(mUserActivePnr.getPnr(), Document.eDocumentType.BOARDING_PASS) > 0);
//	}
//
//	private void onCheckinClick()
//	{
//		tryGoToWebFragment(RequestStringBuilder.getCheckinUrlWithParamsByLanguage(mUserActivePnr.getPnr(), mUserActivePnr.getPassengers().get(0)));
//	}
//
//	private void onFlightDetailsClick()
//	{
//		AppUtils.createFlightDetailsDialog(getActivity(), mUserActivePnr).show();
//	}
//
//	private void onBoardingPassClick()
//	{
//	}
//
//	private void onChangeSeatsClick()
//	{
//		tryGoToWebFragment(RequestStringBuilder.getChangeSeatUrlByLanguage());
//	}
//
//	private void onChangeMealsClick()
//	{
//		tryGoToWebFragment(RequestStringBuilder.getChangeMealUrlByLanguage());
//	}
//
//	private void initTabs()
//	{
//		mShouldScroll = false;
//		ArrayList<TabsBackStackManager.TabItem> tabItems = new ArrayList<>();
//
//		int defaultPosition = 0;
//		String iata = (mUserActivePnr == null || mUserActivePnr.getCurrentItemFlightAtPositionOrNull(0) == null) ? "" : mUserActivePnr.getCurrentItemFlightAtPositionOrNull(0).getDestination();
//
//		Bundle bundle;
//		bundle = null;
//
//		if (AppUtils.isFlightStatusAtDestination(mFlightStatus))
//		{
//			bundle = UsefulInfoFragment.createBundleForFragment(iata, mWeather);
//			tabItems.add(new TabsBackStackManager.TabItem(ElAlApplication.getInstance().getString(R.string.useful_info), UsefulInfoFragment.class, bundle, R.mipmap.icon_to_know));
//
//			bundle = null;
//
//			if (!TextUtils.isEmpty(iata))
//			{
//				bundle = ToSeeFragment.createBundleForFragment(iata);
//			}
//			tabItems.add(new TabsBackStackManager.TabItem(ElAlApplication.getInstance().getString(R.string.to_see), ToSeeFragment.class, bundle, R.mipmap.icon_to_see));
//
//			bundle = MyDocumentsFragment.createBundleForFragment(mUserActivePnr);
//			tabItems.add(new TabsBackStackManager.TabItem(ElAlApplication.getInstance().getString(R.string.to_save), MyDocumentsFragment.class, bundle, R.mipmap.icon_to_save));
//
//		}
//		else
//		{
//			if (mAppContent != null)
//			{
//				bundle = ToKnowFragment.createBundleForFragment(mAppContent);
//			}
//			tabItems.add(new TabsBackStackManager.TabItem(ElAlApplication.getInstance().getString(R.string.to_know), ToKnowFragment.class, bundle, R.mipmap.icon_to_know));
//
//			bundle = null;
//
//			if (!TextUtils.isEmpty(iata))
//			{
//				bundle = ToSeeFragment.createBundleForFragment(iata);
//			}
//			tabItems.add(new TabsBackStackManager.TabItem(ElAlApplication.getInstance().getString(R.string.to_see), ToSeeFragment.class, bundle, R.mipmap.icon_to_see));
//
//			bundle = null;
//
//			if (mUserActivePnr != null && mUserCheckList != null)
//			{
//				bundle = ToDoFragment.createBundleForFragment(mUserActivePnr, mUserCheckList);
//			}
//			tabItems.add(new TabsBackStackManager.TabItem(ElAlApplication.getInstance().getString(R.string.to_do), ToDoFragment.class, bundle, R.mipmap.icon_to_do));
//
//			bundle = MyDocumentsFragment.createBundleForFragment(mUserActivePnr);
//			tabItems.add(new TabsBackStackManager.TabItem(ElAlApplication.getInstance().getString(R.string.to_save), MyDocumentsFragment.class, bundle, R.mipmap.icon_to_save));
//		}
//
//		if (AppUtils.isDefaultLocaleRTL())
//		{
//			Collections.reverse(tabItems);
//			defaultPosition = tabItems.size() - 1;
//		}
//
//		mTabsBackStackManager = new TabsBackStackManager(getActivity(), mFlTabsContainer, mTabHost, tabItems, getChildFragmentManager(), defaultPosition, this);
//	}
//
//	@Override
//	public void onServiceConnected()
//	{
//		getDataIfNeeded();
//	}
//
//	private void getDataIfNeeded()
//	{
//		if (isServiceConnected() && !mDidRequestData)
//		{
//			mDidRequestData = true;
//
//			getAppContent();
//			GetWeather();
//			getUserCheckList();
//		}
//	}
//
//	private void doOnDataReceived()
//	{
//		if (isAllDataReceivedFromWebServices())
//		{
//			initTabs();
//			trySetProgressDialog(false);
//		}
//	}
//
//	private boolean isAllDataReceivedFromWebServices()
//	{
//		return mAppContent != null && mWeather != null && mUserCheckList != null;
//	}
//
//	private void getAppContent()
//	{
//		if (isServiceConnected() && mAppContent == null)
//		{
//			trySetProgressDialog(true);
//
//			PnrFlight currentFlight = mUserActivePnr.getCurrentItemFlightAtPositionOrNull(0);
//			String flightNumber = currentFlight != null ? AppUtils.nullSafeCheckString(currentFlight.getFlightNumber()) : "";
//
//			getService().getController(MainController.class).getGetAppContents(new RequestGetAppContents(mUserActivePnr.getPnr(), flightNumber), new Response.Listener<ResponseGetAppContents>()
//			{
//				@Override
//				public void onResponse(final ResponseGetAppContents iResponse)
//				{
//					if (iResponse != null)
//					{
//						mAppContent = iResponse.getContent();
//					}
//
//					initAppContentIfNeeded();
//					doOnDataReceived();
//				}
//			}, new Response.ErrorListener()
//			{
//				@Override
//				public void onErrorResponse(final VolleyError error)
//				{
//					initAppContentIfNeeded();
//				}
//			});
//		}
//	}
//
//	private void initAppContentIfNeeded()
//	{
//		if (mAppContent == null)
//		{
//			mAppContent = new ArrayList<>();
//		}
//	}
//
//	private void GetWeather()
//	{
//		if (isServiceConnected() && mUserActivePnr != null/* && !AppUtils.isFlightStatusAtDestination(mFlightStatus)*/)
//		{
//			trySetProgressDialog(true);
//
//			getService().getController(MainController.class).GetWeather(mUserActivePnr.generateWeatherRequest(), new Response.Listener<ResponseGetWeather>()
//			{
//				@Override
//				public void onResponse(final ResponseGetWeather response)
//				{
//					if (response != null)
//					{
//						mWeather = response.getContent();
//					}
//
//					initWeatherIfNeeded();
//					doOnDataReceived();
//				}
//			}, new Response.ErrorListener()
//			{
//				@Override
//				public void onErrorResponse(final VolleyError error)
//				{
//					initWeatherIfNeeded();
//				}
//			});
//		}
//	}
//
//	private void initWeatherIfNeeded()
//	{
//		if (mWeather == null)
//		{
//			mWeather = new Weather();
//		}
//	}
//
//	private void getUserCheckList()
//	{
//		if (isServiceConnected() && mUserActivePnr != null/* && !AppUtils.isFlightStatusAtDestination(mFlightStatus)*/)
//		{
//			trySetProgressDialog(true);
//
//			getService().getController(MainController.class).getUserCheckList(mUserActivePnr.generateUserCheckListRequest(), new Response.Listener<ResponseGetUserCheckList>()
//			{
//				@Override
//				public void onResponse(final ResponseGetUserCheckList iResponse)
//				{
//					if (iResponse != null)
//					{
//						mUserCheckList = iResponse.getContent();
//					}
//
//					initUserCheckListIfNeeded();
//					doOnDataReceived();
//				}
//			}, new Response.ErrorListener()
//			{
//				@Override
//				public void onErrorResponse(final VolleyError error)
//				{
//					initUserCheckListIfNeeded();
//				}
//			});
//		}
//	}
//
//	private void initUserCheckListIfNeeded()
//	{
//		if (mUserCheckList == null)
//		{
//			mUserCheckList = new UserCheckList();
//		}
//	}
//
//	@Override
//	public void onErrorResponse(final VolleyError error)
//	{
//		ErrorsHandler.tryShowServiceErrorDialog(error, getActivity());
//	}
//
//	private void initGui()
//	{
//		mLlDetails.setVisibility(View.INVISIBLE);
//		setFlightHeadersByFlightStatus();
//		mBtnCheckIn.setVisibility(mFlightStatus == LessThanDayToDestination ? View.VISIBLE : View.GONE);
//		updateBoardingPassGui();
//
//		new Handler().post(new Runnable()
//		{
//			@Override
//			public void run()
//			{
//				Point mWindowPoint = new Point();
//				getActivity().getWindow().getWindowManager().getDefaultDisplay().getSize(mWindowPoint);
//				int containerHeightWithoutTabs = (int) (mSvRoot.getHeight() - AppUtils.resolveActionBarHeight(ElAlApplication.getInstance()));
//				int indicatorArrowHeight = (int) AppUtils.resolveDrawableHeight(getActivity(), R.mipmap.arrow_down_blue);
//
//				mFlTopContainer.setLayoutParams(new LinearLayout.LayoutParams(mSvRoot.getWidth(), containerHeightWithoutTabs + indicatorArrowHeight));
//				mFlTabsContainer.setLayoutParams(new LinearLayout.LayoutParams(mSvRoot.getWidth(), containerHeightWithoutTabs));
//
//				resolveExpandableLayout();
//			}
//		});
//	}
//
//	private void resolveExpandableLayout()
//	{
//		new Handler().post(new Runnable()
//		{
//			@Override
//			public void run()
//			{
//				int expandedViewHeight = mLlDetails.getHeight();
//				LinearLayout.LayoutParams oldParams = (LinearLayout.LayoutParams) mLlDetails.getLayoutParams();
//				LinearLayout.LayoutParams newParams = new LinearLayout.LayoutParams(oldParams.width, expandedViewHeight);
//				mLlDetails.setLayoutParams(newParams);
//
//				mLlDetails.setVisibility(View.GONE);
//				mExpandCollapseAnimation = new ExpandCollapseAnimation(mLlDetails, false, DETAILS_EXPAND_COLLAPSE_ANIMATION_SPEED, expandedViewHeight);
//				setGuiByExpandState(mExpandCollapseAnimation.isExpand());
//			}
//		});
//	}
//
//	private void setFlightHeadersByFlightStatus()
//	{
//		if (AppUtils.isFlightStatusAtDestination(mFlightStatus))
//		{
//			mBtnFlightStatusReady.setEnabled(false);
//			mBtnFlightStatusAtDestination.setEnabled(true);
//		}
//		else
//		{
//			mBtnFlightStatusReady.setEnabled(true);
//			mBtnFlightStatusAtDestination.setEnabled(false);
//		}
//	}
//
//	private void setTopElementData()
//	{
//		mNivTopRootBackground.setDefaultImageResId(R.mipmap.background_flight_management);
//		mNivTopRootBackground.setErrorImageResId(R.mipmap.background_flight_management);
//
//		PnrFlight currentFlight = mUserActivePnr.getCurrentItemFlightAtPositionOrNull(0);
//		mIvPlane.setImageResource(AppUtils.isDefaultLocaleRTL() ? R.mipmap.plane_white_to_left : R.mipmap.plane_white_to_right);
//
//		if (currentFlight != null)
//		{
//			mNivTopRootBackground.setImageUrl(RequestStringBuilder.getDestinationImageUrl(currentFlight.getDestinationImage()), getService().getImageLoader());
//			mTvTimeTitle.setText(getTimeTitleByFlightStatus());
//			setTimeLeftCounterByFlightStatus();
//			mTvOrigin.setText(currentFlight.getOrigination());
//			mTvDestination.setText(currentFlight.getDestination());
//			mTvDepartureDateTime.setText(setDateTimeText(currentFlight.getDepartureDate()));
//			mTvFlightDuration.setText(currentFlight.getFlightDuration());
//			mTvArrivalDateTime.setText(setDateTimeText(currentFlight.getArrivalDate()));
//			// TODO: 14/02/17 Shahar add these to ws object after reuven adds it to the ws.
//			//			mTvBoardingTime.setText(setDateTimeText(currentFlight.getBoardingTime()));
//			//			mTvGate.setText(currentFlight.getGate());
//			mTvFlightDetailsNumber.setText(currentFlight.getFlightNumber());
//			mTvFlightDetailsPlaneType.setText(currentFlight.getAircraftType());
//			setPassengersAdapter();
//		}
//		else
//		{
//			mTvTimeTitle.setText("");
//			mTvTimeLeftCounter.setText("");
//			mTvOrigin.setText("");
//			mTvDestination.setText("");
//			mTvDepartureDateTime.setText("");
//			mTvFlightDuration.setText("");
//			mTvArrivalDateTime.setText("");
//			mTvBoardingTime.setText("");
//			mTvGate.setText("");
//			mTvFlightDetailsNumber.setText("");
//			mTvFlightDetailsPlaneType.setText("");
//		}
//	}
//
//	private void setPassengersAdapter()
//	{
//		if (mPassengersAdapter == null)
//		{
//			mPassengersAdapter = new PassengersAdapter(mUserActivePnr.getPassengers());
//			mRvPassengers.setLayoutManager(new LinearLayoutManager(getActivity()));
//			mRvPassengers.setAdapter(mPassengersAdapter);
//		}
//		else
//		{
//			mPassengersAdapter.notifyDataSetChanged();
//		}
//	}
//
//	private void setTimeLeftCounterByFlightStatus()
//	{
//		mTvTimeLeftCounter.setText("");
//
//		PnrFlight currentFlight = mUserActivePnr.getCurrentItemFlightAtPositionOrNull(0);
//		if (mFlightStatus == LessThanDayToDestination && currentFlight != null)
//		{
//			long timeDifference = DateTimeUtils.getTimeDifferenceInMilliseconds(new Date(), currentFlight.getDepartureDate());
//
//			if (timeDifference > 0)
//			{
//				new CountDownTimer(timeDifference, DateTimeUtils.SECOND_IN_MILLISECONDS)
//				{
//
//					@Override
//					public void onTick(final long iNewDeltaInMilliseconds)
//					{
//						tryUpdateCounter(mTvTimeLeftCounter, iNewDeltaInMilliseconds);
//					}
//
//					@Override
//					public void onFinish()
//					{
//						tryUpdateCounter(mTvTimeLeftCounter, 0);
//					}
//				}.start();
//			}
//		}
//	}
//
//	private void tryUpdateCounter(final TextView iTvTimeLeftCounter, final long iNewValue)
//	{
//		iTvTimeLeftCounter.setText(DateTimeUtils.convertDDeltaInMillisecondsToTimeString(iNewValue));
//	}
//
//	private String getTimeTitleByFlightStatus()
//	{
//		String result;
//		switch (mFlightStatus)
//		{
//			case MoreThanDayToDestination:
//				Integer timeDifferenceInDays = DateTimeUtils.getTimeDifferenceInDaysOrNull(new Date(), mUserActivePnr.getCurrentItemFlightAtPositionOrNull(0).getDepartureDate());
//				result = timeDifferenceInDays == null ? "" : getString(R.string.days_to_go, String.valueOf(timeDifferenceInDays));
//				break;
//			case LessThanDayToDestination:
//				result = getString(R.string.nearly_there);
//				break;
//			case MoreThanDayToOrigination:
//			case LessThanDayToOrigination:
//				result = getString(R.string.have_a_great_time);
//				break;
//			case AfterBackToOrigination:
//				result = getString(R.string.welcome_back);
//				break;
//			case NoToOrigination:
//				result = "";
//				break;
//			case Error:
//			default:
//				result = "";
//				break;
//		}
//
//		return result;
//	}
//
//	private String setDateTimeText(final Date iDate)
//	{
//		String result = "";
//
//		if (iDate != null)
//		{
//			result = DateTimeUtils.convertDateToDayMonthHourMinuteString(iDate);
//		}
//
//		return result;
//	}
//
//	private void animateDetails()
//	{
//		mExpandCollapseAnimation.reverseAndAnimate();
//		setGuiByExpandState(mExpandCollapseAnimation.isExpand());
//	}
//
//	private void setGuiByExpandState(final boolean iIsExpanded)
//	{
//		if (iIsExpanded)
//		{
//			mLllExpandButtonRoot.setBackgroundColor(getResources().getColor(R.color.ElalBrown));
//			mTvExpandButtonName.setText(getString(R.string.close));
//			mTvExpandButtonName.setTextAppearance(ElAlApplication.getInstance(), R.style.Text_12sp_White);
//			mIvExpandButtonArrow.setImageResource(R.mipmap.arrow_up_white);
//		}
//		else
//		{
//			mLllExpandButtonRoot.setBackgroundColor(getResources().getColor(R.color.ElalTextFieldGray));
//			mTvExpandButtonName.setText(getString(R.string.seat_meal_selection_and_more));
//			mTvExpandButtonName.setTextAppearance(ElAlApplication.getInstance(), R.style.Text_12sp_DarkBlue);
//			mIvExpandButtonArrow.setImageResource(R.mipmap.arrow_down_blue);
//		}
//	}
//
//	@Override
//	public void pushFragmentToCurrentTab(final EWBaseFragment iFragment, final boolean iShouldAnimate, final boolean iShouldAdd)
//	{
//		mTabsBackStackManager.pushFragment(TabsBackStackManager.CURRENT_TAB, iFragment, iShouldAnimate, iShouldAdd);
//	}
//
//	@Override
//	public void popFragment()
//	{
//		mTabsBackStackManager.popFragment();
//	}
//
//	@Override
//	public boolean isFirstInTab()
//	{
//		boolean result;
//
//		if (isTabsContainerFullyVisible())
//		{
//			if (mTabsBackStackManager.isCurrentTabBackStackEmpty())
//			{
//				scrollByVisibility();
//			}
//
//			result = false;
//		}
//		else
//		{
//			result = true;
//		}
//
//		return result;
//	}
//
//	@Override
//	public void onTabRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults)
//	{
//		mTabsBackStackManager.onTabRequestPermissionsResult(requestCode, permissions, grantResults);
//	}
//
//	@Override
//	public void onFinishInit()
//	{
//		mShouldScroll = true;
//	}
//
//	@Override
//	public void onTabSelected(final String iTabName)
//	{
//		if (mShouldScroll)
//		{
//			mSvRoot.fullScroll(View.FOCUS_DOWN);
//		}
//	}
//
//	@Override
//	public void onTabUnselected(final String iTabName)
//	{
//	}
//
//	@Override
//	public void onTabReselected(final String iTabName)
//	{
//		scrollByVisibility();
//	}
//
//	@Override
//	public void onTabStackAdd(final String iTabName)
//	{
//	}
//
//	@Override
//	public void onTabStackPop(final String iTabName)
//	{
//	}
//
//	private void scrollByVisibility()
//	{
//		if (mShouldScroll)
//		{
//			mSvRoot.fullScroll(isTabsContainerFullyVisible() ? View.FOCUS_UP : View.FOCUS_DOWN);
//		}
//	}
//
//	private boolean isTabsContainerFullyVisible()
//	{
//		boolean result = false;
//
//		Rect visibleRect = AppUtils.getVisibleRectOfView(mFlTabsContainer);
//
//		if (visibleRect != null)
//		{
//			result = (visibleRect.bottom - visibleRect.top) == (mFlTabsContainer.getBottom() - mFlTabsContainer.getTop());
//			result &= visibleRect.bottom <= getActivity().getWindow().getWindowManager().getDefaultDisplay().getHeight();
//		}
//
//		return result;
//	}
//
//	@Override
//	public void onActivityResult(int iRequestCode, int iResultCode, Intent iData)
//	{
//		mTabsBackStackManager.onActivityResult(iRequestCode, iResultCode, iData);
//	}
//}
