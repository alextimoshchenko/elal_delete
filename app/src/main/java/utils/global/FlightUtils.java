package utils.global;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import webServices.global.ePnrFlightType;
import webServices.responses.responseGetUserActivePNRs.PnrFlight;
import webServices.responses.responseGetUserActivePNRs.UserActivePnr;

import static utils.global.AppUtils.eFlightStatus;
import static utils.global.AppUtils.eFlightStatus.AfterBackToOrigination;
import static utils.global.AppUtils.eFlightStatus.DepartedNotArrivedToDestination;
import static utils.global.AppUtils.eFlightStatus.LessThan48HoursToDeparture;
import static utils.global.AppUtils.eFlightStatus.LessThanDayToDeparture;
import static utils.global.AppUtils.eFlightStatus.LessThanDayToOrigination;
import static utils.global.AppUtils.eFlightStatus.MoreThan48HoursToDeparture;
import static utils.global.AppUtils.eFlightStatus.MoreThanDayToOrigination;

/**
 * Created by erline.katz on 28/08/2017.
 */

public class FlightUtils
{
	public static eFlightStatus getFlightStatusByDestination(final UserActivePnr iUserActivePnr)
	{
		eFlightStatus result = eFlightStatus.Error;
		ePnrFlightType direction;
		
		PnrFlight currentFlight = iUserActivePnr.getCurrentFlight();
		
		Date currentFlightDepartureDate = null;
		Date currentFlightArrivalDate = null;
		
		String arrivalOffset = "0";
		
		if (currentFlight != null) // departure flight
		{
			
			/*****/
			//				Calendar calendar = new GregorianCalendar(TimeZone.getTimeZone("GMT" + currentFlight.getTimezoneOffset() + ":00"));// Calendar.getInstance();
			//				calendar.setTimeZone(TimeZone.getTimeZone("GMT" + currentFlight.getTimezoneOffset() + ":00"));
			Date currentDate = DateTimeUtils.getCurrentDateByGMT(currentFlight.getmDepartureTimezoneOffset());
			
			/*****/
			
			
			direction = currentFlight.geteNumDirection();
			
			if (direction == null)
			{
				direction = iUserActivePnr.getFlightDirectionByFlightNum(currentFlight.getFlightNumber());
			}
			//			direction = ePnrFlightType.DEPARTURE;
			
			if (direction == ePnrFlightType.DEPARTURE && iUserActivePnr.getDeparturePNRFlights() != null && !iUserActivePnr.getDeparturePNRFlights().isEmpty())
			{
				currentFlightDepartureDate = iUserActivePnr.getDeparturePNRFlights().get(0).getDepartureDate();
				currentFlightArrivalDate = iUserActivePnr.getDeparturePNRFlights().get(0).getArrivalDate();
				
				arrivalOffset = iUserActivePnr.getDeparturePNRFlights().get(0).getmArrivalTimezoneOffset();
				
				if (iUserActivePnr.getDeparturePNRFlights().size() > 1)
				{
					currentFlightArrivalDate = iUserActivePnr.getDeparturePNRFlights().get(1).getArrivalDate();
					arrivalOffset = iUserActivePnr.getDeparturePNRFlights().get(1).getmArrivalTimezoneOffset();
				}
			}
			else // return flight
			{
				//				direction = ePnrFlightType.RETURN;
				
				if (iUserActivePnr.getReturnPNRFlights() != null && !iUserActivePnr.getReturnPNRFlights().isEmpty())
				{
					currentFlightDepartureDate = iUserActivePnr.getReturnPNRFlights().get(0).getDepartureDate();
					currentFlightArrivalDate = iUserActivePnr.getReturnPNRFlights().get(0).getArrivalDate();
					
					arrivalOffset = iUserActivePnr.getReturnPNRFlights().get(0).getmArrivalTimezoneOffset();
					
					if (iUserActivePnr.getReturnPNRFlights().size() > 1)
					{
						currentFlightArrivalDate = iUserActivePnr.getReturnPNRFlights().get(1).getArrivalDate();
						arrivalOffset = iUserActivePnr.getReturnPNRFlights().get(1).getmArrivalTimezoneOffset();
					}
				}
			}
			
			if (currentFlightDepartureDate != null && currentFlightArrivalDate != null)
			{
				currentFlightDepartureDate = DateTimeUtils.convertDateToDateIgnoreGMT(currentFlightDepartureDate);
				currentFlightArrivalDate = DateTimeUtils.convertDateToDateIgnoreGMT(currentFlightArrivalDate);
				//				currentFlightArrivalDate = DateTimeUtils.convertDateToDateWithGMT(currentFlightArrivalDate, arrivalOffset);
				
				if (direction == ePnrFlightType.RETURN) // return flight
				{
					Integer returnFlightDepartureDateTimeDifferenceInHours = DateTimeUtils.getTimeDifferenceInHoursOrNull(currentDate, currentFlightDepartureDate);
					
					if (returnFlightDepartureDateTimeDifferenceInHours > DateTimeUtils.HOURS_OF_A_DAY)
					{
						result = MoreThanDayToOrigination;
					}
					else
					{
						result = LessThanDayToOrigination;
						
						currentDate = DateTimeUtils.getCurrentDateByGMT(arrivalOffset);
						//						currentFlightArrivalDate = DateTimeUtils.getDateByGMT(currentFlightArrivalDate, arrivalOffset);
						Integer returnFlightArrivalDateTimeDifferenceInHours = DateTimeUtils.getTimeDifferenceInHoursOrNull(currentDate, currentFlightArrivalDate);
						
						if (returnFlightArrivalDateTimeDifferenceInHours <= 0)
						{
							result = AfterBackToOrigination;
						}
					}
				}
				else // departure to destination
				{
					// currentDate = DateTimeUtils.convertDateToDateIgnoreGMT(currentDate);
					
					Integer currentFlightDepartureDateTimeDifferenceInHours = DateTimeUtils.getTimeDifferenceInHoursOrNull(currentDate, currentFlightDepartureDate);
					
					if (currentFlightDepartureDateTimeDifferenceInHours > DateTimeUtils.HOURS_OF_A_DAY * 2)
					{
						result = MoreThan48HoursToDeparture;
					}
					else
					{
						result = LessThan48HoursToDeparture;
						
						if (currentFlightDepartureDateTimeDifferenceInHours < DateTimeUtils.HOURS_OF_A_DAY)
						{
							result = LessThanDayToDeparture;
							
							if (currentFlightDepartureDateTimeDifferenceInHours < 0)
							{
								result = DepartedNotArrivedToDestination;
								
								// currentDate = DateTimeUtils.getCurrentDateByGMT(currentFlight.getmDepartureTimezoneOffset()) ;
								
								// Integer currentFlightArrivalDateTimeDifferenceInHours = DateTimeUtils.getTimeDifferenceInHoursOrNull(DateTimeUtils.getCurrentDateByGMT(arrivalOffset)/*currentDate*/, currentFlightArrivalDate);
								Integer currentFlightArrivalDateTimeDifferenceInMinutes = DateTimeUtils.getTimeDifferenceInMinutesOrNull(DateTimeUtils.getCurrentDateByGMT(arrivalOffset), currentFlightArrivalDate);
								
								if (currentFlightArrivalDateTimeDifferenceInMinutes </*=*/ 0) // Arrived to destination
								{
									result = MoreThanDayToOrigination;
								}
							}
						}
					}
				}
			}
			else
			{
				result = eFlightStatus.Error;
			}
		}
		return result;
	}
	
	
	//	public static AppUtils.eFlightStatus getFlightStatusByDestination(final DestinationListItem iCurrentDestination, DestinationListItem iNextDestination)
	//	{
	//		AppUtils.eFlightStatus result = AppUtils.eFlightStatus.Error;
	//
	//		if (iCurrentDestination != null)
	//		{
	//			Date currentFlightDepartureDate = null;
	//			Date currentFlightArrivalDate = null;
	//
	//			if (iCurrentDestination != null)
	//			{
	//				currentFlightDepartureDate = iCurrentDestination.getmVia() != null ? iCurrentDestination.getmVia().getDepartureDate() : iCurrentDestination.getmDestination().getDepartureDate();
	//				currentFlightArrivalDate = iCurrentDestination.getmDestination().getArrivalDate();
	//			}
	//
	//			if (currentFlightDepartureDate != null && currentFlightArrivalDate != null)
	//			{
	//				if (iCurrentDestination.getmFlightType() == ePnrFlightType.RETURN) // return flight
	//				{
	//					Date currentDate = new Date();
	//					Integer returnFlightDepartureDateTimeDifferenceInHours = DateTimeUtils.getTimeDifferenceInHoursOrNull(currentDate, currentFlightDepartureDate);
	//
	//					if (returnFlightDepartureDateTimeDifferenceInHours > DateTimeUtils.HOURS_OF_A_DAY)
	//					{
	//						result = MoreThanDayToOrigination;
	//					}
	//					else
	//					{
	//						result = LessThanDayToOrigination;
	//
	//						Integer returnFlightArrivalDateTimeDifferenceInHours = DateTimeUtils.getTimeDifferenceInHoursOrNull(currentDate, currentFlightArrivalDate);
	//						if (returnFlightArrivalDateTimeDifferenceInHours <= 0)
	//						{
	//							result = AfterBackToOrigination;
	//						}
	//					}
	//				}
	//				else // departure to destination
	//				{
	//					Date currentDate = new Date();
	//					Integer currentFlightDepartureDateTimeDifferenceInHours = DateTimeUtils.getTimeDifferenceInHoursOrNull(currentDate, currentFlightDepartureDate);
	//
	//					if (currentFlightDepartureDateTimeDifferenceInHours > DateTimeUtils.HOURS_OF_A_DAY * 2)
	//					{
	//						result = MoreThan48HoursToDeparture;
	//					}
	//					else
	//					{
	//						result = LessThan48HoursToDeparture;
	//
	//						if (currentFlightDepartureDateTimeDifferenceInHours < DateTimeUtils.HOURS_OF_A_DAY)
	//						{
	//							result = LessThanDayToDeparture;
	//
	//							if (currentFlightDepartureDateTimeDifferenceInHours < 0)
	//							{
	//								result = DepartedNotArrivedToDestination;
	//
	//								Integer currentFlightArrivalDateTimeDifferenceInHours = DateTimeUtils.getTimeDifferenceInHoursOrNull(currentDate, currentFlightArrivalDate);
	//								if (currentFlightArrivalDateTimeDifferenceInHours <= 0) // Arrived to destination
	//								{
	//									result = MoreThanDayToOrigination;
	//								}
	//							}
	//						}
	//					}
	//				}
	//			}
	//			else
	//			{
	//				result = AppUtils.eFlightStatus.Error;
	//			}
	//		}
	//		return result;
	//	}
	//
	
	public static UserActivePnr getActiveFlightForNext48Houres(ArrayList<UserActivePnr> iUserActivePnrsList)
	{
		for (UserActivePnr flight : iUserActivePnrsList)
		{
			if (flight != null && flight.getDeparturePNRFlights() != null)
			{
				if ((flight.getDeparturePNRFlights() != null && flight.getDeparturePNRFlights().size() > 0 && flight.getDeparturePNRFlights()
				                                                                                                    .get(0)
				                                                                                                    .isActive()) && (flight.getReturnPNRFlights() != null && flight.getReturnPNRFlights()
				                                                                                                                                                                   .size() > 0 && flight
						.getReturnPNRFlights()
						.get(0)
						.isActive()))
				{
					Date currentDate = new Date();
					currentDate = DateTimeUtils.convertDateToDateIgnoreGMT(currentDate);
					
					Date departure = flight.getDeparturePNRFlights().get(0).getDepartureDate();
					Integer departureDateTimeDifferenceInHours = DateTimeUtils.getTimeDifferenceInHoursOrNull(currentDate, departure);
					
					if (departureDateTimeDifferenceInHours < DateTimeUtils.HOURS_OF_A_DAY * 2)
					{
						Date returnDate = flight.getReturnPNRFlights().get(0).getArrivalDate();
						Integer returnDateTimeDifferenceInHours = DateTimeUtils.getTimeDifferenceInHoursOrNull(currentDate, returnDate);
						if (returnDateTimeDifferenceInHours > 0)
						{
							return flight;
						}
					}
				}
			}
		}
		return null;
	}
	
	public static UserActivePnr getActiveFlight(ArrayList<UserActivePnr> iUserActivePnrsList)
	{
		for (UserActivePnr flight : iUserActivePnrsList)
		{
			if (flight.getDeparturePNRFlights() != null && flight.getDeparturePNRFlights().size() > 0)
			{
				for (PnrFlight pnrFlight : flight.getDeparturePNRFlights())
				{
					if (pnrFlight.isActive())
					{
						return flight;
					}
				}
			}
			
			if (flight.getReturnPNRFlights() != null && flight.getReturnPNRFlights().size() > 0)
			{
				for (PnrFlight pnrFlight : flight.getReturnPNRFlights())
				{
					if (pnrFlight.isActive())
					{
						return flight;
					}
				}
			}
		}
		return null;
	}
	
	public static UserActivePnr getActiveFlightFromList(ArrayList<UserActivePnr> iUserActivePnrsList, String iFlightPNR)
	{
		for (UserActivePnr pnr : iUserActivePnrsList)
		{
			if (pnr.getPnr().toLowerCase().equals(iFlightPNR.toLowerCase()))
			{
				return pnr;
			}
		}
		return null;
	}
	
	
}
