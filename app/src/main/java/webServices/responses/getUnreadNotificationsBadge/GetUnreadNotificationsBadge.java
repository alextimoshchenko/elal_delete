package webServices.responses.getUnreadNotificationsBadge;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with care by Alexey.T on 21/08/2017.
 */
public class GetUnreadNotificationsBadge
{
	@JsonProperty("UnReadPushes")
	private int mUnReadPushes;
	
	public GetUnreadNotificationsBadge()
	{
	}
	
	public int getUnReadPushes()
	{
		return mUnReadPushes;
	}
}
