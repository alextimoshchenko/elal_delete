package webServices.responses.getMobileCountryCodes;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MobileCountryCode
{
	@JsonProperty("MobCountryCode")
	private String mCountryCode;
	@JsonProperty("CountryName")
	private String mCountryName;
	@JsonProperty("Prefix")
	private String mPrefix;
	
	public MobileCountryCode()
	{
	}
	
	public String getCountryCode()
	{
		return mCountryCode;
	}
	
	public String getCountryName()
	{
		return mCountryName;
	}
	
	public String getPrefix()
	{
		return mPrefix;
	}
	
}
