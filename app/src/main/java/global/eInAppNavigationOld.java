package global;

import ui.activities.MyFlightsActivity;
import ui.activities.NotificationActivity;
import ui.activities.SearchFlightActivity;
import ui.fragments.ChangePasswordFragment;
import ui.fragments.details.DetailScreenFragment;
import ui.fragments.FamilyCardFragment;
import ui.fragments.ForgotPasswordFragment;
import ui.fragments.HomeFragment;
import ui.fragments.LoginFragment;
import ui.fragments.MyDocumentsFragment;
import ui.fragments.RegistrationFragment;
import ui.fragments.SettingsFragment;
import ui.fragments.WebViewFragment;


/**
 * Created with care by Shahar Ben-Moshe on 08/01/17.
 */

public enum eInAppNavigationOld
{
	Error(0, null, false),
	
	//Login module
	Login(100, LoginFragment.class, false),
	Registration(110, RegistrationFragment.class, false),
	FamilyScreen(130, FamilyCardFragment.class, true),
	
	//ForgotPassword(160, ForgotPasswordActivity.class, false),
	ForgotPassword(160, ForgotPasswordFragment.class, false),
	ChangePassword(230, ChangePasswordFragment.class, true),
	
	//Main module
	Main(170, HomeFragment.class, false),
	
	//MyFlights module
	MyFlights(180, MyFlightsActivity.class, false),
	
	Settings(210, SettingsFragment.class, false),
	
	UserInfo(220, DetailScreenFragment.class, true),
	MyDocuments(250, MyDocumentsFragment.class, true),
	
	//Search module
	FlightSearch(260, SearchFlightActivity.class, false),
	
	//Notification module
	Notifications(999, NotificationActivity.class, false),
	
	AddNewFlightFragment(99, ui.fragments.AddNewFlightFragment.class, false),
	
	//WebView
	WebView(9999, WebViewFragment.class, false);
	
	private final int mNavigationId;
	private final Class mNavigationClass;
	private boolean mPersonal;
	
	eInAppNavigationOld(final int iNavigationId, final Class iNavigationClass, final boolean iPersonal)
	{
		mNavigationId = iNavigationId;
		mNavigationClass = iNavigationClass;
		mPersonal = iPersonal;
	}
	
	public int getNavigationId()
	{
		return mNavigationId;
	}
	
	public Class getNavigationClass()
	{
		return mNavigationClass;
	}
	
	public static eInAppNavigationOld getInAppNavigationByIdOrErrorDefault(int iNavigationId)
	{
		eInAppNavigationOld result = Error;
		
		for (eInAppNavigationOld inAppNavigation : values())
		{
			if (inAppNavigation != null && inAppNavigation.getNavigationId() == iNavigationId)
			{
				result = inAppNavigation;
				break;
			}
		}
		
		return result;
	}
	
	public boolean isWebView()
	{
		return eInAppNavigationOld.this == WebView;
	}
	
	public boolean isChangePassword()
	{
		return eInAppNavigationOld.this == ChangePassword;
	}
	
	public boolean isFamilyScreen()
	{
		return eInAppNavigationOld.this.getNavigationId() == FamilyScreen.getNavigationId();
	}
	
	public boolean isPersonal()
	{
		return mPersonal;
	}
	
	// This method works but before use it we need to solve navigation problems, because we need to separate all to moduls , now for swipe we in some cases we are using fragents and in some cases
	// activities, but we need to in all cases use activities
	public static int getClassIdByClass(Class iClass)
	{
		int result = -1;
		
		if (iClass != null)
		{
			String currentClassName = iClass.getSimpleName();
			eInAppNavigationOld[] navigations = values();
			
			for (eInAppNavigationOld tmpNav : navigations)
			{
				Class tpmClass = tmpNav.getNavigationClass();
				
				if (tpmClass != null)
				{
					String className = tmpNav.getNavigationClass().getSimpleName();
					
					if (className.equals(currentClassName))
					{
						result = tmpNav.getNavigationId();
					}
				}
			}
		}
		
		return result;
	}
}
