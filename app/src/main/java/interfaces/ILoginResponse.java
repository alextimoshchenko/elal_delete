package interfaces;

/**
 * Created with care by Shahar Ben-Moshe on 25/05/17.
 */

public interface ILoginResponse<T>
{
	public T getContent();
}
