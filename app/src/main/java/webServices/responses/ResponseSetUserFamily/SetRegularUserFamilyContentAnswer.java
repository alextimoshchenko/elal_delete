package webServices.responses.ResponseSetUserFamily;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SetRegularUserFamilyContentAnswer
{
	@JsonProperty("ID")
	private String mID;
	@JsonProperty("Success")
	private boolean mSuccess;
	
	public String getID()
	{
		return mID;
	}
	
	public boolean isSuccess()
	{
		return mSuccess;
	}
	
	public SetRegularUserFamilyContentAnswer()
	{
	}
}
