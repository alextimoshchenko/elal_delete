package utils.global.enums;

/**
 * Created with care by Alexey.T on 10/07/2017.
 */
public enum eSearchDestinationType
{
	FIND_ORIGIN_DESTINATION,
	FIND_TO_DESTINATION,
	FIND_ADDITIONAL_ORIGIN_DESTINATION,
	FIND_ADDITIONAL_TO_DESTINATION;
	
	public boolean isOriginDestinationType()
	{
		return eSearchDestinationType.this == FIND_ORIGIN_DESTINATION;
	}
	
	public boolean isToDestinationType()
	{
		return eSearchDestinationType.this == FIND_TO_DESTINATION;
	}
	
	public boolean isAdditionalOriginDestinationType()
	{
		return eSearchDestinationType.this == FIND_ADDITIONAL_ORIGIN_DESTINATION;
	}
	
	public boolean isAdditionalToDestinationType()
	{
		return eSearchDestinationType.this == FIND_ADDITIONAL_TO_DESTINATION;
	}
}
