package webServices.global;

import android.text.TextUtils;
import android.util.Log;

import java.util.Date;
import java.util.HashMap;

import global.ElAlApplication;
import global.UserData;
import il.co.ewave.elal.BuildConfig;
import utils.global.AppUtils;
import utils.global.DateTimeUtils;
import utils.global.ElalPreferenceUtils;
import webServices.responses.responseGetUserActivePNRs.Passenger;

/**
 * Created with care by Shahar Ben-Moshe on 13/12/16.
 */

public class RequestStringBuilder
{
	private static final String TAG = RequestStringBuilder.class.getSimpleName();
	//	private static final eEnvironment ENVIRONMENT = BuildConfig.DEBUG ? eEnvironment.Dev : eEnvironment.Prod;
		private static final eEnvironment ENVIRONMENT = eEnvironment.Prod;
//	private static final eEnvironment ENVIRONMENT = eEnvironment.Dev;
	
	public static final String ELAL_CO_IL = "elal.co.il";
	public static final String ELAL_COM = "elal.com";
	public static final String MATMID_INDICATOR = "matmid";
	
	public static final String ELAL_CASE = "ELAL";
	
	
	//region headers
	private static final String PASSWORD = "Password";
	private static final String RBZID = "rbzid";
	private static final String API = "api/";
	//region Web Images
	private static final String PROMOS = "Promos/";
	//endregion
	private static final String HOME_CONTENT = "HomeContents/";
	private static final String APP_CONTENT = "AppContents/";
	private static final String SIDE_MENU_ICONS = "SideMenu/";
	private static final String DESTINATIONS = "Destinations/";
	private static final String WEATHER = "WeatherImages/";
	//region Application
	private static final String APPLICATION = API + "Application/";
	private static final String GET_PROMO = "GetPromo";
	private static final String GET_APP_DATA = "GetAppData";
	private static final String CHECK_APP_VERSION = "CheckAppVersion";
	private static final String GET_HOME_SCREEN_CONTENT = "GetHomeScreenContent";
	private static final String LOGOUT = "Logout";
	private static final String CLEAR_USER_HISTORY = "ClearUserHistory";
	private static final String GET_APP_CONTENTS = "GetAppContents";
	private static final String GET_SIDE_MENU_ITEMS = "GetSideMenuItems";
	private static final String GET_UNREAD_NOTIFICATIONS_BADGE = "GetUnreadNotificationsBadge";
	//endregion
	private static final String GET_TO_SEE_LINK_BY_DESTINATION = "GetToSeeLinkByDestination";
	private static final String GET_WEATHER = "GetWeather";
	//region Lognet
	private static final String LOGNET = API + "Lognet/";
	private static final String RETRIEVE_PNR = "RetrievePNR";
	private static final String PAIR_GROUP_PNR_TO_USER = "PairGroupPNRToUser";
	private static final String GET_USER_DESTINATIONS = "GetUserDestinations";
	//region TeleMessage
	private static final String TELE_MESSAGE = API + "TeleMessage/";
	private static final String SET_PNR = "SetPNR";
	//region AppUsers
	private static final String APP_USERS = API + "AppUsers/";
	private static final String REGISTER = "MobileRegister";
	private static final String LOGIN = "Login";
	private static final String SET_USER_DETAILS = "SetUserDetails";
	private static final String SET_DEVICE = "SetDevice";
	private static final String RESET_PASSWORD = "ResetPassword";
	private static final String CHANGE_PASSWORD = "ChangePassword";
	private static final String GET_USER_SETTINGS = "GetUserSettings";
	private static final String SET_USER_SETTINGS = "SetUserSettings";
	private static final String MATMID = API + "Matmid/";
	static final String MATMID_LOGIN = "MatmidLogin";
	private static final String MatmidGetFamilyMembers = "MatmidGetFamilyMembers";
	private static final String MATMID_UPDATE = "MatmidUpdateMemberData";
	private static final String MATMID_EXTEND_SESSION = "MatmidExtendSession";
	private static final String GET_COUNTRY_CODES = "GetMobileCountryCodes";
	//endregion
	private static final String GET_REPRESENTATION_BY_IP = "GetRepresentationByIP";
	//region Family
	private static final String REGULAR_FAMILY = API + "Family/";
	private static final String SET_REGULAR_USER_FAMILY = "SetUserFamilyMember";
	private static final String SET_MATMID_USER_FAMILY = "MatmidSetFamilyMembers";
	private static final String GET_USER_FAMILY = "GetUserFamily";
	//region Reservation
	private static final String RESERVATION = API + "Reservation/";
	private static final String GET_USER_ACTIVE_PNRS = "GetUserActivePNRs";
	
	//endregion
	private static final String GET_GROUP_PNRS_DATA = "GetGroupPNRsData";
	private static final String GET_MULTIPLE_PNR_DESTINATIONS = "GetMultiplePNRDestinations";
	private static final String GET_USER_PNRS = "GetUserPNRs";
	private static final String GET_PNR_FLIGHTS = "GetPNRFlights";
	//endregion
	private static final String GET_PNR_BY_NUM = "GetPNRByNum";
	private static final String GET_COMPLETE_PNR = "GetCompletePNR";
	
	//region Search
	private static final String SEARCH = API + "Search/";
	private static final String GET_SEARCH_GLOBAL_DATA = "GetSearchGlobalData";
	private static final String GET_SEARCH_HISTORY = "GetSearchHistory";
	private static final String SEARCH_FLIGHT = "SearchFlight";
	private static final String GET_SEARCH_DESTINATION_CITIES = "GetSearchDestiationCities";
	private static final String GET_SEARCH_DEPARTURE_CITIES = "GetSearchDepartureCities";
	//notifications
	private static final String DELETE_USER_PUSH_MESSAGE = "DeleteUserPushMessage";
	private static final String GET_PUSH_NOTIFICATIONS = "GetPushNotifications";
	private static final String SET_PUSH_AS_READ = "SetPushAsRead";
	//region CheckList
	private static final String CHECK_LIST = API + "CheckList/";
	private static final String GET_USER_CHECK_LIST = "GetUserCheckList";
	private static final String SET_USER_CHECK_LIST = "SetUserCheckList";
	private static final String DELETE_USER_CHECK_LIST = "DeleteUserCheckList";
	private static final String DO_CHECK_LIST = "DoCheckList";
	//region web pages urls
	private static final String BASE_ELAL_URL = "https://www.elal.com/";
	private static final String BASE_ELAL_MATMID_URL = "https://www.elal-matmid.com/";
	private static final String FLIGHT_TIMETABLE_HE = BASE_ELAL_URL + "he/PassengersInfo/Useful-Info/Flight-Schedule/Pages/Flight-Updates.aspx?hideheader=true";
	private static final String FLIGHT_TIMETABLE_EN = BASE_ELAL_URL + "en/PassengersInfo/Useful-Info/Flight-Schedule/Pages/Flight-Updates.aspx?hideheader=true";
	private static final String CHANGE_PASSWORD_HE = BASE_ELAL_MATMID_URL + "he/MyAccount/Pages/Change-Passwords.aspx";
	private static final String CHANGE_PASSWORD_EN = BASE_ELAL_MATMID_URL + "en/MyAccount/Pages/Change-Password.aspx";
	private static final String MATMID_URL_HE = BASE_ELAL_MATMID_URL + "he/Pages/HomePage.aspx";
	private static final String MATMID_URL_EN = BASE_ELAL_MATMID_URL + "en/Pages/HomePage.aspx";
	private static final String MATMID_LOCKED_URL_HE = BASE_ELAL_MATMID_URL + "he/Login/Pages/FailedPageLogin.aspx";
	private static final String MATMID_LOCKED_URL_EN = BASE_ELAL_MATMID_URL + "en/Login/Pages/FailedPageLogin.aspx";
	private static final String FORGOT_MATMID_NUMBER_URL_HE = BASE_ELAL_MATMID_URL + "he/Login/Pages/ForgotIdNumber.aspx";
	private static final String FORGOT_MATMID_NUMBER_URL_EN = BASE_ELAL_MATMID_URL + "en/Login/Pages/ForgotIdNumber.aspx";
	//end region
	private static final String FORGOT_PASSWORD_MATMID_URL_HE = BASE_ELAL_MATMID_URL + "he/Login/Pages/ForgotPassword.aspx";
	private static final String FORGOT_PASSWORD_MATMID_URL_EN = BASE_ELAL_MATMID_URL + "en/Login/Pages/ForgotPassword.aspx";
	private static final String MATMID_CHANGE_PASSWORD_HE = BASE_ELAL_MATMID_URL + "he/Login/Pages/ChangePassword.aspx";
	private static final String MATMID_CHANGE_PASSWORD_EN = BASE_ELAL_MATMID_URL + "en/Login/Pages/ChangePassword.aspx";
	private static final String SAVE_BOARDING_PASS_HE = "https://www.elal.com/checkin/home/identification/B?language=heb&isNative=true";
	private static final String SAVE_BOARDING_PASS_EN = "https://www.elal.com/checkin/home/identification/b?language=eng&isNative=true";
	private static final String CANCEL_CHECK_IN_HE = BASE_ELAL_URL + "checkin/home/identification/O?language=heb&isNative=true";
	private static final String CANCEL_CHECK_IN_EN = BASE_ELAL_URL + "checkin/home/identification/O?language=eng&isNative=true";
	
	
	//end region
	private static final String CHECK_IN_HE = BASE_ELAL_URL + "checkin/home/identification/c?language=heb&type=0&isnative=true";
	private static final String CHECK_IN_EN = BASE_ELAL_URL + "checkin/home/identification/c?language=eng&type=0&isnative=true";
	
	
	private static final String ACCOUNT_STATUS_HE = BASE_ELAL_MATMID_URL + "he/MyAccount/Pages/myAccount.aspx";
	private static final String ACCOUNT_STATUS_EN = BASE_ELAL_MATMID_URL + "en/MyAccount/Pages/Account-Balance.aspx";
	private static final String ADDING_MISSING_FLIGHT_HE = BASE_ELAL_MATMID_URL + "he/MyReservations/Pages/MyFlights.aspx";
	private static final String ADDING_MISSING_FLIGHT_EN = BASE_ELAL_MATMID_URL + "en/MyReservations/Pages/MyFlights.aspx";
	
	private static final String NEWSLETTER_HE = BASE_ELAL_URL + "he/DealsAndPackages/Hot-Deals/Pages/SubscribersCampaign.aspx";
	private static final String NEWSLETTER_EN = BASE_ELAL_URL + "en/Deals/Offers/Hot-Deals/Pages/SubscribersCampaign.aspx";
	private static final String CHANGE_SEAT_MEAL_HE = "https://booking.elal.co.il/newBooking/changeOrder.do?LANG=IL";
	private static final String CHANGE_SEAT_MEAL_EN = "https://booking.elal.co.il/newBooking/changeOrder.do?LANG=EN";
	private static final String PERFORM_CHECK_IN_MD5_HE = BASE_ELAL_URL + "checkin/home/identification/c?language=heb&type=0&isnative=true";
	private static final String PERFORM_CHECK_IN_MD5_EN = BASE_ELAL_URL + "checkin/home/identification/c?language=eng&type=0&isnative=true";
	
	//end region
	private static final String ABOUT_PASSENGERS_TYPES_EN = BASE_ELAL_URL + "EN/about-ELAL/Legal/Pages/Age-Definitions.aspx?hideheader=true";
	private static final String ABOUT_PASSENGERS_TYPES_HE = BASE_ELAL_URL + "he/about-ELAL/Legal/Pages/Age-Definitions.aspx?hideheader=true";
	private static final String ELAL_MATMID_LOGIN = BASE_ELAL_MATMID_URL + "he/Login/Pages/Login.aspx";
	private static final String ELAL_TERMS_OF_USE_HE = BASE_ELAL_URL + "he/about-ELAL/Legal/Pages/ELAL-Conditions.aspx?hideheader=true";
	private static final String ELAL_TERMS_OF_USE_EN = BASE_ELAL_URL + "en/about-ELAL/Legal/Pages/ELAL-Conditions.aspx?hideheader=true";
	
	//region other web urls
	private static final String YOUTUBE_BASE_URL = "https://www.youtube.com/embed/";
	
	private static final String MATMID_CA_URL = BASE_ELAL_MATMID_URL + "c3650cdf-216a-4ba2-80b0-9d6c540b105e58d2670b-ea0f-484e-b88c-0e2c1499ec9bd71e4b42-8570-44e3-89b6-845326fa43b6";
	public static final CharSequence MATMID_BASE_COM = "elal-matmid.com";
	public static final CharSequence MATMID_BASE_CO_IL = "elal-matmid.co.il";
	
	//Utm
	private static final String UTM_CHECK_IN = "&utm_source=elal&utm_medium=application&utm_content=checkin&utm_campaign=general"/*"&utm_source=App&utm_medium=Checkin&utm_campaign=general"*/;
	private static final String UTM_CANCEL_CHECK_IN = "&utm_source=elal&utm_medium=application&utm_content=cancel_checkin&utm_campaign=general"/*"&utm_source=App&utm_medium=Cancel_Checkin&utm_campaign=general"*/;
	private static final String UTM_MATMID_ACCOUNT_STATUS = "?utm_source=elal&utm_medium=application&utm_content=account_balance&utm_campaign=general"/*"?utm_source=App&utm_medium=Account-Balance&utm_campaign=general"*/;
	private static final String UTM_MATMID_ADD_MISSING_FLIGHT = "?utm_source=elal&utm_medium=application&utm_content=add_missing_flight&utm_campaign=general"/*"?utm_source=App&utm_medium=Add_Missing_Flight&utm_campaign=general"*/;
	//	private static final String UTM_NEWSLETTER = "?utm_source=App&utm_medium=Newsletter&utm_campaign=general"/*"&utm_source=App&utm_medium=OS&utm_campaign=Newsletter&utm_content="*/;
	private static final String UTM_NEWSLETTER = "?utm_source=elal&utm_medium=application&utm_content=newsletter&utm_campaign=general"/*"?utm_source=App&utm_medium=sidemenu"*/;
	
	private static final String UTM_SEAT_MEAL = "&utm_source=elal&utm_medium=application&utm_content=change_seat_or_meal&utm_campaign=general"/*"&utm_source=App&utm_medium=ChangeSeatOrMeal&utm_campaign=general"*/;
	//	private static final String UTM_TERM_OF_USE = "&utm_source=App&utm_medium=Terms&utm_campaign=general"/*"&utm_source=App&utm_medium=OS&utm_campaign=Terms&utm_content="*/;
	private static final String UTM_TERM_OF_USE = "?utm_source=elal&utm_medium=application&utm_content=terms&utm_campaign=general"/*"?utm_source=App&utm_medium=sidemenu"*/;
	private static final String UTM_FLIGHTS_TIMETABLE = "&utm_source=elal&utm_medium=application&utm_content=flights_timetable&utm_campaign=general"/*"&utm_source=App&utm_medium=Flights-Timetable&utm_campaign=general"*/;
	public static final String UTM_SEARCH_FLIGHT = "&utm_source=elal&utm_medium=application&utm_content=flight_search&utm_campaign=general"/*"&utm_source=App&utm_medium=SearchFlight&utm_campaign=general"*/;
	public static final String UTM_TICKET_MONEY_POINT = "&utm_source=elal&utm_medium=application&utm_content=search_flight_ticket_money_and_points&utm_campaign=general"/*"&utm_source=App&utm_medium=SearchFlightTicketMoneyAndPoints&utm_campaign=general"*/;
	public static final String UTM_BONUS_TICKET = "&utm_source=elal&utm_medium=application&utm_content=search_flight_bonus_ticket&utm_campaign=general"/*"&utm_source=App&utm_medium=SearchFlightBonusTicket&utm_campaign=general"*/;
	public static final String UTM_CHANGE_PASSWORD = "?utm_source=elal&utm_medium=application&utm_content=matmid_change_password&utm_campaign=general"/*"?utm_source=App&utm_medium=MatmidChangePassword&utm_campaign=&general"*/;
	
	public static String getMatmidCaUrl()
	{
		return MATMID_CA_URL;
	}
	
	public static HashMap<String, String> createPasswordHeader(String iPassword)
	{
		HashMap<String, String> result = new HashMap<>();
		
		if (!TextUtils.isEmpty(iPassword))
		{
			result.put(PASSWORD, iPassword);
		}
		
		return result;
	}
	
	public static HashMap<String, String> createMatmidHeaders(String iPassword, String iToken)
	{
		HashMap<String, String> result = new HashMap<>();
		
		if (!TextUtils.isEmpty(iPassword))
		{
			result.put(PASSWORD, iPassword);
		}
		
		if (!TextUtils.isEmpty(iToken))
		{
			result.put(RBZID, iToken);
		}
		
		return result;
	}
	
	public static String getEnvironmentName()
	{
		return ENVIRONMENT == eEnvironment.Prod ? "" : ENVIRONMENT.name();
	}
	
	public static String getPromoImageUrl(String iPromoImage)
	{
		return ENVIRONMENT.getFilesBaseUrl() + PROMOS + iPromoImage;
	}
	
	public static String getHomeContentImageUrl(final String iImageUrl)
	{
		String result = "";
		
		if (!TextUtils.isEmpty(iImageUrl))
		{
			result = ENVIRONMENT.getBaseUrl() + HOME_CONTENT + iImageUrl;
		}
		
		return result;
	}
	
	public static String getAppContentsImageUrl(final String iImageUrl)
	{
		String result = "";
		
		if (!TextUtils.isEmpty(iImageUrl))
		{
			result = ENVIRONMENT.getFilesBaseUrl() + APP_CONTENT + iImageUrl;
			result = result.replaceAll(" ", "%20");
		}
		
		return result;
	}
	
	public static String getSideMenuImageUrl(final String iImageUrl)
	{
		String result = "";
		
		if (!TextUtils.isEmpty(iImageUrl))
		{
			result = ENVIRONMENT.getFilesBaseUrl() + SIDE_MENU_ICONS + iImageUrl;
		}
		
		return result;
	}
	
	public static String getDestinationImageUrl(final String iImageUrl)
	{
		String result = "";
		
		if (!TextUtils.isEmpty(iImageUrl))
		{
			result = ENVIRONMENT.getFilesBaseUrl() + DESTINATIONS + iImageUrl;
		}
		
		return result;
	}
	
	public static String getWeatherImageUrl(final String iImageUrl)
	{
		String result = "";
		
		if (!TextUtils.isEmpty(iImageUrl))
		{
			result = ENVIRONMENT.getFilesBaseUrl() + WEATHER + iImageUrl;
		}
		
		return result;
	}
	
	public static String getGetHomeScreenContentUrl()
	{
		return ENVIRONMENT.getBaseUrl() + APPLICATION + GET_HOME_SCREEN_CONTENT;
	}
	
	public static String getLogoutUrl()
	{
		return ENVIRONMENT.getBaseUrl() + APP_USERS + LOGOUT;
	}
	
	public static String getClearUserHistoryUrl()
	{
		return ENVIRONMENT.getBaseUrl() + SEARCH + CLEAR_USER_HISTORY;
	}
	
	//endregion
	
	public static String getGetAppContentsUrl()
	{
		return ENVIRONMENT.getBaseUrl() + APPLICATION + GET_APP_CONTENTS;
	}
	
	public static String getPromoUrl()
	{
		return ENVIRONMENT.getBaseUrl() + APPLICATION + GET_PROMO;
	}
	
	public static String getCheckAppVersionUrl()
	{
		return ENVIRONMENT.getBaseUrl() + APPLICATION + CHECK_APP_VERSION;
	}
	
	public static String getGetAppDataUrl()
	{
		return ENVIRONMENT.getBaseUrl() + APPLICATION + GET_APP_DATA;
	}
	
	public static String getGetSideMenuItemsUrl()
	{
		return ENVIRONMENT.getBaseUrl() + APPLICATION + GET_SIDE_MENU_ITEMS;
	}
	
	public static String getUnreadNotificationsBadge()
	{
		return ENVIRONMENT.getBaseUrl() + APP_USERS + GET_UNREAD_NOTIFICATIONS_BADGE;
	}
	
	public static String getGetToSeeLinkByDestinationUrl()
	{
		return ENVIRONMENT.getBaseUrl() + APPLICATION + GET_TO_SEE_LINK_BY_DESTINATION;
	}
	
	public static String getWeatherUrl()
	{
		return ENVIRONMENT.getBaseUrl() + APPLICATION + GET_WEATHER;
	}
	
	public static String getRetrievePnrUrl()
	{
		return ENVIRONMENT.getBaseUrl() + LOGNET + RETRIEVE_PNR;
	}
	
	public static String getPairGroupPnrToUserUrl()
	{
		return ENVIRONMENT.getBaseUrl() + LOGNET + PAIR_GROUP_PNR_TO_USER;
	}
	
	//endregion
	
	public static String getGetUserDestinationsUrl()
	{
		return ENVIRONMENT.getBaseUrl() + LOGNET + GET_USER_DESTINATIONS;
	}
	
	public static String getSetPnrUrl()
	{
		return ENVIRONMENT.getBaseUrl() + TELE_MESSAGE + SET_PNR;
	}
	
	public static String getGetRepresentationByIpUrl()
	{
		return ENVIRONMENT.getBaseUrl() + APP_USERS + GET_REPRESENTATION_BY_IP;
	}
	
	public static String getRegisterUrl()
	{
		return ENVIRONMENT.getBaseUrl() + APP_USERS + REGISTER;
	}
	
	public static String getLoginUrl()
	{
		return ENVIRONMENT.getBaseUrl() + APP_USERS + LOGIN;
	}
	
	public static String getMatmidLoginUrl()
	{
		//		return ENVIRONMENT.getBaseUrl() + MATMID + MATMID_LOGIN;
		return ENVIRONMENT.getMatmidBaseUrl() + MATMID + MATMID_LOGIN;
	}
	
	public static String getMatmidKeepAliveUrl()
	{
		return ENVIRONMENT.getMatmidBaseUrl() + MATMID + MATMID_EXTEND_SESSION;
	}
	
	public static String getSetUserDetailsUrl()
	{
		return ENVIRONMENT.getBaseUrl() + APP_USERS + SET_USER_DETAILS;
	}
	
	public static String getSetUserMatmidDetailsUrl()
	{
		return ENVIRONMENT.getMatmidBaseUrl() + MATMID + MATMID_UPDATE;
	}
	
	public static String getSetDeviceUrl()
	{
		return ENVIRONMENT.getBaseUrl() + APP_USERS + SET_DEVICE;
	}
	
	public static String getResetPasswordUrl()
	{
		return ENVIRONMENT.getBaseUrl() + APP_USERS + RESET_PASSWORD;
	}
	
	public static String getChangePasswordUrl()
	{
		//		return ENVIRONMENT.getBaseUrl() + APP_USERS + CHANGE_PASSWORD + UTM_CHANGE_PASSWORD;
		return ENVIRONMENT.getBaseUrl() + APP_USERS + CHANGE_PASSWORD;
	}
	
	public static String getUserSettingsUrl()
	{
		return ENVIRONMENT.getBaseUrl() + APP_USERS + GET_USER_SETTINGS;
	}
	
	public static String getSetUserSettingsUrl()
	{
		return ENVIRONMENT.getBaseUrl() + APP_USERS + SET_USER_SETTINGS;
	}
	
	public static String getSetRegularUserFamilyUrl()
	{
		return ENVIRONMENT.getBaseUrl() + REGULAR_FAMILY + SET_REGULAR_USER_FAMILY;
	}
	
	public static String getSetMatmidUserFamilyUrl()
	{
		return ENVIRONMENT.getMatmidBaseUrl() + MATMID + SET_MATMID_USER_FAMILY;
	}
	
	public static String getUserFamilyUrl()
	{
		return ENVIRONMENT.getBaseUrl() + REGULAR_FAMILY + GET_USER_FAMILY;
	}
	
	public static String getUserActivePnrsUrl()
	{
		return ENVIRONMENT.getBaseUrl() + RESERVATION + GET_USER_ACTIVE_PNRS;
	}
	
	public static String getGetGroupPNRsDataUrl()
	{
		return ENVIRONMENT.getBaseUrl() + RESERVATION + GET_GROUP_PNRS_DATA;
	}
	
	public static String getMultiplePnrDestinationsUrl()
	{
		return ENVIRONMENT.getBaseUrl() + RESERVATION + GET_MULTIPLE_PNR_DESTINATIONS;
	}
	
	public static String getUserPnrsUrl()
	{
		return ENVIRONMENT.getBaseUrl() + RESERVATION + GET_USER_PNRS;
	}
	
	public static String getPnrFlightsUrl()
	{
		return ENVIRONMENT.getBaseUrl() + RESERVATION + GET_PNR_FLIGHTS;
	}
	
	public static String getPnrByNum()
	{
		return ENVIRONMENT.getBaseUrl() + RESERVATION + GET_PNR_BY_NUM;
	}
	
	public static String getCompletePNRUrl()
	{
		return ENVIRONMENT.getBaseUrl() + RESERVATION + GET_COMPLETE_PNR;
	}
	
	public static String getSearchGlobalDataUrl()
	{
		return ENVIRONMENT.getBaseUrl() + SEARCH + GET_SEARCH_GLOBAL_DATA;
	}
	
	public static String getSearchHistoryUrl()
	{
		return ENVIRONMENT.getBaseUrl() + SEARCH + GET_SEARCH_HISTORY;
	}
	
	public static String getSearchFlightUrl()
	{
		return ENVIRONMENT.getBaseUrl() + SEARCH + SEARCH_FLIGHT;
	}
	
	public static String getSearchDepartureCitiesUrl()
	{
		return ENVIRONMENT.getBaseUrl() + SEARCH + GET_SEARCH_DEPARTURE_CITIES;
	}
	
	public static String getDeleteUserPushMessageUrl()
	{
		return ENVIRONMENT.getBaseUrl() + APP_USERS + DELETE_USER_PUSH_MESSAGE;
	}
	
	public static String getGetPushNotificationsUrl()
	{
		return ENVIRONMENT.getBaseUrl() + APP_USERS + GET_PUSH_NOTIFICATIONS;
	}
	
	public static String getSetPushAsReadUrl()
	{
		return ENVIRONMENT.getBaseUrl() + APP_USERS + SET_PUSH_AS_READ;
	}
	
	public static String getSearchDestinationCitiesUrl()
	{
		return ENVIRONMENT.getBaseUrl() + SEARCH + GET_SEARCH_DESTINATION_CITIES;
	}
	
	public static String getGetUserCheckListUrl()
	{
		return ENVIRONMENT.getBaseUrl() + CHECK_LIST + GET_USER_CHECK_LIST;
	}
	
	public static String getSetUserCheckListUrl()
	{
		return ENVIRONMENT.getBaseUrl() + CHECK_LIST + SET_USER_CHECK_LIST;
	}
	
	public static String getDeleteUserCheckListUrl()
	{
		return ENVIRONMENT.getBaseUrl() + CHECK_LIST + DELETE_USER_CHECK_LIST;
	}
	
	public static String getDoCheckListUrl()
	{
		return ENVIRONMENT.getBaseUrl() + CHECK_LIST + DO_CHECK_LIST;
	}
	
	public static String getFlightTimetableUrlByLanguage()
	{
		return (UserData.getInstance().getLanguage().isHebrew() ? FLIGHT_TIMETABLE_HE : FLIGHT_TIMETABLE_EN) + UTM_FLIGHTS_TIMETABLE;
	}
	
	public static String getChangePasswordUrlByLanguage()
	{
		return UserData.getInstance().getLanguage().isHebrew() ? CHANGE_PASSWORD_HE : CHANGE_PASSWORD_EN;
	}
	
	public static String getMatmidUrlByLanguage()
	{
		return UserData.getInstance().getLanguage().isHebrew() ? MATMID_URL_HE : MATMID_URL_EN;
	}
	
	public static String getMatmidLockedUrlByLanguage()
	{
		return UserData.getInstance().getLanguage().isHebrew() ? MATMID_LOCKED_URL_HE : MATMID_LOCKED_URL_EN;
	}
	
	public static String getForgotMatmidNumberUrlByLanguage()
	{
		return UserData.getInstance().getLanguage().isHebrew() ? FORGOT_MATMID_NUMBER_URL_HE : FORGOT_MATMID_NUMBER_URL_EN;
	}
	
	public static String getForgotPasswordMatmidUrlByLanguage()
	{
		return UserData.getInstance().getLanguage().isHebrew() ? FORGOT_PASSWORD_MATMID_URL_HE : FORGOT_PASSWORD_MATMID_URL_EN;
	}
	
	public static String getChangePasswordMatmidUrlByLanguage()
	{
		return UserData.getInstance().getLanguage().isHebrew() ? MATMID_CHANGE_PASSWORD_HE : MATMID_CHANGE_PASSWORD_EN;
	}
	
	public static String getCheckInUrlByLanguage()
	{
		return UserData.getInstance().getLanguage().isHebrew() ? CHECK_IN_HE : CHECK_IN_EN;
	}
	
	public static String getAccountStatusUrlByLanguage()
	{
		return (UserData.getInstance().getLanguage().isHebrew() ? ACCOUNT_STATUS_HE : ACCOUNT_STATUS_EN) + UTM_MATMID_ACCOUNT_STATUS;
	}
	
	public static String getCancelCheckInUrlByLanguage()
	{
		return (UserData.getInstance().getLanguage().isHebrew() ? CANCEL_CHECK_IN_HE : CANCEL_CHECK_IN_EN) + UTM_CANCEL_CHECK_IN;
	}
	
	public static String getAddingMissingFlightUrlByLanguage()
	{
		return (UserData.getInstance().getLanguage().isHebrew() ? ADDING_MISSING_FLIGHT_HE : ADDING_MISSING_FLIGHT_EN) + UTM_MATMID_ADD_MISSING_FLIGHT;
	}
	
	public static String getSaveBoardingPassUrlByLanguage()
	{
		return UserData.getInstance().getLanguage().isHebrew() ? SAVE_BOARDING_PASS_HE : SAVE_BOARDING_PASS_EN;
	}
	
	public static String getNewsletterUrlByLanguage()
	{
		return (UserData.getInstance().getLanguage().isHebrew() ? NEWSLETTER_HE : NEWSLETTER_EN) + UTM_NEWSLETTER;
	}
	
	public static String getChangeSeatMealUrlByLanguage(String iPnr, String iLastName)
	{
		return (UserData.getInstance().getLanguage().isHebrew() ? CHANGE_SEAT_MEAL_HE : CHANGE_SEAT_MEAL_EN) + "&ENC=" + getAESChangeParams(iPnr, iLastName) + UTM_SEAT_MEAL;
	}
	
	private static String getAESChangeParams(String iPnr, String iLastName)
	{
		String strENC = "App=true" + "&requestId=" + AppUtils.generateRandomId(0, 9) + "&REC_LOC=" + iPnr + "&LAST_NAME=" + iLastName + "&ENC_TIME=" + DateTimeUtils.convertDateToENCDateString(DateTimeUtils.getCurrentDateByGMT("+03")/*new Date()*/, false);
		return AppUtils.encrypt(strENC);
	}
	
	public static String getCheckinUrlWithParamsByLanguage(final String iPnr, final Passenger iPassenger, final boolean isGroupPnr)
	{
		String checkinDataString = "&ENC=" + getAESCheckInParams(iPnr, iPassenger.getLastName(), isGroupPnr ? iPassenger.getTicketNumber() : iPnr);
		String url = (UserData.getInstance().getLanguage().isHebrew() ? PERFORM_CHECK_IN_MD5_HE : PERFORM_CHECK_IN_MD5_EN) + checkinDataString + UTM_CHECK_IN;
		
		AppUtils.printLog(Log.DEBUG, TAG, "checking url : " + url);
		
		return url;
	}
	
	private static String getAESCheckInParams(String iPnr, String iLastName, String iTicketNumber)
	{
		//		String strENC = "ENC_TIME=" + DateTimeUtils.convertDateToENCDateString(new Date(), true) + "&LAST_NAME=" + iLastName + "&ticketNumber=" + iTicketNumber + "&PNR=" + iPnr;
		
		/*
		  added extra value to avoid unwanted extra characters added by AES encryption by default
		 */
		String strENC = "ENC_TIME=" + DateTimeUtils.convertDateToENCDateString(DateTimeUtils.getCurrentDateByGMT("+03")/*new Date()*/, true) + "&LAST_NAME=" + iLastName + "&ticketNumber=" +
				iTicketNumber + "&EXTRA=" + iPnr;
		return AppUtils.encrypt(strENC);
	}
	
	public static String getElalMatmidLoginUrl()
	{
		return ELAL_MATMID_LOGIN;
	}
	
	public static String getElalTermsOfUseUrl()
	{
		return (UserData.getInstance().getLanguage().isEnglish() ? ELAL_TERMS_OF_USE_EN : ELAL_TERMS_OF_USE_HE) + UTM_TERM_OF_USE;
	}
	
	public static String getElalAboutPassengerTypesUrl()
	{
		return UserData.getInstance().getLanguage().isEnglish() ? ABOUT_PASSENGERS_TYPES_EN : ABOUT_PASSENGERS_TYPES_HE;
	}
	
	static String getMatmidCookieString()
	{
		String token = ElalPreferenceUtils.getSharedPreferenceStringOrDefault(ElAlApplication.getInstance(), ElalPreferenceUtils.CA_TOKEN, "");
		String session = "";
		if (BuildConfig.DEBUG)
		{
			session = UserData.getInstance().getSMSession();
		}
		//		return "rbzid=" + token + ";" + session + " lbinfosso=26617; push=01";
		if (!token.contains("rbzid="))
		{
			return "rbzid=" + token + "";
		}
		else
		{
			return token;
		}
	}
	
	
	public static String getMatmidCookieStringForWeb()
	{
		String token = ElalPreferenceUtils.getSharedPreferenceStringOrDefault(ElAlApplication.getInstance(), ElalPreferenceUtils.CA_TOKEN, "");
		String session = UserData.getInstance().getSMSession();
		
		if (session != null && !TextUtils.isEmpty(session))
		{
			return "rbzid=" + token + "; SMSESSION=" + session;
		}
		else
		{
			return "rbzid=" + token + ";";
		}
		//		return "rbzid=" + token + ";" + session + " lbinfosso=26617; push=01";
	}
	
	//endregion
	
	public static String getYouTubeUrl(final String iYouTubeUrl)
	{
		String result = "";
		
		if (!TextUtils.isEmpty(iYouTubeUrl))
		{
			result = YOUTUBE_BASE_URL + iYouTubeUrl;
		}
		
		return result;
	}
	
	public static boolean isEnvironmentProd()
	{
		return ENVIRONMENT == eEnvironment.Prod;
	}
	
	public static String getMobileCountryCodesUrl()
	{
		return ENVIRONMENT.getBaseUrl() + APPLICATION + GET_COUNTRY_CODES;
	}
	
	private enum eEnvironment
	{
		Dev("http://elalappws.ewavetest.co.il/", "http://elalappws.ewavetest.co.il/", "http://elalappws.ewavetest.co.il/"),
		Test("", "", ""),
		Prod("https://dpel.elal.co.il:449/ElalAppWS/", /*"https://www.elal-matmid.com/mobileapp/"*//**avishay 30/08/17 just for delivery**/"https://www.elal-matmid.com/mobileapp/", "https://www.elal.com/ElalMobileApp/V2/");
		
		private String mBaseUrl, mMatmidBaseUrl, mFilesBaseUrl;
		
		eEnvironment(final String iBaseUrl, final String iMatmidBaseUrl, final String iFilesBaseUrl)
		{
			mBaseUrl = iBaseUrl;
			mMatmidBaseUrl = iMatmidBaseUrl;
			mFilesBaseUrl = iFilesBaseUrl;
		}
		
		
		public String getBaseUrl()
		{
			return mBaseUrl;
		}
		
		public String getMatmidBaseUrl()
		{
			return mMatmidBaseUrl;
		}
		
		public String getFilesBaseUrl()
		{
			return mFilesBaseUrl;
		}
		
	}
	
	//endregion
}
