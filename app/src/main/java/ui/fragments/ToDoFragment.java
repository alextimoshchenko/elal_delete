package ui.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.tagmanager.DataLayer;

import java.util.ArrayList;
import java.util.Date;

import global.IntentExtras;
import global.UserData;
import il.co.ewave.elal.R;
import interfaces.IAddOrEditCheckListItemListener;
import interfaces.ICheckListListener;
import ui.activities.EWBaseActivity;
import ui.activities.EWBaseDrawerActivity;
import ui.activities.MyFlightsActivity;
import ui.adapters.ToDoAdapter;
import ui.customWidgets.ChangeDirectionCheckbox;
import ui.customWidgets.ExpandCollapseAnimation;
import utils.errors.ErrorsHandler;
import utils.errors.ServerError;
import utils.global.AppUtils;
import utils.global.DateTimeUtils;
import utils.global.googleAnalitics.googleTagManager.TagManagerImplementation;
import utils.managers.TabsBackStackManager;
import webServices.controllers.MainController;
import webServices.global.ePnrFlightType;
import webServices.requests.RequestDeleteUserCheckList;
import webServices.requests.RequestDoCheckList;
import webServices.requests.RequestUpdateUserCheckList;
import webServices.responses.ResponseDeleteUserCheckList;
import webServices.responses.ResponseDoCheckList;
import webServices.responses.ResponseGetUserCheckList;
import webServices.responses.ResponseSetOrUpdateUserCheckList;
import webServices.responses.UserCheckList.CheckList;
import webServices.responses.UserCheckList.CheckListItem;
import webServices.responses.UserCheckList.CheckListParent;
import webServices.responses.UserCheckList.UserCheckList;
import webServices.responses.responseGetUserActivePNRs.PnrFlight;
import webServices.responses.responseGetUserActivePNRs.UserActivePnr;

/**
 * Created with care by Shahar Ben-Moshe on 31/01/17.
 */

public class ToDoFragment extends EWBaseFragment implements ICheckListListener, Response.ErrorListener, IAddOrEditCheckListItemListener
{
	public static final String TAG = ToDoFragment.class.getSimpleName();
	private static final int DELETE_DIALOG_EXPAND_COLLAPSE_ANIMATION_SPEED = 500;
	private ChangeDirectionCheckbox mCbHideCheckedItems;
	private Button mTvAddItem;
	private RecyclerView mRvCheckList;
	private RelativeLayout mLlDeleteBottomDialog;
	private Button mTvDeleteBottomDialogCancel;
	private Button mBtnDeleteBottomDialogApprove;
	private ToDoAdapter mToDoAdapter;
	private CheckList mCheckList;
	private UserActivePnr mUserActivePnr;
	private ArrayList<CheckListParent> mAdapterCheckListHelper = new ArrayList<>();
	//	private boolean mShouldUpdateCheckList = false;
	//	private boolean mShouldUpdateCheckList = true;
	private ExpandCollapseAnimation mExpandCollapseAnimation;
	private CheckListItem mCheckListItem;
	private int mParentPosition;
	private int mChildPosition;
	
	public static Bundle createBundleForFragment(UserActivePnr iUserActivePnr, UserCheckList iUserCheckList)
	{
		Bundle result = new Bundle();
		
		result.putParcelable(IntentExtras.USER_ACTIVE_PNR, iUserActivePnr);
		result.putParcelable(IntentExtras.USER_CHECK_LIST, iUserCheckList);
		
		return result;
	}
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_to_do_layout);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		getIntentExtras();
		initReference();
		setListeners();
		setAdapter();
		initGui();
	}
	
	@Override
	public void onServiceConnected()
	{
		if (getActivity() != null && getActivity() instanceof MyFlightsActivity && ((MyFlightsActivity) getActivity()).isShouldUpdateCheckList())
		{
			((MyFlightsActivity) getActivity()).setmShouldUpdateCheckList(false);
			getUserCheckList();
		}
	}
	
	private void initGui()
	{
		mLlDeleteBottomDialog.setVisibility(View.INVISIBLE);
		resolveExpandableLayout();
	}
	
	private void resolveExpandableLayout()
	{
		new Handler().post(new Runnable()
		{
			@Override
			public void run()
			{
				int expandedViewHeight = mLlDeleteBottomDialog.getHeight();
				mLlDeleteBottomDialog.setVisibility(View.GONE);
				mExpandCollapseAnimation = new ExpandCollapseAnimation(mLlDeleteBottomDialog, false, DELETE_DIALOG_EXPAND_COLLAPSE_ANIMATION_SPEED, expandedViewHeight);
			}
		});
	}
	
	private void getIntentExtras()
	{
		if (getArguments() != null)
		{
			mUserActivePnr = getArguments().getParcelable(IntentExtras.USER_ACTIVE_PNR);
			trySetCheckList((UserCheckList) getArguments().getParcelable(IntentExtras.USER_CHECK_LIST));
		}
	}
	
	private void initReference()
	{
		mCbHideCheckedItems = (ChangeDirectionCheckbox) getActivity().findViewById(R.id.cb_toDo_HideCheckedItems);
		mTvAddItem = (Button) getActivity().findViewById(R.id.btn_toDo_AddItem);
		mRvCheckList = (RecyclerView) getActivity().findViewById(R.id.rv_toDo_CheckList);
		mLlDeleteBottomDialog = (RelativeLayout) getActivity().findViewById(R.id.ll_toDo_DeleteBottomDialog);
		mTvDeleteBottomDialogCancel = (Button) getActivity().findViewById(R.id.tv_toDo_DeleteCancel);
		mBtnDeleteBottomDialogApprove = (Button) getActivity().findViewById(R.id.btn_toDo_DeleteApprove);
	}
	
	private void setListeners()
	{
		mCbHideCheckedItems.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(final CompoundButton iButtonView, final boolean iIsChecked)
			{
				setAdapter();
			}
		});
		mTvAddItem.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				goToAddOrEditCheckListItem(null);
				//event 48
				((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight - Ready", "Action", TagManagerImplementation.TIME_SCOPE + mUserActivePnr.getPnr() + " >> " + "Remember", "Label", "Add item to list"));
			}
		});
		mTvDeleteBottomDialogCancel.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnDeleteCancelClick();
			}
		});
		mBtnDeleteBottomDialogApprove.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnDeleteApproveClick();
			}
		});
	}
	
	private void doOnDeleteCancelClick()
	{
		clearDeleteFields();
		
		if (mExpandCollapseAnimation.isExpand())
		{
			mExpandCollapseAnimation.reverseAndAnimate();
		}
	}
	
	private void doOnDeleteApproveClick()
	{
		performDelete(mCheckListItem, mParentPosition, mChildPosition);
		clearDeleteFields();
		
		if (mExpandCollapseAnimation.isExpand())
		{
			mExpandCollapseAnimation.reverseAndAnimate();
		}
	}
	
	private void clearDeleteFields()
	{
		mCheckListItem = null;
		mParentPosition = -1;
		mChildPosition = -1;
	}
	
	private void setAdapterUpdateAtPosition(final int iParentAdapterPosition, final int iChildAdapterPosition)
	{
		setAdapterAtPosition(iParentAdapterPosition, iChildAdapterPosition, false);
	}
	
	private void setAdapterAtPosition(final int iParentPosition, final int iChildPosition, boolean iIsRemove)
	{
		if (mToDoAdapter != null && iParentPosition > RecyclerView.NO_POSITION && iChildPosition > RecyclerView.NO_POSITION)
		{
			if (iIsRemove)
			{
				mToDoAdapter.notifyChildRemoved(iParentPosition, iChildPosition);
			}
			else
			{
				mToDoAdapter.notifyChildChanged(iParentPosition, iChildPosition);
			}
		}
		else
		{
			//			if (mToDoAdapter == null)
			//			{
			//				mRvCheckList.addItemDecoration(new SpacesItemDecoration(getResources().getDimensionPixelOffset(R.dimen.tutorial_page_indicator_spacing)));
			//			}
			//
			resetAdapterListHelper();
			mToDoAdapter = new ToDoAdapter(mAdapterCheckListHelper, this);
			mRvCheckList.setLayoutManager(new LinearLayoutManager(getActivity()));
			mRvCheckList.setAdapter(mToDoAdapter);
			mRvCheckList.setHasFixedSize(true);
			
			//			initSwipe();
			//			setUpItemTouchHelper();
			//			setUpAnimationDecoratorHelper();
		}
	}
	
	private void resetAdapterListHelper()
	{
		mAdapterCheckListHelper.clear();
		
		if (mCheckList != null)
		{
			mAdapterCheckListHelper.addAll(mCheckList.getCheckListParents());
			filterListByDoneState();
			
			CheckListParent enjoyFlightParent = new CheckListParent(R.string.have_a_great_flight);
			mAdapterCheckListHelper.add(new CheckListParent(R.string.have_a_great_flight));
		}
	}
	
	private void filterListByDoneState()
	{
		if (mAdapterCheckListHelper != null)
		{
			for (CheckListParent checkListParent : mAdapterCheckListHelper)
			{
				checkListParent.filterList(mCbHideCheckedItems.isChecked());
			}
		}
	}
	
	private void setAdapterRemoveAtPosition(final int iParentAdapterPosition, final int iChildAdapterPosition)
	{
		setAdapterAtPosition(iParentAdapterPosition, iChildAdapterPosition, true);
	}
	
	@Override
	public void onCheckChanged(final CheckListItem iCheckListItem)
	{
		//event 49
		((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight - Ready", "Action", TagManagerImplementation.TIME_SCOPE + mUserActivePnr.getPnr() + " >> Remember", "Label", "Select / Change " + ">Task Title"));
		
		performCheckChanged(iCheckListItem);
	}
	
	private void performCheckChanged(final CheckListItem iCheckListItem)
	{
		if (iCheckListItem != null && isServiceConnected())
		{
			if (iCheckListItem.isUserCustom())
			{
				trySetProgressDialog(true);
				getService().getController(MainController.class)
				            .updateUserCheckList(new RequestUpdateUserCheckList(iCheckListItem.getItemID(), iCheckListItem.getItemTitle(), iCheckListItem.isDone(), iCheckListItem.getTimeDimensionID(), mUserActivePnr
						            .getPnr(), UserData.getInstance().getUserID()), new Response.Listener<ResponseSetOrUpdateUserCheckList>()
				            {
					            @Override
					            public void onResponse(final ResponseSetOrUpdateUserCheckList response)
					            {
						            trySetProgressDialog(false);
						            //									setAdapter();
					            }
				            }, this);
			}
			else
			{
				trySetProgressDialog(true);
				
				getService().getController(MainController.class)
				            .doCheckList(new RequestDoCheckList(UserData.getInstance()
				                                                        .getUserID(), iCheckListItem.getItemID(), getPnr(), iCheckListItem.isDone()), new Response.Listener<ResponseDoCheckList>()
				            {
					            @Override
					            public void onResponse(final ResponseDoCheckList response)
					            {
						            trySetProgressDialog(false);
						            //									setAdapter();
					            }
				            }, this);
			}
		}
		
	}
	
	private String getPnr()
	{
		String result = "";
		
		if (mUserActivePnr != null)
		{
			result = mUserActivePnr.getPnr();
		}
		
		return result;
	}
	
	@Override
	public void onLink(final CheckListItem iCheckListItem)
	{
		//event 50
		String itemUrl = iCheckListItem.getUrl();
		((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight - Ready", "Action", TagManagerImplementation.TIME_SCOPE + mUserActivePnr.getPnr() + " >> Remember", "Label", "Link title > " + itemUrl));
		
		resolveLinkClick(iCheckListItem);
	}
	
	private void resolveLinkClick(final CheckListItem iCheckListItem)
	{
		if (iCheckListItem != null)
		{
			if (iCheckListItem.isInApp())
			{
				Bundle bundle = new Bundle();
				bundle.putBoolean(IntentExtras.IS_IN_APP_NAVIGATION, true);
				
				tryGoToScreenById(iCheckListItem.getUrlInt(), bundle);
			}
			else
			{
				tryGoToWebFragment(iCheckListItem.getUrl());
			}
		}
	}
	
	@Override
	public void onEdit(final CheckListItem iCheckListItem, final int iParentAdapterPosition, final int iChildAdapterPosition)
	{
		if (iCheckListItem != null && iCheckListItem.isUserCustom())
		{
			goToAddOrEditCheckListItem(iCheckListItem);
			
			//event 51
			((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight - Ready", "Action", TagManagerImplementation.TIME_SCOPE + mUserActivePnr.getPnr() + " >> Remember", "Label", "Edit Task >Task Title"));
		}
	}
	
	@Override
	public void onDelete(final CheckListItem iCheckListItem, final int iParentPosition, final int iChildPosition)
	{
		mCheckListItem = iCheckListItem;
		mParentPosition = iParentPosition;
		mChildPosition = iChildPosition;
		
		if (!mExpandCollapseAnimation.isExpand())
		{
			mExpandCollapseAnimation.reverseAndAnimate();
		}
		
		//event 52
		((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight - Ready", "Action", TagManagerImplementation.TIME_SCOPE + mUserActivePnr.getPnr() + " >> Remember", "Label", "Delete Task >Task Title"));
	}
	
	@Override
	public void onOptionClicked()
	{
		//event 53
		((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight - Ready", "Action", TagManagerImplementation.TIME_SCOPE + mUserActivePnr.getPnr() + " >> Remember", "Label", "Link " + "title"));
	}
	
	private void goToAddOrEditCheckListItem(final CheckListItem iCheckListItem)
	{
		if (UserData.getInstance().isUserGuest())
		{
			((EWBaseDrawerActivity) getActivity()).openAskLoginDialog();
		}
		else
		{
			if (getParentFragment() != null && getParentFragment() instanceof TabsBackStackManager.ITabsBackStackManagerHandler)
			{
				AddOrEditCheckListItemFragment addOrEditCheckListItemFragment = new AddOrEditCheckListItemFragment();
				Bundle bundle = AddOrEditCheckListItemFragment.createBundleForFragment(getPnr(), mUserActivePnr, iCheckListItem);
				addOrEditCheckListItemFragment.setArguments(bundle);
				
				((TabsBackStackManager.ITabsBackStackManagerHandler) getParentFragment()).pushFragmentToCurrentTab(addOrEditCheckListItemFragment, true, true);
			}
		}
	}
	
	private void performDelete(final CheckListItem iCheckListItem, final int iParentPosition, final int iChildPosition)
	{
		if (isServiceConnected() && iCheckListItem != null && iCheckListItem.isUserCustom())
		{
			trySetProgressDialog(true);
			getService().getController(MainController.class).deleteUserCheckList(new RequestDeleteUserCheckList(iCheckListItem.getItemID()), new Response.Listener<ResponseDeleteUserCheckList>()
			{
				@Override
				public void onResponse(final ResponseDeleteUserCheckList response)
				{
					if (iParentPosition > -1 && iChildPosition > -1)
					{
						setAdapterRemoveAtPosition(iParentPosition, iChildPosition);
					}
					
					trySetProgressDialog(false);
				}
			}, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError error)
				{
					trySetProgressDialog(false);
					if (error != null && error instanceof ServerError)
					{
						AppUtils.createSimpleMessageDialog(getActivity(), getContext().getResources().getString(R.string.item_not_deleted), null, null, getContext().getResources()
						                                                                                                                                            .getString(R.string.close), null)
						        .show();
					}
					else
					{
						ErrorsHandler.tryShowServiceErrorDialog(error, getActivity());
					}
				}
			});
		}
	}
	
	@Override
	public void onSuccessfulAdd()
	{
		if (getActivity() != null && getActivity() instanceof MyFlightsActivity && ((MyFlightsActivity) getActivity()).isShouldUpdateCheckList())
		{
			((MyFlightsActivity) getActivity()).setmShouldUpdateCheckList(true);
			onServiceConnected();
		}
	}
	
	private void getUserCheckList()
	{
		if (getService() != null && mUserActivePnr != null)
		{
			trySetProgressDialog(true);
			
			getService().getController(MainController.class).getUserCheckList(mUserActivePnr.generateUserCheckListRequest(), new Response.Listener<ResponseGetUserCheckList>()
			{
				@Override
				public void onResponse(final ResponseGetUserCheckList response)
				{
					Integer timeDifference = DateTimeUtils.HOURS_OF_A_DAY * 30;
					if (mUserActivePnr != null)
					{
						
						//TODO avishay 17/01/18 check the current date here
						PnrFlight pnrFlight = mUserActivePnr.getCurrentItemFlightAtPositionDepartureOrReurnOrNull(0, ePnrFlightType.DEPARTURE);
//						Date departureDate = pnrFlight/*mUserActivePnr.getCurrentItemFlightAtPositionDepartureOrReurnOrNull(0, ePnrFlightType.DEPARTURE)*/.getDepartureDate();
////						Date currentDate = new Date();
////						currentDate = DateTimeUtils.convertDateToDateIgnoreGMT(currentDate);
						
						Date departureDate = DateTimeUtils.convertDateToDateIgnoreGMT(pnrFlight.getDepartureDate());
						Date currentDate = DateTimeUtils.getCurrentDateByGMT(pnrFlight.getmDepartureTimezoneOffset());
						
						timeDifference = DateTimeUtils.getTimeDifferenceInHoursOrNull(currentDate, departureDate);
					}
					UserCheckList userCheckList = response.getContent().orderListByTimes(timeDifference);
					trySetCheckList(userCheckList);
					setAdapter();
					trySetProgressDialog(false);
				}
			}, this);
		}
	}
	
	@Override
	public void onErrorResponse(final VolleyError error)
	{
		trySetProgressDialog(false);
		ErrorsHandler.tryShowServiceErrorDialog(error, getActivity());
	}
	
	private void trySetCheckList(UserCheckList iUserCheckList)
	{
		if (iUserCheckList != null && mUserActivePnr != null && mUserActivePnr.getCurrentItemFlightAtPositionOrNull(0, ePnrFlightType.DEPARTURE) != null)
		{
			PnrFlight flight = mUserActivePnr.getCurrentItemFlightAtPositionOrNull(0, ePnrFlightType.DEPARTURE);
			mCheckList = new CheckList(iUserCheckList, flight.getDepartureDate(), flight.getmDepartureTimezoneOffset());
		}
	}
	
	private void setAdapter()
	{
		setAdapterAtPosition(RecyclerView.NO_POSITION, RecyclerView.NO_POSITION, false);
	}
	
	
}
