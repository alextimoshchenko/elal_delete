package ui.customWidgets;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created with care by Shahar Ben-Moshe on 20/01/16.
 */
public class NonSwappableViewPager extends ViewPager
{
	
	public NonSwappableViewPager(Context context)
	{
		super(context);
	}
	
	public NonSwappableViewPager(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}
	
	@Override
	public boolean onInterceptTouchEvent(MotionEvent event)
	{
		// Never allow swiping to switch between pages
		return false;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		// Never allow swiping to switch between pages
		return false;
	}
}
