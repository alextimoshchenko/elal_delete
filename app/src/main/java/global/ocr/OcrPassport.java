package global.ocr;

import android.graphics.Bitmap;
import android.os.Parcel;

import java.util.Date;
import java.util.HashMap;

import interfaces.IOcrResultObject;
import scanovate.ocr.common.OCRManager;
import scanovate.ocr.passport.PassportOCRManager;
import utils.global.AppUtils;
import utils.global.DateTimeUtils;
import utils.global.ParseUtils;

/**
 * Created with care by Shahar Ben-Moshe on 02/01/17.
 */

public class OcrPassport implements IOcrResultObject
{
	private OCRManager.SCAN_STATUS mResultScanStatus;
	private String mPassportFirstRow;
	private String mPassportSecondRow;
	private String mPassportNumber;
	private String mGender;
	private String mFullName;
	private String mFirstName;
	private String mLastName;
	private Date mDateOfBirth;
	private Date mDateOfExpiration;
	private String mIdNumber;
	private String mNationality;
	private String mIssuingCountry;
	private Bitmap mPassportImage;
	
	public OcrPassport(final HashMap<String, Object> iResultValues)
	{
		mResultScanStatus = (OCRManager.SCAN_STATUS) iResultValues.get(PassportOCRManager.PASSPORT_SCAN_RESULT_STATUS);
		mPassportFirstRow = (String) iResultValues.get(PassportOCRManager.PASSPORT_SCAN_RESULT_FIRST_ROW);
		mPassportSecondRow = (String) iResultValues.get(PassportOCRManager.PASSPORT_SCAN_RESULT_SECOND_ROW);
		mPassportNumber = (String) iResultValues.get(PassportOCRManager.PASSPORT_SCAN_RESULT_NUMBER);
		mGender = (String) iResultValues.get(PassportOCRManager.PASSPORT_SCAN_RESULT_GENDER);
		mFirstName = (String) iResultValues.get(PassportOCRManager.PASSPORT_SCAN_RESULT_FIRST_NAME);
		mLastName = (String) iResultValues.get(PassportOCRManager.PASSPORT_SCAN_RESULT_LAST_NAME);
		mFullName = mFirstName + mLastName;
		mDateOfBirth = DateTimeUtils.convertStringInSlashSeparatedDayMonthYearToDate(resolveDateOfBirth((HashMap<String, Integer>) iResultValues.get(PassportOCRManager.PASSPORT_SCAN_RESULT_DATE_OF_BIRTH)));
		mDateOfExpiration = DateTimeUtils.convertStringInSlashSeparatedDayMonthYearToDate(resolveExpirationDate((HashMap<String, Integer>) iResultValues.get(PassportOCRManager
				.PASSPORT_SCAN_RESULT_DATE_OF_EXPIRY)));
		mIdNumber = (String) iResultValues.get(PassportOCRManager.PASSPORT_SCAN_RESULT_ID_NUMBER);
		mNationality = (String) iResultValues.get(PassportOCRManager.PASSPORT_SCAN_RESULT_NATIONALITY);
		mIssuingCountry = (String) iResultValues.get(PassportOCRManager.PASSPORT_SCAN_RESULT_ISSUING_COUNTRY);
		mPassportImage = (Bitmap) iResultValues.get(PassportOCRManager.PASSPORT_SCAN_RESULT_IMAGE);
	}
	
	private String resolveDateOfBirth(final HashMap<String, Integer> iDateOfBirthMap)
	{
		String dateOfBirth =  iDateOfBirthMap.get(PassportOCRManager.kPASSPORT_DATE_DAY) + "/" +   iDateOfBirthMap.get(PassportOCRManager.kPASSPORT_DATE_MONTH) + "/" +   iDateOfBirthMap
				.get(PassportOCRManager.kPASSPORT_DATE_YEAR);
		
		if (iDateOfBirthMap.size() == 3)
		{
			int dateOfBirthYearInt =  iDateOfBirthMap.get(PassportOCRManager.kPASSPORT_DATE_YEAR) ;
			
			// is between ages of 0 to 17
			if (dateOfBirthYearInt >= 0 && dateOfBirthYearInt <= 17)
			{
				dateOfBirthYearInt += 2000;
			}
			// is between ages of 18 to 99
			else if (dateOfBirthYearInt >= 18 && dateOfBirthYearInt <= 99)
			{
				dateOfBirthYearInt += 1900;
			}
			
			// if date valid (because it could be -1)
			if (dateOfBirthYearInt > 0)
			{
				dateOfBirth =   iDateOfBirthMap.get(PassportOCRManager.kPASSPORT_DATE_DAY) + "/" +  iDateOfBirthMap.get(PassportOCRManager.kPASSPORT_DATE_MONTH) + "/" + String.valueOf
						(dateOfBirthYearInt);
			}
		}
		
		return dateOfBirth;
	}
	
	
	private String resolveExpirationDate(final  HashMap<String, Integer> iExpirationDateMap)
	{
		 String expirationDate =  iExpirationDateMap.get(PassportOCRManager.kPASSPORT_DATE_DAY) + "/" +   iExpirationDateMap.get(PassportOCRManager.kPASSPORT_DATE_MONTH) + "/" +   iExpirationDateMap
				.get(PassportOCRManager.kPASSPORT_DATE_YEAR);
//		String expirationDate = AppUtils.removeWhiteSpace(iExpirationDate);
//		String[] expirationDateSplit = expirationDate.split("/");
		
		if (iExpirationDateMap.size() == 3)
		{
			int expirationDateYearInt =  iExpirationDateMap.get(PassportOCRManager.kPASSPORT_DATE_YEAR) ;
			
			// is between ages of 0 to 17
			if (expirationDateYearInt >= 0 && expirationDateYearInt <= 79)
			{
				expirationDateYearInt += 2000;
			}
			// is between ages of 18 to 99
			else if (expirationDateYearInt >= 80 && expirationDateYearInt <= 99)
			{
				expirationDateYearInt += 1900;
			}
			
			// if date valid (because it could be -1)
			if (expirationDateYearInt > 0)
			{
				expirationDate =   iExpirationDateMap.get(PassportOCRManager.kPASSPORT_DATE_DAY) + "/" +  iExpirationDateMap.get(PassportOCRManager.kPASSPORT_DATE_MONTH) + "/" + String.valueOf
						(expirationDateYearInt);
			}
		}
		
		return expirationDate;
	}
	
	
	private String resolveDateOfBirth(final String iDateOfBirth)
	{
		String dateOfBirth = AppUtils.removeWhiteSpace(iDateOfBirth);
		String[] dateOfBirthSplit = dateOfBirth.split("/");
		
		if (dateOfBirthSplit.length == 3)
		{
			int dateOfBirthYearInt = ParseUtils.tryParseStringToIntegerOrDefault(dateOfBirth.split("/")[2], -1);
			
			// is between ages of 0 to 17
			if (dateOfBirthYearInt >= 0 && dateOfBirthYearInt <= 17)
			{
				dateOfBirthYearInt += 2000;
			}
			// is between ages of 18 to 99
			else if (dateOfBirthYearInt >= 18 && dateOfBirthYearInt <= 99)
			{
				dateOfBirthYearInt += 1900;
			}
			
			// if date valid (because it could be -1)
			if (dateOfBirthYearInt > 0)
			{
				dateOfBirth = dateOfBirthSplit[0] + "/" + dateOfBirthSplit[1] + "/" + String.valueOf(dateOfBirthYearInt);
			}
		}
		
		return dateOfBirth;
	}
	
	private String resolveExpirationDate(final String iExpirationDate)
	{
		String expirationDate = AppUtils.removeWhiteSpace(iExpirationDate);
		String[] expirationDateSplit = expirationDate.split("/");
		
		if (expirationDateSplit.length == 3)
		{
			int expirationDateYearInt = ParseUtils.tryParseStringToIntegerOrDefault(expirationDate.split("/")[2], -1);
			
			// is between ages of 0 to 17
			if (expirationDateYearInt >= 0 && expirationDateYearInt <= 79)
			{
				expirationDateYearInt += 2000;
			}
			// is between ages of 18 to 99
			else if (expirationDateYearInt >= 80 && expirationDateYearInt <= 99)
			{
				expirationDateYearInt += 1900;
			}
			
			// if date valid (because it could be -1)
			if (expirationDateYearInt > 0)
			{
				expirationDate = expirationDateSplit[0] + "/" + expirationDateSplit[1] + "/" + String.valueOf(expirationDateYearInt);
			}
		}
		
		return expirationDate;
	}
	
	public OCRManager.SCAN_STATUS getResultScanStatus()
	{
		return mResultScanStatus;
	}
	
	public String getPassportFirstRow()
	{
		return mPassportFirstRow;
	}
	
	public String getPassportSecondRow()
	{
		return mPassportSecondRow;
	}
	
	public String getPassportNumber()
	{
		return mPassportNumber;
	}
	
	public String getGender()
	{
		return mGender;
	}
	
	public String getFullName()
	{
		return mFullName;
	}
	
	public String getFirstName()
	{
		return mFirstName;
	}
	
	public String getLastName()
	{
		return mLastName;
	}
	
	public Date getDateOfBirth()
	{
		return mDateOfBirth;
	}
	
	public Date getDateOfExpiration()
	{
		return mDateOfExpiration;
	}
	
	public String getIdNumber()
	{
		return mIdNumber;
	}
	
	public String getNationality()
	{
		return mNationality;
	}
	
	public String getIssuingCountry()
	{
		return mIssuingCountry;
	}
	
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeInt(this.mResultScanStatus == null ? -1 : this.mResultScanStatus.ordinal());
		dest.writeString(this.mPassportFirstRow);
		dest.writeString(this.mPassportSecondRow);
		dest.writeString(this.mPassportNumber);
		dest.writeString(this.mGender);
		dest.writeString(this.mFullName);
		dest.writeString(this.mFirstName);
		dest.writeString(this.mLastName);
		dest.writeLong(this.mDateOfBirth != null ? this.mDateOfBirth.getTime() : -1);
		dest.writeLong(this.mDateOfExpiration != null ? this.mDateOfExpiration.getTime() : -1);
		dest.writeString(this.mIdNumber);
		dest.writeString(this.mNationality);
		dest.writeString(this.mIssuingCountry);
		//        dest.writeParcelable(this.mPassportImage, flags);
	}
	
	protected OcrPassport(Parcel in)
	{
		int tmpMResultScanStatus = in.readInt();
		this.mResultScanStatus = tmpMResultScanStatus == -1 ? null : OCRManager.SCAN_STATUS.values()[tmpMResultScanStatus];
		this.mPassportFirstRow = in.readString();
		this.mPassportSecondRow = in.readString();
		this.mPassportNumber = in.readString();
		this.mGender = in.readString();
		this.mFullName = in.readString();
		this.mFirstName = in.readString();
		this.mLastName = in.readString();
		long tmpMDateOfBirth = in.readLong();
		this.mDateOfBirth = tmpMDateOfBirth == -1 ? null : new Date(tmpMDateOfBirth);
		long tmpMDateOfExpiration = in.readLong();
		this.mDateOfExpiration = tmpMDateOfExpiration == -1 ? null : new Date(tmpMDateOfExpiration);
		this.mIdNumber = in.readString();
		this.mNationality = in.readString();
		this.mIssuingCountry = in.readString();
		//        this.mPassportImage = in.readParcelable(Bitmap.class.getClassLoader());
	}
	
	public static final Creator<OcrPassport> CREATOR = new Creator<OcrPassport>()
	{
		@Override
		public OcrPassport createFromParcel(Parcel source)
		{
			return new OcrPassport(source);
		}
		
		@Override
		public OcrPassport[] newArray(int size)
		{
			return new OcrPassport[size];
		}
	};
}
