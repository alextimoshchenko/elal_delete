package global.ocr;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;
import java.util.HashMap;

import interfaces.IOcrResultObject;
import scanovate.ocr.common.OCRManager;
import scanovate.ocr.israeldriverslicense.IsraelDriversLicenseOCRManager;
import utils.global.AppUtils;
import utils.global.DateTimeUtils;

/**
 * Created with care by Shahar Ben-Moshe on 02/01/17.
 */

public class OcrIsraelDriversLicense implements IOcrResultObject, Parcelable
{
	private OCRManager.SCAN_STATUS mResultScanStatus;
	private String mFirstNameHebrew;
	private String mLastNameHebrew;
	private String mFirstNameEnglish;
	private String mLastNameEnglish;
	private String mAddress;
	private String mIdNumber;
	private String mLicenseNumber;
	private Date mDateOfBirth;
	private String mDateOfExpiration;
	private String mDateOfIssue;
	private Bitmap mFaceImage;
	private Bitmap mCardImage;
	private Bitmap mLastImage;
	
	public OcrIsraelDriversLicense(final HashMap<String, Object> iResultValues)
	{
		mResultScanStatus = (OCRManager.SCAN_STATUS) iResultValues.get(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_STATUS);
		mFirstNameHebrew = (String) iResultValues.get(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_FIRST_NAME_HEBREW);
		mLastNameHebrew = (String) iResultValues.get(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_LAST_NAME_HEBREW);
		mFirstNameEnglish = (String) iResultValues.get(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_FIRST_NAME_ENGLISH);
		mLastNameEnglish = (String) iResultValues.get(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_LAST_NAME_ENGLISH);
		mAddress = (String) iResultValues.get(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_ADDRESS);
		mIdNumber = (String) iResultValues.get(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_ID);
		mLicenseNumber = (String) iResultValues.get(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_LICENSE_NUMBER);
		mDateOfBirth = DateTimeUtils.convertStringInSlashSeparatedDayMonthYearToDate(AppUtils.removeWhiteSpace((String) iResultValues.get(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_DATE_OF_BIRTH)));
		mDateOfExpiration = (String) iResultValues.get(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_DATE_OF_EXPIRY);
		mDateOfIssue = (String) iResultValues.get(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_DATE_OF_ISSUE);
		mFaceImage = (Bitmap) iResultValues.get(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_FACE_IMAGE);
		mCardImage = (Bitmap) iResultValues.get(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_CARD_IMAGE);
		mLastImage = (Bitmap) iResultValues.get(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_IMAGE);
	}
	
	public OCRManager.SCAN_STATUS getResultScanStatus()
	{
		return mResultScanStatus;
	}
	
	public String getFirstNameHebrew()
	{
		return mFirstNameHebrew;
	}
	
	public String getLastNameHebrew()
	{
		return mLastNameHebrew;
	}
	
	public String getFirstNameEnglish()
	{
		return mFirstNameEnglish;
	}
	
	public String getLastNameEnglish()
	{
		return mLastNameEnglish;
	}
	
	public String getAddress()
	{
		return mAddress;
	}
	
	public String getIdNumber()
	{
		return mIdNumber;
	}
	
	public String getLicenseNumber()
	{
		return mLicenseNumber;
	}
	
	public Date getDateOfBirth()
	{
		return mDateOfBirth;
	}
	
	public String getDateOfExpiration()
	{
		return mDateOfExpiration;
	}
	
	public String getDateOfIssue()
	{
		return mDateOfIssue;
	}
	
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeInt(this.mResultScanStatus == null ? -1 : this.mResultScanStatus.ordinal());
		dest.writeString(this.mFirstNameHebrew);
		dest.writeString(this.mLastNameHebrew);
		dest.writeString(this.mFirstNameEnglish);
		dest.writeString(this.mLastNameEnglish);
		dest.writeString(this.mAddress);
		dest.writeString(this.mIdNumber);
		dest.writeString(this.mLicenseNumber);
		dest.writeLong(this.mDateOfBirth != null ? this.mDateOfBirth.getTime() : -1);
		dest.writeString(this.mDateOfExpiration);
		dest.writeString(this.mDateOfIssue);
		//        dest.writeParcelable(this.mFaceImage, flags);
		//        dest.writeParcelable(this.mCardImage, flags);
		//        dest.writeParcelable(this.mLastImage, flags);
	}
	
	protected OcrIsraelDriversLicense(Parcel in)
	{
		int tmpMResultScanStatus = in.readInt();
		this.mResultScanStatus = tmpMResultScanStatus == -1 ? null : OCRManager.SCAN_STATUS.values()[tmpMResultScanStatus];
		this.mFirstNameHebrew = in.readString();
		this.mLastNameHebrew = in.readString();
		this.mFirstNameEnglish = in.readString();
		this.mLastNameEnglish = in.readString();
		this.mAddress = in.readString();
		this.mIdNumber = in.readString();
		this.mLicenseNumber = in.readString();
		long tmpMDateOfBirth = in.readLong();
		this.mDateOfBirth = tmpMDateOfBirth == -1 ? null : new Date(tmpMDateOfBirth);
		this.mDateOfExpiration = in.readString();
		this.mDateOfIssue = in.readString();
		//        this.mFaceImage = in.readParcelable(Bitmap.class.getClassLoader());
		//        this.mCardImage = in.readParcelable(Bitmap.class.getClassLoader());
		//        this.mLastImage = in.readParcelable(Bitmap.class.getClassLoader());
	}
	
	public static final Creator<OcrIsraelDriversLicense> CREATOR = new Creator<OcrIsraelDriversLicense>()
	{
		@Override
		public OcrIsraelDriversLicense createFromParcel(Parcel source)
		{
			return new OcrIsraelDriversLicense(source);
		}
		
		@Override
		public OcrIsraelDriversLicense[] newArray(int size)
		{
			return new OcrIsraelDriversLicense[size];
		}
	};
}
