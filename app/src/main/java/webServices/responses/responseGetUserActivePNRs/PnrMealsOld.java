package webServices.responses.responseGetUserActivePNRs;

import com.fasterxml.jackson.annotation.JsonProperty;



public class PnrMealsOld
{
	@JsonProperty("mDescription")
	private String mDescription;
	@JsonProperty("SSRCode")
	private String mCode;
	@JsonProperty("PNR")
	private String mPnr;
	@JsonProperty("TicketNumber")
	private String mTicketNumber;
	
	public PnrMealsOld()
	{
	}
	
	public String getDescription()
	{
		return mDescription;
	}
	
	public String getCode()
	{
		return mCode;
	}
	
	public String getPnr()
	{
		return mPnr;
	}
	
	public String getTicketNumber()
	{
		return mTicketNumber;
	}
}
