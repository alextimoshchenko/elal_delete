package webServices.responses.responseChangePassword;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Avishay.Peretz on 21/06/2017.
 */

public class ChangePassword
{
	@JsonProperty("UserID")
	private int mUserID;
	@JsonProperty("Success")
	private boolean mSuccess;
	
	public int getUserID()
	{
		return mUserID;
	}
	
	public boolean isSuccess()
	{
		return mSuccess;
	}
	
	public ChangePassword()
	{
	}
}
