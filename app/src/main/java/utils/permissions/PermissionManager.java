package utils.permissions;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import java.util.ArrayList;

/**
 * Created with care by Shahar Ben-Moshe on 11/12/16.
 */

public class PermissionManager
{
    private static final short REQUEST_CODE_PERMISSIONS = 32589;
    
    public static void requestApplicationPermissionIfNeeded(final Activity iActivity, final String iPermission, final IPermissionsListener iPermissionsListener)
    {
        ArrayList<String> permissionsList = new ArrayList<>();
        permissionsList.add(iPermission);
        requestApplicationPermissionsIfNeeded(iActivity, permissionsList, iPermissionsListener);
    }
    
    public static void requestApplicationPermissionsIfNeeded(final Activity iActivity, final ArrayList<String> iPermissions, final IPermissionsListener iPermissionsListener)
    {
        if (iActivity != null && iPermissions != null && iPermissionsListener != null)
        {
            ArrayList<String> finalPermissionsList = new ArrayList<>();
        
            for (String permission : iPermissions)
            {
                if (isPermissionUsedByApplication(iActivity, permission) && !isPermissionGranted(iActivity, permission))
                {
                    shouldRequestPermissionWithExplanation(iActivity, permission);
                    finalPermissionsList.add(permission);
                }
            }
        
            if (finalPermissionsList.size() > 0)
            {
                requestPermissions(iActivity, finalPermissionsList.toArray(new String[0]));
            }
            else
            {
                iPermissionsListener.doOnPermissionsGranted(new String[0]);
            }
        }
    }
    
    private static void requestPermission(final Activity iActivity, final String iPermission)
    {
        requestPermissions(iActivity, new String[] {iPermission});
    }
    
    private static void requestPermissions(final Activity iActivity, final String[] iPermissions)
    {
        ActivityCompat.requestPermissions(iActivity, iPermissions, REQUEST_CODE_PERMISSIONS);
        
    }
    
    public static void handlePermissionResponse(final int iRequestCode, final String[] iPermissions, final int[] iGrantResults, IPermissionsListener iPermissionsListener)
    {
        if (iRequestCode == REQUEST_CODE_PERMISSIONS && iPermissions != null && iPermissions.length > 0 && iGrantResults != null && iGrantResults.length > 0)
        {
            if (iGrantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                iPermissionsListener.doOnPermissionsGranted(iPermissions);
            }
            else
            {
                iPermissionsListener.doOnPermissionsDenied(iPermissions);
            }
        }
    }
	
	public static boolean isPermissionInArray(final String iPermissions[], final String iPermissionToFind)
	{
		boolean result = false;
		
		if (iPermissions != null && !TextUtils.isEmpty(iPermissionToFind))
		{
			for (String currentPermission : iPermissions)
			{
				if (!TextUtils.isEmpty(currentPermission) && currentPermission.equals(iPermissionToFind))
				{
					result = true;
					break;
				}
			}
		}
		
		return result;
	}
	
	private static boolean shouldRequestPermissionWithExplanation(final Activity iActivity, final String iPermission)
    {
        boolean result = false;
        
        if (iActivity != null && !TextUtils.isEmpty(iPermission))
        {
            result = ActivityCompat.shouldShowRequestPermissionRationale(iActivity, iPermission);
        }
        
        return result;
    }
    
    public static boolean isPermissionGranted(final Context iContext, final String iPermission)
    {
        boolean result = false;
        
        if (iContext != null && !TextUtils.isEmpty(iPermission))
        {
            result = ContextCompat.checkSelfPermission(iContext, iPermission) == PackageManager.PERMISSION_GRANTED;
        }
        
        return result;
    }
    
    private static boolean isPermissionUsedByApplication(final Activity iActivity, final String iPermission)
    {
        boolean result = false;
        try
        {
            PackageInfo info = iActivity.getPackageManager().getPackageInfo(iActivity.getPackageName(), PackageManager.GET_PERMISSIONS);
            if (info.requestedPermissions != null)
            {
                for (String requestedPermission : info.requestedPermissions)
                {
                    if (requestedPermission.equals(iPermission))
                    {
                        result = true;
                        break;
                    }
                }
            }
        }
        catch (Exception ignored)
        {
        }
        
        return result;
    }
    
    public static String[] getAllApplicationPermissions(final Context iContext)
    {
        String[] result = null;
        
        try
        {
            PackageInfo info = iContext.getPackageManager().getPackageInfo(iContext.getPackageName(), PackageManager.GET_PERMISSIONS);
            
            result = info.requestedPermissions;
        }
        catch (Exception ignored)
        {
        }
        
        if (result == null)
        {
            result = new String[0];
        }
        
        return result;
    }
    
    public static ArrayList<String> getAllUngrantedPermissions(final Context iContext)
    {
        ArrayList<String> result = new ArrayList<>();
    
        for (String applicationPermission : getAllApplicationPermissions(iContext))
        {
            if (!isPermissionGranted(iContext, applicationPermission))
            {
                result.add(applicationPermission);
            }
        }
    
        return result;
    }
}
