package webServices.requests;

import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.UserData;

public class RequestUserLogin
{
	@JsonProperty("Email")
	private String mEmail;
	@JsonProperty("IPAddress")
	private String mIPAddress;
	@JsonProperty("Device")
	private RequestDevice mDevice = new RequestDevice();
	@JsonProperty("IsGuest")
	private boolean mIsGuest;
	@JsonProperty("LanguageCode")
	private String mLanguageCode = UserData.getInstance().getLanguage().getLanguageCode();
	@JsonProperty("Password")
	private String mPassword;
	@JsonProperty("RememberMe")
	private boolean mRememberMe;
	
	public RequestUserLogin(final String iEmail, final String iPassword, final boolean iRememberMe, final String iIPAddress)
	{
		mEmail = iEmail;
		mPassword = iPassword;
		mIsGuest = TextUtils.isEmpty(iEmail);
		mIPAddress = iIPAddress;
		//		mDevice.setRememberMe(iRememberMe);
		mRememberMe = iRememberMe;
	}
}
