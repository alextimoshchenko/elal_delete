package webServices.responses.responseSetDevice;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SetDevice
{
	@JsonProperty("Success")
	private boolean mSuccess;
	
	public boolean getSuccess()
	{
		return this.mSuccess;
	}
	
	public SetDevice()
	{
	}
}
