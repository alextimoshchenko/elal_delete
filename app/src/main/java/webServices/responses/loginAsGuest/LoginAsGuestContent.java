package webServices.responses.loginAsGuest;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginAsGuestContent
{
	@JsonProperty("Status")
	private String mStatus;
	@JsonProperty("UserID")
	private int mUserID;
	
	public String getStatus()
	{
		return this.mStatus;
	}
	
	public int getUserID()
	{
		return this.mUserID;
	}
	
	public LoginAsGuestContent()
	{
	}
}
