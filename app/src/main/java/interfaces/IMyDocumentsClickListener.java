package interfaces;

import utils.global.dbObjects.Document;

/**
 * Created with care by Shahar Ben-Moshe on 06/03/17.
 */

public interface IMyDocumentsClickListener
{
	void onChildClick(final int iParentPosition, final int iChildPosition, final Document.eDocumentType iDocumentType, final long iCategoryItemsCount);
}
