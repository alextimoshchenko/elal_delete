package utils.errors;

import android.text.TextUtils;

import global.ElAlApplication;
import il.co.ewave.elal.R;

/**
 * Created with care by Shahar Ben-Moshe on 13/04/16.
 */
public class LocalError extends Exception
{
	public enum eLocalError
	{
		NoConnection("L1", R.string.error_L1_no_internet_connection),
		GeneralConnectionError(R.string.general_connection_error),
		GeneralError(R.string.general_error),
		SomeProblemWithArray(R.string.some_problem_with_array),
		
		//Search Flight Activity
		MaxPassengersQuantity(R.string.max_passengers_quantity),
		YouCanNotChoseDestinationDeparture(R.string.you_can_not_chose_destination),
		YouCanNotChoseDestinationArrival(R.string.you_can_not_chose_arrival),
		NoMatchingReturnFromAnotherDestinationItem(R.string.no_matching_return_from_another_destination_item),
		AdultsInfants(R.string.adults_infants),
		InfantsAward(R.string.infants_award),
		AdultsTeenagers(R.string.adults_teenagers),
		SelectedDateLessThenCurrent(R.string.selected_date_less_then_current),
		
		AdultsChildren(R.string.adults_children),
		NoSuchItems(R.string.no_such_items),
		NullPointerException(R.string.null_pointer_exception),
		SomeFieldAreNotValid(R.string.some_fields_are_not_valid),
		DamagedUrl(R.string.damaged_url),
		NoSuchTagName(R.string.no_such_tab_name),
		ControllerError(R.string.controller_error),
		ServiceDisconnected(R.string.service_disconnected),
		KeepAliveError(R.string.keep_alive_error),
		NotificationError(R.string.notification_error);
		
		private String mErrorCode;
		private int mErrorMessageResourceId;
		
		eLocalError(int iErrorMessageResourceId) {
			mErrorCode = "";
			mErrorMessageResourceId = iErrorMessageResourceId;
		}
		
		eLocalError(String iErrorCode, int iErrorMessageResourceId)
		{
			mErrorCode = iErrorCode;
			mErrorMessageResourceId = iErrorMessageResourceId;
		}
		
		public String getErrorCode()
		{
			return mErrorCode;
		}
		
		public String getErrorMessage()
		{
			String result = ElAlApplication.getInstance().getString(mErrorMessageResourceId);
			
			if (TextUtils.isEmpty(result))
			{
				result = "";
			}
			else
			{
				if (result.toLowerCase().contains("%s"))
				{
					result = String.format(result, getErrorCode());
				}
			}
			
			return result;
		}
		
		public static eLocalError getLocalErrorByErrorCodeOrNull(String iErrorCode)
		{
			eLocalError result = null;
			
			for (eLocalError localError : eLocalError.values())
			{
				if (localError.getErrorCode() != null && localError.getErrorCode().equals(iErrorCode))
				{
					result = localError;
					break;
				}
			}
			
			return result;
		}
	}
	
	private String mErrorCode;
	private String mErrorMessage;
	
	public LocalError(eLocalError iLocalError)
	{
		this.mErrorCode = iLocalError.getErrorCode();
		this.mErrorMessage = iLocalError.getErrorMessage();
	}
	
	private String resolveExceptionMessage(Exception e)
	{
		String result = ElAlApplication.getInstance().getString(R.string.error);
		
		if (e != null)
		{
			if (!TextUtils.isEmpty(e.getMessage()))
			{
				result = getMessage();
			}
			else if (e.getCause() != null && !TextUtils.isEmpty(e.getCause().getMessage()))
			{
				result = e.getCause().getMessage();
			}
		}
		
		return result;
	}
	
	@Override
	public String getMessage()
	{
		String message = "";
		
		if (!TextUtils.isEmpty(mErrorMessage))
		{
			message += mErrorCode + ": ";
		}
		
		message += mErrorMessage;
		
		return message;
	}
	
	public String getErrorCode()
	{
		return mErrorCode;
	}
	
	public String getErrorMessage()
	{
		return mErrorMessage;
	}
}
