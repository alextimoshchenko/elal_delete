package webServices.responses.responseGetUserPNRs;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class GetUserPNRsContentPNRFlights
{
	@JsonProperty("Destination")
	private String mDestination;
	@JsonProperty("FlightDuration")
	private String mFlightDuration;
	@JsonProperty("ClassOfService")
	private String mClassOfService;
	@JsonProperty("Origination")
	private String mOrigination;
	@JsonProperty("Direction")
	private String mDirection;
	@JsonProperty("CreateDate")
	private String mCreateDate;
	@JsonProperty("Segment")
	private int mSegment;
	@JsonProperty("LastModified")
	private String mLastModified;
	@JsonProperty("DepartTerminal")
	private String mDepartTerminal;
	@JsonProperty("FlightStatus")
	private String mFlightStatus;
	@JsonProperty("ReferenceId")
	private String mReferenceId;
	@JsonProperty("PNR")
	private String mPNR;
	@JsonProperty("ArrivalDate")
	private String mArrivalDate;
	@JsonProperty("FlightNumber")
	private String mFlightNumber;
	@JsonProperty("FlightCompany")
	private String mFlightCompany;
	@JsonProperty("DepartureDate")
	private Date mDepartureDate;
	@JsonProperty("Carrier")
	private String mCarrier;
	@JsonProperty("AircraftType")
	private String mAircraftType;
	
	public String getDestination()
	{
		return mDestination;
	}
	
	public String getFlightDuration()
	{
		return mFlightDuration;
	}
	
	public String getClassOfService()
	{
		return mClassOfService;
	}
	
	public String getOrigination()
	{
		return mOrigination;
	}
	
	public String getDirection()
	{
		return mDirection;
	}
	
	public String getCreateDate()
	{
		return mCreateDate;
	}
	
	public int getSegment()
	{
		return mSegment;
	}
	
	public String getLastModified()
	{
		return mLastModified;
	}
	
	public String getDepartTerminal()
	{
		return mDepartTerminal;
	}
	
	public String getFlightStatus()
	{
		return mFlightStatus;
	}
	
	public String getReferenceId()
	{
		return mReferenceId;
	}
	
	public String getPNR()
	{
		return mPNR;
	}
	
	public String getArrivalDate()
	{
		return mArrivalDate;
	}
	
	public String getFlightNumber()
	{
		return mFlightNumber;
	}
	
	public String getFlightCompany()
	{
		return mFlightCompany;
	}
	
	public Date getDepartureDate()
	{
		return mDepartureDate;
	}
	
	public String getCarrier()
	{
		return mCarrier;
	}
	
	public String getAircraftType()
	{
		return mAircraftType;
	}
	
	public GetUserPNRsContentPNRFlights()
	{
	}
}
