package webServices.requests;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import utils.global.DateTimeUtils;
import utils.global.LocaleUtils;
import webServices.responses.responseGetUserFamily.GetUserFamilyContent;

public class RequestSetUserFamilyMember
{
	//	@JsonProperty("MiddleNameEng")
	//	private String mMiddleNameEng;
	@JsonProperty("FirstNameEng")
	private String mFirstNameEng;
	@JsonProperty("FamilyID")
	private int mFamilyID;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeUtils.DATE_FORMAT_yyyy_MM_ddHHmmSS)
	@JsonProperty("DOB")
	private Date mDateOfBirth;
	@JsonProperty("LastNameEng")
	private String mLastNameEng;
	@JsonProperty("FamilyTypeID")
	private int mFamilyTypeID;
	@JsonProperty("IsActive")
	private boolean mIsActive;
	@JsonProperty("Title")
	private String mTitle;
	
	public RequestSetUserFamilyMember(String iMiddleNameEng, String iFirstNameEng, int iFamilyID, Calendar iDateOfBirth, String iLastNameEng, int iFamilyTypeID, String iTitle)
	{
		//		mMiddleNameEng = iMiddleNameEng;
		mFirstNameEng = iFirstNameEng;
		mFamilyID = iFamilyID;
	
		this.mDateOfBirth = iDateOfBirth == null ? null : iDateOfBirth.getTime();
		
		mLastNameEng = iLastNameEng;
		mFamilyTypeID = iFamilyTypeID;
		mIsActive = true;
		mTitle = iTitle;
	}
	
	public RequestSetUserFamilyMember(String iMiddleNameEng, String iFirstNameEng, int iFamilyID, Calendar iDateOfBirth, String iLastNameEng, int iFamilyTypeID)
	{
		//		mMiddleNameEng = iMiddleNameEng;
		mFirstNameEng = iFirstNameEng;
		mFamilyID = iFamilyID;
		this.mDateOfBirth = iDateOfBirth == null ? null : iDateOfBirth.getTime();
		mLastNameEng = iLastNameEng;
		mFamilyTypeID = iFamilyTypeID;
		mIsActive = true;
	}
	
	public RequestSetUserFamilyMember(GetUserFamilyContent iGetUserFamilyContent, boolean iIsActive)
	{
		mFirstNameEng = iGetUserFamilyContent.getFirstNameEng();
		mFamilyID = iGetUserFamilyContent.getFamilyID();
		mDateOfBirth = iGetUserFamilyContent.getDateOfBirth();
		mLastNameEng = iGetUserFamilyContent.getLastNameEng();
		mFamilyTypeID = iGetUserFamilyContent.getFamilyTypeID();
		mIsActive = iIsActive;
	}
}
