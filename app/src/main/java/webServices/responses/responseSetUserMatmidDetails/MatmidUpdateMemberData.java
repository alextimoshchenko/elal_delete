package webServices.responses.responseSetUserMatmidDetails;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Avishay.Peretz on 07/06/2017.
 */

public class MatmidUpdateMemberData
{
	
//	@JsonProperty("Success")
//	private boolean mSuccess;
//	@JsonProperty("Status")
//	private String mStatus;
	
	@JsonProperty("IsOK")
	private boolean mSuccess;
	@JsonProperty("MatmidErrorCode")
	private int mStatus;
	
	public boolean getSuccess()
	{
		return this.mSuccess;
	}
	
	public int getStatus()
	{
		return mStatus;
	}
	
	public MatmidUpdateMemberData()
	{
	}
}
