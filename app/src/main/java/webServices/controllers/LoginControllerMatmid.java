package webServices.controllers;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import global.AppData;
import utils.errors.ServerError;
import webServices.global.CAStringRequest;
import webServices.global.JacksonMatmidLoginRequest;
import webServices.global.JacksonRequest;
import webServices.global.RequestStringBuilder;
import webServices.requests.EmptyRequest;
import webServices.requests.RequestGetSideMenuItems;
import webServices.requests.RequestGetUnreadNotificationsBadge;
import webServices.requests.RequestLogout;
import webServices.requests.RequestMatmidLogin;
import webServices.responses.ResponseGetSideMenuItems;
import webServices.responses.ResponseGetUnreadNotificationsBadge;
import webServices.responses.ResponseKeepAlive;
import webServices.responses.ResponseMatmidLogin;
import webServices.responses.getMobileCountryCodes.ResponseGetMobileCountryCodes;

/**
 * Created with care by Alexey.T on 23/10/2017.
 * <p>
 * TODO: Add a class header comment!
 */
public class LoginControllerMatmid extends AController implements MatmidInterface
{
	public void MatmidLogin(final String iMatmidNumber, final String iMatmidPassword, final Response.Listener<ResponseMatmidLogin> iListener, final Response.ErrorListener iErrorListener)
	{
		RequestMatmidLogin requestMatmidLogin = new RequestMatmidLogin(iMatmidNumber, iMatmidPassword);
		
		JacksonMatmidLoginRequest<ResponseMatmidLogin> request = new JacksonMatmidLoginRequest<>(Request.Method.POST, RequestStringBuilder.getMatmidLoginUrl(), requestMatmidLogin, iMatmidPassword, ResponseMatmidLogin.class, new Response.Listener<ResponseMatmidLogin>()
		{
			@Override
			public void onResponse(final ResponseMatmidLogin iResponseMatmidLogin)
			{
				if (iResponseMatmidLogin.getContent().getMatmidErrorCode() == ServerError.eServerError.Ok.getErrorCode())
				{
					handleMatmidLoginUrlResponse(iResponseMatmidLogin, iListener, iErrorListener);
				}
				else
				{
					if (iListener != null)
					{
						iListener.onResponse(iResponseMatmidLogin);
					}
				}
			}
		}, new Response.ErrorListener()
		{
			@Override
			public void onErrorResponse(final VolleyError error)
			{
				if (iErrorListener != null)
				{
					iErrorListener.onErrorResponse(error);
				}
			}
		});
		
		request.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 1.0f));
		request.setSecretHashHeader(request.getBody());
		getRequestQueue().add(request);
	}
	
	public void MatmidExtendSession(final Response.ErrorListener iErrorListener)
	{
		//		RequestMatmidKeepAlive requestMatmidKeepAlive = new RequestMatmidKeepAlive();
		
		Log.d("MatmidLogin", "calling matmid extend session");
		
		EmptyRequest emptyRequest = new EmptyRequest();
		
		JacksonMatmidLoginRequest<ResponseKeepAlive> request = new JacksonMatmidLoginRequest<>(Request.Method.POST, RequestStringBuilder.getMatmidKeepAliveUrl(), emptyRequest, null, ResponseKeepAlive.class, new Response.Listener<ResponseKeepAlive>()
		{
			@Override
			public void onResponse(final ResponseKeepAlive iResponseKeepAlive)
			{
				if (iResponseKeepAlive.getContent().getmErrorCode() == ServerError.eServerError.Ok.getErrorCode())
				{
					handleMatmidKeepAliveUrlResponse(iResponseKeepAlive, iErrorListener);
				}
				else
				{
					// TODO: 23/10/2017 - handle error
				}
			}
		}, new Response.ErrorListener()
		{
			@Override
			public void onErrorResponse(final VolleyError error)
			{
				if (iErrorListener != null)
				{
					iErrorListener.onErrorResponse(error);
				}
			}
		});
		
		getRequestQueue().add(request);
	}
	
	private void handleMatmidKeepAliveUrlResponse(ResponseKeepAlive iResponseKeepAlive, Response.ErrorListener iListener)
	{
		// TODO: 27/11/2017 - save SMSession and remove last one.
		//		UserData.getInstance().setSMSession(((MatmidLogin) iResponseKeepAlive.getContent()).getmSMSession());
		
	}
	
	private void handleMatmidLoginUrlResponse(final ResponseMatmidLogin iResponseMatmidLogin, final Response.Listener<ResponseMatmidLogin> iListener, final Response.ErrorListener iErrorListener)
	{
		getMobileCountryCodes(new Response.Listener<ResponseGetMobileCountryCodes>()
		{
			@Override
			public void onResponse(ResponseGetMobileCountryCodes iResponseCountryCodes)
			{
				if (iResponseMatmidLogin.getContent().getMatmidErrorCode() == ServerError.eServerError.Ok.getErrorCode())
				{
					AppData.getInstance().saveCountryCodes(iResponseCountryCodes.getContent());
				}
				doGetSideMenuItems(iResponseMatmidLogin, iListener, iErrorListener);
				
				//				else
				//				{
				//					if (iListener != null)
				//					{
				//						iListener.onResponse(iResponseMatmidLogin);
				//					}
				//				}
			}
		}, new Response.ErrorListener()
		{
			@Override
			public void onErrorResponse(VolleyError error)
			{
				//				if (iErrorListener != null)
				//				{
				//					iErrorListener.onErrorResponse(error);
				//				}
				
				doGetSideMenuItems(iResponseMatmidLogin, iListener, iErrorListener);
			}
		});
	}
	
	private void doGetSideMenuItems(final ResponseMatmidLogin iResponseMatmidLogin, final Response.Listener<ResponseMatmidLogin> iListener, final Response.ErrorListener iErrorListener)
	{
		final int userId = iResponseMatmidLogin.getContent().getUserID();
		final String sessionId = iResponseMatmidLogin.getContent().getSessionID();
		final String memberId = iResponseMatmidLogin.getContent().getUserObject() != null ? iResponseMatmidLogin.getContent().getUserObject().getMatmidMemberID() : "";
		
		//GetSideMenu
		getSideMenuItems(new RequestGetSideMenuItems(userId, sessionId, memberId), new Response.Listener<ResponseGetSideMenuItems>()
		{
			@Override
			public void onResponse(final ResponseGetSideMenuItems iResponseGetSideMenuItems)
			{
				handleMatmidLoginUrlResponse(iResponseGetSideMenuItems, iResponseMatmidLogin, iListener, iErrorListener);
			}
		}, new Response.ErrorListener()
		{
			@Override
			public void onErrorResponse(final VolleyError error)
			{
				if (iErrorListener != null)
				{
					iErrorListener.onErrorResponse(error);
				}
			}
		});
	}
	
	public void getMobileCountryCodes(final Response.Listener<ResponseGetMobileCountryCodes> countryCodesListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && countryCodesListener != null && iErrorListener != null)
		{
			//			JacksonRequest<ResponseGetMobileCountryCodes> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getMobileCountryCodesUrl(), new RequestLogout(""), ResponseGetMobileCountryCodes.class, countryCodesListener, iErrorListener);
			JacksonRequest<ResponseGetMobileCountryCodes> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getMobileCountryCodesUrl(), new EmptyRequest(), ResponseGetMobileCountryCodes.class, countryCodesListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void getSideMenuItems(final RequestGetSideMenuItems iRequest, final Response.Listener<ResponseGetSideMenuItems> iListener, final Response.ErrorListener errorListener)
	{
		if (!isRequestQueueNull() && iListener != null && errorListener != null)
		{
			JacksonRequest<ResponseGetSideMenuItems> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getGetSideMenuItemsUrl(), iRequest, ResponseGetSideMenuItems.class, iListener, errorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	private void handleMatmidLoginUrlResponse(final ResponseGetSideMenuItems iResponseGetSideMenuItems, final ResponseMatmidLogin iResponseMatmidLogin,
	                                          final Response.Listener<ResponseMatmidLogin> iListener, final Response.ErrorListener iErrorListener)
	{
		AppData.getInstance().setSideMenuData(iResponseGetSideMenuItems);
		final int userId = iResponseMatmidLogin.getContent().getUserID();
		
		//Get notification badge
		GetUnreadNotificationsBadge(new RequestGetUnreadNotificationsBadge(userId), new Response.Listener<ResponseGetUnreadNotificationsBadge>()
		{
			@Override
			public void onResponse(final ResponseGetUnreadNotificationsBadge response)
			{
				AppData.getInstance().setUnReadPushes(response);
				
				if (iListener != null)
				{
					iListener.onResponse(iResponseMatmidLogin);
				}
			}
		}, new Response.ErrorListener()
		{
			@Override
			public void onErrorResponse(final VolleyError error)
			{
				if (iErrorListener != null)
				{
					iErrorListener.onErrorResponse(error);
				}
			}
		});
	}
	
	private void GetUnreadNotificationsBadge(final RequestGetUnreadNotificationsBadge iRequest, final Response.Listener<ResponseGetUnreadNotificationsBadge> iListener,
	                                         final Response.ErrorListener errorListener)
	{
		if (!isRequestQueueNull() && iListener != null && errorListener != null)
		{
			JacksonRequest<ResponseGetUnreadNotificationsBadge> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getUnreadNotificationsBadge(), iRequest, ResponseGetUnreadNotificationsBadge.class, iListener, errorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void MatmidCAToken(final Response.Listener<String> iListener, final Response.ErrorListener iErrorListener)
	{
		CAStringRequest request = new CAStringRequest(Request.Method.GET, RequestStringBuilder.getMatmidCaUrl(), new Response.Listener<String>()
		{
			@Override
			public void onResponse(String response)
			{
				String token = response;
				
				if (token.contains("window.rbzid="))
				{
					Log.i("MatmidCAToken-before", token);
					token = token.replace("window.rbzid=\"", ""); // replace prefix
					token = token.replace("\";\n", ""); // replace suffix
					Log.i("MatmidCAToken-after", token);
					
					iListener.onResponse(token);
				}
				else
				{
					// reblaze error
					iListener.onResponse("");
				}
			}
		}, new Response.ErrorListener()
		{
			@Override
			public void onErrorResponse(VolleyError error)
			{
				// handle error response
				iErrorListener.onErrorResponse(error);
			}
		});
		
		getRequestQueue().getCache().clear();
		
		try
		{
			request.setSecretHashHeader(request.getBody());
		}
		catch (AuthFailureError iError)
		{
			iError.printStackTrace();
		}
		
		getRequestQueue().add(request);
	}
}
