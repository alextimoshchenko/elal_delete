package ui.activities;

import android.os.Bundle;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.Calendar;
import java.util.Date;

import global.AppData;
import global.UserData;
import global.eActivityModule;
import il.co.ewave.elal.R;
import interfaces.IElalScreensNavigator;
import ui.fragments.search.SearchFlightFragment;
import utils.errors.ErrorsHandler;
import utils.errors.LocalError;
import utils.global.AppUtils;
import utils.global.searchFlightObjects.SearchObject;
import webServices.controllers.SearchFlightController;
import webServices.requests.RequestGetSearchGlobalData;
import webServices.requests.RequestGetSearchHistory;
import webServices.requests.RequestSearchDepartureCities;
import webServices.requests.RequestSearchDestinationCities;
import webServices.responses.ResponseClearUserHistory;
import webServices.responses.ResponseGetSearchGlobalData;
import webServices.responses.ResponseGetSearchHistory;
import webServices.responses.ResponseSearchFlight;
import webServices.responses.getSearchDepartureCities.ResponseSearchDepartureCities;
import webServices.responses.getSearchDepartureCities.ResponseSearchDepartureCitiesContent;
import webServices.responses.getSearchDestiationCities.ResponseSearchDestinationCities;
import webServices.responses.getSearchGlobalData.GetSearchGlobalDataContent;

/**
 * Created with care by Alexey.T on 10/07/2017.
 */
public class SearchFlightActivity extends EWBaseUserActivity implements IElalScreensNavigator, Response.ErrorListener
{
	private static final String TAG = SearchFlightActivity.class.getSimpleName();
	
	@Override
	protected void initScreen()
	{
		setContentView(R.layout.activity_main_layout);
		super.initScreen();
		SearchObject.getInstance().cleanSearchObject();
	}
	
	@Override
	protected void setCurrentActivityModuleId()
	{
		AppData.getInstance().setModuleId(eActivityModule.SearchFlight.getActivityModuleId());
	}
	
	public void getSearchDestinationCities(String iCityCode, final Response.Listener<ResponseSearchDestinationCities> iListener, final Response.ErrorListener iErrorListener)
	{
		if (isServiceConnected())
		{
			setProgressDialog(true);
			
			UserData userData = UserData.getInstance();
			
			if (iCityCode == null)
			{
				iCityCode = "";
			}
			
			getService().getController(SearchFlightController.class)
			            .GetSearchDestinationCities(new RequestSearchDestinationCities(userData.getUserID(), iCityCode), new Response.Listener<ResponseSearchDestinationCities>()
			            {
				            @Override
				            public void onResponse(final ResponseSearchDestinationCities response)
				            {
					            if (iListener != null)
					            {
						            iListener.onResponse(response);
					            }
				            }
			            }, new Response.ErrorListener()
			            {
				            @Override
				            public void onErrorResponse(final VolleyError error)
				            {
					            if (iErrorListener != null)
					            {
						            iErrorListener.onErrorResponse(error);
					            }
				            }
			            });
		}
	}
	
	public void saveFlightInHistory(final Response.Listener<ResponseSearchFlight> iListener, final Response.ErrorListener iErrorListener)
	{
		if (isServiceConnected())
		{
			setProgressDialog(true);
			
			getService().getController(SearchFlightController.class).SearchFlight(SearchObject.getInstance().getRequestSearchFlight(), new Response.Listener<ResponseSearchFlight>()
			{
				@Override
				public void onResponse(final ResponseSearchFlight response)
				{
					if (iListener != null)
					{
						iListener.onResponse(response);
					}
				}
			}, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError error)
				{
					if (iErrorListener != null)
					{
						iErrorListener.onErrorResponse(error);
					}
				}
			});
		}
	}
	
	public void getSearchHistory(final Response.Listener<ResponseGetSearchHistory> iListener, final Response.ErrorListener iErrorListener)
	{
		if (isServiceConnected())
		{
			setProgressDialog(true);
			
			getService().getController(SearchFlightController.class).GetSearchHistory(new RequestGetSearchHistory(UserData.getInstance().getUserID()), new Response.Listener<ResponseGetSearchHistory>()
			{
				@Override
				public void onResponse(final ResponseGetSearchHistory response)
				{
					if (iListener != null)
					{
						iListener.onResponse(response);
					}
					
					setProgressDialog(false);
				}
			}, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError error)
				{
					if (iErrorListener != null)
					{
						iErrorListener.onErrorResponse(error);
					}
					setProgressDialog(false);
				}
			});
		}
	}
	
	public void clearUserHistory(final Response.Listener<ResponseClearUserHistory> iListener, final Response.ErrorListener iErrorListener)
	{
		if (isServiceConnected())
		{
			setProgressDialog(true);
			
			getService().getController(SearchFlightController.class).ClearUserHistory(UserData.getInstance().getUserID(), new Response.Listener<ResponseClearUserHistory>()
			{
				@Override
				public void onResponse(final ResponseClearUserHistory response)
				{
					if (iListener != null)
					{
						iListener.onResponse(response);
					}
					
					setProgressDialog(false);
				}
			}, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError error)
				{
					if (iErrorListener != null)
					{
						iErrorListener.onErrorResponse(error);
					}
					setProgressDialog(false);
				}
			});
		}
	}
	
	private void getSearchGlobalData()
	{
		if (isServiceConnected())
		{
			setProgressDialog(true);
			
			final UserData userData = UserData.getInstance();
			
			getService().getController(SearchFlightController.class).GetSearchGlobalData(new RequestGetSearchGlobalData(userData.getUserID()), new Response.Listener<ResponseGetSearchGlobalData>()
			{
				@Override
				public void onResponse(final ResponseGetSearchGlobalData response)
				{
					if (response != null && response.getContent() != null)
					{
						SearchObject.getInstance().setResponseSearchGlobalData(response);
						setSearchGlobalDataByResponse(response.getContent());
						
						getService().getController(SearchFlightController.class)
						            .GetSearchDepartureCities(new RequestSearchDepartureCities(), new Response.Listener<ResponseSearchDepartureCities>()
						            {
							            @Override
							            public void onResponse(final ResponseSearchDepartureCities response)
							            {
								            setProgressDialog(false);
								            if (response != null && response.getContent() != null)
								            {
									            setSearchDepartureCitiesResponse(response.getContent());
								            }
								
								            loadSearchFlightFragment();
							            }
						            }, new Response.ErrorListener()
						            {
							            @Override
							            public void onErrorResponse(final VolleyError iError)
							            {
								            loadSearchFlightFragment();
								            ErrorsHandler.tryShowServiceErrorDialog(iError, SearchFlightActivity.this);
								            setProgressDialog(false);
							            }
						            });
					}
				}
			}, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError iError)
				{
					loadSearchFlightFragment();
					ErrorsHandler.tryShowServiceErrorDialog(iError, SearchFlightActivity.this);
					setProgressDialog(false);
				}
			});
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.GeneralError.getErrorMessage());
		}
	}
	
	private void setSearchDepartureCitiesResponse(ResponseSearchDepartureCitiesContent iContent)
	{
		SearchObject searchObject = SearchObject.getInstance();
		searchObject.setDepartureElalCitiesList(iContent.getDepartureCities());
	}
	
	private Date getBirth(int age)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.YEAR, -age);
		return calendar.getTime();
	}
	
	private void setSearchGlobalDataByResponse(final GetSearchGlobalDataContent iContent)
	{
		SearchObject searchObject = SearchObject.getInstance();
		searchObject.setFlightClassesList(iContent.getFlightClasses());
		searchObject.setUserFamilyList(iContent.getUserFamily());
		//				test();
	}
	
	private void test()
	{
		SearchObject searchObject = SearchObject.getInstance();
		
		//TODO Just for testing (delete it)
//		if (GlobalVariables.isDebugVersion())
//		{
//			searchObject.setUserFamilyList(new ArrayList<GetSearchGlobalDataContentUserFamily>()
//			{{
//				add(new GetSearchGlobalDataContentUserFamily("name1", 9991, 8881, getBirth(40), "lastName1", 1));
//				add(new GetSearchGlobalDataContentUserFamily("name2", 9992, 8882, getBirth(0), "lastName2", 1));
//				add(new GetSearchGlobalDataContentUserFamily("name3", 9993, 8883, getBirth(1), "lastName3", 1));
//			}});
//		}
	}
	
	
	@Override
	protected void onSaveInstanceState(final Bundle outState)
	{
		//No call for super(). Bug on API Level > 11.
//		super.onSaveInstanceState(outState);
	}
	
	public void loadSearchFlightFragment()
	{
		getFragmentSwapper().clearFragments();
		//		goToFragmentByFragmentClass(SearchDatesFragment.class, null, false);
		goToFragmentByFragmentClass(SearchFlightFragment.class, null, false);
		//		goToFragmentByFragmentClass(SearchPassengersFragment.class, null, false);
		//		goToFragmentByFragmentClass(SearchDestinationFragment.class, null, true);
	}
	
	@Override
	protected void onServiceConnected()
	{
		if (SearchObject.getInstance().getResponseSearchGlobalData() == null)
		{
			getSearchGlobalData();
			initDrawer();
		}
	}
	
	@Override
	public void onErrorResponse(final VolleyError iError)
	{
		ErrorsHandler.tryShowServiceErrorDialog(iError, SearchFlightActivity.this);
		setProgressDialog(false);
	}
	
	@Override
	public void onBackPressed()
	{
		if (isDrawerOpen())
		{
			closeDrawer();
		}
		else
		{
			checkDates();
			super.onBackPressed();
		}
	}
	
	private void checkDates()
	{
		SearchObject searchObject = SearchObject.getInstance();
		
		if (!searchObject.isCurrentDatesSelected())
		{
			searchObject.setCurrentFirstDateInSelection(null);
			searchObject.setCurrentLastDateInSelection(null);
		}
	}
}
