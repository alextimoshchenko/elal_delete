package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.UserData;

/**
 * Created by erline.katz on 04/09/2017.
 */

public class RequestCompletePnr
{
	@JsonProperty("UserId")
	private int mUserId;
	
	@JsonProperty("PNR")
	private String mPNRNum;
	
	@JsonProperty("LanguageCode")
	private String mLanguageCode = UserData.getInstance().getLanguage().getLanguageCode();
	
	public RequestCompletePnr(int iUserId, String iPNRNum)
	{
		mUserId = iUserId;
		mPNRNum = iPNRNum;
	}
	
	public RequestCompletePnr()
	{
	}
}
