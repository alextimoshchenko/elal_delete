package ui.customWidgets;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Created with care by Shahar Ben-Moshe on 26/12/16.
 */

public class ExpandCollapseAnimation extends Animation
{
	private View mView;
	private float mTargetHeight;
	private boolean mIsExpand = false;
	private int mMaxViewHeight = -1;
	
	public ExpandCollapseAnimation(final View iView, final boolean iIsExpand, final int iDuration)
	{
		mView = iView;
		mIsExpand = iIsExpand;
		setDuration(iDuration);
	}
	
	public ExpandCollapseAnimation(final View iView, final boolean iIsExpand, final int iDuration, final int maxViewHeight)
	{
		mView = iView;
		mIsExpand = iIsExpand;
		mMaxViewHeight = maxViewHeight;
		setDuration(iDuration);
	}
	
	@Override
	protected void applyTransformation(float interpolatedTime, Transformation t)
	{
		if (mIsExpand)
		{
			mView.getLayoutParams().height = interpolatedTime == 1 ? (int) mTargetHeight : (int) (mTargetHeight * interpolatedTime);
			mView.requestLayout();
		}
		else
		{
			if (interpolatedTime == 1)
			{
				mView.setVisibility(View.GONE);
			}
			else
			{
				mView.getLayoutParams().height = (int) (mTargetHeight - (int) (mTargetHeight * interpolatedTime));
				mView.requestLayout();
			}
		}
	}
	
	@Override
	public boolean willChangeBounds()
	{
		return true;
	}
	
	private void setVewVisibility()
	{
		if (mIsExpand)
		{
			mView.getLayoutParams().height = 1;
			mView.setVisibility(View.VISIBLE);
		}
	}
	
	private void setAnimation(final boolean iIsExpand)
	{
		mIsExpand = iIsExpand;
		mView.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//		mTargetHeight = mView.getMeasuredHeight() > mMaxViewHeight ? mMaxViewHeight : mView.getMeasuredHeight();//avihay: Old
		mTargetHeight = mView.getMeasuredHeight() > mMaxViewHeight ?  mView.getMeasuredHeight():mMaxViewHeight ;//avihay: New
		
	}
	
	public void reverseAndAnimate()
	{
		setAnimation(!mIsExpand);
		setVewVisibility();
		startAnimation();
	}
	
	private void startAnimation()
	{
		mView.startAnimation(this);
	}
	
	public boolean isExpand()
	{
		return mIsExpand;
	}
}
