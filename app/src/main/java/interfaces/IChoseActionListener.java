package interfaces;

import global.TypeOfTripElement;

public interface IChoseActionListener
{
	void onActionClick(TypeOfTripElement iChoseElement);
}
