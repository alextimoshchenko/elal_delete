package utils.global.enums;

import android.text.TextUtils;
import android.util.Log;

import global.ElAlApplication;
import il.co.ewave.elal.R;
import utils.errors.LocalError;
import utils.global.AppUtils;

/**
 * Created with care by Alexey.T on 12/07/2017.
 */
public enum eSideMenuItems
{
	MY_ACCOUNT(R.string.my_account, 1),
	MY_FLIGHT(R.string.my_flight, 2);
	
	private static String TAG = eSideMenuItems.class.getSimpleName();
	
	private int mNameOfItemResourceId;
	private int mItemId;
	
	eSideMenuItems(final int iNameOfItemResourceId, int iItemId)
	{
		mNameOfItemResourceId = iNameOfItemResourceId;
		mItemId = iItemId;
	}
	
	public String getItemName()
	{
		String result = ElAlApplication.getInstance().getString(mNameOfItemResourceId);
		
		if (TextUtils.isEmpty(result))
		{
			result = "";
			AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.NoSuchItems.getErrorMessage());
		}
		
		return result;
	}
	
	public int getNameOfItemResourceId()
	{
		return mNameOfItemResourceId;
	}
}
