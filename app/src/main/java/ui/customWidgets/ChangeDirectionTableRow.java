package ui.customWidgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TableRow;

import java.util.ArrayList;
import java.util.List;

import il.co.ewave.elal.R;
import utils.global.AppUtils;

//CustomWidgets.ChangeDirectionLinearLayout
public class ChangeDirectionTableRow extends TableRow
{
	private Context m_context;
	private boolean m_shouldFlipBackground = false;

	public ChangeDirectionTableRow(Context context)
	{
		super(context);
		this.m_context = context;
	}

	public ChangeDirectionTableRow(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		this.m_context = context;

		handleAttributes(attrs);
	}

	public ChangeDirectionTableRow(Context context, AttributeSet attrs, int defStyle)
	{
		super(context);
		this.m_context = context;
	}

	@SuppressLint("ObsoleteSdkInt")
	@Override
	protected void onFinishInflate()
	{
		super.onFinishInflate();
		
		boolean isRtl = !isInEditMode() && AppUtils.isDefaultLocaleRTL();
		
		if (isRtl && this.getChildCount() > 1)
		{
			List<View> views = new ArrayList<View>();

			for (int i = 0 ; i < this.getChildCount() ; i++)
			{
				views.add(this.getChildAt(i));
			}

			this.removeAllViews();

			for (int i = views.size() - 1 ; i >= 0 ; i--)
			{
				View view = views.get(i);
				//                LinearLayout.LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
				//                layoutParams.gravity = Gravity.RIGHT;
				//                view.setLayoutParams(layoutParams);
				this.addView(view);
			}
		}

		if (m_shouldFlipBackground)
		{
			Drawable bgDrawable = getBackground();

			if (bgDrawable != null && bgDrawable.getIntrinsicHeight() > 0)
			{
				Bitmap bitmap = Bitmap.createBitmap(bgDrawable.getIntrinsicWidth(), bgDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
				Canvas canvas = new Canvas(bitmap);
				bgDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
				bgDrawable.draw(canvas);

				Matrix matrix = new Matrix();
				matrix.preScale(-1.0f, 1.0f);

				Bitmap flippedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);

				if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN)
				{
					setBackgroundDrawable(new BitmapDrawable(m_context.getResources(), flippedBitmap));
				}
				else
				{
					setBackground(new BitmapDrawable(m_context.getResources(), flippedBitmap));
				}
			}
		}
	}

	private void handleAttributes(AttributeSet attrs)
	{
		TypedArray a = m_context.obtainStyledAttributes(attrs, R.styleable.CustomWidgets_ChangeDirectionLinearLayout);

		final int N = a.getIndexCount();

		for (int i = 0 ; i < N ; ++i)
		{
			int attr = a.getIndex(i);
			switch (attr)
			{
				case R.styleable.CustomWidgets_ChangeDirectionLinearLayout_FlipBackgroundOnRTL:
					m_shouldFlipBackground = a.getBoolean(attr, false);
					break;
			}
		}
		a.recycle();
	}
}