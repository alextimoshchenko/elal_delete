package webServices.requests;


import com.fasterxml.jackson.annotation.JsonProperty;

import global.UserData;

/**
 * Created with care by Shahar Ben-Moshe on 2/27/17.
 */
public class RequestGetWeather
{
//	@JsonProperty("UserId")
//	private Integer userId;
	@JsonProperty("DestinationIATA")
	private String destinationIATA;
	
	@JsonProperty("LanguageCode")
	private String mLanguageCode = UserData.getInstance().getLanguage().getLanguageCode();
	
	public RequestGetWeather(final Integer iUserId, final String iDestinationIATA)
	{
//		userId = iUserId;
		destinationIATA = iDestinationIATA;
	}
}