package ui.customWidgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;

import java.util.Locale;

import il.co.ewave.elal.R;

/**
 * Created with care by Shahar Ben-Moshe on 14/01/16.
 */
public class ChangeDirectionCheckbox extends android.support.v7.widget.AppCompatCheckBox
{
	private boolean mIsAutoDetectRtlByValue = true;
	private boolean mIsRTL = false;
	private Drawable mDrawable;
	
	public ChangeDirectionCheckbox(Context context)
	{
		super(context);
	}
	
	public ChangeDirectionCheckbox(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		handleAttributes(context, attrs, 0);
	}
	
	public ChangeDirectionCheckbox(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
		handleAttributes(context, attrs, defStyleAttr);
	}
	
	@Override
	public void setButtonDrawable(Drawable drawable)
	{
		super.setButtonDrawable(drawable);
		
		if (drawable != null)
		{
			mDrawable = drawable;
			setButtonDrawable(null);
			super.setButtonDrawable(getResources().getDrawable(R.drawable.transparent_shape));
			
			if (isCheckboxTextRtl())
			{
				setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
				this.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
			}
			else
			{
				setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
				this.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
			}
			setCompoundDrawablePadding(15);
		}
	}
	
	private boolean isCheckboxTextRtl()
	{
		boolean result;
		
		if (mIsAutoDetectRtlByValue)
		{
			char charRtlEvaluation;
			
			if (TextUtils.isEmpty(this.getText().toString()))
			{
				charRtlEvaluation = Locale.getDefault().getDisplayName().charAt(0);
			}
			else
			{
				charRtlEvaluation = getText().toString().charAt(0);
			}
			
			result = Character.getDirectionality(charRtlEvaluation) == Character.DIRECTIONALITY_RIGHT_TO_LEFT;
		}
		else
		{
			result = mIsRTL;
		}
		
		return result;
	}
	
	private void handleAttributes(Context context, AttributeSet attrs, int defStyle)
	{
		TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ChangeDirectionCheckbox, defStyle, 0);
		
		mIsAutoDetectRtlByValue = typedArray.getBoolean(R.styleable.ChangeDirectionCheckbox_isAutoDetectRtlByValue, true);
		mIsRTL = typedArray.getBoolean(R.styleable.ChangeDirectionCheckbox_isRTL, false);
		
		setButtonDrawable(mDrawable);
		
		typedArray.recycle();
	}
}
