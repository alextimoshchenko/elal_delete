package webServices.responses.getSearchDepartureCities;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import webServices.responses.getSearchGlobalData.GetSearchGlobalDataContentElalCities;

/**
 * Created with care by Alexey.T on 17/07/2017.
 */
public class ResponseSearchDepartureCitiesContent
{
	@JsonProperty("DepartureCities")
	private List<GetSearchGlobalDataContentElalCities> mDepartureCities;
	
	public List<GetSearchGlobalDataContentElalCities> getDepartureCities()
	{
		return mDepartureCities;
	}
}
