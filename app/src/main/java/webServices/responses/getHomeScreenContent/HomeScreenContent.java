package webServices.responses.getHomeScreenContent;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by Avishay.Peretz on 25/06/2017.
 */

public class HomeScreenContent
{
	@JsonProperty("Top1Content")
	private HomeScreenItem mTop1Item;
	@JsonProperty("Top2Content")
	private HomeScreenItem mTop2Item;
	@JsonProperty("Middle1Content")
	private HomeScreenItem mMiddle1Item;
	@JsonProperty("Middle2Content")
	private HomeScreenItem mMiddle2Item;
	@JsonProperty("BottomContent")
	private HomeScreenItem mBottomItem;
	@JsonProperty("PNRCount")
	private int mPNRCount;
	@JsonProperty("MatmidPoints")
	private String mMatmidPoints;
	@JsonProperty("MatmidTypeID")
	private int mMatmidTypeID;
	
	public HomeScreenContent()
	{
	}
	
	public HomeScreenItem getTop1Item()
	{
		return mTop1Item;
	}
	
	public HomeScreenItem getTop2Item()
	{
		return mTop2Item;
	}
	
	public HomeScreenItem getMiddle1Item()
	{
		return mMiddle1Item;
	}
	
	public HomeScreenItem getMiddle2Item()
	{
		return mMiddle2Item;
	}
	
	public HomeScreenItem getBottomItem()
	{
		return mBottomItem;
	}
	
	public int getPNRCount()
	{
		return mPNRCount;
	}
	
	public String getMatmidPoints()
	{
		return mMatmidPoints;
	}
	
	public int getMatmidTypeID()
	{
		return mMatmidTypeID;
	}
}
