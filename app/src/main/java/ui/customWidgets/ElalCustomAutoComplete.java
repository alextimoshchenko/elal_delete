package ui.customWidgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import global.UserData;
import global.eLanguage;
import il.co.ewave.elal.R;
import ui.adapters.CountryCodesAdapter;
import utils.global.AppUtils;
import utils.global.CustomTextWatcher;
import webServices.responses.getMobileCountryCodes.MobileCountryCode;

public class ElalCustomAutoComplete extends LinearLayout
{
	public static final String includeCharacterSet = "><+-|%=&!*_.?: ";
	private Context mContext;
	private AutoCompleteTextView mEditText;
	private TextView mTvHint;
	private String mOriginHintText = "";
	private boolean mIsRTL;
	private ArrayList<MobileCountryCode> mSearchDataArray = new ArrayList<>();
	
	
	public ElalCustomAutoComplete(final Context context)
	{
		super(context);
		mContext = context;
	}
	
	public ElalCustomAutoComplete(final Context context, @Nullable final AttributeSet attrs)
	{
		super(context, attrs);
		mContext = context;
	}
	
	public ElalCustomAutoComplete(final Context context, @Nullable final AttributeSet attrs, final int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
		mContext = context;
	}
	
	public void init(final String iDefaultText)
	{
		initReference();
		setError(false);
		setDefaultHintText(iDefaultText);
		setTvHintVisibility(false);
	}
	
	public void init(final CharSequence iDefaultText)
	{
		initReference();
		setDefaultHintText(iDefaultText);
		setTvHintVisibility(false);
	}
	
	private void initReference()
	{
		try
		{
			mEditText = (AutoCompleteTextView) this.getChildAt(1);
			mTvHint = (TextView) this.getChildAt(0);
			
//			mEditText.addTextChangedListener(new AutoCompleteSpaceTextWatcher(mEditText));
			setListeners();
			setRTL();
		}
		catch (Exception iE)
		{
			iE.printStackTrace();
		}
		
	}
	
	public void setDefaultHintText(final CharSequence iDefText)
	{
		if (iDefText != null)
		{
			mOriginHintText = iDefText.toString();
			mEditText.setHint(iDefText);
			setTvHintText(iDefText);
		}
	}
	
	/**
	 * set the float label visibility
	 **/
	private void setTvHintVisibility(final boolean iVisible)
	{
		if (iVisible)
		{
			mTvHint.setVisibility(VISIBLE);
		}
		else
		{
			mTvHint.setVisibility(INVISIBLE);
		}
		
	}
	
	private void setListeners()
	{
		if (mEditText != null)
		{
			mEditText.addTextChangedListener(new CustomTextWatcher()
			{

				@Override
				public void onTextChanged(final CharSequence s, final int start, final int before, final int count)
				{
					if (s.length() > 0)
					{
						setTvHintVisibility(true);
					}
					else
					{
						setTvHintVisibility(false);
					}
				}

				@Override
				public void afterTextChanged(final Editable s)
				{
					setError(false, null);
				}
			});
		}
	}
	
	private void setRTL()
	{
		boolean isRtl = !isInEditMode() && AppUtils.isDefaultLocaleRTL();
		setErrorTextRTL(isRtl);
		setEditTextGravity(isRtl);
	}
	
	/**
	 * set the float label hint text
	 **/
	public void setTvHintText(final CharSequence iText)
	{
		if (iText == null)
		{
			mTvHint.setText("");
		}
		else
		{
			if (mEditText.getText().toString().length() > 0)
			{
				mTvHint.setText(iText);
			}
			else
			{
				mTvHint.setText(mOriginHintText);
			}
		}
	}
	
	public void setError(final boolean iState, final String iText)
	{
		if (iState)
		{
			setTvHintText(iText);
			setTvHintColor(mContext.getResources().getColor(R.color.red));
			setTvHintVisibility(true);
			
			setUnderlineColor(mContext.getResources().getColor(R.color.red));
			
		}
		else
		{
			setTvHintText(mOriginHintText);
			setTvHintColor(mContext.getResources().getColor(R.color.pinkish_grey));
			//			setTvHintVisibility(true);
			
			clearUnderlineColor();
			
		}
	}
	
	protected void setErrorTextRTL(final boolean iIsRTL)
	{
		if (mTvHint != null)
		{
			mTvHint.setGravity(iIsRTL ? Gravity.RIGHT : Gravity.LEFT);
		}
	}
	
	public void setEditTextGravity(final boolean iIsRTL)
	{
		if (mEditText != null)
		{
			mEditText.setGravity(iIsRTL ? Gravity.RIGHT : Gravity.LEFT);
		}
	}
	
	/**
	 * set the float label hint text
	 **/
	public void setTvHintText(final String iText)
	{
		if (iText == null)
		{
			mTvHint.setText("");
		}
		else
		{
			if (mEditText.getText().toString().length() > 0)
			{
				mTvHint.setText(iText);
			}
			else
			{
				mTvHint.setText(mOriginHintText);
			}
		}
	}
	
	public void setTvHintColor(int iResId)
	{
		mTvHint.setTextColor(iResId);
	}
	
	public void setUnderlineColor(final int iUnderlineColor)
	{
		if (mEditText != null)
		{
			//			ColorStateList colorStateList = ColorStateList.valueOf(iUnderlineColor);
			//			ViewCompat.setBackgroundTintList(getEditText(), colorStateList);
			mEditText.getBackground().setColorFilter(iUnderlineColor, PorterDuff.Mode.SRC_ATOP);
		}
	}
	
	private void clearUnderlineColor()
	{
		if (mEditText != null)
		{
			mEditText.getBackground().clearColorFilter();
		}
	}
	
	public AutoCompleteTextView getAutoComplete()
	{
		return mEditText;
	}
	
	public TextView getTvHintTextView()
	{
		//		if (mTvHint == null)
		//		{
		//			mTvHint = (TextView) findViewById(R.id.tv_hint_custom_til);
		//		}
		return mTvHint;
	}
	
	public void setError(final boolean iState)
	{
		setError(iState, mOriginHintText);
	}
	
	private void setTvHintTextAppearance(@StyleRes int resId)
	{
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
		{
			mTvHint.setTextAppearance(resId);
		}
		else
		{
			mTvHint.setTextAppearance(mContext, resId);
		}
	}
	
	public void setDefaultHintText(final String iDefText)
	{
		if (iDefText != null)
		{
			mOriginHintText = iDefText;
			mEditText.setHint(mOriginHintText);
			setTvHintText(mOriginHintText);
		}
	}
	
	@Override
	protected void onFinishInflate()
	{
		super.onFinishInflate();
		
		setRTL();
	}
	
	public void setEditTextInputType(final int iClass, final int iType)
	{
		if (mEditText != null)
		{
			mEditText.setInputType(iClass | iType | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
			mEditText.setText("");
		}
		
		setRTL();
	}
	
	public void setInputTypeNumeric()
	{
		if (mEditText != null)
		{
			mEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
			mEditText.setText("");
		}
		
		setRTL();
	}
	
	public String getOriginHintText()
	{
		return mOriginHintText;
	}
	
	public void setInputTypePassword()
	{
		if (mEditText != null)
		{
			mEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
			
		}
		
		setRTL();
	}
	
	public void setInputTypePhone()
	{
		if (mEditText != null)
		{
			mEditText.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
			
			int maxLengthOfEditText = 18;
			mEditText.setFilters(new InputFilter[] {new InputFilter.LengthFilter(maxLengthOfEditText)});
			mEditText.setText("");
		}
		
		setRTL();
	}
	
	public void setInputTypeEmail()
	{
		if (mEditText != null)
		{
			mEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
			mEditText.setText("");
		}
		
		setRTL();
	}
	
	public void setInputTypeTextOnly()
	{
		if (mEditText != null)
		{
			InputFilter filtertxt = new InputFilter()
			{
				public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend)
				{
					if (end > 0)
					{
						char c = source.charAt(end - 1);
						if (c == ' ')
						{
							//							return " ";
							return source;
						}
						if (c == '\'')
						{
							return source;
						}
						if (!Character.isLetter(c))
						{
							if (end > 1)
							{
								return source.subSequence(start, end - 1);
							}
							else
							{
								return "";
							}
						}
					}
					return null;
					
					
					//					if (source.equals(" "))
					//					{
					//						return " ";
					//					}
					//					if (source.equals("'"))
					//					{
					//						return "'";
					//					}
					//					if (!TextUtils.isEmpty(source) && (!Character.isLetter(source.charAt(0)) /*|| !AppUtils.isStringContainsLatinCharactersOnly(source.toString())*/))
					//					{
					//						return "";
					//					}
					//
					//					return null;
				}
			};
			
			mEditText.setFilters(new InputFilter[] {filtertxt});
			mEditText.setText("");
		}
		
		setRTL();
	}
	
	
	private void handleAttributes(Context iContext, AttributeSet attrs)
	{
		TypedArray typedArray = iContext.obtainStyledAttributes(attrs, R.styleable.ElalCustomTilLL);
		
		final int N = typedArray.getIndexCount();
		
		for (int i = 0 ; i < N ; ++i)
		{
			int attr = typedArray.getIndex(i);
			switch (attr)
			{
				case R.styleable.ElalCustomTilLL_isECTilRTL:
					mIsRTL = typedArray.getBoolean(attr, false);
					break;
			}
		}
		typedArray.recycle();
	}
	
	public void setCountryCodesData(ArrayList<MobileCountryCode> iSearchDataArray)
	{
		mSearchDataArray = iSearchDataArray;
		//Used to specify minimum number of
		//characters the user has to type in order to display the drop down hint.
		mEditText.setThreshold(1);
		
		CountryCodesAdapter arrayAdapter = new CountryCodesAdapter(mContext, android.R.layout.select_dialog_item, mSearchDataArray);
		mEditText.setAdapter(arrayAdapter);
	}
	
}

//class AutoCompleteSpaceTextWatcher implements TextWatcher
//{
//	private AutoCompleteTextView editText;
//
//	AutoCompleteSpaceTextWatcher(AutoCompleteTextView editText)
//	{
//		this.editText = editText;
//	}
//
//	@Override
//	public void beforeTextChanged(CharSequence s, int start, int count, int after)
//	{
//
//	}
//
//	@Override
//	public void onTextChanged(CharSequence s, int start, int before, int count)
//	{
//		String text = editText.getText().toString();
//		if (!TextUtils.isEmpty(text))
//		{
//			if (text.startsWith(" "))
//			{
//				editText.setText(text.trim());
//			}
//		}
//	}
//
//	@Override
//	public void afterTextChanged(Editable s)
//	{
//
//	}
//}
