package utils.global;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import global.UserData;
import global.eLanguage;
import interfaces.ISearchElement;

/**
 * Created with care by Shahar Ben-Moshe on 23/07/2015.
 */
public class FilterList<T>
{
	private ArrayList<T> mCompleteList;
	private ArrayList<T> mFilteredList;
	
	public FilterList(List<T> iList)
	{
		mCompleteList = new ArrayList<>(iList);
		mFilteredList = new ArrayList<>(iList);
	}
	
	public FilterList()
	{
		mCompleteList = new ArrayList<>();
		mFilteredList = new ArrayList<>();
	}
	
	public void filterListByParameters(IFilter<T> iFilter)
	{
		mFilteredList.clear();
		
		for (T object : mCompleteList)
		{
			if (iFilter.filterByParams(object))
			{
				mFilteredList.add(object);
			}
		}
	}
	
	public void sortAlphabeticAsync(final String iStr, final IOnFilterCompleteListener iOnFilterCompleteListener)
	{
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				if (iStr.length() > 1)
				{
					sortAlphabetic(iStr);
					
					if (iOnFilterCompleteListener != null)
					{
						iOnFilterCompleteListener.onFilterComplete();
					}
				}
			}
		}).start();
	}
	
	private void sortAlphabetic(String iStr)
	{
		mFilteredList.clear();
		List<T> completeListCopy = new ArrayList<>(mCompleteList);
		eLanguage language = UserData.getInstance().getLanguage();
		
		//startsWithItems
		List<T> startsWithItems = getAllItemsStartsWith(iStr, completeListCopy);
		sortAllCities(startsWithItems, language);
//		startsWithItems.rev
		completeListCopy.removeAll(startsWithItems);
		
		//containsItems
		List<T> containsItems = getAllItemsContains(iStr, completeListCopy);
		sortAllCities(containsItems, language);
		completeListCopy.removeAll(containsItems);
		
		//result
		mFilteredList.addAll(startsWithItems);
		mFilteredList.addAll(containsItems);
	}
	
	private List<T> getAllItemsStartsWith(final String iStartsWith, final List<T> iListForSearch)
	{
		List<T> result = new ArrayList<>();
		
		for (int i = 0 ; i < iListForSearch.size() ; i++)
		{
			T currentItem = iListForSearch.get(i);
			
			String currentLine;
			
			if (UserData.getInstance().getLanguage().isHebrew() && AppUtils.isStringContainsHebrewCharactersOnly(iStartsWith))
			{
				currentLine = ((ISearchElement) currentItem).getElementNameHe().toLowerCase();
			}
			else
			{
				currentLine = ((ISearchElement) currentItem).getAliasText().toLowerCase();
			}
			
			if (currentLine.startsWith(iStartsWith.toLowerCase()))
			{
				result.add(currentItem);
			}
		}
		
		return result;
	}
	
	@NonNull
	private List<T> getAllItemsContains(final String iStr, final List<T> iCompleteListCopy)
	{
		List<T> containsItems = new ArrayList<>();
		
		for (int i = 0 ; i < iCompleteListCopy.size() ; i++)
		{
			T currentItem = iCompleteListCopy.get(i);
			String currentLine = ((ISearchElement) currentItem).getAliasText().toLowerCase();
			
			if (currentLine.contains(iStr.toLowerCase()))
			{
				containsItems.add(currentItem);
			}
		}
		
		return containsItems;
	}
	
	private void sortAllCities(final List<T> iList, final eLanguage iLanguage)
	{
		Collections.sort(iList, new Comparator<T>()
		{
			@Override
			public int compare(final T o1, final T o2)
			{
				int result = 0;
				ISearchElement searchElementFirst = (ISearchElement) o1;
				ISearchElement searchElementSecond = (ISearchElement) o2;
				
				if (iLanguage.isHebrew())
				{
					result = searchElementSecond.getElementNameHe().compareToIgnoreCase(searchElementFirst.getElementNameHe());
				}
				else if (iLanguage.isEnglish())
				{
					result = searchElementSecond.getElementNameEn().compareToIgnoreCase(searchElementFirst.getElementNameEn());
				}
				
				return result;
			}
		});
	}
	
	public void filterListByParametersAsync(final IFilter<T> iFilter, final IOnFilterCompleteListener iOnFilterCompleteListener)
	{
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				filterListByParameters(iFilter);
				if (iOnFilterCompleteListener != null)
				{
					iOnFilterCompleteListener.onFilterComplete();
				}
			}
		}).start();
	}
	
	public ArrayList<T> getCompleteList()
	{
		return mCompleteList;
	}
	
	public ArrayList<T> getFilteredList()
	{
		return mFilteredList;
	}
	
	public void clearList()
	{
		mCompleteList.clear();
		mFilteredList.clear();
	}
	
	public void startNewList(List<T> iList)
	{
		clearList();
		mCompleteList.addAll(iList);
		mFilteredList.addAll(iList);
	}
	
	
	public interface IFilter<T>
	{
		boolean filterByParams(T iFilterableObject);
	}
	
	public interface IOnFilterCompleteListener
	{
		void onFilterComplete();
	}
}