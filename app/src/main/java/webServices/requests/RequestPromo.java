package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.UserData;

/**
 * Created by Shahar Ben-Moshe on 5/20/15.
 */
public class RequestPromo
{
	@JsonProperty("UDID")
	private String mUdid = UserData.getInstance().getUdid();
	
	@JsonProperty("LanguageCode")
	private String mLanguageCode = UserData.getInstance().getLanguage().getLanguageCode();
	
	public RequestPromo(String iUdid)
	{
		mUdid = iUdid;
	}
	
	public RequestPromo()
	{
	}
}