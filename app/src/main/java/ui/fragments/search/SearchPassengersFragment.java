package ui.fragments.search;

import android.app.Activity;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.tagmanager.DataLayer;

import java.util.List;

import global.IntentExtras;
import il.co.ewave.elal.R;
import ui.activities.EWBaseActivity;
import ui.customWidgets.ChangeDirectionLinearLayout;
import ui.customWidgets.ExpandCollapseAnimation;
import ui.fragments.EWBaseFragment;
import ui.fragments.WebViewFragment;
import utils.errors.ErrorsHandler;
import utils.errors.LocalError;
import utils.errors.LocalError.eLocalError;
import utils.global.AppUtils;
import utils.global.enums.eFamilyRelation;
import utils.global.enums.eIntention;
import utils.global.enums.ePassengersType;
import utils.global.enums.eTabStatement;
import utils.global.searchFlightObjects.SearchObject;
import webServices.global.RequestStringBuilder;
import webServices.responses.getSearchGlobalData.GetSearchGlobalDataContentUserFamily;

public class SearchPassengersFragment extends EWBaseFragment implements Response.ErrorListener
{
	private static final int EXPAND_DURATION = 500;
	private final String TAG = SearchFlightFragment.class.getSimpleName();
	private LinearLayout mLlFamilyContainer, mLlAdditionalTypes;
	private ConstraintLayout mClFamilyMembersHeader;
	private ExpandCollapseAnimation mFamilyContainerExpandCollapseAnimation, mAdditionalTypesExpandCollapseAnimation, mFamilyBlockErrorExpandCollapseAnimation, mTvErrorTeenagersExpandCollapseAnimation, mTvErrorChildrenExpandCollapseAnimation, mTvErrorInfantsExpandCollapseAnimation;
	private boolean isOpenFamilyContainer = true;
	private Button mBtnOk;
	private ImageView mIvExpandFamily, mIvBackArrow, mIvAdultsMinus, mIvAdultsPlus, mIvChildrenMinus, mIvChildrenPlus, mIvChildrenToolTip, mIvInfantsMinus, mIvInfantsPlus, mIvInfantsToolTip, mIvTeenagersMinus, mIvTeenagersPlus, mIvYouthMinus, mIvYouthPlus;
	private TextView mTvNumberIndicator, mTvAdultsName, mTvAdultsQuantity, mTvChildrenName, mTvChildrenQuantity, mTvErrorChildren, mTvInfantsName, mTvInfantsQuantity, mTvErrorInfants, mTvYouthName, mTvYouthQuantity, mTvTeenagersName, mTvTeenagersQuantity, mTvErrorTeenagers, mTvAction, mTvFamilyBlockError;
	private View mClInfants;
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_search_passengers_layout);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		initReference();
		setListeners();
		initFamilyContainerView();
		setExpandableViews();
		setGui();
		checkIfQuantityOfFamilyValid();
		setContentDescription();
	}
	
	private void setContentDescription()
	{
		String plus = getString(R.string.plus);
		String minus = getString(R.string.minus);
		
		//Adults
		mIvAdultsMinus.setContentDescription(minus);
		mIvAdultsPlus.setContentDescription(plus);
		
		//Children
		mIvChildrenMinus.setContentDescription(minus);
		mIvChildrenPlus.setContentDescription(plus);
		
		//Infants
		mIvInfantsMinus.setContentDescription(minus);
		mIvInfantsPlus.setContentDescription(plus);
		
		//Teenagers
		mIvTeenagersMinus.setContentDescription(minus);
		mIvTeenagersPlus.setContentDescription(plus);
		
		//Youth
		mIvYouthMinus.setContentDescription(minus);
		mIvYouthPlus.setContentDescription(plus);
	}
	
	private void setTvErrorChildren(boolean iIsShow)
	{
		mTvErrorChildren.setText(eLocalError.AdultsChildren.getErrorMessage());
		animateCollapseExpandOfErrorChildren(iIsShow);
		
		animateCollapseExpandOfErrorTeenagers(false);
		animateCollapseExpandOfErrorInfants(false);
	}
	
	//	private void setTvErrorInfantsAward(boolean iIsShow)
	//	{
	//		mTvErrorInfants.setText(eLocalError.InfantsAward.getErrorMessage());
	//		animateCollapseExpandOfErrorInfants(iIsShow);
	//
	//		animateCollapseExpandOfErrorTeenagers(false);
	//		animateCollapseExpandOfErrorChildren(false);
	//	}
	
	private void setTvErrorInfants(boolean iIsShow)
	{
		mTvErrorInfants.setText(eLocalError.AdultsInfants.getErrorMessage());
		animateCollapseExpandOfErrorInfants(iIsShow);
		
		animateCollapseExpandOfErrorTeenagers(false);
		animateCollapseExpandOfErrorChildren(false);
	}
	
	private void setTvErrorTeenagers(boolean iIsShow)
	{
		mTvErrorTeenagers.setText(eLocalError.AdultsTeenagers.getErrorMessage());
		animateCollapseExpandOfErrorTeenagers(iIsShow);
		
		animateCollapseExpandOfErrorInfants(false);
		animateCollapseExpandOfErrorChildren(false);
	}
	
	private void setOffPassengersErrors()
	{
		setTvErrorTeenagers(false);
		setTvErrorInfants(false);
		//		setTvErrorInfantsAward(false);
		setTvErrorChildren(false);
		
		setOffFamilyBlockError();
	}
	
	private void initReference()
	{
		Activity act = getActivity();
		
		mTvAction = act.findViewById(R.id.tv_search_view_action);
		mLlAdditionalTypes = act.findViewById(R.id.ll_search_view_additional_types);
		mBtnOk = act.findViewById(R.id.btn_search_view_ok);
		mLlFamilyContainer = act.findViewById(R.id.ll_search_passengers_family_container);
		mIvExpandFamily = act.findViewById(R.id.iv_search_passengers_expand_family);
		mTvNumberIndicator = act.findViewById(R.id.tv_search_passengers_number_indicator);
		mClFamilyMembersHeader = act.findViewById(R.id.ll_search_passengers_family_members_header);
		mIvBackArrow = act.findViewById(R.id.iv_name_screen_back_arrow);
		mTvFamilyBlockError = act.findViewById(R.id.tv_search_passengers_family_error);
		
		//Adults
		View clAdults = act.findViewById(R.id.cl_custom_up_down_vote_adults);
		mTvAdultsName = clAdults.findViewById(R.id.tv_custom_up_down_vote_name);
		mTvAdultsQuantity = clAdults.findViewById(R.id.tv_custom_up_down_vote_quantity);
		mIvAdultsMinus = clAdults.findViewById(R.id.iv_custom_up_down_vote_minus);
		mIvAdultsPlus = clAdults.findViewById(R.id.iv_custom_up_down_vote_plus);
		
		//Children
		View clChildren = act.findViewById(R.id.cl_custom_up_down_vote_children);
		mTvChildrenName = clChildren.findViewById(R.id.tv_custom_up_down_vote_name);
		mTvChildrenQuantity = clChildren.findViewById(R.id.tv_custom_up_down_vote_quantity);
		mIvChildrenMinus = clChildren.findViewById(R.id.iv_custom_up_down_vote_minus);
		mIvChildrenPlus = clChildren.findViewById(R.id.iv_custom_up_down_vote_plus);
		mIvChildrenToolTip = clChildren.findViewById(R.id.iv_custom_up_down_vote_tool_tip);
		mTvErrorChildren = clChildren.findViewById(R.id.tv_custom_up_down_vote_error);
		
		//Infants
		mClInfants = act.findViewById(R.id.cl_custom_up_down_vote_infants);
		mTvInfantsName = mClInfants.findViewById(R.id.tv_custom_up_down_vote_name);
		mTvInfantsQuantity = mClInfants.findViewById(R.id.tv_custom_up_down_vote_quantity);
		mIvInfantsMinus = mClInfants.findViewById(R.id.iv_custom_up_down_vote_minus);
		mIvInfantsPlus = mClInfants.findViewById(R.id.iv_custom_up_down_vote_plus);
		mIvInfantsToolTip = mClInfants.findViewById(R.id.iv_custom_up_down_vote_tool_tip);
		mTvErrorInfants = mClInfants.findViewById(R.id.tv_custom_up_down_vote_error);
		
		//Teenagers
		View clTeenagers = act.findViewById(R.id.cl_custom_up_down_vote_teenagers);
		mTvTeenagersName = clTeenagers.findViewById(R.id.tv_custom_up_down_vote_name);
		mTvTeenagersQuantity = clTeenagers.findViewById(R.id.tv_custom_up_down_vote_quantity);
		mIvTeenagersMinus = clTeenagers.findViewById(R.id.iv_custom_up_down_vote_minus);
		mIvTeenagersPlus = clTeenagers.findViewById(R.id.iv_custom_up_down_vote_plus);
		mTvErrorTeenagers = clTeenagers.findViewById(R.id.tv_custom_up_down_vote_error);
		
		//Youth
		View clYouth = act.findViewById(R.id.cl_custom_up_down_vote_youth);
		mTvYouthName = clYouth.findViewById(R.id.tv_custom_up_down_vote_name);
		mTvYouthQuantity = clYouth.findViewById(R.id.tv_custom_up_down_vote_quantity);
		mIvYouthMinus = clYouth.findViewById(R.id.iv_custom_up_down_vote_minus);
		mIvYouthPlus = clYouth.findViewById(R.id.iv_custom_up_down_vote_plus);
	}
	
	private void setListeners()
	{
		mTvAction.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				if (mAdditionalTypesExpandCollapseAnimation.isExpand())
				{
					doOnAboutPassengersTypesClick();
				}
				else
				{
					doOnActionClick();
					setGui();
				}
			}
		});
		
		mIvYouthPlus.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnPlusClickByType(ePassengersType.YOUTH);
			}
		});
		
		mIvYouthMinus.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnMinusClickByType(ePassengersType.YOUTH);
				setGui();
			}
		});
		
		mIvTeenagersPlus.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnPlusClickByType(ePassengersType.TEENAGERS);
			}
		});
		
		mIvTeenagersMinus.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnMinusClickByType(ePassengersType.TEENAGERS);
				setGui();
			}
		});
		
		mIvInfantsPlus.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnPlusClickByType(ePassengersType.INFANTS);
			}
		});
		
		mIvInfantsMinus.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnMinusClickByType(ePassengersType.INFANTS);
				setGui();
			}
		});
		
		mIvInfantsToolTip.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnInfantsToolTipClick();
			}
		});
		
		mIvChildrenPlus.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnPlusClickByType(ePassengersType.CHILDREN);
			}
		});
		
		mIvChildrenMinus.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnMinusClickByType(ePassengersType.CHILDREN);
				setGui();
			}
		});
		
		mIvChildrenToolTip.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnChildrenToolTipClick();
			}
		});
		
		mIvAdultsPlus.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnPlusClickByType(ePassengersType.ADULTS);
			}
		});
		
		mIvAdultsMinus.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnMinusClickByType(ePassengersType.ADULTS);
				setGui();
			}
		});
		
		mBtnOk.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnOkClick();
			}
		});
		
		mIvBackArrow.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnBackClick();
			}
		});
		
		mIvExpandFamily.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				setFamilyExpendableButtonDrawable();
				doOnExpandFamilyClick();
				isOpenFamilyContainer = !isOpenFamilyContainer;
			}
		});
	}
	
	private void doOnAboutPassengersTypesClick()
	{
		Bundle bundle = new Bundle();
		bundle.putString(IntentExtras.URL, RequestStringBuilder.getElalAboutPassengerTypesUrl());
		tryGoToFragmentByFragmentClass(WebViewFragment.class, bundle, true);
	}
	
	private void doOnActionClick()
	{
		animateCollapseExpandOfAdditionalTypes(true);
	}
	
	private void popQuantityError()
	{
		ErrorsHandler.tryShowLocalErrorDialog(new LocalError(eLocalError.MaxPassengersQuantity), getActivity());
	}
	
	private void popAdultsErrorByType(ePassengersType iType)
	{
		if (iType.isInfants())
		{
			setTvErrorInfants(true);
		}
		else if (iType.isChildren())
		{
			setTvErrorChildren(true);
		}
		else if (iType.isTeenagers())
		{
			setTvErrorTeenagers(true);
		}
	}
	
	private void doOnChildrenToolTipClick()
	{
		AppUtils.showToolTip(getContext(), mIvChildrenToolTip, Gravity.BOTTOM, getString(R.string.children_tool_tip));
	}
	
	private void doOnInfantsToolTipClick()
	{
		AppUtils.showToolTip(getContext(), mIvInfantsToolTip, Gravity.BOTTOM, getString(R.string.infants_tool_tip));
	}
	
	private void doOnOkClick()
	{
		String str;
		
		if (SearchObject.getInstance().getSelectedUserFamilyMembersQuantity() > 0)
		{
			str = "Add Family Member";
		}
		else
		{
			str = "Add Passenger Member";
		}
		
		//event 28
		((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Flight Search - Actions", "Action", str, "Label", "Add"));
		
		if (isFieldsValid())
		{
			tryPopFragment();
			tryClearFragments();
		}
	}
	
	private boolean isFieldsValid()
	{
		boolean result = true;
		
		SearchObject searchObject = SearchObject.getInstance();
		
		int selectedAdultsAndYouthQuantity = searchObject.getSelectedAdultsAndYouthQuantity();
		int infantsQuantity = searchObject.getSelectedPassengersQuantityByType(ePassengersType.INFANTS);
		
		//Passengers exist adults
		if (!searchObject.isContainsUserFamilyListSelectedAdultsOrYouthAtLeastOne())
		{
			if (searchObject.isUserHimselfIs(ePassengersType.INFANTS))
			{
				setOnFamilyBlockError(eLocalError.AdultsInfants.getErrorMessage());
			}
			else if (searchObject.isUserHimselfIs(ePassengersType.CHILDREN))
			{
				setOnFamilyBlockError(eLocalError.AdultsChildren.getErrorMessage());
			}
			else if (searchObject.isUserHimselfIs(ePassengersType.TEENAGERS))
			{
				setOnFamilyBlockError(eLocalError.AdultsTeenagers.getErrorMessage());
			}
			
			result = false;
		}
		else if (!(selectedAdultsAndYouthQuantity >= infantsQuantity))
		{
			result = false;
		}
		
		return result;
	}
	
	private void checkIfQuantityOfFamilyValid()
	{
		/** Here I am just checking if all quantity members are valid and if not setting error message to corresponding field within `isValidIncreaseActionByPassengerType()` method */
		if (!passengersSelected())
		{
			setOffFamilyBlockError();
		}
		else if (isValidIncreaseActionByPassengerType(ePassengersType.INFANTS, eFamilyRelation.FAMILY_ONLY, eIntention.DECREASE, true))
		{
			if (isValidIncreaseActionByPassengerType(ePassengersType.CHILDREN, eFamilyRelation.FAMILY_ONLY, eIntention.DECREASE, true))
			{
				if (isValidIncreaseActionByPassengerType(ePassengersType.TEENAGERS, eFamilyRelation.FAMILY_ONLY, eIntention.DECREASE, true))
				{
				}
			}
		}
		else if (isValidIncreaseActionByPassengerType(ePassengersType.TEENAGERS, eFamilyRelation.FAMILY_ONLY))
		{
		}
		else if (isValidIncreaseActionByPassengerType(ePassengersType.CHILDREN, eFamilyRelation.FAMILY_ONLY))
		{
		}
		else if (isValidIncreaseActionByPassengerType(ePassengersType.INFANTS, eFamilyRelation.REGULAR_ONLY, eIntention.DECREASE, true))
		{
		}
		else if (isValidIncreaseActionByPassengerType(ePassengersType.TEENAGERS, eFamilyRelation.REGULAR_ONLY))
		{
		}
		else if (isValidIncreaseActionByPassengerType(ePassengersType.CHILDREN, eFamilyRelation.REGULAR_ONLY))
		{
		}
		//		else if (isValidIncreaseActionByPassengerType(ePassengersType.INFANTS, eFamilyRelation.FAMILY_ONLY, eIntention.DECREASE, true))
		//		{
		//		}
		//		else if (isValidIncreaseActionByPassengerType(ePassengersType.TEENAGERS, eFamilyRelation.FAMILY_ONLY))
		//		{
		//		}
		//		else if (isValidIncreaseActionByPassengerType(ePassengersType.CHILDREN, eFamilyRelation.FAMILY_ONLY))
		//		{
		//		}
		else
		{
			setOffFamilyBlockError();
		}
	}
	
	private boolean passengersSelected()
	{
		SearchObject searchObject = SearchObject.getInstance();
		return searchObject != null && searchObject.getFamilyMembersQuantity() > 0;
	}
	
	private boolean isValidDecreaseActionByPassengerType(ePassengersType iType, eFamilyRelation iFamilyRelation, boolean iIsShouldDecrease)
	{
		boolean result = true;
		
		SearchObject searchObject = SearchObject.getInstance();
		
		if (iType.isAdults() || iType.isYouth())
		{
			//Infants
			if (searchObject.getSelectedPassengersQuantityByType(ePassengersType.INFANTS) > 0 && !searchObject.isEnoughAdultsMembersForType(ePassengersType.INFANTS, eIntention.DECREASE))
			{
				if (iIsShouldDecrease)
				{
					searchObject.deleteOneNotFamilyMemberFromListByType(ePassengersType.INFANTS);
				}
				
				if (iFamilyRelation.isFamilyOnly())
				{
					setOnFamilyBlockError(eLocalError.AdultsInfants.getErrorMessage());
				}
				else
				{
					popAdultsErrorByType(ePassengersType.INFANTS);
				}
				
				result = false;
			}
			else if (searchObject.getSelectedPassengersQuantityByType(ePassengersType.TEENAGERS) > 0 && !searchObject.isEnoughAdultsMembersForType(ePassengersType.TEENAGERS))
			{
				//Teenagers
				if (iIsShouldDecrease)
				{
					searchObject.clearAllNotFamilyByType(ePassengersType.TEENAGERS);
				}
				
				if (iFamilyRelation.isFamilyOnly())
				{
					setOnFamilyBlockError(eLocalError.AdultsTeenagers.getErrorMessage());
				}
				else
				{
					popAdultsErrorByType(ePassengersType.TEENAGERS);
				}
				
				result = false;
			}
			else if (searchObject.getSelectedPassengersQuantityByType(ePassengersType.CHILDREN) > 0 && !searchObject.isEnoughAdultsMembersForType(ePassengersType.CHILDREN))
			{
				//Children
				if (iIsShouldDecrease)
				{
					searchObject.clearAllNotFamilyByType(ePassengersType.CHILDREN);
				}
				
				if (iFamilyRelation.isFamilyOnly())
				{
					setOnFamilyBlockError(eLocalError.AdultsChildren.getErrorMessage());
				}
				else
				{
					popAdultsErrorByType(ePassengersType.CHILDREN);
				}
				
				result = false;
			}
		}
		
		return result;
	}
	
	private boolean isValidIncreaseActionByPassengerType(ePassengersType iType, eFamilyRelation iFamilyRelation)
	{
		return isValidIncreaseActionByPassengerType(iType, iFamilyRelation, eIntention.INCREASE, false);
	}
	
	private boolean isValidIncreaseActionByPassengerType(ePassengersType iType, eFamilyRelation iFamilyRelation, eIntention iIntention, boolean iIsFirstChecking)
	{
		boolean result = true;
		
		SearchObject searchObject = SearchObject.getInstance();
		
		if (iType.isNoSuchItem())
		{
			AppUtils.printLog(Log.ERROR, TAG, eLocalError.NoSuchItems.getErrorMessage());
		}
		
		//Infants
		if (iType.isInfants() && searchObject.getCurrentTabStatement() == eTabStatement.AWARD_TICKET && !iIsFirstChecking)
		{
			setOnFamilyBlockError(eLocalError.InfantsAward.getErrorMessage());
			result = false;
		}
		else if (iType.isInfants() && !searchObject.isEnoughAdultsMembersForType(ePassengersType.INFANTS, iIntention))
		{
			if (iFamilyRelation.isFamilyOnly())
			{
				setOnFamilyBlockError(eLocalError.AdultsInfants.getErrorMessage());
			}
			else
			{
				popAdultsErrorByType(ePassengersType.INFANTS);
			}
			
			result = false;
		}
		
		//Teenagers
		if (iType.isTeenagers() && !searchObject.isEnoughAdultsMembersForType(ePassengersType.TEENAGERS))
		{
			if (iFamilyRelation.isFamilyOnly())
			{
				setOnFamilyBlockError(eLocalError.AdultsTeenagers.getErrorMessage());
			}
			else
			{
				popAdultsErrorByType(ePassengersType.TEENAGERS);
			}
			
			result = false;
		}
		
		//Children
		if (iType.isChildren() && !searchObject.isEnoughAdultsMembersForType(ePassengersType.CHILDREN))
		{
			if (iFamilyRelation.isFamilyOnly())
			{
				setOnFamilyBlockError(eLocalError.AdultsChildren.getErrorMessage());
			}
			else
			{
				popAdultsErrorByType(ePassengersType.CHILDREN);
			}
			
			result = false;
		}
		
		//Max quantity
		if (!iIsFirstChecking)
		{
			if (searchObject.getSelectedPassengersQuantity() == searchObject.MAX_PASSENGERS_QUANTITY)
			{
				if (iFamilyRelation.isFamilyOnly())
				{
					setOnFamilyBlockError(eLocalError.MaxPassengersQuantity.getErrorMessage());
				}
				else
				{
					popQuantityError();
				}
				
				result = false;
			}
		}
		
		return result;
	}
	
	private void doOnPlusClickByType(ePassengersType iType)
	{
		SearchObject searchObject = SearchObject.getInstance();
		
		if (isValidIncreaseActionByPassengerType(iType, eFamilyRelation.REGULAR_ONLY))
		{
			if (iType.isAdults() || iType.isYouth())
			{
				setOffFamilyBlockError();
				setOffPassengersErrors();
			}
			else if (iType.isChildren() && searchObject.getCurrentTabStatement() == eTabStatement.AWARD_TICKET)
			{
				setOffFamilyBlockError();
			}
			
			searchObject.addOneNotFamilySelectedMemberToListByType(iType);
			setGui();
		}
	}
	
	private void doOnMinusClickByType(ePassengersType iType)
	{
		SearchObject searchObject = SearchObject.getInstance();
		searchObject.deleteOneNotFamilyMemberFromListByType(iType);
		
		if (!isValidDecreaseActionByPassengerType(iType, eFamilyRelation.ALL_KINDS, false))
		//		if (!isValidDecreaseActionByPassengerType(iType, eFamilyRelation.REGULAR_ONLY, false))
		{
			searchObject.addOneNotFamilySelectedMemberToListByType(iType);
		}
		else
		{
			if (iType.isAdults() || iType.isYouth())
			{
				setOffFamilyBlockError();
				setOffPassengersErrors();
			}
			else if (iType.isInfants())
			{
				animateCollapseExpandOfErrorInfants(false);
				setOffFamilyBlockError();
			}
			else if (iType.isTeenagers())
			{
				animateCollapseExpandOfErrorTeenagers(false);
			}
			else if (iType.isChildren())
			{
				animateCollapseExpandOfErrorChildren(false);
			}
			else if (searchObject.getSelectedPassengersQuantity() == 0)
			{
				setOffPassengersErrors();
			}
		}
	}
	
	private void setGui()
	{
		((TextView) getActivity().findViewById(R.id.tv_name_screen_title)).setText(getString(R.string.search_passengers_header));
		
		SearchObject searchObject = SearchObject.getInstance();
		
		if (searchObject.getFamilyMembersQuantity() > 0)
		{
			mClFamilyMembersHeader.setVisibility(View.VISIBLE);
			int selectedUserFamilyMembers = searchObject.getSelectedUserFamilyMembersQuantity();
			if (selectedUserFamilyMembers > 0)
			{
				mTvNumberIndicator.setVisibility(View.VISIBLE);
				mTvNumberIndicator.setText(String.valueOf(selectedUserFamilyMembers));
			}
			else
			{
				mTvNumberIndicator.setVisibility(View.INVISIBLE);
			}
		}
		else
		{
			mClFamilyMembersHeader.setVisibility(View.GONE);
		}
		
		if (mAdditionalTypesExpandCollapseAnimation != null)
		{
			mTvAction.setText(mAdditionalTypesExpandCollapseAnimation.isExpand() ? getString(R.string.about_passenger_types) : getString(R.string.more_passenger_types));
			mTvAction.setPaintFlags(mTvAction.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
		}
		//Adults
		mTvAdultsName.setText(getString(R.string.adults));
		int regularAdultsQuantity = SearchObject.getInstance().getSelectedPassengersQuantityByType(ePassengersType.ADULTS, eFamilyRelation.REGULAR_ONLY);
		mTvAdultsQuantity.setText(String.valueOf(regularAdultsQuantity));
		
		//Children
		mIvChildrenToolTip.setVisibility(View.VISIBLE);
		mTvChildrenName.setText(getString(R.string.children_2_11));
		int regularChildrenQuantity = SearchObject.getInstance().getSelectedPassengersQuantityByType(ePassengersType.CHILDREN, eFamilyRelation.REGULAR_ONLY);
		mTvChildrenQuantity.setText(String.valueOf(regularChildrenQuantity));
		
		//Infants
		mIvInfantsToolTip.setVisibility(View.VISIBLE);
		mTvInfantsName.setText(getString(R.string.infants_0_2));
		int regularInfantsQuantity = SearchObject.getInstance().getSelectedPassengersQuantityByType(ePassengersType.INFANTS, eFamilyRelation.REGULAR_ONLY);
		mTvInfantsQuantity.setText(String.valueOf(regularInfantsQuantity));
		
		//Teenagers
		mTvTeenagersName.setText(getString(R.string.teenagers_12_14));
		int regularTeenagersQuantity = SearchObject.getInstance().getSelectedPassengersQuantityByType(ePassengersType.TEENAGERS, eFamilyRelation.REGULAR_ONLY);
		mTvTeenagersQuantity.setText(String.valueOf(regularTeenagersQuantity));
		
		//Youth
		mTvYouthName.setText(getString(R.string.youth_15_27));
		int regularYouthQuantity = SearchObject.getInstance().getSelectedPassengersQuantityByType(ePassengersType.YOUTH, eFamilyRelation.REGULAR_ONLY);
		mTvYouthQuantity.setText(String.valueOf(regularYouthQuantity));
		
		eTabStatement tabStatement = searchObject.getCurrentTabStatement();
		if (tabStatement == eTabStatement.AWARD_TICKET) // if getting bonus ticket
		{
			// TODO: 25/02/2018 - remove babies category
			// TODO: 25/02/2018 - do not allow selecting a baby family member
			mTvAction.setVisibility(View.GONE);
			mClInfants.setVisibility(View.GONE);
		}
		else
		{
			mTvAction.setVisibility(View.VISIBLE);
			mClInfants.setVisibility(View.VISIBLE);
		}
		
		mBtnOk.setEnabled(searchObject.isContainsUserFamilyListSelectedAdultsOrYouthAtLeastOne());
	}
	
	private void setExpandableViews()
	{
		mLlFamilyContainer.post(new Runnable()
		{
			@Override
			public void run()
			{
				int expandedViewHeight = mLlFamilyContainer.getHeight();
				mFamilyContainerExpandCollapseAnimation = new ExpandCollapseAnimation(mLlFamilyContainer, false, EXPAND_DURATION, expandedViewHeight);
			}
		});
		
		mAdditionalTypesExpandCollapseAnimation = new ExpandCollapseAnimation(mLlAdditionalTypes, false, EXPAND_DURATION, mLlAdditionalTypes.getHeight() + 150);
		mTvErrorChildrenExpandCollapseAnimation = new ExpandCollapseAnimation(mTvErrorChildren, false, EXPAND_DURATION, mTvErrorChildren.getHeight() + 150);
		mTvErrorTeenagersExpandCollapseAnimation = new ExpandCollapseAnimation(mTvErrorTeenagers, false, EXPAND_DURATION, mTvErrorTeenagers.getHeight() + 150);
		mTvErrorInfantsExpandCollapseAnimation = new ExpandCollapseAnimation(mTvErrorInfants, false, EXPAND_DURATION, mTvErrorInfants.getHeight() + 150);
		mFamilyBlockErrorExpandCollapseAnimation = new ExpandCollapseAnimation(mTvFamilyBlockError, false, EXPAND_DURATION, mTvFamilyBlockError.getHeight() + 150);
	}
	
	private void setOffFamilyBlockError()
	{
		setTextToFamilyBlockErrorAndOpenIfNeed(null, false);
	}
	
	private void setOnFamilyBlockError(String iText)
	{
		setTextToFamilyBlockErrorAndOpenIfNeed(iText, true);
	}
	
	private void setTextToFamilyBlockErrorAndOpenIfNeed(String iText, boolean iIsShow)
	{
		if (mTvFamilyBlockError == null)
		{
			return;
		}
		
		mTvFamilyBlockError.setText(iText);
		
		animateCollapseExpandOfFamilyBlockError(iIsShow);
	}
	
	private void animateCollapseExpandOfFamilyContainer(final boolean iShouldExpand)
	{
		if ((iShouldExpand && !mFamilyContainerExpandCollapseAnimation.isExpand()) || (!iShouldExpand && mFamilyContainerExpandCollapseAnimation.isExpand()))
		{
			mFamilyContainerExpandCollapseAnimation.reverseAndAnimate();
		}
	}
	
	private void animateCollapseExpandOfAdditionalTypes(final boolean iShouldExpand)
	{
		if ((iShouldExpand && !mAdditionalTypesExpandCollapseAnimation.isExpand()) || (!iShouldExpand && mAdditionalTypesExpandCollapseAnimation.isExpand()))
		{
			mAdditionalTypesExpandCollapseAnimation.reverseAndAnimate();
		}
	}
	
	private void animateCollapseExpandOfErrorChildren(final boolean iShouldExpand)
	{
		if ((iShouldExpand && !mTvErrorChildrenExpandCollapseAnimation.isExpand()) || (!iShouldExpand && mTvErrorChildrenExpandCollapseAnimation.isExpand()))
		{
			mTvErrorChildrenExpandCollapseAnimation.reverseAndAnimate();
		}
	}
	
	private void animateCollapseExpandOfErrorInfants(final boolean iShouldExpand)
	{
		if ((iShouldExpand && !mTvErrorInfantsExpandCollapseAnimation.isExpand()) || (!iShouldExpand && mTvErrorInfantsExpandCollapseAnimation.isExpand()))
		{
			mTvErrorInfantsExpandCollapseAnimation.reverseAndAnimate();
		}
	}
	
	private void animateCollapseExpandOfFamilyBlockError(final boolean iShouldExpand)
	{
		if ((iShouldExpand && !mFamilyBlockErrorExpandCollapseAnimation.isExpand()) || (!iShouldExpand && mFamilyBlockErrorExpandCollapseAnimation.isExpand()))
		{
			mFamilyBlockErrorExpandCollapseAnimation.reverseAndAnimate();
		}
	}
	
	private void animateCollapseExpandOfErrorTeenagers(final boolean iShouldExpand)
	{
		if ((iShouldExpand && !mTvErrorTeenagersExpandCollapseAnimation.isExpand()) || (!iShouldExpand && mTvErrorTeenagersExpandCollapseAnimation.isExpand()))
		{
			mTvErrorTeenagersExpandCollapseAnimation.reverseAndAnimate();
		}
	}
	
	private void initFamilyContainerView()
	{
		List<GetSearchGlobalDataContentUserFamily> userFamilyList = SearchObject.getInstance().getUserFamilyList();
		
		for (int i = 0 ; i < userFamilyList.size() ; i++)
		{
			mLlFamilyContainer.addView(getCheckBoxFamilyMemberView(userFamilyList.get(i)));
		}
	}
	
	@NonNull
	private ChangeDirectionLinearLayout getCheckBoxFamilyMemberView(final GetSearchGlobalDataContentUserFamily iUserFamily)
	{
		View familyMemberCheckBoxLayout = LayoutInflater.from(getContext()).inflate(R.layout.family_member_check_box_layout, null);
		ChangeDirectionLinearLayout cdllMemberCheckBoxRoot = familyMemberCheckBoxLayout.findViewById(R.id.cdll_member_check_box_root);
		TextView tvMember = cdllMemberCheckBoxRoot.findViewById(R.id.tv_member_check_box);
		tvMember.setText(iUserFamily.getUserFullName());
		CheckBox cbMember = cdllMemberCheckBoxRoot.findViewById(R.id.cb_member_check_box);
		cbMember.setChecked(iUserFamily.isSelectedForSearching());
		
		cbMember.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			{
				if (mFamilyBlockErrorExpandCollapseAnimation.isExpand())
				{
					mFamilyBlockErrorExpandCollapseAnimation.reverseAndAnimate();
				}
				doOnCheckBoxClick(buttonView, isChecked, iUserFamily);
				setGui();
			}
		});
		
		return cdllMemberCheckBoxRoot;
	}
	
	private void doOnCheckBoxClick(final CompoundButton iCheckBox, final boolean isChecked, final GetSearchGlobalDataContentUserFamily iUserFamily)
	{
		SearchObject searchObject = SearchObject.getInstance();
		ePassengersType iPassengerType = searchObject.getPassengersTypeByExistingPassenger(iUserFamily);
		
		if (isChecked)
		{
			if (!isValidIncreaseActionByPassengerType(iPassengerType, eFamilyRelation.FAMILY_ONLY))
			{
				iUserFamily.setSelectedForSearching(false);
				iCheckBox.setChecked(false);
				return;
			}
			
			if (iPassengerType.isAdults() || iPassengerType.isYouth())
			{
				setOffPassengersErrors();
				setOffFamilyBlockError();
			}
		}
		else
		{
			//Because if you unchecked infants from family block you also need clear error from infants in regular block
			if (iPassengerType.isInfants())
			{
				animateCollapseExpandOfErrorInfants(false);
			}
		}
		
		iUserFamily.setSelectedForSearching(isChecked);
		
		if (!isValidDecreaseActionByPassengerType(iPassengerType, eFamilyRelation.FAMILY_ONLY, false))
		{
			iUserFamily.setSelectedForSearching(true);
			iCheckBox.setChecked(true);
		}
	}
	
	private void doOnBackClick()
	{
		setOffPassengersErrors();
		tryPopFragment();
	}
	
	private void setFamilyExpendableButtonDrawable()
	{
		Drawable familyDrawable;
		
		if (isOpenFamilyContainer)
		{
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
			{
				familyDrawable = getResources().getDrawable(R.mipmap.search_open_plus, getContext().getTheme());
			}
			else
			{
				familyDrawable = getResources().getDrawable(R.mipmap.search_open_plus);
			}
		}
		else
		{
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
			{
				familyDrawable = getResources().getDrawable(R.mipmap.search_close_minus, getContext().getTheme());
			}
			else
			{
				familyDrawable = getResources().getDrawable(R.mipmap.search_close_minus);
			}
		}
		
		mIvExpandFamily.setImageDrawable(familyDrawable);
	}
	
	private void doOnExpandFamilyClick()
	{
		animateCollapseExpandOfFamilyContainer(!isOpenFamilyContainer);
	}
	
	@Override
	public void onErrorResponse(final VolleyError error)
	{
		
	}
}
