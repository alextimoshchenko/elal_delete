package ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import global.UserData;
import il.co.ewave.elal.R;
import ui.customWidgets.TouchableWebView;
import utils.errors.ErrorsHandler;
import utils.global.AppUtils;
import webServices.controllers.MainController;
import webServices.global.JacksonRequest;
import webServices.requests.RequestGetToSeeLinkByDestination;
import webServices.responses.ResponseGetToSeeLinkByDestination;

import static global.IntentExtras.DESTINATION_IATA;

/**
 * Created with care by Shahar Ben-Moshe on 31/01/17.
 */

public class ToSeeFragment extends EWBaseFragment implements Response.ErrorListener
{
	private String mUrl;
	private TouchableWebView mWebView;
	private LinearLayout mLlProgressDialog;
	private String mDestinationIata;
	private ImageView mIvBottomBackArrow;
	
	public static Bundle createBundleForFragment(String iDestinationIata)
	{
		Bundle result = new Bundle();
		
		result.putString(DESTINATION_IATA, iDestinationIata);
		
		return result;
	}
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_to_see_layout);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		getExtras();
		initReference();
		setWebView();
		setProgressBar(false);
		setListeners();
	}

	private void setListeners()
	{
		mIvBottomBackArrow.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnBackBottomArrowClick();
			}
		});
	}

	private void doOnBackBottomArrowClick()
	{
		if (mWebView.canGoBack())
		{
			mWebView.goBack();
		}
	}
	
	private void getExtras()
	{
		Bundle extras = getArguments();
		
		if (extras != null)
		{
			mDestinationIata = extras.getString(DESTINATION_IATA);
		}
	}
	
	private void initReference()
	{
		mLlProgressDialog = (LinearLayout) getActivity().findViewById(R.id.ll_toSee_loader);
		mWebView = (TouchableWebView) getActivity().findViewById(R.id.wv_toSee_webView);
		mIvBottomBackArrow = (ImageView) getActivity().findViewById(R.id.iv_touchable_web_view_bottom_back_arrow);
	}
	
	private void setWebView()
	{
		mWebView.setWebViewClient(new WebViewClient()
		{
			
			public WebResourceResponse shouldInterceptRequest (final WebView view, String url)
			{
				return super.shouldInterceptRequest(view, url);
			}
			
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url)
			{
				if (!TextUtils.isEmpty(url) && !url.startsWith("js:"))
				{
					view.loadUrl(url);
					
					if (url.toLowerCase().contains(".pdf"))
					{
						view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
					}
				}
				
				return true;
			}
			
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon)
			{
				setProgressBar(true);
				
				super.onPageStarted(view, url, favicon);
			}
			
			@Override
			public void onPageFinished(WebView view, String url)
			{
				Activity activity = getActivity();
				if(activity != null && isAdded())
				{
					setProgressBar(false);
					Resources res = activity.getResources();

					if (mWebView.canGoBack())
					{
						mIvBottomBackArrow.setVisibility(View.VISIBLE);
						mIvBottomBackArrow.setImageResource(R.mipmap.back);
						final int unableColor = res.getColor(R.color.midnight_blue_two);
						mIvBottomBackArrow.setColorFilter(unableColor, PorterDuff.Mode.SRC_ATOP);
					}
					else
					{
						mIvBottomBackArrow.setVisibility(View.GONE);
						final int disableColor = res.getColor(R.color.ElalLightGray);
						mIvBottomBackArrow.setColorFilter(disableColor, PorterDuff.Mode.SRC_ATOP);
					}
				}
				
				super.onPageFinished(view, url);
			}
		});
		
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.setWebChromeClient(new WebChromeClient());
		mWebView.getSettings().setDomStorageEnabled(true);
		mWebView.getSettings().setBuiltInZoomControls(true);
		mWebView.getSettings().setDisplayZoomControls(false);
		
		mWebView.getSettings().setUserAgentString(JacksonRequest.USER_AGENT_CONTENT);
		mWebView.setFocusable(false);
	}
	
	private void setProgressBar(boolean iIsShow)
	{
		mLlProgressDialog.setVisibility(iIsShow ? View.VISIBLE : View.GONE);
	}
	
	@Override
	public void onServiceConnected()
	{
		getToSeeLinkByDestination();
	}
	
	private void getToSeeLinkByDestination()
	{
		if (isServiceConnected())
		{
			trySetProgressDialog(true);
			getService().getController(MainController.class)
			            .getGetToSeeLinkByDestination(new RequestGetToSeeLinkByDestination(UserData.getInstance()
			                                                                                       .getUserID(), mDestinationIata), new Response.Listener<ResponseGetToSeeLinkByDestination>()
			            {
				            @Override
				            public void onResponse(final ResponseGetToSeeLinkByDestination iResponse)
				            {
					            if (iResponse != null)
					            {
						            mUrl = iResponse.getContent();
						            loadUrl();
					            }
					            else
					            {
						            // TODO: 20/02/17 Shahar handle error?
					            }
					
					            trySetProgressDialog(false);
				            }
			            }, this);
		}
	}
	
	private void loadUrl()
	{
		if (!TextUtils.isEmpty(mUrl))
		{
			mWebView.loadUrl(AppUtils.addHttpToStringIfNeed(mUrl));
		}
	}
	
	@Override
	public void onErrorResponse(final VolleyError error)
	{
		trySetProgressDialog(false);
		ErrorsHandler.tryShowServiceErrorDialog(error, getActivity());
	}
}
