package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with care by Alexey.T on 16/07/2017.
 * <p>
 * TODO: Add a class header comment!
 */
public class RequestLogout
{
	@JsonProperty("UDID")
	private String mUDID;
	
	public RequestLogout(final String iUDID)
	{
		mUDID = iUDID;
	}
}
