package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.UserData;

public class RequestResetPassword
{
	@JsonProperty("Email")
	private String mEmail;
	@JsonProperty("LanguageCode")
	private String mLanguageCode = UserData.getInstance().getLanguage().getLanguageCode();
	public RequestResetPassword(String iEmail)
	{
		mEmail = iEmail;
	}
}
