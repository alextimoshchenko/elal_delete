package webServices.responses.getSearchGlobalData;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetSearchGlobalDataContentElalSearchOptions
{
	@JsonProperty("CityCode")
	private String mCityCode;
	@JsonProperty("DestinationCodes")
	private String mDestinationCodes;
	
	public String getCityCode()
	{
		return this.mCityCode;
	}
	
	public String getDestinationCodes()
	{
		return this.mDestinationCodes;
	}
	
	public GetSearchGlobalDataContentElalSearchOptions()
	{
	}
}
