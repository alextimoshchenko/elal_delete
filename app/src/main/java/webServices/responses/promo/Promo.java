package webServices.responses.promo;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * Created by Shahar Ben-Moshe on 5/26/15.
 */
public class Promo implements Parcelable
{
	@JsonProperty("PromoID")
	private int mPromoID;
	@JsonProperty("PromoName")
	private String mPromoName;
	@JsonProperty("StartDate")
	private Date mStartDate;
	@JsonProperty("EndDate")
	private Date mEndDate;
	@JsonProperty("Url")
	private String mUrl;
	@JsonProperty("Image")
	private String mImage;
	@JsonProperty("IsActive")
	private boolean mIsActive;
	@JsonProperty("CreateDate")
	private Date mCreateDate;
	@JsonProperty("LastModified")
	private Date mLastModified;
	@JsonProperty("LanguageCode")
	private String mLanguageCode;
	@JsonProperty("RepresentationID")
	private int mRepresentationID;
	
	public Promo()
	{
	}
	
	public String getImage()
	{
		return mImage;
	}
	
	public String getUrl()
	{
		return mUrl;
	}
	
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeInt(this.mPromoID);
		dest.writeString(this.mPromoName);
		dest.writeLong(this.mStartDate != null ? this.mStartDate.getTime() : -1);
		dest.writeLong(this.mEndDate != null ? this.mEndDate.getTime() : -1);
		dest.writeString(this.mUrl);
		dest.writeString(this.mImage);
		dest.writeByte(this.mIsActive ? (byte) 1 : (byte) 0);
		dest.writeLong(this.mCreateDate != null ? this.mCreateDate.getTime() : -1);
		dest.writeLong(this.mLastModified != null ? this.mLastModified.getTime() : -1);
		dest.writeString(this.mLanguageCode);
		dest.writeInt(this.mRepresentationID);
	}
	
	protected Promo(Parcel in)
	{
		this.mPromoID = in.readInt();
		this.mPromoName = in.readString();
		long tmpMStartDate = in.readLong();
		this.mStartDate = tmpMStartDate == -1 ? null : new Date(tmpMStartDate);
		long tmpMEndDate = in.readLong();
		this.mEndDate = tmpMEndDate == -1 ? null : new Date(tmpMEndDate);
		this.mUrl = in.readString();
		this.mImage = in.readString();
		this.mIsActive = in.readByte() != 0;
		long tmpMCreateDate = in.readLong();
		this.mCreateDate = tmpMCreateDate == -1 ? null : new Date(tmpMCreateDate);
		long tmpMLastModified = in.readLong();
		this.mLastModified = tmpMLastModified == -1 ? null : new Date(tmpMLastModified);
		this.mLanguageCode = in.readString();
		this.mRepresentationID = in.readInt();
	}
	
	public static final Parcelable.Creator<Promo> CREATOR = new Parcelable.Creator<Promo>()
	{
		@Override
		public Promo createFromParcel(Parcel source)
		{
			return new Promo(source);
		}
		
		@Override
		public Promo[] newArray(int size)
		{
			return new Promo[size];
		}
	};
}
