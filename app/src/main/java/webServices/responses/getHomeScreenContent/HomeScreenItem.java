package webServices.responses.getHomeScreenContent;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

import utils.global.ParseUtils;

/**
 * Created by Avishay.Peretz on 25/06/2017.
 */
public class HomeScreenItem
{
	
	@JsonProperty("HomeContentName")
	private String mHomeContentName;
	@JsonProperty("HomeContentId")
	private int mHomeContentId;
	@JsonProperty("HomeContentNameEng")
	private String mHomeContentNameEng;
	@JsonProperty("HomeContentNameHeb")
	private String mHomeContentNameHeb;
	@JsonProperty("Description")
	private String mDescription;
	@JsonProperty("InApp")
	private boolean mIsInApp;
	@JsonProperty("Link")
	private String mLink;
	@JsonProperty("IsConst")
	private boolean mIsConst;
	@JsonProperty("ContentLocation")
	private int mContentLocation;
	@JsonProperty("IsActive")
	private boolean mIsActive;
	@JsonProperty("CreateDate")
	private Date mCreateDate;
	@JsonProperty("LastModified")
	private String mLastModified;
	@JsonProperty("LanguageCode")
	private String mLanguageCode;
	@JsonProperty("IsNewWindow")
	private boolean mIsNewWindow;
	@JsonProperty("IsDeleted")
	private boolean mIsDeleted;
	@JsonProperty("IsMatmid")
	private boolean mIsMatmid;
	@JsonProperty("AppContentTypeId")
	private int mAppContentTypeId;
	 

	
	
	public HomeScreenItem()
	{
	}
	
	public boolean isInApp()
	{
		return mIsInApp;
	}
	
	public String getHomeContentName()
	{
		return this.mHomeContentName;
	}
	
	public int getAppContentTypeId()
	{
		return this.mAppContentTypeId;
	}
	
	public String getLink()
	{
		return this.mLink;
	}
	
	public int getLinkInt()
	{
		return ParseUtils.tryParseStringToIntegerOrDefault(mLink, 0);
	}
	
	public int getHomeContentId()
	{
		return mHomeContentId;
	}
	
	public String getHomeContentNameEng()
	{
		return mHomeContentNameEng;
	}
	
	public String getHomeContentNameHeb()
	{
		return mHomeContentNameHeb;
	}
	
	public String getDescription()
	{
		return mDescription;
	}
	
	public boolean isConst()
	{
		return mIsConst;
	}
	
	public int getContentLocation()
	{
		return mContentLocation;
	}
	
	public boolean isActive()
	{
		return mIsActive;
	}
	
	public Date getCreateDate()
	{
		return mCreateDate;
	}
	
	public String getLastModified()
	{
		return mLastModified;
	}
	
	public String getLanguageCode()
	{
		return mLanguageCode;
	}
	
	public boolean isNewWindow()
	{
		return mIsNewWindow;
	}
	
	public boolean isDeleted()
	{
		return mIsDeleted;
	}
	
	public boolean isMatmid()
	{
		return mIsMatmid;
	}
}
