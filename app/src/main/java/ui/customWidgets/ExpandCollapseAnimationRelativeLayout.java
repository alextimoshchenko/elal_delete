package ui.customWidgets;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.RelativeLayout;

/**
 * Created by Avishay.Peretz on 08/06/2017.
 */

public class ExpandCollapseAnimationRelativeLayout
{
	
	public static void expand(final View iView, final int iDuration) {
		iView.measure(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		final int targetHeight = iView.getMeasuredHeight();
		
		// Older versions of android (pre API 21) cancel animations for views with a height of 0.
		iView.getLayoutParams().height = 1;
		iView.setVisibility(View.VISIBLE);
		Animation a = new Animation()
		{
			@Override
			protected void applyTransformation(float interpolatedTime, Transformation t) {
				iView.getLayoutParams().height = interpolatedTime == 1
				                             ? RelativeLayout.LayoutParams.WRAP_CONTENT
				                             : (int)(targetHeight * interpolatedTime);
				iView.requestLayout();
			}
			
			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};
		
		// 1dp/ms
		a.setDuration(iDuration/*(int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density)*/);
		iView.startAnimation(a);
	}
	
	public static void collapse(final View iView, final int iDuration) {
		final int initialHeight = iView.getMeasuredHeight();
		
		Animation a = new Animation()
		{
			@Override
			protected void applyTransformation(float interpolatedTime, Transformation t) {
				if(interpolatedTime == 1){
					iView.setVisibility(View.GONE);
				}else{
					iView.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
					iView.requestLayout();
				}
			}
			
			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};
		
		// 1dp/ms
		a.setDuration(iDuration/*(int)(initialHeight / iView.getContext().getResources().getDisplayMetrics().density)*/);
		iView.startAnimation(a);
	}
}
