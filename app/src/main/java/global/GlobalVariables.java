package global;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import il.co.ewave.elal.BuildConfig;

/**
 * Created with care by Shahar Ben-Moshe on 13/12/16.
 */

public class GlobalVariables
{
	/**
	 * If you need to use this variable in true mode just switch the flavor to `debugWithTrueDebugMode` in `Build Variant` tab
	 */
	public static final Boolean isDebugVersion = BuildConfig.IS_DEBUG_MODE;
	public static final Boolean isTamperDetection = BuildConfig.IS_TAMPER_DETECTION;
	
	//region OS type
	public static final int OS_IOS = 1;
	public static final int OS_ANDROID = 2;
	//endregion
	
	//region OCR type
	public static final int OCR_TYPE_PASSPORT = 1;
	public static final int OCR_TYPE_ISRAELI_DRIVING_LICENSE = 2;
	
	@Retention(RetentionPolicy.SOURCE)
	@IntDef({OCR_TYPE_PASSPORT, OCR_TYPE_ISRAELI_DRIVING_LICENSE})
	public @interface OcrType
	{
	}
	//endregion
	
	//google analytics
	public static final String GTM_CONTAINER_ID = "GTM-KM7VBL";
}
