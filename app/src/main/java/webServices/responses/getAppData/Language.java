package webServices.responses.getAppData;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Language
{
	@JsonProperty("LanguageCode")
	private String mLanguageCode;
	@JsonProperty("LanguageNameHeb")
	private String mLanguageNameHeb;
	@JsonProperty("LanguageName")
	private String mLanguageName;
	@JsonProperty("LanguageNameEng")
	private String mLanguageNameEng;
	
	public String getLanguageCode()
	{
		return this.mLanguageCode;
	}
	
	public String getLanguageNameHeb()
	{
		return this.mLanguageNameHeb;
	}
	
	public String getLanguageName()
	{
		return this.mLanguageName;
	}
	
	public String getLanguageNameEng()
	{
		return this.mLanguageNameEng;
	}
	
	public Language()
	{
	}
	
	@Override
	public String toString()
	{
		return mLanguageName;
	}
}
