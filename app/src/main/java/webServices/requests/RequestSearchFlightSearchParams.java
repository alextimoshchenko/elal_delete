package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

import utils.global.DateTimeUtils;
import utils.global.LocaleUtils;
import utils.global.searchFlightObjects.SearchObject;

public class RequestSearchFlightSearchParams
{
	@JsonProperty("FlightSearchTypeID")
	private int mFlightSearchTypeID;
	@JsonProperty("UserID")
	private int mUserID;
	@JsonProperty("GoldenAge")
	private int mGoldenAge;
	
	@JsonProperty("Young")
	private int mYoung;
	@JsonProperty("Students")
	private int mStudents;
	@JsonProperty("FlightClassID")
	private int mFlightClassID;
	@JsonProperty("DepartureCityCode")
	private String mDepartureCityCode;
	@JsonProperty("Babies")
	private int mBabies;
	@JsonProperty("Adults")
	private int mAdults;
	@JsonProperty("Childs")
	private int mChilds;
	@JsonProperty("Teens")
	private int mTeens;
	@JsonProperty("ArrivalCityCode")
	private String mArrivalCityCode;
	@JsonProperty("DepartureDate")
//	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeUtils.DATE_FORMAT_yyyy_MM_dd)
	private String mDepartureDate;
	@JsonProperty("ArrivalDate")
//	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeUtils.DATE_FORMAT_yyyy_MM_dd)
	private String mArrivalDate;
	@JsonProperty("FlightType")
	private String mFlightType = SearchObject.getInstance().getCurrentTabStatement().getStatementCode();
	
	public RequestSearchFlightSearchParams(int iYoung, int iChild, int iBabies, int iGoldenAge, int iTeens, int iAdults, int iStudents, String iDepartureCityCode, String iArrivalCityCode,
	                                       Date iDepartureDate, Date iArrivalDate, int iFlightSearchTypeID, int iFlightClassID, int iUserID)
	{
		mYoung = iYoung;
		mDepartureCityCode = iDepartureCityCode;
		mChilds = iChild;
		mArrivalCityCode = iArrivalCityCode;
		mFlightClassID = iFlightClassID;
		mBabies = iBabies;
		mGoldenAge = iGoldenAge;
		mStudents = iStudents;
		mUserID = iUserID;
		mTeens = iTeens;
		mAdults = iAdults;
		mDepartureDate = DateTimeUtils.convertDateToStringFormatWithLocaleOrEmptyString(iDepartureDate, DateTimeUtils.DATE_FORMAT_yyyy_MM_ddHHmmSS, LocaleUtils.getLocale()) ;
		mArrivalDate = DateTimeUtils.convertDateToStringFormatWithLocaleOrEmptyString(iArrivalDate, DateTimeUtils.DATE_FORMAT_yyyy_MM_ddHHmmSS, LocaleUtils.getLocale()) ;
		mFlightSearchTypeID = iFlightSearchTypeID;
	}
	
	public RequestSearchFlightSearchParams()
	{
	}
}
