package webServices.responses.ResponseSetUserFamily;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

import utils.global.DateTimeUtils;

public class SetMatmidUserFamilyContentAnswer
{
	@JsonProperty("FamilyID")
	private int mMatmidFamilyID;
	
	@JsonProperty("UserID")
	private int mMatmidUserID;
	
	@JsonProperty("FamilyTypeID")
	private int mMatmidFamilyTypeID;
	
	@JsonProperty("FirstNameEng")
	private String mMatmidFirstNameEng;
	
	@JsonProperty("LastNameEng")
	private String mMatmidLastNameEng;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeUtils.DATE_FORMAT_yyyy_MM_ddHHmmSS)
	@JsonProperty("DOB")
	private Date mMatmidDOB;
	
	@JsonProperty("IsActive")
	private boolean mMatmidIsActive;
	
	@JsonProperty("Title")
	private String mMatmidTitle;
	
	@JsonProperty("MatmidMemberID")
	private String mMatmidMemberID;
	
	public SetMatmidUserFamilyContentAnswer()
	{
	}
	
	@JsonIgnore
	public int getMatmidFamilyID()
	{
		return mMatmidFamilyID;
	}
	
	@JsonIgnore
	public int getMatmidUserID()
	{
		return mMatmidUserID;
	}
	
	@JsonIgnore
	public int getMatmidFamilyTypeID()
	{
		return mMatmidFamilyTypeID;
	}
	
	@JsonIgnore
	public String getMatmidFirstNameEng()
	{
		return mMatmidFirstNameEng;
	}
	
	@JsonIgnore
	public String getMatmidLastNameEng()
	{
		return mMatmidLastNameEng;
	}
	
	@JsonIgnore
	public Date getMatmidDOB()
	{
		return mMatmidDOB;
	}
	
	@JsonIgnore
	public boolean isMatmidIsActive()
	{
		return mMatmidIsActive;
	}
	
	@JsonIgnore
	public String getMatmidTitle()
	{
		return mMatmidTitle;
	}
	
	@JsonIgnore
	public String getMatmidMemberID()
	{
		return mMatmidMemberID;
	}
}
