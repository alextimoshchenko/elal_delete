package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.UserData;

/**
 * Created by erline.katz on 13/08/2017.
 */

public class RequestGetMultiplePNRDestinations
{
	@JsonProperty("UserID")
	private int mIUserID;
	
	@JsonProperty("PNR")
	private String mPNR;
	
	public RequestGetMultiplePNRDestinations(int iUserID, String iPNR)
	{
		this.mPNR = iPNR;
		this.mIUserID = iUserID;
	}
}
