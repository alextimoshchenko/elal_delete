package webServices.responses.retrievePNR;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RetrievePNR
{
	@JsonProperty("IsGroupPNR")
	private boolean mIsGroupPNR;
	@JsonProperty("Success")
	private boolean mSuccess;
	
	public boolean isGroupPNR()
	{
		return this.mIsGroupPNR;
	}
	
	public boolean getSuccess()
	{
		return this.mSuccess;
	}
	
	public RetrievePNR()
	{
	}
}
