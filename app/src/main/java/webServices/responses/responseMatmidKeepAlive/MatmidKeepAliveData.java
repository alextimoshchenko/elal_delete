package webServices.responses.responseMatmidKeepAlive;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by erline.katz on 23/10/2017.
 */

public class MatmidKeepAliveData
{
	@JsonProperty("error_NumberField")
	private int mErrorCode;
	@JsonProperty("prg_OkField")
	private boolean mSuccess;
	
	public int getmErrorCode()
	{
		return mErrorCode;
	}
	
	public boolean ismSuccess()
	{
		return mSuccess;
	}
}
