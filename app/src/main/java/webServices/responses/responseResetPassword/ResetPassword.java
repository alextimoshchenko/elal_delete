package webServices.responses.responseResetPassword;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResetPassword
{
	@JsonProperty("UserID")
	private int mUserID;
	@JsonProperty("Success")
	private boolean mSuccess;
	
	public int getUserID()
	{
		return mUserID;
	}
	
	public boolean isSuccess()
	{
		return mSuccess;
	}
	
	public ResetPassword()
	{
	}
}
