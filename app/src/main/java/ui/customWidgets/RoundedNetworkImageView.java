package ui.customWidgets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import com.android.volley.toolbox.NetworkImageView;

import org.jetbrains.annotations.NotNull;

import il.co.ewave.elal.R;

/**
 * Created by erline.katz on 06/09/2017.
 */

public class RoundedNetworkImageView extends NetworkImageView
{
	private Context mContext;
	
	public RoundedNetworkImageView(Context context) {
		super(context);
		mContext = context;
	}
	
	public RoundedNetworkImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
	}
	
	public RoundedNetworkImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mContext = context;
	}
	

	@Override
	public void setImageBitmap(Bitmap bm)
	{
		if(bm==null) return;
		setImageDrawable(new BitmapDrawable(mContext.getResources(), getCircularBitmap(bm)));
	}
	
	
	public Bitmap getCircularBitmap(Bitmap bitmap)
	{
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(output);
		
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		final RectF rectF = new RectF(rect);
		final float roundPx = 65;
		
		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
		
		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);
		
		return output;
	}

}