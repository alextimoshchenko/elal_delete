package utils.global.googleAnalitics.googleTagManager;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.tagmanager.Container;
import com.google.android.gms.tagmanager.ContainerHolder;
import com.google.android.gms.tagmanager.DataLayer;
import com.google.android.gms.tagmanager.TagManager;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import global.ElAlApplication;
import global.GlobalVariables;
import il.co.ewave.elal.R;
import utils.errors.LocalError;
import utils.global.AppUtils;

/**
 * Created with care by Alexey.T on 28/08/2017.
 * <p>
 * TODO: Add a class header comment!
 */
public class TagManagerImplementation
{
	private static final String TAG = TagManagerImplementation.class.getSimpleName();
	private TagManager tagManager;
	private DataLayer mDataLayer;
	private Context mContext;
	private PendingResult<ContainerHolder> mPendingResult;
	private final String CONTAINER_ID = GlobalVariables.GTM_CONTAINER_ID;
	private final int TIMEOUT_FOR_CONTAINER_OPEN_MILLISECONDS = 2;
	
	/**
	 * I need this temporary value , because in current analytics version this fields should be empty, but maybe in next versions we will need it
	 **/
	public final static String TIME_SCOPE = "";
	
	public TagManagerImplementation(Context iContext)
	{
		mContext = iContext;
	}
	
	public void init()
	{
		initTagManager();
		initDataLayer();
		setListeners();
	}
	
	private void initDataLayer()
	{
		if (tagManager != null)
		{
			mDataLayer = tagManager.getDataLayer();
		}
	}
	
	public void pushEvent(ePushEvents iPushEvent)
	{
		if (mDataLayer != null && iPushEvent != null)
		{
			pushCustomEvent(iPushEvent.getEventName(), iPushEvent.getDataLayer());
		}
	}
	
	public void pushCustomEvent(String iKey, Map<String, Object> iMap)
	{
		if (iMap != null)
		{
			if (mDataLayer != null)
			{
				mDataLayer.pushEvent(iKey, iMap);
			}
		}
	}
	
	private void setListeners()
	{
		// The onResult method will be called as soon as one of the following happens:
		//     1. a saved container is loaded
		//     2. if there is no saved container, a network container is loaded
		//     3. the 2-second timeout occurs
		mPendingResult.setResultCallback(new ResultCallback<ContainerHolder>()
		{
			@Override
			public void onResult(@NonNull ContainerHolder containerHolder)
			{
				ContainerHolderSingleton.setContainerHolder(containerHolder);
				Container container = containerHolder.getContainer();
				
				if (!containerHolder.getStatus().isSuccess())
				{
					AppUtils.printLog(Log.ERROR, TAG, "failure loading container");
					//					displayErrorToUser(R.string.load_error);
					return;
				}
				
				ContainerLoadedCallback.registerCallbacksForContainer(container);
				containerHolder.setContainerAvailableListener(new ContainerLoadedCallback());
				//				startMainActivity();
			}
		}, TIMEOUT_FOR_CONTAINER_OPEN_MILLISECONDS, TimeUnit.MILLISECONDS);
	}
	
	private void initTagManager()
	{
		tagManager = TagManager.getInstance(mContext);
		tagManager.setVerboseLoggingEnabled(true);
		mPendingResult = tagManager.loadContainerPreferNonDefault(CONTAINER_ID, R.raw.default_container_binary);
	}
	
	public enum ePushEvents
	{
		REGISTER_CUSTOMER(R.string.auto_event, DataLayer.mapOf("Category", "Homescreen", "Action", "Registered Customer", "Label", "Go to login")),
		
		//event 8
		REGISTER_CUSTOMER_(R.string.auto_event, DataLayer.mapOf("Category", "Sign up", "Action", "Continue", "Label", "Registered Customer")),
		
		CREATE_ACCOUNT(R.string.auto_event, DataLayer.mapOf("Category", "Homescreen", "Action", "Create Account", "Label", "-")),
		MANAGE_FLIGHTS_AND_CHECK_IN(R.string.auto_event, DataLayer.mapOf("Category", "Homescreen", "Action", "Manage flights & Check In", "Label", "-")),
		TOP_TWO(R.string.auto_event, DataLayer.mapOf("Category", "Homescreen", "Action", "TOP 2", "Label", "-")),
		MIDDLE_ONE(R.string.auto_event, DataLayer.mapOf("Category", "Homescreen", "Action", "MIDDLE 1", "Label", "-")),
		MIDDLE_TWO(R.string.auto_event, DataLayer.mapOf("Category", "Homescreen", "Action", "MIDDLE 2", "Label", "-")),
		FLIGHT_SEARCH(R.string.auto_event, DataLayer.mapOf("Category", "Homescreen", "Action", "Flight Search", "Label", "-")),
		FORGET_PASSWORD(R.string.auto_event, DataLayer.mapOf("Category", "Login", "Action", "Forget Password", "Label", "Registered Customer")),
		LOGIN(R.string.auto_event, DataLayer.mapOf("Category", "Login", "Action", "Login", "Label", "Registered Customer")),
		CREATE_ACCOUNT_SCREEN(R.string.auto_event, DataLayer.mapOf("Category", "Login", "Action", "Create Account", "Label", "Create Account Screen")),
		FORGOT_MEMBER_NUMBER(R.string.auto_event, DataLayer.mapOf("Category", "Login", "Action", "Forget member number", "Label", "Club Member ")),
		FORGET_PASSWORD_CLUB(R.string.auto_event, DataLayer.mapOf("Category", "Login", "Action", "Forget Password", "Label", "Club Member ")),
		LOGIN_CLUB(R.string.auto_event, DataLayer.mapOf("Category", "Login", "Action", "Login", "Label", "Club Member ")),
		CLUB_MEMBER(R.string.auto_event, DataLayer.mapOf("Category", "Fast sign up", "Action", "Club member", "Label", "Create Account screen")),
		MORE_DETAILS_LETS_CONTINUE(R.string.auto_event, DataLayer.mapOf("Category", "Sign up", "Action", "More details", "Label", "Let's continue")),
		MORE_DETAILS_REMIND_ME_LATER(R.string.auto_event, DataLayer.mapOf("Category", "Sign up", "Action", "More details", "Label", "Remind me later")),
		MORE_DETAILS_SKIP(R.string.auto_event, DataLayer.mapOf("Category", "Sign up", "Action", "More details", "Label", "Skip")),
		SCAN_PASSPORT_SCAN(R.string.auto_event, DataLayer.mapOf("Category", "Sign up", "Action", "Scan Passport", "Label", "Scan")),
		MORE_DETAILS_CONTINUE(R.string.auto_event, DataLayer.mapOf("Category", "Sign up", "Action", "More details", "Label", "Continue (Finish)")),
		
		//event 14
		LETS_CONTINUE_REGISTER_CUSTOMER(R.string.auto_event, DataLayer.mapOf("Category", "Sign up - Add a family member", "Action", "Let's continue", "Label", "Registered Customer")),
		LETS_CONTINUE_REGISTER_CLUB(R.string.auto_event, DataLayer.mapOf("Category", "Sign up - Add a family member", "Action", "Let's continue", "Label", "Club Member")),
		
		//event 15
		REMIND_ME_LATTER_REGISTER_CLUB(R.string.auto_event, DataLayer.mapOf("Category", "Sign up - Add a family member", "Action", "Remind me later", "Label", "Club Member")),
		REMIND_ME_LATTER_REGISTER_CUSTOMER(R.string.auto_event, DataLayer.mapOf("Category", "Sign up - Add a family member", "Action", "Remind me later", "Label", "Registered Customer")),
		
		//event 16
		NO_THANKS_REGISTER(R.string.auto_event, DataLayer.mapOf("Category", "Sign up - Add a family member", "Action", "No thanks", "Label", "Registered Customer")),
		NO_THANKS_REGISTER_CLUB(R.string.auto_event, DataLayer.mapOf("Category", "Sign up - Add a family member", "Action", "No thanks", "Label", "Club Member")),
		
		ADD_FAMILY_MEMBER(R.string.auto_event, DataLayer.mapOf("Category", "Family Account", "Action", "Add a family member", "Label", "-")),
		SCAN_PASSPORT(R.string.auto_event, DataLayer.mapOf("Category", "Family Account", "Action", "Scan Passport", "Label", "Scan")),
		FAMILY_MEMBERS_DETAILS(R.string.auto_event, DataLayer.mapOf("Category", "Family Account", "Action", "Family member's Details", "Label", "Add")),
		
		//event 20
		SUCCEEDED_REGISTRATION_ADD_FAMILY_REGISTRATION(R.string.auto_event, DataLayer.mapOf("Category", "Passport scanning", "Action", "Succeeded", "Label", "Registration")),
		SUCCEEDED_REGISTRATION_ADD_FAMILY_ADD(R.string.auto_event, DataLayer.mapOf("Category", "Passport scanning", "Action", "Succeeded", "Label", "Add a family member")),
		
		TRY_AGAIN_REGISTRATION_ADD_FAMILY_ADD(R.string.auto_event, DataLayer.mapOf("Category", "Passport scanning", "Action", "Try again", "Label", "Add a family member")),
		TRY_AGAIN_REGISTRATION_ADD_FAMILY_REGISTRATION(R.string.auto_event, DataLayer.mapOf("Category", "Passport scanning", "Action", "Try again", "Label", "Registration")),
		CANCEL_PASSPORT_SCANNING_ADD(R.string.auto_event, DataLayer.mapOf("Category", "Passport scanning", "Action", "Cancel", "Label", "Add a family member")),
		CANCEL_PASSPORT_SCANNING_REGISTRATION(R.string.auto_event, DataLayer.mapOf("Category", "Passport scanning", "Action", "Cancel", "Label", "Registration")),
		SEARCH_HISTORY_CLICK(R.string.auto_event, DataLayer.mapOf("Category", "Flight Search - Actions", "Action", "Search History", "Label", "Used")),
		CLEAR_SEARCH_HISTORY(R.string.auto_event, DataLayer.mapOf("Category", "Flight Search - Actions", "Action", "Clear Search History", "Label", "Clear")),
		
		//event 30
		ADD_FLIGHT(R.string.auto_event, DataLayer.mapOf("Category", "Manage flights & Check In", "Action", "Add a flight", "Label", "-")),
		
		ADD_FLIGHT_CONTINUE_PNR(R.string.auto_event, DataLayer.mapOf("Category", "Manage flights & Check In", "Action", "Add a flight - Continue", "Label", "PNR")),
		ADD_FLIGHT_CONTINUE_TICKET(R.string.auto_event, DataLayer.mapOf("Category", "Manage flights & Check In", "Action", "PNR - Group", "Label", "Ticket Number")),
		CANCEL_CHECK_IN(R.string.auto_event, DataLayer.mapOf("Category", "Manage flights & Check In", "Action", "Cancel check in", "Label", "Outbound Link")),
		KNOW(R.string.auto_event, DataLayer.mapOf("Category", "Manage Flight", "Action", "PNR >> Lobby", "Label", "Know")),
		REMEMBER(R.string.auto_event, DataLayer.mapOf("Category", "Manage Flight", "Action", "PNR >> Lobby", "Label", "Remember")),
		EXPERIENCE(R.string.auto_event, DataLayer.mapOf("Category", "Manage Flight", "Action", "PNR >> Lobby", "Label", "Experience")),
		
		LOGIN_SIDE_MENU(R.string.auto_event, DataLayer.mapOf("Category", "Side Menu", "Action", "Login", "Label", "-")),
		LOGOUT_SIDE_MENU(R.string.auto_event, DataLayer.mapOf("Category", "Side Menu", "Action", "Logout", "Label", "-")),
		
		//event 70
		FLIGHTS_TIMETABLE(R.string.auto_event, DataLayer.mapOf("Category", "Side Menu", "Action", "Flights Timetable", "Label", "-")),
		
		//event 71
		MY_NOTIFICATIONS(R.string.auto_event, DataLayer.mapOf("Category", "Side Menu", "Action", "My Notifications", "Label", "-")),
		
		//event 72
		MY_ACCOUNT(R.string.auto_event, DataLayer.mapOf("Category", "Side Menu", "Action", "My Account", "Label", "-")),
		
		//event 73
		UPDATE_PERSONAL_DETAILS(R.string.auto_event, DataLayer.mapOf("Category", "Side Menu", "Action", "My Account", "Label", "Update Personal Details")),
		
		//event 74
		UPDATE_FAMILY_ACCOUNT(R.string.auto_event, DataLayer.mapOf("Category", "Side Menu", "Action", "My Account", "Label", "Update Family Account")),
		
		//event 75
		CHANGE_PASSWORD(R.string.auto_event, DataLayer.mapOf("Category", "Side Menu", "Action", "My Account", "Label", "Change Password")),
		
		//event 76
		MY_DOCUMENTS(R.string.auto_event, DataLayer.mapOf("Category", "Side Menu", "Action", "My Account", "Label", "My Documents")),
		
		//event 77
		ACCOUNT_BALANCE(R.string.auto_event, DataLayer.mapOf("Category", "Side Menu", "Action", "My Account", "Label", "Account Balance")),
		
		//event 78
		ADD_MISSING_FLIGHT(R.string.auto_event, DataLayer.mapOf("Category", "Side Menu", "Action", "My Account", "Label", "Add a missing Flight")),
		
		//event 79
		MY_FLIGHT(R.string.auto_event, DataLayer.mapOf("Category", "Side Menu", "Action", "My Flight", "Label", "-")),
		
		//event 81
		ADD_FLIGHT_SIDE_MENU(R.string.auto_event, DataLayer.mapOf("Category", "Side Menu", "Action", "My Flight", "Label", "Add a Flight")),
		
		//event 82
		CHECK_IN_SIDE_MENU(R.string.auto_event, DataLayer.mapOf("Category", "Side Menu", "Action", "My Flight", "Label", "Check-In")),
		
		//event 83
		CANCEL_CHECK_IN_SIDE_MENU(R.string.auto_event, DataLayer.mapOf("Category", "Side Menu", "Action", "My Flight", "Label", "Cancel Check-In")),
		
		//event 84
		PLAN_VACATION(R.string.auto_event, DataLayer.mapOf("Category", "Side Menu", "Action", "Plan your vacation", "Label", "-")),
		
		//event 85
		BOOK_FLIGHT(R.string.auto_event, DataLayer.mapOf("Category", "Side Menu", "Action", "Plan your vacation", "Label", "Book a Flight")),
		
		//event 86
		BOOK_HOTEL(R.string.auto_event, DataLayer.mapOf("Category", "Side Menu", "Action", "Plan your vacation", "Label", "book a hotel")),
		
		//event 87
		RENT_CAR(R.string.auto_event, DataLayer.mapOf("Category", "Side Menu", "Action", "Plan your vacation", "Label", "Rent a Car")),
		
		//event 91
		TRAVEL_INFO(R.string.auto_event, DataLayer.mapOf("Category", "Side Menu", "Action", "Travel Info", "Label", "-")),
		
		//event 92
		ALL_ABOUT_LUGGAGE(R.string.auto_event, DataLayer.mapOf("Category", "Side Menu", "Action", "Travel Info", "Label", "All about Lagguage")),
		
		JOIN_NEWSLETTERS(R.string.auto_event, DataLayer.mapOf("Category", "Side Menu", "Action", "Join our Newsletter", "Label", "-")),
		SETTINGS(R.string.auto_event, DataLayer.mapOf("Category", "Side Menu", "Action", "Settings", "Label", "-")),
		SAVE_ON(R.string.auto_event, DataLayer.mapOf("Category", "Manage Flight" + TagManagerImplementation.TIME_SCOPE, "Action", TagManagerImplementation.TIME_SCOPE + "PNR >> Save", "Label", "Document Type > Save"));
		
		@StringRes
		private int mEventName;
		private Map<String, Object> mDataLayer;
		
		ePushEvents(int iEventName, Map<String, Object> iDataLayer)
		{
			mEventName = iEventName;
			mDataLayer = iDataLayer;
		}
		
		public String getEventName()
		{
			String result = ElAlApplication.getInstance().getString(mEventName);
			
			if (TextUtils.isEmpty(result))
			{
				result = "";
				AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.NoSuchItems.getErrorMessage());
			}
			
			return result;
		}
		
		public Map<String, Object> getDataLayer()
		{
			return mDataLayer;
		}
	}
}
