package utils.errors;

import android.app.Activity;
import android.content.DialogInterface;
import android.text.TextUtils;

import com.android.volley.NoConnectionError;
import com.android.volley.VolleyError;

import il.co.ewave.elal.R;
import ui.activities.EWBaseActivity;
import utils.global.AppUtils;

/**
 * Created with care by Shahar Ben-Moshe on 18/02/16.
 */
public class ErrorsHandler
{
	public static void tryShowServiceErrorDialog(final VolleyError iVolleyError, final Activity iActivity)
	{
		// TODO: 22/10/2017 - remove after test
//		iVolleyError.printStackTrace();
		
		if (iActivity != null)
		{
			iActivity.runOnUiThread(new Runnable()
			{
				public void run()
				{
					if (iActivity instanceof EWBaseActivity)
					{
						try
						{
							((EWBaseActivity) iActivity).setProgressDialog(false);
						}
						catch (Exception ignored)
						{
						}
					}
					
					if (iVolleyError != null && iVolleyError instanceof ServerError)
					{
						AppUtils.createSimpleMessageDialog(iActivity, ((ServerError) iVolleyError).getErrorMessage(), null, null, iActivity.getString(R.string.close), null).show();
					}
					else if (iVolleyError != null && iVolleyError instanceof NoConnectionError)
					{
						tryShowLocalErrorDialog(new LocalError(LocalError.eLocalError.NoConnection), iActivity);
					}
					else
					{
						//TODO avishay 26/10/17 not need it yet
//						tryShowDefaultVolleyErrorMessage(iActivity, iVolleyError);
//						tryShowLocalErrorDialog(new LocalError(LocalError.eLocalError.GeneralError), iActivity);
					}
				}
			});
		}
	}
	
	private static void tryShowDefaultVolleyErrorMessage(final Activity iActivity, final VolleyError iVolleyError)
	{
		if (iActivity != null)
		{
			iActivity.runOnUiThread(new Runnable()
			{
				public void run()
				{
					try
					{
						if (iVolleyError != null && !TextUtils.isEmpty(iVolleyError.getMessage()))
						{
							AppUtils.createSimpleMessageDialog(iActivity, iVolleyError.getMessage(), null, null, iActivity.getString(R.string.close), null).show();
						}
						else {
							AppUtils.createSimpleMessageDialog(iActivity, iActivity.getResources().getString(R.string.some_error), null, null, iActivity.getString(R.string.close), null).show();
						}
					}
					catch (Exception ignored)
					{
					}
				}
			});
		}
	}
	
	public static void tryShowLocalErrorDialog(final LocalError iLocalError, final Activity iActivity)
	{
		tryShowLocalErrorDialog(iLocalError, iActivity, null);
	}
	
	public static void tryShowLocalErrorDialog(final LocalError iLocalError, final Activity iActivity, final DialogInterface.OnClickListener iOnNegativeClickListener)
	{
		if (iActivity != null)
		{
			iActivity.runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					if (iActivity instanceof EWBaseActivity)
					{
						try
						{
							((EWBaseActivity) iActivity).setProgressDialog(false);
						}
						catch (Exception ignored)
						{
						}
					}
					
					if (iLocalError != null)
					{
						AppUtils.createSimpleMessageDialog(iActivity, iLocalError.getErrorMessage(), null, null, iActivity.getString(R.string.close), iOnNegativeClickListener).show();
					}
					else
					{
						tryShowServiceErrorDialog(new ServerError(ServerError.eServerError.Global), iActivity);
					}
				}
			});
		}
	}
}
