package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with care by Alexey.T on 26/07/2017.
 */
public class RequestGetPushNotifications
{
	@JsonProperty("UserID")
	private int mUserID;
	
	public RequestGetPushNotifications(final int iUserID)
	{
		mUserID = iUserID;
	}
	
//	public int getUserID()
//	{
//		return mUserID;
//	}
}
