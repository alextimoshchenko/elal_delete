package ui.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import il.co.ewave.elal.R;
import interfaces.IDestinationListener;
import utils.global.DateTimeUtils;
import webServices.responses.getMultiplePNRDestinations.DestinationListItem;

/**
 * Created by erline.katz on 14/08/2017.
 */

public class MyTripFlightsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
	private static final int FUTURE_FLIGHT_LAYOUT_RESOURCE_ID = R.layout.future_flight_layout;
	private static final int ACTIVE_FLIGHT_RESOURCE_ID = R.layout.active_flight_layout;
	
	
	private ArrayList<DestinationListItem> mArlItems = new ArrayList<DestinationListItem>();
	private Context mContext;
	private Fragment mFragment;
	private IDestinationListener destinationListener = null;
	
	public MyTripFlightsAdapter(ArrayList<DestinationListItem> mArlItems, Context mContext, Fragment fragment)
	{
		this.mArlItems = mArlItems;
		this.mContext = mContext;
		
		if (fragment != null)
		{
			mFragment = fragment;
		}
	}
	
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
	{
		switch (viewType)
		{
			case ACTIVE_FLIGHT_RESOURCE_ID:
				View activeView = LayoutInflater.from(parent.getContext()).inflate(ACTIVE_FLIGHT_RESOURCE_ID, parent, false);
				MainViewHolder activeFlightViewHolder = new MainViewHolder(activeView);
				return activeFlightViewHolder;
			
			case FUTURE_FLIGHT_LAYOUT_RESOURCE_ID:
				View futureView = LayoutInflater.from(parent.getContext()).inflate(FUTURE_FLIGHT_LAYOUT_RESOURCE_ID, parent, false);
				MainViewHolder futureFlightViewHolder = new MainViewHolder(futureView);
				return futureFlightViewHolder;
			
			default:
				View v = LayoutInflater.from(parent.getContext()).inflate(FUTURE_FLIGHT_LAYOUT_RESOURCE_ID, parent, false);
				MainViewHolder viewHolder = new MainViewHolder(v);
				return viewHolder;
		}
	}
	
	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
	{
		final MainViewHolder tempMainViewHolder = (MainViewHolder) viewHolder;
		DestinationListItem item = mArlItems.get(position);
		
		if (tempMainViewHolder.txvDestination != null)
		{
			tempMainViewHolder.txvDestination.setText(item.getmDestination().getDestination());
		}
		
		if (tempMainViewHolder.txvOrigin != null)
		{
			tempMainViewHolder.txvOrigin.setText(item.getmDestination().getOrigination());
		}
		
		if (tempMainViewHolder.txvOrigin != null)
		{
			tempMainViewHolder.txvOrigin.setText(item.getmVia() != null ? item.getmVia().getOrigination() : item.getmDestination().getOrigination());
		}
		
		if (tempMainViewHolder.txvFlightDetailsNumber != null)
		{
			tempMainViewHolder.txvFlightDetailsNumber.setText(item.getmVia() != null ?
			                                                  item.getmVia().getFlightNumber() + ", " + item.getmDestination().getFlightNumber() :
			                                                  item.getmDestination().getFlightNumber());
		}
		
		if (tempMainViewHolder.txvFlightDuration != null)
		{
			if (item.getmVia() != null && !TextUtils.isEmpty(item.getmVia().getDestination()))
			{
				tempMainViewHolder.txvFlightDuration.setText(mContext.getResources().getString(R.string.via) + " " + item.getmVia().getDestination());
				tempMainViewHolder.llFlightDuration.setVisibility(View.VISIBLE);
			}
			else if (item.getmDestination() != null && !TextUtils.isEmpty(item.getmDestination().getFlightDuration()))
			{
				tempMainViewHolder.txvFlightDuration.setText(item.getmDestination().getFlightDuration());
				tempMainViewHolder.llFlightDuration.setVisibility(View.VISIBLE);
			}
			else
			{
				tempMainViewHolder.llFlightDuration.setVisibility(View.INVISIBLE);
			}
		}
		
		if (tempMainViewHolder.txvArrivalDateTime != null)
		{
			tempMainViewHolder.txvArrivalDateTime.setText(DateTimeUtils.convertDateToDayMonthHourMinuteString(item.getmDestination().getArrivalDate()));
		}
		
		if (tempMainViewHolder.txvDepartureDateTime != null)
		{
			tempMainViewHolder.txvDepartureDateTime.setText(DateTimeUtils.convertDateToDayMonthHourMinuteString(item.getmVia() != null ?
			                                                                                                    item.getmVia().getDepartureDate() :
			                                                                                                    item.getmDestination().getDepartureDate()));
		}
		
		setItemClickListener(tempMainViewHolder.flViewContainer, position);
		if (item.isCurrentFlight())
		{
			tryScrollToCurrentItem(position);
		}
	}
	
	@Override
	public int getItemViewType(int position)
	{
		return mArlItems.get(position).isCurrentFlight() ? ACTIVE_FLIGHT_RESOURCE_ID : FUTURE_FLIGHT_LAYOUT_RESOURCE_ID;
	}
	
	@Override
	public int getItemCount()
	{
		return mArlItems.size();
	}
	
	private void setItemClickListener(FrameLayout frame, final int position)
	{
		frame.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				try
				{
					destinationListener = (IDestinationListener) mFragment;
					destinationListener.onDestinationSelected(mArlItems.get(position));
					notifyDataSetChanged();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}
	
	private void tryScrollToCurrentItem(final int position)
	{
		try
		{
			destinationListener = (IDestinationListener) mFragment;
			destinationListener.onScrollToCurrentItem(position);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private class MainViewHolder extends RecyclerView.ViewHolder
	{
		FrameLayout flViewContainer;
		TextView txvOrigin, txvDestination;
		TextView txvFlightDetailsNumber, txvFlightDuration, txvArrivalDateTime, txvDepartureDateTime;
		LinearLayout llFlightDuration;
		MainViewHolder(View v)
		{
			super(v);
			flViewContainer = (FrameLayout) v.findViewById(R.id.fl_view_container);
			txvOrigin = (TextView) v.findViewById(R.id.tv_myTrip_Origin);
			txvDestination = (TextView) v.findViewById(R.id.tv_myTrip_Destination);
			txvFlightDetailsNumber = (TextView) v.findViewById(R.id.tv_myTrip_FlightDetailsNumber);
			txvFlightDuration = (TextView) v.findViewById(R.id.tv_myTrip_FlightDuration);
			txvArrivalDateTime = (TextView) v.findViewById(R.id.tv_myTrip_ArrivalDateTime);
			txvDepartureDateTime = (TextView) v.findViewById(R.id.tv_myTrip_DepartureDateTime);
		llFlightDuration = (LinearLayout) 	v.findViewById(R.id.ll_myTrip_FlightDuration);
		}
	}
	
}
