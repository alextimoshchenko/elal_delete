package ui.customWidgets;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import il.co.ewave.elal.R;

/**
 * Created by erline.katz on 29/08/2017.
 */

public class FlightStatusIndicator extends LinearLayout
{
	public static final int ACTIVE = 1;
	public static final int AVAILABLE = 2;
	public static final int DISABLED = 3;
	
	private LinearLayout mRowLayout;
	private ImageView mDecoratorImg;
	private TextView mTxvTitle;
	private Context mContext;
	
	public FlightStatusIndicator(Context context)
	{
		super(context);
		mContext = context;
		initView();
	}
	
	public FlightStatusIndicator(Context context, @Nullable AttributeSet attrs)
	{
		super(context, attrs);
		mContext = context;
		initView();
	}
	
	public FlightStatusIndicator(Context context, @Nullable AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
		mContext = context;
		initView();
	}
	
	private void initView()
	{
		inflateLayout();
		mRowLayout = (LinearLayout) findViewById(R.id.lil_holder);
		mDecoratorImg = (ImageView) findViewById(R.id.img_bottom_decorator);
		mTxvTitle = (TextView) findViewById(R.id.txv_title);
	}
	
	public void inflateLayout()
	{
		LayoutInflater.from(getContext()).inflate(R.layout.flight_status_indicator, this, true);
	}
	
	public void setText(String strText)
	{
		mTxvTitle.setText(strText);
	}
	
	public void setText(int strTextId)
	{
		mTxvTitle.setText(mContext.getResources().getString(strTextId));
	}
	
	public void setIndicator(int iIndicatorStatus)
	{
		switch (iIndicatorStatus)
		{
			case ACTIVE:
				mTxvTitle.setTextColor(mContext.getResources().getColor(R.color.ElalDarkBlue));
				mDecoratorImg.setImageDrawable(mContext.getResources().getDrawable(R.drawable.flight_status_indicator_new));
				break;
				
			case AVAILABLE:
				mTxvTitle.setTextColor(mContext.getResources().getColor(R.color.ElalDarkBlue));
				mDecoratorImg.setImageDrawable(mContext.getResources().getDrawable(R.drawable.flight_status_indicator_inactive));
				break;
				
			case DISABLED:
				mTxvTitle.setTextColor(mContext.getResources().getColor(R.color.ElalDarkGray));
				mDecoratorImg.setImageDrawable(mContext.getResources().getDrawable(R.drawable.flight_status_indicator_inactive));
				break;
				
			default:
				
				break;
		}
	}
	
	public void setOnClickListener(OnClickListener iClickListener)
	{
		mRowLayout.setOnClickListener(iClickListener);
	}
}
