package ui.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import global.UserData;
import il.co.ewave.elal.R;
import interfaces.IFamilyMemberListener;
import ui.customWidgets.ChangeDirectionLinearLayout;
import ui.customWidgets.swipeLayout.SwipeLayout;
import ui.customWidgets.swipeLayout.adapters.RecyclerSwipeAdapter;
import utils.global.AppUtils;
import webServices.responses.responseGetUserFamily.GetUserFamilyContent;

public class FamilyMembersAdapter extends RecyclerSwipeAdapter<FamilyMembersAdapter.ViewHolder>
{
	private ArrayList<GetUserFamilyContent> mFamilyMembers = new ArrayList<>();
	private IFamilyMemberListener mListener;
	private boolean mIsEnglishLanguage;
	private Context mContext;
	
	private final float WIDTH_OF_ONE_SLOT = 47;
	
	public FamilyMembersAdapter(Context iContext, final ArrayList<GetUserFamilyContent> iFamilyMembers, IFamilyMemberListener iListener)
	{
		mFamilyMembers = iFamilyMembers;
		mContext = iContext;
		mListener = iListener;
		mIsEnglishLanguage = UserData.getInstance().getLanguage().isEnglish();
	}
	
	@Override
	public ViewHolder onCreateViewHolder(final ViewGroup iParent, final int iViewType)
	{
		return new FamilyMembersAdapter.ViewHolder(LayoutInflater.from(iParent.getContext()).inflate(R.layout.family_member_layout, iParent, false));
	}
	
	@Override
	public void onBindViewHolder(final ViewHolder iVH, final int iPosition)
	{
		final GetUserFamilyContent userFamily = mFamilyMembers.get(iPosition);
		final int position = iVH.getAdapterPosition();
		
		final SwipeLayout swipeLayout = iVH.mSwipeLayout;
		//    swipeLayout.close();
		//    swipeLayout.setShowMode(ShowMode.PullOut);
		
		//main settings
		float slotWidth;
		float slotWight;
		
		boolean isFamilyMemberMatmid = userFamily.getMatmidMemberID() > 0;
		
		if (isFamilyMemberMatmid)
		{
			slotWidth = WIDTH_OF_ONE_SLOT;
			slotWight = 1;
			
			iVH.mIvMemberIcon.setVisibility(View.VISIBLE);
			iVH.mIvEdit.setVisibility(View.GONE);
			
			Drawable unlinkDrawable;
			
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
			{
				unlinkDrawable = mContext.getResources().getDrawable(R.mipmap.unlink, mContext.getTheme());
			}
			else
			{
				unlinkDrawable = mContext.getResources().getDrawable(R.mipmap.unlink);
			}
			
			iVH.mIvDelete.setBackgroundColor(ContextCompat.getColor(mContext, R.color.midnight_blue_two));
			iVH.mIvDelete.setImageDrawable(unlinkDrawable);
			iVH.mIvDelete.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(final View v)
				{
					closeAllItems();
					swipeLayout.close();
					mListener.unlinkMatMidMember(swipeLayout, position);
				}
			});
		}
		else
		{
			slotWidth = WIDTH_OF_ONE_SLOT * 2;
			slotWight = 0.5f;
			
			iVH.mIvMemberIcon.setVisibility(View.INVISIBLE);
			iVH.mIvEdit.setVisibility(View.VISIBLE);

			Drawable deleteDrawable;

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
			{
				deleteDrawable = mContext.getResources().getDrawable(R.mipmap.delete, mContext.getTheme());
			}
			else
			{
				deleteDrawable = mContext.getResources().getDrawable(R.mipmap.delete);
			}

			iVH.mIvDelete.setBackgroundColor(ContextCompat.getColor(mContext, R.color.midnight_blue_two));
			iVH.mIvDelete.setImageDrawable(deleteDrawable);
			
			iVH.mIvDelete.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(final View v)
				{
					doOnDeleteItem(position);
				}
			});
			iVH.mIvEdit.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(final View v)
				{
					doOnEditItem(position);
				}
			});
		}
		
		//set params for bottom view(buttons)
		SwipeLayout.LayoutParams params = new SwipeLayout.LayoutParams((int) AppUtils.convertPixelsToDp(slotWidth, mContext), RecyclerView.LayoutParams.MATCH_PARENT);
		
		if (mIsEnglishLanguage)
		{
			params.gravity = Gravity.END;
			swipeLayout.addDrag(SwipeLayout.DragEdge.Right, iVH.mLlBottomWrapper);
		}
		else
		{
			params.gravity = Gravity.START;
			swipeLayout.addDrag(SwipeLayout.DragEdge.Left, iVH.mLlBottomWrapper);
		}
		
		iVH.mLlBottomWrapper.setLayoutParams(params);
		
		//		iVH.mSwipeLayout.setDragEdge(AppUtils.isDefaultLocaleRTL() ? SwipeRevealLayout.DRAG_EDGE_LEFT : SwipeRevealLayout.DRAG_EDGE_RIGHT);
		
		//set params for bottom view elements
		LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, slotWight);
		iVH.mIvDelete.setLayoutParams(param);
		
		iVH.mLlSurface.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnSurfaceClick(position, swipeLayout);
			}
		});
		
		String firstName = !TextUtils.isEmpty(userFamily.getFirstNameEng()) ? userFamily.getFirstNameEng() : "";
		String lastName = !TextUtils.isEmpty(userFamily.getLastNameEng()) ? userFamily.getLastNameEng() : "";
		String fullName = firstName + " " + lastName;
		iVH.mTvDescription.setText(fullName);
	}
	
	private void doOnOptionClick(int iPosition)
	{
		mListener.onOptionClick(iPosition);
	}
	
	private void doOnSurfaceClick(int iPosition, SwipeLayout iSwipeLayout)
	{
		mListener.onItemClick(iPosition, iSwipeLayout);
	}
	
	private void doOnEditItem(int iPosition)
	{
		mListener.onEditItem(iPosition);
	}
	
	private void doOnDeleteItem(int iPosition)
	{
		mListener.onDeleteItem(iPosition);
	}
	
	@Override
	public int getSwipeLayoutResourceId(int iPosition)
	{
		return R.id.sl_family_item;
	}
	
	class ViewHolder extends RecyclerView.ViewHolder
	{
		SwipeLayout mSwipeLayout;
		ImageView mIvDelete, mIvEdit, mIvMemberIcon;
		ChangeDirectionLinearLayout mLlBottomWrapper, mLlSurface;
		TextView mTvDescription;
		
		ViewHolder(final View iV)
		{
			super(iV);
			mSwipeLayout = (SwipeLayout) iV.findViewById(R.id.sl_family_item);
			mIvDelete = (ImageView) iV.findViewById(R.id.iv_family_item_delete);
			mIvEdit = (ImageView) iV.findViewById(R.id.iv_family_item_edit);
						mLlSurface = (ChangeDirectionLinearLayout) iV.findViewById(R.id.ll_family_item_surface);
			mIvMemberIcon = (ImageView) iV.findViewById(R.id.iv_family_item_member_icon);
			mTvDescription = (TextView) iV.findViewById(R.id.tv_family_item_description);
			mLlBottomWrapper = (ChangeDirectionLinearLayout) iV.findViewById(R.id.ll_family_item_bottom_wrapper);
		}
	}
	
	@Override
	public int getItemCount()
	{
		return mFamilyMembers == null || mFamilyMembers.isEmpty() ? 0 : mFamilyMembers.size();
	}
}
