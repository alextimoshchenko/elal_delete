package webServices.global;

public enum eActionType
{
	ADD(1),
	DELETE(2);
	
	public int action;
	
	eActionType(final int iAction)
	{
		action = iAction;
	}
}
