package webServices.controllers;

import com.android.volley.Request;
import com.android.volley.Response;

import webServices.global.JacksonRequest;
import webServices.global.RequestStringBuilder;
import webServices.requests.RequestDeleteUserPushMessage;
import webServices.requests.RequestGetPushNotifications;
import webServices.requests.RequestSetPushAsRead;
import webServices.responses.deleteUserPushMessage.ResponseDeleteUserPushMessage;
import webServices.responses.getPushNotifications.ResponseGetPushNotifications;
import webServices.responses.setPushAsRead.ResponseSetPushAsRead;

/**
 * Created with care by Alexey.T on 25/07/2017.
 */
public class NotificationController extends AController implements MizdamenInterfave
{
	public void DeleteUserPushMessage(final RequestDeleteUserPushMessage iRequest, final Response.Listener<ResponseDeleteUserPushMessage> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseDeleteUserPushMessage> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getDeleteUserPushMessageUrl(), iRequest, ResponseDeleteUserPushMessage.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void GetPushNotifications(final RequestGetPushNotifications iRequest, final Response.Listener<ResponseGetPushNotifications> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseGetPushNotifications> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getGetPushNotificationsUrl(), iRequest, ResponseGetPushNotifications.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void SetPushAsRead(final RequestSetPushAsRead iRequest, final Response.Listener<ResponseSetPushAsRead> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseSetPushAsRead> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getSetPushAsReadUrl(), iRequest, ResponseSetPushAsRead.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
}
