package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestGetPNRFlights
{
	@JsonProperty("TicketNumber")
	private String mTicketNumber;
	@JsonProperty("PNRNum")
	private String mPNRNum;
	
	public RequestGetPNRFlights(String iTicketNumber, String iPNRNum)
	{
		this.mTicketNumber = iTicketNumber;
		this.mPNRNum = iPNRNum;
	}
	
	public RequestGetPNRFlights()
	{
	}
}
