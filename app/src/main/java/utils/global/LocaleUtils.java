package utils.global;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.DisplayMetrics;

import java.util.Locale;

import global.UserData;
import global.eLanguage;

/**
 * Created with care by Shahar Ben-Moshe on 16/01/17.
 */

public class LocaleUtils
{
	private static final String HEBREW_LANGUAGE_CODE_OLD = "iw";
	private static final String HEBREW_LANGUAGE_CODE_NEW = "he";
	
	private enum eLanguageCode
	{
		/*00*/    en_US("English"),
		/*01*/    fr_FR("French (Europe)"),
		/*02*/  fr_CA("French (CA)"),
		/*03*/  nl_NL("Dutch"),
		/*04*/  es_ES("Spanish (Europe)"),
		/*05*/    es_US("Spanish (America)"),
		/*06*/  pt_PT("Portuguese (Europe)"),
		/*07*/  pt_BR("Portuguese (America)"),
		/*08*/  el_GR("Greek"),
		/*09*/  it_IT("Italian"),
		/*10*/    nb_NO("Norwegian"),
		/*11*/  fi_FI("Finnish"),
		/*12*/  sv_SE("Swedish"),
		/*13*/  da_DK("Danish"),
		/*14*/  ru_RU("Russian"),
		/*15*/    de_DE("German"),
		/*16*/  bg_BG("Bulgarian"),
		/*17*/  hu_HU("Hungarian"),
		/*18*/  ro_RO("Romanian"),
		/*19*/  cs_CZ("Czech"),
		/*20*/    sl_SI("Slovenian"),
		/*21*/  pl_PL("Polish"),
		/*22*/  tr_TR("Turkish"),
		/*23*/  iw_IL("Hebrew"),
		/*24*/  ar("Arabic"),
		/*25*/    zh_TW("Chinese (traditional)"),
		/*26*/  zh_CN("Chinese (simplified)"),
		/*27*/  ja_JP("Japanese"),
		/*28*/  ko_KR("Korean");
		
		private String mLanguageName;
		
		private eLanguageCode(String iLanguageName)
		{
			this.mLanguageName = iLanguageName;
		}
		
		public String getLanguageName()
		{
			return mLanguageName;
		}
	}
	
	public static eLanguageCode getLanguageCodeByDefaultLocale()
	{
		eLanguageCode result = eLanguageCode.en_US;
		String defaultLocaleLanguageCode = Locale.getDefault().getLanguage();
		
		for (eLanguageCode eLanguage : eLanguageCode.values())
		{
			if (eLanguage.name().substring(0, 2).equalsIgnoreCase(defaultLocaleLanguageCode))
			{
				result = eLanguage;
				break;
			}
		}
		
		return result;
	}
	
	public static eLanguage getLanguageByDefaultLanguageCode()
	{
		String languageCode = getLanguageCodeByDefaultLocale().name();
		eLanguage result = eLanguage.English;
		
		for (eLanguage language : eLanguage.values())
		{
			if (language.getLanguageCode() != null && language.getLanguageCode().equals(languageCode))
			{
				result = language;
				break;
			}
		}
		
		return result;
	}
	
	public static Locale getLocale()
	{
		eLanguage language = UserData.getInstance().getLanguage();
		return LocaleUtils.convertLanguageCodeToLocaleOrNull(language.getLanguageCode());
	}
	
	public static Locale convertLanguageCodeToLocaleOrNull(String i_languageCode)
	{
		Locale result = null;
		
		String language;
		String country;
		
		if (!TextUtils.isEmpty(i_languageCode))
		{
			String[] splittedLanguageCode = i_languageCode.split("_");
			if (splittedLanguageCode.length == 2 && !TextUtils.isEmpty(splittedLanguageCode[0]) && !TextUtils.isEmpty(splittedLanguageCode[1]))
			{
				language = splittedLanguageCode[0];
				country = splittedLanguageCode[1];
			}
			else
			{
				language = i_languageCode;
				country = "";
			}
			
			result = new Locale(language, country);
		}
		return result;
	}
	
	public static boolean isLocaleRTL(Locale i_localeToCheck)
	{
		return Character.getDirectionality(i_localeToCheck.getDisplayName(i_localeToCheck).charAt(0)) == Character.DIRECTIONALITY_RIGHT_TO_LEFT;
	}
	
	public static String getDefaultLocaleLanguageCode()
	{
		return getDefaultLocaleLanguageCodeConvertIwToHe();
	}
	
	private static String getDefaultLocaleLanguageCodeConvertIwToHe()
	{
		String result = Locale.getDefault().getLanguage();
		
		if (result.equalsIgnoreCase(HEBREW_LANGUAGE_CODE_OLD))
		{
			result = HEBREW_LANGUAGE_CODE_NEW;
		}
		
		return result;
	}
	
	public static void applyLocaleFromLanguageCode(Context iContext, eLanguageCode iLanguageCode)
	{
		applyLocale(iContext, convertLanguageCodeToLocaleOrNull(iLanguageCode.name()));
	}
	
	public static void applyLocaleFromLanguage(Context iContext, eLanguage iLanguage)
	{
		if (iLanguage != null)
		{
			applyLocale(iContext, convertLanguageCodeToLocaleOrNull(iLanguage.getLanguageCode()));
		}
	}
	
	//	public static void applyLocale(Context iContext, Locale iLocale)
	//	{
	//		if (iLocale != null && !TextUtils.isEmpty(iLocale.getLanguage()))
	//		{
	//			Configuration configuration = new Configuration();
	//			configuration.locale = iLocale;
	//			Locale.setDefault(iLocale);
	//
	////			setResources();
	//
	//			iContext.getResources().updateConfiguration(configuration, iContext.getResources().getDisplayMetrics());
	//		}
	//	}
	
	
	/**
	 * @param iContext
	 * @param iLocale
	 *
	 * @author Avishay.Peretz
	 * change locale with new android methods
	 */
	public static void applyLocale(Context iContext, Locale iLocale)
	{
		if (iLocale != null && !TextUtils.isEmpty(iLocale.getLanguage()))
		{
			//			Configuration configuration = new Configuration();
			Configuration configuration = iContext.getResources().getConfiguration();
//			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
//			{
				configuration.setLocale(iLocale);
//			}
//			else
//			{
//				configuration.locale = iLocale;
//			}
			
			Locale.setDefault(iLocale);
//			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
//			{
//				iContext/*.getApplicationContext()*/.createConfigurationContext(configuration);
//			}
//			else
//			{
				iContext./*getApplicationContext().*/getResources().updateConfiguration(configuration, iContext.getResources().getDisplayMetrics());
//			}
			//			iContext.getResources().updateConfiguration(configuration, iContext.getResources().getDisplayMetrics());
		}
	}
	
	
	@NonNull
	public Resources getLocalizedResources(Context context, Locale desiredLocale)
	{
		Configuration conf = context.getResources().getConfiguration();
		conf = new Configuration(conf);
		conf.setLocale(desiredLocale);
		Context localizedContext = context.createConfigurationContext(conf);
		return localizedContext.getResources();
	}
	//	private static void setResources(Context iContext)
	//	{DisplayMetrics metrics = new DisplayMetrics();
	//		iContextgetWindowManager().getDefaultDisplay().getMetrics(metrics);
	//		Resources resources = Resources.getSystem();
	//		resources.getConfiguration() =
	//
	//	}
}
