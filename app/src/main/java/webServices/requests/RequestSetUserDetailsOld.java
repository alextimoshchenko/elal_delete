package webServices.requests;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Calendar;
import java.util.Date;

import utils.global.DateTimeUtils;



public class RequestSetUserDetailsOld
{
	@JsonProperty("UserID")
	private int mUserID;
	@JsonProperty("Email")
	private String mEmail;
	@JsonProperty("Mobile")
	private String mMobile;
	@JsonProperty("UserPassword")
	private String mPassword;
	@JsonProperty("Title")
	private String mTitle;
	@JsonProperty("FirstNameEng")
	private String mFirstNameEng;
	@JsonProperty("LastNameEng")
	private String mLastNameEng;
	@JsonProperty("MiddleNameEng")
	private String mMiddleNameEng;
	@JsonProperty("MatmidMemberId")
	private String mMatmidMemberId;
	@JsonProperty("FirstNameHeb")
	private String mFirstNameHeb;
	@JsonProperty("LastNameHeb")
	private String mLastNameHeb;
	@JsonProperty("MiddleNameHeb")
	private String mMiddleNameHeb;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeUtils.DATE_FORMAT_yyyy_MM_ddHHmmSS)
	@JsonProperty("DOB")
	private Date mDOB;
	@JsonProperty("PassportNum")
	private String mPassportNum;
	@JsonProperty("PassportExpiration")
	private String mPassportExpiration;
	@JsonProperty("IDNumber")
	private String mIDNumber;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeUtils.DATE_FORMAT_yyyy_MM_ddHHmmSS)
	@JsonProperty("WeddingDate")
	private Date mWeddingDate;
	@JsonProperty("CountryCode")
	private String mCountryCode;
	@JsonProperty("LanguageId")
	private String mLanguageId;
	@JsonProperty("RepresentationId")
	private int mRepresentationId;
	@JsonProperty("PushFlightsInfo")
	private Boolean mPushFlightsInfo;
	@JsonProperty("PushOffersInfo")
	private Boolean mPushOffersInfo;
	@JsonProperty("MatmidPassword")
	private String mMatmidMemberPassword;
	
	//	public RequestSetUserDetailsOld(UserObject iUserObject)
	//	{
	//		mUserID = iUserObject.getUserID();
	//		mEmail = iUserObject.getEmail();
	//		mMobile = iUserObject.getPhoneNumber();
	//		mPassword = iUserObject.getUserPassword();
	//		mTitle = iUserObject.getTitle();
	//		mFirstNameEng = iUserObject.getFirstNameEng();
	//		mLastNameEng = iUserObject.getLastNameEng();
	//		mMiddleNameEng = iUserObject.getMiddleNameEng();
	//		mMatmidMemberId = iUserObject.getMatmidMemberID();
	//		mFirstNameHeb = iUserObject.getFirstNameHeb();
	//		mLastNameHeb = iUserObject.getLastNameHeb();
	//		mMiddleNameHeb = iUserObject.getMiddleNameHeb();
	//		mDOB = iUserObject.getDateOfBirth();
	//		mPassportNum = iUserObject.getPassportNum();
	//		mPassportExpiration = iUserObject.getPassportExpiration() == null ? "" : iUserObject.getPassportExpiration().toString();
	//		mIDNumber = iUserObject.getIdNumber();
	//		mWeddingDate = iUserObject.getWeddingDate();
	//		mLanguageId = iUserObject.getLanguageID();
	//		mRepresentationId = iUserObject.getRepresentationID();
	//	}
	
	public RequestSetUserDetailsOld(String iMiddleNameEng, String iEmail, String iMatmidMemberId, String iPassportExpiration, String iLastNameEng, String iPassportNum, String iTitle, String iMobile,
	                             Calendar iWeddingDate, String iFirstNameEng, int iUserID, Calendar iDOB, String iIDNumber, String iMiddleNameHeb, int iRepresentationId, String iFirstNameHeb,
	                             String iLastNameHeb, String iLanguageId, String iPassword, String iMatmidMemberPassword)
	{
		mMiddleNameEng = iMiddleNameEng;
		mEmail = iEmail;
		mMatmidMemberId = iMatmidMemberId;
		mPassportExpiration = iPassportExpiration;
		mLastNameEng = iLastNameEng;
		mPassportNum = iPassportNum;
		mTitle = iTitle;
		mMobile = iMobile;
		mWeddingDate = iWeddingDate == null ? null : iWeddingDate.getTime();
		mFirstNameEng = iFirstNameEng;
		mUserID = iUserID;
		mDOB = iDOB == null ? null : iDOB.getTime();
		mIDNumber = iIDNumber;
		mMiddleNameHeb = iMiddleNameHeb;
		mRepresentationId = iRepresentationId;
		mFirstNameHeb = iFirstNameHeb;
		mLastNameHeb = iLastNameHeb;
		mLanguageId = iLanguageId;
		mPassword = iPassword;
		mMatmidMemberPassword = iMatmidMemberPassword;
	}
	
	public RequestSetUserDetailsOld(String iEmail, String iPassportExpiration, String iLastNameEng, String iPassportNum, String iMobile, String iFirstNameEng, int iUserID, Calendar iDOB,
	                             String iIDNumber, int iRepresentationId, String iFirstNameHeb, String iLastNameHeb, String iLanguageId, String iPassword)
	{
		mEmail = iEmail;
		mPassportExpiration = iPassportExpiration;
		mLastNameEng = iLastNameEng;
		mPassportNum = iPassportNum;
		mMobile = iMobile;
		mFirstNameEng = iFirstNameEng;
		mUserID = iUserID;
		mDOB = iDOB == null ? null : iDOB.getTime();
		mIDNumber = iIDNumber;
		mRepresentationId = iRepresentationId;
		mFirstNameHeb = iFirstNameHeb;
		mLastNameHeb = iLastNameHeb;
		mLanguageId = iLanguageId;
		mPassword = iPassword;
	}
}

