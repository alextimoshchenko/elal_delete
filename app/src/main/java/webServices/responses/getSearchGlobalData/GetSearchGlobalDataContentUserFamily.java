package webServices.responses.getSearchGlobalData;

import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import utils.global.DateTimeUtils;
import utils.global.enums.ePassengersType;
import utils.global.searchFlightObjects.IUserFamily;
import utils.global.searchFlightObjects.SearchObject.AgeRange;

public class GetSearchGlobalDataContentUserFamily implements IUserFamily
{
	@JsonProperty("FamilyID")
	private int mFamilyID;
	@JsonProperty("UserID")
	private int mUserID;
	@JsonProperty("FamilyTypeID")
	private int mFamilyTypeID;
	@JsonProperty("FirstNameEng")
	private String mFirstNameEng = "";
	@JsonProperty("LastNameEng")
	private String mLastNameEng = "";
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeUtils.DATE_FORMAT_yyyy_MM_ddHHmmSS)
	@JsonProperty("DOB")
	private Date mDOB = new Date();
	@JsonProperty("IsActive")
	private boolean mIsActive;
	@JsonProperty("Title")
	private String mTitle = "";
	@JsonProperty("MatmidMemberID")
	private int mMatmidMemberID = -1;
	@JsonProperty("IsFromMatmid")
	private boolean mIsFromMatmid;
	
	@JsonIgnore
	private boolean mIsSelectedForSearching = false;
	@JsonIgnore
	private boolean mIsUserHimself;
	@JsonIgnore
	private boolean mIsFamilyMember;
	
	public GetSearchGlobalDataContentUserFamily()
	{
	}
	
	public GetSearchGlobalDataContentUserFamily(final String iFirstNameEng, final String iLastNameEng, String iTitle, final int iUserID, final Date iDOB)
	{
		mFirstNameEng = iFirstNameEng;
		mUserID = iUserID;
		mDOB = iDOB;
		mLastNameEng = iLastNameEng;
		mTitle = iTitle;
	}
	
	private GetSearchGlobalDataContentUserFamily(final int iFamilyID, final int iUserID, final int iFamilyTypeID, final String iFirstNameEng, final String iLastNameEng, final Date iDOB,
	                                             final boolean iIsActive, final String iTitle, final int iMatmidMemberID, final boolean iIsFromMatmid, final boolean iIsSelectedForSearching,
	                                             final boolean iIsUserHimself, final boolean iIsFamilyMember)
	{
		mFamilyID = iFamilyID;
		mUserID = iUserID;
		mFamilyTypeID = iFamilyTypeID;
		mFirstNameEng = iFirstNameEng;
		mLastNameEng = iLastNameEng;
		mDOB = iDOB;
		mIsActive = iIsActive;
		mTitle = iTitle;
		mMatmidMemberID = iMatmidMemberID;
		mIsFromMatmid = iIsFromMatmid;
		mIsSelectedForSearching = iIsSelectedForSearching;
		mIsUserHimself = iIsUserHimself;
		mIsFamilyMember = iIsFamilyMember;
	}
	
	@JsonIgnore
	public int getMatmidMemberID()
	{
		return mMatmidMemberID;
	}
	
	@JsonIgnore
	public String getFirstNameEng()
	{
		return this.mFirstNameEng;
	}
	
	@JsonIgnore
	public int getFamilyID()
	{
		return this.mFamilyID;
	}
	
	@JsonIgnore
	public int getUserID()
	{
		return this.mUserID;
	}
	
	@JsonIgnore
	public Date getDOB()
	{
		return this.mDOB;
	}
	
	@Override
	public void setDOB(final Date iDOB)
	{
		mDOB = iDOB;
	}
	
	@JsonIgnore
	@Override
	public void addAge(final Date upToThisDate)
	{
		Date today = new Date();
		
		if (mDOB != null && upToThisDate != null && upToThisDate.after(today))
		{
			long current = today.getTime();
			long needed = upToThisDate.getTime();
			long diff = needed - current;
			long newValue = mDOB.getTime() - diff;
			
			mDOB = new Date(newValue);
		}
	}
	
	@JsonIgnore
	public IUserFamily cloneObj()
	{
		return new GetSearchGlobalDataContentUserFamily(mFamilyID, mUserID, mFamilyTypeID, mFirstNameEng, mLastNameEng, mDOB, mIsActive, mTitle, mMatmidMemberID, mIsFromMatmid, mIsSelectedForSearching, mIsUserHimself, mIsFamilyMember);
	}
	
	@JsonIgnore
	public String getLastNameEng()
	{
		return this.mLastNameEng;
	}
	
	@JsonIgnore
	public int getFamilyTypeID()
	{
		return this.mFamilyTypeID;
	}
	
	@JsonIgnore
	public String getUserFullName()
	{
		return (TextUtils.isEmpty(mFirstNameEng) ? "" : mFirstNameEng) + " " + (TextUtils.isEmpty(mLastNameEng) ? "" : mLastNameEng);
	}
	
	@Override
	@JsonIgnore
	public boolean isSelectedForSearching()
	{
		return mIsSelectedForSearching;
	}
	
	@JsonIgnore
	public void setSelectedForSearching(final boolean iCheckedForSearching)
	{
		mIsSelectedForSearching = iCheckedForSearching;
	}
	
	@JsonIgnore
	public boolean isAgeWithinRange(AgeRange iRange)
	{
		return iRange != null && iRange.getMaxAge() >= getMemberAgeInYears() && iRange.getMinAge() <= getMemberAgeInYears();
	}
	
	@JsonIgnore
	private int getMemberAgeInYears()
	{
		if (mDOB == null)
		{
			return -1;
		}
		
		long memberAgeInDays = TimeUnit.MILLISECONDS.toDays(new Date().getTime() - mDOB.getTime());
		int years = (int) memberAgeInDays / 365;
//		int leftDays = (int) memberAgeInDays % 365;
//		if (years == ePassengersType.TEENAGERS.getMemberRangeAge().getMaxAge() && leftDays > 0)
//		{
//			years += 1;
//		}
		return years/*(int) memberAgeInDays / 365*/;
	}
	
	@JsonIgnore
	public boolean isEqualsOrOlderThan(int iAge)
	{
		return iAge <= getMemberAgeInYears();
	}
	
	@JsonIgnore
	public boolean isUserHimself()
	{
		return mIsUserHimself;
	}
	
	@JsonIgnore
	public void setUserHimself(final boolean iUserHimself)
	{
		mIsUserHimself = iUserHimself;
	}
	
	@JsonIgnore
	public boolean isFamilyMember()
	{
		return mIsFamilyMember;
	}
	
	@JsonIgnore
	public void setFamilyMember(final boolean iFamilyMember)
	{
		mIsFamilyMember = iFamilyMember;
	}
	
	@JsonIgnore
	public String getTitle()
	{
		return mTitle;
	}
	
	@JsonIgnore
	public void setTitle(final String iTitle)
	{
		mTitle = iTitle;
	}
	
	@JsonIgnore
	private void setFirstNameEng(final String iFirstNameEng)
	{
		mFirstNameEng = iFirstNameEng;
	}
	
	@JsonIgnore
	private void setLastNameEng(final String iLastNameEng)
	{
		mLastNameEng = iLastNameEng;
	}
	
	@Override
	public void setSelectedStatusAccordingTo(final IUserFamily iUserFamily)
	{
		this.setSelectedForSearching(iUserFamily.isSelectedForSearching());
	}
	
	@Override
	public boolean equals(final Object obj)
	{
		GetSearchGlobalDataContentUserFamily memberToCompare;
		
		if (obj instanceof GetSearchGlobalDataContentUserFamily)
		{
			memberToCompare = (GetSearchGlobalDataContentUserFamily) obj;
		}
		else
		{
			return false;
		}
		
		//This checking because it is possible that I can get String value from server as null
		//Title
		if (TextUtils.isEmpty(this.mTitle))
		{
			this.mTitle = "";
		}
		
		if (TextUtils.isEmpty(memberToCompare.getTitle()))
		{
			memberToCompare.setTitle("");
		}
		
		//First name
		if (TextUtils.isEmpty(this.mFirstNameEng))
		{
			this.mFirstNameEng = "";
		}
		
		if (TextUtils.isEmpty(memberToCompare.getFirstNameEng()))
		{
			memberToCompare.setFirstNameEng("");
		}
		
		//Last name
		if (TextUtils.isEmpty(this.mLastNameEng))
		{
			this.mLastNameEng = "";
		}
		
		if (TextUtils.isEmpty(memberToCompare.getLastNameEng()))
		{
			memberToCompare.setLastNameEng("");
		}
		
		
		/**Here I am checking just a year, because of implementation. If I will check exactly date, I will get count++ in passanger field in SearchFlightFragment after every time that I change date**/
		Calendar cal = Calendar.getInstance();
		cal.setTime(mDOB);
		int thisYear = cal.get(Calendar.YEAR);
		cal.setTime(memberToCompare.mDOB);
		int memberYear = cal.get(Calendar.YEAR);
		
		return this.mTitle.equals(memberToCompare.getTitle()) && this.mFamilyID == memberToCompare.mFamilyID && this.mUserID == memberToCompare.mUserID && this.mFirstNameEng.equals(memberToCompare.getFirstNameEng()) && this.mLastNameEng
				.equals(memberToCompare.mLastNameEng) && this.mFamilyTypeID == memberToCompare.mFamilyTypeID;
	}
}
