package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestGetUserSettings
{
	@JsonProperty("UserId")
	private int mUserId;
	
	public RequestGetUserSettings(int iUserId)
	{
		mUserId = iUserId;
	}
	
	public RequestGetUserSettings()
	{
	}
}
