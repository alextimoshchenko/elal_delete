package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.UserData;
import utils.global.searchFlightObjects.SearchObject;

/**
 * Created with care by Alexey.T on 17/07/2017.
 */
public class RequestSearchDepartureCities
{
	@JsonProperty("LanguageCode")
	private String mLanguageCode = UserData.getInstance().getLanguage().getLanguageCode();
	
	@JsonProperty("FlightType")
	private String mFlightType = SearchObject.getInstance().getCurrentTabStatement().getStatementCode();
	
	public RequestSearchDepartureCities()
	{
	}
}
