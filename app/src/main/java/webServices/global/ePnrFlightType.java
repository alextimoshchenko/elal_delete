package webServices.global;

/**
 * Created by Avishay.Peretz on 22/06/2017.
 */

public enum  ePnrFlightType
{
	DEPARTURE(1),
	RETURN(2);
	
	public int type;
	
	ePnrFlightType(final int iType)
	{
		type = iType;
	}
}
