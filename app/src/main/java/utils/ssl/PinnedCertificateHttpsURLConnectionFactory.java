package utils.ssl;

import android.support.annotation.NonNull;

import org.jetbrains.annotations.Contract;

import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;

/**
 * This HttpsURLConnection factory creates connections with certificate pinning.
 * The trusted certificates are provided in a trust store.
 */
class PinnedCertificateHttpsURLConnectionFactory
{
	private final static String TAG = PinnedCertificateHttpsURLConnectionFactory.class.getSimpleName();
	private ArrayList<String> mHostNames = new ArrayList<>();
	
	PinnedCertificateHttpsURLConnectionFactory()
	{
		setHostNames();
	}
	
	private void setHostNames()
	{
		mHostNames = new ArrayList<String>()
		{{
			add(SslUtils.SSL_LOCAL_HOST_PROD);
			add(SslUtils.SSL_LOCAL_HOST_DEV);
			add(SslUtils.SSL_LOCAL_HOST_MATMID);
		}};
	}
	
	HttpsURLConnection createHttpsURLConnection(String urlString, PubKeyManager iPubKeyManager) throws Throwable
	{
		// Initialize the SSL context.
		TrustManager[] wrappedTrustManagers = {iPubKeyManager};
		SSLContext sslContext = SSLContext.getInstance(SslUtils.PROTOCOL_TLSv1_2);
		sslContext.init(null, wrappedTrustManagers, null);
		
		// Create the https URL connection.
		URL url = new URL(urlString);
		HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
		urlConnection.setSSLSocketFactory(sslContext.getSocketFactory());
		urlConnection.setHostnameVerifier(getHostnameVerifier());
		
		return urlConnection;
	}
	
	// Let's assume your server app is hosting inside a server machine
	// which has a server certificate in which "Issued to" is "localhost",for example.
	// Then, inside verify method you can verify "localhost".
	// If not, you can temporarily return true
	@NonNull
	@Contract(pure = true)
	private HostnameVerifier getHostnameVerifier()
	{
		return new HostnameVerifier()
		{
			@Override
			public boolean verify(String hostname, SSLSession session)
			{
				
				HostnameVerifier hv = HttpsURLConnection.getDefaultHostnameVerifier();
				
				int count = 0;
				for (String hostName : mHostNames)
				{
					if (hv.verify(hostName, session))
					{
						count++;
						break;
					}
				}
				
				return count == SslUtils.COUNT_OF_HOST_MATCHES;
				//				return true;
			}
		};
	}
}
