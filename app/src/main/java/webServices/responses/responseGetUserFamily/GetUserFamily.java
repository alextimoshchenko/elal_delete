package webServices.responses.responseGetUserFamily;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class GetUserFamily
{
	@JsonProperty("FamilyMembers")
	private ArrayList<GetUserFamilyContent> mFamily;
	
	public ArrayList<GetUserFamilyContent> getFamily()
	{
		return this.mFamily;
	}
	
	public GetUserFamily()
	{
	}
}
