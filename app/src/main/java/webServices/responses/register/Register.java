package webServices.responses.register;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Register
{
	@JsonProperty("Success")
	private boolean mIsSuccess;
	@JsonProperty("UserID")
	private int mUserID;
	
	public Register()
	{
	}
	
	public boolean isSuccess()
	{
		return mIsSuccess;
	}
	
	public int getUserID()
	{
		return mUserID;
	}
}
