package webServices.responses.getPushNotifications;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

import webServices.responses.deleteUserPushMessage.NotificationInstance;
import webServices.responses.deleteUserPushMessage.UserMessage;

/**
 * Created with care by Alexey.T on 26/07/2017.
 */
public class ResponseGetPushNotificationsContainer
{
	@JsonProperty("Notifications")
	private List<NotificationInstance> mNotificationInstanceList = new ArrayList<>();
	
	public List<UserMessage> getNotificationInstanceList()
	{
		List<UserMessage> list = new ArrayList<>();
		list.addAll(mNotificationInstanceList);
//		return list ;
		return  (List<UserMessage>)(List<?>) mNotificationInstanceList ;
	}
}
