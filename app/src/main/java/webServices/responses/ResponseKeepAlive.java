package webServices.responses;

import webServices.global.Response;
import webServices.responses.responseMatmidKeepAlive.MatmidKeepAliveData;
import webServices.responses.responseSetUserMatmidDetails.MatmidUpdateMemberData;

/**
 * Created by erline.katz on 23/10/2017.
 */

public class ResponseKeepAlive extends Response<MatmidKeepAliveData>
{
}
