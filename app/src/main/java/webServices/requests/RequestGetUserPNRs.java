package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestGetUserPNRs
{
	@JsonProperty("UserId")
	private int mUserId;
	
	public RequestGetUserPNRs(int iUserId)
	{
		mUserId = iUserId;
	}
	
	public RequestGetUserPNRs()
	{
	}
}
