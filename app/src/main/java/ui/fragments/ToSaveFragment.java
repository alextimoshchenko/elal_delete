package ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import global.IntentExtras;
import il.co.ewave.elal.R;
import webServices.responses.responseGetUserActivePNRs.UserActivePnr;

/**
 * Created with care by Shahar Ben-Moshe on 31/01/17.
 */

public class ToSaveFragment extends EWBaseFragment
{
	public static Bundle createBundleForFragment(final UserActivePnr iUserActivePnr)
	{
		Bundle result = new Bundle();
		
		result.putParcelable(IntentExtras.USER_ACTIVE_PNR, iUserActivePnr);
		
		return result;
	}
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_to_save_layout);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
	}
}
