package ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;

import java.util.List;

import global.ElAlApplication;
import global.UserData;
import il.co.ewave.elal.R;
import interfaces.IMyDocumentsClickListener;
import utils.global.AppUtils;
import utils.global.DateTimeUtils;
import utils.global.dbObjects.Document;
import utils.managers.RealmManager;
import webServices.global.ePnrFlightType;
import webServices.responses.responseGetUserActivePNRs.PnrFlight;
import webServices.responses.responseGetUserActivePNRs.UserActivePnr;

/**
 * Created with care by Shahar Ben-Moshe on 24/01/17.
 */

public class MyDocumentsAdapter extends ExpandableRecyclerAdapter<UserActivePnr, Document.eDocumentType, MyDocumentsAdapter.ParentViewHolder, MyDocumentsAdapter.ChildViewHolder> implements ExpandableRecyclerAdapter.ExpandCollapseListener
{
	private static final String TAG = MyDocumentsAdapter.class.getSimpleName();
	private static final int COUPONS_COLUMNS = 2;
	private static final int HEADER_COLUMNS = 1;
	
	private static final int HEADER_LAYOUT_RESOURCE_ID = R.layout.my_documents_header_item_cell_layout;
	private static final int CHILD_LAYOUT_RESOURCE_ID = R.layout.my_documents_child_item_cell_layout;
	private final IMyDocumentsClickListener mMyDocumentsClickListener;
	private GridLayoutManager mLayoutManager;
	private int mLastExpanded = -1;
	private boolean mIsFlightDocs = false;
	private int mNumFlightDocs;
	private Context mContext;
	
	public MyDocumentsAdapter(final Context iContext, @NonNull final List<UserActivePnr> parentList, final IMyDocumentsClickListener iMyDocumentsClickListener, boolean iIsFlightDocs, int iNumFlightDocs)
	{
		super(parentList);
		
		mContext = iContext;
		mIsFlightDocs = iIsFlightDocs;
		mNumFlightDocs = iNumFlightDocs;
		mMyDocumentsClickListener = iMyDocumentsClickListener;
		mLayoutManager = new GridLayoutManager(iContext, COUPONS_COLUMNS)
		{
			@Override
			protected boolean isLayoutRTL()
			{
				return !AppUtils.isDefaultLocaleRTL();
				//						false;
			}
		};
		mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup()
		{
			@Override
			public int getSpanSize(final int position)
			{
				return getSpanByPosition(position);
			}
		});
		
		setExpandCollapseListener(this);
		
		if (isSinglePnr())
		{
			expandParent(0);
		}
	}
	
	private int getSpanByPosition(final int iPosition)
	{
		return getItemViewType(iPosition) == TYPE_PARENT ? COUPONS_COLUMNS : HEADER_COLUMNS;
	}
	
	private boolean isSinglePnr()
	{
		return getParentList().size() == 1;
	}
	
	public RecyclerView.LayoutManager getLayoutManager()
	{
		return mLayoutManager;
	}
	
	@NonNull
	@Override
	public ParentViewHolder onCreateParentViewHolder(@NonNull final ViewGroup parentViewGroup, final int viewType)
	{
		return new ParentViewHolder(LayoutInflater.from(parentViewGroup.getContext()).inflate(HEADER_LAYOUT_RESOURCE_ID, parentViewGroup, false));
	}
	
	@NonNull
	@Override
	public ChildViewHolder onCreateChildViewHolder(@NonNull final ViewGroup childViewGroup, final int viewType)
	{
		return new ChildViewHolder(LayoutInflater.from(childViewGroup.getContext()).inflate(CHILD_LAYOUT_RESOURCE_ID, childViewGroup, false));
	}
	
	@Override
	public void onBindParentViewHolder(@NonNull final ParentViewHolder iParentViewHolder, final int iParentPosition, @NonNull final UserActivePnr iParent)
	{
		//        if (isSinglePnr())
		if (mIsFlightDocs)
		{
			iParentViewHolder.llRoot.setVisibility(View.GONE);
			iParentViewHolder.lilHederContainer.setVisibility(View.GONE);
			iParentViewHolder.tvName.setVisibility(View.GONE);
			iParentViewHolder.ivArrow.setVisibility(View.GONE);
		}
		else
		{
			//            iParentViewHolder.mLlRoot.setVisibility(View.VISIBLE);
			iParentViewHolder.tvName.setVisibility(View.VISIBLE);
			iParentViewHolder.ivArrow.setVisibility(View.VISIBLE);
			
			setGuiByExpandState(iParentViewHolder);
			
			PnrFlight firstFlight = iParent.getCurrentItemFlightAtPositionDepartureOrReurnOrNull(0, ePnrFlightType.DEPARTURE);
			
			if (firstFlight != null && !TextUtils.isEmpty(firstFlight.getDestinationCityName()) && firstFlight.getDepartureDate() != null)
			{
				// TODO: 13/12/2017 - get outer destination
				iParentViewHolder.tvName.setText(ElAlApplication.getInstance().getString(R.string.documents_of_flight_to2, iParent.getmPNRDestination(),
						DateTimeUtils.convertDateToSlashSeparatedDayMonthYearTwoNumbersString(DateTimeUtils.convertDateToDateIgnoreGMT(firstFlight.getDepartureDate()))));
			}
			else
			{
				iParentViewHolder.tvName.setText("");
			}
		}
	}
	
	@Override
	public void onBindChildViewHolder(@NonNull final ChildViewHolder iChildViewHolder, final int iParentPosition, final int iChildPosition, @NonNull final Document.eDocumentType iChild)
	{
		setGuiByPosition(iChildViewHolder, iParentPosition, iChildPosition);
	}
	
	private void setGuiByPosition(final ChildViewHolder childViewHolder, final int iParentPosition, final int iChildPosition)
	{
		final Document.eDocumentType documentType = Document.eDocumentType.getDocumentTypeById(iChildPosition);
		long documentsCount;
		
		if (documentType != null && documentType == Document.eDocumentType.FLIGHT_DETAILS)
		{
			documentsCount = mNumFlightDocs;
			
			if (!mIsFlightDocs)
			{
				try
				{
					UserActivePnr pnr = getParentList().get(iParentPosition);
					documentsCount = pnr.getDeparturePNRFlights().size() + pnr.getReturnPNRFlights().size();
				}
				catch (Exception e)
				{
					AppUtils.printLog(Log.ERROR, TAG, e.getMessage());
				}
			}
			
			childViewHolder.btnAddDocument.setVisibility(View.GONE);
		}
		else
		{
			documentsCount = 0;
			
			if (!mIsFlightDocs)
			{
				childViewHolder.btnAddDocument.setVisibility(View.GONE);
			}
			else
			{
				childViewHolder.btnAddDocument.setVisibility(View.VISIBLE);
			}
			
			documentsCount = RealmManager.getInstance().getDocumentCountForCategory(getParentList().get(iParentPosition).getPnr(), UserData.getInstance().getUserID(), documentType);
		}
		
		childViewHolder.ivImage.setImageResource(documentType != null ? documentType.getIconResourceId() : R.color.transparent);
		childViewHolder.tvName.setText(documentType != null ? mContext.getResources().getString(documentType.getNameResourceId()) : "");
		
		if (documentsCount > 0)
		{
			childViewHolder.tvStatus.setText(ElAlApplication.getInstance().getString(R.string.documents_count, String.valueOf(documentsCount)));
		}
		else
		{
			childViewHolder.tvStatus.setText("");
		}
		
		final long finalDocumentsCount = documentsCount;
		
		childViewHolder.llRoot.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				if (mMyDocumentsClickListener != null)
				{
					mMyDocumentsClickListener.onChildClick(iParentPosition, iChildPosition, documentType, finalDocumentsCount);
				}
			}
		});
	}
	
	private void setGuiByExpandState(final ParentViewHolder iParentViewHolder)
	{
		if (iParentViewHolder != null)
		{
			if (iParentViewHolder.isExpanded())
			{
				iParentViewHolder.ivArrow.setImageResource(R.mipmap.close_b);
			}
			else
			{
				iParentViewHolder.ivArrow.setImageResource(R.mipmap.open_b);
			}
		}
	}
	
	@Override
	public void onParentExpanded(final int iParentPosition)
	{
		if (!isSinglePnr())
		{
			if (mLastExpanded >= 0)
			{
				notifyParentChanged(mLastExpanded);
				collapseParent(mLastExpanded);
			}
			
			mLastExpanded = iParentPosition;
			notifyParentChanged(mLastExpanded);
		}
	}
	
	@Override
	public void onParentCollapsed(final int iParentPosition)
	{
		notifyParentChanged(iParentPosition);
		mLastExpanded = -1;
	}
	
	class ParentViewHolder extends com.bignerdranch.expandablerecyclerview.ParentViewHolder
	{
		private View llRoot;
		private LinearLayout lilHederContainer;
		private TextView tvName;
		private ImageView ivArrow;
		
		ParentViewHolder(@NonNull final View itemView)
		{
			super(itemView);
			
			llRoot = itemView.findViewById(R.id.ll_myDocumentsHeader_Root);
			lilHederContainer = (LinearLayout) itemView.findViewById(R.id.lilHeaderContainer);
			ivArrow = (ImageView) itemView.findViewById(R.id.iv_myDocumentsHeader_Arrow);
			tvName = (TextView) itemView.findViewById(R.id.tv_myDocumentsHeader_Name);
		}
		
		@Override
		public boolean shouldItemViewClickToggleExpansion()
		{
			return true;
		}
	}
	
	class ChildViewHolder extends com.bignerdranch.expandablerecyclerview.ChildViewHolder
	{
		
		private LinearLayout llRoot;
		private ImageView ivImage;
		private TextView tvName;
		private TextView tvStatus;
		private Button btnAddDocument;
		
		ChildViewHolder(@NonNull final View itemView)
		{
			super(itemView);
			
			llRoot = (LinearLayout) itemView.findViewById(R.id.ll_myDocumentsChild_Root);
			tvStatus = (TextView) itemView.findViewById(R.id.tv_myDocumentsChild_Status);
			btnAddDocument = (Button) itemView.findViewById(R.id.btn_addDocument);
			tvName = (TextView) itemView.findViewById(R.id.tv_myDocumentsChild_Name);
			ivImage = (ImageView) itemView.findViewById(R.id.iv_myDocumentsChild_Image);
		}
	}
}
