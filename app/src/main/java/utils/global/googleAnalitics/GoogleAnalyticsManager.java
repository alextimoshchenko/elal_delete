package utils.global.googleAnalitics;

import android.content.Context;

import utils.global.googleAnalitics.TrackerManager.AnalyticTracker;
import utils.global.googleAnalitics.googleTagManager.TagManagerImplementation;

/**
 * Created with care by Alexey.T on 28/08/2017.
 * <p>
 * It is manager class for all google analytics features, if you need to add new requirements add here. Create a new feature class and set access from this object (for instance take a look
 * TagManagerImplementation)
 */
public class GoogleAnalyticsManager
{
	private AnalyticsCallBack mAnalyticsCallBack;
	private TagManagerImplementation mTagManager;
	private static GoogleAnalyticsManager mObject;
	private AnalyticTracker mTracker;
	
	
	private GoogleAnalyticsManager()
	{
	}
	
	public void setAnalyticsCallBack(AnalyticsCallBack iAnalyticsCallBack)
	{
		mAnalyticsCallBack = iAnalyticsCallBack;
	}
	
	/**
	 * We need to initialize this method because we can not to pass Context in constructor of Singleton class.
	 */
	public void init(Context iContext)
	{
		initTagManager(iContext);
		initTracker();
	}
	
	private void initTagManager(Context iContext)
	{
		if (mTagManager == null)
		{
			mTagManager = new TagManagerImplementation(iContext);
			mTagManager.init();
		}
	}
	
	private void initTracker()
	{
		if (mTracker == null)
		{
			mTracker = new AnalyticTracker();
		}
	}
	
	public AnalyticTracker getTracker()
	{
		return mTracker;
	}
	
	public TagManagerImplementation getTagManager()
	{
		if (mTagManager == null)
		{
			throw new RuntimeException("You need to call GoogleAnalyticsManager.init() before");
		}
		
		return mTagManager;
	}
	
	public static GoogleAnalyticsManager getAnalyticsManagerObject()
	{
		if (mObject == null)
		{
			mObject = new GoogleAnalyticsManager();
		}
		
		return mObject;
	}
}
