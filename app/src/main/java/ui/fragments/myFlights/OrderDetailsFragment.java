package ui.fragments.myFlights;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import global.IntentExtras;
import global.UserData;
import il.co.ewave.elal.R;
import ui.adapters.FlightOrdersViewPagerAdapter;
import ui.fragments.EWBaseFragment;
import utils.global.AppUtils;
import webServices.controllers.MyFlightsController;
import webServices.requests.RequestCompletePnr;
import webServices.responses.ResponseGetPnrByNum;
import webServices.responses.responseGetUserActivePNRs.UserActivePnr;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderDetailsFragment extends EWBaseFragment
{
	private static final String TAG = OrderDetailsFragment.class.getSimpleName();
	
	private UserActivePnr mUserActivePnr;
	private ImageView mIvClose;
	private ViewPager mVpOrdersSwipe;
	private LinearLayout mLilSlideIndicatorsHolder;
	private FlightOrdersViewPagerAdapter mFlightOrdersViewPagerAdapter;
	private boolean mCalledGetCompletePnrOnce = false;
	
	
	@Override
	public void onServiceConnected()
	{
		if (mUserActivePnr != null && !mCalledGetCompletePnrOnce)
		{
			getCompletePNR(mUserActivePnr.getPnr());
		}
	}
	
	public static Bundle createBundleForFragment(@NonNull final UserActivePnr iUserActivePnr)
	{
		Bundle result = new Bundle();
		
		result.putParcelable(IntentExtras.USER_ACTIVE_PNR, iUserActivePnr);
		
		return result;
	}
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_order_details);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		getIntentExtras();
		initReference();
		setListeners();
	}
	
	private void getIntentExtras()
	{
		if (getArguments() != null)
		{
			mUserActivePnr = getArguments().getParcelable(IntentExtras.USER_ACTIVE_PNR);
		}
	}
	
	private void initReference()
	{
		mVpOrdersSwipe = (ViewPager) getActivity().findViewById(R.id.vp_flightDetails_OrdersSwipe);
		mLilSlideIndicatorsHolder = (LinearLayout) getActivity().findViewById(R.id.lilSlideIndicatorsHolder);
		mIvClose = (ImageView) getActivity().findViewById(R.id.iv_flightDetails_Close);
	}
	
	private void setListeners()
	{
		mIvClose.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				tryPopFragment();
			}
		});
	}
	
	
	private void getCompletePNR(String iFlightPnr)
	{
		if (isServiceConnected())
		{
			trySetProgressDialog(true);
			
			int userId = UserData.getInstance().getUserID();
			
			getService().getController(MyFlightsController.class).getCompletePNR(new RequestCompletePnr(userId, iFlightPnr), new Response.Listener<ResponseGetPnrByNum>()
			{
				@Override
				public void onResponse(final ResponseGetPnrByNum iResponse)
				{
					if (iResponse != null)
					{
						doOnPnrReceived(iResponse.getContent());
					}
					trySetProgressDialog(false);
				}
			}, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError error)
				{
					trySetProgressDialog(false);
				}
			});
		}
	}
	
	private void doOnPnrReceived(UserActivePnr iUserActivePnr)
	{
		mCalledGetCompletePnrOnce = true;
		mFlightOrdersViewPagerAdapter = new FlightOrdersViewPagerAdapter(getContext(), iUserActivePnr);
		if (iUserActivePnr != null)
		{
			try
			{
				updateSlidingIndicators((iUserActivePnr.getDeparturePNRFlights() != null ? iUserActivePnr.getDeparturePNRFlights().size() : 0) + (iUserActivePnr.getReturnPNRFlights() != null ? iUserActivePnr.getReturnPNRFlights().size() : 0));
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		
		if (AppUtils.isDefaultLocaleRTL())
		{
			mVpOrdersSwipe.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
			mVpOrdersSwipe.setRotationY(180);
			mLilSlideIndicatorsHolder.setRotationY(180);
		}
		else
		{
			mVpOrdersSwipe.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
		}
		mVpOrdersSwipe.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
		{
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
			{
			}
			
			@Override
			public void onPageSelected(int position)
			{
//				if (AppUtils.isDefaultLocaleRTL())
//				{
//					position = mLilSlideIndicatorsHolder.getChildCount() - position - 1;
//				}
				
				mLilSlideIndicatorsHolder.setVisibility(View.VISIBLE);
				for (int i = 0 ; i < mLilSlideIndicatorsHolder.getChildCount() ; i++)
				{
					if (i == position)
					{
						((ImageView) mLilSlideIndicatorsHolder.getChildAt(position)).setColorFilter(ContextCompat.getColor(getContext(), R.color.topaz));
					}
					else
					{
						((ImageView) mLilSlideIndicatorsHolder.getChildAt(i)).setColorFilter(ContextCompat.getColor(getContext(), R.color.ElalLightGray));
					}
				}
			}
			
			@Override
			public void onPageScrollStateChanged(int state)
			{
			}
		});
		mVpOrdersSwipe.setAdapter(mFlightOrdersViewPagerAdapter);
		mFlightOrdersViewPagerAdapter.notifyDataSetChanged();
	}
	
	private void updateSlidingIndicators(int iIndicatorsSum)
	{
		if (iIndicatorsSum > 1)
		{
			if (mLilSlideIndicatorsHolder != null)
			{
				mLilSlideIndicatorsHolder.setVisibility(View.VISIBLE);
				mLilSlideIndicatorsHolder.removeAllViewsInLayout();
				for (int i = 0 ; i < iIndicatorsSum; i++)
				{
					ImageView imgIndicator = tryGenerateIndicator();
					if (imgIndicator != null)
					{
						mLilSlideIndicatorsHolder.addView(imgIndicator);
					}
				}
				if (mLilSlideIndicatorsHolder != null && mLilSlideIndicatorsHolder.getChildCount() > 0)
				{
					((ImageView) mLilSlideIndicatorsHolder.getChildAt(0)).setImageResource(R.drawable.topaz_circle);
					((ImageView) mLilSlideIndicatorsHolder.getChildAt(0)).setColorFilter(ContextCompat.getColor(getContext(), R.color.topaz));
				}
			}
		}
		else
		{
			mLilSlideIndicatorsHolder.setVisibility(View.INVISIBLE);
		}
	}
	
	private ImageView tryGenerateIndicator()
	{
		try
		{
			ImageView imgIndicator = new ImageView(getContext());
			imgIndicator.setImageResource(R.drawable.topaz_circle);
			imgIndicator.setColorFilter(getResources().getColor(R.color.ElalLightGray));
			imgIndicator.setPadding(5, 5, 5, 5);
			int width = 30;
			int height = 30;
			LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(width, height);
			imgIndicator.setLayoutParams(parms);
			
			return imgIndicator;
		}
		catch (Exception e)
		{
			trySetProgressDialog(false);
			e.printStackTrace();
			return null;
		}
	}
	
	
}
