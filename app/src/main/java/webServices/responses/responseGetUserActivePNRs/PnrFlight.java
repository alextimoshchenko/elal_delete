package webServices.responses.responseGetUserActivePNRs;


import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

import utils.global.DateTimeUtils;
import webServices.global.ePnrFlightType;

public class PnrFlight implements Parcelable
{
	@JsonProperty("AttractionLink")
	private String mAttractionLink;
	@JsonProperty("HotelLink")
	private String mHotelLink;
	@JsonProperty("VehicleLink")
	private String mVehicleLink;
	@JsonProperty("OriginationCityName")
	private String mOriginationCityName;
	@JsonProperty("DestinationCityName")
	private String mDestinationCityName;
	@JsonProperty("OriginationImage")
	private String mOriginationImage;
	@JsonProperty("DestinationImage")
	private String mDestinationImage;
	@JsonProperty("OriginationInfo")
	private String mOriginationInfo;
	@JsonProperty("DestinationInfo")
	private String mDestinationInfo;
	
//	@JsonProperty("TimezoneOffset")
//	private String mTimezoneOffset;
	
	@JsonProperty("DepartureTimezoneOffset")
	private String mDepartureTimezoneOffset;
	
	@JsonProperty("ArrivalTimezoneOffset")
	private String mArrivalTimezoneOffset;
	
	
	@JsonProperty("FlightNumber")
	private String mFlightNumber;
	@JsonProperty("PNR")
	private String mPNR;
	@JsonProperty("Origination")
	private String mOrigination;
	@JsonProperty("Destination")
	private String mDestination;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeUtils.DATE_FORMAT_yyyy_MM_ddHHmmSS)
	@JsonProperty("DepartureDate")
	private Date mDepartureDate;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeUtils.DATE_FORMAT_yyyy_MM_ddHHmmSS)
	@JsonProperty("ArrivalDate")
	private Date mArrivalDate;
	
	@JsonProperty("FlightStatus")
	private String mFlightStatus;
	@JsonProperty("FlightDuration")
	private String mFlightDuration;
	@JsonProperty("ReferenceId")
	private String mReferenceId;
	@JsonProperty("FlightCompany")
	private String mFlightCompany;
	@JsonProperty("AircraftType")
	private String mAircraftType;
	@JsonProperty("DepartTerminal")
	private String mDepartTerminal;
	@JsonProperty("ArrivalTerminal")
	private String mArraivalTerminal;
	@JsonProperty("Segment")
	private int mSegment;
	@JsonProperty("Carrier")
	private String mCarrier;
	@JsonProperty("Direction")
	private String mDirection;
	@JsonProperty("ClassOfService")
	private String mClassOfService;
	@JsonProperty("CreateDate")
	private String mCreateDate;
	@JsonProperty("LastModified")
	private String mLastModified;
	@JsonProperty("IsActive")
	private Boolean mIsActive;
	@JsonProperty("IsError")
	private boolean mIsError;
	@JsonProperty("Pit")
	private String mPit;
	@JsonProperty("BoardingDate")
	private Date mBoardingDate;
	
	private ePnrFlightType eNumDirection;
	
	
	public PnrFlight()
	{
	}
	
	//TEST DELETE IT
	PnrFlight setTestData(String iOriginationCityName, String iDestinationCityName, Date iDepartureDate, Date iArrivalDate)
	{
		mOrigination = iOriginationCityName;
		mDestination = iDestinationCityName;
		mDepartureDate = iDepartureDate;
		mArrivalDate = iArrivalDate;
		
		return this;
	}
	
	public String getAttractionLink()
	{
		return mAttractionLink;
	}
	
	public String getHotelLink()
	{
		return mHotelLink;
	}
	
	public String getVehicleLink()
	{
		return mVehicleLink;
	}
	
	public String getOriginationCityName()
	{
		return mOriginationCityName;
	}
	
	public String getDestinationCityName()
	{
		return mDestinationCityName;
	}
	
	public String getOriginationImage()
	{
		return mOriginationImage;
	}
	
	public String getDestinationImage()
	{
		return mDestinationImage;
	}
	
	public String getOriginationInfo()
	{
		return mOriginationInfo;
	}
	
	public String getDestinationInfo()
	{
		return mDestinationInfo;
	}
	
//	public String getTimezoneOffset()
//	{
//		return mTimezoneOffset;
//	}
	
	
	public String getmDepartureTimezoneOffset()
	{
		return mDepartureTimezoneOffset;
	}
	
	public String getmArrivalTimezoneOffset()
	{
		return mArrivalTimezoneOffset;
	}
	
	public String getFlightNumber()
	{
		return mFlightNumber;
	}
	
	public String getPNR()
	{
		return mPNR;
	}
	
	public String getOrigination()
	{
		return mOrigination;
	}
	
	public String getDestination()
	{
		return mDestination;
	}
	
	public Date getDepartureDate()
	{
		return mDepartureDate;
	}
	
	public Date getArrivalDate()
	{
		return mArrivalDate;
	}
	
	public String getFlightStatus()
	{
		return mFlightStatus;
	}
	
	public String getFlightDuration()
	{
		return mFlightDuration;
	}
	
	public String getReferenceId()
	{
		return mReferenceId;
	}
	
	public String getFlightCompany()
	{
		return mFlightCompany;
	}
	
	public String getAircraftType()
	{
		return mAircraftType;
	}
	
	public String getDepartTerminal()
	{
		if (mDepartTerminal != null && !mDepartTerminal.equals("_"))
		{
			return mDepartTerminal;
		}
		else
		{
			return "";
		}
	}
	
	public String getArraivalTerminal()
	{
		if (mArraivalTerminal != null && !mArraivalTerminal.equals("_"))
		{
			return mArraivalTerminal;
		}
		else
		{
			return "";
		}
	}
	
	public void setArraivalTerminal(String mArraivalTerminal)
	{
		this.mArraivalTerminal = mArraivalTerminal;
	}
	
	public int getSegment()
	{
		return mSegment;
	}
	
	public String getCarrier()
	{
		return mCarrier;
	}
	
	private String getDirection()
	{
		return mDirection;
	}
	
	public String getClassOfService()
	{
		return mClassOfService;
	}
	
	public String getCreateDate()
	{
		return mCreateDate;
	}
	
	public String getLastModified()
	{
		return mLastModified;
	}
	
	public boolean isActive()
	{
		if (mIsActive != null)
		{
			return mIsActive;
		}
		else
		{
			return false;
		}
	}
	
	public boolean isError()
	{
		return mIsError;
	}
	
	public String getPit()
	{
		return mPit;
	}
	
	public Date getBoardingDate()
	{
		return mBoardingDate;
	}
	
	
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeString(this.mAttractionLink);
		dest.writeString(this.mHotelLink);
		dest.writeString(this.mVehicleLink);
		dest.writeString(this.mOriginationCityName);
		dest.writeString(this.mDestinationCityName);
		dest.writeString(this.mOriginationImage);
		dest.writeString(this.mDestinationImage);
		dest.writeString(this.mOriginationInfo);
		dest.writeString(this.mDestinationInfo);
//		dest.writeString(this.mTimezoneOffset);
		dest.writeString(this.mDepartureTimezoneOffset);
		dest.writeString(this.mArrivalTimezoneOffset);
		dest.writeString(this.mFlightNumber);
		dest.writeString(this.mPNR);
		dest.writeString(this.mOrigination);
		dest.writeString(this.mDestination);
		dest.writeLong(this.mDepartureDate != null ? this.mDepartureDate.getTime() : -1);
		dest.writeLong(this.mArrivalDate != null ? this.mArrivalDate.getTime() : -1);
		dest.writeString(this.mFlightStatus);
		dest.writeString(this.mFlightDuration);
		dest.writeString(this.mReferenceId);
		dest.writeString(this.mFlightCompany);
		dest.writeString(this.mAircraftType);
		dest.writeString(this.mDepartTerminal);
		dest.writeInt(this.mSegment);
		dest.writeString(this.mCarrier);
		dest.writeString(this.mDirection);
		dest.writeString(this.mClassOfService);
		dest.writeString(this.mCreateDate);
		dest.writeString(this.mLastModified);
		dest.writeByte(this.mIsActive ? (byte) 1 : (byte) 0);
		dest.writeByte(this.mIsError ? (byte) 1 : (byte) 0);
		dest.writeString(this.mPit);
		dest.writeLong(this.mBoardingDate != null ? this.mBoardingDate.getTime() : -1);
	}
	
	protected PnrFlight(Parcel in)
	{
		this.mAttractionLink = in.readString();
		this.mHotelLink = in.readString();
		this.mVehicleLink = in.readString();
		this.mOriginationCityName = in.readString();
		this.mDestinationCityName = in.readString();
		this.mOriginationImage = in.readString();
		this.mDestinationImage = in.readString();
		this.mOriginationInfo = in.readString();
		this.mDestinationInfo = in.readString();
//		this.mTimezoneOffset = in.readString();
		this.mDepartureTimezoneOffset = in.readString();
		this.mArrivalTimezoneOffset = in.readString();
		this.mFlightNumber = in.readString();
		this.mPNR = in.readString();
		this.mOrigination = in.readString();
		this.mDestination = in.readString();
		long tmpMDepartureDate = in.readLong();
		this.mDepartureDate = tmpMDepartureDate == -1 ? null : new Date(tmpMDepartureDate);
		long tmpMArrivalDate = in.readLong();
		this.mArrivalDate = tmpMArrivalDate == -1 ? null : new Date(tmpMArrivalDate);
		this.mFlightStatus = in.readString();
		this.mFlightDuration = in.readString();
		this.mReferenceId = in.readString();
		this.mFlightCompany = in.readString();
		this.mAircraftType = in.readString();
		this.mDepartTerminal = in.readString();
		this.mSegment = in.readInt();
		this.mCarrier = in.readString();
		this.mDirection = in.readString();
		this.mClassOfService = in.readString();
		this.mCreateDate = in.readString();
		this.mLastModified = in.readString();
		this.mIsActive = in.readByte() != 0;
		this.mIsError = in.readByte() != 0;
		this.mPit = in.readString();
		long tmpMBoardingDate = in.readLong();
		this.mBoardingDate = tmpMBoardingDate == -1 ? null : new Date(tmpMBoardingDate);
	}
	
	public static final Parcelable.Creator<PnrFlight> CREATOR = new Parcelable.Creator<PnrFlight>()
	{
		@Override
		public PnrFlight createFromParcel(Parcel source)
		{
			return new PnrFlight(source);
		}
		
		@Override
		public PnrFlight[] newArray(int size)
		{
			return new PnrFlight[size];
		}
	};
	
	String getDepartureTime()
	{
		return DateTimeUtils.convertDateToDayMonthHourMinuteString(getDepartureDate());
	}
	
	String getArrivalTime()
	{
		return DateTimeUtils.convertDateToDayMonthHourMinuteString(getArrivalDate());
	}
	
//	public ePnrFlightType geteNumDirection()
//	{
//		String direction = this.getDirection();
//		if(direction == null || direction.isEmpty() || direction.equals(""))
//		{
//			return null;
//		}
//		else if(direction.equals("OUT"))
//		{
//			return ePnrFlightType.DEPARTURE;
//		}
//		else
//		{
//			return ePnrFlightType.RETURN;
//		}
//	}
	
	public void seteNumDirection(ePnrFlightType iDirection)
	{
		eNumDirection = iDirection;
	}
	
	public ePnrFlightType geteNumDirection()
	{
		return eNumDirection;
	}
}
