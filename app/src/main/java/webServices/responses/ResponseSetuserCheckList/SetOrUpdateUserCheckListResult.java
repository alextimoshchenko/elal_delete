package webServices.responses.ResponseSetuserCheckList;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with care by Shahar Ben-Moshe on 21/02/17.
 */

public class SetOrUpdateUserCheckListResult
{
	@JsonProperty("CheckListID")
	private int mCheckListID;
	
	public SetOrUpdateUserCheckListResult()
	{
	}
	
	public int getCheckListID()
	{
		return mCheckListID;
	}
}
