package global;

/**
 * Created with care by Shahar Ben-Moshe on 14/12/16.
 */

public class IntentExtras
{
	public static final String IS_FROM_PUSH_NOTIFICATION = "isFromPushNotification";
	public static final String IS_IN_APP_NAVIGATION = "isInAppNavigation";
	public static final String PROMO_DATA = "promoData";
	public static final String URL = "url";
	public static final String OCR_TYPE = "ocrType";
	public static final String DOCUMENT_SCAN_RESULTS = "documentScanResults";
	public static final String SCAN_RESULT_RECEIVING_FRAGMENT = "scanResultReceivingFragment";
	public static final String USER_ID = "userId";
	public static final String DESTINATION_TYPE = "destinationType ";
	public static final String IN_APP_NAVIGATION_ID = "inAppNavigationId";
	public static final String USER_ACTIVE_PNRS = "userActivePnrs";
	public static final String ADD_FLIGHT_LISTENER = "addFlightListener";
	public static final String FAMILY_OBJ = "familyObj";
	public static final String CREATE_NEW_MEMBER = "createNewMember";
	public static final String PNR = "pnr";
	public static final String APP_CONTENTS = "appContents";
	public static final String USER_ACTIVE_PNR = "userActivePnr";
	public static final String FLIGHT_DOCS = "flightDocs";
	public static final String NUM_FLIGHT_DOCS = "num_flight_docs";
	public static final String USER_FLIGHT_DESTINATION = "flightDestination";
	public static final String PNR_DESTINATIONS = "pnrDestinations";
	public static final String DESTINATION_IATA = "destinationIata";
	public static final String DESTINATION_NAME = "destination_name";
	public static final String DESTINATION_INFO = "destination_info";
	public static final String CHECK_LIST_ITEM = "checkListItem";
	public static final String ADD_OR_EDIT_CHECK_LIST_ITEM_LISTENER = "addOrEditCheckListItemListener";
	public static final String USER_CHECK_LIST = "userCheckList";
	public static final String WEATHER = "weather";
	public static final String DOCUMENT = "document";
	public static final String DOCUMENT_TYPE = "documentType";
	public static final String SELECT_TYPE = "select_type";
	public static final String SHOULD_BE_ABLE_TO_ADD = "shouldBeAbleToAdd";
	public static final String LAST_SELECTED_LOGIN_TYPE = "lastSelectedLoginType";
	public static final String LOGGEDIN_TYPE = "loggedInType";
	public static final String LOGIN_AS_MATMID = "loginAsMatmid";
	public static final String IS_MATMID = "isMatmid";
	public static final String IS_REGISTRATION = "isRegistration";
	public static final String IS_GROUP_PNR = "isGroupPnr";
	public static final String SEARCH_VIEW_WEB_VIEW_TITLE = "search_view_web_view_title";
	public static final String ENC_PARAMETERS = "ENC_parameters";
}
