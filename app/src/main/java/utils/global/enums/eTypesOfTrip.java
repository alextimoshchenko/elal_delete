package utils.global.enums;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import java.util.Arrays;
import java.util.List;

import il.co.ewave.elal.R;
import interfaces.IChoseActionElement;
import utils.errors.LocalError;
import utils.global.AppUtils;

/**
 * Created with care by Alexey.T on 10/07/2017.
 * <p>
 * TODO: Add a class header comment!
 */
public enum eTypesOfTrip implements IChoseActionElement
{
	ROUND_TRIP(R.string.round_trip, 1, 1),
	ONE_WAY_TICKET(R.string.one_way, 2, 0),
	RETURN_FROM_ANOTHER_DESTINATION(R.string.return_from_another_destination, 3, 2),
	NO_SUCH_ITEM(R.string.no_such_items, -1, -1);
	
	private static String TAG = eTypesOfTrip.class.getSimpleName();
	
	private int mTypeOfTripNameResourceId;
	private int mTypeOfTripId;
	private int mTypeForWebView;
	
	eTypesOfTrip(final int iTypeOfTripNameResourceId, int iTypeOfTripId, int iTypeForWebView)
	{
		mTypeOfTripNameResourceId = iTypeOfTripNameResourceId;
		mTypeOfTripId = iTypeOfTripId;
		mTypeForWebView = iTypeForWebView;
	}
	
	public static eTypesOfTrip getTypeById(int iId)
	{
		eTypesOfTrip result;
		
		switch (iId)
		{
			case 1:
				result = ROUND_TRIP;
				break;
			case 2:
				result = ONE_WAY_TICKET;
				break;
			case 3:
				result = RETURN_FROM_ANOTHER_DESTINATION;
				break;
			default:
				result = NO_SUCH_ITEM;
				break;
		}
		
		return result;
	}
	
	public String getTypeOfTripName(Context iContext)
	{
		String result = iContext.getString(mTypeOfTripNameResourceId);
		
		if (TextUtils.isEmpty(result))
		{
			result = "";
			AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.NoSuchItems.getErrorMessage());
		}
		
		return result;
	}
	
	public int getTypeOfTripId()
	{
		return mTypeOfTripId;
	}
	
	public int getTypeForWebView()
	{
		return mTypeForWebView;
	}
	
	public static eTypesOfTrip findByName(String iName, Context iContext)
	{
		eTypesOfTrip result = eTypesOfTrip.NO_SUCH_ITEM;
		
		List<eTypesOfTrip> list = Arrays.asList(eTypesOfTrip.values());
		for (eTypesOfTrip item : list)
		{
			if (item.getTypeOfTripName(iContext).equals(iName))
			{
				result = item;
			}
		}
		
		return result;
	}
	
	@Override
	public String getElementName()
	{
		AppUtils.printLog(Log.DEBUG, TAG, LocalError.eLocalError.NoSuchTagName.getErrorMessage());
		return "error";
	}
	
	@Override
	public String getElementName(Context iContext)
	{
		return getTypeOfTripName(iContext);
	}
	
	@Override
	public int getElementId()
	{
		return getTypeOfTripId();
	}
	
	public boolean isOneWayTicket()
	{
		return this == ONE_WAY_TICKET;
	}
	
	public boolean isReturnFromAnotherDestination()
	{
		return this == RETURN_FROM_ANOTHER_DESTINATION;
	}
	
	public boolean isRoundTrip()
	{
		return this == ROUND_TRIP;
	}
	
	public boolean isNoSuchItem()
	{
		return this == NO_SUCH_ITEM;
	}
}
