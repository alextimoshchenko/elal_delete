package utils.global.googleAnalitics.googleTagManager;

import android.util.Log;

import com.google.android.gms.tagmanager.Container;

import java.util.Map;

/**
 * Created with care by Alexey.T on 27/08/2017.
 * <p>
 * TODO: Add a class header comment!
 */
class CustomTagCallback implements Container.FunctionCallTagCallback
{
	@Override
	public void execute(String tagName, Map<String, Object> parameters)
	{
		// The code for firing this custom tag.
		Log.i("CuteAnimals", "Custom function call tag :" + tagName + " is fired.");
	}
}
