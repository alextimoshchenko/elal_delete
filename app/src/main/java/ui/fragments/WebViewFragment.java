package ui.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import global.IntentExtras;
import global.UserData;
import il.co.ewave.elal.R;
import ui.activities.MainActivity;
import ui.activities.MyFlightsActivity;
import ui.customWidgets.TouchableWebView;
import utils.global.AppUtils;
import utils.permissions.IPermissionsListener;
import utils.permissions.PermissionManager;
import webServices.global.JacksonRequest;
import webServices.global.RequestStringBuilder;

@SuppressLint("SetJavaScriptEnabled")
public class WebViewFragment extends EWBaseFragment implements IPermissionsListener
{
	private final static String TAG = WebViewFragment.class.getSimpleName();
	private static final String COOKIE = "Cookie";
	private String mUrl, mTitle;
	private TouchableWebView mWebView;
	private LinearLayout mLlProgressDialog, mLlHeaderScreenName;
	private ImageView mIvBackArrow, mIvBottomBackArrow;
	private Map<String, String> mHeaders = new HashMap<>();
	private TextView mTvTitle;
	private boolean mSendPhotoEmail = false;
	
	private String mSavedTitle = "";
	private String mSavedMessage = "";
	private String[] mSavedImagesBase64 = {};
	private String mPostData;
	
	private WebViewClient mWebViewClient = new WebViewClient()
	{
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url)
		{
			Log.e("WVFragment", "shouldOverrideUrlLoading, URL = " + url);
			
			if (url.contains("facebook.com") || url.contains("booking.com") || url.contains("booking.co.il") || url.contains("rentalcars.com") || url.contains("paypal")) // remove UserAgent for facebook & booking & rentalcars, or page will appear corrupted
			{
				view.getSettings().setUserAgentString(null);
			}
			
			if (!TextUtils.isEmpty(url) && !url.startsWith("js:"))
			{
//				if (!TextUtils.isEmpty(mPostData))
//				{
////					view.postUrl(AppUtils.addHttpToStringIfNeed(mUrl), mPostData.getBytes());
//					view.loadUrl(url + "&" + mPostData, mHeaders);
//
//					Log.d(TAG, "shouldOverrideUrlLoading. url: " + url + "&" + mPostData);
//				}
//				else
//				{
//					view.loadUrl(url, mHeaders);
//
//					Log.d(TAG, "shouldOverrideUrlLoading. just url: " + url);
//				}
//
				view.loadUrl(url, mHeaders);
				
				if (url.toLowerCase().contains(".pdf"))
				{
					view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
				}
			}
			
			return true;
		}
		
		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon)
		{
			setProgressBar(true);
			
			super.onPageStarted(view, url, favicon);
		}
		
		@Override
		public void onPageFinished(WebView view, String url)
		{
			setProgressBar(false);
			
			super.onPageFinished(view, url);
		}
		
//		@Override
//		public WebResourceResponse shouldInterceptRequest(final WebView view, String url)
//		{
//			AppUtils.printLog(Log.DEBUG, TAG, "shouldInterceptRequest");
//			return super.shouldInterceptRequest(view, url);
//		}
		
		@Override
		public WebResourceResponse shouldInterceptRequest(final WebView view, final WebResourceRequest request)
		{
			AppUtils.printLog(Log.DEBUG, TAG, "shouldInterceptRequest");
			return super.shouldInterceptRequest(view, request);
		}
		
		@Override
		public void onReceivedError(final WebView view, final int errorCode, final String description, final String failingUrl)
		{
			Log.e("WVFragment", "onReceivedError");
			super.onReceivedError(view, errorCode, description, failingUrl);
		}
		
		@Override
		public void onReceivedError(final WebView view, final WebResourceRequest request, final WebResourceError error)
		{
			Log.e("WVFragment", "onReceivedError");
			super.onReceivedError(view, request, error);
		}
		
		@Override
		public void onReceivedHttpError(final WebView view, final WebResourceRequest request, final WebResourceResponse errorResponse)
		{
			Log.e("WVFragment", "onReceivedHttpError");
			super.onReceivedHttpError(view, request, errorResponse);
		}
		
//		@Override
//		public void onReceivedSslError(final WebView view, final SslErrorHandler handler, final SslError error)
//		{
//			AppUtils.printLog(Log.DEBUG, TAG, "onReceivedSslError");
//
//			handler.proceed();
//		}
		
		
		@Override
		public void onReceivedSslError(final WebView view, final SslErrorHandler handler, final SslError error)
		{
			AppUtils.printLog(Log.DEBUG, "onReceivedSslError", error.toString());
			super.onReceivedSslError(view, handler, error);
		}
	};
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.web_view_layout);
		
		//		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
		//		{
		//			@Override
		//			public void uncaughtException(Thread thread, Throwable e)
		//			{
		//				handleUncaughtException(thread, e);
		//			}
		//		});
	}
	
	public void handleUncaughtException(Thread thread, Throwable e)
	{
		Log.e(TAG, "handleUncaughtException: " + e.toString() + "\n********************************************"); // not all Android versions will print the stack trace automatically
		e.printStackTrace();
		
		//					Intent intent = new Intent(getActivity(), SplashActivity.class);
		//					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // required when starting from Application
		//					startActivity(intent);
		//
		//					System.exit(1); // kill off the crashed app
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		
		getExtras();
		initReference();
		setGui();
		setListeners();
		setWebView();
		setProgressBar(false);
	}
	
	private void getExtras()
	{
		Bundle extras = getArguments();
		
		if (extras != null)
		{
			if (extras.containsKey(IntentExtras.URL))
			{
				mUrl = extras.getString(IntentExtras.URL);
			}
			
			if (extras.containsKey(IntentExtras.SEARCH_VIEW_WEB_VIEW_TITLE))
			{
				mTitle = extras.getString(IntentExtras.SEARCH_VIEW_WEB_VIEW_TITLE);
			}
			
			if (extras.containsKey(IntentExtras.ENC_PARAMETERS))
			{
				mPostData = extras.getString(IntentExtras.ENC_PARAMETERS);
			}
		}
	}
	
	private void initReference()
	{
		Activity act = getActivity();
		
		mLlProgressDialog = (LinearLayout) act.findViewById(R.id.ll_webView_loader);
		mWebView = (TouchableWebView) act.findViewById(R.id.wv_webView);
		
		//Header
		View header = act.findViewById(R.id.fl_web_view_header);
		mLlHeaderScreenName = (LinearLayout) header.findViewById(R.id.ll_header_screen_name);
		mIvBackArrow = (ImageView) header.findViewById(R.id.iv_name_screen_back_arrow);
		
		mIvBottomBackArrow = (ImageView) act.findViewById(R.id.iv_web_view_bottom_back_arrow);
	}
	
	private void setGui()
	{
		if (TextUtils.isEmpty(mTitle))
		{
			mLlHeaderScreenName.setVisibility(View.GONE);
		}
		else
		{
			mLlHeaderScreenName.findViewById(R.id.iv_header_name_screen_left).setVisibility(View.GONE);
			mLlHeaderScreenName.findViewById(R.id.iv_header_name_screen_right).setVisibility(View.GONE);
			mLlHeaderScreenName.setGravity(Gravity.END);
			mTvTitle = (TextView) mLlHeaderScreenName.findViewById(R.id.tv_name_screen_title);
			mTvTitle.setPadding(60,0,60,0);
			mTvTitle.setText(mTitle);
			mTvTitle.setTextColor(getResources().getColor(R.color.ElalDarkBlue));
			mTvTitle.setTextSize(12);
			mTvTitle.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(final View v)
				{
					doOnBackArrowClick();
				}
			});
		}
	}
	
	private void setListeners()
	{
		mIvBackArrow.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnBackArrowClick();
			}
		});
		
		mIvBottomBackArrow.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnBackClickInsideWebView();
			}
		});
	}
	
	private void doOnBackClickInsideWebView()
	{
		if (mWebView.canGoBack())
		{
			mWebView.goBack();
		}
	}
	
	private void setWebView()
	{
		mWebView.setWebViewClient(mWebViewClient);
		
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.getSettings().setDomStorageEnabled(true);
		WebSettings webSettings = mWebView.getSettings();
		
		//		webSettings.setJavaScriptEnabled(true);
		//		webSettings.setDomStorageEnabled(true);
		webSettings.setBuiltInZoomControls(true/*false*/);
		webSettings.setDisplayZoomControls(false);
		//		webSettings.setSupportZoom(/*false*/true);
		webSettings.setUseWideViewPort(true);
		webSettings.setLoadWithOverviewMode(true);
		
		webSettings.setTextZoom(100);
		
		mWebView.setHorizontalScrollBarEnabled(false);
		mWebView.getSettings().setSupportMultipleWindows(false);
		mWebView.getSettings().setAppCacheEnabled(true);
		mWebView.getSettings().setAllowFileAccess(true);
		mWebView.getSettings().setSaveFormData(false);// disable the autocomplete in the webview
		mWebView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
		
		mWebView.setWebChromeClient(new WebChromeClient()
		{
			@Override
			public void onCloseWindow(final WebView window)
			{
				Log.d(TAG, "onCloseWindow: ");
				super.onCloseWindow(window);
			}
			
			@Override
			public boolean onJsAlert(final WebView view, final String url, final String message, final JsResult result)
			{
				Log.d(TAG, "onJsAlert: " + message);
				return super.onJsAlert(view, url, message, result);
			}
			
			@Override
			public boolean onConsoleMessage(final ConsoleMessage consoleMessage)
			{
				Log.d(TAG, "onConsoleMessage: " + consoleMessage.message() + " -- From line " + consoleMessage.lineNumber() + " of " + consoleMessage.sourceId());
				return super.onConsoleMessage(consoleMessage);
			}
			
			@Override
			public Bitmap getDefaultVideoPoster()
			{
				//avishay 22/11/2017 fix the video in web CRASH
				//				Bitmap icon = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.msg1);
				//				return icon;
				return Bitmap.createBitmap(50, 50, Bitmap.Config.ARGB_8888);
			}
			
			
		});
		mWebView.getSettings().setMediaPlaybackRequiresUserGesture(false);
		mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
		
//		mWebView.loadData(mUrl, "text/html", "UTF-8");

//		Log.d(TAG, "loading url: " + mUrl);
		
		/***
		 * do not remove this test or facebook links will corrupt
		 * ***/
//		if (mUrl.contains(RequestStringBuilder.ELAL_CO_IL) || mUrl.contains(RequestStringBuilder.ELAL_COM) || mUrl.contains(RequestStringBuilder.MATMID_BASE_COM))
		if (mUrl.contains(RequestStringBuilder.ELAL_CASE) || mUrl.contains(RequestStringBuilder.ELAL_CASE.toLowerCase()) && !urlException(mUrl))
		{
			mWebView.getSettings().setUserAgentString(JacksonRequest.USER_AGENT_CONTENT);
		}
		else
		{
			mWebView.getSettings().setUserAgentString(null);
		}
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
		{
			CookieManager.getInstance().setAcceptThirdPartyCookies(mWebView, true);
		}
		else
		{
			CookieManager.getInstance().setAcceptCookie(true);
		}
		
		CookieManager.getInstance().removeAllCookie();
		
//		String cookieToSet = RequestStringBuilder.getMatmidCookieStringForWeb();
		//		Log.d("Matmid", "setting cookie: " + cookieToSet);
		
		CookieManager.getInstance().setCookie(mUrl, RequestStringBuilder.getMatmidCookieStringForWeb());
		CookieManager.getInstance().setCookie(mUrl, "SMSESSION=" + UserData.getInstance().getSMSession());
		
		mHeaders.put(COOKIE, RequestStringBuilder.getMatmidCookieStringForWeb());
		System.out.println("HEADERS LOAD" + mHeaders);
		
		mWebView.setFocusable(true);
		
		mWebView.addJavascriptInterface(new ElAlJavaScriptHandler(getActivity()), ElAlJavaScriptHandler.ELAL_JAVA_SCRIPT_HANDLER);
		//		mWebView.setInitialScale(1);
		//		mWebView.getSettings().setSupportZoom(true);
		//		mWebView.getSettings().setBuiltInZoomControls(true);
		
		if (!TextUtils.isEmpty(mUrl))
		{
			if (!TextUtils.isEmpty(mPostData))
			{
////				mWebView.postUrl(AppUtils.addHttpToStringIfNeed(mUrl), mPostData.getBytes());
				mWebView.loadUrl(AppUtils.addHttpToStringIfNeed(mUrl + "&" + mPostData), mHeaders);
				
				Log.d(TAG, "url: " + mUrl + "&" + mPostData);
			}
			else
			{
				mWebView.loadUrl(AppUtils.addHttpToStringIfNeed(mUrl), mHeaders);
				Log.d(TAG, "just url: " + mUrl);
			}
		}
		
		String cookies = CookieManager.getInstance().getCookie(mUrl);
		System.out.println("All COOKIES " + cookies);
	}
	
	private boolean urlException(String iUrl)
	{
		return iUrl.contains("facebook.com") || iUrl.contains("booking.com") || iUrl.contains("booking.co.il") || iUrl.contains("rentalcars.com");
	}
	
	private void setProgressBar(boolean iIsShow)
	{
		if (mLlProgressDialog != null)
		{
			mLlProgressDialog.setVisibility(iIsShow ? View.VISIBLE : View.GONE);
		}
	}
	
	@Override
	public void doOnPermissionsGranted(String[] iPermissions)
	{
		if (mSendPhotoEmail)
		{
			doSendPhotoEmail();
		}
		else
		{
			doSaveImage();
		}
	}
	
	@Override
	public void doOnPermissionsDenied(String[] iPermissions)
	{
		// TODO: 13/12/2017 - what todo if denied?
		//SAGI: MAKES ERROR ON PUSH NOTIFICATION!!
//		doOnBackArrowClick();
	}
	
	private void doOnBackArrowClick()
	{
		tryNotifyPreviousFragment();
		//		tryPopFragment();
		
		//SAGI: MAKES ERROR ON PUSH NOTIFICATION!!
		if (getActivity() != null)
		{
			getActivity().onBackPressed();
		}
	}
	
	private void tryNotifyPreviousFragment()
	{
		if (getActivity() != null && getActivity() instanceof MyFlightsActivity)
		{
			((MyFlightsActivity) getActivity()).updateDataListener();
		}
	}
	
	//	@Override
	//	public void onDestroyView()
	//	{
	//		if (mWebView!= null)
	//		mWebView=null; // remove webView, prevent chromium to crash
	//		super.onDestroyView();
	//
	//	}
	
	private void doSendPhotoEmail()
	{
		ArrayList<Uri> attachmentsUris = new ArrayList<>();
		
		for (String singleBase64Image : mSavedImagesBase64)
		{
			if (!TextUtils.isEmpty(singleBase64Image))
			{
				attachmentsUris.add(AppUtils.convertBase64ImageToUri(singleBase64Image));
			}
		}
		
		startActivity(AppUtils.getEmailIntent("", mSavedTitle, mSavedMessage, attachmentsUris));
	}
	
	private void doSaveImage()
	{
		for (String singleBase64Image : mSavedImagesBase64)
		{
			if (!TextUtils.isEmpty(singleBase64Image))
			{
				AppUtils.saveImageToDevice(AppUtils.convertBase64ImageToBitmap(singleBase64Image), AppUtils.DEFAULT_ELAL_CAMERA_FOLDER);
			}
		}
		
		getActivity().runOnUiThread(new Runnable()
		{
			public void run()
			{
				Toast.makeText(getActivity(), R.string.boarding_ticket_saved_to_device, Toast.LENGTH_SHORT).show();
			}
		});
	}
	
	private void onPhotoDocumentClick()
	{
		int permissionReadImages = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
		int permissionWrite = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
		ArrayList<String> listPermissionsNeeded = new ArrayList<>();
		if (permissionReadImages != PackageManager.PERMISSION_GRANTED && permissionWrite != PackageManager.PERMISSION_GRANTED)
		{
			listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
			listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
			
			PermissionManager.requestApplicationPermissionsIfNeeded(getActivity(), listPermissionsNeeded, this);
		}
		else
		{
			try
			{
				if (mSendPhotoEmail)
				{
					doSendPhotoEmail();
				}
				else
				{
					doSaveImage();
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				Toast.makeText(getContext(), getContext().getString(R.string.error_L3_timeout), Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	
	@Override
	public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults)
	{
//		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if (grantResults.length > 0
				&& grantResults[0] == PackageManager.PERMISSION_GRANTED)
		{
			doOnPermissionsGranted(permissions);
		}
		else{
			doOnPermissionsDenied(permissions);
		}
	}
	
	private class ElAlJavaScriptHandler
	{
		static final String ELAL_JAVA_SCRIPT_HANDLER = "Native";
		
		Context mContext;
		
		ElAlJavaScriptHandler(Context activity)
		{
			mContext = activity;
		}
		
		@JavascriptInterface
		public void gotoHome()
		{
			if (getActivity() != null)
			{
				getActivity().runOnUiThread(new Runnable()
				{
					public void run()
					{
						if (getActivity() != null && getActivity() instanceof MainActivity)
						{
							((MainActivity) getActivity()).loadHomeFragment();
						}
					}
				});
			}
		}
		
		@JavascriptInterface
		public void sendEmail(final String iTitle, final String iMessage, final String[] iImagesBase64)
		{
			mSendPhotoEmail = true;
			
			mSavedTitle = iTitle;
			mSavedMessage = iMessage;
			mSavedImagesBase64 = iImagesBase64;
			
			onPhotoDocumentClick();
		}
		
		@JavascriptInterface
		public void saveImage(String[] iImagesBase64)
		{
			mSendPhotoEmail = false;
			
			mSavedImagesBase64 = iImagesBase64;
			
			onPhotoDocumentClick();
		}
	}
}
