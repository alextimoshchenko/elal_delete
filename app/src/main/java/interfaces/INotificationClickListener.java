package interfaces;

import webServices.responses.deleteUserPushMessage.NotificationInstance;

/**
 * Created with care by Alexey.T on 02/08/2017.
 */
public interface INotificationClickListener
{
	void onItemClick(NotificationInstance iNotificationInstance);
	
	void onDelete(NotificationInstance iNotificationInstance);
}
