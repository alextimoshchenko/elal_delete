package webServices.global;

import java.util.ArrayList;

/**
 * Created by Avishay.Peretz on 05/11/2017.
 */

/**
 * helper to take specific values for request hashing
 */
public enum eRequestItem
{
	REGULAR(0, null),
	MatmidLogin(1, new String[] {"Email", "Mobile", "PassportNum", "PassportExp", "LanguageCode", "MemberID", "MemberID", "LanguageCode", "UDID", "OSID", "Token", "DeviceName", "OsVersion", "AppVersion", "RepresentationID"}),
	MatmidSetFamilyMembers(2, new String[] {"FamilyMemberID", "RelationID", "FamilyFirstName", "FamilyLastName", "MemberID", "ActionTypeID", "FamilyID"}),
	MatmidUpdateMemberData(3, new String[] {"Email", "Mobile", "MobileCountryPrefix", "PassportNum", "PassportExp", "LanguageCode", "MemberID"});
	
	private final int mRequestItemId;
	private String[] mKeysArr;
	
	eRequestItem(final int iRequestItemId, final String[] iKeysArr)
	{
		mRequestItemId = iRequestItemId;
		mKeysArr = iKeysArr;
	}
	
	public int getRequestItemId()
	{
		return mRequestItemId;
	}
	
	public String[] getKeysArr()
	{
		return mKeysArr;
	}
}
