package utils.global.searchFlightObjects;

import java.util.Date;

/**
 * Created with care by Alexey.T on 13/11/2017.
 * <p>
 * TODO: Add a class header comment!
 */
public interface IUserFamily
{
	void addAge(Date upToThisDate);
	
	IUserFamily cloneObj();
	
	boolean isSelectedForSearching();
	
	void setSelectedStatusAccordingTo(IUserFamily iUserFamily);
	
	boolean equals(final Object obj);
	
	void setDOB(final Date iDOB);
}
