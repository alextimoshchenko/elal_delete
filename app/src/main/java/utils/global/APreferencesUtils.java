package utils.global;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;

import il.co.ewave.elal.BuildConfig;

/**
 * Created with care by Shahar Ben-Moshe on 15/12/16.
 */

@SuppressLint("ApplySharedPref")
public abstract class APreferencesUtils
{
	private static final String PREFS = "prefs_";
	
	protected APreferencesUtils()
	{
	}
	
	private static @NonNull
	String getPreferencesName()
	{
		return PREFS + BuildConfig.APPLICATION_ID;
	}
	
	//region string
	public static String getSharedPreferenceStringOrNull(Context iContext, String iKey)
	{
		return getSharedPreferenceStringOrDefault(iContext, iKey, null);
	}
	
	public static String getSharedPreferenceStringOrDefault(Context iContext, String iKey, String iDefault)
	{
		return iContext.getSharedPreferences(getPreferencesName(), Context.MODE_PRIVATE).getString(iKey, iDefault);
	}
	
	public static void setSharedPreference(Context iContext, String iKey, String iValue)
	{
		iContext.getSharedPreferences(getPreferencesName(), Context.MODE_PRIVATE).edit().putString(iKey, iValue).commit();
	}
	//endregion
	
	//region int
	public static int getSharedPreferenceIntOrMinusOne(Context iContext, String iKey)
	{
		return getSharedPreferenceIntOrDefault(iContext, iKey, -1);
	}
	
	public static int getSharedPreferenceIntOrDefault(Context iContext, String iKey, int iDefault)
	{
		return iContext.getSharedPreferences(getPreferencesName(), Context.MODE_PRIVATE).getInt(iKey, iDefault);
	}
	
	public static void setSharedPreference(Context iContext, String iKey, int iValue)
	{
		if (iContext != null && iKey != null)
		{
			iContext.getSharedPreferences(getPreferencesName(), Context.MODE_PRIVATE).edit().putInt(iKey, iValue).commit();
		}
	}
	//endregion
	
	//region boolean
	public static boolean getSharedPreferenceBooleanOrFalse(Context iContext, String iKey)
	{
		return getSharedPreferenceBooleanOrDefault(iContext, iKey, false);
	}
	
	private static boolean getSharedPreferenceBooleanOrDefault(Context iContext, String iKey, boolean iDefault)
	{
		return iContext.getSharedPreferences(getPreferencesName(), Context.MODE_PRIVATE).getBoolean(iKey, iDefault);
	}
	
	public static void setSharedPreference(Context iContext, String iKey, boolean iValue)
	{
		iContext.getSharedPreferences(getPreferencesName(), Context.MODE_PRIVATE).edit().putBoolean(iKey, iValue).commit();
	}
	//endregion
	
	//region float
	public static float getSharedPreferenceFloatOrMinusOne(Context iContext, String iKey)
	{
		return getSharedPreferenceFloatOrDefault(iContext, iKey, -1);
	}
	
	private static float getSharedPreferenceFloatOrDefault(Context iContext, String iKey, float iDefault)
	{
		return iContext.getSharedPreferences(getPreferencesName(), Context.MODE_PRIVATE).getFloat(iKey, iDefault);
	}
	
	public static void setSharedPreference(Context iContext, String iKey, float iValue)
	{
		iContext.getSharedPreferences(getPreferencesName(), Context.MODE_PRIVATE).edit().putFloat(iKey, iValue).commit();
	}
	//endregion
	
	//region long
	public static long getSharedPreferenceLongOrMinusOne(Context iContext, String iKey)
	{
		return getSharedPreferenceLongOrDefault(iContext, iKey, -1);
	}
	
	private static long getSharedPreferenceLongOrDefault(Context iContext, String iKey, long iDefault)
	{
		return iContext.getSharedPreferences(getPreferencesName(), Context.MODE_PRIVATE).getLong(iKey, iDefault);
	}
	
	public static void setSharedPreference(Context iContext, String iKey, long iValue)
	{
		iContext.getSharedPreferences(getPreferencesName(), Context.MODE_PRIVATE).edit().putFloat(iKey, iValue).commit();
	}
	//endregion
}
