package webServices.responses;

import interfaces.ILoginResponse;
import webServices.global.Response;
import webServices.responses.responseUserLogin.UserLogin;

public class ResponseUserLogin extends Response<UserLogin> implements ILoginResponse<UserLogin>
{
}
