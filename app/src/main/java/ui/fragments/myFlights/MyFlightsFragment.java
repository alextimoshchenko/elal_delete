package ui.fragments.myFlights;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.tagmanager.DataLayer;

import java.util.ArrayList;

import global.IntentExtras;
import global.UserData;
import il.co.ewave.elal.R;
import interfaces.IFlightsSwipeDeckListener;
import ui.activities.EWBaseActivity;
import ui.activities.MyFlightsActivity;
import ui.adapters.MyFlightsSwipeDeckAdapter;
import ui.customWidgets.swipeDeck.SwipeDeck;
import ui.fragments.AddNewFlightFragment;
import ui.fragments.EWBaseFragment;
import utils.errors.ErrorsHandler;
import utils.errors.LocalError;
import utils.global.AppUtils;
import utils.global.googleAnalitics.googleTagManager.TagManagerImplementation.ePushEvents;
import utils.global.myFlightsObjects.MyFlightsObject;
import webServices.responses.ResponseGetPnrByNum;
import webServices.responses.getMultiplePNRDestinations.GetMultiplePNRDestinations;
import webServices.responses.getMultiplePNRDestinations.ResponseGetMultiplePNRDestinations;
import webServices.responses.responseGetUserActivePNRs.UserActivePnr;

/**
 * Created with care by Alexey.T on 13/08/2017.
 */
public class MyFlightsFragment extends EWBaseFragment implements Response.ErrorListener
{
	private final String TAG = MyFlightsFragment.class.getSimpleName();
	private SwipeDeck mSdFlightsDeck;
	private TextView mTvTitle, mTvMyFlightsAddFlight;
	private ImageView mIvBackArrow;
	private LinearLayout mLlBookHotel, mLlRentCar;
	private MyFlightsSwipeDeckAdapter mMyFlightsSwipeDeckAdapter;
	private boolean mIsFromInAppNavigation = false;
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_flights_fragment_layout);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		getIntentExtras();
		initReference();
		setFlightDeckAdapter();
		setListeners();
		setGui();
	
		pushScreenOpenEvent("My Flights");
	}
	
	private void getIntentExtras()
	{
		if (getArguments() != null)
		{
			mIsFromInAppNavigation = getArguments().getBoolean(IntentExtras.IS_IN_APP_NAVIGATION, false);
		}
	}
	
	/**
	 * calling getUserActivePNRs from on connected causes an infinate loop in case of a user with 1 flight
	 * when clicking a link from ToDoFragment
	**/

//	@Override
//	public void onServiceConnected()
//	{
//		getUserActivePNRs();
//	}

	private void setGui()
	{
		mTvTitle.setText(R.string.my_flights);
	}
	
	private void initReference()
	{
		Activity act = getActivity();
		
		//Card Swiper
		mSdFlightsDeck = (SwipeDeck) act.findViewById(R.id.sd_my_flights);
		
		//Header
		View header = act.findViewById(R.id.ll_frag_search_destination_header);
		mTvTitle = (TextView) header.findViewById(R.id.tv_name_screen_title);
		mIvBackArrow = (ImageView) header.findViewById(R.id.iv_name_screen_back_arrow);
		
		//add flight
		mTvMyFlightsAddFlight = (TextView) act.findViewById(R.id.tv_my_flights_add_flight);
		
		//Book hotel
		mLlBookHotel = (LinearLayout) act.findViewById(R.id.ll_my_flights_book_hotel);
		
		//Rent car
		mLlRentCar = (LinearLayout) act.findViewById(R.id.ll_my_flights_rent_car);
	}
	
	private void setListeners()
	{
		mLlRentCar.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnRentCarClick();
			}
		});
		
		mLlBookHotel.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnBookHotelClick();
			}
		});
		
		mIvBackArrow.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnBackClick();
			}
		});
		
		mTvMyFlightsAddFlight.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnAddFlightClick();
			}
		});
	}
	
	private void doOnRentCarClick()
	{
		MyFlightsObject myFlightsObject = MyFlightsObject.getInstance();
		tryGoToWebFragment(myFlightsObject.getCurrentPnrFlight().getVehicleLink());
		//event 32
		((EWBaseActivity) getActivity()).pushCustomEvent(getContext().getString(R.string.auto_event), DataLayer.mapOf("Category", "Manage flights & Check In", "Action", "Car booking", "Label", MyFlightsObject.getInstance()
		                                                                                                                                                                                                        .getCurrentPnrFlight()
		                                                                                                                                                                                                        .getVehicleLink()));
	}
	
	private void doOnBookHotelClick()
	{
		MyFlightsObject myFlightsObject = MyFlightsObject.getInstance();
		tryGoToWebFragment(myFlightsObject.getCurrentPnrFlight().getHotelLink());
		
		//event 31
		((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage flights & Check In", "Action", "Hotel booking", "Label", MyFlightsObject.getInstance()
		                                                                                                                                                            .getCurrentPnrFlight()
		                                                                                                                                                            .getHotelLink()));
	}
	
	private void doOnAddFlightClick()
	{
		tryGoToFragmentByFragmentClass(AddNewFlightFragment.class, Bundle.EMPTY, true);
		
		//event 30
		((EWBaseActivity) getActivity()).pushEvent(ePushEvents.ADD_FLIGHT);
	}
	
	private void doOnDeckItemClick()
	{
		MyFlightsObject myFlightsObject = MyFlightsObject.getInstance();
		UserActivePnr currentUserActivePnr = myFlightsObject.getCurrentUserActivePnr();
		boolean isItMeanSomething = UserData.getInstance().getGroupPnrs().contains(currentUserActivePnr) && currentUserActivePnr.isGroupPnr();

		if (isItMeanSomething)
		{
			loadMyFlightAddFragment();
		}
		else
		{
			getPnrByNum();
		}
		
		//event 29
		((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage flights & Check In", "Action", "Manage flight", "Label", UserData.getInstance().getActivePnr()));
	}
	
	private void loadMyFlightAddFragment()
	{
		((MyFlightsActivity) getActivity()).loadMyFlightsAddFragment(true);
	}
	
	private void getPnrByNum()
	{
		if (isServiceConnected() && (isResumed() || isVisible()))
		{
			UserActivePnr userActivePnr = MyFlightsObject.getInstance().getCurrentUserActivePnr();
			((MyFlightsActivity) getActivity()).GetPNRByNum(new Response.Listener<ResponseGetPnrByNum>()
			{
				@Override
				public void onResponse(final ResponseGetPnrByNum response)
				{
					handleResponseGetPnrByNum(response);
				}
			}, this, userActivePnr.getPnr(), userActivePnr.getIsMultiple() ? "" : userActivePnr.getCurrentFlightNum());
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.GeneralError.getErrorMessage());
		}
	}
	
	private void handleResponseGetPnrByNum(ResponseGetPnrByNum response)
	{
		if (response != null && response.getContent() != null)
		{
			UserActivePnr activePnr = response.getContent();
			MyFlightsObject myFlightsObject = MyFlightsObject.getInstance();
			myFlightsObject.setCurrentUserActivePnr(activePnr);
			
			getMultiplePNRDestinations();
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.GeneralError.getErrorMessage());
		}
	}
	
	private void getMultiplePNRDestinations()
	{
		if (isServiceConnected() && (isResumed() || isVisible()))
		{
			((MyFlightsActivity) getActivity()).GetMultiplePNRDestinations(new Response.Listener<ResponseGetMultiplePNRDestinations>()
			{
				@Override
				public void onResponse(final ResponseGetMultiplePNRDestinations response)
				{
					handleResponseGetMultiplePNRDestinations(response);
				}
			}, this);
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.GeneralError.getErrorMessage());
		}
	}
	
	private void handleResponseGetMultiplePNRDestinations(final ResponseGetMultiplePNRDestinations response)
	{
		if (response != null && response.getContent() != null)
		{
			GetMultiplePNRDestinations pnrDestinations = response.getContent();
			MyFlightsObject myFlightsObject = MyFlightsObject.getInstance();
			myFlightsObject.setCurrentPnrDestinations(pnrDestinations);
			tryGoToFragmentByFragmentClass(MyFlightFragmentNew2.class, Bundle.EMPTY, true);
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.GeneralError.getErrorMessage());
		}
	}
	
//	private void getUserActivePNRs()
//	{
//		MyFlightsObject flightsObject = MyFlightsObject.getInstance();
//
//		UserActivePnr currentUserActivePnr = flightsObject.getCurrentUserActivePnr();
//
//		if (flightsObject.isUserActivePnrExist(currentUserActivePnr))
//		{
//			setFlightDeckAdapter();
//		}
//		else
//		{
//			((MyFlightsActivity) getActivity()).GetUserActivePNRs();
//		}
//	}
	
	private void setFlightDeckAdapter()
	{
		if (MyFlightsObject.getInstance().getCurrentUserActivePnr() != null)
		{
			UserActivePnr currentUserActivePnr = MyFlightsObject.getInstance().getCurrentUserActivePnr();
			if (!MyFlightsObject.getInstance().isUserActivePnrExist(currentUserActivePnr))
			{
				((MyFlightsActivity)getActivity()).GetUserActivePNRs();
				//				MyFlightsObject.getInstance().getUserActivePnrList().add(currentUserActivePnr);
				//				MyFlightsObject.getInstance().sortByDatePnrList();
			}
		}
		
		ArrayList<UserActivePnr> userActivePnrList = MyFlightsObject.getInstance().getUserActivePnrList();
		
		if (userActivePnrList.size() == 1 && !mIsFromInAppNavigation)
		{
			tryGoToFragmentByFragmentClass(MyFlightFragmentNew2.class, userActivePnrList.get(0) != null ?
			                                                           MyFlightFragmentNew2.createBundleForFragment(userActivePnrList.get(0), null) :
			                                                           Bundle.EMPTY, false);
			return;
		}
		
		if (mMyFlightsSwipeDeckAdapter == null)
		{
			mMyFlightsSwipeDeckAdapter = new MyFlightsSwipeDeckAdapter(userActivePnrList, new IFlightsSwipeDeckListener()
			{
				@Override
				public void cardClick()
				{
					doOnDeckItemClick();
				}
			});
			
			mSdFlightsDeck.setAdapter(mMyFlightsSwipeDeckAdapter);
			mSdFlightsDeck.setCallback(new SwipeDeck.SwipeDeckCallback()
			{
				@Override
				public void cardSwipedLeft(final long stableId)
				{
					doOnCardSwipeLeft(stableId);
				}
				
				@Override
				public void cardSwipedRight(final long stableId)
				{
					doOnCardSwipedRight(stableId);
				}
				
				@Override
				public void cardClick(final View card)
				{
					doOnDeckItemClick();
				}
			});
		}
		else
		{
			mMyFlightsSwipeDeckAdapter.updateDataAndRefresh(userActivePnrList);
		}
	}
	
	private void doOnCardSwipedRight(final long iStableId)
	{
		MyFlightsObject myFlightsObject = MyFlightsObject.getInstance();
		myFlightsObject.setCurrentUserActivePnrByIndex((int) iStableId + 1);
	}
	
	private void doOnCardSwipeLeft(final long iStableId)
	{
		MyFlightsObject myFlightsObject = MyFlightsObject.getInstance();
		myFlightsObject.setCurrentUserActivePnrByIndex((int) iStableId + 1);
	}
	
	private void showNoPnrsDialog()
	{
		AppUtils.createSimpleMessageDialog(getActivity(), getString(R.string.no_pnrs), getString(R.string.close), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, final int which)
			{
				dialog.dismiss();
				tryPopFragment();
			}
		}, null, null).show();
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		
		MyFlightsObject myFlightsObject = MyFlightsObject.getInstance();
		
		if (myFlightsObject.getUserActivePnrList().isEmpty())
		{
			// TODO: 30/10/2017
//			showNoPnrsDialog();
		}
		else
		{
			setFlightDeckAdapter();
		}
		
	
	}
	
	@Override
	public void onErrorResponse(final VolleyError iError)
	{
		ErrorsHandler.tryShowServiceErrorDialog(iError, getActivity());
	}
	
	private void doOnBackClick()
	{
		getActivity().onBackPressed();
	}
}
