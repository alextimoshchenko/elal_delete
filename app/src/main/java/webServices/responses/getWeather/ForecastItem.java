package webServices.responses.getWeather;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class ForecastItem implements Parcelable
{
	@JsonProperty("MinTempC")
	private int mMinTempC;
	@JsonProperty("MaxTempC")
	private int mMaxTempC;
	@JsonProperty("Description")
	private String mDescription;
	@JsonProperty("MaxTempF")
	private int mMaxTempF;
	@JsonProperty("Image")
	private String mImage;
	@JsonProperty("Day")
	private String mDay;
	@JsonProperty("MinTempF")
	private int mMinTempF;
	@JsonProperty("Date")
	private Date mDate;
	
	public ForecastItem()
	{
	}
	
	public Integer getMinTempC()
	{
		return mMinTempC;
	}
	
	public Integer getMaxTempC()
	{
		return mMaxTempC;
	}
	
	public String getDescription()
	{
		return mDescription;
	}
	
	public Integer getMaxTempF()
	{
		return mMaxTempF;
	}
	
	public String getImage()
	{
		return mImage;
	}
	
	public String getDay()
	{
		return mDay;
	}
	
	public Integer getMinTempF()
	{
		return mMinTempF;
	}
	
	public Date getDate()
	{
		return mDate;
	}
	
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeInt(this.mMinTempC);
		dest.writeInt(this.mMaxTempC);
		dest.writeString(this.mDescription);
		dest.writeInt(this.mMaxTempF);
		dest.writeString(this.mImage);
		dest.writeString(this.mDay);
		dest.writeInt(this.mMinTempF);
		dest.writeLong(this.mDate != null ? this.mDate.getTime() : -1);
	}
	
	protected ForecastItem(Parcel in)
	{
		this.mMinTempC = in.readInt();
		this.mMaxTempC = in.readInt();
		this.mDescription = in.readString();
		this.mMaxTempF = in.readInt();
		this.mImage = in.readString();
		this.mDay = in.readString();
		this.mMinTempF = in.readInt();
		long tmpMDate = in.readLong();
		this.mDate = tmpMDate == -1 ? null : new Date(tmpMDate);
	}
	
	public static final Parcelable.Creator<ForecastItem> CREATOR = new Parcelable.Creator<ForecastItem>()
	{
		@Override
		public ForecastItem createFromParcel(Parcel source)
		{
			return new ForecastItem(source);
		}
		
		@Override
		public ForecastItem[] newArray(int size)
		{
			return new ForecastItem[size];
		}
	};
}