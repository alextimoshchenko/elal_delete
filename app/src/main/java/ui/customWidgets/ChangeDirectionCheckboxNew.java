package ui.customWidgets;

import android.content.Context;
import android.content.SyncStatusObserver;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import il.co.ewave.elal.R;
import utils.global.AppUtils;

/**
 * Created by erline.katz on 07/08/2017.
 */

public class ChangeDirectionCheckboxNew extends ChangeDirectionLinearLayout
{
	LinearLayout mCheckBoxRoot;
	CheckBox mCheckBox;
	TextView mCbTitle;
	
	public ChangeDirectionCheckboxNew(Context context)
	{
		super(context);
		initView();
	}
	
	public ChangeDirectionCheckboxNew(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView();
	}
	
	public ChangeDirectionCheckboxNew(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initView();
	}
	
	private void initView()
	{
		inflateLayout();
		mCheckBoxRoot = (LinearLayout) findViewById(R.id.ll_checkBox_Root);
		mCheckBox = (CheckBox) findViewById(R.id.cb_checkbox);
		mCbTitle = (TextView) findViewById(R.id.tv_checkBox_Title);
		
		if (AppUtils.isDefaultLocaleRTL())
		{
			mCheckBoxRoot.setGravity(Gravity.END);
		}
		else
		{
			mCheckBoxRoot.setGravity(Gravity.START);
		}
		
		setOnClickToCheck();
	}
	
	private void setOnClickToCheck()
	{
		if (mCheckBoxRoot != null)
		{
			mCheckBoxRoot.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					mCheckBox.setChecked(!mCheckBox.isChecked());
				}
			});
		}
	}
	
	public void inflateLayout()
	{
		LayoutInflater.from(getContext()).inflate(R.layout.change_directions_checkbox, this, true);
	}
	
	public void setText(int iTextId)
	{
		if(mCbTitle != null)
		{
			mCbTitle.setText(getResources().getString(iTextId));
		}
	}
	
	public void setText(String iTextStr)
	{
		if(mCbTitle != null)
		{
//			System.out.println("ChangeDirectionCheckboxNew: setting text to CB: " + iTextStr + " end");
			mCbTitle.setText(iTextStr);
		}
	}
	
	public void setBackground(int iDrawableId)
	{
		if(mCheckBoxRoot != null)
		{
			mCheckBoxRoot.setBackground(getResources().getDrawable(iDrawableId));
		}
	}
	
	public void setBackgroundColor(int iColorId)
	{
		if(mCheckBoxRoot != null)
		{
			mCheckBoxRoot.setBackgroundColor(iColorId);
		}
	}
	
	public void setTextColor(int iColorId)
	{
		if(mCbTitle != null)
		{
			mCbTitle.setTextColor(iColorId);
		}
	}
	
	public void setChecked(boolean checked)
	{
		if(mCheckBox != null)
		{
			mCheckBox.setChecked(checked);
		}
	}
	
	public void setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener listener)
	{
		if(mCheckBox != null)
		{
			mCheckBox.setOnCheckedChangeListener(listener);
		}
	}
	
	public void setPaintFlags(int flag)
	{
		if(mCbTitle != null)
		{
			mCbTitle.setPaintFlags(flag);
		}
	}
	
	public int getPaintFlags()
	{
		if(mCbTitle != null)
		{
			return mCbTitle.getPaintFlags();
		}
		return 0;
	}
	
	
	
	
}
