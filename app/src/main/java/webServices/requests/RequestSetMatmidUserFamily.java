package webServices.requests;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

import webServices.global.eActionType;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
public class RequestSetMatmidUserFamily
{
	@JsonProperty("SessionID")
	private String mSessionID;
	
	@JsonProperty("FamilyMemberID")
	private String mFamilyMemberID;
	
	@JsonProperty("RelationID")
	private String mRelationID;
	
	@JsonProperty("FamilyFirstName")
	private String mFamilyFirstName;
	
	@JsonProperty("FamilyLastName")
	private String mFamilyLastName;
	
	@JsonProperty("ActionTypeID")
	private int mActionType;
	
	@JsonProperty("MemberID")
	private String mMemberID;
	
	@JsonProperty("FamilyID")
	private int mFamilyID;
	
	public RequestSetMatmidUserFamily()
	{
	}
	
	
	/**
	 * avishay 18/07/17
	 * deprecated constructor.
	 * Alexey - remove it at finish
	 **/
	public RequestSetMatmidUserFamily(final String iSessionID, final String iFamilyMemberID, final String iRelationID, final String iFamilyFirstName, final String iFamilyLastName,
	                                  final eActionType iActionType)
	{
		mSessionID = iSessionID;
		mFamilyMemberID = iFamilyMemberID;
		mRelationID = iRelationID;
		mFamilyFirstName = iFamilyFirstName;
		mFamilyLastName = iFamilyLastName;
		mActionType = iActionType.action;
	}
	
	/**
	 * avishay 18/07/17
	 * new constructor.
	 **/
	public RequestSetMatmidUserFamily(final String iSessionID, final String iFamilyMemberID, final String iRelationID, final String iFamilyFirstName, final String iFamilyLastName,
	                                  final eActionType iActionType, final String iMemberID, final int iFamilyID)
	{
		mSessionID = iSessionID;
		mFamilyMemberID = iFamilyMemberID;
		mRelationID = iRelationID;
		mFamilyFirstName = iFamilyFirstName;
		mFamilyLastName = iFamilyLastName;
		mActionType = iActionType.action;
		mMemberID = iMemberID;
		mFamilyID = iFamilyID;
	}
	
	
	public String getSessionID()
	{
		return mSessionID;
	}
	
	public String getFamilyMemberID()
	{
		return mFamilyMemberID;
	}
	
	public String getRelationID()
	{
		return mRelationID;
	}
	
	public String getFamilyFirstName()
	{
		return mFamilyFirstName;
	}
	
	public String getFamilyLastName()
	{
		return mFamilyLastName;
	}
	
	public int getActionType()
	{
		return mActionType;
	}
	
	public String getMemberID()
	{
		return mMemberID;
	}
	
	public int getFamilyID()
	{
		return mFamilyID;
	}
}
