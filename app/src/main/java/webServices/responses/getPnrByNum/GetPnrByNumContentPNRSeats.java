package webServices.responses.getPnrByNum;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetPnrByNumContentPNRSeats
{
	@JsonProperty("CategoryId")
	private int CategoryId;
	
	@JsonProperty("PNR")
	private String PNR;
	
	@JsonProperty("TicketNumber")
	private String TicketNumber;
	
	@JsonProperty("SubText")
	private String SubText;
	
	@JsonProperty("HasEligibleSeats")
	private boolean HasEligibleSeats;
	
	@JsonProperty("HasSeats")
	private boolean HasSeats;
	
	@JsonProperty("Icon")
	private String Icon;
	
	@JsonProperty("URL")
	private String URL;
	
	public int getCategoryId()
	{
		return this.CategoryId;
	}
	
	public void setCategoryId(int CategoryId)
	{
		this.CategoryId = CategoryId;
	}
	
	public String getPNR()
	{
		return this.PNR;
	}
	
	public void setPNR(String PNR)
	{
		this.PNR = PNR;
	}
	
	public String getTicketNumber()
	{
		return this.TicketNumber;
	}
	
	public void setTicketNumber(String TicketNumber)
	{
		this.TicketNumber = TicketNumber;
	}
	
	public String getSubText()
	{
		return this.SubText;
	}
	
	public void setSubText(String SubText)
	{
		this.SubText = SubText;
	}
	
	public boolean getHasEligibleSeats()
	{
		return this.HasEligibleSeats;
	}
	
	public void setHasEligibleSeats(boolean HasEligibleSeats)
	{
		this.HasEligibleSeats = HasEligibleSeats;
	}
	
	public boolean getHasSeats()
	{
		return this.HasSeats;
	}
	
	public void setHasSeats(boolean HasSeats)
	{
		this.HasSeats = HasSeats;
	}
	
	public String getIcon()
	{
		return this.Icon;
	}
	
	public void setIcon(String Icon)
	{
		this.Icon = Icon;
	}
	
	public String getURL()
	{
		return this.URL;
	}
	
	public void setURL(String URL)
	{
		this.URL = URL;
	}
}
