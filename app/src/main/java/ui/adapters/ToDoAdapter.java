package ui.adapters;

import android.graphics.Paint;
import android.os.Build;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chauthai.swipereveallayout.ViewBinderHelper;

import java.util.ArrayList;

import global.ElAlApplication;
import il.co.ewave.elal.R;
import interfaces.ICheckListListener;
import ui.customWidgets.ChangeDirectionCheckboxNew;
import utils.global.AppUtils;
import webServices.responses.UserCheckList.CheckListItem;
import webServices.responses.UserCheckList.CheckListParent;

/**
 * Created with care by Shahar Ben-Moshe on 22/02/17.
 */
public class ToDoAdapter extends ExpandableRecyclerAdapter<CheckListParent, CheckListItem, ToDoAdapter.ParentViewHolder, ToDoAdapter.ChildViewHolder>
{
	private static final int HEADER_LAYOUT_RESOURCE_ID = R.layout.check_list_header_layout_cell;
	private static final int CHILD_LAYOUT_RESOURCE_ID = R.layout.check_list_child_item_layout_cell;
	private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();
	private ICheckListListener mCheckListListener;
//	private ArrayList<CheckListParent> mParentList = new ArrayList<>();
	
	public ToDoAdapter(@NonNull final ArrayList<CheckListParent> parentList, final ICheckListListener iCheckListListener)
	{
		super(parentList);
//		mParentList = parentList;
		mCheckListListener = iCheckListListener;
	}
	
	@NonNull
	@Override
	public ParentViewHolder onCreateParentViewHolder(@NonNull final ViewGroup parentViewGroup, final int viewType)
	{
		return new ParentViewHolder(LayoutInflater.from(parentViewGroup.getContext()).inflate(HEADER_LAYOUT_RESOURCE_ID, parentViewGroup, false));
	}
	
	@NonNull
	@Override
	public ChildViewHolder onCreateChildViewHolder(@NonNull final ViewGroup childViewGroup, final int viewType)
	{
		return new ChildViewHolder(LayoutInflater.from(childViewGroup.getContext()).inflate(CHILD_LAYOUT_RESOURCE_ID, childViewGroup, false));
	}
	
	@Override
	public void onBindParentViewHolder(@NonNull final ParentViewHolder iParentViewHolder, final int parentPosition, @NonNull final CheckListParent iCheckListParent)
	{
		iParentViewHolder.tvTitle.setText(iCheckListParent.getParentNameResourceId());
		iParentViewHolder.tvTitle.setPadding(20, 20, 20, 10);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		params.setMargins(20, 30, 20, 10);
		iParentViewHolder.tvTitle.setLayoutParams(params);
		
		iParentViewHolder.lilParentLayout.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				// TODO: 17/09/2017 - nothing 	
			}
		});
		iParentViewHolder.setExpanded(true);
		
		if (iCheckListParent.getParentNameResourceId() == R.string.have_a_great_flight)
		{
			iParentViewHolder.tvTitle.setTextSize(20);
			iParentViewHolder.tvTitle.setGravity(Gravity.CENTER);
		}
		else
		{
			iParentViewHolder.tvTitle.setTextSize(22);
			iParentViewHolder.tvTitle.setGravity(Gravity.START);
		}
	}
	
	@Override
	public void onBindChildViewHolder(@NonNull final ChildViewHolder iChildViewHolder, final int iParentPosition, final int iChildPosition, @NonNull final CheckListItem iCheckListItem)
	{
		viewBinderHelper.bind(iChildViewHolder.mSwipeRevealLayout, iCheckListItem.getItemID() + "");
		
		boolean isToDoNow = getParentList().get(iParentPosition).getParentNameResourceId() == R.string.to_do_now;
		iChildViewHolder.mCbItem.setOnCheckedChangeListener(null);
		
		iChildViewHolder.mSwipeRevealLayout.setDragEdge(AppUtils.isDefaultLocaleRTL() ? SwipeRevealLayout.DRAG_EDGE_LEFT : SwipeRevealLayout.DRAG_EDGE_RIGHT);
		
		if (!iCheckListItem.isUserCustom())
		{
			iChildViewHolder.mSwipeRevealLayout.setLockDrag(true);
			iChildViewHolder.mImgMoreOptions.setVisibility(View.GONE);
		}
		else
		{
			iChildViewHolder.mImgMoreOptions.setVisibility(View.VISIBLE);
			
			iChildViewHolder.mImgMoreOptions.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					mCheckListListener.onOptionClicked();
					
					if (iChildViewHolder.mSwipeRevealLayout.isClosed())
					{
						iChildViewHolder.mSwipeRevealLayout.open(true);
					}
					else
					{
						iChildViewHolder.mSwipeRevealLayout.close(true);
					}
				}
			});
			
		}
		
		if (AppUtils.isDefaultLocaleRTL())
		{
			iChildViewHolder.mCbItem.setGravity(Gravity.RIGHT);
		}
		else
		{
			iChildViewHolder.mCbItem.setGravity(Gravity.LEFT);
		}
		
		iChildViewHolder.mLlRoot.setBackgroundColor(ElAlApplication.getInstance().getResources().getColor(isToDoNow ? R.color.ElalDarkBlue : R.color.ElalTooltipGrey));
		iChildViewHolder.mCbItem.setTextColor(ElAlApplication.getInstance().getResources().getColor(isToDoNow ? R.color.white : R.color.ElalDarkBlue));
		iChildViewHolder.mTvLink.setTextColor(ElAlApplication.getInstance().getResources().getColor(isToDoNow ? R.color.white : R.color.ElalDarkBlue));
		
//		iChildViewHolder.mLlActions.setVisibility(iCheckListItem.isUserCustom() ? View.VISIBLE : View.GONE);
		iChildViewHolder.mTvLink.setVisibility(TextUtils.isEmpty(iCheckListItem.getUrl()) ? View.INVISIBLE : View.VISIBLE);
		iChildViewHolder.mTvLink.setText(AppUtils.nullSafeCheckString(iCheckListItem.getLinkTitle()));
		iChildViewHolder.mTvLink.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
		
		iChildViewHolder.mCbItem.setText(AppUtils.nullSafeCheckString(iCheckListItem.getItemTitle()));
		if (iCheckListItem.isDone())
		{
			iChildViewHolder.mCbItem.setChecked(true);
			iChildViewHolder.mCbItem.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
		}
		else
		{
			iChildViewHolder.mCbItem.setChecked(false);
			if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP)
			{
				iChildViewHolder.mCbItem.setPaintFlags(iChildViewHolder.mCbItem.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
			}
			else
			{
				iChildViewHolder.mCbItem.setPaintFlags(0);
			}
		}
		
	
		iChildViewHolder.mTvLink.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				if (mCheckListListener != null)
				{
					mCheckListListener.onLink(iCheckListItem);
				}
			}
		});
		
		iChildViewHolder.mCbItem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked)
			{
				iCheckListItem.setDone(isChecked);
				if (isChecked)
				{
					iChildViewHolder.mCbItem.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
				}
				else
				{
					iChildViewHolder.mCbItem.setPaintFlags(iChildViewHolder.mCbItem.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
				}
				
				
				if (mCheckListListener != null)
				{
					mCheckListListener.onCheckChanged(iCheckListItem);
				}
			}
		});
		
		iChildViewHolder.mIbDelete.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (mCheckListListener != null)
				{
					mCheckListListener.onDelete(iCheckListItem, iChildViewHolder.getParentAdapterPosition(), iChildViewHolder.getChildAdapterPosition());
				}
			}
		});
		
		iChildViewHolder.mIbEdit.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (mCheckListListener != null)
				{
					mCheckListListener.onEdit(iCheckListItem, iChildViewHolder.getParentAdapterPosition(), iChildViewHolder.getChildAdapterPosition());
				}
			}
		});
		
	}
	
	
	class ParentViewHolder extends com.bignerdranch.expandablerecyclerview.ParentViewHolder
	{
		private TextView tvTitle;
		private LinearLayout lilParentLayout;
		
		ParentViewHolder(@NonNull final View itemView)
		{
			super(itemView);
			
			tvTitle = (TextView) itemView.findViewById(R.id.tv_checkListHeader_Title);
			lilParentLayout = (LinearLayout) itemView.findViewById(R.id.lil_parent_layout);
		}
	}
	
	class ChildViewHolder extends com.bignerdranch.expandablerecyclerview.ChildViewHolder
	{
		private SwipeRevealLayout mSwipeRevealLayout;
		private FrameLayout mLlRoot;
		private ChangeDirectionCheckboxNew mCbItem;
		private TextView mTvLink;
		private FrameLayout mLlActions;
		private ImageButton mIbEdit;
		private ImageButton mIbDelete;
		private ImageView mImgMoreOptions;
		
		ChildViewHolder(@NonNull final View itemView)
		{
			super(itemView);
			
			mSwipeRevealLayout = (SwipeRevealLayout) itemView.findViewById(R.id.srl_Root_swipable);
			mLlRoot = (FrameLayout) itemView.findViewById(R.id.ll_checkListChild_Root);
			mCbItem = (ChangeDirectionCheckboxNew) itemView.findViewById(R.id.cb_checkListChild_Item);
			
			mImgMoreOptions = (ImageView) itemView.findViewById(R.id.img_moreOptions);
			mTvLink = (TextView) itemView.findViewById(R.id.tv_checkListChild_Link);
			mLlActions = (FrameLayout) itemView.findViewById(R.id.ll_checkListChild_Actions);
			mIbEdit = (ImageButton) itemView.findViewById(R.id.ib_checkListChild_Edit);
			mIbDelete = (ImageButton) itemView.findViewById(R.id.ib_checkListChild_Delete);
		}
	}
	
}
