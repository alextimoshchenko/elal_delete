package webServices.responses.UserCheckList;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

import utils.global.ParseUtils;

/**
 * Created with care by Shahar Ben-Moshe on 21/02/17.
 */

public class CheckListItem implements Parcelable
{
	@JsonIgnore
	public static final int TIME_DIMENSION_UNTIL_7_DAYS_BEFORE_FLIGHT = 1;
	@JsonIgnore
	public static final int TIME_DIMENSION_FROM_7_DAYS_UNTIL_24_HOURS_BEFORE_FLIGHT = 2;
	@JsonIgnore
	public static final int TIME_DIMENSION_24_HOURS_BEFORE_FLIGHT = 3;
	
	@JsonProperty("ItemID")
	private int mItemID;
	@JsonProperty("ItemTitle")
	private String mItemTitle;
	@JsonProperty("InApp")
	private boolean mInApp;
	@JsonProperty("Url")
	private String mUrl;
	@JsonProperty("IsNew")
	private boolean mIsNew;
	@JsonProperty("CreatedDate")
	private Date mCreatedDate;
	@JsonProperty("IsMatmid")
	private boolean mIsMatmid;
	@JsonProperty("TimeDimentionID")
	private int mTimeDimensionID;
	@JsonProperty("IsDone")
	private boolean mIsDone;
	@JsonProperty("IsUserCustom")
	private boolean mIsUserCustom;
	@JsonProperty("LinkTitle")
	private String mLinkTitle;
	
	public CheckListItem()
	{
	}
	
	public int getItemID()
	{
		return mItemID;
	}
	
	public String getItemTitle()
	{
		return mItemTitle;
	}
	
	public boolean isInApp()
	{
		return mInApp;
	}
	
	public String getUrl()
	{
		return mUrl;
	}
	
	public int getUrlInt()
	{
		return ParseUtils.tryParseStringToIntegerOrDefault(mUrl, 0);
	}
	
	public boolean isNew()
	{
		return mIsNew;
	}
	
	public Date getCreatedDate()
	{
		return mCreatedDate;
	}
	
	public boolean isMatmid()
	{
		return mIsMatmid;
	}
	
	public int getTimeDimensionID()
	{
		return mTimeDimensionID;
	}
	
	public boolean isDone()
	{
		return mIsDone;
	}
	
	public boolean isUserCustom()
	{
		return mIsUserCustom;
	}
	
	public void setDone(final boolean iIDone)
	{
		mIsDone = iIDone;
	}
	
	public String getLinkTitle()
	{
		return mLinkTitle;
	}
	
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeInt(this.mItemID);
		dest.writeString(this.mItemTitle);
		dest.writeByte(this.mInApp ? (byte) 1 : (byte) 0);
		dest.writeString(this.mUrl);
		dest.writeByte(this.mIsNew ? (byte) 1 : (byte) 0);
		dest.writeLong(this.mCreatedDate != null ? this.mCreatedDate.getTime() : -1);
		dest.writeByte(this.mIsMatmid ? (byte) 1 : (byte) 0);
		dest.writeInt(this.mTimeDimensionID);
		dest.writeByte(this.mIsDone ? (byte) 1 : (byte) 0);
		dest.writeByte(this.mIsUserCustom ? (byte) 1 : (byte) 0);
		dest.writeString(this.mLinkTitle);
	}
	
	protected CheckListItem(Parcel in)
	{
		this.mItemID = in.readInt();
		this.mItemTitle = in.readString();
		this.mInApp = in.readByte() != 0;
		this.mUrl = in.readString();
		this.mIsNew = in.readByte() != 0;
		long tmpMCreatedDate = in.readLong();
		this.mCreatedDate = tmpMCreatedDate == -1 ? null : new Date(tmpMCreatedDate);
		this.mIsMatmid = in.readByte() != 0;
		this.mTimeDimensionID = in.readInt();
		this.mIsDone = in.readByte() != 0;
		this.mIsUserCustom = in.readByte() != 0;
		this.mLinkTitle = in.readString();
	}
	
	public static final Creator<CheckListItem> CREATOR = new Creator<CheckListItem>()
	{
		@Override
		public CheckListItem createFromParcel(Parcel source)
		{
			return new CheckListItem(source);
		}
		
		@Override
		public CheckListItem[] newArray(int size)
		{
			return new CheckListItem[size];
		}
	};
}
