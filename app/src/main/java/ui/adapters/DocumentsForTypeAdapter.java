package ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import il.co.ewave.elal.R;
import interfaces.IDocumentsForTypeClickListener;
import utils.global.AppUtils;
import utils.global.dbObjects.Document;

/**
 * Created with care by Shahar Ben-Moshe on 08/03/17.
 */

public class DocumentsForTypeAdapter extends RecyclerView.Adapter<DocumentsForTypeAdapter.ViewHolder>
{
	private ArrayList<Document> mDocuments;
	private IDocumentsForTypeClickListener mDocumentsForTypeClickListener;
	
	public DocumentsForTypeAdapter(final ArrayList<Document> iDocuments, IDocumentsForTypeClickListener iDocumentsForTypeClickListener)
	{
		mDocuments = iDocuments;
		mDocumentsForTypeClickListener = iDocumentsForTypeClickListener;
	}
	
	@Override
	public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType)
	{
		return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.documents_for_type_item_cell_layout, parent, false));
	}
	
	@Override
	public void onBindViewHolder(final ViewHolder iViewHolder, final int iPosition)
	{
		final Document currentDocument = mDocuments.get(iPosition);
		
		if (currentDocument != null && currentDocument.isValid())
		{
			try
			{
				iViewHolder.mTvDocumentName.setText(AppUtils.nullSafeCheckString(currentDocument.getName()));
				
				iViewHolder.mIvDocumentImage.setImageBitmap(currentDocument.getDocumentImage());
				
				iViewHolder.mIvDocumentImage.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(final View v)
					{
						if (mDocumentsForTypeClickListener != null)
						{
							mDocumentsForTypeClickListener.onImageClick(currentDocument);
						}
					}
				});
				
				iViewHolder.mTvRemove.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(final View v)
					{
						if (mDocumentsForTypeClickListener != null)
						{
							int pos = iViewHolder.getAdapterPosition();
							mDocuments.remove(pos);
							mDocumentsForTypeClickListener.onRemoveClick(currentDocument, pos);
						}
					}
				});
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public int getItemCount()
	{
		return mDocuments == null ? 0 : mDocuments.size();
	}
	
	class ViewHolder extends RecyclerView.ViewHolder
	{
		TextView mTvDocumentName;
		ImageView mTvRemove;
		ImageView mIvDocumentImage;
		
		ViewHolder(final View itemView)
		{
			super(itemView);
			
			mTvDocumentName = (TextView) itemView.findViewById(R.id.tv_documentsForTypeCell_DocumentName);
			mTvRemove = (ImageView) itemView.findViewById(R.id.tv_documentsForTypeCell_Remove);
			mIvDocumentImage = (ImageView) itemView.findViewById(R.id.iv_documentsForTypeCell_DocumentImage);
		}
	}
}
