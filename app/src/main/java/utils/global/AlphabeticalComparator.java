package utils.global;

import java.util.Comparator;

import interfaces.ISearchElement;

public class AlphabeticalComparator implements Comparator<ISearchElement>
{
	@Override
	public int compare(final ISearchElement i1, final ISearchElement i2)
	{
		return i1.toString().compareTo(i2.toString());
	}
}
