package interfaces;

/**
 * Created by Erline.Katz on 04/12/2017.
 */

public interface IUpdateDataListener
{
	void updateDataListener();
}
