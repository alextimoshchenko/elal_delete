package webServices.responses.deleteUserPushMessage;

import android.text.TextUtils;
import android.text.format.DateUtils;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

import global.UserData;
import global.eLanguage;
import utils.global.DateTimeUtils;

/**
 * Created with care by Alexey.T on 25/07/2017.
 */
public class NotificationInstance implements UserMessage
{
	@JsonProperty("CampaignID")
	private int mCampaignID;
	@JsonProperty("CampaignName")
	private String mCampaignName = "";
	@JsonProperty("IsDeleted")
	private boolean mIsDeleted;
	@JsonProperty("IsRead")
	private boolean mIsRead;
	@JsonProperty("MessageName")
	private String mMessageName = "";
	@JsonProperty("MessageText")
	private String mMessageText = "";
	@JsonProperty("OSID")
	private int mOSID;
	
	@JsonProperty("PushType")
	private int mPushType;
	
	/**
	 * If true(link into App), if false(link to Url), if null(none)
	 */
	@JsonProperty("P1")
	private Boolean mP1;
	
	/**
	 * link if P1 true or false
	 */
	@JsonProperty("P2")
	private String mP2 = "";
	
	/**
	 * if true(open in browser), if false(open in WV)
	 */
	@JsonProperty("P3")
	private Boolean mP3;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeUtils.DATE_FORMAT_yyyy_MM_ddHHmmSS)
	@JsonProperty("RecievedDate")
	private Date mReceivedDate = new Date();
	@JsonProperty("UserID")
	private int mUserID;
	
	public NotificationInstance()
	{
	}
	
	//Constructor for test
	public NotificationInstance(final String iMessageText, final Date iReceivedDate, boolean iIsRead)
	{
		mMessageText = iMessageText;
		mReceivedDate = iReceivedDate;
		mIsRead = iIsRead;
	}
	
	public void setRead(final boolean iRead)
	{
		mIsRead = iRead;
	}
	
	public int getCampaignID()
	{
		return mCampaignID;
	}
	
	public boolean isRead()
	{
		return mIsRead;
	}
	
	public String getMessageName()
	{
		return mMessageName;
	}
	
	public String getMessageText()
	{
		return mMessageText;
	}
	
	private Date getReceivedDate()
	{
		return mReceivedDate;
	}
	
	public int getUserID()
	{
		return mUserID;
	}
	
	@JsonIgnore
	public String getNotificationDate()
	{
		String result;
		Date date = getReceivedDate();
		eLanguage language = UserData.getInstance().getLanguage();
		
		boolean isNotificationDateOlderThenToday = DateUtils.isToday(date.getTime());
		
		if (isNotificationDateOlderThenToday)
		{
			result = DateTimeUtils.convertDateToHourMinuteStringWithGMT(date);
		}
		else
		{
			result = DateTimeUtils.convertDateToHourMinuteDayMonthYearString(date, language);
		}
		
		return result;
	}
	
	public String getNotificationUrl()
	{
		return TextUtils.isEmpty(mP2) ? "" : mP2;
	}
	
	public eOpenType getOpenType()
	{
		return eOpenType.parse(mP3);
	}
	
	public enum eOpenType
	{
		NONE(false),
		BROWSER(true),
		WEB_VIEW(false);
		
		private Boolean mType;
		
		eOpenType(final Boolean iType)
		{
			mType = iType;
		}
		
		public static eOpenType parse(Boolean mType)
		{
			eOpenType result;
			
			if (mType == null)
			{
				result = NONE;
			}
			else if (mType)
			{
				result = BROWSER;
			}
			else
			{
				result = WEB_VIEW;
			}
			
			return result;
		}
		
	}
	
	@JsonIgnore
	public eKindOfLink getKindOfLink()
	{
		return eKindOfLink.parse(mP1);
	}
	
	public enum eKindOfLink
	{
		INTERNAL(true),
		EXTERNAL(false),
		NULL(null);
		
		private Boolean mType;
		
		eKindOfLink(final Boolean iType)
		{
			mType = iType;
		}
		
		public static eKindOfLink parse(Boolean iType)
		{
			eKindOfLink result;
			
			if (iType == null)
			{
				result = NULL;
			}
			else if (iType)
			{
				result = INTERNAL;
			}
			else
			{
				result = EXTERNAL;
			}
			
			return result;
		}
		
		public boolean isNull()
		{
			return this == NULL;
		}
	}
	
	public int getPushType()
	{
		return mPushType;
	}
	
	public boolean isTelemessage()
	{
		return mPushType == 2;
	}
}
