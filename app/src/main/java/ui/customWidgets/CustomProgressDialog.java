package ui.customWidgets;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import il.co.ewave.elal.R;

/**
 * Created by Avishay.Peretz on 06/09/2017.
 */

public class CustomProgressDialog extends ProgressDialog
{
	public CustomProgressDialog(Context context)
	{
		super(context, android.R.style.Theme_Translucent_NoTitleBar /*R.style.ProgressDialogTheme*/);
		setCancelable(false);
		setIndeterminate(true);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.custom_progress_dialog_view);
	}
}
