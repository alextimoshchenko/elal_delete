package webServices.responses.getWeather;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with care by Shahar Ben-Moshe on 28/02/17.
 */

public class Weather implements Parcelable
{
	@JsonProperty("CurrentTempC")
	private int mCurrentTempC;
	@JsonProperty("Description")
	private String mDescription;
	@JsonProperty("Forecast")
	private List<ForecastItem> mForecastItems;
	@JsonProperty("Image")
	private String mImage;
	@JsonProperty("Day")
	private String mDay;
	@JsonProperty("Date")
	private String mDate;
	@JsonProperty("CurrentTempF")
	private int mCurrentTempF;
	
	public Weather()
	{
	}
	
	public Integer getCurrentTempC()
	{
		return mCurrentTempC;
	}
	
	public String getDescription()
	{
		return mDescription;
	}
	
	public List<ForecastItem> getForecastItems()
	{
		return mForecastItems;
	}
	
	public String getImage()
	{
		return mImage;
	}
	
	public String getDay()
	{
		return mDay;
	}
	
	public String getDate()
	{
		return mDate;
	}
	
	public Integer getCurrentTempF()
	{
		return mCurrentTempF;
	}
	
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeInt(this.mCurrentTempC);
		dest.writeString(this.mDescription);
		dest.writeList(this.mForecastItems);
		dest.writeString(this.mImage);
		dest.writeString(this.mDay);
		dest.writeString(this.mDate);
		dest.writeInt(this.mCurrentTempF);
	}
	
	protected Weather(Parcel in)
	{
		this.mCurrentTempC = in.readInt();
		this.mDescription = in.readString();
		this.mForecastItems = new ArrayList<ForecastItem>();
		in.readList(this.mForecastItems, ForecastItem.class.getClassLoader());
		this.mImage = in.readString();
		this.mDay = in.readString();
		this.mDate = in.readString();
		this.mCurrentTempF = in.readInt();
	}
	
	public static final Parcelable.Creator<Weather> CREATOR = new Parcelable.Creator<Weather>()
	{
		@Override
		public Weather createFromParcel(Parcel source)
		{
			return new Weather(source);
		}
		
		@Override
		public Weather[] newArray(int size)
		{
			return new Weather[size];
		}
	};
}
