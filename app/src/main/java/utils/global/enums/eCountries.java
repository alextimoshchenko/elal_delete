package utils.global.enums;

/**
 * Created with care by Aleksey Timoshchenko on 8/18/17.
 */
public enum eCountries
{
	ISRAEL("IL");
	
	private String mCountryCode;
	
	eCountries(String iCountryCode)
	{
		mCountryCode = iCountryCode;
	}
	
	public String getCountryCode()
	{
		return mCountryCode;
	}
}
