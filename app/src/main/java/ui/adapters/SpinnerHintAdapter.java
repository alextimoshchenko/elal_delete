package ui.adapters;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import il.co.ewave.elal.R;
import ui.customWidgets.ChangeDirectionLinearLayout;

/**
 * Created by Avishay.Peretz on 26/07/2017.
 */

public class SpinnerHintAdapter extends ArrayAdapter
{
	
	
	private String mTitle = "";
	private int hidingItemIndex;
	private List mList;
	
	public SpinnerHintAdapter(Context context, int textViewResourceId, List objects, int hidingItemIndex)
	{
		super(context, textViewResourceId, objects);
		this.hidingItemIndex = hidingItemIndex;
		this.mList = objects;
	}
	
	//	public SpinnerHintAdapter(Context context, int textViewResourceId, List  objects, int hidingItemIndex, String title) {
	//		super(context, textViewResourceId, objects);
	//		this.hidingItemIndex = hidingItemIndex;
	//		this.mList = objects;
	//		mTitle = title;
	//	}
	//
	//	public SpinnerHintAdapter(@NonNull final Context context, @LayoutRes final int resource, @IdRes final int textViewResourceId, @NonNull final List objects, final int iHidingItemIndex)
	//	{
	//		super(context, resource, textViewResourceId, objects);
	//		hidingItemIndex = iHidingItemIndex;
	//		this.mList = objects;
	//	}
	
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent)
	{
		View v = null;
		if (position == hidingItemIndex)
		{
			TextView tv = new TextView(getContext());
			tv.setVisibility(View.GONE);
			tv.setHeight(0);
			v = tv;
		}
		else
		{
			v = super.getDropDownView(position, null, parent);
		}
		return v;
	}
	
	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_spinner_details_layout, parent, false);
		}
		TextView tvName = (TextView) convertView.findViewById(android.R.id.text1);
		tvName.setText(mList.get(position).toString());
		
		return convertView;
	}
	
	
}