package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class RequestSearchFlight
{
	@JsonProperty("SearchParams")
	private ArrayList<RequestSearchFlightSearchParams> mSearchParams;
	@JsonProperty("Family")
	private ArrayList<RequestSearchFlightFamily> mFamily;
	
	public RequestSearchFlight(ArrayList<RequestSearchFlightSearchParams> iSearchParams, ArrayList<RequestSearchFlightFamily> iFamily)
	{
		mSearchParams = iSearchParams;
		mFamily = iFamily;
	}
	
	public RequestSearchFlight()
	{
	}
}
