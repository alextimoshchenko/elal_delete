package ui.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.hannesdorfmann.adapterdelegates3.AdapterDelegatesManager;

import java.util.ArrayList;
import java.util.List;

import il.co.ewave.elal.R;
import interfaces.INotificationClickListener;
import ui.customWidgets.swipeLayout.adapters.RecyclerSwipeAdapter;
import webServices.responses.deleteUserPushMessage.NotificationInstanceDelegate;
import webServices.responses.deleteUserPushMessage.UserMessage;

/**
 * Created with care by Alexey.T on 26/07/2017.
 * </p>
 * Implementation was taken from this article http://hannesdorfmann.com/android/adapter-delegates
 * </p>
 * and this git repo https://github.com/sockeqwe/AdapterDelegates
 */
public class NotificationInstancesAdapter extends RecyclerSwipeAdapter
{
	private AdapterDelegatesManager<List<UserMessage>> mDelegatesManager;
	private List<UserMessage> mCurrentCurrentUserMessageList = new ArrayList<>();
	
	public NotificationInstancesAdapter(Activity iActivity, List<UserMessage> iCurrentUserMessageList, final INotificationClickListener iListener)
	{
		mCurrentCurrentUserMessageList = new ArrayList<>(iCurrentUserMessageList);
		mDelegatesManager = new AdapterDelegatesManager<>();
		mDelegatesManager.addDelegate(new NotificationInstanceDelegate(iActivity, iListener));
	}
	
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup iParent, final int viewType)
	{
		return mDelegatesManager.onCreateViewHolder(iParent, viewType);
	}
	
	@Override
	public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position)
	{
		mDelegatesManager.onBindViewHolder(mCurrentCurrentUserMessageList, position, holder);
	}
	
	@Override
	public int getItemViewType(final int position)
	{
		return mDelegatesManager.getItemViewType(mCurrentCurrentUserMessageList, position);
	}
	
	public void updateNotificationInstancesList(List<UserMessage> iNotificationInstancesList)
	{
		mCurrentCurrentUserMessageList.clear();
		
		if (iNotificationInstancesList != null && !iNotificationInstancesList.isEmpty())
		{
			mCurrentCurrentUserMessageList.addAll(iNotificationInstancesList);
		}
	}
	
	@Override
	public int getItemCount()
	{
		return mCurrentCurrentUserMessageList == null || mCurrentCurrentUserMessageList.isEmpty() ? 0 : mCurrentCurrentUserMessageList.size();
	}
	
	@Override
	public int getSwipeLayoutResourceId(final int position)
	{
		return R.id.sl_notification_item_cover_view;
	}
}