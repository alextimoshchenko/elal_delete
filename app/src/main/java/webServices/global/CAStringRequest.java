package webServices.global;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import il.co.ewave.elal.BuildConfig;
import utils.HashUtilsManager;
import utils.global.AppUtils;

/**
 * Created by erline.katz on 23/10/2017.
 */

public class CAStringRequest extends StringRequest
{
	private static final String TAG = CAStringRequest.class.getSimpleName();
	private static final String USER_AGENT = "User-Agent";
	//	private static final String USER_AGENT_CONTENT = "ElalMobile/2.0 (Android) Ewave/1.0 (http://www.ewave.co.il)";
	private HashMap<String, String> mHeaders;
	
	public CAStringRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener)
	{
		super(method, url, listener, errorListener);
		
		if (il.co.ewave.elal.BuildConfig.DEBUG)
		{
			AppUtils.printLog(Log.DEBUG, TAG, "RequestWsUrl = " + (getUrl() == null ? "" : getUrl()));
		}
		
		
		//		CookieHandler.setDefault(new java.net.CookieManager(null, CookiePolicy.ACCEPT_ALL));
	}
	
	@Override
	public final Map<String, String> getHeaders() /*throws AuthFailureError*/
	{
		if (mHeaders == null)
		{
			mHeaders = new HashMap<>();
		}
		mHeaders.put(USER_AGENT, JacksonRequest.USER_AGENT_CONTENT);
		
		AppUtils.printLog(Log.DEBUG, TAG, "headers: " + mHeaders.toString());
		
		return mHeaders;
	}
	
	@Override
	protected Response<String> parseNetworkResponse(NetworkResponse response)
	{
		try
		{
			String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
			
			if (BuildConfig.DEBUG)
			{
				AppUtils.printLog(Log.DEBUG, TAG, "ResponseJsonString = " + Mapper.prettifyString(jsonString));
				AppUtils.printLog(Log.DEBUG, TAG, "ResponseWsUrl = " + (getUrl() == null ? "" : getUrl()));
			}
			
			
			if (response.headers.get(JacksonRequest.EWAVE_HASH) != null){
				if ( !TextUtils.isEmpty(response.headers.get(JacksonRequest.EWAVE_HASH)))
				{
					if (!TextUtils.isEmpty(jsonString))
					{
						if (HashUtilsManager.getInstance().isAuthenticHash(jsonString, response.headers.get(JacksonRequest.EWAVE_HASH)))
						{
							//do nothing
							Log.w("parseNetworkResponse", "isAuthenticHash: **true**");
						}
						else
						{
							return Response.error(new VolleyError());
						}
					}
					else
					{
						return Response.error(new VolleyError());
					}
				}
				else
				{
					return Response.error(new VolleyError());
				}
			}
			
			return Response.success(jsonString, HttpHeaderParser.parseCacheHeaders(response));
		}
		catch (Exception iE)
		{
			return Response.error(new VolleyError(iE));
		}
	}
	
	@Override
	protected VolleyError parseNetworkError(VolleyError volleyError)
	{
		if (BuildConfig.DEBUG)
		{
			AppUtils.printLog(Log.DEBUG, TAG, "ResponseErrorCode = " + String.valueOf((volleyError != null && volleyError.networkResponse != null) ? volleyError.networkResponse.statusCode : -1));
			AppUtils.printLog(Log.DEBUG, TAG, "ResponseErrorMessage = " + ((volleyError != null && !TextUtils.isEmpty(volleyError.toString())) ? volleyError.toString() : ""));
			AppUtils.printLog(Log.DEBUG, TAG, "ResponseWsUrl = " + (getUrl() == null ? "" : getUrl()));
		}
		
		return super.parseNetworkError(volleyError);
	}
	
	public void setSecretHashHeader(final byte[] iBody)
	{
		String hash = HashUtilsManager.getInstance().getSecretHashHeader(iBody);
		if (!TextUtils.isEmpty(hash))
		{
			getHeaders().put(JacksonRequest.EWAVE_HASH, hash);
		}
	}
}
