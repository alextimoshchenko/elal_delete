package ui.customWidgets;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import global.ElAlApplication;
import il.co.ewave.elal.R;
import utils.global.AppUtils;

/**
 * Created by Avishay.Peretz on 21/06/2017.
 */
// TODO: 19/03/17 Shahar change occurrences of "2" position to 2 or 0 depending on rtl.
public class TooltipEditText extends android.support.v7.widget.AppCompatAutoCompleteTextView implements View.OnTouchListener, View.OnFocusChangeListener
{
	private Drawable mTooltipDrawable;
	
	public TooltipEditText(Context context)
	{
		super(context);
		init();
	}
	
	public TooltipEditText(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}
	
	public TooltipEditText(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		init();
	}
	
	@Override
	public void setOnTouchListener(OnTouchListener l)
	{
		this.l = l;
	}
	
	@Override
	public void setOnFocusChangeListener(OnFocusChangeListener f)
	{
		this.f = f;
	}
	
	private OnTouchListener l;
	private OnFocusChangeListener f;
	
	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		if (getCompoundDrawables()[2] != null)
		{
			boolean tappedX = event.getX() > (getWidth() - getPaddingRight() - mTooltipDrawable.getIntrinsicWidth());
			if (tappedX)
			{
				if (event.getAction() == MotionEvent.ACTION_UP)
				{
					setText("");
//					AppUtils.showToolTip(ElAlApplication.getInstance(),mTooltipDrawable.get  , "TOOLTIP!!!!!");
				}
				return true;
			}
		}
		
		return l != null && l.onTouch(v, event);
	}
	
	@Override
	public void onFocusChange(View v, boolean hasFocus)
	{
		//		if (hasFocus)
		//		{
		//			setClearIconVisible(!TextUtils.isEmpty(getText()));
		//		}
		//		else
		//		{
		//			setClearIconVisible(false);
		//		}
		if (f != null)
		{
			f.onFocusChange(v, hasFocus);
		}
	}
	
	@SuppressWarnings("deprecated")
	private void init()
	{
		mTooltipDrawable = getCompoundDrawables()[2];
		
		if (mTooltipDrawable == null)
		{
			//			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
			//			{
			//				mTooltipDrawable = getResources().getDrawable(android.R.drawable.presence_offline, getContext().getTheme());
			//			}
			//			else
			//			{
			//				mTooltipDrawable = getResources().getDrawable(android.R.drawable.presence_offline);
			//			}
			mTooltipDrawable = getResources().getDrawable(R.mipmap.tooltip);
			
		}
		
		//		if (mTooltipDrawable != null)
		//		{
		//			mTooltipDrawable.setBounds(0, 0, mTooltipDrawable.getIntrinsicWidth(), mTooltipDrawable.getIntrinsicHeight());
		//		}
		
		//		if ( isCheckboxTextRtl() )
		//		{
		//			setCompoundDrawablesWithIntrinsicBounds(null, null, mTooltipDrawable, null);
		//		}
		//		else
		//		{
//		setCompoundDrawablesWithIntrinsicBounds(mTooltipDrawable, null, null, null);
		//		}
//		setCompoundDrawablePadding(15);
		
		
		//		setClearIconVisible(false);
		setClearIconVisible(true);
		super.setOnTouchListener(this);
		super.setOnFocusChangeListener(this);
		setTextWatcher();
	}
	
	protected void setClearTextDrawableRTL(final boolean iIsRTL)
	{
		if (mTooltipDrawable !=null)
		{
			if (!iIsRTL)
			{
				setCompoundDrawablesWithIntrinsicBounds(null, null, mTooltipDrawable, null);
			}
			else
			{
				setCompoundDrawablesWithIntrinsicBounds(mTooltipDrawable, null, null, null);
			}
			setCompoundDrawablePadding(15);
		}
	}
	
	private void setTextWatcher()
	{
		addTextChangedListener(new TextWatcher()
		{
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after)
			{
			}
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				//				if (isFocused())
				//				{
				//					setClearIconVisible(!TextUtils.isEmpty(TooltipEditText.this.getText().toString()));
				//				}
			}
			
			@Override
			public void afterTextChanged(Editable s)
			{
			}
		});
	}
	
	protected void setClearIconVisible(boolean visible)
	{
		boolean wasVisible = (getCompoundDrawables()[2] != null);
		if (visible != wasVisible)
		{
			Drawable x = visible ? mTooltipDrawable : null;
			setCompoundDrawables(getCompoundDrawables()[0], getCompoundDrawables()[1], x, getCompoundDrawables()[3]);
		}
	}
}