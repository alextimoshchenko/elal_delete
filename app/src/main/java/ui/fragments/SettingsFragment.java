package ui.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import global.AppData;
import global.ElAlApplication;
import global.UserData;
import global.eLanguage;
import il.co.ewave.elal.R;
import utils.errors.ErrorsHandler;
import utils.global.AppUtils;
import utils.global.LocaleUtils;
import webServices.controllers.MainController;
import webServices.requests.RequestGetUserSettings;
import webServices.requests.RequestSetUserSettings;
import webServices.responses.ResponseGetUserSettings;
import webServices.responses.ResponseSetUserSettings;
import webServices.responses.getAppData.Language;
import webServices.responses.getAppData.Representation;

/**
 * Created with care by Shahar Ben-Moshe on 11/01/17.
 */

public class SettingsFragment extends EWBaseFragment
{
	private Spinner mSpnrRepresentation;
	private Spinner mSpnrLanguages;
	private Button mBtnSave;
	private ToggleButton mTglIsFlight;
	private ToggleButton mTglIsDeal;
	private boolean mShouldSetUserSettings = true;
	private boolean mIsFromSaveBtn;
	private TextView mTvVersionCode;
	//	private Representation mCurrentRepresentation;
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_settings_layout_new);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		initReference();
		setListeners();
		setSpinners();
		
		pushScreenOpenEvent("Settings");
	}
	
	private void initReference()
	{
		this.mSpnrRepresentation = getActivity().findViewById(R.id.spnr_settings_Representation);
		this.mSpnrLanguages = getActivity().findViewById(R.id.spnr_settings_Languages);
		this.mBtnSave = getActivity().findViewById(R.id.btn_settings_Save);
		this.mTglIsFlight = getActivity().findViewById(R.id.tgl_settings_IsFlight);
		this.mTglIsDeal = getActivity().findViewById(R.id.tgl_settings_IsDeal);
		this.mTvVersionCode = getActivity().findViewById(R.id.tv_splash_versionCode);
		mTvVersionCode.setText(String.format("v%s", String.valueOf(AppUtils.getApplicationVersion(ElAlApplication.getInstance()))));
		
	}
	
	private void setListeners()
	{
		getActivity().findViewById(R.id.iv_name_screen_back_arrow).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnBack();
				
			}
		});
		mBtnSave.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				mIsFromSaveBtn = true;
				setUserSettings();
			}
		});
		
		mTglIsFlight.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked)
			{
				setUserSettings();
			}
		});
		
		mTglIsDeal.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked)
			{
				setUserSettings();
			}
		});
	}
	
	private void doOnBack()
	{
		tryPopFragment();
	}
	
	private void setSpinners()
	{
		setRepresentationsAdapter();
		setLanguagesAdapter();
	}
	
	private void setRepresentationsAdapter()
	{
		if (AppData.getInstance().getRepresentations() != null)
		{
			final ArrayAdapter<Representation> representationsAdapter = new ArrayAdapter<>(getActivity(), R.layout.custom_spinner_settings_layout_new, android.R.id.text1, AppData.getInstance()
			                                                                                                                                                                      .getRepresentations());
			representationsAdapter.setDropDownViewResource(R.layout.custom_drop_down);
			mSpnrRepresentation.setAdapter(representationsAdapter);
			
			//set ISRAEL by default
			int position=AppData.getInstance().getIsraelRepresentationPosition();
			mSpnrRepresentation.setSelection( position);
			
		}
	}
	
	private void setLanguagesAdapter()
	{
		if (AppData.getInstance().getLanguages() != null)
		{
			final ArrayAdapter<Language> languagesAdapter = new ArrayAdapter<>(getActivity(), R.layout.custom_spinner_settings_layout_new, android.R.id.text1, AppData.getInstance().getLanguages());
			languagesAdapter.setDropDownViewResource(R.layout.custom_drop_down);
			mSpnrLanguages.setAdapter(languagesAdapter);
		}
	}
	
	@Override
	public void onServiceConnected()
	{
		if (isServiceConnected())
		{
			trySetProgressDialog(true);
			
			getService().getController(MainController.class).GetUserSettings(new RequestGetUserSettings(UserData.getInstance().getUserID()), new Response.Listener<ResponseGetUserSettings>()
			{
				@Override
				public void onResponse(final ResponseGetUserSettings response)
				{
					onGetUserSettings(response);
				}
			}, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError iVolleyError)
				{
//					// TODO: 11/01/17 Shahar show iVolleyError...?
					// TODO: avishay 30/11/17 don't show the popup
//					ErrorsHandler.tryShowServiceErrorDialog(iVolleyError, getActivity());
					int position=AppData.getInstance().getIsraelRepresentationPosition();
					mSpnrRepresentation.setSelection( position);
					
				}
			});
		}
	}
	
	private void setUserSettings()
	{
		if (mShouldSetUserSettings && isServiceConnected())
		{
			trySetProgressDialog(true);
			
			getService().getController(MainController.class)
			            .SetUserSettings(new RequestSetUserSettings(UserData.getInstance()
			                                                                .getUserID(), ((Representation) mSpnrRepresentation.getSelectedItem()).getRepresentationID(), ((Language) mSpnrLanguages.getSelectedItem())
					            .getLanguageCode(), mTglIsFlight.isChecked(), mTglIsDeal.isChecked()), new Response.Listener<ResponseSetUserSettings>()
			            {
				            @Override
				            public void onResponse(final ResponseSetUserSettings response)
				            {
					            trySetProgressDialog(false);
					
					            onSuccessfulSetUserDetails();
				            }
			            }, new Response.ErrorListener()
			            {
				            @Override
				            public void onErrorResponse(final VolleyError iVolleyError)
				            {
					            // TODO: 11/01/17 Shahar show iVolleyError...?
					            ErrorsHandler.tryShowServiceErrorDialog(iVolleyError, getActivity());
				            }
			            });
		}
	}
	
	private void onSuccessfulSetUserDetails()
	{
		eLanguage language = eLanguage.getLanguageByCodeOrDefault(((Language) mSpnrLanguages.getSelectedItem()).getLanguageCode(), UserData.getInstance().getLanguage());
		int representationId = ((Representation) mSpnrRepresentation.getSelectedItem()).getRepresentationID();
		
		
		if (representationId != UserData.getInstance().getRepresentationId())
		{
			//			doBack = true;
			UserData.getInstance().setUserRepresentation(representationId);
			UserData.getInstance().saveUserData();
		}
		
		if (language != UserData.getInstance().getLanguage())
		{
			mIsFromSaveBtn = false;
			UserData.getInstance().setLanguage(language);
			UserData.getInstance().saveUserData();
			LocaleUtils.applyLocale(ElAlApplication.getInstance(), LocaleUtils.convertLanguageCodeToLocaleOrNull(language.getLanguageCode()));
			
			AlertDialog dialog = AppUtils.createSimpleMessageDialog(getActivity(), /*ElAlApplication.getInstance().*/getString(R.string.restart_after_language_change), null, null, getString(R.string.close), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(final DialogInterface dialog, final int which)
				{
					//					getActivity().finish();
					if (getActivity() != null)
					{
						getActivity().finishAffinity();
					}
				}
			});
			dialog.setCancelable(false);
			dialog.show();
		}
		if (mIsFromSaveBtn)
		{
			mIsFromSaveBtn = false;
			doOnBack();
			//		tryGoToScreenById(eInAppNavigation.Main.getNavigationId(), Bundle.EMPTY);
		}
	}
	
	private void onGetUserSettings(final ResponseGetUserSettings iResponse)
	{
		if (iResponse != null && iResponse.getContent() != null)
		{
			mShouldSetUserSettings = false;
			
			if (iResponse.getContent().getRepresentationID() > -1 &&   AppData.getInstance().getRepresentations()!= null)
			{
				int representationsSize = AppData.getInstance().getRepresentations().size();
				for (int i = 0 ; i < representationsSize ; i++)
				{
					Representation representation = AppData.getInstance().getRepresentations().get(i);
					
					if (representation != null && iResponse.getContent().getRepresentationID() == representation.getRepresentationID())
					{
						mSpnrRepresentation.setSelection(i);
						//						mCurrentRepresentation = representation;
						break;
					}
				}
			}
			
			if (!TextUtils.isEmpty(iResponse.getContent().getLanguageID()))
			{
				int languagesSize = AppData.getInstance().getLanguages().size();
				for (int i = 0 ; i <languagesSize ; i++)
				{
					Language language = AppData.getInstance().getLanguages().get(i);
					
					if (language != null && iResponse.getContent().getLanguageID().equals(language.getLanguageCode()))
					{
						mSpnrLanguages.setSelection(i);
						break;
					}
				}
			}
			
			mTglIsFlight.setChecked(iResponse.getContent().isPushFlightsInfo());
			mTglIsDeal.setChecked(iResponse.getContent().isPushOffersInfo());
			
			mShouldSetUserSettings = true;
		}
		
		trySetProgressDialog(false);
	}
}
