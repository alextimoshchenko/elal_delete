package utils.global.searchFlightObjects;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import global.AppData;
import global.ElAlApplication;
import global.UserData;
import il.co.ewave.elal.R;
import interfaces.IChoseActionElement;
import interfaces.ISearchElement;
import utils.errors.LocalError.eLocalError;
import utils.global.AppUtils;
import utils.global.enums.eCities;
import utils.global.enums.eCountries;
import utils.global.enums.eEditAction;
import utils.global.enums.eFamilyRelation;
import utils.global.enums.eIntention;
import utils.global.enums.eListType;
import utils.global.enums.ePassengersType;
import utils.global.enums.eSearchDestinationType;
import utils.global.enums.eTabStatement;
import utils.global.enums.eTypesOfTrip;
import utils.network.NetworkUtils;
import webServices.requests.RequestSearchFlight;
import webServices.requests.RequestSearchFlightFamily;
import webServices.requests.RequestSearchFlightSearchParams;
import webServices.responses.ResponseGetSearchGlobalData;
import webServices.responses.getSearchGlobalData.GetSearchGlobalDataContentElalCities;
import webServices.responses.getSearchGlobalData.GetSearchGlobalDataContentFlightClasses;
import webServices.responses.getSearchGlobalData.GetSearchGlobalDataContentUserFamily;
import webServices.responses.getSearchHistory.ResponseGetSearchHistoryContent;
import webServices.responses.getSearchHistory.ResponseGetSearchHistoryContentFamily;
import webServices.responses.responseUserLogin.UserObject;

public class SearchObject implements NetworkUtils.DataQuery
{
	private static final String TAG = SearchObject.class.getSimpleName();
	private final int USER_HIMSELF = 0;
	
	private static SearchObject mObject;
	
	public final int MAX_PASSENGERS_QUANTITY = 9;
	
	private UserFamilyDelegate<GetSearchGlobalDataContentUserFamily> mUserFamilyDelegate;
	
	//This field I use for checking if it is null so need to execute request to server otherwise no
	private ResponseGetSearchGlobalData mResponseSearchGlobalData;
	
	//Global data
	private List<GetSearchGlobalDataContentElalCities> mElalDepartureCitiesList = new ArrayList<>();
	private List<GetSearchGlobalDataContentElalCities> mElalDestinationCitiesList = new ArrayList<>();
	private List<GetSearchGlobalDataContentFlightClasses> mFlightClassesList = new ArrayList<>();
	
	//Types of trip
	private List<IChoseActionElement> mTypeOfTripList = new ArrayList<IChoseActionElement>()
	{{
		add(eTypesOfTrip.ROUND_TRIP);
		add(eTypesOfTrip.ONE_WAY_TICKET);
		add(eTypesOfTrip.RETURN_FROM_ANOTHER_DESTINATION);
	}};
	
	//Passengers List
	private Map<ePassengersType, ArrayList<GetSearchGlobalDataContentUserFamily>> mPassengersMap = new HashMap<>();
	
	//Current data
	private GetSearchGlobalDataContentElalCities mCurrentOriginDestination = new GetSearchGlobalDataContentElalCities();
	private GetSearchGlobalDataContentElalCities mCurrentToDestination = new GetSearchGlobalDataContentElalCities();
	private GetSearchGlobalDataContentElalCities mAdditionalCurrentOriginDestination = new GetSearchGlobalDataContentElalCities();
	private GetSearchGlobalDataContentElalCities mAdditionalCurrentToDestination = new GetSearchGlobalDataContentElalCities();
	private eTabStatement mCurrentTabStatement = eTabStatement.FLIGHT_TICKET;
	private eSearchDestinationType mCurrentDestinationType = eSearchDestinationType.FIND_ORIGIN_DESTINATION;
	private Date mCurrentFirstDateInSelection, mCurrentLastDateInSelection;
	private eTypesOfTrip mCurrentTypeOfTrip = (eTypesOfTrip) mTypeOfTripList.get(0);
	private GetSearchGlobalDataContentFlightClasses mCurrentFlightClass;
	private boolean mIsCurrentDateSelected;
	
	private SearchObject()
	{
		mUserFamilyDelegate = new UserFamilyDelegate<>();
		resetPassengerMap();
		
		//		if (GlobalVariables.isDebugVersion())
		//		{
		//TODO Just for testing (delete it)
		//			mUserFamilyList = new ArrayList<GetSearchGlobalDataContentUserFamily>()
		//			{{
		//				add(new GetSearchGlobalDataContentUserFamily("name1", 9991, 8881, getBirth(), "lastName1", 1));
		//				add(new GetSearchGlobalDataContentUserFamily("name2", 9992, 8882, new Date(today.getTime() + day * 2), "lastName2", 1));
		//				add(new GetSearchGlobalDataContentUserFamily("name3", 9993, 8883, new Date(today.getTime() + day * 3), "lastName3", 1));
		//			}};
		//		}
	}
	
	private void resetPassengerMap()
	{
		mPassengersMap = new HashMap<ePassengersType, ArrayList<GetSearchGlobalDataContentUserFamily>>()
		{{
			put(ePassengersType.ADULTS, new ArrayList<GetSearchGlobalDataContentUserFamily>());
			put(ePassengersType.CHILDREN, new ArrayList<GetSearchGlobalDataContentUserFamily>());
			put(ePassengersType.INFANTS, new ArrayList<GetSearchGlobalDataContentUserFamily>());
			put(ePassengersType.TEENAGERS, new ArrayList<GetSearchGlobalDataContentUserFamily>());
			put(ePassengersType.YOUTH, new ArrayList<GetSearchGlobalDataContentUserFamily>());
		}};
	}
	
	private void deleteAllFamilyMembers()
	{
		for (Map.Entry<ePassengersType, ArrayList<GetSearchGlobalDataContentUserFamily>> entry : mPassengersMap.entrySet())
		{
			ArrayList<GetSearchGlobalDataContentUserFamily> currentList = entry.getValue();
			Iterator it = currentList.iterator();
			
			while (it.hasNext())
			{
				GetSearchGlobalDataContentUserFamily item = (GetSearchGlobalDataContentUserFamily) it.next();
				
				if (item.isFamilyMember())
				{
					it.remove();
				}
			}
		}
	}
	
	public ePassengersType getPassengersTypeByExistingPassenger(GetSearchGlobalDataContentUserFamily iUserFamily)
	{
		ePassengersType result = ePassengersType.NO_SUCH_ITEM;
		List<ePassengersType> listOfTypes = new ArrayList<>(getPassengersList().keySet());
		
		for (ePassengersType currentType : listOfTypes)
		{
			List<GetSearchGlobalDataContentUserFamily> currentFamilyList = getPassengersListByType(currentType);
			
			for (GetSearchGlobalDataContentUserFamily currentUser : currentFamilyList)
			{
				if (currentUser == iUserFamily)
				{
					result = currentType;
				}
			}
		}
		
		return result;
	}
	
	public static SearchObject getInstance()
	{
		if (mObject == null)
		{
			mObject = new SearchObject();
		}
		
		return mObject;
	}
	
	public int getSelectedAdultsAndYouthQuantity()
	{
		int adultsQuantity = getSelectedPassengersQuantityByType(ePassengersType.ADULTS);
		int youthQuantity = getSelectedPassengersQuantityByType(ePassengersType.YOUTH);
		return adultsQuantity + youthQuantity;
	}
	
	public boolean isContainsUserFamilyListSelectedAdultsOrYouthAtLeastOne()
	{
		return getSelectedAdultsAndYouthQuantity() >= 1;
	}
	
	public boolean isEnoughAdultsMembersForType(ePassengersType iType)
	{
		return isEnoughAdultsMembersForType(iType, null);
	}
	
	public boolean isEnoughAdultsMembersForType(ePassengersType iType, eIntention iIntention)
	{
		boolean result = false;
		
		if (iType.isInfants())
		{
			result = isEnoughAdultsMembersForInfants(iIntention);
		}
		else if (iType.isChildren())
		{
			result = isEnoughAdultsMembersForChildren();
		}
		else if (iType.isTeenagers())
		{
			result = isEnoughAdultsMembersForTeenagers();
		}
		
		return result;
	}
	
	private boolean isEnoughAdultsMembersForInfants(eIntention iIntention)
	{
		boolean result = false;
		
		int adultsSummary = getSelectedAdultsAndYouthQuantity();
		
		if (iIntention.isIncrease())
		{
			result = adultsSummary > getSelectedPassengersQuantityByType(ePassengersType.INFANTS);
		}
		else if (iIntention.isDecrease())
		{
			result = adultsSummary >= getSelectedPassengersQuantityByType(ePassengersType.INFANTS);
		}
		
		return result;
	}
	
	private boolean isEnoughAdultsMembersForChildren()
	{
		return isContainsUserFamilyListSelectedAdultsOrYouthAtLeastOne();
	}
	
	private boolean isEnoughAdultsMembersForTeenagers()
	{
		return isContainsUserFamilyListSelectedAdultsOrYouthAtLeastOne();
	}
	
	private void cleanAllCurrentData()
	{
		mResponseSearchGlobalData = null;
		mCurrentOriginDestination = new GetSearchGlobalDataContentElalCities();
		mCurrentToDestination = new GetSearchGlobalDataContentElalCities();
		mAdditionalCurrentOriginDestination = new GetSearchGlobalDataContentElalCities();
		mAdditionalCurrentToDestination = new GetSearchGlobalDataContentElalCities();
		mCurrentFirstDateInSelection = mCurrentLastDateInSelection = null;
		mCurrentTypeOfTrip = (eTypesOfTrip) mTypeOfTripList.get(0);
		mCurrentFlightClass = new GetSearchGlobalDataContentFlightClasses();
		
		unselectAllMembers();
	}
	
	public void unselectAllMembers()
	{
		for (ArrayList<GetSearchGlobalDataContentUserFamily> currentList : getPassengersList().values())
		{
			for (GetSearchGlobalDataContentUserFamily iMember : currentList)
			{
				if (iMember.isSelectedForSearching())
				{
					iMember.setSelectedForSearching(false);
				}
			}
		}
	}
	
	public void cleanSearchObject()
	{
		mObject = null;
	}
	
	public List<GetSearchGlobalDataContentElalCities> getElalDepartureCitiesList()
	{
		return mElalDepartureCitiesList;
	}
	
	public void setDepartureElalCitiesList(final List<GetSearchGlobalDataContentElalCities> iElalDepartureCitiesList)
	{
		if (iElalDepartureCitiesList != null && !iElalDepartureCitiesList.isEmpty())
		{
			mElalDepartureCitiesList = iElalDepartureCitiesList;
			tryToSetTLVasDefaultOriginDestination();
		}
	}
	
	public List<GetSearchGlobalDataContentElalCities> getElalDestinationCitiesList()
	{
		return mElalDestinationCitiesList;
	}
	
	public void setElalDestinationCitiesList(final List<GetSearchGlobalDataContentElalCities> iElalDestinationCitiesList)
	{
		if (iElalDestinationCitiesList != null && !iElalDestinationCitiesList.isEmpty())
		{
			mElalDestinationCitiesList = iElalDestinationCitiesList;
		}
	}
	
	private void tryToSetTLVasDefaultOriginDestination()
	{
		int currentUserRepresentation = UserData.getInstance().getRepresentationId();
		int israelRepresentation = AppData.getInstance().getRepresentationPositionByCity(eCountries.ISRAEL);
		
		boolean isUserFromIsrael = currentUserRepresentation == israelRepresentation;
		
		if (isUserFromIsrael)
		{
			Context context = ElAlApplication.getInstance();
			for (GetSearchGlobalDataContentElalCities item : mElalDepartureCitiesList)
			{
				if (item.getCityName().toLowerCase().contains(context.getString(R.string.compare_name).toLowerCase()))
				{
					mCurrentOriginDestination = item;
					return;
				}
			}
		}
	}
	
	
	public List<GetSearchGlobalDataContentUserFamily> getUserFamilyList()
	{
		return mUserFamilyDelegate.getFamilyListWithAgeEdition();
	}
	
	public void setUserFamilyList(final ArrayList<GetSearchGlobalDataContentUserFamily> iUserFamilyList)
	{
		if (iUserFamilyList == null || iUserFamilyList.isEmpty())
		{
			UserObject uO = UserData.getInstance().getUserObject();
			
			/**
			 * If user didn't provide name, so in SearchPassengersFragment no need to present his in family block at the top of screen
			 * **/
			//			boolean isUserNameKnown = uO == null ? false : !TextUtils.isEmpty(uO.getFirstNameEng());
			boolean isUserNameKnown = uO != null && !TextUtils.isEmpty(uO.getFirstNameEng());
			
			if (UserData.getInstance().isUserGuest() || !isUserNameKnown)
			{
				// According to documentation need to be existed at least one adults
				if (!isContainsUserFamilyListSelectedAdultsOrYouthAtLeastOne())
				{
					addOneNotFamilySelectedMemberToListByType(ePassengersType.ADULTS);
				}
				
				return;
			}
			else
			{
				GetSearchGlobalDataContentUserFamily userHimself = new GetSearchGlobalDataContentUserFamily(uO.getFirstNameEng(), uO.getLastNameEng(), uO.getTitle(), uO.getUserID(), uO.getDateOfBirth());
				mUserFamilyDelegate.add(userHimself);
			}
		}
		
		if (iUserFamilyList != null)
		{
			mUserFamilyDelegate.setUserFamilyList(iUserFamilyList);
		}
		
		//it is User himself
		mUserFamilyDelegate.get(USER_HIMSELF).setSelectedForSearching(true);
		mUserFamilyDelegate.get(USER_HIMSELF).setUserHimself(true);
		
		mUserFamilyDelegate.updateAgeFamilyList();
		
		sortListAccordingToAge(mUserFamilyDelegate.getFamilyListWithAgeEdition());
	}
	
	private void sortListAccordingToAge(List<GetSearchGlobalDataContentUserFamily> iUserFamilyList)
	{
		deleteAllFamilyMembers();
		
		for (GetSearchGlobalDataContentUserFamily familyMember : iUserFamilyList)
		{
			for (ePassengersType iType : ePassengersType.values())
			{
				if (!iType.isNoSuchItem() && familyMember.isAgeWithinRange(iType.getMemberRangeAge()))
				{
					List<GetSearchGlobalDataContentUserFamily> currentFamilyList = getPassengersListByType(iType);
					
					if (!isMemberContainsInFamilyList(currentFamilyList, familyMember))
					{
						familyMember.setFamilyMember(true);
						currentFamilyList.add(familyMember);
						break;
					}
				}
			}
		}
	}
	
	public void updateSelectedStatusInMainList()
	{
		mUserFamilyDelegate.updateSelectedStatusInMainList();
	}
	
	public void editFamilyList(boolean iIsShouldReset, eEditAction iEditAction)
	{
		List<GetSearchGlobalDataContentUserFamily> userFamily = mUserFamilyDelegate.editFamilyList(iIsShouldReset, iEditAction, new Pair<>(mCurrentFirstDateInSelection, mCurrentLastDateInSelection), mCurrentTypeOfTrip);
		sortListAccordingToAge(userFamily);
	}
	
	private boolean isMemberContainsInFamilyList(List<GetSearchGlobalDataContentUserFamily> iFamilyList, GetSearchGlobalDataContentUserFamily iFamilyMember)
	{
		boolean result = false;
		
		if (iFamilyList != null && !iFamilyList.isEmpty())
		{
			for (GetSearchGlobalDataContentUserFamily currentMember : iFamilyList)
			{
				if (currentMember.equals(iFamilyMember))
				{
					result = true;
				}
			}
		}
		
		return result;
	}
	
	public boolean isUserHimselfIs(ePassengersType iType)
	{
		boolean result = false;
		
		if (!mUserFamilyDelegate.isEmpty())
		{
			result = mUserFamilyDelegate.getFamilyListWithAgeEdition().get(USER_HIMSELF).isAgeWithinRange(iType.getMemberRangeAge());
		}
		
		return result;
	}
	
	public boolean isUserHimselfOlderThen(ePassengersType iType)
	{
		boolean result = false;
		
		if (mUserFamilyDelegate.getFamilyListWithAgeEdition().get(USER_HIMSELF).isEqualsOrOlderThan(iType.getMemberRangeAge().getMinAge()))
		{
			result = true;
		}
		
		return result;
	}
	
	public int getSelectedUserFamilyMembersQuantity()
	{
		int result = 0;
		
		for (GetSearchGlobalDataContentUserFamily iMember : getPassengersListByType(ePassengersType.FAMILY))
		{
			if (iMember.isSelectedForSearching())
			{
				++result;
			}
		}
		
		return result;
	}
	
	public int getFamilyMembersQuantity()
	{
		return getPassengersListByType(ePassengersType.FAMILY).size();
	}
	
	private List<GetSearchGlobalDataContentUserFamily> getSelectedFamilyMembersList()
	{
		List<GetSearchGlobalDataContentUserFamily> result = new ArrayList<>();
		
		for (GetSearchGlobalDataContentUserFamily iMember : getPassengersListByType(ePassengersType.FAMILY))
		{
			if (iMember.isSelectedForSearching())
			{
				result.add(iMember);
			}
		}
		
		return result;
	}
	
	public int getSelectedPassengersQuantity()
	{
		int result = 0;
		
		for (ArrayList<GetSearchGlobalDataContentUserFamily> currentList : getPassengersList().values())
		{
			for (GetSearchGlobalDataContentUserFamily iMember : currentList)
			{
				if (iMember.isSelectedForSearching())
				{
					++result;
				}
			}
		}
		
		return result;
	}
	
	@Override
	public int getSelectedPassengersQuantityByType(ePassengersType iType)
	{
		return getSelectedPassengersQuantityByType(iType, eFamilyRelation.ALL_KINDS);
	}
	
	private Map<ePassengersType, ArrayList<GetSearchGlobalDataContentUserFamily>> getPassengersList()
	{
		return mPassengersMap;
	}
	
	@Override
	public int getSelectedPassengersQuantityByType(ePassengersType iType, eFamilyRelation iFamilyRelation)
	{
		int result = 0;
		
		for (GetSearchGlobalDataContentUserFamily iMember : getPassengersList().get(iType))
		{
			if (iMember.isSelectedForSearching())
			{
				if (iFamilyRelation.isAllKinds())
				{
					++result;
				}
				else if (iFamilyRelation.isFamilyOnly())
				{
					if (iMember.isFamilyMember())
					{
						++result;
					}
				}
				else if (iFamilyRelation.isRegularOnly())
				{
					if (!iMember.isFamilyMember())
					{
						++result;
					}
				}
			}
		}
		
		return result;
	}
	
	public List<GetSearchGlobalDataContentFlightClasses> getFlightClassesList()
	{
		return mFlightClassesList;
	}
	
	public void setFlightClassesList(final ArrayList<GetSearchGlobalDataContentFlightClasses> iFlightClassesList)
	{
		if (iFlightClassesList != null && !iFlightClassesList.isEmpty())
		{
			mFlightClassesList = iFlightClassesList;
			mCurrentFlightClass = mFlightClassesList.get(0);
		}
	}
	
	@Override
	public GetSearchGlobalDataContentElalCities getCurrentOriginDestination()
	{
		return mCurrentOriginDestination;
	}
	
	public void setCurrentOriginDestination(final ISearchElement iCurrentOriginDestination)
	{
		if (iCurrentOriginDestination == null)
		{
			mCurrentOriginDestination = new GetSearchGlobalDataContentElalCities();
		}
		else if (iCurrentOriginDestination instanceof GetSearchGlobalDataContentElalCities)
		{
			mCurrentOriginDestination = (GetSearchGlobalDataContentElalCities) iCurrentOriginDestination;
		}
	}
	
	@Override
	public GetSearchGlobalDataContentElalCities getCurrentToDestination()
	{
		return mCurrentToDestination;
	}
	
	public void setCurrentToDestination(final ISearchElement iCurrentToDestination)
	{
		if (iCurrentToDestination == null)
		{
			mCurrentToDestination = new GetSearchGlobalDataContentElalCities();
		}
		else if (iCurrentToDestination instanceof GetSearchGlobalDataContentElalCities)
		{
			mCurrentToDestination = (GetSearchGlobalDataContentElalCities) iCurrentToDestination;
		}
	}
	
	public void setAdditionalCurrentToDestination(final ISearchElement iAdditionalCurrentToDestination)
	{
		if (iAdditionalCurrentToDestination == null)
		{
			mAdditionalCurrentToDestination = new GetSearchGlobalDataContentElalCities();
		}
		else if (iAdditionalCurrentToDestination instanceof GetSearchGlobalDataContentElalCities)
		{
			mAdditionalCurrentToDestination = (GetSearchGlobalDataContentElalCities) iAdditionalCurrentToDestination;
		}
	}
	
	@Override
	public GetSearchGlobalDataContentElalCities getAdditionalCurrentOriginDestination()
	{
		return mAdditionalCurrentOriginDestination;
	}
	
	public void setAdditionalCurrentOriginDestination(final ISearchElement iAdditionalCurrentToDestinationGlobalContent)
	{
		if (iAdditionalCurrentToDestinationGlobalContent instanceof GetSearchGlobalDataContentElalCities)
		{
			mAdditionalCurrentOriginDestination = (GetSearchGlobalDataContentElalCities) iAdditionalCurrentToDestinationGlobalContent;
		}
		else if (iAdditionalCurrentToDestinationGlobalContent == null)
		{
			mAdditionalCurrentOriginDestination = new GetSearchGlobalDataContentElalCities();
		}
	}
	
	@Override
	public GetSearchGlobalDataContentElalCities getAdditionalCurrentToDestination()
	{
		return mAdditionalCurrentToDestination;
	}
	
	public void bindByHistoryObject(ResponseGetSearchHistoryContent iHistoryContent)
	{
		if (iHistoryContent == null)
		{
			return;
		}
		
		cleanAllCurrentData();
		tryToSetSelectedForSearchingFamilyMembersIfNeed(iHistoryContent);
		
		//Here I need to set current Origin and To as well as additional destinations
		List<GetSearchGlobalDataContentElalCities> originDestinationList = getCityListFromListByCode(eListType.DEPARTURE_CITIES_LIST, iHistoryContent.getDepartureCityCode());
		
		if (!originDestinationList.isEmpty())
		{
			mCurrentOriginDestination = originDestinationList.get(0);
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, eLocalError.SomeProblemWithArray.getErrorMessage());
		}
		
		List<GetSearchGlobalDataContentElalCities> toDestinationList = getCityListFromListByCode(eListType.DESTINATION_CITIES_LIST, iHistoryContent.getArrivalCityCode());
		
		if (!toDestinationList.isEmpty())
		{
			mCurrentToDestination = toDestinationList.get(0);
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, eLocalError.SomeProblemWithArray.getErrorMessage());
		}
		
		mCurrentTypeOfTrip = eTypesOfTrip.getTypeById(iHistoryContent.getFlightSearchTypeID());
		
		if (mCurrentTypeOfTrip.isReturnFromAnotherDestination())
		{
			
			List<GetSearchGlobalDataContentElalCities> additionalOriginDestinationList = getCityListFromListByCode(eListType.DEPARTURE_CITIES_LIST, iHistoryContent.getAdditionalDepartureCityCode());
			
			if (!additionalOriginDestinationList.isEmpty())
			{
				mAdditionalCurrentOriginDestination = additionalOriginDestinationList.get(0);
			}
			else
			{
				AppUtils.printLog(Log.ERROR, TAG, eLocalError.SomeProblemWithArray.getErrorMessage());
			}
			
			List<GetSearchGlobalDataContentElalCities> additionalToDestinationList = getCityListFromListByCode(eListType.DEPARTURE_CITIES_LIST, iHistoryContent.getAdditionalArrivalCityCode());
			
			if (!additionalToDestinationList.isEmpty())
			{
				mAdditionalCurrentToDestination = additionalToDestinationList.get(0);
			}
			else
			{
				AppUtils.printLog(Log.ERROR, TAG, eLocalError.SomeProblemWithArray.getErrorMessage());
			}
		}
		
		mCurrentFirstDateInSelection = iHistoryContent.getDepartureDate();
		mCurrentLastDateInSelection = iHistoryContent.getArrivalDate();
		mCurrentFlightClass = getCurrentFlightClassById(iHistoryContent.getFlightClassID());
		
		addSeveralNotFamilyMemberToListByType(iHistoryContent.getAdults(), ePassengersType.ADULTS);
		addSeveralNotFamilyMemberToListByType(iHistoryContent.getChilds(), ePassengersType.CHILDREN);
		addSeveralNotFamilyMemberToListByType(iHistoryContent.getBabies(), ePassengersType.INFANTS);
		addSeveralNotFamilyMemberToListByType(iHistoryContent.getTeens(), ePassengersType.TEENAGERS);
		addSeveralNotFamilyMemberToListByType(iHistoryContent.getYoung(), ePassengersType.YOUTH);
		
		//Here I need to update family list age according to new first and last date in selection
		SearchObject.getInstance().editFamilyList(false, eEditAction.ADD_AGE);
	}
	
	private void tryToSetSelectedForSearchingFamilyMembersIfNeed(final ResponseGetSearchHistoryContent iHistoryContent)
	{
		ArrayList<ResponseGetSearchHistoryContentFamily> selectedForSearchingHistoryFamilyMembersList = iHistoryContent.getFamily();
		
		if (selectedForSearchingHistoryFamilyMembersList != null && !selectedForSearchingHistoryFamilyMembersList.isEmpty() && !mUserFamilyDelegate.getFamilyListWithAgeEdition().isEmpty())
		{
			for (ResponseGetSearchHistoryContentFamily familyHistoryMember : selectedForSearchingHistoryFamilyMembersList)
			{
				for (GetSearchGlobalDataContentUserFamily familyExistedMember : mUserFamilyDelegate.getFamilyListWithAgeEdition())
				{
					if (familyHistoryMember.getFamilyID() == familyExistedMember.getFamilyID())
					{
						familyExistedMember.setSelectedForSearching(true);
					}
				}
			}
		}
	}
	
	private GetSearchGlobalDataContentFlightClasses getCurrentFlightClassById(int iId)
	{
		GetSearchGlobalDataContentFlightClasses result = null;
		
		for (GetSearchGlobalDataContentFlightClasses currentFlightClassElement : mFlightClassesList)
		{
			if (currentFlightClassElement.getFlightClassID() == iId)
			{
				result = currentFlightClassElement;
			}
		}
		
		return result;
	}
	
	public List<GetSearchGlobalDataContentElalCities> getCityListFromListByCode(eListType iListType, eCities iCities)
	{
		return getCityListFromListByCode(iListType, iCities.name().toLowerCase());
	}
	
	public List<GetSearchGlobalDataContentElalCities> getCityListFromListByCode(eListType iListType, String iCityCode)
	{
		List<GetSearchGlobalDataContentElalCities> resultCity = new ArrayList<>();
		
		if (iListType != null && !TextUtils.isEmpty(iCityCode))
		{
			
			List<GetSearchGlobalDataContentElalCities> mCitiesList = new ArrayList<>();
			
			switch (iListType)
			{
				case DEPARTURE_CITIES_LIST:
				{
					mCitiesList = mElalDepartureCitiesList;
					break;
				}
				case DESTINATION_CITIES_LIST:
				{
					mCitiesList = mElalDestinationCitiesList;
					
					//Because there is such situation when mElalDestinationCitiesList.size() == 0, if user tried to click on departure field, but we get mElalDestinationCitiesList list only if user
					// click on arrival field
					if (mCitiesList.isEmpty())
					{
						mCitiesList = mElalDepartureCitiesList;
					}
					
					break;
				}
			}
			
			for (GetSearchGlobalDataContentElalCities currentCity : mCitiesList)
			{
				String cityCode = currentCity.getCityCode();
				
				if (cityCode != null && cityCode.toLowerCase().contains(iCityCode.toLowerCase()))
				{
					resultCity.add(currentCity);
				}
			}
		}
		
		return resultCity;
	}
	
	@Override
	public eTabStatement getCurrentTabStatement()
	{
		return mCurrentTabStatement;
	}
	
	public void setCurrentTabStatement(final eTabStatement iTabStatement)
	{
		mCurrentTabStatement = iTabStatement;
	}
	
	public eSearchDestinationType getCurrentDestinationType()
	{
		return mCurrentDestinationType;
	}
	
	public void setCurrentDestinationType(final eSearchDestinationType iCurrentDestinationType)
	{
		if (iCurrentDestinationType != null)
		{
			mCurrentDestinationType = iCurrentDestinationType;
		}
	}
	
	@Override
	public Date getCurrentFirstDateInSelection()
	{
		Calendar date = new GregorianCalendar();
		date.set(Calendar.HOUR_OF_DAY, 0);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		date.set(Calendar.MILLISECOND, 0);
		
		if (mCurrentFirstDateInSelection != null && mCurrentFirstDateInSelection.before(date.getTime()))
		{
			mCurrentFirstDateInSelection = null;
			mCurrentLastDateInSelection = null;
			AppUtils.printLog(Log.ERROR, TAG, eLocalError.SelectedDateLessThenCurrent.getErrorMessage());
		}
		
		return mCurrentFirstDateInSelection;
	}
	
	public void setCurrentFirstDateInSelection(final Date iCurrentFirstDateInSelection)
	{
		mCurrentFirstDateInSelection = iCurrentFirstDateInSelection;
	}
	
	@Override
	public Date getCurrentLastDateInSelection()
	{
		return mCurrentLastDateInSelection;
	}
	
	public void setCurrentLastDateInSelection(final Date iCurrentLastDateInSelection)
	{
		mCurrentLastDateInSelection = iCurrentLastDateInSelection;
	}
	
	private List<GetSearchGlobalDataContentUserFamily> getPassengersListByType(ePassengersType iType)
	{
		List<GetSearchGlobalDataContentUserFamily> result = null;
		
		if (iType != null)
		{
			if (iType.isFamily())
			{
				result = mUserFamilyDelegate.getFamilyListWithAgeEdition();
			}
			else
			{
				result = getPassengersList().get(iType);
			}
		}
		
		return result;
	}
	
	public void clearAllNotFamilyByType(ePassengersType iType)
	{
		int size = getPassengersListByType(iType).size();
		
		for (int i = 0 ; i < size ; i++)
		{
			deleteOneNotFamilyMemberFromListByType(iType);
		}
	}
	
	private void addSeveralNotFamilyMemberToListByType(int iQuantity, ePassengersType iType)
	{
		if (iQuantity > 0)
		{
			for (int i = 0 ; i < iQuantity ; i++)
			{
				addOneNotFamilySelectedMemberToListByType(iType);
			}
		}
	}
	
	public void addOneNotFamilySelectedMemberToListByType(ePassengersType iType)
	{
		GetSearchGlobalDataContentUserFamily newMember = new GetSearchGlobalDataContentUserFamily();
		newMember.setSelectedForSearching(true);
		List<GetSearchGlobalDataContentUserFamily> memberList = getPassengersListByType(iType);
		memberList.add(newMember);
	}
	
	public void deleteOneNotFamilyMemberFromListByType(ePassengersType iType)
	{
		List<GetSearchGlobalDataContentUserFamily> memberList = getPassengersListByType(iType);
		
		if (memberList != null && !memberList.isEmpty())
		{
			for (GetSearchGlobalDataContentUserFamily member : memberList)
			{
				if (!member.isFamilyMember())
				{
					memberList.remove(member);
					return;
				}
			}
		}
	}
	
	public List<IChoseActionElement> getTypeOfTripList()
	{
		return mTypeOfTripList;
	}
	
	public List<IChoseActionElement> getTypeOfTripListWithout(eTypesOfTrip... iTypes)
	{
		List<IChoseActionElement> typeOfTripList = new ArrayList<>(getTypeOfTripList());
		
		if (iTypes != null && iTypes.length > 0)
		{
			typeOfTripList.removeAll(Arrays.asList(iTypes));
		}
		
		return typeOfTripList;
	}
	
	@Override
	public eTypesOfTrip getCurrentTypeOfTrip()
	{
		return mCurrentTypeOfTrip;
	}
	
	public void setCurrentTypeOfTrip(final eTypesOfTrip iCurrentTypeOfTrip)
	{
		mCurrentTypeOfTrip = iCurrentTypeOfTrip;
	}
	
	public GetSearchGlobalDataContentFlightClasses getCurrentFlightClass()
	{
		return mCurrentFlightClass;
	}
	
	@Override
	public String getFlightClass()
	{
		return mCurrentFlightClass.getFlightClass();
	}
	
	public void setCurrentFlightClass(final IChoseActionElement iCurrentFlightClass)
	{
		if (iCurrentFlightClass != null && iCurrentFlightClass instanceof GetSearchGlobalDataContentFlightClasses)
		{
			mCurrentFlightClass = (GetSearchGlobalDataContentFlightClasses) iCurrentFlightClass;
		}
	}
	
	public String getSearchFlightUrl()
	{
		return new NetworkUtils().getSearchFlightUrl();
	}
	
	public String getSearchFlightUrlNoENC()
	{
		return new NetworkUtils().getSearchFlightUrlNoENC();
	}
	
	public String getSearchFlightParameters()
	{
		return new NetworkUtils().getSearchFlightDetails();
	}
	
	public RequestSearchFlight getRequestSearchFlight()
	{
		return new RequestSearchFlight(getSearchParams(), getFamiliesForSearchRequest());
	}
	
	@NonNull
	private ArrayList<RequestSearchFlightFamily> getFamiliesForSearchRequest()
	{
		ArrayList<RequestSearchFlightFamily> family = new ArrayList<>();
		for (GetSearchGlobalDataContentUserFamily selectedMember : getSelectedFamilyMembersList())
		{
			family.add(new RequestSearchFlightFamily(selectedMember.getUserID(), selectedMember.getFamilyID()));
		}
		return family;
	}
	
	@NonNull
	private ArrayList<RequestSearchFlightSearchParams> getSearchParams()
	{
		ArrayList<RequestSearchFlightSearchParams> searchParams = new ArrayList<>();
		
		int youthQuantity = getSelectedPassengersQuantityByType(ePassengersType.YOUTH, eFamilyRelation.REGULAR_ONLY);
		int childQuantity = getSelectedPassengersQuantityByType(ePassengersType.CHILDREN, eFamilyRelation.REGULAR_ONLY);
		int babiesQuantity = getSelectedPassengersQuantityByType(ePassengersType.INFANTS, eFamilyRelation.REGULAR_ONLY);
		int goldenAgeQuantity = 0;
		int teenagersQuantity = getSelectedPassengersQuantityByType(ePassengersType.TEENAGERS, eFamilyRelation.REGULAR_ONLY);
		int adultsQuantity = getSelectedPassengersQuantityByType(ePassengersType.ADULTS, eFamilyRelation.REGULAR_ONLY);
		int studentsQuantity = 0;
		String departureCityCode = mCurrentOriginDestination.getCityCode();
		String arrivalCityCode = mCurrentToDestination.getCityCode();
		Date departureDate = mCurrentFirstDateInSelection;
		Date arrivalDate = mCurrentLastDateInSelection;
		int flightSearchTypeID = mCurrentTypeOfTrip.getTypeOfTripId();
		int flightClassID = mCurrentFlightClass.getFlightClassID();
		int userId = UserData.getInstance().getUserID();
		
		RequestSearchFlightSearchParams requestSearchFlightSearchParams = new RequestSearchFlightSearchParams(youthQuantity, childQuantity, babiesQuantity, goldenAgeQuantity, teenagersQuantity, adultsQuantity, studentsQuantity, departureCityCode, arrivalCityCode, departureDate, arrivalDate, flightSearchTypeID, flightClassID, userId);
		
		searchParams.add(requestSearchFlightSearchParams);
		
		if (flightSearchTypeID == eTypesOfTrip.RETURN_FROM_ANOTHER_DESTINATION.getTypeOfTripId())
		{
			//Here you just need to change place of {@param arrivalCityCode, departureCityCode}
			String additionalDepartureCityCode = mAdditionalCurrentOriginDestination.getCityCode();
			String additionalArrivalCityCode = mAdditionalCurrentToDestination.getCityCode();
			RequestSearchFlightSearchParams requestAdditionalSearchFlightSearchParams = new RequestSearchFlightSearchParams(youthQuantity, childQuantity, babiesQuantity, goldenAgeQuantity, teenagersQuantity, adultsQuantity, studentsQuantity, additionalDepartureCityCode, additionalArrivalCityCode, departureDate, arrivalDate, flightSearchTypeID, flightClassID, userId);
			
			searchParams.add(requestAdditionalSearchFlightSearchParams);
		}
		
		return searchParams;
	}
	
	@Override
	public Collection<GetSearchGlobalDataContentUserFamily> getPassengers()
	{
		return mUserFamilyDelegate;
	}
	
	public void resetUserFamilyList()
	{
		List<GetSearchGlobalDataContentUserFamily> familyList = getUserFamilyList();
		for (GetSearchGlobalDataContentUserFamily familyMember : familyList)
		{
			familyMember.setSelectedForSearching(false);
		}
		
		mUserFamilyDelegate.setUserFamilyList(familyList);
		
		//it is User himself
		mUserFamilyDelegate.get(USER_HIMSELF).setSelectedForSearching(true);
		mUserFamilyDelegate.get(USER_HIMSELF).setUserHimself(true);
		
		mUserFamilyDelegate.updateAgeFamilyList();
		
		sortListAccordingToAge(mUserFamilyDelegate.getFamilyListWithAgeEdition());
	}
	
	public void resetPassengersList()
	{
		getPassengersList().clear();
	}
	
	public static class AgeRange
	{
		private int mMinAge;
		private int mMaxAge;
		
		public AgeRange(final int iMinAge, final int iMaxAge)
		{
			mMinAge = iMinAge;
			mMaxAge = iMaxAge;
		}
		
		public int getMinAge()
		{
			return mMinAge;
		}
		
		public int getMaxAge()
		{
			return mMaxAge;
		}
	}
	
	public boolean isCurrentDatesSelected()
	{
		return mIsCurrentDateSelected;
	}
	
	public void setCurrentDateSelected(final boolean iCurrentDateSelected)
	{
		mIsCurrentDateSelected = iCurrentDateSelected;
	}
	
	public ResponseGetSearchGlobalData getResponseSearchGlobalData()
	{
		return mResponseSearchGlobalData;
	}
	
	public void setResponseSearchGlobalData(final ResponseGetSearchGlobalData iResponseSearchGlobalData)
	{
		mResponseSearchGlobalData = iResponseSearchGlobalData;
	}
	
	public boolean isOriginDestinationTLV()
	{
		return mCurrentOriginDestination.getCityCode().toLowerCase().contains(eCities.TLV.name().toLowerCase());
	}
	
	public boolean isToDestinationTLV()
	{
		return mCurrentToDestination.getCityCode().toLowerCase().contains(eCities.TLV.name().toLowerCase());
	}
}
