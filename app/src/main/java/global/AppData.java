package global;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import il.co.ewave.elal.R;
import utils.global.enums.eCountries;
import utils.global.enums.eSideMenuItems;
import webServices.global.RequestStringBuilder;
import webServices.responses.ResponseGetAppData;
import webServices.responses.ResponseGetSideMenuItems;
import webServices.responses.ResponseGetUnreadNotificationsBadge;
import webServices.responses.getAppData.FamilyType;
import webServices.responses.getAppData.Language;
import webServices.responses.getAppData.Representation;
import webServices.responses.getHomeScreenContent.HomeScreenContent;
import webServices.responses.getMobileCountryCodes.MobileCountryCode;
import webServices.responses.getMobileCountryCodes.ResponseGetMobileCountryCodes;
import webServices.responses.getSideMenuItems.SideMenuCategory;
import webServices.responses.getSideMenuItems.SideMenuItem;

/**
 * Created with care by Shahar Ben-Moshe on 04/01/17.
 */

public class AppData
{
	private static final AppData mInstance = new AppData();
	
	private ArrayList<Representation> mRepresentations=new ArrayList<>();
	private ArrayList<FamilyType> mFamilyTypes = new ArrayList<>();
	private ArrayList<Language> mLanguages=new ArrayList<>();
	private ArrayList<SideMenuCategory> mSideMenuCategories;
	private ArrayList<MobileCountryCode> mMobileCountryCodes=new ArrayList<>();
	private int mUnReadPushes;
	private HomeScreenContent mHomeItems;
	private int mModuleId;
	
	public static AppData getInstance()
	{
		return mInstance;
	}
	
	private AppData()
	{
	}
	
	public void setAppData(final ResponseGetAppData iResponseGetAppData)
	{
		if (iResponseGetAppData != null && iResponseGetAppData.getContent() != null)
		{
			mRepresentations = iResponseGetAppData.getContent().getRepresentations();
			mFamilyTypes = iResponseGetAppData.getContent().getFamilyTypes();
			mLanguages = iResponseGetAppData.getContent().getLanguages();
		}
	}
	
	public ArrayList<Representation> getRepresentations()
	{
		return mRepresentations;
	}
	
	public ArrayList<FamilyType> getFamilyTypes()
	{
		return mFamilyTypes;
	}
	
	public ArrayList<Language> getLanguages()
	{
		return mLanguages;
	}
	
	public void setUnReadPushes(ResponseGetUnreadNotificationsBadge iUnReadPushes)
	{
		if (iUnReadPushes != null && iUnReadPushes.getContent() != null)
		{
			mUnReadPushes = iUnReadPushes.getContent().getUnReadPushes();
		}
	}
	
	public int getRepresentationPositionByCity(eCountries iCity)
	{
		int result = -1;
		
		if (mRepresentations != null && !mRepresentations.isEmpty())
		{
			for (int i = 0 ; i < mRepresentations.size() ; i++)
			{
				Representation representation = mRepresentations.get(i);
				
				if (representation.getCountryCode().toLowerCase().equals(iCity.getCountryCode().toLowerCase()))
				{
					result = representation.getRepresentationID();
					break;
				}
			}
		}
		
		return result;
	}
	
	public int getIsraelRepresentationPosition()
	{
		int result = 0;
		
		if (mRepresentations != null && !mRepresentations.isEmpty())
		{
			for (int i = 0 ; i < mRepresentations.size() ; i++)
			{
				Representation representation = mRepresentations.get(i);
				
				if (representation.getCountryCode().equalsIgnoreCase("IL"))
				{
					result = i;
				return result;
				}
			}
		}
		
		return result;
	}
	
	
	public int getUnReadPushes()
	{
		return mUnReadPushes;
	}
	
	public ArrayList<SideMenuCategory> getSideMenuCategories()
	{
		return mSideMenuCategories;
	}
	
	public void setSideMenuData(final ResponseGetSideMenuItems iResponseGetSideMenuItems)
	{
		if (iResponseGetSideMenuItems != null && iResponseGetSideMenuItems.getContent() != null)
		{
			mSideMenuCategories = iResponseGetSideMenuItems.getContent().getSideMenuItems();
		}
	}
	
	public ArrayList<SideMenuCategory> getPermanentSideMenuCategories(Context context)
	{
		//		Context context = ElAlApplication.getInstance();
		ArrayList<SideMenuCategory> result = new ArrayList<>();
		
		//My Account category and subcategories
		ArrayList<SideMenuItem> myAccountSideMenuItems = new ArrayList<>();
		myAccountSideMenuItems.add(new SideMenuItem(eInAppNavigation.UserInfo, context.getString(R.string.my_details)));
		myAccountSideMenuItems.add(new SideMenuItem(eInAppNavigation.FamilyScreen, context.getString(R.string.update_family_account)));
		myAccountSideMenuItems.add(new SideMenuItem(eInAppNavigation.ChangePassword, context.getString(R.string.change_password), RequestStringBuilder.getChangePasswordUrlByLanguage()));
		myAccountSideMenuItems.add(new SideMenuItem(eInAppNavigation.MyDocuments, context.getString(R.string.my_documents)));
		
		if (UserData.getInstance().getUserObject() != null && UserData.getInstance().getUserObject().isMatmid())
		{
			myAccountSideMenuItems.add(new SideMenuItem(eInAppNavigation.WebView, context.getString(R.string.account_status), RequestStringBuilder.getAccountStatusUrlByLanguage()));
			myAccountSideMenuItems.add(new SideMenuItem(eInAppNavigation.WebView, context.getString(R.string.adding_missing_flight), RequestStringBuilder.getAddingMissingFlightUrlByLanguage()));
		}
		
		SideMenuCategory myAccountSideMenuCategory = new SideMenuCategory(myAccountSideMenuItems, context.getString(eSideMenuItems.MY_ACCOUNT.getNameOfItemResourceId()), R.mipmap.menu_icon3);
		
		//My Flight category and subcategories
		ArrayList<SideMenuItem> myFlightSideMenuItems = new ArrayList<>();
		myFlightSideMenuItems.add(new SideMenuItem(eInAppNavigation.MyFlights, context.getString(R.string.manage_my_flight)));
		myFlightSideMenuItems.add(new SideMenuItem(eInAppNavigation.AddNewFlightFragment, context.getString(R.string.add_flight)));
		myFlightSideMenuItems.add(new SideMenuItem(eInAppNavigation.WebView, context.getString(R.string.check_in), RequestStringBuilder.getCheckInUrlByLanguage()));
		myFlightSideMenuItems.add(new SideMenuItem(eInAppNavigation.WebView, context.getString(R.string.cancel_check_in_underline), RequestStringBuilder.getCancelCheckInUrlByLanguage()));
		
		SideMenuCategory myFlightSideMenuCategory = new SideMenuCategory(myFlightSideMenuItems, context.getString(eSideMenuItems.MY_FLIGHT.getNameOfItemResourceId()), -1);
		
		result.add(myAccountSideMenuCategory);
		result.add(myFlightSideMenuCategory);
		
		return result;
	}
	
	public void setHomeItems(final HomeScreenContent iHomeItems)
	{
		mHomeItems = iHomeItems;
	}
	
	public HomeScreenContent getHomeItems()
	{
		return mHomeItems;
	}
	
	
	public int getModuleId()
	{
		return mModuleId;
	}
	
	public void setModuleId(final int iModuleId)
	{
		mModuleId = iModuleId;
	}
	
	public void saveCountryCodes(ArrayList<MobileCountryCode> iResponseCountryCodes)
	{
		mMobileCountryCodes = iResponseCountryCodes;
	}
	
	public ArrayList<MobileCountryCode> getMobileCountryCodes()
	{
		return mMobileCountryCodes;
	}
}
