package webServices.responses.responseGetPNRFlights;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

import utils.global.DateTimeUtils;

public class GetPNRFlightsContent implements Parcelable
{
	@JsonProperty("Destination")
	private String mDestination;
	@JsonProperty("FlightDuration")
	private String mFlightDuration;
	@JsonProperty("ClassOfService")
	private String mClassOfService;
	@JsonProperty("Origination")
	private String mOrigination;
	@JsonProperty("Direction")
	private String mDirection;
	@JsonProperty("CreateDate")
	private Date mCreateDate;
	@JsonProperty("Segment")
	private int mSegment;
	@JsonProperty("LastModified")
	private String mLastModified;
	@JsonProperty("DepartTerminal")
	private String mDepartTerminal;
	@JsonProperty("FlightStatus")
	private String mFlightStatus;
	@JsonProperty("ReferenceId")
	private String mReferenceId;
	@JsonProperty("PNR")
	private String mPNR;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeUtils.DATE_FORMAT_yyyy_MM_ddHHmmSS)
	@JsonProperty("ArrivalDate")
	private Date mArrivalDate;
	@JsonProperty("FlightNumber")
	private String mFlightNumber;
	@JsonProperty("FlightCompany")
	private String mFlightCompany;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeUtils.DATE_FORMAT_yyyy_MM_ddHHmmSS)
	@JsonProperty("DepartureDate")
	private Date mDepartureDate;
	@JsonProperty("Carrier")
	private String mCarrier;
	@JsonProperty("AircraftType")
	private String mAircraftType;
	@JsonProperty("IsActive")
	private boolean mIsActive;
	
	
	public GetPNRFlightsContent()
	{
	}
	
	
	public static final Creator<GetPNRFlightsContent> CREATOR = new Creator<GetPNRFlightsContent>()
	{
		@Override
		public GetPNRFlightsContent createFromParcel(Parcel in)
		{
			return new GetPNRFlightsContent(in);
		}
		
		@Override
		public GetPNRFlightsContent[] newArray(int size)
		{
			return new GetPNRFlightsContent[size];
		}
	};
	
	public String getDestination()
	{
		return !TextUtils.isEmpty(this.mDestination) ? this.mDestination : "";
	}
	
	public String getFlightDuration()
	{
		return !TextUtils.isEmpty(this.mFlightDuration) ? this.mFlightDuration : "";
//		return this.mFlightDuration;
	}
	
	public String getClassOfService()
	{
		return this.mClassOfService;
	}
	
	public String getOrigination()
	{
		return !TextUtils.isEmpty(this.mOrigination) ? this.mOrigination : "";
	}
	
	public String getDirection()
	{
		return this.mDirection;
	}
	
	public Date getCreateDate()
	{
		return this.mCreateDate;
	}
	
	public int getSegment()
	{
		return this.mSegment;
	}
	
	public String getLastModified()
	{
		return this.mLastModified;
	}
	
	public String getDepartTerminal()
	{
		return this.mDepartTerminal;
	}
	
	public String getFlightStatus()
	{
		return this.mFlightStatus;
	}
	
	public String getReferenceId()
	{
		return this.mReferenceId;
	}
	
	public String getPNR()
	{
		return this.mPNR;
	}
	
	public Date getArrivalDate()
	{
		return this.mArrivalDate;
	}
	
	public String getFlightNumber()
	{
		return !TextUtils.isEmpty(mFlightNumber) ? mFlightNumber : "";
//		return this.mFlightNumber;
	}
	
	public String getFlightCompany()
	{
		return this.mFlightCompany;
	}
	
	public Date getDepartureDate()
	{
		return this.mDepartureDate;
	}
	
	public String getCarrier()
	{
		return this.mCarrier;
	}
	
	public String getAircraftType()
	{
		return this.mAircraftType;
	}
	
	public boolean ismIsActive()
	{
		return mIsActive;
	}
	
	
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeString(this.mDestination);
		dest.writeString(this.mFlightDuration);
		dest.writeString(this.mClassOfService);
		dest.writeString(this.mOrigination);
		dest.writeString(this.mDirection);
		dest.writeLong(this.mCreateDate != null ? this.mCreateDate.getTime() : -1);
		dest.writeLong(this.mArrivalDate != null ? this.mArrivalDate.getTime() : -1);
		dest.writeLong(this.mDepartureDate != null ? this.mDepartureDate.getTime() : -1);
		dest.writeInt(this.mSegment);
		dest.writeString(this.mLastModified);
		dest.writeString(this.mDepartTerminal);
		dest.writeString(this.mFlightStatus);
		dest.writeString(this.mReferenceId);
		dest.writeString(this.mPNR);
		dest.writeString(this.mFlightNumber);
		dest.writeString(this.mFlightCompany);
		dest.writeString(this.mCarrier);
		dest.writeString(this.mAircraftType);
		dest.writeByte((byte) (mIsActive ? 1 : 0));
	}
	
	private GetPNRFlightsContent(Parcel in)
	{
		this.mDestination = in.readString();
		this.mFlightDuration = in.readString();
		this.mClassOfService = in.readString();
		this.mOrigination = in.readString();
		this.mDirection = in.readString();
		long tmpCreateDate = in.readLong();
		this.mCreateDate = tmpCreateDate == -1 ? null : new Date(tmpCreateDate);
		this.mFlightNumber = in.readString();
		this.mPNR = in.readString();
		this.mOrigination = in.readString();
		this.mDestination = in.readString();
		long tmpMDepartureDate = in.readLong();
		this.mDepartureDate = tmpMDepartureDate == -1 ? null : new Date(tmpMDepartureDate);
		long tmpMArrivalDate = in.readLong();
		this.mArrivalDate = tmpMArrivalDate == -1 ? null : new Date(tmpMArrivalDate);
		this.mFlightStatus = in.readString();
		this.mFlightDuration = in.readString();
		this.mReferenceId = in.readString();
		this.mFlightCompany = in.readString();
		this.mAircraftType = in.readString();
		this.mDepartTerminal = in.readString();
		this.mSegment = in.readInt();
		this.mCarrier = in.readString();
		this.mDirection = in.readString();
		this.mClassOfService = in.readString();
		this.mLastModified = in.readString();
		this.mIsActive = in.readByte() != 0;
	}
	
	
}
