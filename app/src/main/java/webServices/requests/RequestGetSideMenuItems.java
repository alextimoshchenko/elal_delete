package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.UserData;

public class RequestGetSideMenuItems
{
	@JsonProperty("UserID")
	private int mUserID;
	
	@JsonProperty("LanguageCode")
	private String mLanguageCode = UserData.getInstance().getLanguage().getLanguageCode();
	
	@JsonProperty("SessionID")
	private String mSessionID;
	@JsonProperty("MemberID")
	private String mMemberID;
	
//	public RequestGetSideMenuItems(int iUserID)
//	{
//		mUserID = iUserID;
//	}
	
	public RequestGetSideMenuItems(final int iUserID, final String iSessionID, final String iMemberID)
	{
		mUserID = iUserID;
		mSessionID = iSessionID;
		mMemberID = iMemberID;
	}
}
