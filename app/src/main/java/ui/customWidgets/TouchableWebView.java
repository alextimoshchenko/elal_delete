package ui.customWidgets;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.webkit.WebView;

/**
 * Created with care by Shahar Ben-Moshe on 02/03/17.
 *
 * This allows to intercept touch events inside a {@link android.widget.ScrollView ScrollView}
 */
public class TouchableWebView extends WebView
{
	public TouchableWebView(final Context context)
	{
		super(context);
	}
	
	public TouchableWebView(final Context context, final AttributeSet attrs)
	{
		super(context, attrs);
	}
	
	public TouchableWebView(final Context context, final AttributeSet attrs, final int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
	}
	
	@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
	public TouchableWebView(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes)
	{
		super(context, attrs, defStyleAttr, defStyleRes);
	}
	
	public TouchableWebView(final Context context, final AttributeSet attrs, final int defStyleAttr, final boolean privateBrowsing)
	{
		super(context, attrs, defStyleAttr, privateBrowsing);
	}
	
	
	@Override
	public boolean onTouchEvent(MotionEvent ev)
	{
		requestDisallowInterceptTouchEvent(true);
		switch (ev.getAction())
		{
			case MotionEvent.ACTION_DOWN:
			case MotionEvent.ACTION_UP:
				if (!hasFocus())
					requestFocus();
				break;
		}
		
		return super.onTouchEvent(ev);
	}
	
	@Override
	protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect)
	{
		super.onFocusChanged(true, direction, previouslyFocusedRect);
	}
	
	@Override
	public boolean onCheckIsTextEditor()
	{
		return true;
	}
	
	
}
