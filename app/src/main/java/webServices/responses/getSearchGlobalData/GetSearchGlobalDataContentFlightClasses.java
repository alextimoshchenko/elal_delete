package webServices.responses.getSearchGlobalData;

import android.content.Context;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import interfaces.IChoseActionElement;

public class GetSearchGlobalDataContentFlightClasses implements IChoseActionElement
{
	@JsonProperty("FlightClassName")
	private String mFlightClassName;
	@JsonProperty("FlightClassNameEng")
	private String mFlightClassNameEng;
	@JsonProperty("FlightClassID")
	private int mFlightClassID;
	@JsonProperty("FlightClass")
	private String mFlightClass;
	@JsonProperty("FlightClassNameHeb")
	private String mFlightClassNameHeb;
	
	public String getFlightClassName()
	{
		return this.mFlightClassName;
	}
	
	public String getFlightClassNameEng()
	{
		return this.mFlightClassNameEng;
	}
	
	public int getFlightClassID()
	{
		return this.mFlightClassID;
	}
	
	public String getFlightClassNameHeb()
	{
		return this.mFlightClassNameHeb;
	}
	
	public String getFlightClass()
	{
		return mFlightClass;
	}
	
	public GetSearchGlobalDataContentFlightClasses()
	{
	}
	
	@Override
	public String getElementName(final Context iContext)
	{
		return mFlightClassName;
	}
	
	@JsonIgnore
	@Override
	public String getElementName()
	{
		return mFlightClassName;
	}
	
	@JsonIgnore
	@Override
	public int getElementId()
	{
		return mFlightClassID;
	}
}
