package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.UserData;

public class RequestPnrByNum
{
	@JsonProperty("UserId")
	private int mUserId;
	
	@JsonProperty("PNRNum")
	private String mPNRNum;
	
	@JsonProperty("FlightNumber")
	private String mFlightNumber;
	
	
	@JsonProperty("LanguageCode")
	private String mLanguageCode = UserData.getInstance().getLanguage().getLanguageCode();
	
	public RequestPnrByNum(int iUserId, String iPNRNum, String iFlightNumber)
	{
		mUserId = iUserId;
		mPNRNum = iPNRNum;
		mFlightNumber = iFlightNumber;
	}
	
	public RequestPnrByNum()
	{
	}
}
