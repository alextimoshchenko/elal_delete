package ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import il.co.ewave.elal.R;
import webServices.responses.responseGetUserActivePNRs.Passenger;

/**
 * Created by erline.katz on 31/08/2017.
 */

public class FlightOrderPassengersAdapter extends RecyclerView.Adapter<FlightOrderPassengersAdapter.ViewHolder>
{
	private ArrayList<Passenger> mPassengers;
	private String mPnr;
	private Context mContext;
	
	public FlightOrderPassengersAdapter(final ArrayList<Passenger> iPassengers, final String iPnr, Context iContext)
	{
		mPassengers = iPassengers;
		mPnr = iPnr;
		mContext = iContext;
	}
	
	@Override
	public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType)
	{
		return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.order_details_passenger_item_cell_layout, parent, false));
	}
	
	@Override
	public void onBindViewHolder(final ViewHolder iViewHolder, final int iPosition)
	{
		final Passenger currentPassenger = mPassengers.get(iPosition);
		
		if (currentPassenger != null)
		{
			iViewHolder.mTvPassengerName.setText(currentPassenger.getTitle() + " " + currentPassenger.getFirstName() + " " + currentPassenger.getLastName());
//			iViewHolder.mTvMatmidNumber.setText("?");
			iViewHolder.mTvTicketNumber.setText(currentPassenger.getTicketNumber());
			iViewHolder.mTvReservationNumber.setText(mPnr);
			
			
			iViewHolder.mLilContainer.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					if (iViewHolder.mLilPassengerDataContainer.getVisibility() == View.VISIBLE)
					{
						iViewHolder.mLilPassengerDataContainer.setVisibility(View.GONE);
						iViewHolder.mArrow.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.open_w));
					}
					else
					{
						iViewHolder.mLilPassengerDataContainer.setVisibility(View.VISIBLE);
						iViewHolder.mArrow.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.close_w));
					}
				}
			});
			
			//			iViewHolder.mTvTicketNumber.setText(currentPassenger.getTicketNumber());
			//			iViewHolder.mTvReservationCode.setText(AppUtils.nullSafeCheckString(mPnr));
			//
			//			if (currentPassenger.isExpanded())
			//			{
			//				iViewHolder.mLlExpand.setVisibility(View.VISIBLE);
			//				iViewHolder.mTvExpandArrow.setImageResource(R.mipmap.arrow_down_white);
			//			}
			//			else
			//			{
			//				iViewHolder.mLlExpand.setVisibility(View.GONE);
			//				iViewHolder.mTvExpandArrow.setImageResource(/*AppUtils.isDefaultLocaleRTL() ? R.mipmap.arrow_left_white_full : */R.mipmap.arrow_right_white_full);
			//			}
			//
			//			String matmidNumber = UserData.getInstance().getUserObject().getMatmidMemberID();
			//			iViewHolder.mTvMatmidNumber.setText(AppUtils.nullSafeCheckString(matmidNumber));
			//			iViewHolder.mLlMatmidNumber.setVisibility(TextUtils.isEmpty(matmidNumber) ? View.GONE : View.VISIBLE);
			//
			//			iViewHolder.mBtnExpand.setOnClickListener(new View.OnClickListener()
			//			{
			//				@Override
			//				public void onClick(final View v)
			//				{
			//					currentPassenger.reverseExpand();
			//					notifyItemChanged(iViewHolder.getAdapterPosition());
			//				}
			//			});
		}
	}
	
	
	@Override
	public int getItemCount()
	{
		return mPassengers == null ? 0 : mPassengers.size();
	}
	
	class ViewHolder extends RecyclerView.ViewHolder
	{
		TextView mTvPassengerName, mTvMatmidNumber, mTvTicketNumber, mTvReservationNumber;
		LinearLayout mLilContainer, mLilPassengerDataContainer;
		ImageView mArrow;
		
		ViewHolder(final View itemView)
		{
			super(itemView);
			
			mTvPassengerName = (TextView) itemView.findViewById(R.id.tv_orderdeatils_PassengerName);
			mTvMatmidNumber = (TextView) itemView.findViewById(R.id.tv_orderDetails_MatmidNumber);
			mTvTicketNumber = (TextView) itemView.findViewById(R.id.tv_orderDetails_TicketNumber);
			mTvReservationNumber = (TextView) itemView.findViewById(R.id.tv_orderDetails_ReservationNumber);
			mLilContainer = (LinearLayout) itemView.findViewById(R.id.lilContainer);
			mLilPassengerDataContainer = (LinearLayout) itemView.findViewById(R.id.lil_orderDetails_PassengerDataContainer);
			
			mArrow = (ImageView) itemView.findViewById(R.id.iv_orderDeatails_Arrow);
			
		}
	}
}
