package ui.activities;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import il.co.ewave.elal.R;
import utils.errors.ErrorsHandler;
import utils.errors.ServerError;
import webServices.responses.ResponseResetPassword;

/**
 * Created by Avishay.Peretz on 18/07/2017.
 */

public class ForgotPasswordActivity extends EWBaseUserActivity implements Response.Listener<ResponseResetPassword>, Response.ErrorListener
{
//	private View mLlToolbar;
//	private EditText mEtEmail;
//	private TextView mTvInvalidEmailMessage;
//	private TextView mTvSuccessMessage;
//	private Button mTvContinue;
//	private Button mTvConfirm;
	
	@Override
	protected void initScreen()
	{
		setContentView(R.layout.activity_forgot_password);
		super.initScreen();
		
//		init();
//		initToolbar();
//		setListeners();
//		setShowSuccessMessage(false);
	}
	
	@Override
	protected void setCurrentActivityModuleId()
	{
		
	}
	
	
	private void initReference()
	{
//		mLlToolbar =   findViewById(R.id.ll_forgotPassword_Toolbar);
//		mEtEmail = (EditText) findViewById(R.id.et_forgotPassword_Email);
//		mTvInvalidEmailMessage = (TextView) findViewById(R.id.tv_forgotPassword_InvalidEmail);
//		mTvSuccessMessage = (TextView) findViewById(R.id.tv_forgotPassword_SuccessMessage);
//		mTvContinue = (Button) findViewById(R.id.tv_forgotPassword_Continue);
//		mTvConfirm = (Button) findViewById(R.id.tv_forgotPassword_Confirm);
	}
	
	private void initToolbar()
	{
//		if (mLlToolbar != null)
//		{
//			mLlToolbar.findViewById(R.id.img_actionBar_drawer).setVisibility(View.GONE);
////			mLlToolbar.findViewById(R.id.ll_actionBar_flightSchedule).setVisibility(View.GONE);
//			((ImageView) mLlToolbar.findViewById(R.id.img_actionBar_logo)).setImageResource(UserData.getInstance().getLanguage() == eLanguage.Hebrew ? R.mipmap.elal_logo_he : R.mipmap.elal_logo_en);
//		}
	}
	
	private void setListeners()
	{
//		mTvContinue.setOnClickListener(new View.OnClickListener()
//		{
//			@Override
//			public void onClick(final View v)
//			{
//				doOnContinueClick();
//			}
//		});
//
//		mTvConfirm.setOnClickListener(new View.OnClickListener()
//		{
//			@Override
//			public void onClick(final View iView)
//			{
//				doOnConfirmClick();
//			}
//		});
	}
	
//	@Override
//	protected void handleLogoutResponse()
//	{
//
//	}
	
	//	private void doOnContinueClick()
//	{
//		if (isDataValidAndShowErrorMessageIfNeeded())
//		{
//			if (isServiceConnected())
//			{
//				setProgressDialog(true);
//				getService().getController(LoginController.class).ResetPassword(new RequestResetPassword(mEtEmail.getText().toString()), this, this);
//			}
//		}
//	}
	
//	private boolean isDataValidAndShowErrorMessageIfNeeded()
//	{
//		boolean result = false;
//
//		if (TextUtils.isEmpty(mEtEmail.getText().toString()))
//		{
//			setErrorMessage(getString(R.string.empty_email_address));
//		}
//		else if (!AppUtils.isEmailValid(mEtEmail.getText().toString()))
//		{
//			setErrorMessage(getString(R.string.invalid_email_address));
//		}
//		else
//		{
//			setErrorMessage("");
//			result = true;
//		}
//
//		setShowSuccessMessage(false);
//
//		return result;
//	}
	
//	private void setErrorMessage(final String iTextToShow)
//	{
//		mTvInvalidEmailMessage.setText(iTextToShow == null ? "" : iTextToShow);
//	}
	
//	private void setShowSuccessMessage(final boolean iShowSuccessMessage)
//	{
//		mTvSuccessMessage.setVisibility(iShowSuccessMessage ? View.VISIBLE : View.INVISIBLE);
//	}
	
	private void doOnConfirmClick()
	{
		finish();
	}
	
	@Override
	protected void onServiceConnected()
	{
		initDrawer();
	}
	
	@Override
	public void onResponse(final ResponseResetPassword iResponse)
	{
//		onSuccessResponse();
	}
	
//	private void onSuccessResponse()
//	{
//		mEtEmail.setEnabled(false);
//		mTvContinue.setVisibility(View.GONE);
//		setShowSuccessMessage(true);
//		mTvConfirm.setVisibility(View.VISIBLE);
//
//		setProgressDialog(false);
//	}
	
	@Override
	public void onErrorResponse(final VolleyError iError)
	{
		if (iError != null && iError instanceof ServerError && ((ServerError) iError).getErrorCode() == ServerError.eServerError.MobileUserNotFound.getErrorCode())
		{
			iError.printStackTrace();
//			onSuccessResponse();
		}
		else
		{
			ErrorsHandler.tryShowServiceErrorDialog(iError, this);
		}
	}
}
