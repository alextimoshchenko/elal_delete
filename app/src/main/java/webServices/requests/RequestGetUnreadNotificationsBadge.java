package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with care by Alexey.T on 21/08/2017.
 */
public class RequestGetUnreadNotificationsBadge
{
	@JsonProperty("UserID")
	private int mUserID;
	
	public RequestGetUnreadNotificationsBadge(final int iUserID)
	{
		mUserID = iUserID;
	}
	
	public RequestGetUnreadNotificationsBadge()
	{
	}
}
