package webServices.responses.deleteUserPushMessage;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hannesdorfmann.adapterdelegates3.AbsListItemAdapterDelegate;

import java.util.List;

import global.UserData;
import il.co.ewave.elal.R;
import interfaces.INotificationClickListener;
import ui.customWidgets.ChangeDirectionLinearLayout;
import ui.customWidgets.swipeLayout.SwipeLayout;
import utils.global.AppUtils;

/**
 * Created with care by Alexey.T on 06/09/2017.
 */
public class NotificationInstanceDelegate extends AbsListItemAdapterDelegate<NotificationInstance, UserMessage, NotificationInstanceDelegate.ViewHolder>
{
	private LayoutInflater mInflater;
	private Context mContext;
	private final float MAX_WIDTH_OF_BOTTOM_VIEW = 74;
	private boolean mIsEnglishLanguage;
	private INotificationClickListener mListener;
	
	public NotificationInstanceDelegate(Activity iActivity, INotificationClickListener iListener)
	{
		mContext = iActivity;
		mInflater = iActivity.getLayoutInflater();
		mIsEnglishLanguage = UserData.getInstance().getLanguage().isEnglish();
		mListener = iListener;
	}
	
	@Override
	protected boolean isForViewType(@NonNull final UserMessage item, @NonNull final List<UserMessage> items, final int position)
	{
		return item instanceof NotificationInstance;
	}
	
	@NonNull
	@Override
	protected ViewHolder onCreateViewHolder(@NonNull final ViewGroup iParent)
	{
		return new NotificationInstanceDelegate.ViewHolder(mInflater.inflate(R.layout.notification_item_layout, iParent, false));
	}
	
	@Override
	protected void onBindViewHolder(@NonNull final NotificationInstance iCurrentNotificationInstance, @NonNull final ViewHolder iVH, @NonNull final List<Object> payloads)
	{
		iVH.bind(iCurrentNotificationInstance);
	}
	
	private void doOnDeleteClick(NotificationInstance iNotificationInstance)
	{
		mListener.onDelete(iNotificationInstance);
	}
	
	private void doOnSurfaceClick(NotificationInstance iNotificationInstance)
	{
		mListener.onItemClick(iNotificationInstance);
	}
	
	class ViewHolder extends RecyclerView.ViewHolder
	{
		SwipeLayout mSwipeLayout;
		ImageView mIvDelete, mIvNotificationIcon;
		ChangeDirectionLinearLayout mLlBottomWrapper;
		ConstraintLayout mClSurface;
		TextView mTvNotificationText, mTvNotificationDate;
		
		public ViewHolder(final View iV)
		{
			super(iV);
			mSwipeLayout = iV.findViewById(R.id.sl_notification_item_cover_view);
			mIvDelete = iV.findViewById(R.id.iv_family_item_delete);
			mLlBottomWrapper = iV.findViewById(R.id.ll_notification_item_cover_view_bottom_wrapper);
			
			//included view
			mClSurface = iV.findViewById(R.id.cl_notification_item_cover_view);
			mIvNotificationIcon = mClSurface.findViewById(R.id.iv_notification_item_cover_view);
			mTvNotificationText = mClSurface.findViewById(R.id.tv_notification_item_cover_view_notification_text);
			mTvNotificationDate = mClSurface.findViewById(R.id.tv_notification_item_cover_view_notification_date);
		}
		
		void bind(final NotificationInstance iNotification)
		{
			mSwipeLayout.close();
			mSwipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
			
			SwipeLayout.LayoutParams params = new SwipeLayout.LayoutParams((int) AppUtils.convertPixelsToDp(MAX_WIDTH_OF_BOTTOM_VIEW, mContext), RecyclerView.LayoutParams.MATCH_PARENT);
			
			if (mIsEnglishLanguage)
			{
				params.gravity = Gravity.END;
				mSwipeLayout.addDrag(SwipeLayout.DragEdge.Right, mLlBottomWrapper);
			}
			else
			{
				params.gravity = Gravity.START;
				mSwipeLayout.addDrag(SwipeLayout.DragEdge.Left, mLlBottomWrapper);
			}
			
			mLlBottomWrapper.setLayoutParams(params);
			
			//Surface
			mClSurface.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(final View v)
				{
					doOnSurfaceClick(iNotification);
				}
			});
			
			// iv delete
			mIvDelete.setBackgroundColor(ContextCompat.getColor(mContext, R.color.midnight_blue_two));
			mIvDelete.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(final View v)
				{
					doOnDeleteClick(iNotification);
				}
			});
			
			//NotificationIcon
			Drawable notificationDrawable;
			
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
			{
				notificationDrawable = mContext.getResources().getDrawable(iNotification.isTelemessage() ? R.drawable.msg1 : R.drawable.msg2, mContext.getTheme());
			}
			else
			{
				notificationDrawable = mContext.getResources().getDrawable(iNotification.isTelemessage() ? R.drawable.msg1 : R.drawable.msg2);
			}
			
			mIvNotificationIcon.setImageDrawable(notificationDrawable);
			
			//NotificationText
			String currentNotificationText = iNotification.getMessageText();
			String notificationText = TextUtils.isEmpty(currentNotificationText) ? mContext.getString(R.string.some_text_here) : currentNotificationText;
			
			if (iNotification.isRead())
			{
				mTvNotificationText.setText(notificationText);
			}
			else
			{
				//make text bold style
				SpannableString styledString = new SpannableString(notificationText);
				styledString.setSpan(new StyleSpan(Typeface.BOLD), 0, notificationText.length(), 0);
				mTvNotificationText.setText(styledString);
			}
			
			//NotificationDate
			mTvNotificationDate.setText(iNotification.getNotificationDate());
		}
	}
}
