package webServices.responses.getSideMenuItems;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import utils.global.enums.eSideMenuItems;

public class SideMenuCategory
{
	@JsonProperty("SideMenuCategoryID")
	private int mSideMenuCategoryID;
	@JsonProperty("LanguageCode")
	private String mLanguageCode;
	@JsonProperty("IsActive")
	private boolean mIsActive;
	@JsonProperty("IsConst")
	private boolean mIsConst;
	@JsonProperty("CategoryItems")
	private ArrayList<SideMenuItem> mSideMenuItems;
	@JsonProperty("CategoryName")
	private String mCategoryName;
	@JsonProperty("CategoryIndex")
	private int mCategoryIndex;
	@JsonIgnore
	private int mIconResourceId;
	
	public int getSideMenuCategoryID()
	{
		return mSideMenuCategoryID;
	}
	
	public String getLanguageCode()
	{
		return mLanguageCode;
	}
	
	public boolean isActive()
	{
		return mIsActive;
	}
	
	public ArrayList<SideMenuItem> getSideMenuItems()
	{
		return mSideMenuItems;
	}
	
	public String getCategoryName()
	{
		return mCategoryName;
	}
	
	public int getCategoryIndex()
	{
		return mCategoryIndex;
	}
	
	public int getIconResourceId()
	{
		return mIconResourceId;
	}
	
	public boolean isConst()
	{
		return mIsConst;
	}
	
	public SideMenuCategory(final ArrayList<SideMenuItem> iSideMenuItems, final String iCategoryName, final int iIconResourceId)
	{
		mSideMenuCategoryID = -1;
		mLanguageCode = "";
		mIsActive = true;
		mSideMenuItems = iSideMenuItems;
//		mCategoryName = iItem.getItemName();
		mCategoryName = iCategoryName;
		mCategoryIndex = -1;
		mIconResourceId = iIconResourceId;
	}
	
	public SideMenuCategory()
	{
	}
}
