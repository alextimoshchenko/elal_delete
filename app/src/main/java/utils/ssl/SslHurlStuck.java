package utils.ssl;

import android.util.Log;

import com.android.volley.toolbox.HurlStack;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import utils.global.AppUtils;

/**
 * Created with care by Alexey.T on 28/11/2017.
 * <p>
 * TODO: Add a class header comment!
 */
public class SslHurlStuck extends HurlStack
{
	private final static String TAG = SslHurlStuck.class.getSimpleName();
	
	private PubKeyManager mPubKeyManager;
	
	public SslHurlStuck(final PubKeyManager iPubKeyManager)
	{
		super();
		
		mPubKeyManager = iPubKeyManager;
	}
	
	@Override
	protected HttpURLConnection createConnection(URL url) throws IOException
	{
		try
		{
			return new PinnedCertificateHttpsURLConnectionFactory().createHttpsURLConnection(url.toString(), mPubKeyManager);
		}
		catch (Throwable iThrowable)
		{
			AppUtils.printLog(Log.ERROR, TAG, iThrowable.getMessage());
			return (HttpURLConnection) url.openConnection();
		}
	}
}
