package utils.global;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Date;

import il.co.ewave.elal.BuildConfig;

/**
 * Created by Mor.Burk.Hazan on 18/02/2016. ©Shahar Ben-Moshe
 */
public class CameraUtil
{
	private static final String TAG = CameraUtil.class.getSimpleName();
	
	public static final int REQUEST_TAKE_PICTURE = 2532;
	private static final int REQUEST_SELECT_GALLERY = 7325;
	private static final String TYPE_IMAGE = "image/*";
	private static final int IMAGE_MAX_SIZE = 768;
	
	private static final String ELAL = "Elal";
//	private static final String PATH_SUFFIX = "image.jpg";
	private static final String PATH_SUFFIX = ELAL + new Date().getTime() + ".png";
	
	
	//	public static AlertDialog createSelectPhotoDialog(final Activity iActivity)
	//	{
	//		return createSelectPhotoDialog((Object) iActivity);
	//	}
	//
	//	public static AlertDialog createSelectPhotoDialog(final Fragment iFragment)
	//	{
	//		return createSelectPhotoDialog((Object) iFragment);
	//	}
	//
	//	public static AlertDialog createSelectPhotoDialog(final android.support.v4.app.Fragment iFragment)
	//	{
	//		return createSelectPhotoDialog((Object) iFragment);
	//	}
	//
	//	private static AlertDialog createSelectPhotoDialog(final Object iRawContext)
	//	{
	//		AlertDialog dialog = null;
	//
	//		if (iRawContext != null)
	//		{
	//			final Activity activity;
	//
	//			if (iRawContext instanceof Fragment)
	//			{
	//				activity = ((Fragment) iRawContext).getActivity();
	//			}
	//			else if (iRawContext instanceof android.support.v4.app.Fragment)
	//			{
	//				activity = ((android.support.v4.app.Fragment) iRawContext).getActivity();
	//			}
	//			else if (iRawContext instanceof Activity)
	//			{
	//				activity = (Activity) iRawContext;
	//			}
	//			else
	//			{
	//				activity = null;
	//			}
	//
	//			if (activity != null)
	//			{
	//				AlertDialog.Builder builder = new AlertDialog.Builder(activity);
	//
	//				ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, R.layout.bottom_popup_item, activity.getResources().getStringArray(R.array.photoSelect));
	//				builder.setAdapter(adapter, new DialogInterface.OnClickListener()
	//				{
	//					@Override
	//					public void onClick(DialogInterface dialog, int which)
	//					{
	//						Intent photoIntent = null;
	//						int intentRequestCode = -1;
	//
	//						switch (which)
	//						{
	//							case 0: //take photo
	//								photoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	//								photoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(createOrGetFile()));
	//								intentRequestCode = REQUEST_TAKE_PICTURE;
	//								break;
	//							case 1: //select gallery
	//								Intent tempPhotoIntent = new Intent();
	//								tempPhotoIntent.setType(TYPE_IMAGE);
	//								tempPhotoIntent.setAction(Intent.ACTION_GET_CONTENT);
	//								photoIntent = Intent.createChooser(tempPhotoIntent, null);
	//								intentRequestCode = REQUEST_SELECT_GALLERY;
	//								break;
	//							case 2: //cancel
	//								dialog.dismiss();
	//								break;
	//							default:
	//								break;
	//						}
	//
	//						if (photoIntent != null && photoIntent.resolveActivity(activity.getPackageManager()) != null)
	//						{
	//                            activity.startActivityForResult(photoIntent, intentRequestCode);
	//						}
	//					}
	//				}).setOnDismissListener(new DialogInterface.OnDismissListener()
	//				{
	//					@Override
	//					public void onDismiss(DialogInterface dialog)
	//					{
	//					}
	//				});
	//
	//				dialog = builder.create();
	//				WindowManager.LayoutParams dialogParams = dialog.getWindow().getAttributes();
	//				dialogParams.gravity = Gravity.BOTTOM;
	//			}
	//		}
	//
	//		return dialog;
	//	}
	
	public static void startCameraIntent(final Object iRawContext, File iImgFile)
	{
		if (iRawContext != null)
		{
			final Activity activity;
			
			if (iRawContext instanceof Fragment)
			{
				activity = ((Fragment) iRawContext).getActivity();
			}
			else if (iRawContext instanceof android.support.v4.app.Fragment)
			{
				activity = ((android.support.v4.app.Fragment) iRawContext).getActivity();
			}
			else if (iRawContext instanceof Activity)
			{
				activity = (Activity) iRawContext;
			}
			else
			{
				activity = null;
			}
			
			if (activity != null)
			{
				Intent photoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				
				if (photoIntent.resolveActivity(activity.getPackageManager()) != null)
				{
					// Create the File where the photo should go
					File photoFile = null;
					photoFile = iImgFile;
					
					// Continue only if the File was successfully created
					if (photoFile != null)
					{
						Uri photoURI = FileProvider.getUriForFile(activity,BuildConfig.APPLICATION_ID + ".provider", photoFile);
						photoIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
						photoIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
						
						activity.startActivityForResult(photoIntent, REQUEST_TAKE_PICTURE);
					}
				}
				
//				photoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(iImgFile));
//				photoIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//
//				if (photoIntent.resolveActivity(activity.getPackageManager()) != null)
//				{
//					activity.startActivityForResult(photoIntent, REQUEST_TAKE_PICTURE);
//				}
				
			}
		}
	}
	
	public static void startGalleryIntent(final Object iRawContext)
	{
		if (iRawContext != null)
		{
			final Activity activity;
			
			if (iRawContext instanceof Fragment)
			{
				activity = ((Fragment) iRawContext).getActivity();
			}
			else if (iRawContext instanceof android.support.v4.app.Fragment)
			{
				activity = ((android.support.v4.app.Fragment) iRawContext).getActivity();
			}
			else if (iRawContext instanceof Activity)
			{
				activity = (Activity) iRawContext;
			}
			else
			{
				activity = null;
			}
			
			if (activity != null)
			{
				Intent tempPhotoIntent = new Intent();
				tempPhotoIntent.setType(TYPE_IMAGE);
				tempPhotoIntent.setAction(Intent.ACTION_GET_CONTENT);
				Intent photoIntent = Intent.createChooser(tempPhotoIntent, null);
				
				if (photoIntent.resolveActivity(activity.getPackageManager()) != null)
				{
					activity.startActivityForResult(photoIntent, REQUEST_SELECT_GALLERY);
				}
			}
		}
	}
	
	@SuppressWarnings("ResultOfMethodCallIgnored")
	public static Bitmap resolveSelectedPhotoToBitmap(Context iContext, int iRequestCode, int iResultCode, Intent iData, String iImgPath)
	{
		Bitmap result = null;
		
		if (iResultCode == Activity.RESULT_OK || iResultCode == Activity.RESULT_CANCELED)
		{
			if (iRequestCode == REQUEST_TAKE_PICTURE)
			{
				try
				{
					result = decodeFile(iImgPath);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else if (iRequestCode == REQUEST_SELECT_GALLERY)
			{
				if (iData != null)
				{
					Uri pickedImage = iData.getData();
					if (pickedImage != null)
					{
						// Let's read picked image path using content resolver
						String[] filePath = {MediaStore.Images.Media.DATA};
						
						try
						{
							result = MediaStore.Images.Media.getBitmap(iContext.getContentResolver(), pickedImage);
						}
						catch (IOException e)
						{
							e.printStackTrace();
						}
					}
				}
			}
		}
		
		return result;
	}
	
	private static Bitmap decodeFile(String path)
	{
		try
		{
			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			Bitmap tempBit = BitmapFactory.decodeFile(path, o);
			// The new size we want to scale to
			final int REQUIRED_SIZE = 750;
//			final int REQUIRED_SIZE = 1024;
			
			// Find the correct scale value. It should be the power of 2.
			int scale = 1;
			while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE)
				scale *= 2;
			
			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			return BitmapFactory.decodeFile(path, o2);
		}
		catch (Throwable e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public static File createOrGetFile()
	{
		final File path = new File(Environment.getExternalStorageDirectory() + "/ELAL/", ELAL + new Date().getTime() + ".png");
		if (!path.exists())
		{
			path.mkdir();
		}
		return path;
//		return new File(path, PATH_SUFFIX);
	}
	
	public static File createFile()
	{
		final File path = new File(Environment.getExternalStorageDirectory() + "/ELAL/", ELAL + new Date().getTime() + ".png");
		if (!path.exists())
		{
			path.mkdir();
		}
		return path;
		//		return new File(path, PATH_SUFFIX);
	}
	
	public static File createImageFile(Context iContext) throws IOException
	{
		// Create an image file name
		String timeStamp = new Date().getTime() + "";
		String imageFileName = ELAL + "_" + timeStamp;
		File storageDir = iContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
		File image = File.createTempFile(
				imageFileName,  /* prefix */
				".jpg",         /* suffix */
				storageDir      /* directory */
		);
		
		return image;
	}
	
	public static byte[] getByteArrFromUri(Context iContext, Uri iUri)
	{
		byte[] result = null;
		ByteBuffer byteBuffer;
		
		try
		{
			InputStream is = iContext.getContentResolver().openInputStream(iUri);
			//			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			byteBuffer = ByteBuffer.allocate(is != null ? is.available() : 0);
			int bufferSize = 1024;
			byte[] buffer = new byte[bufferSize];
			
			int len;
			
			if (is != null)
			{
				while ((len = is.read(buffer)) != -1)
				{
					//					baos.write(buffer, 0, len);
					byteBuffer.put(buffer, 0, len);
				}
			}
			
			//			result = baos.toByteArray();
			result = byteBuffer.array();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return result;
	}
	
	public static Bitmap decodeFile(byte[] iByteArr)
	{
		Bitmap bitmap = null;
		
		try
		{
			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			
			ByteArrayInputStream bis = new ByteArrayInputStream(iByteArr);
			BitmapFactory.decodeStream(bis, null, o);
			bis.close();
			
			int scale = 1;
			if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE)
			{
				scale = (int) Math.pow(2, (int) Math.ceil(Math.log(IMAGE_MAX_SIZE / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
			}
			
			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			bis = new ByteArrayInputStream(iByteArr);
			bitmap = BitmapFactory.decodeStream(bis, null, o2);
			bis.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return bitmap;
	}
	
	public static Uri getImageUri(Context inContext, Bitmap iBitmap)
	{
		Uri uri = null;
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		iBitmap.compress(Bitmap.CompressFormat.JPEG, 70, bytes);
		String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), iBitmap, "Title", null);
		if (path != null)
		{
			uri = Uri.parse(path);
		}
		return uri;
	}
	
	public static String getRealPathFromURI(Uri uri, Context iContext)
	{
		String result = null;
		
		Cursor cursor = iContext.getContentResolver().query(uri, null, null, null, null);
		if (cursor != null)
		{
			cursor.moveToFirst();
			int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
			result = cursor.getString(idx);
			
			cursor.close();
		}
		
		return result;
	}
}
