package webServices.controllers;

import com.android.volley.Request;
import com.android.volley.Response;

import webServices.global.JacksonRequest;
import webServices.global.RequestStringBuilder;
import webServices.requests.RequestCompletePnr;
import webServices.requests.RequestGetMultiplePNRDestinations;
import webServices.requests.RequestGetUserActivePNRs;
import webServices.requests.RequestPnrByNum;
import webServices.responses.ResponseGetPnrByNum;
import webServices.responses.ResponseGetUserActivePNRs;
import webServices.responses.getMultiplePNRDestinations.ResponseGetMultiplePNRDestinations;

/**
 * Created with care by Alexey.T on 14/08/2017.
 */
public class MyFlightsController extends AController implements MizdamenInterfave
{
	public void GetUserActivePNRs(final RequestGetUserActivePNRs iRequest, final Response.Listener<ResponseGetUserActivePNRs> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseGetUserActivePNRs> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getUserActivePnrsUrl(), iRequest, ResponseGetUserActivePNRs.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void GetMultiplePNRDestinations(final RequestGetMultiplePNRDestinations iRequest, final Response.Listener<ResponseGetMultiplePNRDestinations> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseGetMultiplePNRDestinations> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getMultiplePnrDestinationsUrl(), iRequest, ResponseGetMultiplePNRDestinations.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	
	public void GetPnrByNum(final RequestPnrByNum iRequest, final Response.Listener<ResponseGetPnrByNum> iListener, final Response.ErrorListener errorListener)
	{
		if (!isRequestQueueNull() && iListener != null && errorListener != null)
		{
			JacksonRequest<ResponseGetPnrByNum> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getPnrByNum(), iRequest, ResponseGetPnrByNum.class, iListener, errorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void getCompletePNR(final RequestCompletePnr iRequest, final Response.Listener<ResponseGetPnrByNum> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseGetPnrByNum> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getCompletePNRUrl(), iRequest, ResponseGetPnrByNum.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
}
