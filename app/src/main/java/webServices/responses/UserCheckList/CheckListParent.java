package webServices.responses.UserCheckList;

import com.bignerdranch.expandablerecyclerview.model.Parent;

import java.util.ArrayList;
import java.util.List;

import utils.global.FilterList;

/**
 * Created with care by Shahar Ben-Moshe on 22/02/17.
 */

public class CheckListParent implements Parent<CheckListItem>
{
	private int mParentNameResourceId;
	private FilterList<CheckListItem> mCheckListItems;
	
	public CheckListParent(final int iParentNameResourceId)
	{
		mParentNameResourceId = iParentNameResourceId;
		mCheckListItems = new FilterList<>();
	}
	
	@Override
	public List<CheckListItem> getChildList()
	{
		return mCheckListItems.getFilteredList();
	}
	
	@Override
	public boolean isInitiallyExpanded()
	{
		return true;
	}
	
	public int getParentNameResourceId()
	{
		return mParentNameResourceId;
	}
	
	void addCheckListItems(final ArrayList<CheckListItem> iICheckListItems)
	{
		if (iICheckListItems != null)
		{
			mCheckListItems.startNewList(iICheckListItems);
		}
	}
	
	public ArrayList<CheckListItem> getCheckListItems()
	{
		return mCheckListItems.getFilteredList();
	}
	
	public boolean isHasItems()
	{
		return mCheckListItems != null && mCheckListItems.getCompleteList().size() > 0;
	}
	
	public void filterList(final boolean iShouldShowOnlyUndone)
	{
		mCheckListItems.filterListByParameters(new FilterList.IFilter<CheckListItem>()
		{
			@Override
			public boolean filterByParams(final CheckListItem iFilterableObject)
			{
				boolean result = true;
				
				if (iShouldShowOnlyUndone)
				{
					result = !iFilterableObject.isDone();
				}
				
				return result;
			}
		});
	}
}
