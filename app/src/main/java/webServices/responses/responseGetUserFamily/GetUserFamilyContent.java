package webServices.responses.responseGetUserFamily;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class GetUserFamilyContent implements Parcelable
{
	@JsonProperty("FamilyID")
	private int mFamilyID;
	@JsonProperty("UserID")
	private int mUserID;
	@JsonProperty("FamilyTypeID")
	private int mFamilyTypeID;
	@JsonProperty("FirstNameEng")
	private String mFirstNameEng;
	@JsonProperty("LastNameEng")
	private String mLastNameEng;
	@JsonProperty("DOB")
	private Date mDateOfBirth;
	@JsonProperty("IsActive")
	private boolean mIsActive;
	@JsonProperty("Title")
	private String mTitle = "";
	@JsonProperty("MatmidMemberID")
	private int mMatmidMemberID;
	
	public GetUserFamilyContent()
	{
	}
	
	public String getFirstNameEng()
	{
		return mFirstNameEng;
	}
	
	public int getFamilyID()
	{
		return mFamilyID;
	}
	
	public int getUserID()
	{
		return mUserID;
	}
	
	public Date getDateOfBirth()
	{
		return mDateOfBirth;
	}
	
	public String getLastNameEng()
	{
		return mLastNameEng;
	}
	
	public int getFamilyTypeID()
	{
		return mFamilyTypeID;
	}
	
	public boolean isActive()
	{
		return mIsActive;
	}
	
	public String getTitle()
	{
		return mTitle;
	}
	
	public int getMatmidMemberID()
	{
		return mMatmidMemberID;
	}
	
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeInt(this.mFamilyID);
		dest.writeInt(this.mUserID);
		dest.writeInt(this.mFamilyTypeID);
		dest.writeString(this.mFirstNameEng);
		dest.writeString(this.mLastNameEng);
		dest.writeLong(this.mDateOfBirth != null ? this.mDateOfBirth.getTime() : -1);
		dest.writeByte(this.mIsActive ? (byte) 1 : (byte) 0);
		dest.writeString(this.mTitle);
		dest.writeInt(this.mMatmidMemberID);
	}
	
	protected GetUserFamilyContent(Parcel in)
	{
		this.mFamilyID = in.readInt();
		this.mUserID = in.readInt();
		this.mFamilyTypeID = in.readInt();
		this.mFirstNameEng = in.readString();
		this.mLastNameEng = in.readString();
		long tmpMDateOfBirth = in.readLong();
		this.mDateOfBirth = tmpMDateOfBirth == -1 ? null : new Date(tmpMDateOfBirth);
		this.mIsActive = in.readByte() != 0;
		this.mTitle = in.readString();
		this.mMatmidMemberID = in.readInt();
	}
	
	public static final Parcelable.Creator<GetUserFamilyContent> CREATOR = new Parcelable.Creator<GetUserFamilyContent>()
	{
		@Override
		public GetUserFamilyContent createFromParcel(Parcel source)
		{
			return new GetUserFamilyContent(source);
		}
		
		@Override
		public GetUserFamilyContent[] newArray(int size)
		{
			return new GetUserFamilyContent[size];
		}
	};
}
