package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.UserData;

public class RequestGetSearchGlobalData
{
	@JsonProperty("UserId")
	private int mUserId;
	@JsonProperty("LanguageCode")
	private String mLanguageCode = UserData.getInstance().getLanguage().getLanguageCode();
	
	public RequestGetSearchGlobalData(int iUserId)
	{
		mUserId = iUserId;
	}
	
	public RequestGetSearchGlobalData()
	{
	}
}
