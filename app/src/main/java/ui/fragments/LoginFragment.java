package ui.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;

import global.ElAlApplication;
import global.GlobalVariables;
import global.IntentExtras;
import global.UserData;
import global.eInAppNavigation;
import global.eLanguage;
import il.co.ewave.elal.R;
import interfaces.ILoginResponse;
import ui.activities.EWBaseActivity;
import ui.activities.EWBaseDrawerActivity;
import ui.activities.EWBaseUserActivity;
import ui.customWidgets.ChangeDirectionRadioGroup;
import ui.customWidgets.ChangeDirectionTextInputLayout;
import ui.customWidgets.ElalCustomTilLL;
import ui.customWidgets.ExpandCollapseAnimation;
import utils.errors.ServerError;
import utils.global.AppUtils;
import utils.global.CustomTextWatcher;
import utils.global.ElalPreferenceUtils;
import utils.global.FlightUtils;
import utils.global.LocaleUtils;
import utils.global.googleAnalitics.googleTagManager.TagManagerImplementation.ePushEvents;
import webServices.global.RequestStringBuilder;
import webServices.requests.RequestGetGroupPNRsData;
import webServices.responses.ResponseGetGroupPNRs;
import webServices.responses.ResponseGetUserActivePNRs;
import webServices.responses.ResponseMatmidLogin;
import webServices.responses.ResponseUserLogin;
import webServices.responses.responseGetUserActivePNRs.UserActivePnr;
import webServices.responses.responseUserLogin.UserLogin;
import webServices.responses.resposeMatmidLogin.MatmidLogin;

import static ui.activities.MainActivity.LOGIN_TYPE_MATMID_CLUB_MEMBER;
import static ui.activities.MainActivity.LOGIN_TYPE_REGISTERED_USER;

/**
 * Created with care by Shahar Ben-Moshe on 03/05/17.
 */

public class LoginFragment extends EWBaseFragment implements Response.ErrorListener
{
	private static final String TAG = LoginFragment.class.getSimpleName();
	private static final int REGISTRATION_BUTTON_LAYOUT_DURATION = 200;
	
	private RadioButton mRbRegisteredAccount;
	private RadioButton mRbMatmidMember;
	private ChangeDirectionRadioGroup mRgLoginType;
	private EditText mEtEmail;
	private EditText mEtPassword;
	private TextView mTvForgotMyPassword;
	private CheckBox mCbRememberMe;
	private Button mBtnContinue;
	private TextView mTvRegister;
	private ExpandCollapseAnimation mBottomRegistrationExpandCollapseAnimation;
	private LinearLayout mLlRegisteredAccount;
	private LinearLayout mLlMatmidClubMember;
	private LinearLayout mLlRegistrationButtonLayout;
	private LinearLayout mLlHeaderContainer;
	private int mLastSelectedLoginType;
	private EditText mEtMatmidMemberNumber;
	private EditText mEtMatmidMemberPassword;
	private TextView mTvMatmidMemberForgotMyMatmidNumber;
	private TextView mTvMatmidMemberForgotMyPassword;
	private ElalCustomTilLL mTilEmail;
	private ElalCustomTilLL mTilPassword;
	private ElalCustomTilLL mTilMatmidMemberPassword;
	private ElalCustomTilLL mTilMatmidMemberNumber;
	private static final int EXPAND_DURATION = 500;
	private ExpandCollapseAnimation mHeaderExpandCollapseAnimation;
	private ImageView mIvTooltipMatimidMemberNumber;
	private ImageView mIvTooltipMatmidPassword;
	private boolean mIsLoginAsMatmid;
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_login_layout);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		getIntentExtras();
		initReference();
		setListeners();
		setGui();
		setExpandableViews();
		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		attachKeyboardListeners((ViewGroup) view.findViewById(R.id.ll_frag_login_root_view_group));
		
		setDebugData();
		
		pushScreenOpenEvent("Login");
	}
	
	private void setExpandableViews()
	{
		mLlHeaderContainer.post(new Runnable()
		{
			@Override
			public void run()
			{
				int expandedViewHeight = mLlHeaderContainer.getHeight();
				mHeaderExpandCollapseAnimation = new ExpandCollapseAnimation(mLlHeaderContainer, true, EXPAND_DURATION, expandedViewHeight);
			}
		});
	}
	
	@Override
	protected void onShowKeyboard(final int keyboardHeight)
	{
		animateCollapseExpandOfHeader(false);
	}
	
	@Override
	protected void onHideKeyboard()
	{
		animateCollapseExpandOfHeader(true);
	}
	
	private void animateCollapseExpandOfHeader(final boolean iShouldExpand)
	{
		if (mHeaderExpandCollapseAnimation != null && ((iShouldExpand && !mHeaderExpandCollapseAnimation.isExpand()) || (!iShouldExpand && mHeaderExpandCollapseAnimation.isExpand())))
		{
			mHeaderExpandCollapseAnimation.reverseAndAnimate();
		}
	}
	
	private void setDebugData()
	{
		if (GlobalVariables.isDebugVersion)
		{
			
			if (RequestStringBuilder.isEnvironmentProd())
			{
				mEtMatmidMemberNumber.setText("110743572");
				mEtMatmidMemberPassword.setText("Kosta2015");
				
				//			mEtMatmidMemberNumber.setText("110755444");
				//			mEtMatmidMemberPassword.setText("Pnina1256");
				
				mEtEmail.setText("pp@p.pp");
				mEtPassword.setText("Qwer1234");
			}
			else
			{
				mEtMatmidMemberNumber.setText("2588879");
				mEtMatmidMemberPassword.setText("Matmid1234");
				
				mEtEmail.setText("pp@p.pp");
				mEtPassword.setText("Aa123456");
			}
			
			
			//			mEtEmail.setText("tal.koren@ewave.co.il");
			//			mEtPassword.setText("Qwer1234");
			
			
		}
	}
	
	private void initReference()
	{
		mLlHeaderContainer = getActivity().findViewById(R.id.ll_login_frag_header_container);
		mRbRegisteredAccount = getActivity().findViewById(R.id.rb_login_RegisteredAccount);
		mRbMatmidMember = getActivity().findViewById(R.id.rb_login_MatmidMember);
		mRgLoginType = getActivity().findViewById(R.id.rg_login_LoginType);
		
		mTilEmail = getActivity().findViewById(R.id.til_login_Email);
		mTilEmail.init(getResources().getText(R.string.email_asterisk));
		mTilEmail.setInputTypeEmail();
		
		mTilPassword = getActivity().findViewById(R.id.til_login_RegisteredAccountPassword);
		mTilPassword.init(getString(R.string.password_asterisk));
		mTilPassword.setInputTypePassword();
		
		mEtEmail = mTilEmail.getEditText();
		mEtPassword = mTilPassword.getEditText();
		mEtPassword.setImeOptions(EditorInfo.IME_ACTION_DONE);
		
		mTvForgotMyPassword = getActivity().findViewById(R.id.tv_login_RegisteredAccountForgotMyPassword);
		
		mTilMatmidMemberNumber = getActivity().findViewById(R.id.til_login_MatmidMemberNumber);
		mTilMatmidMemberNumber.init(getString(R.string.member_number_asterisk));
		mTilMatmidMemberNumber.setEditTextInputType(InputType.TYPE_CLASS_NUMBER, InputType.TYPE_NUMBER_FLAG_DECIMAL);
		
		mTilMatmidMemberPassword = getActivity().findViewById(R.id.til_login_MatmidMemberPassword);
		mTilMatmidMemberPassword.init(getString(R.string.password_asterisk));
		mTilMatmidMemberPassword.setInputTypePassword();
		
		mEtMatmidMemberNumber = mTilMatmidMemberNumber.getEditText();
		mEtMatmidMemberPassword = mTilMatmidMemberPassword.getEditText();
		mEtMatmidMemberPassword.setImeOptions(EditorInfo.IME_ACTION_DONE);
		
		mIvTooltipMatimidMemberNumber = getActivity().findViewById(R.id.iv_tooltip_MatmidMemberNumber);
		mIvTooltipMatmidPassword = getActivity().findViewById(R.id.iv_tooltip_MatmidMemberPassword);
		
		mTvMatmidMemberForgotMyMatmidNumber = getActivity().findViewById(R.id.tv_login_MatmidMemberForgotMyMatmidNumber);
		mTvMatmidMemberForgotMyPassword = getActivity().findViewById(R.id.tv_login_MatmidMemberForgotMyPassword);
		mCbRememberMe = getActivity().findViewById(R.id.cb_login_RememberMe);
		mBtnContinue = getActivity().findViewById(R.id.btn_login_Continue);
		mTvRegister = getActivity().findViewById(R.id.tv_login_Register);
		mLlRegisteredAccount = getActivity().findViewById(R.id.ll_login_RegisteredAccount);
		mLlMatmidClubMember = getActivity().findViewById(R.id.ll_login_MatmidClubMember);
		mLlRegistrationButtonLayout = getActivity().findViewById(R.id.ll_login_RegistrationButtonLayout);
	}
	
	private void setListeners()
	{
		getActivity().findViewById(R.id.iv_name_screen_back_arrow).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				getActivity().onBackPressed();
				//				((EWBaseUserActivity) getActivity()).loadHomeFragment();
			}
		});
		
		
		mTvForgotMyPassword.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View iView)
			{
				doOnForgotPasswordClick();
			}
		});
		
		mTvMatmidMemberForgotMyPassword.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View iView)
			{
				doOnMatmidForgotPasswordClick();
			}
		});
		
		mTvMatmidMemberForgotMyMatmidNumber.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View iView)
			{
				doOnForgotMatmidNumberClick();
			}
		});
		
		mBtnContinue.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View iView)
			{
				hideKeyboard();
				doOnContinueClick();
			}
		});
		
		mTvRegister.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View iView)
			{
				doOnRegisterClick();
			}
		});
		
		mRgLoginType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(final RadioGroup group, @IdRes final int checkedId)
			{
				onLoginTypeChange(checkedId);
			}
		});
		
		mEtEmail.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				showEmailError(false);
			}
		});
		
		mEtEmail.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus && !TextUtils.isEmpty(mEtEmail.getText().toString()) && !isEmailValid())
				{
					showEmailError(true);
				}
			}
		});
		
		mEtPassword.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				showPasswordError(mTilPassword, false);
			}
		});
		
		mEtPassword.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus && !TextUtils.isEmpty(mEtPassword.getText().toString()) && !isPasswordValid(mEtPassword.getText().toString(), false))
				{
					showPasswordError(mTilPassword, true);
				}
			}
		});
		
		mEtMatmidMemberNumber.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				showMatmidNumberError(false);
			}
		});
		
		mEtMatmidMemberNumber.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus && !TextUtils.isEmpty(mEtMatmidMemberNumber.getText().toString()) && !isMatmidNumberValid(mEtMatmidMemberNumber.getText().toString()))
				{
					showMatmidNumberError(true);
				}
			}
		});
		
		mEtMatmidMemberPassword.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				showPasswordError(mTilMatmidMemberPassword, false);
			}
		});
		
		mEtMatmidMemberPassword.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus && !TextUtils.isEmpty(mEtMatmidMemberPassword.getText().toString()) && !isPasswordValid(mEtMatmidMemberPassword.getText().toString(), true))
				{
					showPasswordError(mTilMatmidMemberPassword, true);
				}
			}
		});
		
		mIvTooltipMatimidMemberNumber.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View iV)
			{
				AppUtils.showToolTip(getContext(), iV, getString(R.string.matmid_membership_number_tooltip));
				
			}
		});
		
		mIvTooltipMatmidPassword.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View iV)
			{
				AppUtils.showToolTip(getContext(), iV, getString(R.string.matmid_password_tooltip));
			}
		});
	}
	
	private void doOnMatmidForgotPasswordClick()
	{
		
		tryGoToWebFragment(RequestStringBuilder.getForgotPasswordMatmidUrlByLanguage());
		((EWBaseActivity) getActivity()).pushEvent(ePushEvents.FORGET_PASSWORD_CLUB);
	}
	
	private void getIntentExtras()
	{
		if (getArguments() != null)
		{
			mIsLoginAsMatmid = getArguments().getBoolean(IntentExtras.LOGIN_AS_MATMID, false);
		}
	}
	
	private void setGui()
	{
		((TextView) getActivity().findViewById(R.id.tv_name_screen_title)).setText(R.string.login_title);
		getActivity().findViewById(R.id.iv_name_screen_back_arrow).setVisibility(View.VISIBLE);
		
		if (mIsLoginAsMatmid)
		{
			mLastSelectedLoginType = LOGIN_TYPE_MATMID_CLUB_MEMBER;
		}
		else if (ElalPreferenceUtils.getSharedPreferenceIntOrMinusOne(getActivity(), IntentExtras.LOGGEDIN_TYPE) != -1)
		{
			mLastSelectedLoginType = ElalPreferenceUtils.getSharedPreferenceIntOrDefault(getActivity(), IntentExtras.LOGGEDIN_TYPE, LOGIN_TYPE_REGISTERED_USER);
		}
		else
		{
			mLastSelectedLoginType = ElalPreferenceUtils.getSharedPreferenceIntOrDefault(getActivity(), IntentExtras.LAST_SELECTED_LOGIN_TYPE, LOGIN_TYPE_REGISTERED_USER);
		}
		
		new Handler().post(new Runnable()
		{
			@Override
			public void run()
			{
				int expandedViewHeight = mLlRegistrationButtonLayout.getHeight();
				LinearLayout.LayoutParams oldParams = (LinearLayout.LayoutParams) mLlRegistrationButtonLayout.getLayoutParams();
				LinearLayout.LayoutParams newParams = new LinearLayout.LayoutParams(oldParams.width, expandedViewHeight);
				mLlRegistrationButtonLayout.setLayoutParams(newParams);
				
				mBottomRegistrationExpandCollapseAnimation = new ExpandCollapseAnimation(mLlRegistrationButtonLayout, true, REGISTRATION_BUTTON_LAYOUT_DURATION, expandedViewHeight);
				
				if (mLastSelectedLoginType == LOGIN_TYPE_MATMID_CLUB_MEMBER)
				{
					mRbMatmidMember.performClick();
				}
			}
		});
	}
	
	
	private void onLoginTypeChange(final int iCheckedId)
	{
		try
		{
			switch (iCheckedId)
			{
				case R.id.rb_login_RegisteredAccount:
					mLlMatmidClubMember.setVisibility(View.GONE);
					mLlRegisteredAccount.setVisibility(View.VISIBLE);
					if (mBottomRegistrationExpandCollapseAnimation != null)
					{
						mBottomRegistrationExpandCollapseAnimation.reverseAndAnimate();
					}
					ElalPreferenceUtils.setSharedPreference(getActivity(), IntentExtras.LAST_SELECTED_LOGIN_TYPE, LOGIN_TYPE_REGISTERED_USER);
					mLastSelectedLoginType = LOGIN_TYPE_REGISTERED_USER;
					break;
				case R.id.rb_login_MatmidMember:
					mLlMatmidClubMember.setVisibility(View.VISIBLE);
					mLlRegisteredAccount.setVisibility(View.GONE);
					
					if (mBottomRegistrationExpandCollapseAnimation != null)
					{
						mBottomRegistrationExpandCollapseAnimation.reverseAndAnimate();
					}
					
					ElalPreferenceUtils.setSharedPreference(getActivity(), IntentExtras.LAST_SELECTED_LOGIN_TYPE, LOGIN_TYPE_MATMID_CLUB_MEMBER);
					mLastSelectedLoginType = LOGIN_TYPE_MATMID_CLUB_MEMBER;
					break;
				default:
					break;
			}
		}
		catch (Exception iE)
		{
			AppUtils.printLog(Log.ERROR, TAG, "onLoginTypeChange: " + iE.toString());
		}
	}
	
	private void doOnForgotPasswordClick()
	{
		tryGoToScreenById(eInAppNavigation.ForgotPassword.getNavigationId(), null);
		((EWBaseActivity) getActivity()).pushEvent(ePushEvents.FORGET_PASSWORD);
	}
	
	
	private void doOnContinueClick()
	{
		if (isDataValidAndShowErrorMessageIfNeeded())
		{
			Activity activity = getActivity();
			
			if (activity != null && activity instanceof EWBaseUserActivity)
			{
				if (mLastSelectedLoginType == LOGIN_TYPE_REGISTERED_USER)
				{
					((EWBaseUserActivity) getActivity()).performRegisteredUserLogin(mEtEmail.getText().toString(), mEtPassword.getText()
					                                                                                                          .toString(), mCbRememberMe.isChecked(), new Response.Listener<ResponseUserLogin>()
					{
						@Override
						public void onResponse(final ResponseUserLogin response)
						{
							
							ElalPreferenceUtils.setSharedPreference(getActivity(), IntentExtras.LOGGEDIN_TYPE, LOGIN_TYPE_REGISTERED_USER);
							
							doOnSuccessfulRegisteredUserLogin(response);
							
						}
					}, this);
					
					((EWBaseActivity) getActivity()).pushEvent(ePushEvents.LOGIN);
				}
				else
				{
					((EWBaseUserActivity) getActivity()).performMatmidUserLogin(mEtMatmidMemberNumber.getText().toString(), mEtMatmidMemberPassword.getText()
					                                                                                                                               .toString(), new Response.Listener<ResponseMatmidLogin>()
					{
						@Override
						public void onResponse(final ResponseMatmidLogin response)
						{
							ElalPreferenceUtils.setSharedPreference(getActivity(), IntentExtras.LOGGEDIN_TYPE, LOGIN_TYPE_MATMID_CLUB_MEMBER);
							
							if (response.getContent().getMatmidErrorCode() != ServerError.eServerError.Ok.getErrorCode())
							{
								handleMatmidErrorCode(response.getContent().getMatmidErrorCode());
								((EWBaseDrawerActivity) getActivity()).setDrawerAdapter();
							}
							else
							{
								doGetGroupPnrsData(response);
								//								doOnSuccessfulRegisteredUserLogin(response);
							}
						}
					}, this);
					
					((EWBaseActivity) getActivity()).pushEvent(ePushEvents.LOGIN_CLUB);
				}
			}
		}
	}
	
	private void doGetGroupPnrsData(final ResponseMatmidLogin iResponse)
	{
		if (iResponse.getContent() != null && iResponse.getContent().getGroupPNRs() != null && getActivity() != null)
		//		if (iResponse.getContent() instanceof MatmidLogin && iResponse.getContent().getGroupPNRs() != null)
		{
			trySetProgressDialog(true);
			//			doOnSuccessfulRegisteredUserLogin(iResponse);
			((EWBaseUserActivity) getActivity()).performGetGroupPnrsData(new RequestGetGroupPNRsData(iResponse.getContent().getGroupPNRs()), new Response.Listener<ResponseGetGroupPNRs>()
			{
				@Override
				public void onResponse(final ResponseGetGroupPNRs response)
				{
					UserData.getInstance().setGroupPnrs(new ArrayList<UserActivePnr>(response.getContent()));
					doOnSuccessfulRegisteredUserLogin(iResponse);
				}
				
			}, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError error)
				{
					doOnSuccessfulRegisteredUserLogin(iResponse);
				}
			});
		}
		else
		{
			doOnSuccessfulRegisteredUserLogin(iResponse);
		}
	}
	
	private void handleMatmidErrorCode(final int iMatmidErrorCode)
	{
		
		if (iMatmidErrorCode == ServerError.eServerError.UserLocked.getErrorCode()
				||iMatmidErrorCode == ServerError.eServerError.UserLocked2.getErrorCode()
				|| iMatmidErrorCode == ServerError.eServerError.PasswordExpired.getErrorCode()
				|| iMatmidErrorCode == ServerError.eServerError.PasswordExpired2.getErrorCode())
		{
			tryGoToWebFragment(RequestStringBuilder.getMatmidLockedUrlByLanguage());
		}
		else if (iMatmidErrorCode == ServerError.eServerError.PasswordIsNotActive.getErrorCode() || iMatmidErrorCode == ServerError.eServerError.PasswordIsNotActive2.getErrorCode() || iMatmidErrorCode == ServerError.eServerError.AttemptsNumberIsOver
				.getErrorCode() || iMatmidErrorCode == ServerError.eServerError.AttemptsNumberIsOver2.getErrorCode())
		{
			
			AlertDialog dialog = AppUtils.createSimpleMessageDialog(getActivity(), getResources().getString(R.string.locked_user_matmid), getResources().getString(R.string.restore_matmid_number), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(final DialogInterface dialog, final int which)
				{
					tryGoToWebFragment(RequestStringBuilder.getForgotMatmidNumberUrlByLanguage());
					dialog.dismiss();
				}
			}, getResources().getString(R.string.create_new_password), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(final DialogInterface dialog, final int which)
				{
					tryGoToWebFragment(RequestStringBuilder.getForgotPasswordMatmidUrlByLanguage());
					dialog.dismiss();
				}
			});
			dialog.setCancelable(true);
			dialog.show();
		}
		else if (iMatmidErrorCode == ServerError.eServerError.PasswordExpired.getErrorCode() || iMatmidErrorCode == ServerError.eServerError.PasswordExpired2.getErrorCode() || iMatmidErrorCode == ServerError.eServerError.MustChangeTempPassword
				.getErrorCode() || iMatmidErrorCode == ServerError.eServerError.MustChangeTempPassword2.getErrorCode() || iMatmidErrorCode == ServerError.eServerError.MustChangeTempPassword3.getErrorCode())
		{
			AlertDialog dialog = AppUtils.createSimpleMessageDialog(getActivity(), getResources().getString(R.string.password_expired_matmid), getResources().getString(R.string._continue), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(final DialogInterface dialog, final int which)
				{
					tryGoToWebFragment(RequestStringBuilder.getChangePasswordMatmidUrlByLanguage());
					dialog.dismiss();
				}
			}, null, null);
			dialog.setCancelable(false);
			dialog.show();
		}
		else
		{
			AlertDialog dialog = AppUtils.createSimpleMessageDialog(getActivity(), getResources().getString(R.string.incorrect_username_or_password_matmid), getResources().getString(R.string.close), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(final DialogInterface dialog, final int which)
				{
					dialog.dismiss();
				}
			}, null, null);
			dialog.setCancelable(false);
			dialog.show();
		}
	}
	
	private void showEmailError(boolean iState)
	{
		if (iState)
		{
			mTilEmail.setError(true, getActivity().getResources().getString(R.string.email_not_match));
		}
		else
		{
			mTilEmail.setError(false, null);
		}
	}
	
	private void showMatmidNumberError(boolean iState)
	{
		if (iState)
		{
			mTilMatmidMemberNumber.setError(true, mTilMatmidMemberNumber.getOriginHintText());
		}
		else
		{
			mTilMatmidMemberNumber.setError(false, null);
		}
	}
	
	private void showPasswordError(ChangeDirectionTextInputLayout iTilPassword, boolean iState)
	{
		if (iState)
		{
			iTilPassword.setError(getActivity().getResources().getString(R.string.invalid_password));
		}
		else
		{
			iTilPassword.setError(null);
		}
	}
	
	
	private void showPasswordError(ElalCustomTilLL iTilPassword, boolean iState)
	{
		if (iState)
		{
			
			iTilPassword.setError(iState, getActivity().getResources().getString(R.string.invalid_password));
			
		}
		else
		{
			iTilPassword.setError(false, null);
		}
	}
	
	private void doOnForgotMatmidNumberClick()
	{
		tryGoToWebFragment(RequestStringBuilder.getForgotMatmidNumberUrlByLanguage());
		((EWBaseActivity) getActivity()).pushEvent(ePushEvents.FORGOT_MEMBER_NUMBER);
	}
	
	@SuppressWarnings("deprecation")
	private boolean isDataValidAndShowErrorMessageIfNeeded()
	{
		boolean result = true;
		
		if (mLastSelectedLoginType == LOGIN_TYPE_REGISTERED_USER)
		{
			
			if (!isEmailValid())
			{
				result = false;
				showEmailError(true);
			}
			if (!isPasswordValid(mEtPassword.getText().toString(), false))
			{
				result = false;
				showPasswordError(mTilPassword, true);
			}
			
		}
		else
		{
			if (!isMatmidNumberValid(mEtMatmidMemberNumber.getText().toString()))
			{
				result = false;
				showMatmidNumberError(true);
			}
			if (!isPasswordValid(mEtMatmidMemberPassword.getText().toString(), true))
			{
				result = false;
				showPasswordError(mTilMatmidMemberPassword, true);
			}
		}
		return result;
	}
	
	private void doOnRegisterClick()
	{
		//		tryGoToScreenById(eInAppNavigation.Registration.getNavigationId(), null);
		tryGoToFragmentByFragmentClass(eInAppNavigation.Registration.getNavigationFragmentClass(), null, true);
		((EWBaseActivity) getActivity()).pushEvent(ePushEvents.CREATE_ACCOUNT_SCREEN);
	}
	
	// TODO: 07/05/17 Shahar check if the transition is good.
	private void goToHomeFragment()
	{
		if (getActivity() != null && getActivity() instanceof EWBaseUserActivity)
		{
			((EWBaseUserActivity) getActivity()).loadHomeFragment();
		}
	}
	
	private void goToDetailScreenFragment()
	{
		trySetProgressDialog(false);
		
		Log.d(TAG, "entered goToDetailScreenFragment");
		if (getActivity() != null && getActivity() instanceof EWBaseUserActivity)
		{
			((EWBaseUserActivity) getActivity()).loadDetailsMatmidFragment();
		}
	}
	
	@Override
	public void onServiceConnected()
	{
	}
	
	private void doOnSuccessfulRegisteredUserLogin(final ILoginResponse iResponse)
	{
		trySetProgressDialog(true);
		if (iResponse != null && iResponse.getContent() != null)
		{
			if (mCbRememberMe.isChecked())
			{
				UserData.getInstance().setEmailAndPassword(mEtEmail.getText().toString(), mEtPassword.getText().toString());
			}
			else
			{
				UserData.getInstance().setEmailAndPassword(null, null);
				UserData.getInstance().setEmail(mEtEmail.getText().toString());
				UserData.getInstance().setTemporaryPassword(mEtPassword.getText().toString());
			}
			
			eLanguage userLanguageInServer = eLanguage.getLanguageByCodeOrDefault("", eLanguage.English);
			
			boolean isMatmid = false;
			if (iResponse.getContent() instanceof MatmidLogin) // matmid login
			{
				isMatmid = true;
				UserData.getInstance().setUserId(((MatmidLogin) iResponse.getContent()).getUserID());
				UserData.getInstance().setUserObject(((MatmidLogin) iResponse.getContent()).getUserObject());
				UserData.getInstance().setTemporaryPassword(mEtMatmidMemberPassword.getText().toString());
				UserData.getInstance().setActivePnr(((MatmidLogin) iResponse.getContent()).getmPNRNum());
				UserData.getInstance().setSMSession(((MatmidLogin) iResponse.getContent()).getmSMSession());
				UserData.getInstance().setLastSessionUpdate(/*new Date()*/);
				
				userLanguageInServer = eLanguage.getLanguageByCodeOrDefault(((MatmidLogin) iResponse.getContent()).getLanguageID(), eLanguage.English);
			}
			else if (iResponse.getContent() instanceof UserLogin)
			{
				UserData.getInstance().setUserId(((UserLogin) iResponse.getContent()).getUserID());
				UserData.getInstance().setUserObject(((UserLogin) iResponse.getContent()).getUserObject());
				UserData.getInstance().setActivePnr(((UserLogin) iResponse.getContent()).getPNRNum());
				
				userLanguageInServer = eLanguage.getLanguageByCodeOrDefault(((UserLogin) iResponse.getContent()).getUserObject().getLanguageID(), eLanguage.English);
			}
			UserData.getInstance().getUserObject().setIsMatmid(isMatmid);
			UserData.getInstance().saveUserData();
			
			updateNavigationDrawer();
			
			if (userLanguageInServer != UserData.getInstance().getLanguage())
			{
				UserData.getInstance().setLanguage(userLanguageInServer);
				UserData.getInstance().saveUserData();
				LocaleUtils.applyLocale(ElAlApplication.getInstance(), LocaleUtils.convertLanguageCodeToLocaleOrNull(userLanguageInServer.getLanguageCode()));
				
				AlertDialog dialog = AppUtils.createSimpleMessageDialog(getActivity(), getString(R.string.restart_after_language_change), null, null, getString(R.string.close), new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(final DialogInterface dialog, final int which)
					{
						goToHomeFragment();
					}
				});
				dialog.setCancelable(false);
				dialog.show();
				
				trySetProgressDialog(false);
			}
			else
			{
				
				if (UserData.getInstance().getUserObject().isMatmid() && UserData.getInstance().getUserObject().isMatmidFirstLogin())
				{
					trySetProgressDialog(false);
					goToDetailScreenFragment();
				}
				else
				{
					/****Avishay 07/02/18*****/
					moveToFlightScreenOrHome();
					
					//					moveToFlightScreenOrHomeOld();
					/***/
					
					
				}
			}
		}
	}
	
	private void moveToFlightScreenOrHomeOld()
	{
		trySetProgressDialog(true);
		
		if (getActivity() != null)
		{
			((EWBaseUserActivity) getActivity()).GetUserActivePNRs(new Response.Listener<ResponseGetUserActivePNRs>()
			{
				@Override
				public void onResponse(final ResponseGetUserActivePNRs iResponse)
				{
					if (iResponse != null && iResponse.getContent() != null)
					{
						handleResponseGetUserActivePNRs(iResponse.getContent());
					}
					trySetProgressDialog(false);
				}
			}, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError iError)
				{
					trySetProgressDialog(false);
					goToHomeFragment();
				}
			});
		}
	}
	
	private void moveToFlightScreenOrHome()
	{
		String activePnr = UserData.getInstance().getActivePnr();
		if (!TextUtils.isEmpty(activePnr))
		{
			ContinueToMyFlightActivity(activePnr);
		}
		else
		{
			goToHomeFragment();
		}
	}
	
	
	private void handleResponseGetUserActivePNRs(ArrayList<UserActivePnr> iUserActivePnrsList)
	{
		if (iUserActivePnrsList.isEmpty()) // user has no flights
		{
			goToHomeFragment();
		}
		else
		{
			UserActivePnr flight = null;
			String activeFlightPNR = UserData.getInstance().getActivePnr();
			if (activeFlightPNR != null && !TextUtils.isEmpty(activeFlightPNR))
			{
				flight = FlightUtils.getActiveFlightFromList(iUserActivePnrsList, activeFlightPNR);
			}
			
			if (flight != null)
			{
				ContinueToMyFlightActivity(flight.getPnr());
			}
			else
			{
				goToHomeFragment();
			}
		}
	}
	
	private void ContinueToMyFlightActivity(String iPnr)
	{
		try
		{
			Bundle bundle = new Bundle();
			bundle.putString(IntentExtras.USER_ACTIVE_PNR, iPnr);
			
			tryGoToScreenById(eInAppNavigation.MyFlights.getNavigationId(), bundle);
			
			getActivity().finish();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private void updateNavigationDrawer()
	{
		if (getActivity() != null)
		{
			((EWBaseDrawerActivity) getActivity()).setDrawerAdapter();
		}
	}
	
	
	@Override
	public void onErrorResponse(final VolleyError error)
	{
		//		setErrorMessage(getString(R.string.incorrect_username_or_password));
		
		trySetProgressDialog(false);
		AlertDialog dialog = AppUtils.createSimpleMessageDialog(getActivity(), getString(R.string.incorrect_username_or_password_matmid), getString(R.string.close), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, final int which)
			{
				dialog.dismiss();
				
			}
		}, null, null);
		dialog.setCancelable(false);
		dialog.show();
	}
	
	public boolean isEmailValid()
	{
		return AppUtils.isEmailValid(mEtEmail.getText().toString());
	}
	
	private boolean isPasswordValid(String iPassword, boolean iIsMatmid)
	{
		return AppUtils.isPasswordStrong(iPassword, iIsMatmid);
	}
	
	public boolean isMatmidNumberValid(String iMatmidNumber)
	{
		return AppUtils.isMatmidNumberValid(iMatmidNumber);
	}
}
