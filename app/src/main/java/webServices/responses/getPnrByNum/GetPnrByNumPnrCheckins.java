package webServices.responses.getPnrByNum;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetPnrByNumPnrCheckins
{
	@JsonProperty("CategoryId")
	private int mCategoryId;
	@JsonProperty("CheckInEligible")
	private boolean mCheckInEligible;
	@JsonProperty("PNR")
	private String mPNR;
	@JsonProperty("TicketNumber")
	private String mTicketNumber;
	@JsonProperty("SubText")
	private String mSubText;
	@JsonProperty("HasCheckin")
	private boolean mHasCheckin;
	@JsonProperty("Icon")
	private String mIcon;
	@JsonProperty("URL")
	private String mURL;
	
	public int getCategoryId()
	{
		return mCategoryId;
	}
	
	public boolean isCheckInEligible()
	{
		return mCheckInEligible;
	}
	
	public String getPNR()
	{
		return mPNR;
	}
	
	public String getTicketNumber()
	{
		return mTicketNumber;
	}
	
	public String getSubText()
	{
		return mSubText;
	}
	
	public boolean isHasCheckin()
	{
		return mHasCheckin;
	}
	
	public String getIcon()
	{
		return mIcon;
	}
	
	public String getURL()
	{
		return mURL;
	}
	
	public GetPnrByNumPnrCheckins()
	{
	}
}
