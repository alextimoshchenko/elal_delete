package webServices.responses.getAppData;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

import utils.global.ParseUtils;

public class AppContent implements Parcelable
{
	@JsonIgnore
	public static final int MEDIA_TYPE_BUTTON = 1;
	@JsonIgnore
	public static final int MEDIA_TYPE_IMAGE = 2;
	@JsonIgnore
	public static final int MEDIA_TYPE_VIDEO = 3;
	
	@JsonProperty("Description")
	private String mDescription;
	@JsonProperty("IsActive")
	private boolean mIsActive;
	@JsonProperty("Title")
	private String mTitle;
	@JsonProperty("AppContentID")
	private int mAppContentID;
	@JsonProperty("InApp")
	private boolean mInApp;
	@JsonProperty("Poped")
	private Date mPoped;
	@JsonProperty("Image")
	private String mImage;
	@JsonProperty("CreateDate")
	private Date mCreateDate;
	@JsonProperty("LastModified")
	private Date mLastModified;
	@JsonProperty("LanguageCode")
	private String mLanguageCode;
	@JsonProperty("MediaTypeID")
	private int mMediaTypeID;
	@JsonProperty("AppContentName")
	private String mAppContentName;
	@JsonProperty("ButtonBackground")
	private String mButtonBackground;
	@JsonProperty("TextColor")
	private String mTextColor;
	@JsonProperty("IsNewWindow")
	private boolean mIsNewWindow;
	@JsonProperty("Link")
	private String mLink;
	
	public AppContent()
	{
	}
	
	public String getDescription()
	{
		return mDescription;
	}
	
	public boolean isActive()
	{
		return mIsActive;
	}
	
	public String getTitle()
	{
		return mTitle;
	}
	
	public int getAppContentID()
	{
		return mAppContentID;
	}
	
	public boolean isInApp()
	{
		return mInApp;
	}
	
	public Date getPoped()
	{
		return mPoped;
	}
	
	public String getImage()
	{
		return mImage;
	}
	
	public Date getCreateDate()
	{
		return mCreateDate;
	}
	
	public Date getLastModified()
	{
		return mLastModified;
	}
	
	public String getLanguageCode()
	{
		return mLanguageCode;
	}
	
	public int getMediaTypeID()
	{
		return mMediaTypeID;
	}
	
	public String getAppContentName()
	{
		return mAppContentName;
	}
	
	public String getButtonBackground()
	{
		return mButtonBackground;
	}
	
	public String getTextColor()
	{
		return mTextColor;
	}
	
	public boolean isNewWindow()
	{
		return mIsNewWindow;
	}
	
	public String getLink()
	{
		return mLink;
	}
	
	public int getLinkInt()
	{
		return ParseUtils.tryParseStringToIntegerOrDefault(mLink, 0);
	}
	
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeString(this.mDescription);
		dest.writeByte(this.mIsActive ? (byte) 1 : (byte) 0);
		dest.writeString(this.mTitle);
		dest.writeInt(this.mAppContentID);
		dest.writeByte(this.mInApp ? (byte) 1 : (byte) 0);
		dest.writeLong(this.mPoped != null ? this.mPoped.getTime() : -1);
		dest.writeString(this.mImage);
		dest.writeLong(this.mCreateDate != null ? this.mCreateDate.getTime() : -1);
		dest.writeLong(this.mLastModified != null ? this.mLastModified.getTime() : -1);
		dest.writeString(this.mLanguageCode);
		dest.writeInt(this.mMediaTypeID);
		dest.writeString(this.mAppContentName);
		dest.writeString(this.mButtonBackground);
		dest.writeByte(this.mIsNewWindow ? (byte) 1 : (byte) 0);
		dest.writeString(this.mLink);
		dest.writeString(this.mTextColor);
	}
	
	protected AppContent(Parcel in)
	{
		this.mDescription = in.readString();
		this.mIsActive = in.readByte() != 0;
		this.mTitle = in.readString();
		this.mAppContentID = in.readInt();
		this.mInApp = in.readByte() != 0;
		long tmpMPoped = in.readLong();
		this.mPoped = tmpMPoped == -1 ? null : new Date(tmpMPoped);
		this.mImage = in.readString();
		long tmpMCreateDate = in.readLong();
		this.mCreateDate = tmpMCreateDate == -1 ? null : new Date(tmpMCreateDate);
		long tmpMLastModified = in.readLong();
		this.mLastModified = tmpMLastModified == -1 ? null : new Date(tmpMLastModified);
		this.mLanguageCode = in.readString();
		this.mMediaTypeID = in.readInt();
		this.mAppContentName = in.readString();
		this.mButtonBackground = in.readString();
		this.mIsNewWindow = in.readByte() != 0;
		this.mLink = in.readString();
		this.mTextColor = in.readString();
	}
	
	public static final Parcelable.Creator<AppContent> CREATOR = new Parcelable.Creator<AppContent>()
	{
		@Override
		public AppContent createFromParcel(Parcel source)
		{
			return new AppContent(source);
		}
		
		@Override
		public AppContent[] newArray(int size)
		{
			return new AppContent[size];
		}
	};
}
