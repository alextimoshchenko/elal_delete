package ui.fragments.notification;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.tagmanager.DataLayer;

import java.util.List;

import il.co.ewave.elal.R;
import interfaces.INotificationClickListener;
import jp.wasabeef.recyclerview.animators.FadeInLeftAnimator;
import ui.activities.EWBaseActivity;
import ui.activities.NotificationActivity;
import ui.adapters.NotificationInstancesAdapter;
import ui.fragments.EWBaseFragment;
import utils.errors.ErrorsHandler;
import utils.errors.LocalError;
import utils.global.AppUtils;
import utils.global.notificationObjects.NotificationObject;
import webServices.responses.deleteUserPushMessage.NotificationInstance;
import webServices.responses.deleteUserPushMessage.ResponseDeleteUserPushMessage;
import webServices.responses.deleteUserPushMessage.ResponseDeleteUserPushMessageContent;
import webServices.responses.deleteUserPushMessage.UserMessage;
import webServices.responses.setPushAsRead.ResponseSetPushAsRead;

/**
 * Created with care by Alexey.T on 25/07/2017.
 */
public class NotificationListFragment extends EWBaseFragment implements Response.ErrorListener
{
	private final String TAG = NotificationListFragment.class.getSimpleName();
	
	private TextView mTvHeaderScreenName;
	private ImageView mIvArrowBack;
	private RecyclerView mRvNotificationList;
	private NotificationInstancesAdapter mAdapter;
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.notification_list_fragment_layout);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		initReference();
		setListeners();
		//		AppUtils.changeLocale(this, eLanguage.English);
		pushScreenOpenEvent("My Notifications");
	}
	
	private void setListeners()
	{
		mIvArrowBack.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnBackClick();
			}
		});
	}
	
	private void doOnBackClick()
	{
		getActivity().onBackPressed();
	}
	
	private void setGui()
	{
		mTvHeaderScreenName.setText(R.string.my_notifications);
	}
	
	private void setAdapter()
	{
		NotificationObject notificationObject = NotificationObject.getInstance();
		List<UserMessage> mCurrentNotificationInstancesList = notificationObject.getCurrentUserMessageList();
		
		if (mCurrentNotificationInstancesList == null)
		{
			return;
		}
		
		if (mAdapter == null)
		{
			mAdapter = new NotificationInstancesAdapter(getActivity(), mCurrentNotificationInstancesList, new INotificationClickListener()
			{
				@Override
				public void onItemClick(NotificationInstance iNotificationInstance)
				{
					doOnItemClick(iNotificationInstance);
					
					//event 113
					int pushType = iNotificationInstance.getPushType();
					String mesName = iNotificationInstance.getMessageName();
					((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "My Massages", "Action", "Open Massage category - " + pushType, "Label", "Massage" + mesName));
				}
				
				@Override
				public void onDelete(NotificationInstance iNotificationInstance)
				{
					doOnDeleteClick(iNotificationInstance);
					
					//event 114
					int pushType = iNotificationInstance.getPushType();
					String mesName = iNotificationInstance.getMessageName();
					((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "My Massages", "Action", "Delit Massage - " + pushType, "Label", mesName));
				}
			});
			
			mRvNotificationList.setLayoutManager(new LinearLayoutManager(getActivity()));
			mRvNotificationList.setItemAnimator(new FadeInLeftAnimator());
			mRvNotificationList.setAdapter(mAdapter);
		}
		else
		{
			mAdapter.updateNotificationInstancesList(mCurrentNotificationInstancesList);
			mAdapter.notifyDataSetChanged();
		}
	}
	
	private void doOnDeleteClick(NotificationInstance iNotificationInstance)
	{
		if (isServiceConnected() && (isResumed() || isVisible()) && iNotificationInstance != null)
		{
			((NotificationActivity) getActivity()).deleteUserPushMessage(iNotificationInstance, new Response.Listener<ResponseDeleteUserPushMessage>()
			{
				@Override
				public void onResponse(final ResponseDeleteUserPushMessage response)
				{
					if (response != null)
					{
						ResponseDeleteUserPushMessageContent content = response.getContent();
						
						if (content != null && content.isDeleted())
						{
							handleDeleteResponse(response.getContent());
						}
					}
				}
			}, /*this*/new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError iVolleyError)
				{
				
				}
			});
		}
	}
	
	private void handleDeleteResponse(ResponseDeleteUserPushMessageContent iResponse)
	{
		updateMessageNumber();
		NotificationObject.getInstance().setCurrentUserMessageList(iResponse.getNotificationInstanceList());
		setAdapter();
	}
	
	private void doOnItemClick(final NotificationInstance iNotificationInstance)
	{
		if (iNotificationInstance == null)
		{
			AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.GeneralError.getErrorMessage());
			return;
		}
		
		if (iNotificationInstance.isRead())
		{
			setCurrentMessageAndOpenMessageScreen(iNotificationInstance);
		}
		else if (isResumed() || isVisible())
		{
			((NotificationActivity) getActivity()).setPushAsRead(iNotificationInstance, new Response.Listener<ResponseSetPushAsRead>()
			{
				@Override
				public void onResponse(final ResponseSetPushAsRead response)
				{
					iNotificationInstance.setRead(true);
					setCurrentMessageAndOpenMessageScreen(iNotificationInstance);
					updateMessageNumber();
				}
			}, /*this*/new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError iVolleyError)
				{
				
				}
			});
		}
	}
	
	private void updateMessageNumber()
	{
		((EWBaseActivity) getActivity()).getAndUpdateMessageNumber();
	}
	
	private void setCurrentMessageAndOpenMessageScreen(final NotificationInstance iNotificationInstance)
	{
		NotificationObject.getInstance().setCurrentNotificationMessage(iNotificationInstance);
		tryGoToFragmentByFragmentClass(NotificationFragment.class, Bundle.EMPTY, true);
	}
	
	private void initReference()
	{
		Activity act = getActivity();
		
		//header
		View v = act.findViewById(R.id.ll_frag_search_destination_header);
		mTvHeaderScreenName = (TextView) v.findViewById(R.id.tv_name_screen_title);
		mIvArrowBack = (ImageView) v.findViewById(R.id.iv_name_screen_back_arrow);
		
		//RecyclerView
		mRvNotificationList = (RecyclerView) act.findViewById(R.id.rv_notification_list);
	}
	
	@Override
	public void onErrorResponse(final VolleyError error)
	{
		trySetProgressDialog(false);
		ErrorsHandler.tryShowServiceErrorDialog(error, getActivity());
	}
	
	@Override
	public void onResume()
	{
		setGui();
		setAdapter();
		AppUtils.hideKeyboard(getContext(), getView());
		super.onResume();
	}
}
