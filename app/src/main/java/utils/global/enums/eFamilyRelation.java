package utils.global.enums;

/**
 * Created with care by Alexey.T on 23/07/2017.
 */
public enum eFamilyRelation
{
	FAMILY_ONLY,
	REGULAR_ONLY,
	ALL_KINDS;
	
	public boolean isFamilyOnly()
	{
		return this == FAMILY_ONLY;
	}
	
	public boolean isRegularOnly()
	{
		return this == REGULAR_ONLY;
	}
	
	public boolean isAllKinds()
	{
		return this == ALL_KINDS;
	}
}
