package webServices.responses;

import java.util.ArrayList;

import webServices.global.Response;
import webServices.responses.getAppData.AppContent;

/**
 * Created with care by Shahar Ben-Moshe on 02/02/17.
 */
public class ResponseGetAppContents extends Response<ArrayList<AppContent>>
{
	public boolean isContentValid()
	{
		return getContent() != null && getContent().size() > 0;
	}
}
