package ui.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import global.eInAppNavigation;
import il.co.ewave.elal.R;
import ui.customWidgets.ElalCustomTilLL;
import utils.global.AppUtils;
import utils.global.CustomTextWatcher;
import webServices.controllers.LoginController;
import webServices.requests.RequestResetPassword;
import webServices.responses.ResponseResetPassword;

/**
 * Created by Avishay.Peretz on 18/07/2017.
 */
public class ForgotPasswordFragment extends EWBaseFragment implements Response.ErrorListener
{
	private ImageView mBtnBackArrow;
	
//	private TextInputLayout mTilEmail;
	private ElalCustomTilLL mTilEmail;
	private EditText mEtEmail;
	private Button mBtnContinue;
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_forgot_password);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		initReference();
		setListeners();
		setGui();
	}
	
	
	private void initReference()
	{
		mBtnBackArrow = (ImageView) getActivity().findViewById(R.id.iv_name_screen_back_arrow);
//		mTilEmail = (TextInputLayout) getActivity().findViewById(R.id.til_forgot_password_Email);
		
		mTilEmail = (ElalCustomTilLL) getActivity().findViewById(R.id.til_forgot_password_Email);
		mTilEmail.init(getString(R.string.email_asterisk));
		mTilEmail.setInputTypeEmail();
		
		mEtEmail = mTilEmail.getEditText();
		mEtEmail.setImeOptions(EditorInfo.IME_ACTION_DONE);
//		mEtEmail = (EditText) getActivity().findViewById(R.id.et_forgot_password_Email);
		
		mBtnContinue = (Button) getActivity().findViewById(R.id.btn_forgot_password_Continue);
	}
	
	private void setListeners()
	{
		mBtnBackArrow.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				tryPopFragment();
			}
		});
		
		mEtEmail.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				showEmailError(false);
			}
		});
		
		mEtEmail.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus && !TextUtils.isEmpty(mEtEmail.getText().toString()) && !isEmailValid())
				{
					showEmailError(true);
				}
			}
		});
		
		mBtnContinue.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View iView)
			{
				hideKeyboard();
				doOnContinueClick();
			}
		});
		
	}
	
	private void doOnContinueClick()
	{
		if (isEmailValid())
		{
			if (isServiceConnected())
			{
				trySetProgressDialog(true);
				getService().getController(LoginController.class).ResetPassword(new RequestResetPassword(mEtEmail.getText().toString()), new Response.Listener<ResponseResetPassword>()
				{
					@Override
					public void onResponse(final ResponseResetPassword response)
					{
						trySetProgressDialog(false);
						openPasswordResetSuccessDialog();
					}
				}, this);
			}
		}
		else
		{
			showEmailError(true);
		}
	}
	
	private void setGui()
	{
		((TextView) getActivity().findViewById(R.id.tv_name_screen_title)).setText(getString(R.string.password_reset));
		mBtnBackArrow.setVisibility(View.VISIBLE);
	}
	
	private boolean isEmailValid()
	{
		return AppUtils.isEmailValid(mEtEmail.getText().toString());
	}
	
	private void showEmailError(boolean iState)
	{
		if (iState)
		{
			mTilEmail.setError(true, getActivity().getResources().getString(R.string.email_not_match));
		}
		else
		{
			mTilEmail.setError(false);
		}
	}
	
	private void openPasswordResetSuccessDialog()
	{
		if (getActivity() != null)
		{
			AlertDialog dialog = AppUtils.createSimpleMessageDialog(getActivity(), getResources().getString(R.string.password_reset_dialog_message), getResources().getString(R.string.approve), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(final DialogInterface dialog, final int which)
				{
					dialog.dismiss();
//					tryPopFragment();
					tryGoToScreenById(eInAppNavigation.Login.getNavigationId(), Bundle.EMPTY);
				}
			}, null, null);
			dialog.setCancelable(false);
			dialog.show();
		}
	}
	
	
	@Override
	public void onErrorResponse(final VolleyError iError)
	{
		trySetProgressDialog(false);
//		if (iError != null && iError instanceof ServerError && ((ServerError) iError).getErrorCode() == ServerError.eServerError.MobileUserNotFound.getErrorCode())
//		{
//			openPasswordResetSuccessDialog();
//		}
//		else
//		{
		openPasswordResetSuccessDialog();
//			ErrorsHandler.tryShowServiceErrorDialog(iError, getActivity());
//		}
	}
}
