package webServices.global;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Shahar Ben-Moshe on 5/25/15.
 */
public class Response<T>
{
	@JsonProperty("Status")
	private boolean mStatus;
	@JsonProperty("ErrorCode")
	private int mErrorCode;
	@JsonProperty("ErrorMessage")
	private String mErrorMessage;
	@JsonProperty("Content")
	private T mContent;
	
	public boolean isSuccess()
	{
		return mStatus;
	}
	
	public int getErrorCode()
	{
		return mErrorCode;
	}
	
	public String getErrorMessage()
	{
		return mErrorMessage;
	}
	
	public T getContent()
	{
		return mContent;
	}
}