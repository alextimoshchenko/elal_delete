package webServices.responses.responseGetRepresentationByIP;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetRepresentationByIpContent
{
	@JsonProperty("RepresentationNameHeb")
	private String mRepresentationNameHeb;
	@JsonProperty("RepresentationID")
	private int mRepresentationId;
	@JsonProperty("RepresentationNameEng")
	private String mRepresentationNameEng;
	@JsonProperty("CountryCode")
	private String mCountryCode;
	@JsonProperty("RepresentationName")
	private String mRepresentationName;
	
	public String getRepresentationNameHeb()
	{
		return this.mRepresentationNameHeb;
	}
	
	public int getRepresentationId()
	{
		return this.mRepresentationId;
	}
	
	public String getRepresentationNameEng()
	{
		return this.mRepresentationNameEng;
	}
	
	public String getCountryCode()
	{
		return this.mCountryCode;
	}
	
	public String getRepresentationName()
	{
		return this.mRepresentationName;
	}
	
	public GetRepresentationByIpContent()
	{
	}
}
