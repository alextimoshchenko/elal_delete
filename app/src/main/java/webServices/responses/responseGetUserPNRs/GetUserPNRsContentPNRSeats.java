package webServices.responses.responseGetUserPNRs;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetUserPNRsContentPNRSeats
{
	@JsonProperty("CategoryId")
	private int mCategoryId;
	@JsonProperty("PNR")
	private String mPNR;
	@JsonProperty("TicketNumber")
	private String mTicketNumber;
	@JsonProperty("SubText")
	private String mSubText;
	@JsonProperty("HasEligibleSeats")
	private boolean mHasEligibleSeats;
	@JsonProperty("HasSeats")
	private boolean mHasSeats;
	@JsonProperty("Icon")
	private String mIcon;
	@JsonProperty("URL")
	private String mURL;
	
	public int getCategoryId()
	{
		return mCategoryId;
	}
	
	public String getPNR()
	{
		return mPNR;
	}
	
	public String getTicketNumber()
	{
		return mTicketNumber;
	}
	
	public String getSubText()
	{
		return mSubText;
	}
	
	public boolean isHasEligibleSeats()
	{
		return mHasEligibleSeats;
	}
	
	public boolean isHasSeats()
	{
		return mHasSeats;
	}
	
	public String getIcon()
	{
		return mIcon;
	}
	
	public String getURL()
	{
		return mURL;
	}
	
	public GetUserPNRsContentPNRSeats()
	{
	}
}
