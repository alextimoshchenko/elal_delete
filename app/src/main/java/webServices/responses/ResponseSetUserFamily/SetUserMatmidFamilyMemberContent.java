package webServices.responses.ResponseSetUserFamily;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class SetUserMatmidFamilyMemberContent
{
	@JsonProperty("IsOK")
	private boolean mIsOK;
	
	@JsonProperty("FamilyMembers")
	private ArrayList<SetMatmidUserFamilyContentAnswer> mMatmidFamilyMembers;
	
	public SetUserMatmidFamilyMemberContent()
	{
	}
	
	@JsonIgnore
	public ArrayList<SetMatmidUserFamilyContentAnswer> getMatmidFamilyMembers()
	{
		return mMatmidFamilyMembers;
	}
	
	@JsonIgnore
	public boolean isOK()
	{
		return mIsOK;
	}
}
