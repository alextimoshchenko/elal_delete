package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.UserData;
import utils.global.searchFlightObjects.SearchObject;

/**
 * Created with care by Alexey.T on 17/07/2017.
 */
public class RequestSearchDestinationCities
{
	@JsonProperty("UserId")
	private int mUserId;
	@JsonProperty("LanguageCode")
	private String mLanguageCode = UserData.getInstance().getLanguage().getLanguageCode();
	@JsonProperty("CityCode")
	private String mCityCode;
	@JsonProperty("FlightType")
	private String mFlightType = SearchObject.getInstance().getCurrentTabStatement().getStatementCode();
	
	public RequestSearchDestinationCities()
	{
	}
	
	public RequestSearchDestinationCities(final int iUserId,  final String iCityCode)
	{
		mUserId = iUserId;
		mCityCode = iCityCode;
	}
}
