package ui.customWidgets;

import android.content.Context;
import android.graphics.PointF;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.DisplayMetrics;

/**
 * Created by erline.katz on 14/08/2017.
 */

public class CustomLinearLayoutManager extends LinearLayoutManager
{
	
	private static final float MILLISECONDS_PER_INCH = 25f; //default is 25f (bigger = slower)
	
	public CustomLinearLayoutManager(Context context)
	{
		super(context);
	}
	
	public CustomLinearLayoutManager(Context context, int orientation, boolean reverseLayout)
	{
		super(context, orientation, reverseLayout);
	}
	
	public CustomLinearLayoutManager(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
	{
		super(context, attrs, defStyleAttr, defStyleRes);
	}
	
	@Override
	public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position)
	{
		
		final LinearSmoothScroller linearSmoothScroller = new LinearSmoothScroller(recyclerView.getContext())
		{
			
			@Override
			public PointF computeScrollVectorForPosition(int targetPosition)
			{
				return CustomLinearLayoutManager.this.computeScrollVectorForPosition(targetPosition);
			}
			
			@Override
			protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics)
			{
				return MILLISECONDS_PER_INCH / displayMetrics.densityDpi;
			}
		};
		
		linearSmoothScroller.setTargetPosition(position);
		startSmoothScroll(linearSmoothScroller);
	}
}