package webServices.responses.getHomeScreenContent;

import com.fasterxml.jackson.annotation.JsonProperty;


public class HomeScreenContentOld
{
	@JsonProperty("LeftContent")
	private HomeScreenItem mLeftItem;
	@JsonProperty("RightContent")
	private HomeScreenItem mRightItem;
	@JsonProperty("TopContent")
	private HomeScreenItem mTopItem;
	@JsonProperty("BottomContent")
	private HomeScreenItem mBottomItem;
	@JsonProperty("PNRCount")
	private int mPNRCount;
	
	public HomeScreenContentOld()
	{
	}
	
	public HomeScreenItem getLeftItem()
	{
		return mLeftItem;
	}
	
	public HomeScreenItem getRightItem()
	{
		return mRightItem;
	}
	
	public HomeScreenItem getTopItem()
	{
		return mTopItem;
	}
	
	public HomeScreenItem getBottomItem()
	{
		return mBottomItem;
	}
	
	public int getPNRCount()
	{
		return mPNRCount;
	}
}

