package webServices.responses.getSearchHistory;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseGetSearchHistoryContentFamily
{
	@JsonProperty("FlightSearchID")
	private int mFlightSearchID;
	@JsonProperty("UserID")
	private int mUserID;
	@JsonProperty("FamilyID")
	private int mFamilyID;
	
	public ResponseGetSearchHistoryContentFamily()
	{
	}
	
	public int getFlightSearchID()
	{
		return this.mFlightSearchID;
	}
	
	public int getUserID()
	{
		return this.mUserID;
	}
	
	public int getFamilyID()
	{
		return this.mFamilyID;
	}
	
	@Override
	public boolean equals(final Object obj)
	{
		ResponseGetSearchHistoryContentFamily historyContentFamily;
		
		if (obj instanceof ResponseGetSearchHistoryContentFamily)
		{
			historyContentFamily = (ResponseGetSearchHistoryContentFamily) obj;
		}
		else
		{
			return false;
		}
		
		boolean isFlightSearchEquals = this.mFlightSearchID == historyContentFamily.mFlightSearchID;
		boolean isUserIdEquals = this.mUserID == historyContentFamily.mUserID;
		boolean isFamilyId = this.mFamilyID == historyContentFamily.mFamilyID;
		
		return isFlightSearchEquals && isUserIdEquals && isFamilyId;
	}
}
