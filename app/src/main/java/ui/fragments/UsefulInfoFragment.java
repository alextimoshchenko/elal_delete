package ui.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import global.IntentExtras;
import global.UserData;
import il.co.ewave.elal.R;
import ui.adapters.WeatherAdapter;
import ui.customWidgets.TouchableWebView;
import utils.errors.ErrorsHandler;
import utils.global.AppUtils;
import webServices.controllers.MainController;
import webServices.global.JacksonRequest;
import webServices.global.ePnrFlightType;
import webServices.requests.RequestGetToSeeLinkByDestination;
import webServices.responses.ResponseGetToSeeLinkByDestination;
import webServices.responses.getWeather.Weather;
import webServices.responses.responseGetUserActivePNRs.PnrFlight;
import webServices.responses.responseGetUserActivePNRs.UserActivePnr;

/**
 * Created with care by Shahar Ben-Moshe on 31/01/17.
 */

public class UsefulInfoFragment extends EWBaseFragment implements Response.ErrorListener
{
	private String mUrl;
	private RecyclerView mRvWeather;
//	private TouchableWebView mWebView;
	private TouchableWebView mWebView;
	private LinearLayout mLlProgressDialog;
	private String mDestinationIata;
	private Weather mWeather;
	private WeatherAdapter mWeatherAdapter;
	private TextView mUsefulInfoTitle;
	private String mDestinationCityName;
	private String mDestinationInfo;
	private PnrFlight mCurrentFlight;
	
	public static Bundle createBundleForFragment(String iDestinationIata, Weather iWeather, @NonNull final PnrFlight iUserActiveFlight, String iDestinationName, String iDestinationInfo)
	{
		Bundle result = new Bundle();

		result.putString(IntentExtras.DESTINATION_IATA, iDestinationIata);
		result.putParcelable(IntentExtras.WEATHER, iWeather);
//		result.putParcelable(IntentExtras.USER_ACTIVE_PNR, iUserActiveFlight);
		result.putString(IntentExtras.DESTINATION_NAME, iDestinationName);
		result.putString(IntentExtras.DESTINATION_INFO, iDestinationInfo);
		
		return result;
	}
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_useful_info_layout);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		getExtras();
		initReference();
		initData();
//		setWebView();
		setProgressBar(false);
		setWeatherAdapter();
		setDestinationInfo();
	}
	
	private void initData()
	{
		mUsefulInfoTitle.setText(getResources().getString(R.string.weather_in, mDestinationCityName));
	}
	
	private void getExtras()
	{
		Bundle extras = getArguments();
		
		if (extras != null)
		{
//			mCurrentFlight = getArguments().getParcelable(IntentExtras.USER_ACTIVE_PNR);
			mDestinationIata = extras.getString(IntentExtras.DESTINATION_IATA);
			mWeather = extras.getParcelable(IntentExtras.WEATHER);
			
			mDestinationCityName = extras.getString(IntentExtras.DESTINATION_NAME);
			mDestinationInfo = extras.getString(IntentExtras.DESTINATION_INFO);
			
//
//			if (mCurrentFlight != null)
//			{
//				mDestinationCityName = mCurrentFlight.getDestinationCityName();
//				mDestinationInfo = mCurrentFlight.getDestinationInfo();
//			}
		}
	}
	
	private void initReference()
	{
		mLlProgressDialog = (LinearLayout) getActivity().findViewById(R.id.ll_usefulInfo_loader);
		mRvWeather = (RecyclerView) getActivity().findViewById(R.id.rv_usefulInfo_Weather);
		mWebView = (TouchableWebView) getActivity().findViewById(R.id.wv_usefulInfo_webView);
		mUsefulInfoTitle = (TextView) getActivity().findViewById(R.id.tv_UsefulInfoTitle);
	}
	
	private void setWebView()
	{
		mWebView.setWebViewClient(new WebViewClient()
		{
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url)
			{
				if (!TextUtils.isEmpty(url) && !url.startsWith("js:"))
				{
					view.loadUrl(url);
					
					if (url.toLowerCase().contains(".pdf"))
					{
						view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
					}
				}
				
				return true;
			}
			
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon)
			{
				setProgressBar(true);
				
				super.onPageStarted(view, url, favicon);
			}
			
			@Override
			public void onPageFinished(WebView view, String url)
			{
				setProgressBar(false);
				
				super.onPageFinished(view, url);
			}
		});
		
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.setWebChromeClient(new WebChromeClient());
		mWebView.getSettings().setDomStorageEnabled(true);
		mWebView.getSettings().setBuiltInZoomControls(true);
		mWebView.getSettings().setDisplayZoomControls(false);
		mWebView.getSettings().setUserAgentString(JacksonRequest.USER_AGENT_CONTENT);
		
		mWebView.setFocusable(false);
	}
	
	private void setDestinationInfo()
	{
		if (mDestinationInfo != null && !mDestinationInfo.equals(""))
		{
			mWebView.setVisibility(View.VISIBLE);
			
			mWebView.getSettings().setBuiltInZoomControls(true);
			mWebView.getSettings().setDisplayZoomControls(false);
			mWebView.getSettings().setAllowUniversalAccessFromFileURLs(true);
			mWebView.getSettings().setLoadWithOverviewMode(true);
			mWebView.getSettings().setJavaScriptEnabled(true);
			mWebView.getSettings().setDefaultTextEncodingName("utf-8");
			mWebView.setHorizontalScrollBarEnabled(false);
			mWebView.getSettings().setDomStorageEnabled(true);
			mWebView.loadDataWithBaseURL(null, mDestinationInfo, "text/html; charset=utf-8", "utf-8", null);
//			mWebView.loadData(mDestinationInfo, "text/html; charset=utf-8", "utf-8");
			// TODO: 03/08/2017 erline - ask Reuven to add type="text/css" in html
//			mWebView.loadUrl("file:///android_asset/new york.html");
		}
		else
		{
			mWebView.setVisibility(View.INVISIBLE);
			Log.d(this.getTag(), "mDestinationInfo is empty: " + mDestinationInfo);
		}
	}
	
	private void setProgressBar(boolean iIsShow)
	{
		mLlProgressDialog.setVisibility(iIsShow ? View.VISIBLE : View.GONE);
	}
	
	private void setWeatherAdapter()
	{
		if (mWeather != null)
		{
			mUsefulInfoTitle.setVisibility(View.VISIBLE);
			if (mWeatherAdapter == null)
			{
				try
				{
					mWeatherAdapter = new WeatherAdapter(getService().getImageLoader(), mWeather, getContext());
					mRvWeather.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
					mRvWeather.setAdapter(mWeatherAdapter);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				mWeatherAdapter.notifyDataSetChanged();
			}
		}
		else
		{
			mUsefulInfoTitle.setVisibility(View.GONE);
		}
	}
	
	@Override
	public void onServiceConnected()
	{
//		getToSeeLinkByDestination();
	}
	
//	private void getToSeeLinkByDestination()
//	{
//		if (isServiceConnected())
//		{
//			trySetProgressDialog(true);
//			getService().getController(MainController.class)
//			            .getGetToSeeLinkByDestination(new RequestGetToSeeLinkByDestination(UserData.getInstance()
//			                                                                                       .getUserID(), mDestinationIata), new Response.Listener<ResponseGetToSeeLinkByDestination>()
//			            {
//				            @Override
//				            public void onResponse(final ResponseGetToSeeLinkByDestination iResponse)
//				            {
//					            if (iResponse != null)
//					            {
//						            mUrl = iResponse.getContent();
////						            loadUrl();
//					            }
//					            else
//					            {
//						            // TODO: 20/02/17 Shahar handle error?
//					            }
//
//					            trySetProgressDialog(false);
//				            }
//			            }, this);
//		}
//	}
	
//	private void loadUrl()
//	{
//		if (!TextUtils.isEmpty(mUrl))
//		{
//			mWebView.loadUrl(AppUtils.addHttpToStringIfNeed(mUrl));
//		}
//	}
	
	@Override
	public void onErrorResponse(final VolleyError error)
	{
		ErrorsHandler.tryShowServiceErrorDialog(error, getActivity());
	}
}
