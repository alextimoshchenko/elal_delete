package ui.activities;


import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.tagmanager.DataLayer;

import java.util.Date;

import global.AppData;
import global.ElAlApplication;
import global.IntentExtras;
import global.UserData;
import global.eActivityModule;
import global.eInAppNavigation;
import global.eLanguage;
import il.co.ewave.elal.R;
import interfaces.IElalScreensNavigator;
import ui.adapters.MainDrawerAdapter;
import ui.customWidgets.AnimatedExpandableListView;
import ui.fragments.details.DetailScreenFragment;
import ui.fragments.EWBaseFragment;
import ui.fragments.LoginFragment;
import ui.fragments.RegistrationFragment;
import ui.fragments.SettingsFragment;
import ui.fragments.UserFamilyFragment;
import ui.fragments.WebViewFragment;
import utils.errors.ErrorsHandler;
import utils.errors.LocalError.eLocalError;
import utils.global.AppUtils;
import utils.global.DateTimeUtils;
import utils.global.ElalPreferenceUtils;
import utils.global.ParseUtils;
import utils.global.enums.eSideMenuItems;
import utils.global.googleAnalitics.googleTagManager.TagManagerImplementation.ePushEvents;
import webServices.controllers.LoginController;
import webServices.controllers.LoginControllerMatmid;
import webServices.global.RequestStringBuilder;
import webServices.requests.RequestLogout;
import webServices.responses.ResponseLogout;
import webServices.responses.ResponseUserLogin;
import webServices.responses.getSideMenuItems.SideMenuCategory;
import webServices.responses.getSideMenuItems.SideMenuItem;
import webServices.responses.responseUserLogin.UserObject;

import static ui.activities.MainActivity.LOGIN_TYPE_MATMID_CLUB_MEMBER;

public abstract class EWBaseDrawerActivity extends EWBaseActivity implements IElalScreensNavigator, Response.ErrorListener
{
	private final String TAG = EWBaseDrawerActivity.class.getSimpleName();
	private static final int MATMID_TIMER = 15 * 60 * 1000;
	//			private static final int MATMID_TIMER = 2 * 60 * 1000;
	private static final int MATMID_MAX_SESSION_RESTORES = 3;
	private static final long EXIT_TIME = 2000;
	private static boolean mKillMatmidRunnable = false;
	
	private Toolbar mToolbar;
	private DrawerLayout mDrawerLayout;
	private AnimatedExpandableListView mElvDrawer;
	private ImageView mIvToolbarDrawer;
	private ImageView mIvToolbarLogo;
	private FrameLayout mFlContainer;
	private MainDrawerAdapter mDrawerAdapter;
	protected boolean mShouldExit;
	private String mUrl = "";
	
	@Override
	protected void initScreen()
	{
		//TODO avishay 12/12/17 restart the app if userObject = null
		/**
		 * if App process killed, check if user is empty. then restart the app.
		 **/
		if (AppData.getInstance().getModuleId() <= 0)
		{
			restartAppIfNeed();
		}
		/***/
		
		setCurrentActivityModuleId();
		initReference();
		setListeners();
		setContentDescription();
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		setCurrentActivityModuleId();
	}
	
	@Override
	protected void onRestart()
	{
		super.onRestart();
		Log.i("avishay test", "onRestart: user.isUserGuest() = " + UserData.getInstance().isUserGuest());
		Log.i("avishay test", "onRestart: user.getUserID() = " + UserData.getInstance().getUserID());
		Log.i("avishay test", "onRestart: UserData:  " + UserData.getInstance().toString());
		
		//		if (UserData.getInstance() == null || UserData.getInstance().getUserObject() == null/*user.isUserGuest() && user.getUserID() <= 0*/)
		//		{
		//			//					doOnLogoutClick();
		//			Log.i("avishay test", "user.isUserGuest() = true");
		//			startActivity(new Intent(this, SplashActivity.class));
		//			finishAffinity();
		//			ElAlApplication.setAppWasInBackground(false);
		//			return;
		//		}
		
		if (ElAlApplication.isAppWasInBackground())
		{
			// TODO: 25/10/2017 - if user matmid is signed in and passed more than 15 min. since last session update
			UserData user = UserData.getInstance();
			if (user != null && user.getUserObject() != null && user.getUserObject().isMatmid() && user.getLastSessionUpdateDate() != null)
			{
				long timeDifference = DateTimeUtils.getTimeDifferenceInMilliseconds(user.getLastSessionUpdateDate(), new Date());
				Log.d(TAG, "timeDifference from last session update: " + timeDifference + " MATMID_TIMER in millis: " + MATMID_TIMER);
				if (timeDifference > MATMID_TIMER)
				{
					
					doOnLogoutClick();
				}
				else
				{
					ElAlApplication.setAppWasInBackground(false);
					mKillMatmidRunnable = false;
					// TODO: 27/11/2017 - keep counting MATMID_TIMER from last call.
					//					doKeepAlive();
				}
			}
			else
			{
				if (getFragmentSwapper().getTopFragment() instanceof LoginFragment)
				{
					return;
				}
				else
				{
					
					
					if (getFragmentSwapper() != null && getFragmentSwapper().getTopFragment() != null && (getFragmentSwapper().getTopFragment() instanceof LoginFragment || getFragmentSwapper().getTopFragment() instanceof RegistrationFragment || getFragmentSwapper()
							.getTopFragment() instanceof DetailScreenFragment/*scan*/ || getFragmentSwapper().getTopFragment() instanceof UserFamilyFragment /*scan*/))
					{
						return;
					}
					restartAppIfNeed();
				}
			}
			ElAlApplication.setAppWasInBackground(false);
		}
		else
		{
			
			if (getFragmentSwapper() != null && getFragmentSwapper().getTopFragment() != null && (getFragmentSwapper().getTopFragment() instanceof LoginFragment || getFragmentSwapper().getTopFragment() instanceof RegistrationFragment || getFragmentSwapper()
					.getTopFragment() instanceof DetailScreenFragment/*scan*/ || getFragmentSwapper().getTopFragment() instanceof UserFamilyFragment /*scan*/))
			{
				return;
			}
			restartAppIfNeed();
			
			return;
			
		}
	}
	
	private void restartAppIfNeed()
	{
		//		if (getFragmentSwapper() != null && getFragmentSwapper().getTopFragment() != null && (getFragmentSwapper().getTopFragment() instanceof LoginFragment || getFragmentSwapper().getTopFragment() instanceof RegistrationFragment || getFragmentSwapper()
		//				.getTopFragment() instanceof DetailScreenFragment/*scan*/ || getFragmentSwapper().getTopFragment() instanceof UserFamilyFragment /*scan*/))
		//		{
		//			return;
		//		}
		if (AppData.getInstance().getModuleId() > 0)
		{
			return;
		}
		if (UserData.getInstance() == null || UserData.getInstance().getUserObject() == null/*user.isUserGuest() && user.getUserID() <= 0*/)
		{
			//						startActivity(new Intent(this, SplashActivity.class));
			//						finishAffinity();
			
			Intent intent = new Intent(this, SplashActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // required when starting from Application
			startActivity(intent);
			
			System.exit(1); // kill off the crashed app
		}
	}
	
	protected abstract void setCurrentActivityModuleId();
	
	private void setContentDescription()
	{
		mIvToolbarDrawer.setContentDescription(getString(R.string.menu));
	}
	
	private void initReference()
	{
		mToolbar = (Toolbar) findViewById(R.id.tb_application_Toolbar);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.dl_main_root);
		mElvDrawer = (AnimatedExpandableListView) findViewById(R.id.elv_main_drawer);
		mIvToolbarDrawer = (ImageView) mToolbar.findViewById(R.id.img_actionBar_drawer);
		mIvToolbarLogo = (ImageView) mToolbar.findViewById(R.id.img_actionBar_logo);
		mFlContainer = (FrameLayout) findViewById(R.id.fl_main_Container);
	}
	
	protected void initDrawer()
	{
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, null, 0, 0);
		mDrawerLayout.addDrawerListener(toggle);
		toggle.syncState();
		setDrawerAdapter();
	}
	
	private ExpandableListView.OnGroupClickListener mGroupListener = new ExpandableListView.OnGroupClickListener()
	{
		@Override
		public boolean onGroupClick(final ExpandableListView parent, final View v, final int groupPosition, final long id)
		{
			
			if (mDrawerAdapter == null)
			{
				mElvDrawer.collapseGroupWithAnimation(groupPosition);
				return true;
			}
			
			SideMenuCategory sideMenuCategory = mDrawerAdapter.getGroup(groupPosition);
			
			if (sideMenuCategory == null)
			{
				mElvDrawer.collapseGroupWithAnimation(groupPosition);
				return true;
			}
			
			String categoryName = sideMenuCategory.getCategoryName();
			
			if (categoryName.equals(eSideMenuItems.MY_ACCOUNT.getItemName()))
			{
				//event 72
				pushEvent(ePushEvents.MY_ACCOUNT);
				
				if (UserData.getInstance().isUserGuest())
				{
					openAskLoginDialog();
					closeDrawer();
					mElvDrawer.collapseGroupWithAnimation(groupPosition);
					return true;
				}
			}
			else if (categoryName.equals(eSideMenuItems.MY_FLIGHT.getItemName()))
			{
				//event 79
				pushEvent(ePushEvents.MY_FLIGHT);
			}
			else if (categoryName.equals(getString(R.string.plan_vacation)))
			{
				//event 84
				pushEvent(ePushEvents.PLAN_VACATION);
			}
			else if (categoryName.equals(getString(R.string.travel_info)))
			{
				//event 91
				pushEvent(ePushEvents.TRAVEL_INFO);
			}
			
			if (mElvDrawer.isGroupExpanded(groupPosition))
			{
				mElvDrawer.collapseGroupWithAnimation(groupPosition);
			}
			else
			{
				mElvDrawer.expandGroupWithAnimation(groupPosition);
			}
			
			return true;
		}
	};
	
	public void setDrawerAdapter()
	{
		AppData appData = AppData.getInstance();
		
		if (mDrawerAdapter == null)
		{
			mDrawerAdapter = new MainDrawerAdapter(this);
			mDrawerAdapter.setPermanentSideMenuCategories(appData.getPermanentSideMenuCategories(this));
			mDrawerAdapter.setSideMenuCategories(AppData.getInstance().getSideMenuCategories());
			mElvDrawer.setAdapter(mDrawerAdapter);
			mElvDrawer.addHeaderView(mDrawerAdapter.setHeaderView(createSideMenuHeader()));
			mElvDrawer.addFooterView(mDrawerAdapter.setFooterView(createSideMenuFooter()));
			mElvDrawer.setOnGroupClickListener(mGroupListener);
			mElvDrawer.setOnChildClickListener(mChildListener);
			mElvDrawer.setChildDivider(R.color.black);
		}
		else
		{
//			//			mDrawerAdapter.setSideMenuCategories(appData.getSideMenuCategories());
//			mDrawerAdapter.setSideMenuCategories(appData.getSideMenuCategories());
//			mDrawerAdapter.setPermanentSideMenuCategories(appData.getPermanentSideMenuCategories(this));
//			//			mDrawerAdapter.setSideMenuCategories(AppData.getInstance().getSideMenuCategories());
//			//			mDrawerAdapter.setSideMenuCategories(appData.getSideMenuCategories());
//
////			mElvDrawer.setAdapter(mDrawerAdapter);
			
 			mDrawerAdapter.setPermanentSideMenuCategories(appData.getPermanentSideMenuCategories(this));
			mDrawerAdapter.setSideMenuCategories(AppData.getInstance().getSideMenuCategories());
//			mElvDrawer.setAdapter(mDrawerAdapter);
			
			mElvDrawer.removeHeaderView(mDrawerAdapter.getHeaderView());
			mElvDrawer.removeFooterView(mDrawerAdapter.getFooterView());
			mElvDrawer.addHeaderView(mDrawerAdapter.setHeaderView(createSideMenuHeader()));
			mElvDrawer.addFooterView(mDrawerAdapter.setFooterView(createSideMenuFooter()));
//			mDrawerAdapter.notifyDataSetChanged();
		}
		
		mDrawerAdapter.expandPermanentGroups(mElvDrawer);
		setUnreadPushes(appData.getUnReadPushes());
		notifyPushesIndicator();
	}
	
	public void setUnreadPushes(int iNumber)
	{
		if (mDrawerAdapter != null)
		{
			mDrawerAdapter.setUnReadPushes(iNumber);
		}
	}
	
	public void notifyPushesIndicator()
	{
		if (mDrawerAdapter != null)
		{
			mDrawerAdapter.notifyPushesIndicator();
		}
	}
	
	private ExpandableListView.OnChildClickListener mChildListener = new ExpandableListView.OnChildClickListener()
	{
		@Override
		public boolean onChildClick(final ExpandableListView parent, final View v, final int groupPosition, final int childPosition, final long id)
		{
			handleSideMenuChildClick(groupPosition, childPosition);
			closeDrawer();
			return true;
		}
	};
	
	private void handleSideMenuChildClick(final int iGroupPosition, final int iChildPosition)
	{
		if (mDrawerAdapter == null)
		{
			return;
		}
		
		SideMenuItem sideMenuItem = mDrawerAdapter.getChild(iGroupPosition, iChildPosition);
		
		if (sideMenuItem == null)
		{
			return;
		}
		
		String itemName = sideMenuItem.getSideMenuName();
		
		if (sideMenuItem.isInApp())
		{
			UserData userData = UserData.getInstance();
			UserObject userObject = userData.getUserObject();
			
			int inAppNavigationId = ParseUtils.tryParseStringToIntegerOrDefault(sideMenuItem.getItemUrl(), -1);
			
			boolean isUserPassedDetailScreen = false;
			
			if (userObject != null)
			{
				isUserPassedDetailScreen = userObject.isUserPassedDetailScreen();
			}
			
			if (inAppNavigationId > -1)
			{
				eInAppNavigation navigation = eInAppNavigation.getInAppNavigationByIdOrErrorDefault(inAppNavigationId);
				
				//analytics
				pushEvent(itemName, navigation);
				
				if (navigation.isWebView())
				{
					goToWebFragment(sideMenuItem.getExternalUrl());
				}
				else if (userData.isUserGuest() && navigation.isPersonal())
				{
					openAskLoginDialog();
				}
				else if (navigation.isFamilyScreen() && !isUserPassedDetailScreen)
				{
					openAskUpdateDetailDialog();
				}
				else if (userObject != null && userObject.isMatmid() && navigation.isChangePassword())
				{
					goToWebFragment(sideMenuItem.getExternalUrl());
				}
				else
				{
					goToScreenById(inAppNavigationId/*, eActivityModule.getNavigationActivityClassByIdOrMainDefault(ElAlApplication.getInstance().getModuleId())*/, null);
				}
			}
		}
		else
		{
			UserObject userObject = UserData.getInstance().getUserObject();
			
			if (itemName.equals(getString(R.string.book_a_hotel)))
			{
				//event 86
				pushEvent(ePushEvents.BOOK_HOTEL);
			}
			else if (itemName.equals(getString(R.string.rent_a_car)))
			{
				//event 87
				pushEvent(ePushEvents.RENT_CAR);
			}
			
			if (userObject != null && userObject.isMatmid() && sideMenuItem.isMatmidIsNewWindow() || sideMenuItem.isNewWindow())
			{
				goToBrowserByUrl(sideMenuItem.getItemUrl());
			}
			else
			{
				if (!TextUtils.isEmpty(sideMenuItem.getItemUrl()))
				{
					goToWebFragment(sideMenuItem.getItemUrl());
				}
			}
		}
	}
	
	private void pushEvent(final String iItemName, final eInAppNavigation iNavigation)
	{
		switch (iNavigation)
		{
			case UserInfo:
			{
				//event 73
				pushEvent(ePushEvents.UPDATE_PERSONAL_DETAILS);
				break;
			}
			case FamilyScreen:
			{
				//event 74
				pushEvent(ePushEvents.UPDATE_FAMILY_ACCOUNT);
				break;
			}
			case ChangePassword:
			{
				//event 75
				pushEvent(ePushEvents.CHANGE_PASSWORD);
				break;
			}
			case MyDocuments:
			{
				//event 76
				pushEvent(ePushEvents.MY_DOCUMENTS);
				break;
			}
			case MyFlight:
			{
				//event 80
				String activePnr = UserData.getInstance().getActivePnr();
				pushCustomEvent(DataLayer.mapOf("Category", "Side Menu", "Action", "My Flight", "Label", "Manage My Flight >> " + activePnr));
				break;
			}
			case AddNewFlightFragment:
			{
				//event 81
				pushEvent(ePushEvents.ADD_FLIGHT_SIDE_MENU);
				break;
			}
			case FlightSearch:
			{
				//event 85
				pushEvent(ePushEvents.ADD_FLIGHT_SIDE_MENU);
				break;
			}
			case WebView:
			{
				if (iItemName.equals(getString(R.string.account_status)))
				{
					//event 77
					pushEvent(ePushEvents.ACCOUNT_BALANCE);
				}
				else if (iItemName.equals(getString(R.string.adding_missing_flight)))
				{
					//event 78
					pushEvent(ePushEvents.ADD_MISSING_FLIGHT);
				}
				else if (iItemName.equals(getString(R.string.check_in)))
				{
					//event 82
					pushEvent(ePushEvents.CHECK_IN_SIDE_MENU);
				}
				else if (iItemName.equals(getString(R.string.cancel_check_in_underline)))
				{
					//event 83
					pushEvent(ePushEvents.CANCEL_CHECK_IN_SIDE_MENU);
				}
				else if (iItemName.equals(getString(R.string.all_about_luggage)))
				{
					//event 92
					pushEvent(ePushEvents.ALL_ABOUT_LUGGAGE);
				}
				
				break;
			}
		}
	}
	
	//	@Override
	public void goToBrowserByUrl(String iUrl)
	{
		if (!iUrl.startsWith("http://") && !iUrl.startsWith("https://"))
		{
			iUrl = "http://" + iUrl;
		}
		
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(iUrl));
		startActivity(browserIntent);
	}
	
	private void setListeners()
	{
		mIvToolbarDrawer.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				AppUtils.hideKeyboard(EWBaseDrawerActivity.this, v);
				openDrawer();
			}
		});
		
		mIvToolbarLogo.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				AppUtils.hideKeyboard(EWBaseDrawerActivity.this, v);
				onLogoClick();
			}
		});
	}
	
	private void onLogoClick()
	{
		loadHomeFragment();
	}
	
	public void loadHomeFragment()
	{
		try
		{
			getFragmentSwapper().clearFragments();//TODO avishay 31/10/17, mark this line and check it
//			getFragmentSwapper().clearStack();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		goToScreenById(eInAppNavigation.Main.getNavigationId(), Bundle.EMPTY);
	}
	
	public void loadDetailsMatmidFragment()
	{
		getFragmentSwapper().clearFragments();
		
		Bundle bundle = new Bundle();
		bundle.putSerializable(IntentExtras.IS_MATMID, true);
		
		goToScreenById(eInAppNavigation.UserInfo.getNavigationId(), bundle);
	}
	
	private void openDrawer()
	{
		mDrawerLayout.openDrawer(GravityCompat.END);
	}
	
	private View createSideMenuFooter()
	{
		boolean isHebrewLanguage = UserData.getInstance().getLanguage().isHebrew();
		View footer = LayoutInflater.from(this).inflate(R.layout.side_menu_footer, null, false);
		
		//Terms of use
		View termsOfUse = footer.findViewById(R.id.side_menu_footer_terms_of_use);
		termsOfUse.findViewById(R.id.img_side_menu_category_icon).setVisibility(View.INVISIBLE);
		
		TextView tvTermsOfUse = (TextView) termsOfUse.findViewById(R.id.tv_side_menu_category_name);
		
		if (isHebrewLanguage)
		{
			tvTermsOfUse.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_20sp));
		}
		
		tvTermsOfUse.setText(R.string.terms_of_use);
		
		termsOfUse.findViewById(R.id.img_side_menu_category_group_indicator).setVisibility(View.INVISIBLE);
		
		termsOfUse.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				closeDrawer();
				doOnTermsOfUseClick();
			}
		});
		
		//Settings
		View settings = footer.findViewById(R.id.side_menu_footer_settings);
		
		Drawable settingsDrawable;
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
		{
			settingsDrawable = getResources().getDrawable(R.mipmap.menu_icon6, getTheme());
		}
		else
		{
			settingsDrawable = getResources().getDrawable(R.mipmap.menu_icon6);
		}
		
		((ImageView) settings.findViewById(R.id.img_side_menu_category_icon)).setImageDrawable(settingsDrawable);
		
		TextView tvSettings = (TextView) settings.findViewById(R.id.tv_side_menu_category_name);
		
		if (isHebrewLanguage)
		{
			tvSettings.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_20sp));
		}
		
		tvSettings.setText(R.string.settings);
		
		settings.findViewById(R.id.img_side_menu_category_group_indicator).setVisibility(View.INVISIBLE);
		
		settings.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				doOnSettingClick();
				closeDrawer();
			}
		});
		
		//Join Newsletters
		View joinNewsletters = footer.findViewById(R.id.side_menu_footer_join_our_newsletters);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
		{
			joinNewsletters.setBackgroundColor(getResources().getColor(R.color.black_opacity_30_side_menu, getTheme()));
		}
		else
		{
			joinNewsletters.setBackgroundColor(getResources().getColor(R.color.black_opacity_30_side_menu));
		}
		
		Drawable joinNewslettersDrawable;
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
		{
			joinNewslettersDrawable = getResources().getDrawable(R.mipmap.menu_newsletter, getTheme());
		}
		else
		{
			joinNewslettersDrawable = getResources().getDrawable(R.mipmap.menu_newsletter);
		}
		
		((ImageView) joinNewsletters.findViewById(R.id.img_side_menu_category_icon)).setImageDrawable(joinNewslettersDrawable);
		
		TextView tvJoinNewsletters = (TextView) joinNewsletters.findViewById(R.id.tv_side_menu_category_name);
		
		if (isHebrewLanguage)
		{
			tvJoinNewsletters.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_20sp));
		}
		
		String text = getString(R.string.newsletter);
		SpannableString styledString = new SpannableString(text);
		int indexOfSpace = text.indexOf("\n");
		styledString.setSpan(new RelativeSizeSpan(0.8f), 0, indexOfSpace, 0);
		styledString.setSpan(new StyleSpan(Typeface.BOLD), 0, indexOfSpace, 0);
		styledString.setSpan(new RelativeSizeSpan(0.8f), indexOfSpace, text.length(), 0);
		
		tvJoinNewsletters.setText(styledString);
		
		joinNewsletters.findViewById(R.id.img_side_menu_category_group_indicator).setVisibility(View.INVISIBLE);
		
		joinNewsletters.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				closeDrawer();
				doOnJoinNewslettersClick();
			}
		});
		
		return footer;
	}
	
	private void doOnSettingClick()
	{
		goToFragmentByFragmentClass(SettingsFragment.class, null, true);
		pushEvent(ePushEvents.SETTINGS);
	}
	
	private void doOnJoinNewslettersClick()
	{
		tryStartWebViewFragmentWithUrl(RequestStringBuilder.getNewsletterUrlByLanguage());
		pushEvent(ePushEvents.JOIN_NEWSLETTERS);
	}
	
	private void doOnTermsOfUseClick()
	{
		goToWebFragment(RequestStringBuilder.getElalTermsOfUseUrl());
	}
	
	private void onFlightScheduleClick()
	{
		goToWebFragment(RequestStringBuilder.getFlightTimetableUrlByLanguage());
		pushEvent(ePushEvents.FLIGHTS_TIMETABLE);
	}
	
	@SuppressLint("InflateParams")
	private View createSideMenuHeader()
	{
		View header = LayoutInflater.from(EWBaseDrawerActivity.this).inflate(R.layout.side_menu_header, null);
		UserData userData = UserData.getInstance();
		
		if (userData == null)
		{
			AppUtils.printLog(Log.ERROR, TAG, eLocalError.NullPointerException.getErrorMessage());
			return header;
		}
		
		boolean isHebrewLanguage = userData.getLanguage().isHebrew();
		
		//App language
		eLanguage language = userData.getLanguage();
		
		UserObject userObject = userData.getUserObject();
		boolean isUserGuest = userData.isUserGuest();
		
		//Title and user name
		TextView tvTitle = (TextView) header.findViewById(R.id.tv_side_menu_title);
		
		String userName = "";
		
		if (userObject != null)
		{
			if (language.isEnglish())
			{
				userName = userObject.getFirstNameEng();
			}
			else if (language.isHebrew())
			{
				userName = userObject.getFirstNameHeb();
				
				if (TextUtils.isEmpty(userName))
				{
					userName = userObject.getFirstNameEng();
				}
			}
		}
		
		String titleResultText = isUserGuest ? getString(R.string.hello_guest) : String.format(getString(R.string.hello_user), !TextUtils.isEmpty(userName) ? userName : "");
		
		if (isHebrewLanguage)
		{
			tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_22sp));
		}
		
		tvTitle.setText(titleResultText);
		
		//Login/logout button
		TextView tvLoginLogout = (TextView) header.findViewById(R.id.tv_side_menu_login_logout);
		tvLoginLogout.setText(isUserGuest ? R.string.log_in : R.string.logout);
		
		tvLoginLogout.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnLoginLogoutClick();
				closeDrawer();
			}
		});
		
		//Edit profile
		ImageView ivEditProfile = (ImageView) header.findViewById(R.id.iv_side_menu_edit);
		ivEditProfile.setVisibility(isUserGuest ? View.GONE : View.VISIBLE);
		
		ivEditProfile.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				closeDrawer();
				doOnEditProfileClick();
			}
		});
		
		//Flights Timetable
		View scheduleView = header.findViewById(R.id.side_menu_header_flights_timetable);
		
		Drawable scheduleDrawable;
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
		{
			scheduleDrawable = getResources().getDrawable(R.mipmap.menu_icon2, getTheme());
		}
		else
		{
			scheduleDrawable = getResources().getDrawable(R.mipmap.menu_icon2);
		}
		
		((ImageView) scheduleView.findViewById(R.id.img_side_menu_category_icon)).setImageDrawable(scheduleDrawable);
		
		TextView tvSchedule = (TextView) scheduleView.findViewById(R.id.tv_side_menu_category_name);
		
		if (isHebrewLanguage)
		{
			tvSchedule.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_20sp));
		}
		
		tvSchedule.setText(R.string.flights_timetable);
		
		scheduleView.findViewById(R.id.img_side_menu_category_group_indicator).setVisibility(View.INVISIBLE);
		
		scheduleView.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				closeDrawer();
				doOnScheduleClick();
			}
		});
		
		//my Notifications
		View notificationView = header.findViewById(R.id.cl_side_menu_notification);
		
		TextView tvNotification = (TextView) notificationView.findViewById(R.id.tv_side_menu_notification_main_text);
		String notificationText = getResources().getString(R.string.my_messages);
		
		if (isHebrewLanguage)
		{
			tvNotification.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_20sp));
		}
		
		tvNotification.setText(notificationText);
		
		notificationView.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				closeDrawer();
				doOnNotificationClick();
			}
		});
		
		
		return header;
	}
	
	private void doOnScheduleClick()
	{
		onFlightScheduleClick();
		
		//event 70
		pushEvent(ePushEvents.FLIGHTS_TIMETABLE);
	}
	
	private void doOnNotificationClick()
	{
//		Fragment fragment = getFragmentSwapper().getTopFragment();
//
//		if (fragment != null)
//		{
//			boolean isUserTryToClickOnOpenMenuItem = eInAppNavigation.getClassIdByClass(fragment.getActivity().getClass()) == eInAppNavigation.Notifications.getNavigationId();
//
////			if (!isUserTryToClickOnOpenMenuItem)
////			{
////			goToScreenById(eInAppNavigation.Notifications.getNavigationId(), Bundle.EMPTY);
//				goToScreenById(eInAppNavigation.Notifications.getNavigationId(),eInAppNavigation.Empty.getNavigationActivityClass(), Bundle.EMPTY);
////			}
//		}
//		else
//		{
////			goToScreenById(eInAppNavigation.Notifications.getNavigationId(), Bundle.EMPTY);
			goToScreenById(eInAppNavigation.Notifications.getNavigationId(),eInAppNavigation.Empty.getNavigationActivityClass(), Bundle.EMPTY);
//		}
		
		//event 71
		pushEvent(ePushEvents.MY_NOTIFICATIONS);
	}
	
	public void registerMatmidKeepAlive()
	{
		mKillMatmidRunnable = false;
		try
		{
			final Handler handler = new Handler(Looper.getMainLooper());
			final int delay = MATMID_TIMER;
			
			handler.postDelayed(new Runnable()
			{
				public void run()
				{
					if (!mKillMatmidRunnable)
					{
						String rbzToken = ElalPreferenceUtils.getSharedPreferenceStringOrDefault(ElAlApplication.getInstance(), ElalPreferenceUtils.CA_TOKEN, "");
						
						if (isMatmidUser() && UserData.getInstance().getSessionUpdateCounter() < MATMID_MAX_SESSION_RESTORES && !TextUtils.isEmpty(rbzToken))
						{
							doKeepAlive();
							handler.postDelayed(this, delay);
						}
						else
						{
							if (isMatmidUser())
							{
								Log.d("EWBaseDrawerActivity", "keepAlive: setting killRunnable true");
								mKillMatmidRunnable = true;
								doOnLogoutClick();
							}
							handler.removeCallbacks(this);
						}
					}
					else
					{
						return;
					}
				}
			}, delay);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Override
	public void onErrorResponse(final VolleyError iError)
	{
		ErrorsHandler.tryShowServiceErrorDialog(iError, this);
	}
	
	private void doOnEditProfileClick()
	{
		goToScreenById(eInAppNavigation.UserInfo.getNavigationId(), null);
	}
	
	private void doOnLoginLogoutClick()
	{
		if (UserData.getInstance().isUserGuest())
		{
			goToLoginScreen();
			pushEvent(ePushEvents.LOGIN_SIDE_MENU);
		}
		else
		{
			pushEvent(ePushEvents.LOGOUT_SIDE_MENU);
			doOnLogoutClick();
		}
	}
	
	public void doOnLogoutClick()
	{
		Log.d("EWBaseDrawerActivity", "trying to do Logout");
		UserData userData = UserData.getInstance();
		
		//		if (userData != null && getService() != null)
		if (userData != null)
		{
			
			try
			{
				setProgressDialog(true);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			
			getService().getController(LoginController.class).Logout(new RequestLogout(userData.getUdid()), new Response.Listener<ResponseLogout>()
			{
				@Override
				public void onResponse(final ResponseLogout response)
				{
					handleLogoutResponse();
				}
			}, this);
			mKillMatmidRunnable = false;
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, eLocalError.ServiceDisconnected.getErrorMessage());
		}
	}
	
	@Override
	protected void onServiceConnected()
	{
		Log.d(TAG, "entered onServiceConnected. mKillMatmidRunnable = " + mKillMatmidRunnable);
		if (mKillMatmidRunnable)
		{
			doOnLogoutClick();
			mKillMatmidRunnable = false;
		}
	}
	
	protected void handleLogoutResponse()
	{
		UserData.getInstance().clearUserData();
		
		handleGetRepresentationByIpResponse();
//		if (getService() != null)
//		{
//			getService().getController(LoginController.class).GetRepresentationByIp(new Response.Listener<ResponseGetRepresentationByIp>()
//			{
//				@Override
//				public void onResponse(final ResponseGetRepresentationByIp iResponse)
//				{
//					if (iResponse != null && iResponse.getContent() != null && iResponse.getContent().size() > 0)
//					{
//						UserData.getInstance().setUserRepresentation(iResponse.getContent().get(0).getRepresentationId());
//					}
//
//					handleGetRepresentationByIpResponse();
//				}
//			}, new Response.ErrorListener()
//			{
//				@Override
//				public void onErrorResponse(final VolleyError iVolleyError)
//				{
////					ErrorsHandler.tryShowServiceErrorDialog(iVolleyError, EWBaseDrawerActivity.this);
//					handleGetRepresentationByIpResponse();
//
//				}
//			});
//		}
//		else
//		{
//			AppUtils.printLog(Log.ERROR, TAG, eLocalError.ServiceDisconnected.getErrorMessage());
//		}
	}
	
	private void handleGetRepresentationByIpResponse()
	{
		if (getService() != null)
		{
			getService().getController(LoginController.class).LoginAsGuest(new Response.Listener<ResponseUserLogin>()
			{
				@Override
				public void onResponse(final ResponseUserLogin response)
				{
					setProgressDialog(false);
					
					if (response != null && response.getContent() != null)
					{
						UserData.getInstance().setUserId(response.getContent().getUserID());
						UserData.getInstance().setGuestUserId(response.getContent().getUserID());
						UserData.getInstance().setUserObject(response.getContent().getUserObject());
						ElAlApplication.getInstance().setUserDefinedLanguage();
					}
					startActivity(new Intent(EWBaseDrawerActivity.this, MainActivity.class));
					finish();
					
				}
			}, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError iVolleyError)
				{
					ErrorsHandler.tryShowServiceErrorDialog(iVolleyError, EWBaseDrawerActivity.this);
				}
			});
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, eLocalError.ServiceDisconnected.getErrorMessage());
		}
	}
	
	protected void doKeepAlive()
	{
		String token = ElalPreferenceUtils.getSharedPreferenceStringOrDefault(ElAlApplication.getInstance(), ElalPreferenceUtils.CA_TOKEN, "");
		if (!ElAlApplication.isAppWasInBackground() && !TextUtils.isEmpty(token))
		{
			Log.d(TAG, "doing keep alive. isAppWasInBackground: " + ElAlApplication.isAppWasInBackground());
			
			UserData.getInstance().updateSessionCounter();
			UserData.getInstance().setLastSessionUpdate(/*new Date()*/);
			
			getService().getController(LoginControllerMatmid.class).MatmidExtendSession(new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError error)
				{
					AppUtils.printLog(Log.ERROR, "MatmidExtendSession: onErrorResponse: ", eLocalError.KeepAliveError.getErrorMessage());
					/* do nothing, like in iPhone*/
				}
			});
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, "not calling keep alive");
		}
	}
	
	
	private void openAskUpdateDetailDialog()
	{
		AppUtils.createSimpleMessageDialog(EWBaseDrawerActivity.this, getString(R.string.enter_your_personal_details), getString(R.string.approve), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, final int which)
			{
				//noting
			}
		}, null, null).show();
	}
	
	public void openAskLoginDialog()
	{
		AppUtils.createSimpleMessageDialog(this, getString(R.string.login_dialog_message), getString(R.string.login), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, final int which)
			{
				ElalPreferenceUtils.setSharedPreference(EWBaseDrawerActivity.this, IntentExtras.LAST_SELECTED_LOGIN_TYPE, LOGIN_TYPE_MATMID_CLUB_MEMBER);
				goToLoginScreen();
			}
		}, getString(R.string.create_an_account), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, final int which)
			{
				goToRegistrationActivity();
			}
		}, getString(R.string.cancel), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, final int which)
			{
				// nothing...
			}
		}).show();
	}
	
	public void goToRegistrationActivity()
	{
		UserData.getInstance().clearUserData();
		goToScreenById(eInAppNavigation.Registration.getNavigationId(), null);
	}
	
	protected void closeDrawer()
	{
		mDrawerLayout.closeDrawer(GravityCompat.END);
	}
	
	public void goToLoginScreen()
	{
		UserData.getInstance().clearUserData();
		goToScreenById(eInAppNavigation.Login.getNavigationId(), eInAppNavigation.Login.getNavigationActivityClass(), Bundle.EMPTY);
	}
	
	public void goToRegisterFragment()
	{
		goToScreenById(eInAppNavigation.Registration.getNavigationId(), eInAppNavigation.Registration.getNavigationActivityClass(), null);
	}
	
	protected boolean isDrawerOpen()
	{
		return mDrawerLayout.isDrawerOpen(GravityCompat.END);
	}
	
	public void setShouldExit(boolean mShouldExit)
	{
		this.mShouldExit = mShouldExit;
	}
	
	protected void startExitTimer()
	{
		mShouldExit = true;
		
		new Handler().postDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				mShouldExit = false;
				
			}
		}, EXIT_TIME);
	}
	
	public void tryStartWebViewFragmentWithUrl(final String iUrl)
	{
		if (getFragmentSwapper() != null && !TextUtils.isEmpty(iUrl))
		{
			Bundle bundle = new Bundle();
			bundle.putString(IntentExtras.URL, iUrl);
			getFragmentSwapper().swapToFragment(WebViewFragment.class, bundle, mFlContainer.getId(), true, false);
			//			goToScreenById(eInAppNavigation.WebView.getNavigationId(), bundle);
		}
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void goToScreenById(final int iScreenId, final Bundle iBundle)
	{
		goToScreenById(iScreenId, eActivityModule.getNavigationActivityClassByIdOrMainDefault(AppData.getInstance().getModuleId()), iBundle);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void goToScreenById(final int iScreenId, final Class iOriginActivity, final Bundle iBundle)
	{
		handleHeaderButtonsAsMatmidFirstLogin(iScreenId);
		
		eInAppNavigation inAppNavigation = eInAppNavigation.getInAppNavigationByIdOrErrorDefault(iScreenId);
		
		if (inAppNavigation.isPersonal() && UserData.getInstance().isUserGuest())
		{
			openAskLoginDialog();
		}
		else
		{
			if (inAppNavigation != eInAppNavigation.Error /*&& inAppNavigation.getNavigationFragmentClass() != null*/)
			{
				if (iOriginActivity != null && inAppNavigation.getNavigationActivityClass() != null
						&& inAppNavigation.getNavigationActivityClass().isAssignableFrom((Class<?>) iOriginActivity)
						&& inAppNavigation.getNavigationActivityClass().equals(iOriginActivity))
				{
					if (inAppNavigation.getNavigationFragmentClass() != null && EWBaseFragment.class.isAssignableFrom(inAppNavigation.getNavigationFragmentClass()))
					{
						Intent intent = getIntent();
						intent.putExtra(IntentExtras.IN_APP_NAVIGATION_ID, inAppNavigation.getNavigationId());
						
						getFragmentSwapper().swapToFragment(inAppNavigation.getNavigationFragmentClass(), iBundle, mFlContainer.getId(), true, false);
					}
					else if (EWBaseActivity.class.isAssignableFrom(inAppNavigation.getNavigationFragmentClass()) && inAppNavigation.getNavigationFragmentClass() != iOriginActivity /*don't replace activities if there's equals*/
							/***TODO avishay 23/10/17 map flights screens and implement some logic for screens. the remove --> || inAppNavigation == eInAppNavigation.MyFlights from this line**/ || inAppNavigation == eInAppNavigation.MyFlights)
					{
						Intent intent = new Intent(this, inAppNavigation.getNavigationFragmentClass());
						intent.putExtra(IntentExtras.IN_APP_NAVIGATION_ID, inAppNavigation.getNavigationId());
						intent.putExtra(IntentExtras.IS_FROM_PUSH_NOTIFICATION, false);
						if (iBundle != null)
						{
							intent.putExtras(iBundle);
						}
						
						startActivity(intent);
						//					finish();
					}
					else
					{
						Log.d(TAG, "fail to load fragment: " + inAppNavigation.getNavigationFragmentClass());
						AppUtils.printLog(Log.ERROR, TAG, eLocalError.GeneralError.getErrorMessage());
					}
				}
				else if (EWBaseActivity.class.isAssignableFrom(inAppNavigation.getNavigationActivityClass()))
				{
					Intent intent = new Intent(this, inAppNavigation.getNavigationActivityClass());
					intent.putExtra(IntentExtras.IN_APP_NAVIGATION_ID, inAppNavigation.getNavigationId());
					intent.putExtra(IntentExtras.IS_FROM_PUSH_NOTIFICATION, false);
					if (iBundle != null)
					{
						intent.putExtras(iBundle);
					}
					
					if (iScreenId == eInAppNavigation.Main.getNavigationId())
					{
						intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					}
					
					startActivity(intent);
				}
				else if (inAppNavigation.getNavigationFragmentClass() != null && EWBaseFragment.class.isAssignableFrom(inAppNavigation.getNavigationFragmentClass()))
				{
					getFragmentSwapper().swapToFragment(inAppNavigation.getNavigationFragmentClass(), iBundle, mFlContainer.getId(), true, false);
				}
			}
		}
	}
	
	@Override
	public void goToWebFragment(final String iUrl)
	{
		if (!mUrl.equals(iUrl) || getFragmentSwapper().getTopFragment().getClass() != WebViewFragment.class)
		{
			mUrl = iUrl;
			tryStartWebViewFragmentWithUrl(iUrl);
		}
	}
	
	@Override
	public void goToFragmentByFragmentClass(@NonNull final Class<? extends EWBaseFragment> iFragmentClass, final Bundle iBundle, final boolean iAddToBackStack)
	{
		if (getFragmentSwapper() != null)
		{
			if (getFragmentSwapper().getTopFragment() != null && getFragmentSwapper().getTopFragment().getClass() == iFragmentClass)
			{
				return;
			}
			getFragmentSwapper().swapToFragment(iFragmentClass, iBundle, mFlContainer.getId(), iAddToBackStack, false);
		}
		
	}
	
	public void handleHeaderButtonsAsMatmidFirstLogin(int iScreenId)
	{
		if (iScreenId == eInAppNavigation.UserInfo.getNavigationId() && UserData.getInstance().getUserObject() != null && UserData.getInstance().getUserObject().isMatmid() && UserData.getInstance()
		                                                                                                                                                                               .getUserObject()
		                                                                                                                                                                               .isMatmidFirstLogin())
		{
			//		mIvToolbarLogo.setOnClickListener(null);
			mIvToolbarDrawer.setVisibility(View.INVISIBLE);
			mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
			mIvToolbarLogo.setEnabled(false);
		}
		else
		{
			mIvToolbarDrawer.setVisibility(View.VISIBLE);
			mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
			mIvToolbarLogo.setEnabled(true);
		}
		
	}
	
	public boolean isMatmidUser()
	{
		return UserData.getInstance() != null && UserData.getInstance().getUserObject() != null && UserData.getInstance().getUserObject().isMatmid();
	}
	
	public void enableLogoBtn(boolean enable)
	{
		if (enable)
		{
			mIvToolbarLogo.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(final View v)
				{
					AppUtils.hideKeyboard(EWBaseDrawerActivity.this, v);
					onLogoClick();
				}
			});
		}
		else
		{
			mIvToolbarLogo.setOnClickListener(null);
		}
	}
}
