package ui.adapters;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import il.co.ewave.elal.R;
import utils.global.AppUtils;
import webServices.responses.responseGetUserActivePNRs.Passenger;

/**
 * Created with care by Shahar Ben-Moshe on 15/02/17.
 */

public class PassengersAdapter extends android.support.v7.widget.RecyclerView.Adapter<PassengersAdapter.ViewHolder>
{
	private ArrayList<Passenger> mPassengers;
	
	public PassengersAdapter(final ArrayList<Passenger> iPassengers)
	{
		mPassengers = iPassengers;
	}
	
	@Override
	public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType)
	{
		return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.passenger_item_cell_layout, parent, false));
	}
	
	@SuppressLint("RtlHardcoded")
	@Override
	public void onBindViewHolder(final ViewHolder iHolder, final int iPosition)
	{
		Passenger currentPassenger = mPassengers.get(iPosition);
		
		if (currentPassenger != null)
		{
			iHolder.tvPassengerName.setText(currentPassenger.getFirstName() + " " + currentPassenger.getLastName());
			iHolder.tvSeatNumber.setText(TextUtils.isEmpty(currentPassenger.getSeat()) ? "-" : currentPassenger.getSeat());
			iHolder.tvMeal.setText(TextUtils.isEmpty(currentPassenger.getMeal()) ? "-" : currentPassenger.getMeal());
			
			iHolder.tvPassengerName.setGravity(AppUtils.isDefaultLocaleRTL() ? Gravity.RIGHT : Gravity.LEFT);
		}
	}
	
	@Override
	public int getItemCount()
	{
		return mPassengers == null ? 0 : mPassengers.size();
	}
	
	class ViewHolder extends RecyclerView.ViewHolder
	{
		private TextView tvPassengerName;
		private TextView tvSeatNumber;
		private TextView tvMeal;
		
		ViewHolder(final View itemView)
		{
			super(itemView);
			
			this.tvMeal = (TextView) itemView.findViewById(R.id.tv_passengerCell_Meal);
			this.tvSeatNumber = (TextView) itemView.findViewById(R.id.tv_passengerCell_SeatNumber);
			this.tvPassengerName = (TextView) itemView.findViewById(R.id.tv_passengerCell_PassengerName);
		}
	}
}
