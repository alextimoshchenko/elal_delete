package ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import il.co.ewave.elal.R;

/**
 * Created with care by Alexey.T on 19/07/2017.
 * <p>
 * TODO: Add a class header comment!
 */
public class AlekseyTestFragment extends EWBaseFragment
{
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.aleksey_test_fragment_layout);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		initReference();
		setListeners();
	}
	
	private void setListeners()
	{
		
	}
	
	private void initReference()
	{
	}
}
