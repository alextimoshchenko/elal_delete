package utils.global.dbObjects;

import android.graphics.Bitmap;
import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.util.ArrayList;

import global.ElAlApplication;
import il.co.ewave.elal.R;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import utils.global.AppUtils;

import static android.R.attr.id;
import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * Created with care by Shahar Ben-Moshe on 22/01/17.
 */

public class Document extends RealmObject
{
	public static final String ID = "id";
	public static final String PNR = "pnr";
	public static final String USER_ID = "userId";
	public static final String TYPE = "type";
	public static final String NAME = "name";
	public static final String DATA = "data";
	
	public static final int PASSPORT = 0;
	public static final int FLIGHT_DETAILS = 1;
	public static final int BOARDING_PASS = 2;
	public static final int HOTEL_VOUCHER = 3;
	public static final int CAR_VOUCHER = 4;
	public static final int OTHER = 5;
	
	@Retention(SOURCE)
	@IntDef({PASSPORT, FLIGHT_DETAILS, BOARDING_PASS, HOTEL_VOUCHER, CAR_VOUCHER, OTHER})
	public @interface eCategory
	{
	}
	
	public enum eDocumentType
	{
		BOARDING_PASS(0, R.string.boarding_pass, R.mipmap.boarding_pass2),
		FLIGHT_DETAILS(1, R.string.flight_details, R.mipmap.plane),
		CAR_VOUCHER(2, R.string.car_voucher, R.mipmap.car),
		HOTEL_VOUCHER(3, R.string.hotel_voucher, R.mipmap.hotel),
		OTHER(4, R.string.other, R.mipmap.other),
		PASSPORT_PHOTO(5, R.string.passport_photo, R.mipmap.passport);
		
		private final int mDocumentTypeId;
		private final int mNameResourceId;
		private final int mIconResourceId;
		
		eDocumentType(final int iDocumentTypeId, final int iNameResourceId, final int iIconResourceId)
		{
			mDocumentTypeId = iDocumentTypeId;
			mNameResourceId = iNameResourceId;
			mIconResourceId = iIconResourceId;
		}
		
		public static eDocumentType getDocumentTypeById(int iDocumentTypeId)
		{
			eDocumentType result = null;
			
			for (eDocumentType eDocumentType : values())
			{
				if (eDocumentType.mDocumentTypeId == iDocumentTypeId)
				{
					result = eDocumentType;
					break;
				}
			}
			
			return result;
		}
		
		public int getDocumentTypeId()
		{
			return mDocumentTypeId;
		}
		
		public int getNameResourceId()
		{
			return mNameResourceId;
		}
		
		public int getIconResourceId()
		{
			return mIconResourceId;
		}
		
		@Override
		public String toString()
		{
			return ElAlApplication.getInstance().getString(getNameResourceId());
		}
	}
	
	public static ArrayList<Integer> createDocumentTypesList()
	{
		ArrayList<Integer> result = new ArrayList<>();
		
		result.add(PASSPORT);
		result.add(FLIGHT_DETAILS);
		result.add(BOARDING_PASS);
		result.add(HOTEL_VOUCHER);
		result.add(CAR_VOUCHER);
		result.add(OTHER);
		
		return result;
	}
	
	private String pnr;
	private int userId;
	private int type;
	private String name;
	private String data;
	@Ignore
	private Bitmap mDocumentImage;
	
	public Document()
	{
	}
	
	public Document(final String iPnr, int iUserId, final int iType, final String iName, final String iData)
	{
		setPnr(iPnr);
		setUserId(iUserId);
		setType(iType);
		setName(iName);
		setData(iData);
		mDocumentImage = null;
	}
	
	public long getId()
	{
		return id;
	}
	
	public String getPnr()
	{
		return pnr;
	}
	
	public void setPnr(final String iIPnr)
	{
		pnr = iIPnr;
	}
	
	public int getUserId()
	{
		return userId;
	}
	
	public void setUserId(int userId)
	{
		this.userId = userId;
	}
	
	public int getType()
	{
		return type;
	}
	
	public void setType(final int iIType)
	{
		type = iIType;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(final String iIName)
	{
		name = iIName;
	}
	
	public String getData()
	{
		return data;
	}
	
	public void setData(final String iIData)
	{
		data = iIData;
		mDocumentImage = null;
	}
	
	public Bitmap getDocumentImage()
	{
		if (mDocumentImage == null)
		{
			mDocumentImage = AppUtils.convertBase64ImageToBitmap(data);
		}
		
		return mDocumentImage;
	}
}
