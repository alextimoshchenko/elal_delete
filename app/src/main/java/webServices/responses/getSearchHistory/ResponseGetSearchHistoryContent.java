package webServices.responses.getSearchHistory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Date;

import interfaces.ISearchElement;

public class ResponseGetSearchHistoryContent implements ISearchElement
{
	@JsonProperty("SearchTitleEng")
	private String mSearchTitleEng = "";
	@JsonProperty("SearchTitleHeb")
	private String mSearchTitleHeb = "";
	@JsonProperty("SearchTitle")
	private String mSearchTitle = "";
	@JsonProperty("Family")
	private ArrayList<ResponseGetSearchHistoryContentFamily> mFamily = new ArrayList<>();
	@JsonProperty("FilghtSearchID")
	private int mFilghtSearchID;
	@JsonProperty("FlightSearchTypeID")
	private int mFlightSearchTypeID;
	@JsonProperty("UserID")
	private int mUserID;
	@JsonProperty("DepartureIATA")
	private String mDepartureIATA = "";
	@JsonProperty("ArrivalIATA")
	private String mArrivalIATA = "";
	
	@JsonProperty("Adults")
	private int mAdults;
	@JsonProperty("Childs")
	private int mChilds;
	@JsonProperty("Babies")
	private int mBabies;
	@JsonProperty("Teens")
	private int mTeens;
	@JsonProperty("Young")
	private int mYoung;
	@JsonProperty("Students")
	private int mStudents;
	@JsonProperty("GoldenAge")
	private int mGoldenAge;
	@JsonProperty("SearchDate")
	private Date mSearchDate;
	@JsonProperty("Reference")
	private int mReference;
	@JsonProperty("FlightClassID")
	private int mFlightClassID;
	
	@JsonProperty("DepartureCityCode")
	private String mDepartureCityCode = "";
	@JsonProperty("ArrivalCityCode")
	private String mArrivalCityCode = "";
	@JsonProperty("DepartureDate")
	private Date mDepartureDate;
	@JsonProperty("ArrivalDate")
	private Date mArrivalDate;
	
	/**
	 * This params {@param mAdditionalDepartureCityCode, mAdditionalArrivalCityCode, mAdditionalDepartureDate, mAdditionalArrivalDate} we need if user history include `Return from another
	 * destination
	 */
	@JsonIgnore
	private String mAdditionalDepartureCityCode = "";
	@JsonIgnore
	private String mAdditionalArrivalCityCode = "";
	@JsonIgnore
	private Date mAdditionalDepartureDate;
	@JsonIgnore
	private Date mAdditionalArrivalDate;
	
	public int getYoung()
	{
		return mYoung;
	}
	
	public int getReference()
	{
		return mReference;
	}
	
	public Date getSearchDate()
	{
		return mSearchDate;
	}
	
	public int getChilds()
	{
		return mChilds;
	}
	
	public String getSearchTitleHeb()
	{
		return mSearchTitleHeb;
	}
	
	public int getFlightClassID()
	{
		return mFlightClassID;
	}
	
	public int getFilghtSearchID()
	{
		return mFilghtSearchID;
	}
	
	public int getBabies()
	{
		return mBabies;
	}
	
	public String getDepartureCityCode()
	{
		return mDepartureCityCode;
	}
	
	public int getGoldenAge()
	{
		return mGoldenAge;
	}
	
	public String getArrivalCityCode()
	{
		return mArrivalCityCode;
	}
	
	public int getStudents()
	{
		return mStudents;
	}
	
	private String getSearchTitle()
	{
		return mSearchTitle;
	}
	
	public void setSearchTitle(final String iSearchTitle)
	{
		mSearchTitle = iSearchTitle;
	}
	
	public Date getArrivalDate()
	{
		return mArrivalDate;
	}
	
	public int getUserID()
	{
		return mUserID;
	}
	
	public int getTeens()
	{
		return mTeens;
	}
	
	public String getSearchTitleEng()
	{
		return mSearchTitleEng;
	}
	
	public ArrayList<ResponseGetSearchHistoryContentFamily> getFamily()
	{
		return mFamily;
	}
	
	public int getAdults()
	{
		return mAdults;
	}
	
	public Date getDepartureDate()
	{
		return mDepartureDate;
	}
	
	public int getFlightSearchTypeID()
	{
		return mFlightSearchTypeID;
	}
	
	public String getDepartureIATA()
	{
		return mDepartureIATA;
	}
	
	public String getArrivalIATA()
	{
		return mArrivalIATA;
	}
	
	public String getAdditionalDepartureCityCode()
	{
		return mAdditionalDepartureCityCode;
	}
	
	public void setAdditionalDepartureCityCode(final String iAdditionalDepartureCityCode)
	{
		mAdditionalDepartureCityCode = iAdditionalDepartureCityCode;
	}
	
	public String getAdditionalArrivalCityCode()
	{
		return mAdditionalArrivalCityCode;
	}
	
	public void setAdditionalArrivalCityCode(final String iAdditionalArrivalCityCode)
	{
		mAdditionalArrivalCityCode = iAdditionalArrivalCityCode;
	}
	
	public Date getAdditionalDepartureDate()
	{
		return mAdditionalDepartureDate;
	}
	
	public void setAdditionalDepartureDate(final Date iAdditionalDepartureDate)
	{
		mAdditionalDepartureDate = iAdditionalDepartureDate;
	}
	
	public Date getAdditionalArrivalDate()
	{
		return mAdditionalArrivalDate;
	}
	
	public void setAdditionalArrivalDate(final Date iAdditionalArrivalDate)
	{
		mAdditionalArrivalDate = iAdditionalArrivalDate;
	}
	
	public ResponseGetSearchHistoryContent()
	{
	}
	
	@Override
	public String getElementNameEn()
	{
		return getSearchTitle();
	}
	
	@Override
	public String getElementNameHe()
	{
		return getSearchTitle();
	}
	
	@Override
	public String getAliasText()
	{
		return null;
	}
	
	@Override
	public String toString()
	{
		return getSearchTitle();
	}
	
//	@Override
//	public boolean equals(final Object obj)
//	{
//		//I can not add more params to compare here, because I get a message that there are a lot of params to compare for this equals method
//
//		ResponseGetSearchHistoryContent historyContent;
//
//		if (obj instanceof ResponseGetSearchHistoryContent)
//		{
//			historyContent = (ResponseGetSearchHistoryContent) obj;
//		}
//		else
//		{
//			return false;
//		}
//
//		boolean isSearchTitle = this.mSearchTitle.equals(historyContent.mSearchTitle);
//
//		boolean isFilghtSearchID = this.mFilghtSearchID == historyContent.mFilghtSearchID;
//		boolean isFlightSearchTypeID = this.mFlightSearchTypeID == historyContent.mFlightSearchTypeID;
//		boolean isUserID = this.mUserID == historyContent.mUserID;
//
//		boolean isAdults = this.mAdults == historyContent.mAdults;
//		boolean isChilds = this.mChilds == historyContent.mChilds;
//
//		boolean isFlightClassID = this.mFlightClassID == historyContent.mFlightClassID;
//
//		boolean isDepartureCityCode = this.mDepartureCityCode.equals(historyContent.mDepartureCityCode);
//		boolean isArrivalCityCode = this.mArrivalCityCode.equals(historyContent.mArrivalCityCode);
//
//		return  isSearchTitle && isFilghtSearchID && isFlightSearchTypeID && isUserID && isAdults && isChilds  && isFlightClassID &&
//				isDepartureCityCode && isArrivalCityCode;
//	}
}
