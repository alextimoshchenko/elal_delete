package webServices.responses.responseGetUserPNRs;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetUserPNRsContentPNRTickets
{
	@JsonProperty("CategoryId")
	private int mCategoryId;
	@JsonProperty("HasInfantTravelers")
	private boolean mHasInfantTravelers;
	@JsonProperty("FirstName")
	private String mFirstName;
	@JsonProperty("HasCheckin")
	private boolean mHasCheckin;
	@JsonProperty("HandBagAmount")
	private int mHandBagAmount;
	@JsonProperty("Title")
	private String mTitle;
	@JsonProperty("CreateDate")
	private String mCreateDate;
	@JsonProperty("LastModified")
	private String mLastModified;
	@JsonProperty("SSRAccessibilities")
	private String mSSRAccessibilities;
	@JsonProperty("PassengerType")
	private String mPassengerType;
	@JsonProperty("ClassService")
	private String mClassService;
	@JsonProperty("ReferenceId")
	private String mReferenceId;
	@JsonProperty("PNR")
	private String mPNR;
	@JsonProperty("TicketNumber")
	private String mTicketNumber;
	@JsonProperty("HasChildTravelers")
	private boolean mHasChildTravelers;
	@JsonProperty("FlightNumber")
	private String mFlightNumber;
	@JsonProperty("HasSSRs")
	private boolean mHasSSRs;
	@JsonProperty("LastName")
	private String mLastName;
	@JsonProperty("HasSeats")
	private boolean mHasSeats;
	@JsonProperty("SSRMeals")
	private String mSSRMeals;
	@JsonProperty("HasHandBag")
	private boolean mHasHandBag;
	
	public int getCategoryId()
	{
		return mCategoryId;
	}
	
	public boolean isHasInfantTravelers()
	{
		return mHasInfantTravelers;
	}
	
	public String getFirstName()
	{
		return mFirstName;
	}
	
	public boolean isHasCheckin()
	{
		return mHasCheckin;
	}
	
	public int getHandBagAmount()
	{
		return mHandBagAmount;
	}
	
	public String getTitle()
	{
		return mTitle;
	}
	
	public String getCreateDate()
	{
		return mCreateDate;
	}
	
	public String getLastModified()
	{
		return mLastModified;
	}
	
	public String getSSRAccessibilities()
	{
		return mSSRAccessibilities;
	}
	
	public String getPassengerType()
	{
		return mPassengerType;
	}
	
	public String getClassService()
	{
		return mClassService;
	}
	
	public String getReferenceId()
	{
		return mReferenceId;
	}
	
	public String getPNR()
	{
		return mPNR;
	}
	
	public String getTicketNumber()
	{
		return mTicketNumber;
	}
	
	public boolean isHasChildTravelers()
	{
		return mHasChildTravelers;
	}
	
	public String getFlightNumber()
	{
		return mFlightNumber;
	}
	
	public boolean isHasSSRs()
	{
		return mHasSSRs;
	}
	
	public String getLastName()
	{
		return mLastName;
	}
	
	public boolean isHasSeats()
	{
		return mHasSeats;
	}
	
	public String getSSRMeals()
	{
		return mSSRMeals;
	}
	
	public boolean isHasHandBag()
	{
		return mHasHandBag;
	}
	
	public GetUserPNRsContentPNRTickets()
	{
	}
}
