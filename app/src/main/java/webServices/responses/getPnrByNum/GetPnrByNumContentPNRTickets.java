package webServices.responses.getPnrByNum;

public class GetPnrByNumContentPNRTickets
{
	private int CategoryId;
	private boolean HasInfantTravelers;
	private String FirstName;
	private boolean HasCheckin;
	private int HandBagAmount;
	private String Title;
	private String CreateDate;
	private String LastModified;
	private String SSRAccessibilities;
	private Object PassengerType;
	private String ClassService;
	private String ReferenceId;
	private String PNR;
	private String TicketNumber;
	private boolean HasChildTravelers;
	private boolean IsPrimary;
	private String FlightNumber;
	private boolean HasSSRs;
	private String LastName;
	private boolean HasSeats;
	private String SSRMeals;
	private boolean HasHandBag;
	
	public int getCategoryId()
	{
		return this.CategoryId;
	}
	
	public void setCategoryId(int CategoryId)
	{
		this.CategoryId = CategoryId;
	}
	
	public boolean getHasInfantTravelers()
	{
		return this.HasInfantTravelers;
	}
	
	public void setHasInfantTravelers(boolean HasInfantTravelers)
	{
		this.HasInfantTravelers = HasInfantTravelers;
	}
	
	public String getFirstName()
	{
		return this.FirstName;
	}
	
	public void setFirstName(String FirstName)
	{
		this.FirstName = FirstName;
	}
	
	public boolean getHasCheckin()
	{
		return this.HasCheckin;
	}
	
	public void setHasCheckin(boolean HasCheckin)
	{
		this.HasCheckin = HasCheckin;
	}
	
	public int getHandBagAmount()
	{
		return this.HandBagAmount;
	}
	
	public void setHandBagAmount(int HandBagAmount)
	{
		this.HandBagAmount = HandBagAmount;
	}
	
	public String getTitle()
	{
		return this.Title;
	}
	
	public void setTitle(String Title)
	{
		this.Title = Title;
	}
	
	public String getCreateDate()
	{
		return this.CreateDate;
	}
	
	public void setCreateDate(String CreateDate)
	{
		this.CreateDate = CreateDate;
	}
	
	public String getLastModified()
	{
		return this.LastModified;
	}
	
	public void setLastModified(String LastModified)
	{
		this.LastModified = LastModified;
	}
	
	public String getSSRAccessibilities()
	{
		return this.SSRAccessibilities;
	}
	
	public void setSSRAccessibilities(String SSRAccessibilities)
	{
		this.SSRAccessibilities = SSRAccessibilities;
	}
	
	public Object getPassengerType()
	{
		return this.PassengerType;
	}
	
	public void setPassengerType(Object PassengerType)
	{
		this.PassengerType = PassengerType;
	}
	
	public String getClassService()
	{
		return this.ClassService;
	}
	
	public void setClassService(String ClassService)
	{
		this.ClassService = ClassService;
	}
	
	public String getReferenceId()
	{
		return this.ReferenceId;
	}
	
	public void setReferenceId(String ReferenceId)
	{
		this.ReferenceId = ReferenceId;
	}
	
	public String getPNR()
	{
		return this.PNR;
	}
	
	public void setPNR(String PNR)
	{
		this.PNR = PNR;
	}
	
	public String getTicketNumber()
	{
		return this.TicketNumber;
	}
	
	public void setTicketNumber(String TicketNumber)
	{
		this.TicketNumber = TicketNumber;
	}
	
	public boolean getHasChildTravelers()
	{
		return this.HasChildTravelers;
	}
	
	public void setHasChildTravelers(boolean HasChildTravelers)
	{
		this.HasChildTravelers = HasChildTravelers;
	}
	
	public boolean getIsPrimary()
	{
		return this.IsPrimary;
	}
	
	public void setIsPrimary(boolean IsPrimary)
	{
		this.IsPrimary = IsPrimary;
	}
	
	public String getFlightNumber()
	{
		return this.FlightNumber;
	}
	
	public void setFlightNumber(String FlightNumber)
	{
		this.FlightNumber = FlightNumber;
	}
	
	public boolean getHasSSRs()
	{
		return this.HasSSRs;
	}
	
	public void setHasSSRs(boolean HasSSRs)
	{
		this.HasSSRs = HasSSRs;
	}
	
	public String getLastName()
	{
		return this.LastName;
	}
	
	public void setLastName(String LastName)
	{
		this.LastName = LastName;
	}
	
	public boolean getHasSeats()
	{
		return this.HasSeats;
	}
	
	public void setHasSeats(boolean HasSeats)
	{
		this.HasSeats = HasSeats;
	}
	
	public String getSSRMeals()
	{
		return this.SSRMeals;
	}
	
	public void setSSRMeals(String SSRMeals)
	{
		this.SSRMeals = SSRMeals;
	}
	
	public boolean getHasHandBag()
	{
		return this.HasHandBag;
	}
	
	public void setHasHandBag(boolean HasHandBag)
	{
		this.HasHandBag = HasHandBag;
	}
}
