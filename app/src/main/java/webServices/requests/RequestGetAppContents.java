package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.UserData;

/**
 * Created with care by Shahar Ben-Moshe on 02/02/17.
 */

public class RequestGetAppContents
{
	@JsonProperty("PNRNum")
	private String mPnrNum;
	
	@JsonProperty("UserID")
	private int mUserID = UserData.getInstance().getUserID();
	
	@JsonProperty("FlightNumber")
	private String mFlightNumber;
	
	@JsonProperty("LanguageCode")
	private String mLanguageCode = UserData.getInstance().getLanguage().getLanguageCode();
	
	public RequestGetAppContents(final String iPnrNum, final String iFlightNumber)
	{
		mPnrNum = iPnrNum;
		mFlightNumber = iFlightNumber;
	}
}
