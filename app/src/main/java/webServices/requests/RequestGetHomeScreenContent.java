package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.UserData;

public class RequestGetHomeScreenContent
{
	@JsonProperty("UserID")
	private int mUserID;
	@JsonProperty("LanguageCode")
	private String mLanguageCode = UserData.getInstance().getLanguage().getLanguageCode();
//	@JsonProperty("IsGuest")
//	private boolean mIsGuest = UserData.getInstance().isUserGuest();
	@JsonProperty("SessionID")
	private String mSessionID;
	@JsonProperty("MemberID")
	private String mMemberID;
	
	public RequestGetHomeScreenContent(int iUserID, String iSessionID, String iMemberID)
	{
		this.mUserID = iUserID;
		this.mSessionID = iSessionID;
		this.mMemberID = iMemberID;
	}
	
	public RequestGetHomeScreenContent()
	{
		this.mUserID = UserData.getInstance().isUserGuest() ? UserData.getInstance().getGuestUserId() : UserData.getInstance().getUserID();
		this.mSessionID = UserData.getInstance().getUserObject().getSessionID();
		this.mMemberID = UserData.getInstance().getUserObject().getMatmidMemberID();
	}
}
