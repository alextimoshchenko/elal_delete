package webServices.responses.ResponseSetUserFamily;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

class SetRegularUserFamilyContent
{
	@JsonProperty("Answer")
	private ArrayList<SetRegularUserFamilyContentAnswer> mAnswer;
	
	public ArrayList<SetRegularUserFamilyContentAnswer> getAnswer()
	{
		return this.mAnswer;
	}
	
	public SetRegularUserFamilyContent()
	{
	}
}
