package webServices.responses.getSideMenuItems;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by Avishay.Peretz on 14/06/2017.
 */

public class GetSideMenuItems
{
	@JsonProperty("SideMenuItems")
	private ArrayList<SideMenuCategory> mSideMenuItems;
//	@JsonProperty("UnReadPushes")
//	private int mUnReadPushes;
	
	public GetSideMenuItems()
	{
	}
	
	public GetSideMenuItems(final ArrayList<SideMenuCategory> iSideMenuItems, final int iUnReadPushes)
	{
		mSideMenuItems = iSideMenuItems;
//		mUnReadPushes = iUnReadPushes;
	}
	
	public ArrayList<SideMenuCategory> getSideMenuItems()
	{
		return mSideMenuItems;
	}
	
//	public int getUnReadPushes()
//	{
//		return mUnReadPushes;
//	}
}
