package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.UserData;
import utils.global.searchFlightObjects.SearchObject;

public class RequestGetSearchHistory
{
	@JsonProperty("UserID")
	private int mUserID;
	
	@JsonProperty("LanguageCode")
	private String mLanguageCode = UserData.getInstance().getLanguage().getLanguageCode();
	
	@JsonProperty("FlightType")
	private String mFlightType = SearchObject.getInstance().getCurrentTabStatement().getStatementCode();
	
	public RequestGetSearchHistory(int iUserID)
	{
		this.mUserID = iUserID;
	}
	
	public RequestGetSearchHistory()
	{
	}
}
