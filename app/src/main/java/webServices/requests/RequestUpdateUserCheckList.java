package webServices.requests;


import com.fasterxml.jackson.annotation.JsonProperty;


public class RequestUpdateUserCheckList
{
	@JsonProperty("UserItemID")
	private int userItemID;
	@JsonProperty("UserItemTitle")
	private String userItemTitle;
	@JsonProperty("TimeDimentionID")
	private int mTimeDimensionID;
	@JsonProperty("IsDone")
	private boolean isDone;
	@JsonProperty("PNR")
	private String mPnr;
	@JsonProperty("UserID")
	private int mUserID;
	
	
//	public RequestUpdateUserCheckList(final Integer iUserItemID, final String iUserItemTitle, final boolean iIsDone, final int iTimeDimentionID)
//	{
//				mTimeDimensionID = iTimeDimentionID;
//				userItemID = iUserItemID;
//				userItemTitle = iUserItemTitle;
//				isDone = iIsDone;
//	}
	
	
	public RequestUpdateUserCheckList(final int iUserItemID, final String iUserItemTitle, final boolean iIsDone, final int iTimeDimensionID, final String iPnr, final int iUserID)
	{
		userItemID = iUserItemID;
		userItemTitle = iUserItemTitle;
		isDone = iIsDone;
		mTimeDimensionID = iTimeDimensionID;
		mPnr = iPnr;
		mUserID = iUserID;
	}
}