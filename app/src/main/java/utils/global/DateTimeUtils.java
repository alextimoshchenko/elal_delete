package utils.global;

import android.text.TextUtils;

import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.LocalDate;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.SimpleTimeZone;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import global.eLanguage;

/**
 * Created with care by Shahar Ben-Moshe on 29/12/16.
 */

public class DateTimeUtils
{
	public static final String DATE_FORMAT_yyyy_MM_dd = "yyyy-MM-dd";
	public static final String DATE_FORMAT_MM = "MM";
	public static final String DATE_FORMAT_dd = "dd";
	public static final String DATE_FORMAT_yyyy_MM_ddHHmmSS = "yyyy-MM-dd'T'HH:mm:ss";
	public static final String DATE_FORMAT_WITH_TIMEZONE = "yyyy-MM-dd HH:mm:ss z";
	public static final Integer HOURS_OF_A_DAY = 24;
	public static final long SECOND_IN_MILLISECONDS = 1000;
	private static final String DATE_FORMAT_SLASHES_SEPARATOR_dd_MM_yyyy = "dd/MM/yyyy";
	private static final String DATE_FORMAT_SLASHES_SEPARATOR_dd_MM_yy = "dd/MM/yy";
	private static final String DATE_FORMAT_dd_MM_PIPE_HH_mm = "dd/MM | HH:mm";
	private static final String DATE_FORMAT_HH_mm_PIPE_SLASHES_SEPARATOR_dd_MM_yyyy = "HH:mm | dd/MM/yyyy";
	private static final String DATE_FORMAT_SLASHES_SEPARATOR_dd_MM_yyyy_PIPE_HH_mm_ = "dd/MM/yyyy | HH:mm";
	private static final String DATE_FORMAT_HH_mm = "HH:mm";
	private static final String FILE_SAVE_DATE_FORMAT = "yyyyMMdd_hhmmssSSS";
	private static final String ENC_DATE_FORMAT = "yyyyMMddHHmmss";
	private static final String ENC_DATE_FORMAT_CHECKIN = "yyyyMMddHHmm";
	private static final String NAME_OF_DAY = "EEEE";
	private static final String MONTH_NAME_AND_YEAR = "MMMM yyyy";
	private static final Locale LOCALE_HEBREW = new Locale("iw");
	
	public static Calendar convertYearMonthDayToCalendar(final int iYear, final int iMonth, final int iDayOfMonth)
	{
		Calendar result = Calendar.getInstance();
		
		result.set(Calendar.YEAR, iYear);
		result.set(Calendar.MONTH, iMonth);
		result.set(Calendar.DAY_OF_MONTH, iDayOfMonth);
		
		return result;
	}
	
	/**
	 * @param iCalendarToConvert date.
	 *
	 * @return a string in the format {@link DateTimeUtils#DATE_FORMAT_SLASHES_SEPARATOR_dd_MM_yyyy} or empty string.
	 */
	public static String convertCalendarToSlashSeparatedDayMonthYearString(final Calendar iCalendarToConvert)
	{
		String result = "";
		
		if (iCalendarToConvert != null)
		{
			result = convertDateToStringFormatWithLocaleOrEmptyString(iCalendarToConvert.getTime(), DATE_FORMAT_SLASHES_SEPARATOR_dd_MM_yyyy, LOCALE_HEBREW);
		}
		
		return result;
	}
	
	public static String convertDateToStringFormatWithLocaleOrEmptyString(Date iDate, String iFormat, Locale iLocale)
	{
		String result = "";
		Locale locale = iLocale == null ? Locale.getDefault() : iLocale;
		
		if (iDate != null && !TextUtils.isEmpty(iFormat))
		{
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(iFormat, locale);
			result = simpleDateFormat.format(iDate);
		}
		
		return result;
	}
	
	public static String convertDateToNameOfDayWithLocaleOrString(Date iDate)
	{
		String result = "";
		
		if (iDate != null)
		{
			result = convertDateToStringFormatWithLocaleOrEmptyString(iDate, NAME_OF_DAY, LocaleUtils.getLocale());
		}
		
		return result;
	}
	
	public static String convertCalendarToSlashSeparatedDayMonthYearString(final Date iDateToConvert)
	{
		String result = "";
		
		if (iDateToConvert != null)
		{
			result = convertDateToStringFormatWithLocaleOrEmptyString(iDateToConvert, DATE_FORMAT_SLASHES_SEPARATOR_dd_MM_yyyy, LOCALE_HEBREW);
		}
		
		return result;
	}
	
	/**
	 * @param iDateToConvert date.
	 *
	 * @return a string in the format {@link DateTimeUtils#DATE_FORMAT_SLASHES_SEPARATOR_dd_MM_yyyy} or empty string.
	 */
	public static String convertDateToSlashSeparatedDayMonthYearString(final Date iDateToConvert)
	{
		String result = "";
		
		if (iDateToConvert != null)
		{
			//			result = convertDateToStringFormatWithLocaleOrEmptyString(iDateToConvert, DATE_FORMAT_SLASHES_SEPARATOR_dd_MM_yyyy, LOCALE_HEBREW);
			result = convertDateToStringFormatByGMTOrEmptyString(iDateToConvert, DATE_FORMAT_SLASHES_SEPARATOR_dd_MM_yyyy);
		}
		
		return result;
	}
	
	/**
	 * @param iDateToConvert date.
	 *
	 * @return a string in the format {@link DateTimeUtils#DATE_FORMAT_SLASHES_SEPARATOR_dd_MM_yy} or empty string.
	 */
	public static String convertDateToSlashSeparatedDayMonthYearTwoNumbersString(final Date iDateToConvert)
	{
		String result = "";
		
		if (iDateToConvert != null)
		{
			result = convertDateToStringFormatWithLocaleOrEmptyString(iDateToConvert, DATE_FORMAT_SLASHES_SEPARATOR_dd_MM_yy, LocaleUtils.getLocale());
		}
		
		return result;
	}
	
	/**
	 * @param iDateToConvert date.
	 *
	 * @return a string in the format {@link DateTimeUtils#MONTH_NAME_AND_YEAR} or empty string.
	 */
	public static String convertDateToMonthNameAndYearOrString(final Date iDateToConvert)
	{
		String result = "";
		
		if (iDateToConvert != null)
		{
			result = convertDateToStringFormatWithLocaleOrEmptyString(iDateToConvert, MONTH_NAME_AND_YEAR, LocaleUtils.getLocale());
		}
		
		return result;
	}
	
	/**
	 * @param iDateToConvert date.
	 *
	 * @return a string in the format {@link DateTimeUtils#DATE_FORMAT_HH_mm} or empty string.
	 */
	public static String convertDateToHourMinuteString(final Date iDateToConvert)
	{
		String result = "";
		
		if (iDateToConvert != null)
		{
			result = convertDateToStringFormatByGMTOrEmptyString(iDateToConvert, DATE_FORMAT_HH_mm);
			//			result = convertDateToStringFormatWithLocaleOrEmptyString(iDateToConvert, DATE_FORMAT_HH_mm, LocaleUtils.getLocale());
		}
		
		return result;
	}
	
	public static String convertDateToHourMinuteStringWithGMT(final Date iDateToConvert)
	{
		String result = "";
		
		if (iDateToConvert != null)
		{
			//			result = convertDateToStringFormatByGMTOrEmptyString(iDateToConvert, DATE_FORMAT_HH_mm);
			result = convertDateToStringFormatWithLocaleOrEmptyString(iDateToConvert, DATE_FORMAT_HH_mm, LocaleUtils.getLocale());
		}
		
		return result;
	}
	
	private static String convertDateToStringFormatByGMTOrEmptyString(Date iDate, String iFormat)
	{
		String result = "";
		
		if (iDate != null && !TextUtils.isEmpty(iFormat))
		{
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(iFormat, LOCALE_HEBREW);
			//			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
			result = simpleDateFormat.format(iDate);
		}
		
		return result;
	}
	
	/**
	 * @param iDateToConvert date.
	 *
	 * @return a string in the format {@link DateTimeUtils#DATE_FORMAT_HH_mm_PIPE_SLASHES_SEPARATOR_dd_MM_yyyy} or empty string.
	 */
	public static String convertDateToHourMinuteDayMonthYearString(final Date iDateToConvert, eLanguage iLanguage)
	{
		String result = "";
		
		if (iDateToConvert != null)
		{
			if (iLanguage.isHebrew())
			{
				result = convertDateToStringFormatWithLocaleOrEmptyString(iDateToConvert, DATE_FORMAT_HH_mm_PIPE_SLASHES_SEPARATOR_dd_MM_yyyy, LocaleUtils.getLocale());
			}
			else if (iLanguage.isEnglish())
			{
				result = convertDateToStringFormatWithLocaleOrEmptyString(iDateToConvert, DATE_FORMAT_SLASHES_SEPARATOR_dd_MM_yyyy_PIPE_HH_mm_, LocaleUtils.getLocale());
			}
		}
		
		return result;
	}
	
	/**
	 * @param iDateToConvert date.
	 *
	 * @return a string in the format {@link DateTimeUtils#DATE_FORMAT_HH_mm} or empty string.
	 */
	public static String convertDateToHourMinuteStringByGMT(final Date iDateToConvert)
	{
		String result = "";
		
		if (iDateToConvert != null)
		{
			result = convertDateToStringFormatByGMTOrEmptyString(iDateToConvert, DATE_FORMAT_HH_mm);
		}
		
		return result;
	}
	
	/**
	 * @param iDateToConvert date.
	 *
	 * @return a string in the format {@link DateTimeUtils#FILE_SAVE_DATE_FORMAT} or empty string.
	 */
	public static String convertDateToFileSaveDateString(final Date iDateToConvert)
	{
		String result = "";
		
		if (iDateToConvert != null)
		{
			result = convertDateToStringFormatWithLocaleOrEmptyString(iDateToConvert, FILE_SAVE_DATE_FORMAT, LOCALE_HEBREW);
		}
		
		return result;
	}
	
	public static Date convertStringToFileSaveDate(final String iDateToConvert)
	{
		Date result = null;
		
		if (iDateToConvert != null)
		{
			result = convertStringWithFormatToDateOrNull(iDateToConvert, FILE_SAVE_DATE_FORMAT);
		}
		
		return result;
	}
	
	public static String convertDateToENCDateString(final Date iDateToConvert, boolean isCheckin)
	{
		String result = "";
		
		if (iDateToConvert != null)
		{
			result = convertDateToStringFormatWithLocaleOrEmptyString(iDateToConvert, isCheckin ? ENC_DATE_FORMAT_CHECKIN : ENC_DATE_FORMAT, LOCALE_HEBREW);
		}
		
		return result;
	}
	
	/**
	 * @param iDateStringInSlashSeparatedDayMonthYear String in the format {@link DateTimeUtils#DATE_FORMAT_SLASHES_SEPARATOR_dd_MM_yyyy}
	 *
	 * @return a date object from the string or null.
	 */
	public static Date convertStringInSlashSeparatedDayMonthYearToDate(String iDateStringInSlashSeparatedDayMonthYear)
	{
		return convertStringWithFormatToDateWithLocaleOrNull(iDateStringInSlashSeparatedDayMonthYear, DATE_FORMAT_SLASHES_SEPARATOR_dd_MM_yyyy, LOCALE_HEBREW);
	}
	
	private static Date convertStringWithFormatToDateWithLocaleOrNull(String iDateInStringToConvert, String iFormat, Locale iLocale)
	{
		Date result = null;
		Locale locale = iLocale == null ? Locale.getDefault() : iLocale;
		
		
		if (!TextUtils.isEmpty(iDateInStringToConvert) && !TextUtils.isEmpty(iFormat))
		{
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(iFormat, locale);
			try
			{
				result = simpleDateFormat.parse(iDateInStringToConvert);
			}
			catch (ParseException e)
			{
				e.printStackTrace();
			}
		}
		
		return result;
	}
	
	public static Integer getTimeDifferenceInDaysOrNull(Date iDate1, Date iDate2)
	{
		LocalDate date1 = new LocalDate(iDate1);
		LocalDate date2 = new LocalDate(iDate2);
		
		return Days.daysBetween(date1, date2).getDays();
		
		//
		//
		//		Integer result = null;
		//
		//		if (iDate1 != null && iDate2 != null)
		//		{
		//			iDate1.setHours(0);
		//			iDate2.setHours(0);
		//
		//			iDate1.setMinutes(0);
		//			iDate2.setMinutes(0);
		//
		//			iDate1.setSeconds(0);
		//			iDate2.setSeconds(0);
		//
		//			//			//milliseconds
		//			long different = iDate2.getTime() - iDate1.getTime();
		//
		//			//			long secondsInMilli = 1000;
		//			//			long minutesInMilli = secondsInMilli * 60;
		//			//			long hoursInMilli = minutesInMilli * 60;
		//			//			long daysInMilli = hoursInMilli * 24;
		//			//
		//			//			long elapsedDays = different / daysInMilli;
		//			//			different = different % daysInMilli;
		//
		//			//			result = (int) elapsedDays + 1;
		//
		//			int daysDiffer = (int) TimeUnit.DAYS.convert(different, TimeUnit.MILLISECONDS);
		//
		//			result = daysDiffer;
		//		}
		//
		//		return result;
	}
	
	public static long getTimeDifferenceInMilliseconds(Date iDateFrom, Date iDateTo)
	{
		long result = 0;
		
		if (iDateFrom != null && iDateTo != null)
		{
			result = iDateTo.getTime() - iDateFrom.getTime();
		}
		
		return result;
	}
	
	public static Date convertDateToDateIgnoreGMT(Date iDate)
	{
		if (iDate != null)
		{
			//			String strDate = convertDateToStringFormatByGMTOrEmptyString(iDate, DATE_FORMAT_SLASHES_SEPARATOR_dd_MM_yyyy_PIPE_HH_mm_);
			//			return convertStringWithFormatToDateOrNull(strDate, DATE_FORMAT_SLASHES_SEPARATOR_dd_MM_yyyy_PIPE_HH_mm_);
			
			DateFormat df = new SimpleDateFormat(DATE_FORMAT_SLASHES_SEPARATOR_dd_MM_yyyy_PIPE_HH_mm_, LOCALE_HEBREW);
			df.setTimeZone(TimeZone.getTimeZone("GMT"));
			String dateStr = df.format(iDate);
			
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_SLASHES_SEPARATOR_dd_MM_yyyy_PIPE_HH_mm_, LOCALE_HEBREW);
			Date date = null;
			try
			{
				date = sdf.parse(dateStr);
			}
			catch (ParseException e)
			{
				e.printStackTrace();
			}
			
			return date;
		}
		else
		{
			return null;
		}
	}
	
	
	//	public static Date convertDateToDateIgnoreGMT(Date iDate)
	//	{
	//		if (iDate != null)
	//		{
	//			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_yyyy_MM_ddHHmmSS);
	//			sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
	//			return sdf.format(iDate);
	//
	//		}
	//		else
	//		{
	//			return null;
	//		}
	//	}
	
	/**
	 * @param iDateToConvert date.
	 *
	 * @return a string in the format {@link DateTimeUtils#DATE_FORMAT_dd_MM_PIPE_HH_mm} or empty string.
	 */
	public static String convertDateToDayMonthHourMinuteString(final Date iDateToConvert)
	{
		String result = "";
		
		if (iDateToConvert != null)
		{
			result = convertDateToStringFormatByGMTOrEmptyString(iDateToConvert, DATE_FORMAT_dd_MM_PIPE_HH_mm);
			//			result = convertDateToStringFormatWithLocaleOrEmptyString(iDateToConvert, DATE_FORMAT_dd_MM_PIPE_HH_mm, LocaleUtils.getLocale());
			//			result = convertDateToStringFormatWithLocaleOrEmptyString(iDateToConvert, DATE_FORMAT_yyyy_MM_ddHHmmSS, LocaleUtils.getLocale());
		}
		
		return result;
	}
	
	private static Date convertStringWithFormatToDateOrNull(String iDateInStringToConvert, String iFormat)
	{
		Date result = null;
		
		if (!TextUtils.isEmpty(iDateInStringToConvert) && !TextUtils.isEmpty(iFormat))
		{
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(iFormat, LOCALE_HEBREW);
			SimpleTimeZone tz = new SimpleTimeZone(0, "Out Timezone");
			simpleDateFormat.setTimeZone(tz);
			
			try
			{
				result = simpleDateFormat.parse(iDateInStringToConvert);
			}
			catch (ParseException e)
			{
				e.printStackTrace();
			}
		}
		
		return result;
	}
	
	public static String convertDDeltaInMillisecondsToTimeString(final long iTimeDeltaMilliseconds)
	{
		String result;
		
		long second = (iTimeDeltaMilliseconds / SECOND_IN_MILLISECONDS) % 60;
		long minute = (iTimeDeltaMilliseconds / (SECOND_IN_MILLISECONDS * 60)) % 60;
		long hour = (iTimeDeltaMilliseconds / (SECOND_IN_MILLISECONDS * 60 * 60));
		
		result = String.format(LOCALE_HEBREW, "%02d:%02d:%02d", hour, minute, second);
		
		return result;
	}
	
	public static String[] convertDDeltaInMillisecondsToTimeStringArray(final long iTimeDeltaMilliseconds)
	{
		String result[] = new String[3];
		
		long second = (iTimeDeltaMilliseconds / SECOND_IN_MILLISECONDS) % 60;
		long minute = (iTimeDeltaMilliseconds / (SECOND_IN_MILLISECONDS * 60)) % 60;
		long hour = (iTimeDeltaMilliseconds / (SECOND_IN_MILLISECONDS * 60 * 60));
		
		result[0] = String.format("%02d", hour);
		result[1] = String.format("%02d", minute);
		result[2] = String.format("%02d", second);
		
		return result;
	}
	
	public static String convertDDeltaInMillisecondsToTimeStringWithoutSeconds(final long iTimeDeltaMilliseconds)
	{
		String result;
		
		long second = (iTimeDeltaMilliseconds / SECOND_IN_MILLISECONDS) % 60;
		long minute = (iTimeDeltaMilliseconds / (SECOND_IN_MILLISECONDS * 60)) % 60;
		long hour = (iTimeDeltaMilliseconds / (SECOND_IN_MILLISECONDS * 60 * 60));
		
		result = String.format(LOCALE_HEBREW, "%02d:%02d", hour, minute);
		
		return result;
	}
	
	//TODO Just for testing (delete it)
	public static Date getDate(eCalendarUnit iCalendarUnit, int iAmount)
	{
		return getDate(null, iCalendarUnit, iAmount);
	}
	
	public static Date getDate(Date iStartDate, eCalendarUnit iCalendarUnit, int iAmount)
	{
		//if you like to get date in future put positive amount otherwise negative
		
		Calendar calendar = Calendar.getInstance();
		
		if (iStartDate != null)
		{
			calendar.setTime(iStartDate);
		}
		
		calendar.add(iCalendarUnit.getCalendarUnit(), iAmount);
		
		return calendar.getTime();
	}
	
	public static boolean dateHasPassed(Date iDateToCompare)
	{
		Date currentDate = new Date();
		currentDate = convertDateToDateIgnoreGMT(currentDate);
		Integer dateTimeDifferenceInHours = getTimeDifferenceInHoursOrNull(currentDate, iDateToCompare);
		
		return dateTimeDifferenceInHours < 0;
	}
	
	public static Integer getTimeDifferenceInHoursOrNull(Date iDate1, Date iDate2)
	{
		//		LocalDate date1 = new LocalDate(iDate1);
		//		LocalDate date2 = new LocalDate(iDate2);
		//		return Hours.hoursBetween(date1, date2).getHours();
		
		
		Integer result = null;
		
		//		iDate1 = convertDateToDateIgnoreGMT(iDate1);
		//		iDate2 = convertDateToDateIgnoreGMT(iDate2);
		
		if (iDate1 != null && iDate2 != null)
		{
			result = (int) TimeUnit.HOURS.convert(iDate2.getTime() - iDate1.getTime(), TimeUnit.MILLISECONDS);
		}
		
		return result;
	}
	
	public static Integer getTimeDifferenceInMinutesOrNull(Date iDate1, Date iDate2)
	{
		Integer result = null;
		
		if (iDate1 != null && iDate2 != null)
		{
			result = (int) TimeUnit.MINUTES.convert(iDate2.getTime() - iDate1.getTime(), TimeUnit.MILLISECONDS);
		}
		
		return result;
	}
	
	public static boolean isDatesIsEqualsByParam(Date iDate1, Date iDate2, eCalendarUnit iCalendarUnit)
	{
		DateObject first = new DateObject(iDate1, iCalendarUnit);
		DateObject second = new DateObject(iDate2, iCalendarUnit);
		
		return first.equals(second);
	}
	
	public static Date getDateNow()
	{
		//		String formattedDate = new SimpleDateFormat(DATE_FORMAT_yyyy_MM_ddHHmmSS, LOCALE_HEBREW).format(Calendar.getInstance().getTime());
		//		Date date = convertStringWithFormatToDateOrNull(formattedDate, DATE_FORMAT_yyyy_MM_ddHHmmSS);
		//
		SimpleDateFormat isoFormat = new SimpleDateFormat(DATE_FORMAT_yyyy_MM_ddHHmmSS, LOCALE_HEBREW);
		//		isoFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		
		Date today = new Date();
		
		String formattedDate = new SimpleDateFormat(DATE_FORMAT_yyyy_MM_ddHHmmSS, LOCALE_HEBREW).format(today);
		
		
		Date date = null;
		try
		{
			date = isoFormat.parse(formattedDate);
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}
		
		return date;
	}
	
	public static Date getCurrentDateByGMT(final String iTimezoneOffset)
	{
		Date date = new Date();
		return getDateByGMT(date, iTimezoneOffset);
		//		SimpleDateFormat readFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"/* DateTimeUtils.DATE_FORMAT_WITH_TIMEZONE*/);
		//		readFormat.setTimeZone(TimeZone.getTimeZone("GMT" +iTimezoneOffset+ ":00"));
		//		String dStr = readFormat.format(date);
		//		DateFormat writeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		//		//				writeFormat.setTimeZone(TimeZone.getTimeZone("GMT-05:00"));
		//		try
		//		{
		//
		//			date = writeFormat.parse(dStr);
		//		}
		//		catch (ParseException iE)
		//		{
		//			iE.printStackTrace();
		//		}
		//		return date;
	}
	
	public static Date getDateByGMT(Date iDate, final String iTimezoneOffset)
	{
		SimpleDateFormat readFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"/* DateTimeUtils.DATE_FORMAT_WITH_TIMEZONE*/);
//		readFormat.setTimeZone(TimeZone.getTimeZone("GMT" /*+iTimezoneOffset+ NOTE: the good solution. allow it after MARCH version*/ + /*AVISHAY 15/04/18: JUST FOR MARCH VERSION */ "+03" + ":00"));
		readFormat.setTimeZone(TimeZone.getTimeZone("GMT" +iTimezoneOffset+    ":00"));

		String dStr = readFormat.format(iDate);
		DateFormat writeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		//				writeFormat.setTimeZone(TimeZone.getTimeZone("GMT-05:00"));
		try
		{
			iDate = writeFormat.parse(dStr);
		}
		catch (ParseException iE)
		{
			iE.printStackTrace();
		}
		return iDate;
	}
	
	public static Date convertDateToDateWithGMT(Date iDate, String iOffset)
	{
		if (iDate != null)
		{
			if (iOffset == null || TextUtils.isEmpty(iOffset))
			{
				iOffset = "0";
			}
			
			//			String strDate = convertDateToStringFormatByGMTOrEmptyString(iDate, DATE_FORMAT_SLASHES_SEPARATOR_dd_MM_yyyy_PIPE_HH_mm_);
			//			return convertStringWithFormatToDateOrNull(strDate, DATE_FORMAT_SLASHES_SEPARATOR_dd_MM_yyyy_PIPE_HH_mm_);
			
			DateFormat df = new SimpleDateFormat(DATE_FORMAT_SLASHES_SEPARATOR_dd_MM_yyyy_PIPE_HH_mm_, LOCALE_HEBREW);
			
			if (iOffset.contains("+"))
			{
				iOffset.replace("+", "-");
			}
			else if (iOffset.contains("-"))
			{
				iOffset.replace("-", "+");
			}
			
			df.setTimeZone(TimeZone.getTimeZone("GMT " + iOffset));
			String dateStr = df.format(iDate);
			
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_SLASHES_SEPARATOR_dd_MM_yyyy_PIPE_HH_mm_, LOCALE_HEBREW);
			Date date = null;
			try
			{
				date = sdf.parse(dateStr);
			}
			catch (ParseException e)
			{
				e.printStackTrace();
			}
			
			return date;
		}
		else
		{
			return null;
		}
	}
	
	public enum eCalendarUnit
	{
		NOTING(-1),
		HOUR(Calendar.HOUR),
		DAY(Calendar.DAY_OF_WEEK),
		MONTH(Calendar.MONTH);
		
		private int mCalendarUnit;
		
		eCalendarUnit(final int iCalendarUnit)
		{
			mCalendarUnit = iCalendarUnit;
		}
		
		public int getCalendarUnit()
		{
			return mCalendarUnit;
		}
		
		public boolean isHour()
		{
			return eCalendarUnit.this == HOUR;
		}
		
		public boolean isDayOfWeek()
		{
			return eCalendarUnit.this == DAY;
		}
		
		public boolean isMonth()
		{
			return eCalendarUnit.this == MONTH;
		}
		
		public boolean isNoting()
		{
			return eCalendarUnit.this == NOTING;
		}
	}
	
	private static class DateObject
	{
		int mYear;
		int mMonth;
		int mDay;
		eCalendarUnit mCalendarUnit = eCalendarUnit.NOTING;
		
		public DateObject(final int iYear, final int iMonth, final int iDay, eCalendarUnit iCalendarUnit)
		{
			mYear = iYear;
			mMonth = iMonth;
			mDay = iDay;
			mCalendarUnit = iCalendarUnit;
		}
		
		DateObject(Date iDate, eCalendarUnit iCalendarUnit)
		{
			if (iDate != null)
			{
				Calendar cal = Calendar.getInstance();
				cal.setTime(iDate);
				mYear = cal.get(Calendar.YEAR);
				mMonth = cal.get(Calendar.MONTH);
				mDay = cal.get(Calendar.DAY_OF_MONTH);
				mCalendarUnit = iCalendarUnit;
			}
		}
		
		@Override
		public boolean equals(final Object obj)
		{
			boolean result = false;
			
			if (obj instanceof DateObject)
			{
				DateObject compareDate = (DateObject) obj;
				
				if (mCalendarUnit.isNoting())
				{
					result = false;
				}
				else if (mCalendarUnit.isDayOfWeek())
				{
					result = this.mYear == compareDate.mYear && this.mMonth == compareDate.mMonth && this.mDay == compareDate.mDay;
				}
			}
			
			return result;
		}
	}
}
