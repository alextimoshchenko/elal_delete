package ui.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.tagmanager.DataLayer;

import java.util.ArrayList;

import global.GlobalVariables;
import global.IntentExtras;
import global.UserData;
import il.co.ewave.elal.R;
import interfaces.IAddFlightListener;
import ui.activities.EWBaseActivity;
import ui.activities.MyFlightsActivity;
import ui.customWidgets.ElalCustomTilLL;
import utils.errors.ErrorsHandler;
import utils.errors.ServerError;
import utils.global.AppUtils;
import utils.global.CustomTextWatcher;
import utils.global.myFlightsObjects.MyFlightsObject;
import webServices.controllers.MainController;
import webServices.controllers.MyFlightsController;
import webServices.global.RequestStringBuilder;
import webServices.requests.RequestGetUserActivePNRs;
import webServices.requests.RequestPairGroupPNRToUser;
import webServices.requests.RequestRetrievePNR;
import webServices.responses.ResponseGetUserActivePNRs;
import webServices.responses.ResponsePairGroupPnrToUser;
import webServices.responses.ResponseRetrievePNR;
import webServices.responses.responseGetUserActivePNRs.UserActivePnr;

/**
 * Created by Avishay.Peretz on 17/08/2017.
 */

public class AddNewFlightFragment extends EWBaseFragment implements Response.ErrorListener
{
	private static final String TAG = AddNewFlightFragment.class.getSimpleName();
	private static final int DEFAULT_TYPE = 1;
	private static final int CUSTOM_TYPE = 2;
	
	private Button mBtnPerformCheckIn;
	private Button mBtnSaveBoardingPass;
	private ElalCustomTilLL mTilOrderNumber, mTilFamilyName, mTilTicketNumber;
	private EditText mEtOrderNumber;
	private EditText mEtFamilyName;
	private EditText mEtTicketNumber;
	private Button mBtnContinue;
	private TextView mTvCancelCheckIn;
	private LinearLayout llAddNewFlightCenterView;
	private LinearLayout llFlightTicketNumberCenterView;
	private IAddFlightListener mAddFlightListener;
	private boolean mShouldShowTicketNumberCenterView;
	private boolean mResult = false;
	private String mCurrentPnr = "";
	private TextView mTvTitle;
	
	
	public static Bundle createBundleForFragment(@NonNull final boolean iIsGroupPnr)
	{
		Bundle result = new Bundle();
		result.putBoolean(IntentExtras.IS_GROUP_PNR, iIsGroupPnr);
		
		return result;
	}
	
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_add_new_flight_layout);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		
		getIntentExtras();
		initReference();
		setScreenTitle();
		setListeners();
		setGui();
		
		setDebugData();
		
		pushScreenOpenEvent("Add a flight");
	}
	
	private void setDebugData()
	{
		if (GlobalVariables.isDebugVersion)
		{
//			mEtOrderNumber.setText("SMN8M2");
//			mEtFamilyName.setText("MALKI");
		}
	}
	
	private void setGui()
	{
		
		setCenterViewGui();
	}
	
	private void setScreenTitle()
	{
		String title = getActivity().getString(R.string.manage_flight_screen_title);
		
		if (!TextUtils.isEmpty(title))
		{
			mTvTitle.setText(title);
			mTvTitle.setVisibility(View.VISIBLE);
			
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
			{
				mTvTitle.setTextAppearance(R.style.Text_18sp_Topaz_Light);
			}
			else
			{
				mTvTitle.setTextAppearance(getActivity(), R.style.Text_18sp_Topaz_Light);
			}
			Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Rubik-Light.ttf");
			mTvTitle.setTypeface(face);
		}
		else
		{
			mTvTitle.setVisibility(View.INVISIBLE);
		}
		
	}
	
	private void getIntentExtras()
	{
		if (getArguments() != null)
		{
			mAddFlightListener = (IAddFlightListener) getArguments().getSerializable(IntentExtras.ADD_FLIGHT_LISTENER);
			mShouldShowTicketNumberCenterView = getArguments().getBoolean(IntentExtras.IS_GROUP_PNR, false);
		}
	}
	
	private void initReference()
	{
		mTvTitle = getActivity().findViewById(R.id.tv_name_screen_title);
		
		mBtnPerformCheckIn = getActivity().findViewById(R.id.btn_addNewFlight_PerformCheckIn);
		mBtnSaveBoardingPass = getActivity().findViewById(R.id.btn_addNewFlight_SaveBoardingPass);
		llAddNewFlightCenterView = getActivity().findViewById(R.id.ll_addNewFlight_AddFlight);
		llFlightTicketNumberCenterView = getActivity().findViewById(R.id.ll_addNewFlight_GroupPnr);
		
		mTilOrderNumber = getActivity().findViewById(R.id.til_addNewFlight_OrderNumber);
		mTilOrderNumber.init(getString(R.string.reservation_code));
		//		mTilOrderNumber.setInputTypeTextOnly();
		mEtOrderNumber = mTilOrderNumber.getEditText();
		//		mEtOrderNumber = (EditText) getActivity().findViewById(R.id.et_addNewFlight_OrderNumber);
		
		mTilFamilyName = getActivity().findViewById(R.id.til_addNewFlight_FamilyName);
		mTilFamilyName.init(getActivity().getResources().getText(R.string.family_name_));
		//		mTilFamilyName.setInputTypeTextOnly();
		mEtFamilyName = mTilFamilyName.getEditText();
		mEtFamilyName.setImeOptions(EditorInfo.IME_ACTION_DONE);
		//		mEtFamilyName = (EditText) getActivity().findViewById(R.id.et_addNewFlight_FamilyName);
		
		
		mTilTicketNumber = getActivity().findViewById(R.id.til_addNewFlight_FlightTicketNumber);
		mTilTicketNumber.init(getString(R.string.flight_ticket_number));
		mTilTicketNumber.setInputTypeNumeric();
		mEtTicketNumber = mTilTicketNumber.getEditText();
		mEtTicketNumber.setImeOptions(EditorInfo.IME_ACTION_DONE);
		//		mEtTicketNumber = (EditText) getActivity().findViewById(R.id.et_addNewFlight_FlightTicketNumber);
		
		
		mBtnContinue = getActivity().findViewById(R.id.btn_addNewFlight_Continue);
		mTvCancelCheckIn = getActivity().findViewById(R.id.tv_addNewFlight_CancelCheckIn);
	}
	
	private void setListeners()
	{
		
		getActivity().findViewById(R.id.iv_name_screen_back_arrow).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				//				doOnBack();
				getActivity().onBackPressed();
				
			}
		});
		
		mBtnPerformCheckIn.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				onPerformCheckInClick();
			}
		});
		
		mBtnSaveBoardingPass.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				onSaveBoardingPassClick();
			}
		});
		
		mBtnContinue.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				hideKeyboard();
				onContinueClick();
			}
		});
		
		mTvCancelCheckIn.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				onCancelCheckInClick();
			}
		});
		
		
		mEtFamilyName.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				if (!TextUtils.isEmpty(s.toString()) && !isLastNameEnContainsLatinCharactersOnly())
				{
					showFamilyNameEnError(true, DEFAULT_TYPE);
				}
				else
				{
					showFamilyNameEnError(false, DEFAULT_TYPE);
				}
			}
		});
		
		
		mEtFamilyName.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus && !TextUtils.isEmpty(mEtFamilyName.getText().toString()))
				{
					if (!isLastNameEnContainsLatinCharactersOnly())
					{
						showFamilyNameEnError(true, DEFAULT_TYPE);
					}
					else if (!isLastNameEnContainsAtLeastTwoChar())
					{
						showFamilyNameEnError(true, CUSTOM_TYPE);
					}
				}
			}
		});
		
		
		mEtTicketNumber.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				showTicketNumberError(false, DEFAULT_TYPE);
			}
		});
		
		
		mEtTicketNumber.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus)
				{
					if (!AppUtils.isTicketNumberValid(mEtTicketNumber.getText().toString()))
					{
						showTicketNumberError(true, CUSTOM_TYPE);
					}
				}
			}
		});
		
		
	}
	
	private void doOnBack()
	{
		tryPopFragment();
	}
	
	private void showTicketNumberError(final boolean iState, final int iType)
	{
		if (iState)
		{
			if (iType == DEFAULT_TYPE)
			{
				mTilTicketNumber.setError(true);
			}
			else if (iType == CUSTOM_TYPE)
			{
				mTilTicketNumber.setError(true, getActivity().getResources().getString(R.string.ticket_number_13_digits_error));
			}
		}
		else
		{
			mTilTicketNumber.setError(false);
		}
	}
	
	private void showFamilyNameEnError(boolean iState, int iType)
	{
		if (iState)
		{
			if (iType == DEFAULT_TYPE)
			{
				mTilFamilyName.setError(true, getActivity().getResources().getString(R.string.only_latin_alphabet));
			}
			else if (iType == CUSTOM_TYPE)
			{
				mTilFamilyName.setError(true, getActivity().getResources().getString(R.string.at_least_two_char_long));
			}
		}
		else
		{
			mTilFamilyName.setError(false, null);
		}
	}
	
	
	private void setCenterViewGui()
	{
		llAddNewFlightCenterView.setVisibility(mShouldShowTicketNumberCenterView ? View.GONE : View.VISIBLE);
		llFlightTicketNumberCenterView.setVisibility(mShouldShowTicketNumberCenterView ? View.VISIBLE : View.GONE);
	}
	
	private void onPerformCheckInClick()
	{
		tryGoToWebFragment(RequestStringBuilder.getCheckInUrlByLanguage());
		
		((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage flights & Check In", "Action", "Online check in", "Label", RequestStringBuilder.getCheckInUrlByLanguage()));
	}
	
	private void onSaveBoardingPassClick()
	{
		tryGoToWebFragment(RequestStringBuilder.getSaveBoardingPassUrlByLanguage());
		
		((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage flights & Check In", "Action", "Airplane boarding pass", "Label", RequestStringBuilder.getSaveBoardingPassUrlByLanguage()));
	}
	
	private void onContinueClick()
	{
		if (mShouldShowTicketNumberCenterView)
		{
			final String ticketNumber = mEtTicketNumber.getText().toString();
			
			if (!TextUtils.isEmpty(ticketNumber))
			{
				if (!AppUtils.isTicketNumberValid(ticketNumber))
				{
					showTicketNumberError(true, CUSTOM_TYPE);
				}
				else
				{
					trySetProgressDialog(true);
					
					if ((MyFlightsObject.getInstance().getCurrentUserActivePnr() != null && MyFlightsObject.getInstance()
					                                                                                       .getCurrentUserActivePnr()
					                                                                                       .isGroupPnr()) && !TextUtils.isEmpty(MyFlightsObject.getInstance()
					                                                                                                                                           .getCurrentUserActivePnr()
					                                                                                                                                           .getPnr()))
					{
						mCurrentPnr = MyFlightsObject.getInstance().getCurrentUserActivePnr().getPnr();
					}
					else
					{
						mCurrentPnr = mEtOrderNumber.getText().toString().toUpperCase();
					}
					
					if (!TextUtils.isEmpty(mCurrentPnr))
					{
						getService().getController(MainController.class)
						            .PairGroupPnrToUser(new RequestPairGroupPNRToUser(mCurrentPnr, ticketNumber), new Response.Listener<ResponsePairGroupPnrToUser>()
						            {
							            @Override
							            public void onResponse(final ResponsePairGroupPnrToUser response)
							            {
								            UserData.getInstance().getGroupPnrs().remove(MyFlightsObject.getInstance().getCurrentUserActivePnr());
								            
								            //event 36
								            ((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage flights & Check In", "Action",
										            mCurrentPnr + " - Group", "Label", ticketNumber));
								            
								            AppUtils.printLog(Log.ERROR, TAG, String.valueOf(mCurrentPnr) + " : " + String.valueOf(ticketNumber) + " - " +
												            "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
								            
								            setSuccessfulResult(mCurrentPnr);
							            }
						            }, new Response.ErrorListener()
						            {
							            @Override
							            public void onErrorResponse(final VolleyError error)
							            {
								            trySetProgressDialog(false);
								            showFlightNotFoundDialog();
							            }
						            });
					}
				}
			}
			
			else
			{
				showTicketNumberError(true, DEFAULT_TYPE);
			}
		}
		else
		{
			if (isDataValidAndShowErrorMessageIfNeeded() && isServiceConnected())
			{
				trySetProgressDialog(true);
				getService().getController(MainController.class)
				            .RetrievePnr(new RequestRetrievePNR(mEtOrderNumber.getText().toString().toUpperCase(), mEtFamilyName.getText().toString()), new Response.Listener<ResponseRetrievePNR>()
				            {
					            @Override
					            public void onResponse(final ResponseRetrievePNR response)
					            {
						            onRetrievePnrResponse(response);
					            }
				            }, new Response.ErrorListener()
				            {
					            @Override
					            public void onErrorResponse(final VolleyError error)
					            {
						
						            trySetProgressDialog(false);
						            showFlightNotFoundDialog();
					            }
				            });
			}
			
			//event 35
			String activePnr = UserData.getInstance().getActivePnr();
			((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage flights & Check In", "Action", "Add a flight - Continue", "Label", activePnr));
			
		}
	}
	
	private void showFlightNotFoundDialog()
	{
		AlertDialog dialog = AppUtils.createSimpleMessageDialog(getActivity(), getResources().getString(R.string.error_S301_could_not_find_your_reservation_details), getResources().getString(R.string.close), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, final int which)
			{
				dialog.dismiss();
			}
		}, null, null);
		dialog.setCancelable(false);
		dialog.show();
	}
	
	private boolean isDataValidAndShowErrorMessageIfNeeded()
	{
		boolean result = true;
		
		
		if (!isLastNameEnContainsLatinCharactersOnly())
		{
			result = false;
			showFamilyNameEnError(true, 1);
			//			focusOnView(mSvMainContainer, mTilLastNameEn);
		}
		
		if (!isLastNameEnContainsAtLeastTwoChar())
		{
			result = false;
			showFamilyNameEnError(true, 2);
			//			focusOnView(mSvMainContainer, mTilLastNameEn);
		}
		
		if (TextUtils.isEmpty(mEtOrderNumber.getText().toString()))
		{
			result = false;
			showOrderError(true);
		}
		
		return result;
	}
	
	private void showOrderError(final boolean iState)
	{
		if (iState)
		{
			mTilOrderNumber.setError(true);
		}
		else
		{
			mTilOrderNumber.setError(false, null);
		}
	}
	
	
	private void onRetrievePnrResponse(final ResponseRetrievePNR iResponse)
	{
		if (iResponse != null && iResponse.getContent() != null && iResponse.getContent().isGroupPNR())
		{
			trySetProgressDialog(false);
			switchToPairGroupPnrToUserView();
		}
		else
		{
			if (mEtOrderNumber != null && !TextUtils.isEmpty(mEtOrderNumber.getText()))
			{
				mCurrentPnr = mEtOrderNumber.getText().toString();
			}
			else
			{
				mCurrentPnr = "";
			}
			
			setSuccessfulResult(mCurrentPnr);
		}
	}
	
	private void onCancelCheckInClick()
	{
		tryGoToWebFragment(RequestStringBuilder.getCancelCheckInUrlByLanguage());
		
		//event 37
		((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage flights & Check In", "Action", "Cancel check in", "Label", "Outbound Link"));
	}
	
	private void setSuccessfulResult(final String iPnr)
	{
		trySetProgressDialog(false);
		mResult = true;
		
		if (userHasPnrs())
		{
			try
			{
//				mCurrentPnr = "";
				((MyFlightsActivity) getActivity()).mCurrentFlightPnr = "";
				getUserActivePNRs();
				//				((MyFlightsActivity) getActivity()).loadMyFlightsFragment();
			}
			catch (Exception e)
			{
				AppUtils.printLog(Log.ERROR, TAG, e.getMessage());
			}
		}
		else
		{
			try
			{
				((MyFlightsActivity) getActivity()).loadMyFlightFragment(iPnr);
			}
			catch (Exception e)
			{
				AppUtils.printLog(Log.ERROR, TAG, e.getMessage());
			}
		}
	}
	
	private void getUserActivePNRs()
	{
		GetUserActivePNRs();
	}
	
	public void GetUserActivePNRs()
	{
		if (isServiceConnected())
		{
			trySetProgressDialog(true);
			
			int userId = UserData.getInstance().getUserID();
			
			getService().getController(MyFlightsController.class).GetUserActivePNRs(new RequestGetUserActivePNRs(userId), new Response.Listener<ResponseGetUserActivePNRs>()
			{
				@Override
				public void onResponse(final ResponseGetUserActivePNRs iResponse)
				{
					trySetProgressDialog(false);
					if (iResponse != null && iResponse.getContent() != null)
					{
						handleResponseGetUserActivePNRs(iResponse.getContent());
					}
				}
			}, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError iError)
				{
					trySetProgressDialog(false);
					
					((MyFlightsActivity) getActivity()).loadMyFlightsFragment();
					ErrorsHandler.tryShowServiceErrorDialog(iError, getActivity());
				}
			});
		}
	}
	
	private void handleResponseGetUserActivePNRs(ArrayList<UserActivePnr> iUserActivePnrsList)
	{
		MyFlightsObject.getInstance().setUserActivePnrList(iUserActivePnrsList);
		if (!UserData.getInstance().isUserGuest() && !UserData.getInstance().getGroupPnrs().isEmpty())
		{
			MyFlightsObject.getInstance().getUserActivePnrList().addAll(UserData.getInstance().getGroupPnrs());
		}
		
		((MyFlightsActivity) getActivity()).loadMyFlightsFragment();
	}
	
	
	private boolean userHasPnrs()
	{
		//I changed this logic, because if you connect with new mizdamen user and immidiately add a flight so detail screen is open and then you go to the
//		side menu and try to add one more flight, actual result : again detail screen is opening , expected result : cards screen should be opened , because it has already second flight
//		according to the documentation if we have more then 1 flight card screen should be opened
//		HomeScreenContent homeScreenContent = AppData.getInstance().getHomeItems();
//		return homeScreenContent != null && homeScreenContent.getPNRCount() > 0;
		
		MyFlightsObject obj = MyFlightsObject.getInstance();
		return obj != null && obj.getUserActivePnrList().size() > 0;
	}
	
	
	@Override
	public void onDestroy()
	{
		trySetAddFlightResult();
		
		super.onDestroy();
	}
	
	private void trySetAddFlightResult()
	{
		if (mAddFlightListener != null)
		{
			mAddFlightListener.addFlightListener(mResult);
		}
	}
	
	private void switchToPairGroupPnrToUserView()
	{
		mShouldShowTicketNumberCenterView = true;
		setCenterViewGui();
	}
	
	@Override
	public void onErrorResponse(final VolleyError error)
	{
		if (error != null && error instanceof ServerError && ((utils.errors.ServerError) error).getErrorCode() == utils.errors.ServerError.eServerError.LognetPNRNotFound.getErrorCode())
		{
			trySetProgressDialog(false);
			
			AlertDialog dialog = AppUtils.createSimpleMessageDialog(getActivity(), getResources().getString(R.string.incorrect_username_or_password_matmid), getResources().getString(R.string.close), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(final DialogInterface dialog, final int which)
				{
					dialog.dismiss();
				}
			}, null, null);
			dialog.setCancelable(false);
			dialog.show();
		}
		else
		{
			ErrorsHandler.tryShowServiceErrorDialog(error, getActivity());
		}
	}
	
	
	public boolean isFamilyNameValid()
	{
		
		return AppUtils.isStringContainsLatinCharactersOnly(mEtFamilyName.getText().toString());
		
	}
	
	private boolean isLastNameEnContainsLatinCharactersOnly()
	{
		return AppUtils.isStringContainsLatinCharactersOnlyLetters(mEtFamilyName.getText().toString());
	}
	
	private boolean isLastNameEnContainsAtLeastTwoChar()
	{
		return AppUtils.isStringContainsAtLeastTwoChar(mEtFamilyName.getText().toString());
	}
	
}

