# DexGuard's default settings are fine for this sample application.

# We'll just display some more statistics about the processed code.
-verbose

# If you encounter problems in your project, you can let DexGuard instrument
# your code, so the app prints out configuration suggestions at run-time, in
# the logcat:
#
#!!!NEED TO BE DISABLED IN RELEASE VERSION!!!
#-addconfigurationdebugging

# Otherwise, you can try to narrow down the issue by disabling some processing
# steps:
#
#-dontshrink
#!!!need to be disabled in release version!!!
#-dontoptimize
#-dontobfuscate
-multidex

#-keepresourcefiles lib/*/libScanovateImagingSDK_CPP.so
#-keepresourcefiles **/libScanovateImagingSDK_CPP.so
-keepresourcefiles **.so

# You can also check if the problem can be solved by keeping additional
# code and/or resources:
#
#!!!NEED TO BE DISABLED IN RELEASE VERSION!!!
#-keep class * { *; }
#-keepattributes *
#-keepresources */*
#-keepresourcefiles res/**
#-keepresourcefiles assets/**
#-keepresourcefiles lib/**
#-keepresourcexmlelements **
#-keepresourcexmlattributenames **
#-printconfiguration config.txt

#!!!NEED TO BE ENABLE IN RELEASE VERSION!!!
# Remove the logging code in the exceptions.
#-assumenosideeffects class android.util.Log {
#   public static int d(...);
#}

#Asset
-keepresourcefiles assets/fonts/exljbris_MuseoSans-500.otf
-keepresourcefiles assets/fonts/exljbris_MuseoSans-900.otf
-keepresourcefiles assets/fonts/Rubik-Bold.ttf
-keepresourcefiles assets/fonts/Rubik-Light.ttf
-keepresourcefiles assets/fonts/Rubik-Regular.ttf
-keepresourcefiles assets/fonts/Spoiler-Bold.ttf
-keepresourcefiles assets/fonts/Spoiler-Regular.ttf

#This configuration keep classes and class members and all classes that inherit from them under these dir
-keep,allowshrinking,allowobfuscation class webServices.responses.**{ *; }

-keep,allowshrinking,allowobfuscation class webServices.requests.**{ *; }

#-----------------------------
-keepclassmembers class utils.global.dbObjects.Document$eDocumentType { *; }

-keepclassmembers class webServices.global.ePnrFlightType { *; }

-keepclassmembers class io.realm.DefaultRealmModuleMediator { *; }

-keepclassmembers class webServices.push.PushObject { *; }

-keepclassmembers class interfaces.IChoseActionElement { *; }

-keepclassmembers class com.qualcomm.qti.Performance { *; }

-keep class com.google.android.gms.tagmanager.TagManagerService

-keepresources integer/google_app_measurement_enable

-keep class com.google.firebase.crash.FirebaseCrash

-keep class com.google.firebase.auth.FirebaseAuth

-keepresources string/ga_trackingId

-keep class io.realm.internal.objectserver.SyncObjectServerFacade

-keep class rx.Observable

-keepresources attr/selectableItemBackgroundBorderless

#SSL
-encryptstrings class services.EWSSLService

-encryptstrings class utils.ssl.SslUtils

-encryptstrings "https://www.elal.com/*"

-encryptstrings "https://www.elal-matmid.com/*"

-encryptstrings class com.guardsquare.dexguard.runtime.net.PublicKeyTrustManager

-encryptstrings "MD5"
-encryptstrings "BKS"
-encryptstrings "TLS"
-encryptstrings "GET"
-encryptstrings "X509"
-encryptstrings "https"

-encryptclasses com.guardsquare.dexguard.runtime.**

-encryptstrings public class utils.ssl.SslUtils {
    java.lang.String PROTOCOL_TLSv1;
    java.lang.String PROTOCOL_TLSv1_1;
    java.lang.String PROTOCOL_TLSv1_2;

    java.lang.String SECRET_KEY;
    java.lang.String TRUST_MANAGER_X509;
    java.lang.String SSL_LOCAL_HOST_PROD;
    java.lang.String SSL_LOCAL_HOST_DEV;
    java.lang.String SSL_LOCAL_HOST_MATMID;

    java.lang.String MATMID_CA_KEY;
    java.lang.String MATMID_ROOT_KEY;
    java.lang.String MIZDAMEN_CA_KEY;
    java.lang.String MIZDAMEN_ROOT_KEY;

    java.lang.String KEYSTORE_EXTENSION_BKS;

    java.lang.String COUNT_OF_MATCHES;
    java.lang.String COUNT_OF_HOST_MATCHES;

    java.lang.String SSL_PORT;
}

# Encrypt all layout resource files.
#-encryptresourcefiles res/layout/**

# Encrypt all strings in the class that checks the environment and sets up
# the activity.
-encryptstrings class ui.activities.EWBaseActivity$Delegate

# Encrypt the classes that check the environment, as another layer of
# protection.
-encryptclasses ui.activities.EWBaseActivity$Delegate
-encryptclasses com.guardsquare.dexguard.runtime.**

#Fabric
-keepattributes *Annotation*
-keepattributes SourceFile,LineNumberTable
-keep public class * extends java.lang.Exception
-keepresourcexmlelements manifest/application/meta-data@name=io.fabric.ApiKey
-keep class com.crashlytics.** { *; }
-keepresources string/com.crashlytics.Trace
-keepresources bool/com.crashlytics.Trace
-keepresources string/com.crashlytics.CollectCustomLogs
-keepresources bool/com.crashlytics.CollectCustomLogs
-keepresources string/com.crashlytics.RequireBuildId
-keepresources bool/com.crashlytics.RequireBuildId
-keepresources string/io.fabric.android.build_id
-dontwarn com.crashlytics.**
#-dontwarn com.squareup.okhttp.**
-keepresources string/com.crashlytics.ApiEndpoint
-keep class com.google.android.gms.ads.AdView
-keepresources string/com.crashlytics.CollectUserIdentifiers
-keepresources bool/com.crashlytics.CollectUserIdentifiers
-keepresources string/com.crashlytics.CollectDeviceIdentifiers
-keepresources bool/com.crashlytics.CollectDeviceIdentifiers
-keepresources string/com.crashlytics.ApiKey
-keepresources string/io.fabric.ApiKey
-keepresources string/com.crashlytics.useFirebaseAppId
-keepresources bool/com.crashlytics.useFirebaseAppId
-keep class com.google.android.instantapps.supervisor.InstantAppsRuntime

-keep,allowshrinking,allowobfuscation,includecode class com.crashlytics.android.core.CrashTest { *; }

-keep,allowshrinking,allowobfuscation,includecode class ui.fragments.WebViewFragment { *; }

-keep,allowshrinking,allowobfuscation class com.fasterxml.jackson.** { *; }

#Scannovate
#-keep class scanovate.ocr.** { *; }
-keep class scanovate.** {*; }
#-keep class scanovate.** {*; }

#SharePreferenses
-encryptclasses utils.global.ElalPreferenceUtils
-encryptstrings utils.global.ElalPreferenceUtils

-encryptclasses utils.global.APreferencesUtils
-encryptstrings utils.global.APreferencesUtils


-keep class global.ElAlApplication