package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with care by Shahar Ben-Moshe on 21/02/17.
 */

public class RequestSetUserCheckList
{
	@JsonProperty("PNR")
	private String mPnr;
	@JsonProperty("UserID")
	private int mUserID;
	@JsonProperty("TimeDimentionID")
	private int mTimeDimensionID;
	@JsonProperty("UserItemTitle")
	private String mUserItemTitle;
	@JsonProperty("IsDone")
	private boolean mIsDone;
	@JsonProperty("UserItemID")
	private int mUserItemID;
	
	
//	public RequestSetUserCheckList(final String iPnr, final int iUserID, final int iTimeDimensionID, final String iUserItemTitle, final boolean iIsDone)
//	{
//		mPnr = iPnr;
//		mUserID = iUserID;
//		mTimeDimensionID = iTimeDimensionID;
//		mUserItemTitle = iUserItemTitle;
//		mIsDone = iIsDone;
//	}
	
	public RequestSetUserCheckList(final String iPnr, final int iUserID, final int iTimeDimensionID, final String iUserItemTitle, final boolean iIsDone, final int iUserItemID)
	{
		mPnr = iPnr;
		mUserID = iUserID;
		mTimeDimensionID = iTimeDimensionID;
		mUserItemTitle = iUserItemTitle;
		mIsDone = iIsDone;
		mUserItemID = iUserItemID;
	}
}
