package services;

import android.os.Binder;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import utils.errors.LocalError;
import utils.global.AppUtils;
import utils.ssl.PubKeyManager;
import utils.ssl.SslHurlStuck;
import utils.ssl.SslUtils;
import webServices.controllers.AController;
import webServices.controllers.MatmidInterface;
import webServices.controllers.MizdamenInterfave;

/**
 * Created with care by Alexey.T on 26/10/2017.
 * <p>
 * TODO: Add a class header comment!
 */
public class EWSSLService extends EWService
{
	private static final String TAG = EWSSLService.class.getSimpleName();
	
	private RequestQueue mSSLMizdamenRequestQueue;
	private RequestQueue mSSLMatmidRequestQueue;
	
	/**
	 * Retrieves the wanted controller by the controller type from the controllers hashMap.<BR>
	 * If this is the first time retrieving the controller it will be created and added to the controllers hashMap.<BR>
	 *
	 * @param iControllerClass - The class of the controller to retrieve. Must implement IController.
	 *
	 * @return The instance of the wanted controller.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T extends AController> T getController(final Class<T> iControllerClass)
	{
		T result = (T) mControllers.get(iControllerClass.getSimpleName());
		
		if (result == null)
		{
			try
			{
				result = iControllerClass.newInstance();
				
				if (result instanceof MizdamenInterfave)
				{
					result.setVolleyRequestQueue(mSSLMizdamenRequestQueue);
				}
				else if (result instanceof MatmidInterface)
				{
					result.setVolleyRequestQueue(mSSLMatmidRequestQueue);
				}
				else
				{
					throw new Exception("Controller need to implement one of the MizdamenInterfave or MatmidInterface");
				}
				
				mControllers.put(iControllerClass.getSimpleName(), result);
			}
			catch (Exception iE)
			{
				AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.ControllerError.getErrorMessage());
			}
		}
		
		return result;
	}
	
	@Override
	public void initQueues() throws Throwable
	{
		super.initQueues();
		
		PubKeyManager allKeys = new PubKeyManager(SslUtils.MIZDAMEN_CA_KEY, SslUtils.MIZDAMEN_ROOT_KEY, SslUtils.MATMID_CA_KEY, SslUtils.MATMID_ROOT_KEY);
		
		mSSLMizdamenRequestQueue = Volley.newRequestQueue(this, new SslHurlStuck(allKeys));
		mSSLMatmidRequestQueue = Volley.newRequestQueue(this, new SslHurlStuck(allKeys));
	}
	
	public class EWSSLServiceBinder extends Binder
	{
		/**
		 * @return bound service.
		 */
		public EWSSLService getService()
		{
			return EWSSLService.this;
		}
	}
	
	@Override
	protected void initBind()
	{
		mBinder = new EWSSLServiceBinder();
	}
}
