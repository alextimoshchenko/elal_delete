package webServices.push;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import global.AppData;
import global.ElAlApplication;
import global.IntentExtras;
import global.eInAppNavigation;
import il.co.ewave.elal.R;
import utils.errors.LocalError.eLocalError;
import utils.global.AppUtils;
import webServices.global.Mapper;

/**
 * Created with care by Shahar Ben-Moshe on 22/11/16.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService
{
	private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
	private static final String MESSAGE = "Message";
	private static final int NOTIFICATION_ID_MIN = 100000;
	private static final int NOTIFICATION_ID_MAX = Integer.MAX_VALUE;
	
	public static final String INTENT_NOTIFICATION = "elal.notification_received";
	
	@Override
	public void onMessageReceived(RemoteMessage iRemoteMessage)
	{
		handleOnMassageReceivedSimple(iRemoteMessage);
	}
	
	private void handleOnMassageReceivedSimple(final RemoteMessage iRemoteMessage)
	{
		AppUtils.printLog(Log.DEBUG, TAG, "handleOnMassageReceivedSimple");
		
		if (iRemoteMessage != null && iRemoteMessage.getData() != null)
		{
			handleOnMassageReceived(iRemoteMessage);
			
			//This broadcast we use to update messages number in side menu
			Intent intentBroadcast = new Intent(INTENT_NOTIFICATION);
			sendBroadcast(intentBroadcast);
		}
		else
		{
			AppUtils.printLog(Log.DEBUG, TAG, eLocalError.NotificationError.getErrorMessage());
		}
	}
	
	private void handleOnMassageReceived(final RemoteMessage iRemoteMessage)
	{
		try
		{
			if (iRemoteMessage != null && iRemoteMessage.getData() != null && iRemoteMessage.getData().get(MESSAGE) != null)
			{
				PushObject pushObject = Mapper.object(new JSONObject(iRemoteMessage.getData()).toString(), PushObject.class);
				Intent intent = null;
				if( ElAlApplication.isAppWasInBackground() || AppData.getInstance().getModuleId()>0){ //the app is alive
					intent = AppUtils.getMainIntent(getApplicationContext());
				}
				else{// the app was killed
					intent = AppUtils.getSplashIntent(getApplicationContext());
				}
				if (pushObject != null && pushObject.getLinkMode() != null)
				{
					switch (pushObject.getLinkMode())
					{
						case Error:
						{
//							intent = AppUtils.getSplashIntent(getApplicationContext());
							break;
						}
						case InApp:
						{
//							intent = AppUtils.getSplashIntent(getApplicationContext());
							eInAppNavigation inAppNavigation;
							
							try
							{
								inAppNavigation = eInAppNavigation.getInAppNavigationByIdOrErrorDefault(Integer.parseInt(pushObject.getUrl()));
							}
							catch (NumberFormatException ignore)
							{
								inAppNavigation = eInAppNavigation.Main;
							}
							
							if (inAppNavigation != eInAppNavigation.Error)
							{
								intent.putExtra(IntentExtras.IN_APP_NAVIGATION_ID, inAppNavigation.getNavigationId());
							}
							
							break;
						}
						case Url:
						{
//							intent = AppUtils.getSplashIntent(getApplicationContext());
							intent.putExtra(IntentExtras.URL, pushObject.getUrl());
							break;
						}
						case Store:
						{
							intent = AppUtils.getAppInMarketIntent();
							break;
						}
						default:
						{
							AppUtils.printLog(Log.DEBUG, TAG, eLocalError.NotificationError.getErrorMessage());
						}
					}
					
					if (!pushObject.getLinkMode().isError() && intent != null)
					{
						intent.putExtra(IntentExtras.IS_FROM_PUSH_NOTIFICATION, true);
						AppUtils.sendSimpleNotification(getApplicationContext(), getString(R.string.app_name), pushObject.getMessageText(), pushObject.getMessageText(), 0, intent, AppUtils.generateRandomNumberInRange(NOTIFICATION_ID_MIN, NOTIFICATION_ID_MAX));
					}
				}
				else
				{
					AppUtils.printLog(Log.DEBUG, TAG, eLocalError.NotificationError.getErrorMessage());
				}
			}
			else
			{
				AppUtils.printLog(Log.DEBUG, TAG, eLocalError.NotificationError.getErrorMessage());
			}
		}
		catch (Exception e)
		{
			AppUtils.printLog(Log.DEBUG, TAG, e.getMessage());
		}
	}
}
