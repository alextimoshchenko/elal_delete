package webServices.responses.getSearchGlobalData;

import com.fasterxml.jackson.annotation.JsonProperty;

import interfaces.ISearchElement;

public class GetSearchGlobalDataContentElalCities implements ISearchElement
{
	@JsonProperty("AliasText")
	private String mAliasText;
	@JsonProperty("BoardPoint")
	private boolean mBoardPoint;
	@JsonProperty("BonusCity")
	private boolean mBonusCity;
	@JsonProperty("CityCode")
	private String mCityCode;
	@JsonProperty("CityName")
	private String mCityName;
	@JsonProperty("CountryCode")
	private String mCountryCode;
	@JsonProperty("InMoneyAndPointsFlight")
	private boolean mInMoneyAndPointsFlight;
	@JsonProperty("IsCruisesCity")
	private boolean mIsCruisesCity;
	@JsonProperty("IsFamily")
	private boolean mIsFamily;
	@JsonProperty("IsGalorCity")
	private boolean mIsGalorCity;
	@JsonProperty("IsMagazine")
	private boolean mIsMagazine;
	@JsonProperty("IsSundor")
	private boolean mIsSundor;
	@JsonProperty("OffPoint")
	private boolean mOffPoint;
	@JsonProperty("PaymentCity")
	private boolean mPaymentCity;
	
	public GetSearchGlobalDataContentElalCities()
	{
	}
	
	@Override
	public String getAliasText()
	{
		return mAliasText;
	}
	
	public boolean isBoardPoint()
	{
		return mBoardPoint;
	}
	
	public boolean isBonusCity()
	{
		return mBonusCity;
	}
	
	public boolean isInMoneyAndPointsFlight()
	{
		return mInMoneyAndPointsFlight;
	}
	
	public boolean isCruisesCity()
	{
		return mIsCruisesCity;
	}
	
	public boolean isFamily()
	{
		return mIsFamily;
	}
	
	public boolean isGalorCity()
	{
		return mIsGalorCity;
	}
	
	public boolean isMagazine()
	{
		return mIsMagazine;
	}
	
	public boolean isSundor()
	{
		return mIsSundor;
	}
	
	public boolean isOffPoint()
	{
		return mOffPoint;
	}
	
	public boolean isPaymentCity()
	{
		return mPaymentCity;
	}
	
	public String getCityCode()
	{
		return this.mCityCode;
	}
	
	public String getCityName()
	{
		return this.mCityName;
	}
	
	public String getCountryCode()
	{
		return this.mCountryCode;
	}
	
	@Override
	public String getElementNameEn()
	{
		return getCityName();
	}
	
	@Override
	public String getElementNameHe()
	{
		return getCityName();
	}
	
	@Override
	public String toString()
	{
		return getCityName();
	}
}
