package utils;

import android.text.TextUtils;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import utils.global.AppUtils;
import utils.ssl.SslUtils;
import webServices.responses.ResponseGetAppData;
import webServices.responses.ResponseIpInfo;
import webServices.responses.ResponseKeepAlive;
import webServices.responses.ResponseSetUserMatmidDetails;
import webServices.responses.getMobileCountryCodes.ResponseGetMobileCountryCodes;
import webServices.responses.getSearchDepartureCities.ResponseSearchDepartureCities;
import webServices.responses.getSearchDestiationCities.ResponseSearchDestinationCities;

/**
 * Created by Avishay.Peretz on 01/11/2017.
 */

public class HashUtilsManager
{
	
	private static HashUtilsManager instance;
	
	
	private HashUtilsManager()
	{
	}
	
	public static HashUtilsManager getInstance()
	{
		if (instance == null)
		{
			instance = new HashUtilsManager();
		}
		return instance;
	}
	
	
	public boolean isAuthenticHash(final String iJsonString, final String iHeaderHash)
	{
		if (getStringHashByString(iJsonString, true).equalsIgnoreCase(iHeaderHash))
		{
			return true;
		}
		return false;
	}
	
	private static final String TAG = HashUtilsManager.class.getSimpleName();
	
	
	public String getStringHashByString(final String iBody )
	{
		try
		{
			String body = iBody.toLowerCase();
			//			AppUtils.printLog(Log.DEBUG, TAG, "getBody : " + body);
			//			AppUtils.printLog(Log.DEBUG, TAG, "getBody3 : " + getStringForHash(body) + SslUtils.SECRET_KEY);
			String hashString = SslUtils.doHashSha256(getStringForHash(body, false) + SslUtils.SECRET_KEY);
			//			AppUtils.printLog(Log.DEBUG, TAG, "getBody6 : " + "SHA256 = " + hashString);
			return hashString;
		}
		catch (Exception iError)
		{
			iError.printStackTrace();
		}
		return iBody;
	}
	
	
	public String getStringHashByString(final String iBody, boolean isResponse)
	{
		try
		{
			String body = iBody.toLowerCase();
//			AppUtils.printLog(Log.DEBUG, TAG, "getBody : " + body);
//			AppUtils.printLog(Log.DEBUG, TAG, "getBody3 : " + getStringForHash(body) + SslUtils.SECRET_KEY);
			String hashString = SslUtils.doHashSha256(getStringForHash(body, isResponse) + SslUtils.SECRET_KEY);
//			AppUtils.printLog(Log.DEBUG, TAG, "getBody6 : " + "SHA256 = " + hashString);
			return hashString;
		}
		catch (Exception iError)
		{
			iError.printStackTrace();
		}
		return iBody;
	}
	
	public String getSecretHashHeader(final byte[] iBody )
	{
		try
		{
			String body = convertByteArrayToString(iBody);
			//			String body = new String(iBody, "UTF-8");
			AppUtils.printLog(Log.DEBUG, TAG, "getBody : " + body);
			return getStringHashByString(body, false);
		}
		//		catch (UnsupportedEncodingException iError)
		//		{
		//			iError.printStackTrace();
		//		}
		catch (Exception iError)
		{
			iError.printStackTrace();
		}
		return "";
	}
	
	public String convertByteArrayToString(final byte[] iBody)
	{
		if (iBody != null && iBody.length > 0)
		{
			try
			{
				return new String(iBody, "UTF-8");
			}
			catch (UnsupportedEncodingException iE)
			{
				AppUtils.printLog(Log.ERROR, TAG, iE.getMessage());
			}
		}
		
		return "";
	}
	
	
	private String getStringForHash(String iStr, boolean isRequest)
	{
		String ans = sortStringAlphabetically(iStr.toLowerCase());
		return removeUnnecessaryChars(ans, isRequest);
	}
	
	private static String removeUnnecessaryChars(String iStr, boolean isResponse)
	{
		try
		{
			if (!TextUtils.isEmpty(iStr))
			{
				if(isResponse)
					iStr = iStr.replaceAll("[^a-zA-Zא-ת]", "");
				else{//type = REQUEST
				iStr = iStr.replaceAll("[^a-zA-Zא-ת1-9]", "");
				
				}
				iStr = iStr.replaceAll("n", "");
				iStr = iStr.replaceAll("u", "");
				iStr = iStr.replaceAll("l", "");
				
			}
			return iStr.toString();
		}
		catch (Exception iE)
		{
			iE.printStackTrace();
		}
		return iStr;
		
	}
	
	public String sortStringAlphabetically(String iStr)
	{
		try
		{
			char[] charArray = iStr.toCharArray();
			Arrays.sort(charArray);
			String sortedString = new String(charArray);
			return sortedString;
		}
		catch (Exception iE)
		{
			iE.printStackTrace();
			
		}
		return iStr;
	}
	
	public boolean isHashService(final String iResponseType)
	{
		if (iResponseType.equalsIgnoreCase(ResponseGetAppData.class.getSimpleName()) || iResponseType.equalsIgnoreCase(ResponseSearchDepartureCities.class.getSimpleName()) || iResponseType.equalsIgnoreCase(ResponseSearchDestinationCities.class
				.getSimpleName()) || iResponseType.equalsIgnoreCase(ResponseIpInfo.class.getSimpleName()) || iResponseType.equalsIgnoreCase(ResponseKeepAlive.class.getSimpleName())
				|| iResponseType.equalsIgnoreCase(ResponseGetMobileCountryCodes.class.getSimpleName()))
		{
			return false;
		}
		return true;
	}
	
	
	public static String handleMatmidJsonIfNeed(final String iResponseType, final String iJsonString)
	{
		if (iResponseType.equalsIgnoreCase(ResponseSetUserMatmidDetails.class.getSimpleName()) || iResponseType.equalsIgnoreCase(ResponseSearchDepartureCities.class.getSimpleName()) || iResponseType.equalsIgnoreCase(ResponseSearchDestinationCities.class
				.getSimpleName()) || iResponseType.equalsIgnoreCase(ResponseIpInfo.class.getSimpleName()))
		{
			
		}
		return iJsonString;
	}
	
	
}
