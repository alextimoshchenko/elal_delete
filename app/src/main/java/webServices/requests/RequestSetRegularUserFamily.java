package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class RequestSetRegularUserFamily
{
	@JsonProperty("UserID")
	private int mUserID;
	//	@JsonProperty("FamilyMembers")
	//	private ArrayList<RequestSetUserFamilyMember> mFamilyMembers;
	@JsonProperty("FamilyMember")
	private RequestSetUserFamilyMember mFamilyMember;
	
	//	public RequestSetRegularUserFamily(int iUserID, ArrayList<RequestSetUserFamilyMember> iFamilyMembers)
	//	{
	//		mUserID = iUserID;
	//		mFamilyMembers = iFamilyMembers;
	//	}
	//
	//	public RequestSetRegularUserFamily(int iUserID, RequestSetUserFamilyMember iFamilyMember)
	//	{
	//		mUserID = iUserID;
	//		mFamilyMembers = new ArrayList<>();
	//		mFamilyMembers.add(iFamilyMember);
	//	}
	
	
	public RequestSetRegularUserFamily(final int iUserID, final RequestSetUserFamilyMember iFamilyMember)
	{
		mUserID = iUserID;
		mFamilyMember = iFamilyMember;
	}
}
