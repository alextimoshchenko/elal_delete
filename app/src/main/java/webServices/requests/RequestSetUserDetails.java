package webServices.requests;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Calendar;
import java.util.Date;

import global.UserData;
import utils.global.DateTimeUtils;

/**
 * Created by Avishay.Peretz on 28/06/2017.
 */
public class RequestSetUserDetails
{
	@JsonProperty("UserID")
	private int mUserID;
	@JsonProperty("Email")
	private String mEmail;
	@JsonProperty("PhoneNumber")
	private String mPhoneNumber;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("CountryCode")
	private String mCountryCode;
	@JsonProperty("UserPassword")
	private String mPassword;
	@JsonProperty("Title")
	private String mTitle;
	@JsonProperty("FirstNameEng")
	private String mFirstNameEng;
	@JsonProperty("LastNameEng")
	private String mLastNameEng;
	@JsonProperty("MatmidMemberId")
	private String mMatmidMemberId;
	@JsonProperty("FirstNameHeb")
	private String mFirstNameHeb;
	@JsonProperty("LastNameHeb")
	private String mLastNameHeb;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeUtils.DATE_FORMAT_yyyy_MM_ddHHmmSS)
	@JsonProperty("DOB")
	private Date mDOB;
	@JsonProperty("PassportNum")
	private String mPassportNum;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeUtils.DATE_FORMAT_yyyy_MM_ddHHmmSS)
	@JsonProperty("PassportExpiration")
	private Date mPassportExpiration;
	@JsonProperty("IDNumber")
	private String mIDNumber;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateTimeUtils.DATE_FORMAT_yyyy_MM_ddHHmmSS)
	@JsonProperty("LanguageId")
	private String mLanguageId;
	@JsonProperty("RepresentationId")
	private int mRepresentationId;
	//	@JsonProperty("PushFlightsInfo")
	//	private Boolean mPushFlightsInfo;
	//	@JsonProperty("PushOffersInfo")
	//	private Boolean mPushOffersInfo;
	@JsonProperty("MatmidPassword")
	private String mMatmidMemberPassword;
	
	
	public RequestSetUserDetails(String iEmail, String iMatmidMemberId, Calendar iPassportExpiration, String iLastNameEng, String iPassportNum, String iTitle, String iPhoneNumber, String iCountryCode,
	                             String iFirstNameEng, int iUserID, Calendar iDOB, String iIDNumber, int iRepresentationId, String iFirstNameHeb, String iLastNameHeb, String iLanguageId,
	                             String iPassword, String iMatmidMemberPassword)
	{
		
		mEmail = iEmail;
		mMatmidMemberId = iMatmidMemberId;
		mPassportExpiration = iPassportExpiration == null ? null : iPassportExpiration.getTime();
		mLastNameEng = iLastNameEng;
		mPassportNum = iPassportNum;
		mTitle = iTitle;
		mPhoneNumber = iPhoneNumber;
		if (UserData.getInstance().getUserObject() != null && UserData.getInstance().getUserObject().isMatmid())
		{
			mCountryCode = iCountryCode;
		}
		mFirstNameEng = iFirstNameEng;
		mUserID = iUserID;
		mDOB = iDOB == null ? null : iDOB.getTime();
		mIDNumber = iIDNumber;
		mRepresentationId = iRepresentationId;
		mFirstNameHeb = iFirstNameHeb;
		mLastNameHeb = iLastNameHeb;
		mLanguageId = iLanguageId;
		mPassword = iPassword;
		mMatmidMemberPassword = iMatmidMemberPassword;
	}
	
	//	public RequestSetUserDetails(String iEmail, Calendar iPassportExpiration, String iLastNameEng, String iPassportNum, String iPhoneNumber, String iFirstNameEng, int iUserID, Calendar iDOB,
	//	                             String iIDNumber, int iRepresentationId, String iFirstNameHeb, String iLastNameHeb, String iLanguageId, String iPassword)
	//	{
	//		mEmail = iEmail;
	//		mPassportExpiration = iPassportExpiration == null ? null : iPassportExpiration.getTime();
	//		mLastNameEng = iLastNameEng;
	//		mPassportNum = iPassportNum;
	//		mPhoneNumber = iPhoneNumber;
	//		mFirstNameEng = iFirstNameEng;
	//		mUserID = iUserID;
	//		mDOB = iDOB == null ? null : iDOB.getTime();
	//		mIDNumber = iIDNumber;
	//		mRepresentationId = iRepresentationId;
	//		mFirstNameHeb = iFirstNameHeb;
	//		mLastNameHeb = iLastNameHeb;
	//		mLanguageId = iLanguageId;
	//		mPassword = iPassword;
	//	}
}
