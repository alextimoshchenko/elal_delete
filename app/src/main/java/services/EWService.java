package services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.util.LruCache;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.Volley;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import utils.ssl.SslUtils;
import utils.errors.LocalError;
import utils.global.AppUtils;
import utils.global.CustomImageLoader;
import webServices.controllers.AController;

/**
 * Represents Business Logic Layer
 * Provides background functions.
 *
 * @author Galit.Feller
 */
public class EWService extends Service
{
	private static final String TAG = EWService.class.getSimpleName();
	private static final int sf_MAX_ACTIVE_THREADS_IN_THREAD_POOL = 3;
	private static final int IMAGE_LOADER_CACHE_SIZE = 30;
	
	protected RequestQueue mRequestQueue;
	private CustomImageLoader mImageLoader;
	protected HashMap<String, AController> mControllers;
	public IBinder mBinder;
	private ExecutorService mThreadExecutor;
	
	/**
	 * Retrieves the wanted controller by the controller type from the controllers hashMap.<BR>
	 * If this is the first time retrieving the controller it will be created and added to the controllers hashMap.<BR>
	 *
	 * @param iControllerClass - The class of the controller to retrieve. Must implement IController.
	 *
	 * @return The instance of the wanted controller.
	 */
	@SuppressWarnings("unchecked")
	public <T extends AController> T getController(Class<T> iControllerClass)
	{
		T result = (T) mControllers.get(iControllerClass.getSimpleName());
		
		if (result == null)
		{
			try
			{
				result = iControllerClass.newInstance();
				result.setVolleyRequestQueue(mRequestQueue);
				mControllers.put(iControllerClass.getSimpleName(), result);
			}
			catch (Exception iE)
			{
				AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.ControllerError.getErrorMessage());
			}
		}
		
		return result;
	}
	
	@Override
	public void onCreate()
	{
		super.onCreate();
		initBind();
		
		CookieHandler.setDefault(new CookieManager());
		
		try
		{
			initQueues();
		}
		catch (Throwable iThrowable)
		{
			AppUtils.printLog(Log.ERROR, TAG,LocalError.eLocalError.GeneralConnectionError.getErrorMessage() + iThrowable);
		}
		
		initImageLoader();
		mControllers = new HashMap<>();
		mThreadExecutor = Executors.newFixedThreadPool(sf_MAX_ACTIVE_THREADS_IN_THREAD_POOL);
	}
	
	protected void initBind()
	{
		mBinder = new EWServiceBinder();
	}
	
	protected void initQueues() throws Throwable
	{
		mRequestQueue = Volley.newRequestQueue(this, new HttpClientStack(createSslHttpClientIfNeeded()));
	}
	
	private void initImageLoader()
	{
		mImageLoader = new CustomImageLoader(Volley.newRequestQueue(getApplicationContext(), new HttpClientStack(createSslHttpClientIfNeeded())), new CustomImageLoader.ImageCache()
		{
			private final LruCache<String, Bitmap> mCache = new LruCache<>(IMAGE_LOADER_CACHE_SIZE);
			
			@Override
			public Bitmap getBitmap(String url)
			{
				return mCache.get(url);
			}
			
			@Override
			public void putBitmap(String url, Bitmap bitmap)
			{
				mCache.put(url, bitmap);
			}
			
		});
	}
	
	public CustomImageLoader getImageLoader()
	{
		return mImageLoader;
	}
	
	private CloseableHttpClient createSslHttpClientIfNeeded()
	{
		CloseableHttpClient httpClient;
		
		try
		{
			//            if (RequestStringBuilder.isUrlSecure())
			//            {
			httpClient = SslUtils.createSslHttpClient(getApplication().getApplicationContext(), SslUtils.PROTOCOL_TLSv1_2, null, null);
			
			//            }
			//            else
			//            {
			//                httpClient = HttpClients.createDefault();
			//            }
		}
		catch (Exception e)
		{
			e.printStackTrace();
			/* Optional - create a 'normal' CloseableHttpClient if creation of one with ssl capabilities fail.
			 * For example:
			 * httpClient = HttpClients.createDefault();
			 */
			
			httpClient = HttpClients.createDefault();
		}
		
		CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
		
		return httpClient;
	}
	
	@Override
	public IBinder onBind(Intent intent)
	{
		return mBinder;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		// if the process is killed with no remaining start commands to deliver,
		// then the service will be  restarted with a null intent object.
		return START_STICKY;
	}
	
	public class EWServiceBinder extends Binder
	{
		/**
		 * @return bound service.
		 */
		public EWService getService()
		{
			return EWService.this;
		}
	}
	
	public boolean isNetworkAvailable()
	{
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		
		if (isWifiNetworkAvailable() || isMobileNetworkAvailable())
		{
			return true;
		}
		
		NetworkInfo activeNetwork = null;
		
		if (cm != null)
		{
			activeNetwork = cm.getActiveNetworkInfo();
		}
		
		return activeNetwork != null && activeNetwork.isConnected();
	}
	
	public boolean isMobileNetworkAvailable()
	{
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		
		NetworkInfo mobileNetwork = null;
		
		if (cm != null)
		{
			mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		}
		
		return mobileNetwork != null && mobileNetwork.isConnected();
	}
	
	public boolean isWifiNetworkAvailable()
	{
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		
		NetworkInfo wifiNetwork = null;
		
		if (cm != null)
		{
			wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		}
		
		return wifiNetwork != null && wifiNetwork.isConnected();
	}
	
	public void addRunnableToThreadExecuter(Runnable iRunnable)
	{
		try
		{
			mThreadExecutor.execute(iRunnable);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}