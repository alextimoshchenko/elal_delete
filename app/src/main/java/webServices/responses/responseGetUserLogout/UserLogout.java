package webServices.responses.responseGetUserLogout;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with care by Alexey.T on 16/07/2017.
 * <p>
 * TODO: Add a class header comment!
 */
public class UserLogout
{
	@JsonProperty("Success")
	private boolean mSuccess;
	
	@JsonProperty("UserID")
	private int mUserID;
	
	public UserLogout()
	{
	}
	
	public boolean isSuccess()
	{
		return mSuccess;
	}
	
	public int getUserID()
	{
		return mUserID;
	}
}
