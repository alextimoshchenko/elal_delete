package global;

import ui.activities.MainActivity;
import ui.activities.MyFlightsActivity;
import ui.activities.NotificationActivity;
import ui.activities.SearchFlightActivity;


/**
 * Created by Avishay.Peretz on 14/09/2017.
 */

public enum eActivityModule
{
	//Main module / Login module
	Main(1, MainActivity.class),
	
	//MyFlights module
	MyFlights(2, MyFlightsActivity.class),
	
	//Search module
	SearchFlight(3, SearchFlightActivity.class),
	
	//Notifications module
	Notifications(4,NotificationActivity.class);
	
	
	private final int mActivityModuleId;
	private final Class mNavigationActivityClass;
	
	
	eActivityModule(final int iActivityModuleId, final Class iNavigationActivityClass)
	{
		mActivityModuleId = iActivityModuleId;
		mNavigationActivityClass = iNavigationActivityClass;
	}
	
	public int getActivityModuleId()
	{
		return mActivityModuleId;
	}
	
	public Class getNavigationActivityClass()
	{
		return mNavigationActivityClass;
	}
	
	
	public static Class getNavigationActivityClassByIdOrMainDefault(int iNavigationId)
	{
		
		if (iNavigationId == Main.getActivityModuleId())
		{
			return Main.getNavigationActivityClass();
		}
		else if (iNavigationId == MyFlights.getActivityModuleId())
		{
			return MyFlights.getNavigationActivityClass();
		}
		else if (iNavigationId == SearchFlight.getActivityModuleId())
		{
			return SearchFlight.getNavigationActivityClass();
		}
		else if (iNavigationId == Notifications.getActivityModuleId())
		{
			return Notifications.getNavigationActivityClass();
		}
		else
		{
			return Main.getNavigationActivityClass();
		}
	}
	
	
	//
	//
	//	// This method works but before use it we need to solve navigation problems, because we need to separate all to moduls , now for swipe we in some cases we are using fragents and in some cases
	//	// activities, but we need to in all cases use activities
	//	public static int getClassIdByClass(Class iClass)
	//	{
	//		int result = -1;
	//
	//		if (iClass != null)
	//		{
	//			String currentClassName = iClass.getSimpleName();
	//			eInAppNavigation[] navigations = values();
	//
	//			for (eInAppNavigation tmpNav : navigations)
	//			{
	//				Class tpmClass = tmpNav.getNavigationFragmentClass();
	//
	//				if (tpmClass != null)
	//				{
	//					String className = tmpNav.getNavigationFragmentClass().getSimpleName();
	//
	//					if (className.equals(currentClassName))
	//					{
	//						result = tmpNav.getNavigationId();
	//					}
	//				}
	//			}
	//		}
	//
	//		return result;
	//	}
}
