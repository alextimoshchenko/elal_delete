package ui.customWidgets;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;


/**
 * Created by Avishay.Peretz on 07/06/2017.
 */

public class TypewriterTextView extends AppCompatTextView
{
	private CharSequence mText;
	private int mIndex;
	private long mDelay = 50; //Default 50ms delay
	
	
	public TypewriterTextView(Context context) {
		super(context);
	}
	
	public TypewriterTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	private Handler mHandler = new Handler();
	private Runnable characterAdder = new Runnable() {
		@Override
		public void run() {
			setText(mText.subSequence(0, mIndex++));
			if(mIndex <= mText.length()) {
				mHandler.postDelayed(characterAdder, mDelay);
			}
		}
	};
	
	public void animateText(CharSequence text) {
		mText = text;
		mIndex = 0;
		
		setText("");
		mHandler.removeCallbacks(characterAdder);
		mHandler.postDelayed(characterAdder, mDelay);
	}
	
	public void setCharacterDelay(long millis) {
		mDelay = millis;
	}
}
