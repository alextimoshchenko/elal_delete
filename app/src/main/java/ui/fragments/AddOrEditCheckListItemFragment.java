package ui.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.tagmanager.DataLayer;

import java.util.Date;

import global.IntentExtras;
import global.UserData;
import il.co.ewave.elal.R;
import ui.activities.EWBaseActivity;
import ui.activities.MyFlightsActivity;
import utils.errors.ErrorsHandler;
import utils.global.AppUtils;
import utils.global.DateTimeUtils;
import utils.global.googleAnalitics.googleTagManager.TagManagerImplementation;
import utils.managers.TabsBackStackManager;
import webServices.controllers.MainController;
import webServices.requests.RequestSetUserCheckList;
import webServices.requests.RequestUpdateUserCheckList;
import webServices.responses.ResponseSetOrUpdateUserCheckList;
import webServices.responses.UserCheckList.CheckListItem;
import webServices.responses.responseGetUserActivePNRs.PnrFlight;
import webServices.responses.responseGetUserActivePNRs.UserActivePnr;

/**
 * Created with care by Shahar Ben-Moshe on 26/02/17.
 */

public class AddOrEditCheckListItemFragment extends EWBaseFragment implements Response.Listener<ResponseSetOrUpdateUserCheckList>, Response.ErrorListener
{
	private static final int DEFAULT_TIME_DIMENSION = R.id.rb_addOrEditCheckListItem_ToDoNow;
	
	protected RadioGroup mRgTimeDimension;
	protected EditText mEtTaskDescription;
	protected Button mBtnSave;
	private ImageView mIvClose;
	private TextView mTxvAddOrEditTitle;
	private String mPnr;
	private UserActivePnr mUserActivePnr;
	private CheckListItem mCheckListItem;
	//	private IAddOrEditCheckListItemListener mAddOrEditCheckListItemListener;
	
	public static Bundle createBundleForFragment(String iPnr, UserActivePnr iUserActivePnr, CheckListItem iCheckListItem)
	{
		Bundle result = new Bundle();
		
		result.putParcelable(IntentExtras.USER_ACTIVE_PNR, iUserActivePnr);
		result.putString(IntentExtras.PNR, iPnr);
		result.putParcelable(IntentExtras.CHECK_LIST_ITEM, iCheckListItem);
		//		result.putSerializable(IntentExtras.ADD_OR_EDIT_CHECK_LIST_ITEM_LISTENER, iAddOrEditCheckListItemListener);
		
		return result;
	}
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_add_or_edit_check_list_item_new);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		getIntentExtras();
		initReference();
		setListeners();
		setData();
	}
	
	private void getIntentExtras()
	{
		if (getArguments() != null)
		{
			mPnr = getArguments().getString(IntentExtras.PNR);
			mUserActivePnr = getArguments().getParcelable(IntentExtras.USER_ACTIVE_PNR);
			mCheckListItem = getArguments().getParcelable(IntentExtras.CHECK_LIST_ITEM);
			//			mAddOrEditCheckListItemListener = (IAddOrEditCheckListItemListener) getArguments().getSerializable(IntentExtras.ADD_OR_EDIT_CHECK_LIST_ITEM_LISTENER);
		}
	}
	
	private void initReference()
	{
		mRgTimeDimension = getActivity().findViewById(R.id.rg_addOrEditCheckListItem_TimeDimension);
		mEtTaskDescription = getActivity().findViewById(R.id.et_addOrEditCheckListItem_TaskDescription);
		mTxvAddOrEditTitle = getActivity().findViewById(R.id.txv_add_or_edit_title);
		mBtnSave = getActivity().findViewById(R.id.btn_addOrEditCheckListItem_Save);
		mIvClose = getActivity().findViewById(R.id.iv_name_screen_back_arrow);
		
	}
	
	private void setListeners()
	{
		mBtnSave.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				onSaveClick();
			}
		});
		
		mIvClose.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				popFragment();
			}
		});
	}
	
	private void setData()
	{
		disableUnAvailableTimeDimensions(mRgTimeDimension);
		
		if (mCheckListItem != null)
		{
			mTxvAddOrEditTitle.setText(getResources().getString(R.string.edit_an_item));
			
			mEtTaskDescription.setText(mCheckListItem.getItemTitle());
			
			int idToCheck = DEFAULT_TIME_DIMENSION;
			
			switch (mCheckListItem.getTimeDimensionID())
			{
				case CheckListItem.TIME_DIMENSION_24_HOURS_BEFORE_FLIGHT:
					idToCheck = R.id.rb_addOrEditCheckListItem_DayBefore;
					break;
				case CheckListItem.TIME_DIMENSION_UNTIL_7_DAYS_BEFORE_FLIGHT:
					idToCheck = R.id.rb_addOrEditCheckListItem_ToDoNow;
					break;
				case CheckListItem.TIME_DIMENSION_FROM_7_DAYS_UNTIL_24_HOURS_BEFORE_FLIGHT:
					idToCheck = R.id.rb_addOrEditCheckListItem_WeekBefore;
					break;
				default:
					break;
			}
			mRgTimeDimension.check(idToCheck);
		}
		else
		{
			mRgTimeDimension.check(DEFAULT_TIME_DIMENSION);
		}
		
	}
	
	private void onSaveClick()
	{
		if (TextUtils.isEmpty(mEtTaskDescription.getText().toString()))
		{
			AppUtils.createSimpleMessageDialog(getActivity(), getString(R.string.please_fill_task), getString(R.string.confirm), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(final DialogInterface dialog, final int which)
				{
					// nothing...
				}
			}, null, null).show();
		}
		else if (isServiceConnected())
		{
			trySetProgressDialog(true);
			if (mCheckListItem == null)
			{
				getService().getController(MainController.class).setUserCheckList(new RequestSetUserCheckList(mPnr, UserData.getInstance().getUserID(), getSelectedTimeDimension(), mEtTaskDescription.getText().toString(), false, -1), this, this);
				
			}
			else
			{
				getService().getController(MainController.class).updateUserCheckList(new RequestUpdateUserCheckList(mCheckListItem.getItemID(), mEtTaskDescription.getText().toString(), mCheckListItem.isDone(), getSelectedTimeDimension(), mPnr, UserData.getInstance().getUserID()), this, this);
				
			}
		}
		
		//event 54
		String activePnr = UserData.getInstance().getActivePnr();
		((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight - Ready", "Action", TagManagerImplementation.TIME_SCOPE + activePnr + " >> Remember", "Label", "Add task > Add"));
	}
	
	private void popFragment()
	{
		if (getParentFragment() != null && getParentFragment() instanceof TabsBackStackManager.ITabsBackStackManagerHandler)
		{
			((TabsBackStackManager.ITabsBackStackManagerHandler) getParentFragment()).popFragment();
		}
	}
	
	private void disableUnAvailableTimeDimensions(RadioGroup mRgTimeDimension)
	{
		PnrFlight flight = mUserActivePnr.getCurrentFlight();
		
		if (flight != null)
		{
			long timeDifference = DateTimeUtils.getTimeDifferenceInHoursOrNull(new Date(), flight.getDepartureDate());
			if (timeDifference < DateTimeUtils.HOURS_OF_A_DAY)
			{
				mRgTimeDimension.getChildAt(mRgTimeDimension.getChildCount() - 3).setEnabled(false);
				mRgTimeDimension.getChildAt(mRgTimeDimension.getChildCount() - 2).setEnabled(false);
			}
			else if (timeDifference < DateTimeUtils.HOURS_OF_A_DAY * 7)
			{
				mRgTimeDimension.getChildAt(mRgTimeDimension.getChildCount() - 2).setEnabled(false);
			}
		}
	}
	
	private int getSelectedTimeDimension()
	{
		int result = 0;
		
		switch (mRgTimeDimension.getCheckedRadioButtonId())
		{
			case R.id.rb_addOrEditCheckListItem_ToDoNow:
				result = CheckListItem.TIME_DIMENSION_UNTIL_7_DAYS_BEFORE_FLIGHT;
				break;
			case R.id.rb_addOrEditCheckListItem_WeekBefore:
				result = CheckListItem.TIME_DIMENSION_FROM_7_DAYS_UNTIL_24_HOURS_BEFORE_FLIGHT;
				break;
			case R.id.rb_addOrEditCheckListItem_DayBefore:
				result = CheckListItem.TIME_DIMENSION_24_HOURS_BEFORE_FLIGHT;
				break;
			default:
				break;
		}
		
		return result;
	}
	
	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		if (getView() != null)
		{
			ViewGroup parent = (ViewGroup) getView().getParent();
			if (parent != null)
			{
				parent.removeAllViews();
			}
		}
	}
	
	@Override
	public void onResponse(final ResponseSetOrUpdateUserCheckList response)
	{
		trySetProgressDialog(false);
		
		if (getActivity() != null && getActivity() instanceof MyFlightsActivity)
		{
			((MyFlightsActivity) getActivity()).onSuccessfulAdd();
		}
		
		if (getParentFragment() != null && getParentFragment() instanceof ToDoFragment)
		{
			((ToDoFragment) getParentFragment()).onSuccessfulAdd();
		}
		
		popFragment();
	}
	
	@Override
	public void onErrorResponse(final VolleyError error)
	{
		ErrorsHandler.tryShowServiceErrorDialog(error, getActivity());
	}
	
}
