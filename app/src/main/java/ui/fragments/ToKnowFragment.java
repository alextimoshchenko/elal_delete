package ui.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.google.android.gms.tagmanager.DataLayer;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import global.UserData;
import il.co.ewave.elal.R;
import interfaces.IAppContentListener;
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import ui.activities.EWBaseActivity;
import ui.adapters.AppContentAdapter;
import ui.customWidgets.RoundedNetworkImageView;
import utils.global.googleAnalitics.googleTagManager.TagManagerImplementation;
import webServices.global.JacksonRequest;
import webServices.responses.getAppData.AppContent;

import static global.IntentExtras.APP_CONTENTS;

/**
 * Created with care by Shahar Ben-Moshe on 31/01/17.
 */

public class ToKnowFragment extends EWBaseFragment implements IAppContentListener
{
	private RecyclerView mRvList;
	private ArrayList<AppContent> mAppContent;
	private AppContentAdapter mAppContentAdapter;
	
	public static Bundle createBundleForFragment(ArrayList<AppContent> iAppContent)
	{
		Bundle result = new Bundle();
		
		result.putParcelableArrayList(APP_CONTENTS, iAppContent);
		
		return result;
	}
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_to_know_layout);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		getExtras();
		initReference();
		setAdapter();
	}
	
	private void getExtras()
	{
		if (getArguments() != null)
		{
			mAppContent = getArguments().getParcelableArrayList(APP_CONTENTS);
		}
	}
	
	private void initReference()
	{
		mRvList = (RecyclerView) getActivity().findViewById(R.id.rv_toKnow_List);
	}
	
	private void setAdapter()
	{
		if (mAppContentAdapter == null)
		{
			try
			{
				mAppContent = removeIllegalVideoLinks(mAppContent);
				
				mAppContentAdapter = new AppContentAdapter(mAppContent, getService().getImageLoader(), this, getContext());
				mRvList.setLayoutManager(new LinearLayoutManager(getActivity()));
				mRvList.setAdapter(mAppContentAdapter);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			mAppContentAdapter.notifyDataSetChanged();
		}
	}
	
	private ArrayList<AppContent> removeIllegalVideoLinks(ArrayList<AppContent> iAppContent)
	{
		if (iAppContent != null && !iAppContent.isEmpty())
		{
			for (int i = 0 ; i < iAppContent.size() ; i++)
			{
				AppContent content = iAppContent.get(i);
				switch (content.getMediaTypeID())
				{
					case AppContent.MEDIA_TYPE_VIDEO:
						if (!(content.getLink() != null && content.getLink().contains("http")))
						{
							iAppContent.remove(i);
						}
						break;
				}
			}
		}
		return iAppContent;
	}
	
	@Override
	public void onButtonClick(final AppContent iCurrentAppContentItem)
	{
		onAppContentButtonClick(iCurrentAppContentItem);
		sendPushEvent(iCurrentAppContentItem);
	}
	
	@Override
	public void onLoadButtonImage(final String iImgUrl, final RoundedNetworkImageView iRoundedImageView, final FrameLayout iFlImageFrame)
	{
		
		Transformation transformation = new Transformation()
		{
			@Override
			public Bitmap transform(Bitmap source)
			{
				int targetWidth = iRoundedImageView.getWidth();
				
				if (targetWidth <= 0)
				{
					targetWidth = 320;
				}
				
				double aspectRatio = (double) source.getHeight() / (double) source.getWidth();
				int targetHeight = (int) (targetWidth * aspectRatio);
				
				if (targetHeight <= 0)
				{
					targetHeight = 320;
				}
				
				Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
				
				
				if (result != source)
				{
					// Same bitmap is returned if sizes are the same
					source.recycle();
				}
				return result;
			}
			
			@Override
			public String key()
			{
				return "transformation" + " desiredWidth";
			}
		};
		
		if (iImgUrl.contains("http") || iImgUrl.contains("https"))
		{
			OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor()
			{
				@Override
				public Response intercept(Chain chain) throws IOException
				{
					Request newRequest = chain.request().newBuilder().addHeader(JacksonRequest.USER_AGENT, JacksonRequest.USER_AGENT_CONTENT).build();
					return chain.proceed(newRequest);
				}
			}).build();
			
			Picasso picasso = new Picasso.Builder(getContext()).downloader(new OkHttp3Downloader(client)).build();
			
			try
			{
				picasso.load(iImgUrl)
				       .error(android.R.drawable.stat_notify_error)
				       .transform(transformation)
				       .transform(new RoundedCornersTransformation(60, 0))
				       .memoryPolicy(MemoryPolicy.NO_CACHE)
				       .into(iRoundedImageView, new Callback()
				       {
					       @Override
					       public void onSuccess()
					       {
						       iFlImageFrame.setVisibility(View.VISIBLE);
						       iRoundedImageView.setVisibility(View.VISIBLE);
					       }
					
					       @Override
					       public void onError()
					       {
						       Log.e("picasso", "error loading " + iImgUrl);
						       iFlImageFrame.setVisibility(View.GONE);
					       }
				       });
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			
		}
		else
		{
			Picasso.with(getContext()).load(new File(iImgUrl)).resize(600, 320).centerInside().error(R.mipmap.elal_logo).into(iRoundedImageView);
			iRoundedImageView.setVisibility(View.VISIBLE);
		}
	}
	
	private void sendPushEvent(final AppContent iCurrentAppContentItem)
	{
		String subject = iCurrentAppContentItem.getTitle();
		
		if (iCurrentAppContentItem.getMediaTypeID() == AppContent.MEDIA_TYPE_VIDEO)
		{
			subject = "video";
		}
		
		//event 47
		String activePnr = UserData.getInstance().getActivePnr();
		((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight" + TagManagerImplementation.TIME_SCOPE, "Action", TagManagerImplementation.TIME_SCOPE + activePnr + " >> Know", "Label", subject));
	}
	
	private void onAppContentButtonClick(final AppContent iCurrentAppContentItem)
	{
		if (iCurrentAppContentItem != null)
		{
			if (iCurrentAppContentItem.isInApp())
			{
				tryGoToScreenById(iCurrentAppContentItem.getLinkInt(), null);
			}
			else if (iCurrentAppContentItem.getLink() != null)
			{
				tryGoToWebFragment(iCurrentAppContentItem.getLink());
			}
		}
	}
}
