package webServices.responses.responseGetUserActivePNRs;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.bignerdranch.expandablerecyclerview.model.Parent;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

import global.ElAlApplication;
import global.UserData;
import global.eLanguage;
import il.co.ewave.elal.R;
import utils.global.AppUtils;
import utils.global.DateTimeUtils;
import utils.global.dbObjects.Document;
import webServices.global.ePnrFlightType;
import webServices.requests.RequestGetUserCheckList;
import webServices.requests.RequestGetWeather;

import static utils.global.AppUtils.eFlightStatus.AfterBackToOrigination;
import static utils.global.AppUtils.eFlightStatus.DepartedNotArrivedToDestination;
import static utils.global.AppUtils.eFlightStatus.LessThan48HoursToDeparture;
import static utils.global.AppUtils.eFlightStatus.LessThanDayToDeparture;
import static utils.global.AppUtils.eFlightStatus.LessThanDayToOrigination;
import static utils.global.AppUtils.eFlightStatus.MoreThan48HoursToDeparture;
import static utils.global.AppUtils.eFlightStatus.MoreThanDayToOrigination;
import static utils.global.AppUtils.eFlightStatus.NoToOrigination;

public class UserActivePnr implements Parent<Document.eDocumentType>, Parcelable
{
	public static final Creator<UserActivePnr> CREATOR = new Creator<UserActivePnr>()
	{
		@Override
		public UserActivePnr createFromParcel(Parcel source)
		{
			return new UserActivePnr(source);
		}
		
		@Override
		public UserActivePnr[] newArray(int size)
		{
			return new UserActivePnr[size];
		}
	};
	
	@JsonProperty("DeparturePNRFlights")
	private ArrayList<PnrFlight> mDeparturePNRFlights = new ArrayList<>();
	@JsonProperty("ReturnPNRFlights")
	private ArrayList<PnrFlight> mReturnPNRFlights = new ArrayList<>();
	@JsonProperty("PNRCheckins")
	private ArrayList<PnrCheckin> mPNRCheckins = new ArrayList<>();
	@JsonProperty("PNRSeats")
	private ArrayList<PnrSeat> mPNRSeats = new ArrayList<>();
	@JsonProperty("PNRTickets")
	private ArrayList<PnrTicket> mPNRTickets = new ArrayList<>();
	@JsonProperty("PNRMeals")
	private ArrayList<PnrMeals> mPNRMeals = new ArrayList<>();
	@JsonProperty("PNR")
	private String mPnr;
	@JsonProperty("LastName")
	private String mLastName;
	@JsonProperty("CurrencyCode")
	private String mCurrencyCode;
	@JsonProperty("TotalFarePaid")
	private String mTotalFarePaid;
	@JsonProperty("IsGroupPnr")
	private boolean mIsGroupPnr;
	@JsonProperty("TicketingDate")
	private Date mTicketingDate;
	@JsonProperty("CreateDate")
	private Date mCreateDate;
	@JsonProperty("LastModified")
	private String mLastModified;
	@JsonProperty("IsActive")
	private String mIsActive;
	@JsonProperty("NumOfStops")
	private int mNumOfStops;
	@JsonProperty("IsMultiple")
	private boolean mIsMultiple;
	@JsonProperty("PNRDestination")
	private String mPNRDestination;
	@JsonProperty("PNROrigination")
	private String mPNROrigination;
	
	
	@JsonIgnore
	private ArrayList<Passenger> mPassengers;
	@JsonIgnore
	private ArrayList<Document.eDocumentType> mDocumentTypes = new ArrayList<>(Arrays.asList(Document.eDocumentType.values()));
	private HashMap<String, PnrTicket> mDeparturePNRTicketsMap, mReturnPNRTicketsMap;
	
	public UserActivePnr()
	{
	}
	
	protected UserActivePnr(Parcel in)
	{
		this.mDeparturePNRFlights = in.createTypedArrayList(PnrFlight.CREATOR);
		this.mReturnPNRFlights = in.createTypedArrayList(PnrFlight.CREATOR);
		this.mPNRCheckins = in.createTypedArrayList(PnrCheckin.CREATOR);
		this.mPNRSeats = in.createTypedArrayList(PnrSeat.CREATOR);
		this.mPNRTickets = in.createTypedArrayList(PnrTicket.CREATOR);
		this.mPNRMeals = in.createTypedArrayList(PnrMeals.CREATOR);
		this.mPnr = in.readString();
		this.mLastName = in.readString();
		this.mCurrencyCode = in.readString();
		this.mTotalFarePaid = in.readString();
		this.mIsGroupPnr = in.readByte() != 0;
		long tmpMTicketingDate = in.readLong();
		this.mTicketingDate = tmpMTicketingDate == -1 ? null : new Date(tmpMTicketingDate);
		long tmpMCreateDate = in.readLong();
		this.mCreateDate = tmpMCreateDate == -1 ? null : new Date(tmpMCreateDate);
		this.mLastModified = in.readString();
		this.mIsActive = in.readString();
		this.mPassengers = new ArrayList<Passenger>();
		in.readList(this.mPassengers, Passenger.class.getClassLoader());
		this.mDocumentTypes = new ArrayList<Document.eDocumentType>();
		in.readList(this.mDocumentTypes, Document.eDocumentType.class.getClassLoader());
		this.mPNRDestination = in.readString();
		this.mPNROrigination = in.readString();
	}
	
	//TEST DELETE IT
	public UserActivePnr setTestData(String iOriginationCityName, String iDestinationCityName, Date iDepartureDate, Date iArrivalDate)
	{
		mDeparturePNRFlights.add(new PnrFlight().setTestData(iOriginationCityName, iDestinationCityName, iDepartureDate, iArrivalDate));
		
		return this;
	}
	
	/**
	 * avishay 25/06/17
	 * return the target flight
	 *
	 * @return
	 */
	public PnrFlight getLastDeparturePNRFlightOrNull()
	{
		PnrFlight result = null;
		
		if (getDeparturePNRFlights() != null && !getDeparturePNRFlights().isEmpty())
		{
			int position = getDeparturePNRFlights().size() - 1;
			result = getDeparturePNRFlights().get(position);
		}
		
		return result;
	}
	
	/**
	 * avishay 06/07/17
	 * return the target return flight
	 *
	 * @return
	 */
	public PnrFlight getLastReturnPNRFlightOrNull()
	{
		PnrFlight result = null;
		
		if (getReturnPNRFlights() != null && !getReturnPNRFlights().isEmpty())
		{
			int position = getReturnPNRFlights().size() - 1;
			result = getReturnPNRFlights().get(position);
		}
		return result;
	}
	
	public ArrayList<PnrCheckin> getPNRCheckins()
	{
		return mPNRCheckins;
	}
	
	public String getIsActive()
	{
		return mIsActive;
	}
	
	public ArrayList<PnrSeat> getPNRSeats()
	{
		return mPNRSeats;
	}
	
	public boolean isGroupPnr()
	{
		return mIsGroupPnr;
	}
	
	public Date getCreateDate()
	{
		return mCreateDate;
	}
	
	public String getCurrencyCode()
	{
		return mCurrencyCode;
	}
	
	public String getLastModified()
	{
		return mLastModified;
	}
	
	public Date getTicketingDate()
	{
		return mTicketingDate;
	}
	
	public String getPnr()
	{
		return mPnr;
	}
	
	public String getmPNRDestination()
	{
		return mPNRDestination;
	}
	
	public String getmPNROrigination()
	{
		return mPNROrigination;
	}
	
	public ArrayList<PnrTicket> getPNRTickets()
	{
		return mPNRTickets;
	}
	
	public HashMap<String, PnrTicket> getDeparturePNRTicketsMap()
	{
		if (mDeparturePNRTicketsMap == null || mDeparturePNRTicketsMap.isEmpty())
		{
			mDeparturePNRTicketsMap = new HashMap<>();
			if (mDeparturePNRFlights != null && !mDeparturePNRFlights.isEmpty())
			{
				for (int i = 0 ; i < mDeparturePNRFlights.size() ; i++)
				{
					for (int j = 0 ; j < mPNRTickets.size() ; j++)
					{
						if (mDeparturePNRFlights.get(i).getFlightNumber().equalsIgnoreCase(mPNRTickets.get(j).getFlightNumber()))
						{
							mDeparturePNRTicketsMap.put(mPNRTickets.get(j).getFlightNumber(), mPNRTickets.get(j));
							break;
						}
					}
				}
			}
		}
		return mDeparturePNRTicketsMap;
	}
	
	public HashMap<String, PnrTicket> getReturnPNRTicketsMap()
	{
		if (mReturnPNRTicketsMap == null || mReturnPNRTicketsMap.isEmpty())
		{
			mReturnPNRTicketsMap = new HashMap<>();
			if (mReturnPNRFlights != null && !mReturnPNRFlights.isEmpty())
			{
				for (int i = 0 ; i < mReturnPNRFlights.size() ; i++)
				{
					for (int j = 0 ; j < mPNRTickets.size() ; j++)
					{
						if (mReturnPNRFlights.get(i).getFlightNumber().equalsIgnoreCase(mPNRTickets.get(j).getFlightNumber()))
						{
							mReturnPNRTicketsMap.put(mPNRTickets.get(j).getFlightNumber(), mPNRTickets.get(j));
							break;
						}
					}
				}
			}
		}
		return mReturnPNRTicketsMap;
	}
	
	public String getLastName()
	{
		return mLastName;
	}
	
	public ArrayList<Passenger> getPassengers()
	{
		if (mPassengers == null)
		{
			mPassengers = new ArrayList<>();
			
			
			if (mPNRTickets != null)
			{
				for (PnrTicket pnrTicket : mPNRTickets)
				{
					String meal = /*null*/ getMealByTicketNumberAndFlightNumber(pnrTicket.getTicketNumber(), pnrTicket.getFlightNumber());
					//					String flightNum = null;
					//					for (PnrMeals pnrMeal : mPNRMeals)
					//					{
					//						if (pnrMeal != null && !TextUtils.isEmpty(pnrMeal.getTicketNumber()) && pnrMeal.getTicketNumber().equals(pnrTicket.getTicketNumber()))
					//						{
					//							meal = pnrMeal.getSSRDescript();
					//							flightNum = pnrTicket.getFlightNumber();
					//						}
					//					}
					
					String flightNum = pnrTicket.getFlightNumber();
					
					String seat = getSeatByTicketNumberAndFlightNumber(pnrTicket.getTicketNumber(), pnrTicket.getFlightNumber());
					mPassengers.add(new Passenger(pnrTicket.getTicketNumber(), pnrTicket.getTitle(), pnrTicket.getFirstName(), pnrTicket.getLastName(), seat, meal, flightNum));
				}
			}
		}
		
		return mPassengers;
	}
	
	
	public ArrayList<Passenger> getPassengersByFlightNum(String iFlightNumber)
	{
		mPassengers = new ArrayList<>();
		
		if (mPNRTickets != null)
		{
			for (PnrTicket pnrTicket : mPNRTickets)
			{
				if (pnrTicket.getFlightNumber().equals(iFlightNumber))
				{
					String meal = /*null*/getMealByTicketNumberAndFlightNumber(pnrTicket.getTicketNumber(), pnrTicket.getFlightNumber());
					//					String flightNum = pnrTicket.getFlightNumber();
					//					for (PnrMeals pnrMeal : mPNRMeals)
					//					{
					//						if (pnrMeal != null && !TextUtils.isEmpty(pnrMeal.getTicketNumber()) && pnrMeal.getTicketNumber().equals(pnrTicket.getTicketNumber())
					//								&& pnrMeal.getFlightNumber().equals(mFlightNumber))
					//						{
					//							meal = pnrMeal.getSSRDescript();
					//						}
					//					}
					
					
					String seat = getSeatByTicketNumberAndFlightNumber(pnrTicket.getTicketNumber(), pnrTicket.getFlightNumber());
					mPassengers.add(new Passenger(pnrTicket.getTicketNumber(), pnrTicket.getTitle(), pnrTicket.getFirstName(), pnrTicket.getLastName(), seat, meal, iFlightNumber));
				}
			}
		}
		
		return mPassengers;
	}
	
	private String getMealByTicketNumberAndFlightNumber(final String iTicketNumber, final String iFlightNumber)
	{
		String meal = null;
		for (PnrMeals pnrMeal : mPNRMeals)
		{
			if (pnrMeal != null && !TextUtils.isEmpty(pnrMeal.getTicketNumber()) && pnrMeal.getTicketNumber()
			                                                                               .equals(iTicketNumber) && !TextUtils.isEmpty(pnrMeal.getFlightNumber()) && pnrMeal.getFlightNumber()
			                                                                                                                                                                 .equals(iFlightNumber))
			{
				meal = pnrMeal.getSSRDescript();
				//				flightNum = pnrTicket.getFlightNumber();
			}
		}
		return meal;
	}
	
	
	private String getSeatByTicketNumberAndFlightNumber(final String iTicketNumber, final String iFlightNumber)
	{
		String seat = null;
		for (PnrSeat pnrSeat : mPNRSeats)
		{
			if (pnrSeat != null && !TextUtils.isEmpty(pnrSeat.getTicketNumber()) && pnrSeat.getTicketNumber()
			                                                                               .equals(iTicketNumber) && !TextUtils.isEmpty(pnrSeat.getFlightNumber()) && pnrSeat.getFlightNumber()
			                                                                                                                                                                 .equals(iFlightNumber))
			{
				seat = pnrSeat.getSeatNumber();
				//				flightNum = pnrTicket.getFlightNumber();
			}
		}
		return seat;
	}
	
	public RequestGetUserCheckList generateUserCheckListRequest()
	{
		RequestGetUserCheckList result = null;
		
		if (getCurrentItemFlightAtPositionOrNull(0, ePnrFlightType.DEPARTURE) != null)
		{
			String flightNum = getCurrentFlightNum();
			PnrFlight currentFlight = getCurrentFlightLocal();
			if (currentFlight != null)
			{
				flightNum = getCurrentFlightLocal().getFlightNumber();
			}
			
			result = new RequestGetUserCheckList(UserData.getInstance().getUserID(), mPnr, flightNum);
			//			result = new RequestGetUserCheckList(UserData.getInstance().getUserID(), mPnr, getCurrentItemFlightAtPositionOrNull(0, ePnrFlightType.DEPARTURE).getFlightNumber());
		}
		
		return result;
	}
	
	public PnrFlight getCurrentItemFlightAtPositionOrNull(final int iPosition, @NonNull final ePnrFlightType iFlightType)
	{
		PnrFlight result = null;
		
		if (iFlightType == ePnrFlightType.DEPARTURE)
		{
			result = getCurrentItemDepartureFlightAtPositionOrNull(iPosition);
		}
		else if (iFlightType == ePnrFlightType.RETURN)
		{
			result = getCurrentItemReturnFlightAtPositionOrNull(iPosition);
		}
		
		return result;
	}
	
	public PnrFlight getCurrentItemDepartureFlightAtPositionOrNull(final int iPosition)
	{
		PnrFlight result = null;
		
		if (getDeparturePNRFlights() != null && !getDeparturePNRFlights().isEmpty() && iPosition >= 0 && iPosition < getDeparturePNRFlights().size())
		{
			result = getDeparturePNRFlights().get(iPosition);
		}
		
		return result;
	}
	
	public PnrFlight getCurrentItemReturnFlightAtPositionOrNull(final int iPosition)
	{
		PnrFlight result = null;
		
		if (getReturnPNRFlights() != null && !getReturnPNRFlights().isEmpty() && iPosition >= 0 && iPosition < getReturnPNRFlights().size())
		{
			result = getReturnPNRFlights().get(iPosition);
		}
		
		return result;
	}
	
	public ArrayList<PnrFlight> getDeparturePNRFlights()
	{
		return mDeparturePNRFlights;
	}
	
	public ArrayList<PnrFlight> getReturnPNRFlights()
	{
		return mReturnPNRFlights;
	}
	
	public RequestGetWeather generateWeatherRequest()
	{
		RequestGetWeather result = null;
		PnrFlight currentFlight = getCurrentFlight();
		if (currentFlight != null)
		{
			result = new RequestGetWeather(UserData.getInstance().getUserID(), currentFlight.getDestination());
		}
		
		return result;
	}
	
	public PnrFlight getCurrentFlight()
	{
		for (PnrFlight flight : mDeparturePNRFlights)
		{
			if (DateTimeUtils.dateHasPassed(flight.getDepartureDate()))
			{
				if (!DateTimeUtils.dateHasPassed(flight.getArrivalDate()))
				{
					flight.seteNumDirection(ePnrFlightType.DEPARTURE);
					return flight;
				}
			}
			else
			{
				flight.seteNumDirection(ePnrFlightType.DEPARTURE);
				return flight;
			}
		}
		
		Date today = new Date();
//		today = DateTimeUtils.convertDateToDateIgnoreGMT(today);
		
		for (PnrFlight flight : mReturnPNRFlights)
		{
			if (DateTimeUtils.dateHasPassed(flight.getDepartureDate()))
			{
				if (!DateTimeUtils.dateHasPassed(flight.getArrivalDate()))
				{
					flight.seteNumDirection(ePnrFlightType.RETURN);
					return flight;
				}
			}
			else if (DateTimeUtils.getTimeDifferenceInHoursOrNull(today, flight.getDepartureDate()) < DateTimeUtils.HOURS_OF_A_DAY)
			{
				flight.seteNumDirection(ePnrFlightType.RETURN);
				return flight;
			}
		}
		
		
		if (mReturnPNRFlights != null && mReturnPNRFlights.size() > 0)
		{
			if (mDeparturePNRFlights != null && !mDeparturePNRFlights.isEmpty() && DateTimeUtils.getTimeDifferenceInHoursOrNull(today, mReturnPNRFlights.get(0)
			                                                                                                                                            .getDepartureDate()) > DateTimeUtils.HOURS_OF_A_DAY)
			{
				mDeparturePNRFlights.get(0).seteNumDirection(ePnrFlightType.DEPARTURE);
				return mDeparturePNRFlights.get(0);
			}
			else
			{
				mReturnPNRFlights.get(0).seteNumDirection(ePnrFlightType.RETURN);
				return mReturnPNRFlights.get(0);
			}
		}
		else if (mDeparturePNRFlights != null && mDeparturePNRFlights.size() > 0)
		{
			mDeparturePNRFlights.get(0).seteNumDirection(ePnrFlightType.DEPARTURE);
			return mDeparturePNRFlights.get(0);
		}
		else
		{
			return null;
		}
	}
	
	/**
	 * avishay 25/06/17
	 * this method is solution just for today
	 */
	public PnrFlight getCurrentItemFlightAtPositionDepartureOrReurnOrNull(final int iPosition, @NonNull final ePnrFlightType iFlightType)
	{
		PnrFlight result = null;
		
		if (iFlightType == ePnrFlightType.DEPARTURE)
		{
			result = getCurrentItemDepartureFlightAtPositionOrNull(iPosition);
			if (result == null)
			{
				result = getCurrentItemReturnFlightAtPositionOrNull(iPosition);
			}
		}
		else if (iFlightType == ePnrFlightType.RETURN)
		{
			result = getCurrentItemReturnFlightAtPositionOrNull(iPosition);
		}
		
		
		return result;
	}
	
	@Override
	public ArrayList<Document.eDocumentType> getChildList()
	{
		return mDocumentTypes;
	}
	
	@Override
	public boolean isInitiallyExpanded()
	{
		return false;
	}
	
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeTypedList(this.mDeparturePNRFlights);
		dest.writeTypedList(this.mReturnPNRFlights);
		dest.writeTypedList(this.mPNRCheckins);
		dest.writeTypedList(this.mPNRSeats);
		dest.writeTypedList(this.mPNRTickets);
		dest.writeTypedList(this.mPNRMeals);
		dest.writeString(this.mPnr);
		dest.writeString(this.mLastName);
		dest.writeString(this.mCurrencyCode);
		dest.writeString(this.mTotalFarePaid);
		dest.writeByte(this.mIsGroupPnr ? (byte) 1 : (byte) 0);
		dest.writeLong(this.mTicketingDate != null ? this.mTicketingDate.getTime() : -1);
		dest.writeLong(this.mCreateDate != null ? this.mCreateDate.getTime() : -1);
		dest.writeString(this.mLastModified);
		dest.writeString(this.mIsActive);
		dest.writeList(this.mPassengers);
		dest.writeList(this.mDocumentTypes);
	}
	
	public String getCityNameByFlightNumber(String iFlightNumber)
	{
		PnrFlight flight = getFlightByNumber(iFlightNumber);
		if (flight != null)
		{
			return flight.getDestinationCityName();
		}
		
		return "";
	}
	
	public PnrFlight getFlightByNumber(String iFlightNumber)
	{
		for (PnrFlight flight : mDeparturePNRFlights)
		{
			if (flight.getFlightNumber().equals(iFlightNumber))
			{
				return flight;
			}
		}
		
		for (PnrFlight flight : mReturnPNRFlights)
		{
			if (flight.getFlightNumber().equals(iFlightNumber))
			{
				return flight;
			}
		}
		
		return null;
	}
	
	public String getOriginationCityNameByFlightNumber(String iFlightNumber)
	{
		PnrFlight flight = getFlightByNumber(iFlightNumber);
		if (flight != null && flight.getFlightNumber().equals(iFlightNumber))
		{
			return flight.getOriginationCityName();
		}
		
		return "";
	}
	
	public String getDestinationImageByFlightNumber(String iFlightNumber)
	{
		PnrFlight flight = getFlightByNumber(iFlightNumber);
		if (flight != null && flight.getDestinationImage() != null && !TextUtils.isEmpty(flight.getDestinationImage()))
		{
			return flight.getDestinationImage();
		}
		
		return "";
	}
	
	public ePnrFlightType getFlightDirectionByFlightNum(String iFlightNumber)
	{
		//		Log.d("DirectionByFlightNum", "iFlightNumber: " + iFlightNumber);
		for (PnrFlight flight : mDeparturePNRFlights)
		{
			if (flight.getFlightNumber().equals(iFlightNumber))
			{
				return ePnrFlightType.DEPARTURE;
			}
		}
		
		for (PnrFlight flight : mReturnPNRFlights)
		{
			if (flight.getFlightNumber().equals(iFlightNumber))
			{
				return ePnrFlightType.RETURN;
			}
		}
		
		return ePnrFlightType.DEPARTURE;
	}
	
	public String getTimeTitleByFlightStatus()
	{
		String result;
		
		Context context = ElAlApplication.getInstance();
		AppUtils.eFlightStatus flightStatus = getFlightStatusByUserActivePnr(UserActivePnr.this);
		
		PnrFlight currentPnr = getCurrentItemFlightAtPositionOrNull(0, ePnrFlightType.DEPARTURE);
		PnrFlight lastDeparturePNR = getLastDeparturePNRFlightOrNull();
		String currentDestinationCityName = "";
		String lastDeparturePNRCityName = "";
		
		if (currentPnr != null)
		{
			currentDestinationCityName = currentPnr.getDestinationCityName();
		}
		
		if (lastDeparturePNR != null)
		{
			lastDeparturePNRCityName = lastDeparturePNR.getDestinationCityName();
		}
		
		eLanguage language = UserData.getInstance().getLanguage();
		
		switch (flightStatus)
		{
			case MoreThan48HoursToDeparture:
			{
				if (currentPnr != null && currentPnr.getDepartureDate() != null)
				{
					Integer timeDifferenceInDays = DateTimeUtils.getTimeDifferenceInDaysOrNull(new Date(), new Date(currentPnr.getDepartureDate().getTime()));
					
					if (timeDifferenceInDays == null)
					{
						result = "";
					}
					else
					{
						result = context.getString(R.string.day_to_go, String.valueOf(timeDifferenceInDays), currentDestinationCityName);
						
						if (timeDifferenceInDays > 1)
						{
							if (language.isEnglish())
							{
								result = context.getString(R.string.days_to_go, String.valueOf(timeDifferenceInDays), currentDestinationCityName);
							}
							else if (language.isHebrew() && timeDifferenceInDays <= 10)
							{
								result = context.getString(R.string.days_to_go, String.valueOf(timeDifferenceInDays), currentDestinationCityName);
							}
						}
					}
					
					//					if (!getIsMultiple())
					//					{
					//						//					if (language.isEnglish())
					//						//					{
					//						//						result = currentDestinationCityName + ", " + result;
					//						//					}
					//						//					else
					//						//					{
					//						//						result += " " + currentDestinationCityName;
					//						//					}
					//					}
				}
				else
				{
					result = "";
				}
				
				break;
			}
			case LessThan48HoursToDeparture:
			{
				result = context.getString(R.string.nearly_there) + " " + currentDestinationCityName;
				break;
			}
			case DepartedNotArrivedToDestination:
			{
			}
			case LessThanDayToDeparture:
			{
				result = context.getString(R.string.nearly_there) + " " + currentDestinationCityName;
				break;
			}
			case MoreThanDayToOrigination:
			{
				if (getIsMultiple())
				{
					result = context.getString(R.string.enjoy);
				}
				else
				{
					result = context.getString(R.string.enjoy_in, lastDeparturePNRCityName);
				}
				
				break;
			}
			case LessThanDayToOrigination:
			{
				result = context.getString(R.string.ready_fly_back);
				break;
			}
			case NoToOrigination:
			{
				result = context.getString(R.string.welcome_back);
				break;
			}
			case AfterBackToOrigination:
			{//Because if person have already came to last destination and he had no return flights(means one way ticket) so we need to show him enjoy
				if (getReturnPNRFlights() != null && getReturnPNRFlights().isEmpty())
				{
					result = context.getString(R.string.enjoy_in) + " " + lastDeparturePNRCityName;
				}
				else
				{
					result = context.getString(R.string.welcome_back);
				}
				
				break;
			}
			case Error:
			{
			}
			default:
			{
				result = "";
				break;
			}
		}
		
		return result;
	}
	
	private AppUtils.eFlightStatus getFlightStatusByUserActivePnr(final UserActivePnr iUserActivePnr)
	{
		AppUtils.eFlightStatus result = AppUtils.eFlightStatus.Error;
		
		if (iUserActivePnr != null)
		{
			PnrFlight firstFlight = iUserActivePnr.getCurrentItemFlightAtPositionOrNull(0, ePnrFlightType.DEPARTURE);
			//			PnrFlight lastFlight = iUserActivePnr.getLastDeparturePNRFlightOrNull();
			PnrFlight lastFlight = iUserActivePnr.getLastReturnPNRFlightOrNull();
			
			Date firstFlightDepartureDate = null;
			Date firstFlightArrivalDate = null;
			
			Date lastFlightDepartureDate = null;
			Date lastFlightArrivalDate = null;
			
			if (firstFlight != null)
			{
				firstFlightDepartureDate = firstFlight.getDepartureDate();
				firstFlightArrivalDate = firstFlight.getArrivalDate();
			}
			
			if (lastFlight != null)
			{
				lastFlightDepartureDate = lastFlight.getDepartureDate();
				lastFlightArrivalDate = lastFlight.getArrivalDate();
			}
			
			if (firstFlightDepartureDate != null)
			{
				Date currentDate = new Date();
				//				currentDate = DateTimeUtils.convertDateToDateIgnoreGMT(currentDate);
				
				//				firstFlightDepartureDate = DateTimeUtils.convertDateToDateIgnoreGMT(firstFlightDepartureDate);
				
				Integer firstFlightDepartureDateTimeDifferenceInHours = DateTimeUtils.getTimeDifferenceInHoursOrNull(currentDate, firstFlightDepartureDate);
				
				if (firstFlightDepartureDateTimeDifferenceInHours != null)
				{
					if (firstFlightDepartureDateTimeDifferenceInHours > 0)
					{
						if (firstFlightDepartureDateTimeDifferenceInHours <= DateTimeUtils.HOURS_OF_A_DAY)
						{
							result = LessThanDayToDeparture;
						}
						else if (firstFlightDepartureDateTimeDifferenceInHours <= DateTimeUtils.HOURS_OF_A_DAY * 2)
						{
							result = LessThan48HoursToDeparture;
						}
						else
						{
							result = MoreThan48HoursToDeparture;
						}
					}
					else
					{
						Integer firstFlightArrivalDateTimeDifferenceInHours = DateTimeUtils.getTimeDifferenceInHoursOrNull(currentDate, firstFlightArrivalDate);
						
						if (firstFlightArrivalDateTimeDifferenceInHours != null && firstFlightArrivalDateTimeDifferenceInHours > 0)
						{
							result = DepartedNotArrivedToDestination;
						}
						else if (lastFlightDepartureDate != null)
						{
							Integer lastFlightDepartureDateTimeDifferenceInHours = DateTimeUtils.getTimeDifferenceInHoursOrNull(currentDate, lastFlightDepartureDate);
							
							if (lastFlightDepartureDateTimeDifferenceInHours != null)
							{
								if (lastFlightDepartureDateTimeDifferenceInHours >= DateTimeUtils.HOURS_OF_A_DAY)
								{
									result = MoreThanDayToOrigination;
								}
								else if (lastFlightArrivalDate != null)
								{
									Integer lastFlightArrivalDateTimeDifferenceInHours = DateTimeUtils.getTimeDifferenceInHoursOrNull(currentDate, lastFlightArrivalDate);
									
									if (lastFlightArrivalDateTimeDifferenceInHours != null)
									{
										if (lastFlightArrivalDateTimeDifferenceInHours < DateTimeUtils.HOURS_OF_A_DAY && lastFlightArrivalDateTimeDifferenceInHours > 0)
										{
											result = LessThanDayToOrigination;
										}
										else
										{
											result = AfterBackToOrigination;
										}
									}
								}
								
							}
						}
						else
						{
							result = NoToOrigination;
						}
					}
					
				}
			}
		}
		
		return result;
	}
	
	public String getOriginDestination()
	{
		PnrFlight pnrFlight = getFirstPnr();
		String originDestination = pnrFlight.getOrigination();
		return TextUtils.isEmpty(originDestination) ? "" : originDestination;
	}
	
	private PnrFlight getFirstPnr()
	{
		PnrFlight pnrFlight = new PnrFlight();
		ArrayList<PnrFlight> departurePnrFlights = getDeparturePNRFlights();
		
		if (departurePnrFlights != null && !departurePnrFlights.isEmpty())
		{
			pnrFlight = departurePnrFlights.get(0);
		}
		
		return pnrFlight;
	}
	
	public String getDepartureDestination()
	{
		PnrFlight pnrFlight = getSecondPnr();
		String departureDestination = pnrFlight.getDestination();
		return TextUtils.isEmpty(departureDestination) ? "" : departureDestination;
	}
	
	private PnrFlight getSecondPnr()
	{
		PnrFlight pnrFlight = new PnrFlight();
		ArrayList<PnrFlight> departurePnrFlights = getDeparturePNRFlights();
		
		if (departurePnrFlights != null && !departurePnrFlights.isEmpty())
		{
			pnrFlight = departurePnrFlights.get(departurePnrFlights.size() - 1);
		}
		
		return pnrFlight;
	}
	
	public String getDepartureTime()
	{
		PnrFlight pnrFlight = getFirstPnr();
		return pnrFlight.getDepartureTime();
	}
	
	public String getArrivalTime()
	{
		PnrFlight pnrFlight = getSecondPnr();
		return pnrFlight.getArrivalTime();
	}
	
	public String getNumberOfStopsOrVia()
	{
		String result;
		
		if (getIsMultiple())
		{
			result = getStringNumberOfStops();
		}
		else
		{
			result = getViaDestination();
		}
		
		return result;
	}
	
	public boolean getIsMultiple()
	{
		return mIsMultiple;
	}
	
	private String getStringNumberOfStops()
	{
		String result = "";
		Context context = ElAlApplication.getInstance();
		
		int numberOfStops = getNumOfStops();
		
		if (numberOfStops == 1)
		{
			result = context.getString(R.string.number_of_stop, String.valueOf(numberOfStops));
		}
		else if (numberOfStops > 1)
		{
			result = context.getString(R.string.number_of_stops, String.valueOf(numberOfStops));
		}
		
		return result;
	}
	
	private String getViaDestination()
	{
		Context context = ElAlApplication.getInstance();
		
		String result = "";
		ArrayList<PnrFlight> departurePnrFlights = getDeparturePNRFlights();
		
		if (departurePnrFlights != null && !departurePnrFlights.isEmpty())
		{
			if (departurePnrFlights.size() == 2)
			{
				String cityName = departurePnrFlights.get(0).getDestinationCityName();
				result = context.getString(R.string.via_destination, cityName);
			}
		}
		
		return result;
	}
	
	public int getNumOfStops()
	{
		return mNumOfStops;
	}
	
	public String getCurrentFlightNum()
	{
		for (PnrFlight flight : mDeparturePNRFlights)
		{
			if (DateTimeUtils.dateHasPassed(flight.getDepartureDate()))
			{
				if (!DateTimeUtils.dateHasPassed(flight.getArrivalDate()))
				{
					return flight.getFlightNumber();
				}
			}
			else
			{
				return flight.getFlightNumber();
			}
		}
		
		Date today = new Date();
		today = DateTimeUtils.convertDateToDateIgnoreGMT(today);
		
		for (PnrFlight flight : mReturnPNRFlights)
		{
			if (DateTimeUtils.dateHasPassed(flight.getDepartureDate()))
			{
				if (!DateTimeUtils.dateHasPassed(flight.getArrivalDate()))
				{
					return flight.getFlightNumber();
				}
			}
			else if (DateTimeUtils.getTimeDifferenceInHoursOrNull(today, flight.getDepartureDate()) < DateTimeUtils.HOURS_OF_A_DAY)
			{
				return flight.getFlightNumber();
			}
		}
		
		if (mReturnPNRFlights != null && mReturnPNRFlights.size() > 0)
		{
			return mReturnPNRFlights.get(0).getFlightNumber();
		}
		else if (mDeparturePNRFlights != null && mDeparturePNRFlights.size() > 0)
		{
			return mDeparturePNRFlights.get(0).getFlightNumber();
		}
		else
		{
			return "";
		}
	}
	
	public PnrTicket getPNRTicketByFlightNum(String iLastName, String iFlightNumber)
	{
		for (PnrTicket ticket : mPNRTickets)
		{
			if (ticket.getFlightNumber().equals(iFlightNumber) && ticket.getLastName().toLowerCase().equals(iLastName.toLowerCase()))
			{
				return ticket;
			}
		}
		return null;
	}
	
	public String getCurrentFlightDestinationName()
	{
		PnrFlight currentFlight = getCurrentFlightLocal();
		
		if (currentFlight != null)
		{
			return currentFlight.getDestinationCityName();
		}
		else
		{
			return "";
		}
	}
	
	public String getCurrentFlightDestinationInfo()
	{
		PnrFlight currentFlight = getCurrentFlightLocal();
		
		if (currentFlight != null)
		{
			return currentFlight.getDestinationInfo();
		}
		else
		{
			return "";
		}
	}
	
	public String getCurrentFlightDestination()
	{
		PnrFlight currentFlight = getCurrentFlightLocal();
		
		if (currentFlight != null)
		{
			return currentFlight.getDestination();
		}
		else
		{
			return "";
		}
		
		//		if (mDeparturePNRFlights != null && !mDeparturePNRFlights.isEmpty())
		//		{
		//			if (mDeparturePNRFlights.size() > 1)
		//			{
		//				return mDeparturePNRFlights.get(1).getDestination();
		//			}
		//			else
		//			{
		//				return mDeparturePNRFlights.get(0).getDestination();
		//			}
		//		}
		//		else if (mReturnPNRFlights != null && !mReturnPNRFlights.isEmpty())
		//		{
		//			if (mReturnPNRFlights.size() > 1)
		//			{
		//				return mReturnPNRFlights.get(1).getDestination();
		//			}
		//			else
		//			{
		//				return mReturnPNRFlights.get(0).getDestination();
		//			}
		//		}
		//		else
		//		{
		//			return "";
		//		}
	}
	
	private PnrFlight getCurrentFlightLocal()
	{
		if (mDeparturePNRFlights != null && !mDeparturePNRFlights.isEmpty())
		{
			if (mDeparturePNRFlights.size() > 1)
			{
				return mDeparturePNRFlights.get(1);
			}
			else
			{
				return mDeparturePNRFlights.get(0);
			}
		}
		else if (mReturnPNRFlights != null && !mReturnPNRFlights.isEmpty())
		{
			if (mReturnPNRFlights.size() > 1)
			{
				return mReturnPNRFlights.get(1);
			}
			else
			{
				return mReturnPNRFlights.get(0);
			}
		}
		else
		{
			return null;
		}
	}
	
	public int getFlightsCount()
	{
		return mDeparturePNRFlights.size() + mReturnPNRFlights.size();
	}
	
	public String getErrorFlightNumber()
	{
		
		for (PnrFlight flight : mDeparturePNRFlights)
		{
			if (flight.isError())
			{
				return flight.getFlightNumber();
			}
		}
		
		for (PnrFlight flight : mReturnPNRFlights)
		{
			if (flight.isError())
			{
				return flight.getFlightNumber();
			}
		}
		
		return "";
	}
}
