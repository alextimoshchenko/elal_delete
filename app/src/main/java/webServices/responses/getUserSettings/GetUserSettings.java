package webServices.responses.getUserSettings;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetUserSettings
{
	@JsonProperty("PushFlightsInfo")
	private boolean mPushFlightsInfo;
	@JsonProperty("UserID")
	private int mUserID;
	@JsonProperty("RepresentationID")
	private int mRepresentationID;
	@JsonProperty("CountryCode")
	private String mCountryCode;
	@JsonProperty("IsGuest")
	private boolean mIsGuest;
	@JsonProperty("PushOffersInfo")
	private boolean mPushOffersInfo;
	@JsonProperty("LanguageID")
	private String mLanguageID;
	
	public boolean isPushFlightsInfo()
	{
		return mPushFlightsInfo;
	}
	
	public int getUserID()
	{
		return mUserID;
	}
	
	public int getRepresentationID()
	{
		return mRepresentationID;
	}
	
	public String getCountryCode()
	{
		return mCountryCode;
	}
	
	public boolean isGuest()
	{
		return mIsGuest;
	}
	
	public boolean isPushOffersInfo()
	{
		return mPushOffersInfo;
	}
	
	public String getLanguageID()
	{
		return mLanguageID;
	}
	
	public GetUserSettings()
	{
	}
}