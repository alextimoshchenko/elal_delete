package utils.global.searchFlightObjects;

import android.support.annotation.NonNull;
import android.util.Log;
import android.util.Pair;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import utils.errors.LocalError.eLocalError;
import utils.global.AppUtils;
import utils.global.enums.eEditAction;
import utils.global.enums.eTypesOfTrip;

/**
 * Created with care by Alexey.T on 13/11/2017.
 * <p>
 * TODO: Add a class header comment!
 */
class UserFamilyDelegate<T extends IUserFamily> extends ArrayList<T> implements Iterable<T>, Cloneable
{
	private static final String TAG = UserFamilyDelegate.class.getSimpleName();
	private List<T> mUserFamilyList;
	private List<T> mFamilyListWithAgeEdition;
	
	UserFamilyDelegate()
	{
		mUserFamilyList = new ArrayList<>();
		updateAgeFamilyList();
	}
	
	void updateAgeFamilyList()
	{
		mFamilyListWithAgeEdition = cloneList();
	}
	
	private List<T> cloneList()
	{
		List<T> clone = new ArrayList<>();
		
		for (T tmp : mUserFamilyList)
		{
			clone.add((T) tmp.cloneObj());
		}
		
		return clone;
	}
	
	/**
	 * This method will update selected for flight family member status
	 */
	void updateSelectedStatusInMainList()
	{
		for (T familyMemberWithAgeEdition : mFamilyListWithAgeEdition)
		{
			for (T mainFamilyMember : mUserFamilyList)
			{
				if (familyMemberWithAgeEdition.equals(mainFamilyMember))
				{
					mainFamilyMember.setSelectedStatusAccordingTo(familyMemberWithAgeEdition);
					break;
				}
			}
		}
	}
	
	void setUserFamilyList(List<T> iUserFamilyList)
	{
		if (iUserFamilyList != null)
		{
			mUserFamilyList = iUserFamilyList;
			updateAgeFamilyList();
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, eLocalError.SomeProblemWithArray.getErrorMessage());
		}
	}
	
	List<T> getFamilyListWithAgeEdition()
	{
		return mFamilyListWithAgeEdition;
	}
	
	@Override
	public boolean add(final T iT)
	{
		return mUserFamilyList.add(iT);
	}
	
	@Override
	public T get(final int index)
	{
		return mUserFamilyList.get(index);
	}
	
	@Override
	public boolean isEmpty()
	{
		return mUserFamilyList.size() == 0;
	}
	
	@Override
	public int size()
	{
		return mUserFamilyList.size();
	}
	
	@NonNull
	@Override
	public Iterator<T> iterator()
	{
		return new Iterator<T>()
		{
			private int currentIndex = 0;
			
			@Override
			public boolean hasNext()
			{
				return currentIndex < size() && mUserFamilyList.get(currentIndex) != null;
			}
			
			@Override
			public T next()
			{
				return mUserFamilyList.get(currentIndex++);
			}
			
			@Override
			public void remove()
			{
				throw new UnsupportedOperationException();
			}
		};
	}
	
	/**
	 * This method return copy of userFamilyList with needed age configuration. First date in pair is firstDate in selection, second date is lastDate in selection
	 */
	List<T> editFamilyList(boolean iIsShouldReset, eEditAction iEditAction, Pair<Date, Date> iDatePair, eTypesOfTrip iTypesOfTrip)
	{
		if (iIsShouldReset)
		{
			updateAgeFamilyList();
		}
		
		if (iEditAction.isAddAge())
		{
			for (T element : mFamilyListWithAgeEdition)
			{
				Date currentFirstDateInSelection = iDatePair.first != null ? iDatePair.first : new Date();
				Date currentLastDateInSelection = iDatePair.second != null ? iDatePair.second : new Date();
				
				if (iTypesOfTrip.isOneWayTicket())
				{
					element.addAge(currentFirstDateInSelection);
				}
				else if (iTypesOfTrip.isRoundTrip())
				{
					element.addAge(currentLastDateInSelection);
				}
				else
				{
					AppUtils.printLog(Log.ERROR, TAG, eLocalError.NoSuchItems.getErrorMessage());
				}
			}
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, eLocalError.NoSuchItems.getErrorMessage());
		}
		
		return mFamilyListWithAgeEdition;
	}
}
