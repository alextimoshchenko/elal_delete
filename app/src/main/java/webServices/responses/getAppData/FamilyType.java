package webServices.responses.getAppData;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FamilyType
{
	@JsonProperty("FamilyTypeDescHeb")
	private String mFamilyTypeDescHeb;
	@JsonProperty("FamilyTypeDescEng")
	private String mFamilyTypeDescEng;
	@JsonProperty("FamilyTypeDesc")
	private String mFamilyTypeDesc;
	@JsonProperty("FamilyTypeID")
	private int mFamilyTypeID;
	
	public String getFamilyTypeDescHeb()
	{
		return this.mFamilyTypeDescHeb;
	}
	
	public String getFamilyTypeDescEng()
	{
		return this.mFamilyTypeDescEng;
	}
	
	public String getFamilyTypeDesc()
	{
		return this.mFamilyTypeDesc;
	}
	
	public int getFamilyTypeID()
	{
		return this.mFamilyTypeID;
	}
	
	public FamilyType()
	{
	}
	
	public FamilyType(final String iFamilyTypeDesc, final int iFamilyTypeID)
	{
		mFamilyTypeDesc = iFamilyTypeDesc;
		mFamilyTypeID = iFamilyTypeID;
	}
	
	@Override
	public String toString()
	{
		return mFamilyTypeDesc;
	}
}
