package webServices.responses.resposeMatmidLogin;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * Created with care by Shahar Ben-Moshe on 25/05/17.
 */

class FamilyMember
{
	@JsonProperty("DOB")
	private Date mDateOfBirth;
	@JsonProperty("FamilyTypeID")
	private int mFamilyTypeID;
	@JsonProperty("FirstNameEng")
	private String mFirstNameEng;
	@JsonProperty("IsActive")
	private Object mIsActive;
	@JsonProperty("LastNameEng")
	private String mLastNameEng;
	@JsonProperty("MatmidMemberID")
	private String mMatmidMemberID;
	@JsonProperty("Title")
	private String mTitle;
	@JsonProperty("UserID")
	private int mUserID;
	
	private FamilyMember()
	{
	}
}
