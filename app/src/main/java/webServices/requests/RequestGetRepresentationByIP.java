package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.UserData;

public class RequestGetRepresentationByIP
{
	@JsonProperty("LanguageCode")
	private String mLanguageCode = UserData.getInstance().getLanguage().getLanguageCode();
	@JsonProperty("IPAddress")
	private String mIpAddress;
	
	public RequestGetRepresentationByIP(String iIpAddress)
	{
		this.mIpAddress = iIpAddress;
	}
}
