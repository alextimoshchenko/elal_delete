package webServices.responses.responseGetUserActivePNRs;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class PnrTicket implements Parcelable
{
	
	@JsonProperty("PNR")
	private String mPNR;
	@JsonProperty("TicketNumber")
	private String mTicketNumber;
	@JsonProperty("FlightNumber")
	private String mFlightNumber;
	@JsonProperty("FirstName")
	private String mFirstName;
	@JsonProperty("LastName")
	private String mLastName;
	@JsonProperty("Title")
	private String mTitle;
	@JsonProperty("PassengerType")
	private String mPassengerType;
	@JsonProperty("SSRMeals")
	private String mSSRMeals;
	@JsonProperty("HasInfantTravelers")
	private boolean mHasInfantTravelers;
	@JsonProperty("ReferenceId")
	private String mReferenceId;
	@JsonProperty("HasChildTravelers")
	private boolean mHasChildTravelers;
	@JsonProperty("HasCheckin")
	private boolean mHasCheckin;
	@JsonProperty("HasSeats")
	private boolean mHasSeats;
	@JsonProperty("HandBagAmount")
	private int mHandBagAmount;
	@JsonProperty("CategoryId")
	private int mCategoryId;
	@JsonProperty("SSRAccessibilities")
	private String mSSRAccessibilities;
	@JsonProperty("HasSSRs")
	private boolean mHasSSRs;
	@JsonProperty("ClassService")
	private String mClassService;
	@JsonProperty("HasHandBag")
	private boolean mHasHandBag;
	@JsonProperty("IsPrimary")
	private boolean mIsPrimary;
	@JsonProperty("CreateDate")
	private Date mCreateDate;
	@JsonProperty("LastModified")
	private String mLastModified;
	@JsonProperty("IsActive")
	private boolean mIsActive;
	@JsonProperty("SeatNumber")
	private String mSeatNumber;
	@JsonProperty("IsCheckinButtonValid")
	private boolean mIsCheckinButtonValid;
	
	 
	
	public PnrTicket()
	{
	}
	
	public String getPNR()
	{
		return mPNR;
	}
	
	public String getTicketNumber()
	{
		return mTicketNumber;
	}
	
	public String getFlightNumber()
	{
		return mFlightNumber;
	}
	
	public String getFirstName()
	{
		return mFirstName;
	}
	
	public String getLastName()
	{
		return mLastName;
	}
	
	public String getTitle()
	{
		return mTitle;
	}
	
	public String getPassengerType()
	{
		return mPassengerType;
	}
	
	public String getSSRMeals()
	{
		return mSSRMeals;
	}
	
	public boolean isHasInfantTravelers()
	{
		return mHasInfantTravelers;
	}
	
	public String getReferenceId()
	{
		return mReferenceId;
	}
	
	public boolean isHasChildTravelers()
	{
		return mHasChildTravelers;
	}
	
	public boolean isHasCheckin()
	{
		return mHasCheckin;
	}
	
	public boolean isHasSeats()
	{
		return mHasSeats;
	}
	
	public int getHandBagAmount()
	{
		return mHandBagAmount;
	}
	
	public int getCategoryId()
	{
		return mCategoryId;
	}
	
	public String getSSRAccessibilities()
	{
		return mSSRAccessibilities;
	}
	
	public boolean isHasSSRs()
	{
		return mHasSSRs;
	}
	
	public String getClassService()
	{
		return mClassService;
	}
	
	public boolean isHasHandBag()
	{
		return mHasHandBag;
	}
	
	public boolean isPrimary()
	{
		return mIsPrimary;
	}
	
	public Date getCreateDate()
	{
		return mCreateDate;
	}
	
	public String getLastModified()
	{
		return mLastModified;
	}
	
	public boolean isActive()
	{
		return mIsActive;
	}
	
	public String getSeatNumber()
	{
		return mSeatNumber;
	}
	
	public boolean isCheckinButtonValid()
	{
		return mIsCheckinButtonValid;
	}
	
	
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeString(this.mPNR);
		dest.writeString(this.mTicketNumber);
		dest.writeString(this.mFlightNumber);
		dest.writeString(this.mFirstName);
		dest.writeString(this.mLastName);
		dest.writeString(this.mTitle);
		dest.writeString(this.mPassengerType);
		dest.writeString(this.mSSRMeals);
		dest.writeByte(this.mHasInfantTravelers ? (byte) 1 : (byte) 0);
		dest.writeString(this.mReferenceId);
		dest.writeByte(this.mHasChildTravelers ? (byte) 1 : (byte) 0);
		dest.writeByte(this.mHasCheckin ? (byte) 1 : (byte) 0);
		dest.writeByte(this.mHasSeats ? (byte) 1 : (byte) 0);
		dest.writeInt(this.mHandBagAmount);
		dest.writeInt(this.mCategoryId);
		dest.writeString(this.mSSRAccessibilities);
		dest.writeByte(this.mHasSSRs ? (byte) 1 : (byte) 0);
		dest.writeString(this.mClassService);
		dest.writeByte(this.mHasHandBag ? (byte) 1 : (byte) 0);
		dest.writeByte(this.mIsPrimary ? (byte) 1 : (byte) 0);
		dest.writeLong(this.mCreateDate != null ? this.mCreateDate.getTime() : -1);
		dest.writeString(this.mLastModified);
		dest.writeByte(this.mIsActive ? (byte) 1 : (byte) 0);
		dest.writeString(this.mSeatNumber);
		dest.writeByte(this.mIsCheckinButtonValid ? (byte) 1 : (byte) 0);
	}
	
	protected PnrTicket(Parcel in)
	{
		this.mPNR = in.readString();
		this.mTicketNumber = in.readString();
		this.mFlightNumber = in.readString();
		this.mFirstName = in.readString();
		this.mLastName = in.readString();
		this.mTitle = in.readString();
		this.mPassengerType = in.readString();
		this.mSSRMeals = in.readString();
		this.mHasInfantTravelers = in.readByte() != 0;
		this.mReferenceId = in.readString();
		this.mHasChildTravelers = in.readByte() != 0;
		this.mHasCheckin = in.readByte() != 0;
		this.mHasSeats = in.readByte() != 0;
		this.mHandBagAmount = in.readInt();
		this.mCategoryId = in.readInt();
		this.mSSRAccessibilities = in.readString();
		this.mHasSSRs = in.readByte() != 0;
		this.mClassService = in.readString();
		this.mHasHandBag = in.readByte() != 0;
		this.mIsPrimary = in.readByte() != 0;
		long tmpMCreateDate = in.readLong();
		this.mCreateDate = tmpMCreateDate == -1 ? null : new Date(tmpMCreateDate);
		this.mLastModified = in.readString();
		this.mIsActive = in.readByte() != 0;
		this.mSeatNumber = in.readString();
		this.mIsCheckinButtonValid = in.readByte() != 0;
	}
	
	public static final Parcelable.Creator<PnrTicket> CREATOR = new Parcelable.Creator<PnrTicket>()
	{
		@Override
		public PnrTicket createFromParcel(Parcel source)
		{
			return new PnrTicket(source);
		}
		
		@Override
		public PnrTicket[] newArray(int size)
		{
			return new PnrTicket[size];
		}
	};
}
