package utils.managers;

import java.util.ArrayList;

import global.ElAlApplication;
import io.realm.DynamicRealm;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmConfiguration;
import io.realm.RealmMigration;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.RealmSchema;
import io.realm.internal.IOException;
import utils.global.dbObjects.Document;

/**
 * Created with care by Shahar Ben-Moshe on 22/01/17.
 */

public class RealmManager implements RealmMigration
{
	private static RealmManager mInstance;
	
	private Realm mRealm;
	
	public static RealmManager getInstance()
	{
		if (mInstance == null)
		{
			mInstance = new RealmManager();
		}
		
		return mInstance;
	}
	
	@Override
	public void migrate(final DynamicRealm realm, long oldVersion, final long newVersion)
	{
		RealmSchema schema = realm.getSchema();
		
		if (oldVersion == 0)
		{
			schema.get("Document").addField(Document.USER_ID, int.class);
			oldVersion++;
		}
	}
	
	public interface IRealmListener<T extends RealmObject>
	{
		void onSuccess(ArrayList<T> iResultObjects);
		
		void onFailed();
	}
	
	//region private methods
	
	private RealmManager()
	{
		Realm.init(ElAlApplication.getInstance());
		mRealm = Realm.getInstance(new RealmConfiguration.Builder().schemaVersion(1).migration(this).build());
	}
	
	private <T extends RealmObject> void trySetSuccessResult(IRealmListener<T> iRealmListener, ArrayList<T> iResultObjects)
	{
		if (iRealmListener != null)
		{
			iRealmListener.onSuccess(iResultObjects);
		}
	}
	
	private void trySetFailedResult(IRealmListener iRealmListener)
	{
		if (iRealmListener != null)
		{
			iRealmListener.onFailed();
		}
	}
	
	private <T extends RealmObject> ArrayList<T> convertRealmListToArrayList(RealmResults<T> iRealmResults)
	{
		ArrayList<T> result = null;
		
		if (iRealmResults != null)
		{
			result = new ArrayList<>();
			
			for (T realmResult : iRealmResults)
			{
				result.add(realmResult);
			}
		}
		
		return result;
	}
	
	//endregion
	
	//region document
	
	public void insertDocument(final Document iDocument, final IRealmListener iRealmListener)
	{
		mRealm.executeTransactionAsync(new Realm.Transaction()
		{
			@Override
			public void execute(final Realm realm)
			{
				if (iDocument != null)
				{
					realm.copyToRealm(iDocument);
				}
				else
				{
					throw new IOException();
				}
			}
		}, new Realm.Transaction.OnSuccess()
		{
			@Override
			public void onSuccess()
			{
				trySetSuccessResult(iRealmListener, null);
			}
		}, new Realm.Transaction.OnError()
		{
			@Override
			public void onError(final Throwable error)
			{
				trySetFailedResult(iRealmListener);
			}
		});
	}
	
	public void getDocumentsForCategory(final String iPnr, final int iUserId, final Document.eDocumentType iEDocumentType, final IRealmListener<Document>  iRealmListener)
	{
		if (iRealmListener != null)
		{
			mRealm.where(Document.class)
			      .equalTo(Document.PNR, iPnr)
				  .equalTo(Document.USER_ID, iUserId)
			      .equalTo(Document.TYPE, iEDocumentType.getDocumentTypeId())
			      .findAllAsync()
			      .addChangeListener(new RealmChangeListener<RealmResults<Document>>()
			      {
				      @Override
				      public void onChange(final RealmResults<Document> iDocuments)
				      {
					      iRealmListener.onSuccess(convertRealmListToArrayList(iDocuments));
				      }
			      });
		}
	}
	
	public long getDocumentCountForCategory(final String iPnr, final int iUserId, final Document.eDocumentType iCategory)
	{
		return mRealm.where(Document.class).equalTo(Document.PNR, iPnr).equalTo(Document.USER_ID, iUserId).equalTo(Document.TYPE, iCategory.getDocumentTypeId()).count();
	}
	
	public void deleteDocument(final Document iDocumentToDelete)
	{
		mRealm.executeTransaction(new Realm.Transaction()
		{
			@Override
			public void execute(final Realm realm)
			{
				if (iDocumentToDelete != null && iDocumentToDelete.isValid())
				{
					iDocumentToDelete.deleteFromRealm();
				}
			}
		});
	}
	
	//endregion
}
