package webServices.responses.searchFlight;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseSearchFlightContent
{
	@JsonProperty("Success")
	private boolean iSuccess;
	
	public boolean getSuccess()
	{
		return this.iSuccess;
	}
	
	public void setSuccess(boolean Success)
	{
		this.iSuccess = Success;
	}
}
