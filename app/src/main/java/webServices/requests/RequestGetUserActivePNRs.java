package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.UserData;

public class RequestGetUserActivePNRs
{
	@JsonProperty("UserID")
	private int mIUserID;
	
	@JsonProperty("LanguageCode")
	private String mLanguageCode = UserData.getInstance().getLanguage().getLanguageCode();
	
	public RequestGetUserActivePNRs(int iUserID)
	{
		this.mIUserID = iUserID;
	}
}
