package ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;

import java.util.ArrayList;
import java.util.Collections;

import il.co.ewave.elal.R;
import ui.customWidgets.NonSwappableViewPager;
import utils.global.AppUtils;

/**
 * Created with care by Shahar Ben-Moshe on 21/12/16.
 */

public class RegistrationMainTabsFragment extends EWBaseFragment
{
	private TabLayout mTabLayout;
	private NonSwappableViewPager mVpContainer;
	private SectionsPagerAdapter mSectionsPagerAdapter;
	private ArrayList<EWBaseFragment> mFragments;
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_registration_main_tabs_layout);
	}
	
	@Override
	public void onViewCreatedOnce(final View iView, @Nullable final Bundle iSavedInstanceState)
	{
		initReference(iView);
		initTabs();
	}
	
	private void initReference(final View iView)
	{
		mTabLayout = (TabLayout) iView.findViewById(R.id.tl_registrationMain_Tabs);
		mVpContainer = (NonSwappableViewPager) iView.findViewById(R.id.nsvp_registrationMain_Container);
	}
	
	private void initTabs()
	{
		mFragments = new ArrayList<>();
//		mFragments.add(new OldRegistrationFragment());
//		mFragments.add(new OldFamilyCardFragment());
		
		if (AppUtils.isDefaultLocaleRTL())
		{
			Collections.reverse(mFragments);
		}
		
		mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
		mVpContainer.setAdapter(mSectionsPagerAdapter);
		mTabLayout.setupWithViewPager(mVpContainer);
		
		if (AppUtils.isDefaultLocaleRTL() && mFragments.size() > 0)
		{
			TabLayout.Tab tab = mTabLayout.getTabAt(mFragments.size() - 1);
			
			if (tab != null)
			{
				tab.select();
			}
		}
	}
	
	private class SectionsPagerAdapter extends FragmentPagerAdapter
	{
		SectionsPagerAdapter(final FragmentManager fm)
		{
			super(fm);
		}
		
		@Override
		public Fragment getItem(final int iPosition)
		{
			Fragment result = null;
			
			if (mFragments != null && mFragments.size() > iPosition)
			{
				result = mFragments.get(iPosition);
			}
			
			return result;
		}
		
		@Override
		public int getCount()
		{
			return mFragments.size();
		}
		
		@Override
		public CharSequence getPageTitle(final int iPosition)
		{
			String result = null;
			
			if (mFragments != null && mFragments.size() > iPosition)
			{
				result = mFragments.get(iPosition).getFragmentTitle();
			}
			
			if (result == null)
			{
				result = "";
			}
			
			return result;
		}
	}
	
	public void switchToTabByFragment(final Class<? extends EWBaseFragment> iFragmentClass, Bundle iBundle)
	{
		if (iFragmentClass != null && mFragments != null)
		{
			for (int i = 0 ; i < mFragments.size() ; i++)
			{
				if (mFragments.get(i) != null && mFragments.get(i).getClass().getSimpleName().equals(iFragmentClass.getSimpleName()))
				{
					TabLayout.Tab tab = mTabLayout.getTabAt(i);
					if (tab != null)
					{
						if (mFragments.get(i).getArguments() == null)
						{
							mFragments.get(i).setArguments(iBundle);
						}
						else
						{
							mFragments.get(i).getArguments().putAll(iBundle);
						}
						
						tab.select();
					}
					
					break;
				}
			}
		}
	}
}
