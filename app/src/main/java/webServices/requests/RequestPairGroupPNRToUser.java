package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.UserData;


/**
 * Created by Aviv.Kabeli on 15/12/2016.
 */

public class RequestPairGroupPNRToUser
{
	@JsonProperty("PNRNum")
	private String mPNRNum;
	@JsonProperty("UDID")
	private String mUdid = UserData.getInstance().getUdid();
	@JsonProperty("TicketNumber")
	private String mTicketNumber;
	
	public RequestPairGroupPNRToUser(String iPNRNum, String iTicketNumber)
	{
		this.mPNRNum = iPNRNum;
		this.mTicketNumber = iTicketNumber;
	}
}
