package webServices.responses.responseGetUserActivePNRs;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Avishay.Peretz on 22/06/2017.
 */
public class PnrMeals implements Parcelable
{
	
	@JsonProperty("PNR")
	private String mPnr;
	@JsonProperty("TicketNumber")
	private String mTicketNumber;
	@JsonProperty("FlightNumber")
	private String mFlightNumber;
	@JsonProperty("SSRCode")
	private String mCode;
	@JsonProperty("MealDescription")
	private String mSSRDescript;
	
	
	public PnrMeals()
	{
	}
	
	
	public String getPnr()
	{
		return mPnr;
	}
	
	public String getTicketNumber()
	{
		return mTicketNumber;
	}
	
	public String getFlightNumber()
	{
		return mFlightNumber;
	}
	
	public String getCode()
	{
		return mCode;
	}
	
	public String getSSRDescript()
	{
		return !TextUtils.isEmpty(mSSRDescript) ? mSSRDescript : "-";
	}
	
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeString(this.mPnr);
		dest.writeString(this.mTicketNumber);
		dest.writeString(this.mFlightNumber);
		dest.writeString(this.mCode);
		dest.writeString(this.mSSRDescript);
	}
	
	protected PnrMeals(Parcel in)
	{
		this.mPnr = in.readString();
		this.mTicketNumber = in.readString();
		this.mFlightNumber = in.readString();
		this.mCode = in.readString();
		this.mSSRDescript = in.readString();
	}
	
	public static final Parcelable.Creator<PnrMeals> CREATOR = new Parcelable.Creator<PnrMeals>()
	{
		@Override
		public PnrMeals createFromParcel(Parcel source)
		{
			return new PnrMeals(source);
		}
		
		@Override
		public PnrMeals[] newArray(int size)
		{
			return new PnrMeals[size];
		}
	};
}
