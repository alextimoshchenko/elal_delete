package webServices.responses.responseGetGroupPNRs;

import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

import utils.global.dbObjects.Document;
import webServices.responses.responseGetUserActivePNRs.Passenger;
import webServices.responses.responseGetUserActivePNRs.PnrCheckin;
import webServices.responses.responseGetUserActivePNRs.PnrFlight;
import webServices.responses.responseGetUserActivePNRs.PnrMeals;
import webServices.responses.responseGetUserActivePNRs.PnrSeat;
import webServices.responses.responseGetUserActivePNRs.PnrTicket;
import webServices.responses.responseGetUserActivePNRs.UserActivePnr;

/**
 * Created by Avishay.Peretz on 28/08/2017.
 */

public class GroupPNR extends UserActivePnr
{
//	@JsonProperty("DeparturePNRFlights")
//	private ArrayList<PnrFlight> mDeparturePNRFlights = new ArrayList<>();
//	@JsonProperty("ReturnPNRFlights")
//	private ArrayList<PnrFlight> mReturnPNRFlights = new ArrayList<>();
//	@JsonProperty("PNRCheckins")
//	private ArrayList<PnrCheckin> mPNRCheckins = new ArrayList<>();
//	@JsonProperty("PNRSeats")
//	private ArrayList<PnrSeat> mPNRSeats = new ArrayList<>();
//	@JsonProperty("PNRTickets")
//	private ArrayList<PnrTicket> mPNRTickets = new ArrayList<>();
//	@JsonProperty("PNRMeals")
//	private ArrayList<PnrMeals> mPNRMeals = new ArrayList<>();
//	@JsonProperty("PNR")
//	private String mPnr;
//	@JsonProperty("LastName")
//	private String mLastName;
//	@JsonProperty("CurrencyCode")
//	private String mCurrencyCode;
//	@JsonProperty("TotalFarePaid")
//	private String mTotalFarePaid;
//	@JsonProperty("IsGroupPnr")
//	private boolean mIsGroupPnr;
//	@JsonProperty("TicketingDate")
//	private Date mTicketingDate;
//	@JsonProperty("CreateDate")
//	private Date mCreateDate;
//	@JsonProperty("LastModified")
//	private String mLastModified;
//	@JsonProperty("IsActive")
//	private String mIsActive;
//	@JsonProperty("NumOfStops")
//	private int mNumOfStops;
//	@JsonProperty("IsMultiple")
//	private boolean mIsMultiple;
	
	public GroupPNR()
	{
	}
}
