package webServices.global;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;

import java.util.HashMap;
import java.util.Map;

import il.co.ewave.elal.BuildConfig;
import utils.HashUtilsManager;
import utils.errors.ServerError;
import utils.global.AppUtils;

public class JacksonRequest<T> extends JsonRequest<T>
{
	private static final String TAG = JacksonRequest.class.getSimpleName();
	private static final String CONTENT = "Content-Type";
	public static final String JSON = "application/json";
	public static final String USER_AGENT = "User-Agent";
	public static final String USER_AGENT_CONTENT = "ElalMobile/2.0 (Android) Ewave/1.0 (http://www.ewave.co.il)";
	static final String EWAVE_HASH = "ewavehash";
	
	private static final int REQUEST_TIMEOUT = 40 * 1000;
	private static final int REQUEST_MAX_RETRIES = 0;
	
	private Class<T> responseType;
	private HashMap<String, String> mHeaders;
	
	/**
	 * Creates a new request.
	 *
	 * @param method        the HTTP method to use
	 * @param url           URL to fetch the JSON from
	 * @param requestData   A {@link Object} to post and convert into json as the request. Null is allowed and indicates no parameters will be posted along with request.
	 * @param listener      Listener to receive the JSON response
	 * @param errorListener Error listener, or null to ignore errors.
	 */
	public JacksonRequest(int method, String url, Object requestData, Class<T> responseType, Response.Listener<T> listener, Response.ErrorListener errorListener)
	{
		super(method, url, (requestData == null) ? null : Mapper.string(requestData), listener, errorListener);
		this.responseType = responseType;
		
		if (BuildConfig.DEBUG)
		{
			AppUtils.printLog(Log.DEBUG, TAG, "RequestJsonString = " + ((requestData == null) ? "" : Mapper.prettifyString(Mapper.string(requestData))));
			AppUtils.printLog(Log.DEBUG, TAG, "RequestWsUrl = " + (getUrl() == null ? "" : getUrl()));
		}
		
		setRetryPolicy(new DefaultRetryPolicy(REQUEST_TIMEOUT, REQUEST_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
	}
	
	@Override
	protected Map<String, String> getParams() throws AuthFailureError
	{
		Map<String, String> map = new HashMap<>();
		map.put(CONTENT, JSON);
		map.put(USER_AGENT, USER_AGENT_CONTENT);
		
		return map;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected Response<T> parseNetworkResponse(NetworkResponse response)
	{
		try
		{
			String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
			
			if (BuildConfig.DEBUG)
			{
				AppUtils.printLog(Log.DEBUG, TAG, "ResponseJsonString = " + Mapper.prettifyString(jsonString));
				AppUtils.printLog(Log.DEBUG, TAG, "ResponseWsUrl = " + (getUrl() == null ? "" : getUrl()));
			}
			
			T responseObject = Mapper.objectOrThrow(jsonString, responseType);
			
			if (responseObject != null && responseObject instanceof webServices.global.Response)
			{
				if (((webServices.global.Response) responseObject).getErrorCode() != ServerError.eServerError.Ok.getErrorCode())
				{
					throw new ServerError((webServices.global.Response) responseObject);
				}
			}
			
			if (HashUtilsManager.getInstance().isHashService(responseType.getSimpleName()))
			{
				if (response.headers.get(JacksonRequest.EWAVE_HASH) != null)
				{
					if (!TextUtils.isEmpty(response.headers.get(JacksonRequest.EWAVE_HASH)))
					{
						if (!TextUtils.isEmpty(jsonString))
						{
							if (HashUtilsManager.getInstance().isAuthenticHash(jsonString, response.headers.get(JacksonRequest.EWAVE_HASH)))
							{
								//do nothing
								AppUtils.printLog(Log.DEBUG, TAG, "isAuthenticHash: **true**");
								return Response.success(responseObject, HttpHeaderParser.parseCacheHeaders(response));
							}
							else
							{
								throw new ServerError((webServices.global.Response) responseObject);
							}
						}
						else
						{
							throw new ServerError((webServices.global.Response) responseObject);
						}
					}
					else
					{
						throw new ServerError((webServices.global.Response) responseObject);
					}
				}
				else
				{
					throw new ServerError((webServices.global.Response) responseObject);
				}
			}
			else
			{
				return Response.success(responseObject, HttpHeaderParser.parseCacheHeaders(response));
			}
		}
		catch (Exception iE)
		{
			return Response.error(iE instanceof ServerError ? (ServerError) iE : new VolleyError(iE));
		}
	}
	
	@Override
	protected VolleyError parseNetworkError(VolleyError volleyError)
	{
		if (BuildConfig.DEBUG)
		{
			AppUtils.printLog(Log.DEBUG, TAG, "ResponseErrorCode = " + String.valueOf((volleyError != null && volleyError.networkResponse != null) ? volleyError.networkResponse.statusCode : -1));
			AppUtils.printLog(Log.DEBUG, TAG, "ResponseErrorMessage = " + ((volleyError != null && !TextUtils.isEmpty(volleyError.toString())) ? volleyError.toString() : ""));
			AppUtils.printLog(Log.DEBUG, TAG, "ResponseWsUrl = " + (getUrl() == null ? "" : getUrl()));
		}
		
		return super.parseNetworkError(volleyError);
	}
	
	@Override
	public final Map<String, String> getHeaders() /*throws AuthFailureError*/
	{
		if (mHeaders == null)
		{
			mHeaders = new HashMap<>();
		}
		
//		mHeaders.put(CONTENT, JSON);
		mHeaders.put(USER_AGENT, USER_AGENT_CONTENT);
		
		AppUtils.printLog(Log.DEBUG, TAG, "headers: " + mHeaders.toString());
		
		return mHeaders;
	}
	
	public void setHeaders(final HashMap<String, String> iHeaders)
	{
		mHeaders = iHeaders;
	}
	
	public static String getUserAgentContent()
	{
		if (!TextUtils.isEmpty(USER_AGENT_CONTENT))
		{
			return USER_AGENT_CONTENT;
		}
		else
		{
			return "";
		}
	}
	
	public void setSecretHashHeader(final byte[] iBody)
	{
		String hash = HashUtilsManager.getInstance().getSecretHashHeader(iBody);
		
		if (!TextUtils.isEmpty(hash))
		{
			getHeaders().put(JacksonRequest.EWAVE_HASH, hash);
		}
	}
}
