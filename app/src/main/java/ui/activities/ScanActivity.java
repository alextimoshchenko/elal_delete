package ui.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;

import global.GlobalVariables;
import global.IntentExtras;
import global.UserData;
import global.ocr.OcrIsraelDriversLicense;
import global.ocr.OcrPassport;
import il.co.ewave.elal.R;
import interfaces.IOcrResultObject;
import scanovate.ocr.common.OCRManager;
import scanovate.ocr.common.ScanListener;
import scanovate.ocr.israeldriverslicense.IsraelDriversLicenseOCRManager;
import scanovate.ocr.passport.PassportOCRManager;
import ui.fragments.EWBaseFragment;
import utils.errors.LocalError;
import utils.global.AppUtils;
import utils.global.googleAnalitics.googleTagManager.TagManagerImplementation.ePushEvents;

import static global.IntentExtras.OCR_TYPE;
import static utils.global.AppUtils.createSimpleMessageDialog;
import static utils.global.AppUtils.createSimpleMessageDialogWithXButton;

/**
 * Created by Avishay.Peretz on 19/09/2017.
 */

public class ScanActivity extends EWBaseActivity
{
	private static String TAG = ScanActivity.class.getSimpleName();
	public static final int DOCUMENT_SCAN_REQUEST_CODE = 23486;
	private static final double OCR_SCANNER_TIMEOUT = 40;
	
	public static void tryStartDocumentScanActivityForResult(@NonNull Activity iActivity, @Nullable EWBaseFragment iFragment, @GlobalVariables.OcrType int iOcrType)
	{
		Intent intent = new Intent(iActivity, ScanActivity.class);
		intent.putExtra(OCR_TYPE, iOcrType);
		
		try
		{
			if (iFragment != null)
			{
				iFragment.startActivityForResult(intent, DOCUMENT_SCAN_REQUEST_CODE);
			}
			else
			{
				iActivity.startActivityForResult(intent, DOCUMENT_SCAN_REQUEST_CODE);
			}
		}
		catch (Exception iE)
		{
			iE.printStackTrace();
		}
	}
	
	private ScanListener mScanListener;
	private OCRManager mOCRManager;
	private int mOcrType;
	private ImageView mBtnBack;
	private TextView mTvTitle;
	private TextView mTvinstructions;
	private FrameLayout mFlCameraPreview;
	
	@Override
	protected void initScreen()
	{
		setContentView(R.layout.activity_document_scan_new);
		
		getIntentExtras();
		initReference();
		setListeners();
		setGui();
		startScan();
	}
	
	private void setGui()
	{
		if (mOcrType == GlobalVariables.OCR_TYPE_PASSPORT)
		{
			mTvTitle.setText(R.string.passport_scan);
			//            mTvinstructions.setText(getString(R.string.document_scan_instructions, getString(R.string.passport)));
		}
		else if (mOcrType == GlobalVariables.OCR_TYPE_ISRAELI_DRIVING_LICENSE)
		{
			mTvTitle.setText(R.string.driving_license_scan);
			//            mTvinstructions.setText(getString(R.string.document_scan_instructions, getString(R.string.driving_license)));
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.GeneralError.getErrorMessage());
		}
		mTvinstructions.setText(Html.fromHtml(getString(R.string.instructions_underline2)));
	}
	
	private void getIntentExtras()
	{
		if (getIntent() != null && getIntent().getExtras() != null)
		{
			mOcrType = getIntent().getExtras().getInt(OCR_TYPE);
		}
	}
	
	private void initReference()
	{
		mBtnBack = (ImageView) findViewById(R.id.iv_name_screen_back_arrow);
		mTvTitle = (TextView) findViewById(R.id.tv_name_screen_title);
		mTvinstructions = (TextView) findViewById(R.id.tv_documentScan_Instructions);
		mFlCameraPreview = (FrameLayout) findViewById(R.id.fl_documentScan_CameraPreview);
	}
	
	private void setListeners()
	{
		
		mBtnBack.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onBackPressed();
			}
		});
		
		mTvinstructions.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onTvInstructionsClick();
			}
		});
	}
	
	private void onTvInstructionsClick()
	{
		tryPauseOcr();
		
		Dialog dialog = createSimpleMessageDialogWithXButton(this, getString(R.string.instructions), getString(R.string.document_scan_instructions, getString(R.string.passport)), null, null, null, null, null, null);
		dialog.setOnDismissListener(new DialogInterface.OnDismissListener()
		{
			@Override
			public void onDismiss(DialogInterface dialog)
			{
				startScan();
			}
		});
		
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
	}
	
	
	private void startScan()
	{
		//        new StartScanTask().execute();
		
		if (mOCRManager == null)
		{
			mScanListener = new ScanListener()
			{
				@Override
				public void ScanSucceeded(final HashMap<String, Object> resultValues)
				{
					onSuccess(resultValues);
				}
				
				@Override
				public void ScanCanceled(final HashMap<String, Object> resultValues)
				{
					onCancel();
				}
				
				@Override
				public void ScanFailed(final HashMap<String, Object> resultValues)
				{
					onFail();
				}
			};
			
			try
			{
				switch (mOcrType)
				{
					case GlobalVariables.OCR_TYPE_PASSPORT:
					default:
						mOCRManager = new PassportOCRManager();
						mOCRManager.setTimeoutInSeconds(OCR_SCANNER_TIMEOUT);
						((PassportOCRManager) mOCRManager).scanListener = mScanListener;
						((PassportOCRManager) mOCRManager).init(mFlCameraPreview, this);
						((PassportOCRManager) mOCRManager).startScan();
						break;
					case GlobalVariables.OCR_TYPE_ISRAELI_DRIVING_LICENSE:
						mOCRManager = new IsraelDriversLicenseOCRManager();
						mOCRManager.setTimeoutInSeconds(OCR_SCANNER_TIMEOUT);
						((PassportOCRManager) mOCRManager).scanListener = mScanListener;
						((IsraelDriversLicenseOCRManager) mOCRManager).init(mFlCameraPreview, this);
						((IsraelDriversLicenseOCRManager) mOCRManager).startScan();
						break;
				}
			}
			catch (Exception ignored)
			{
			}
		}
		else
		{
			try
			{
				if (mOCRManager instanceof PassportOCRManager)
				{
					((PassportOCRManager) mOCRManager).resetAndScan();
				}
				else if (mOCRManager instanceof IsraelDriversLicenseOCRManager)
				{
					((IsraelDriversLicenseOCRManager) mOCRManager).resetAndScan();
				}
			}
			catch (Exception ignored)
			{
			}
		}
	}
	
	private void onSuccess(final HashMap<String, Object> iResultValues)
	{
		tryFreeOcr();
		
		Dialog dialog = createSimpleMessageDialog(this, getString(R.string.passport_successful_scan_title), getString(R.string.passport_successful_scan_message), getString(R.string.approve), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, final int which)
			{
				dialog.dismiss();
				
				Intent intent = new Intent();
				intent.putExtra(IntentExtras.DOCUMENT_SCAN_RESULTS, createObjectByResultValues(iResultValues));
				finish(OCRManager.SCAN_STATUS.SCAN_STATUS_SUCCESS.getValue(), intent);
				
				if (UserData.getInstance().isUserGuest())
				{
					//event 20
					pushEvent(ePushEvents.SUCCEEDED_REGISTRATION_ADD_FAMILY_REGISTRATION);
				}
				else
				{
					//event 20
					pushEvent(ePushEvents.SUCCEEDED_REGISTRATION_ADD_FAMILY_ADD);
				}
				
			}
		}, null, null);
		
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
	}
	
	private IOcrResultObject createObjectByResultValues(final HashMap<String, Object> iResultValues)
	{
		IOcrResultObject result = null;
		
		if (iResultValues != null)
		{
			if (iResultValues.containsKey(PassportOCRManager.PASSPORT_SCAN_RESULT_STATUS))
			{
				result = new OcrPassport(iResultValues);
			}
			else if (iResultValues.containsKey(IsraelDriversLicenseOCRManager.ISRAEL_DRIVERS_LICENSE_SCAN_RESULT_STATUS))
			{
				result = new OcrIsraelDriversLicense(iResultValues);
			}
		}
		
		return result;
	}
	
	private void onCancel()
	{
		tryFreeOcr();
		
		finish(OCRManager.SCAN_STATUS.SCAN_STATUS_CANCEL.getValue());
	}
	
	private void onFail()
	{
		Dialog dialog = createSimpleMessageDialog(this, getString(R.string.failed_scan_message), "", getString(R.string.try_again), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, final int which)
			{
				startScan();
				
				if (UserData.getInstance().isUserGuest())
				{
					pushEvent(ePushEvents.TRY_AGAIN_REGISTRATION_ADD_FAMILY_REGISTRATION);
				}
				else
				{
					pushEvent(ePushEvents.TRY_AGAIN_REGISTRATION_ADD_FAMILY_ADD);
				}
			}
		}, getString(R.string.cancel), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				onCancel();
				
				if (UserData.getInstance().isUserGuest())
				{
					pushEvent(ePushEvents.CANCEL_PASSPORT_SCANNING_REGISTRATION);
				}
				else
				{
					pushEvent(ePushEvents.CANCEL_PASSPORT_SCANNING_ADD);
				}
			}
		});
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
	}
	
	@Override
	public void onBackPressed()
	{
		tryStopOcr();
		
		super.onBackPressed();
	}
	
	private void tryPauseOcr()
	{
		try
		{
			if (mOCRManager instanceof PassportOCRManager)
			{
				((PassportOCRManager) mOCRManager).pauseScan();
			}
			else if (mOCRManager instanceof IsraelDriversLicenseOCRManager)
			{
				((IsraelDriversLicenseOCRManager) mOCRManager).pauseScan();
			}
		}
		catch (Exception ignored)
		{
		}
	}
	
	private void tryStopOcr()
	{
		if (mOCRManager != null)
		{
			try
			{
				if (mOCRManager instanceof PassportOCRManager)
				{
					((PassportOCRManager) mOCRManager).cancelScan();
				}
				else if (mOCRManager instanceof IsraelDriversLicenseOCRManager)
				{
					((IsraelDriversLicenseOCRManager) mOCRManager).cancelScan();
				}
			}
			catch (Exception ignored)
			{
			}
		}
	}
	
	private void tryFreeOcr()
	{
		if (mOCRManager != null)
		{
			try
			{
				if (mOCRManager instanceof PassportOCRManager)
				{
					((PassportOCRManager) mOCRManager).free();
				}
				else if (mOCRManager instanceof IsraelDriversLicenseOCRManager)
				{
					((IsraelDriversLicenseOCRManager) mOCRManager).free();
				}
			}
			catch (Exception ignored)
			{
			}
		}
	}
	
	@Override
	protected void onServiceConnected()
	{
	}
	
	
	class StartScanTask extends AsyncTask
	{
		@Override
		protected void onPreExecute()
		{
			
			
			setProgressDialog(true);
			//            super.onPreExecute();
			
			if (mOCRManager == null)
			{
				mScanListener = new ScanListener()
				{
					@Override
					public void ScanSucceeded(final HashMap<String, Object> resultValues)
					{
						onSuccess(resultValues);
					}
					
					@Override
					public void ScanCanceled(final HashMap<String, Object> resultValues)
					{
						onCancel();
					}
					
					@Override
					public void ScanFailed(final HashMap<String, Object> resultValues)
					{
						onFail();
					}
				};
			}
		}
		
		@Override
		protected Object doInBackground(Object[] params)
		{
			
			if (mOCRManager == null)
			{
				//                mScanListener = new ScanListener() {
				//                    @Override
				//                    public void ScanSucceeded(final HashMap<String, Object> resultValues) {
				//                        onSuccess(resultValues);
				//                    }
				//
				//                    @Override
				//                    public void ScanCanceled(final HashMap<String, Object> resultValues) {
				//                        onCancel();
				//                    }
				//
				//                    @Override
				//                    public void ScanFailed(final HashMap<String, Object> resultValues) {
				//                        onFail();
				//                    }
				//                };
				
				try
				{
					switch (mOcrType)
					{
						case GlobalVariables.OCR_TYPE_PASSPORT:
						default:
							mOCRManager = new PassportOCRManager();
							mOCRManager.setTimeoutInSeconds(OCR_SCANNER_TIMEOUT);
							((IsraelDriversLicenseOCRManager) mOCRManager).scanListener = mScanListener;
							//                            ((PassportOCRManager) mOCRManager).init(mFlCameraPreview, DocumentScanActivity.this);
							//                            ((PassportOCRManager) mOCRManager).startScan();
							break;
						case GlobalVariables.OCR_TYPE_ISRAELI_DRIVING_LICENSE:
							mOCRManager = new IsraelDriversLicenseOCRManager();
							mOCRManager.setTimeoutInSeconds(OCR_SCANNER_TIMEOUT);
							((IsraelDriversLicenseOCRManager) mOCRManager).scanListener = mScanListener;
							//                            ((IsraelDriversLicenseOCRManager) mOCRManager).init(mFlCameraPreview, DocumentScanActivity.this);
							//                            ((IsraelDriversLicenseOCRManager) mOCRManager).startScan();
							break;
					}
				}
				catch (Exception ignored)
				{
				}
			}
			else
			{
				try
				{
					if (mOCRManager instanceof PassportOCRManager)
					{
						((PassportOCRManager) mOCRManager).resetAndScan();
					}
					else if (mOCRManager instanceof IsraelDriversLicenseOCRManager)
					{
						((IsraelDriversLicenseOCRManager) mOCRManager).resetAndScan();
					}
				}
				catch (Exception ignored)
				{
				}
			}
			
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Object o)
		{
			
			
			if (mOCRManager != null)
			{
				try
				{
					switch (mOcrType)
					{
						case GlobalVariables.OCR_TYPE_PASSPORT:
						default:
							//                            mOCRManager = new PassportOCRManager();
							//                            mOCRManager.setTimeoutInSeconds(OCR_SCANNER_TIMEOUT);
							//                            mOCRManager.scanListener = mScanListener;
							((PassportOCRManager) mOCRManager).init(mFlCameraPreview, ScanActivity.this);
							((PassportOCRManager) mOCRManager).startScan();
							break;
						case GlobalVariables.OCR_TYPE_ISRAELI_DRIVING_LICENSE:
							//                            mOCRManager = new IsraelDriversLicenseOCRManager();
							//                            mOCRManager.setTimeoutInSeconds(OCR_SCANNER_TIMEOUT);
							//                            mOCRManager.scanListener = mScanListener;
							((IsraelDriversLicenseOCRManager) mOCRManager).init(mFlCameraPreview, ScanActivity.this);
							((IsraelDriversLicenseOCRManager) mOCRManager).startScan();
							break;
					}
				}
				catch (Exception ignored)
				{
				}
			} /*else {
			    try {
                    if (mOCRManager instanceof PassportOCRManager) {
                        ((PassportOCRManager) mOCRManager).resetAndScan();
                    } else if (mOCRManager instanceof IsraelDriversLicenseOCRManager) {
                        ((IsraelDriversLicenseOCRManager) mOCRManager).resetAndScan();
                    }
                } catch (Exception ignored) {
                }
            }*/
			
			setProgressDialog(false);
			//            super.onPostExecute(o);
		}
	}
}
