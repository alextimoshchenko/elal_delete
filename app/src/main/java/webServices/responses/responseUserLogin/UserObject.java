package webServices.responses.responseUserLogin;

import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * Created with care by Shahar Ben-Moshe on 05/01/17.
 */
public class UserObject
{
	@JsonProperty("UserID")
	private int mUserID;
	@JsonProperty("PhoneNumber")
	private String mPhoneNumber;
	@JsonProperty("CountryCode")
	private String mCountryCode;
	@JsonProperty("Email")
	private String mEmail;
	@JsonProperty("UserPassword")
	private String mUserPassword;
	@JsonProperty("Title")
	private String mTitle;
	@JsonProperty("FirstNameHeb")
	private String mFirstNameHeb;
	@JsonProperty("LastNameHeb")
	private String mLastNameHeb;
	@JsonProperty("FirstNameEng")
	private String mFirstNameEng;
	@JsonProperty("LastNameEng")
	private String mLastNameEng;
	@JsonProperty("MiddleNameHeb")
	private String mMiddleNameHeb;
	@JsonProperty("MiddleNameEng")
	private String mMiddleNameEng;
	@JsonProperty("DOB")
	private Date mDateOfBirth;
	@JsonProperty("PassportNum")
	private String mPassportNum;
	@JsonProperty("PassportExpiration")
	private Date mPassportExpiration;
	@JsonProperty("MatmidMemberID")
	private String mMatmidMemberID;
	@JsonProperty("IDNumber")
	private String mIdNumber;
	@JsonProperty("WeddingDate")
	private Date mWeddingDate;
	@JsonProperty("LanguageID")
	private String mLanguageID;
	@JsonProperty("RepresentationID")
	private int mRepresentationID;
	@JsonProperty("ModifiedDate")
	private String mModifiedDate;
	@JsonProperty("CreatedDate")
	private String mCreatedDate;
	@JsonProperty("IsGuest")
	private boolean mIsGuest;
	@JsonProperty("PasswordGUID")
	private String mPasswordGUID;
	@JsonProperty("PushFlightsInfo")
	private boolean mPushFlightsInfo;
	@JsonProperty("PushOffersInfo")
	private boolean mPushOffersInfo;
	@JsonProperty("SessionID")
	private String mSessionID;
	@JsonProperty("MatmidPoints")
	private String mMatmidPoints;
	@JsonProperty("MatmidTypeID")
	private int mMatmidTypeID;
	@JsonProperty("IsMatmidFirstLogin")
	private boolean mIsMatmidFirstLogin;
	
	
	private boolean mIsMatmid;
	
	public UserObject()
	{
	}
	
	public UserObject(final int iUserID, final String iPhoneNumber, String iCountryCode, final String iEmail, final String iUserPassword, final String iTitle, final String iFirstNameHeb, final String iLastNameHeb,
	                  final String iFirstNameEng, final String iLastNameEng, final String iMiddleNameHeb, final String iMiddleNameEng, final Date iDateOfBirth, final String iPassportNum,
	                  final Date iPassportExpiration, final String iMatmidMemberID, final String iIdNumber, final Date iWeddingDate, final String iLanguageID, final int iRepresentationID,
	                  final String iModifiedDate, final String iCreatedDate, final boolean iIsGuest, final String iPasswordGUID, final boolean iPushFlightsInfo, final boolean iPushOffersInfo,
	                  final String iSessionID, final String iMatmidPoints, final int iMatmidTypeID, final boolean iIsMatmidFirstLogin)
	{
		mUserID = iUserID;
		mPhoneNumber = iPhoneNumber;
		mCountryCode = iCountryCode;
		mEmail = iEmail;
		mUserPassword = iUserPassword;
		mTitle = iTitle;
		mFirstNameHeb = iFirstNameHeb;
		mLastNameHeb = iLastNameHeb;
		mFirstNameEng = iFirstNameEng;
		mLastNameEng = iLastNameEng;
		mMiddleNameHeb = iMiddleNameHeb;
		mMiddleNameEng = iMiddleNameEng;
		mDateOfBirth = iDateOfBirth;
		mPassportNum = iPassportNum;
		mPassportExpiration = iPassportExpiration;
		mMatmidMemberID = iMatmidMemberID;
		mIdNumber = iIdNumber;
		mWeddingDate = iWeddingDate;
		mLanguageID = iLanguageID;
		mRepresentationID = iRepresentationID;
		mModifiedDate = iModifiedDate;
		mCreatedDate = iCreatedDate;
		mIsGuest = iIsGuest;
		mPasswordGUID = iPasswordGUID;
		mPushFlightsInfo = iPushFlightsInfo;
		mPushOffersInfo = iPushOffersInfo;
		mSessionID = iSessionID;
		mMatmidPoints = iMatmidPoints;
		mMatmidTypeID = iMatmidTypeID;
		mIsMatmidFirstLogin = iIsMatmidFirstLogin;
	}
	
	public int getUserID()
	{
		return mUserID;
	}
	
	public String getPhoneNumber()
	{
		return mPhoneNumber;
	}
	
	public String getCountryCode()
	{
		return mCountryCode;
	}
	
	public void setCountryCode(String iCountryCode)
	{
		this.mCountryCode = iCountryCode;
	}
	
	public String getEmail()
	{
		return mEmail;
	}
	
	public String getUserPassword()
	{
		return mUserPassword;
	}
	
	public String getTitle()
	{
		String title =  mTitle;
		if (!TextUtils.isEmpty(title)&& title.contains("."))
			return title.replace(".", "");
		return mTitle;
	}
	
	public String getFirstNameHeb()
	{
		return mFirstNameHeb;
	}
	
	public String getLastNameHeb()
	{
		return mLastNameHeb;
	}
	
	public String getFirstNameEng()
	{
		try{
			return  mFirstNameEng.substring(0, 1).toUpperCase() + mFirstNameEng.substring(1).toLowerCase();
		}
		catch (Exception ignore)
		{
			return mFirstNameEng;
		}
	}
	
	public String getLastNameEng()
	{
		return mLastNameEng;
	}
	
	public String getMiddleNameHeb()
	{
		return mMiddleNameHeb;
	}
	
	public String getMiddleNameEng()
	{
		return mMiddleNameEng;
	}
	
	public Date getDateOfBirth()
	{
		return mDateOfBirth;
	}
	
	public String getPassportNum()
	{
		return mPassportNum;
	}
	
	public Date getPassportExpiration()
	{
		return mPassportExpiration;
	}
	
	public String getMatmidMemberID()
	{
		return mMatmidMemberID;
	}
	
	public String getIdNumber()
	{
		return mIdNumber;
	}
	
	public Object getWeddingDate()
	{
		return mWeddingDate;
	}
	
	public String getLanguageID()
	{
		return mLanguageID;
	}
	
	public int getRepresentationID()
	{
		return mRepresentationID;
	}
	
	public String getModifiedDate()
	{
		return mModifiedDate;
	}
	
	public String getCreatedDate()
	{
		return mCreatedDate;
	}
	
	public boolean isGuest()
	{
		return mIsGuest;
	}
	
	public Object getPasswordGUID()
	{
		return mPasswordGUID;
	}
	
	public boolean isPushFlightsInfo()
	{
		return mPushFlightsInfo;
	}
	
	public boolean isPushOffersInfo()
	{
		return mPushOffersInfo;
	}
	
	public boolean isMatmid()
	{
		//		return !TextUtils.isEmpty(mMatmidMemberID);
		return mIsMatmid;
	}
	
	public void setIsMatmid(boolean iIsMatmid)
	{
		mIsMatmid = iIsMatmid;
	}
	
	public boolean isMatmidFirstLogin()
	{
		return mIsMatmidFirstLogin;
	}
	
	public boolean setIsMatmidFirstLogin(boolean iIsMatmidFirstLogin)
	{
		return mIsMatmidFirstLogin=iIsMatmidFirstLogin;
	}
	
	public String getSessionID()
	{
		return mSessionID;
	}
	
	public String getMatmidPoints()
	{
		return mMatmidPoints;
	}
	
	public int getMatmidTypeID()
	{
		return mMatmidTypeID;
	}
	
	public void setEmail(final String iEmail)
	{
		mEmail = iEmail;
	}
	
	public void setPhoneNumber(final String iPhoneNumber)
	{
		mPhoneNumber = iPhoneNumber;
	}
	
	public void setPassportNum(final String iPassportNum)
	{
		mPassportNum = iPassportNum;
	}
	
	public void setPassportExpiration(final Date iPassportExpiration)
	{
		mPassportExpiration = iPassportExpiration;
	}
	
	public boolean isUserPassedDetailScreen()
	{
		boolean result = false;
		
		if (!TextUtils.isEmpty(mFirstNameEng) && !TextUtils.isEmpty(mLastNameEng))
		{
			result = true;
		}
		
		return result;
	}
}
