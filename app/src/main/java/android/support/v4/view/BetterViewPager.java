package android.support.v4.view;

import android.content.Context;
import android.util.AttributeSet;

import ui.customWidgets.materialcalendarview.LocalUtilsCalendar;

/**
 * {@linkplain #setChildrenDrawingOrderEnabledCompat(boolean)} does some reflection that isn't needed.
 * And was making view creation time rather large. So lets override it and make it better!
 */
public class BetterViewPager extends ViewPager
{
	
	public BetterViewPager(Context context)
	{
		super(context);
		
		if (LocalUtilsCalendar.isRTL())
		{
			setRotationY(180);
		}
	}
	
	public BetterViewPager(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}
	

	//	@Override
	/**TODO Avishay 21/12/17:
	 * Alexey removed the override annotation.
	 * check it!
	 */
	public void setChildrenDrawingOrderEnabledCompat(boolean enable)
	{
		setChildrenDrawingOrderEnabled(enable);
	}
}
