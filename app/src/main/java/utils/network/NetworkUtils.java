package utils.network;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import org.jetbrains.annotations.Contract;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import global.ElAlApplication;
import global.GlobalVariables;
import global.UserData;
import global.eLanguage;
import utils.errors.LocalError;
import utils.global.AppUtils;
import utils.global.DateTimeUtils;
import utils.global.ElalPreferenceUtils;
import utils.global.LocaleUtils;
import utils.global.enums.eFamilyRelation;
import utils.global.enums.ePassengersType;
import utils.global.enums.eTabStatement;
import utils.global.enums.eTypesOfTrip;
import utils.global.searchFlightObjects.SearchObject;
import webServices.global.RequestStringBuilder;
import webServices.responses.getSearchGlobalData.GetSearchGlobalDataContentElalCities;
import webServices.responses.getSearchGlobalData.GetSearchGlobalDataContentUserFamily;
import webServices.responses.responseUserLogin.UserObject;

/**
 * Created with care by Alexey.T on 05/11/2017.
 * <p>
 * TODO: Add a class header comment!
 */
public class NetworkUtils
{
	private static final String TAG = NetworkUtils.class.getSimpleName();
	
	private final String LAST_MONTH = "12";
	private final String FIRST_MONTH = "0";
	private static final String ENC = "&ENC=";
	private static final String ENC_TIME = "&ENC_TIME=";
	private static final String UDID = "&UDID=";
	private static final String USER_ID = "&UserID=";
	private static final String HOME_PHONE = "&homePhone=";
	private static final String EMAIL = "&email=";
	private static final String TITLE = "&title";
	private static final String EQUAL_SING = "=";
	private static final String MEMBER_NUMBER = "&memberNumber";
	private static final String FIRST_NAME = "&firstName";
	private static final String LAST_NAME = "&lastName";
	private static final String DOB = "&dateOfBirth";
	private static final String URL_POINTS_AND_MONEY_TICKET = "https://booking.elal.co.il/newBooking/protectRedirect.do?ndis=true&site=elal";
	private static final String URL_FLIGHT_TICKET = "https://booking.elal.co.il/newBooking/urlDirector.do?ndis=false&hp=true";
	private static final String URL_AWARD_TICKET = "https://booking.elal.co.il/newBooking/protectRedirect.do?ndis=false&site=elal&isDirect=true";
	private static final String CABIN = "&cabin=";
	private static final String SETTINGS_1 = "&returnUrl=https://www.elal.com&LANG=";
	private static String SETTINGS_2 = "";
	private static String NOT_ONLY_ELAL = "2";
	private static final String SYSTEM_ID_10 = "&systemId=10";
	private static final String SYSTEM_ID_1 = "&systemId=1";
	private static final String SYSTEM_ID_4 = "&systemId=4";
	private static final String GOLDEN_AGES = "&goldenAges=0";
	private static final String CHILDREN = "&children=";
	private static final String STUDENTS = "&students=0";
	private static final String YOUTHS = "&youths=";
	private static final String ADULTS = "&adults=";
	private static final String INFANTS = "&infants=";
	private static final String RETURN_MONTH = "&returnMonth=";
	private static final String RETURN_DAY = "&returnDay=";
	private static final String DEPART_MONTH = "&departMonth=";
	private static final String DEPART_DAY = "&departDay=";
	private static final String JOURNEY_TYPE = "&journeyType=";
	private static final String DESTINATION = "&destination=";
	private static final String ORIGIN = "origin=";
	private static final String RETURN_FROM = "&returnFrom=";
	private static final String RETURN_TO = "&returnTo=";
	
	private DataQuery mDataQuery = SearchObject.getInstance();
	
	
	public interface DataQuery
	{
		Collection<GetSearchGlobalDataContentUserFamily> getPassengers();
		
		String getFlightClass();
		
		int getSelectedPassengersQuantityByType(ePassengersType iType, eFamilyRelation iFamilyRelation);
		
		int getSelectedPassengersQuantityByType(ePassengersType iType);
		
		Date getCurrentLastDateInSelection();
		
		Date getCurrentFirstDateInSelection();
		
		eTypesOfTrip getCurrentTypeOfTrip();
		
		GetSearchGlobalDataContentElalCities getCurrentToDestination();
		
		GetSearchGlobalDataContentElalCities getCurrentOriginDestination();
		
		GetSearchGlobalDataContentElalCities getAdditionalCurrentOriginDestination();
		
		GetSearchGlobalDataContentElalCities getAdditionalCurrentToDestination();
		
		eTabStatement getCurrentTabStatement();
	}
	
	private String mLink = "";
	
	public String getSearchFlightUrl()
	{
		String searchFlightUrl = getBaseUrlByFlightType();
		
		String searchFlightDetails = getSearchFlightDetails();/*getOriginDestinationUrl() + getToDestinationUrl() + getJourneyTypeUrl() + getDepartDayUrl() + //
				getDepartMonthUrl() + getReturnDayUrl() + getReturnMonthUrl() + getAdultsQuantityUrl() + getInfantsQuantityUrl() + //
				getStudentsQuantityUrl() + getYouthsQuantityUrl() + getChildrenQuantityUrl() + getGoldenAgesQuantityUrl() + //
				getTypeOfFlightUrl() + getSettings() + getUDID() + getFamilyCredentialsUrl() + tryToGetEmailAndPhoneUrl() + isMobile();*/
		
		mLink += searchFlightUrl + "&"/*ENC=*/ + searchFlightDetails/* + "&ENC_TIME=" + new Date().getTime()*/;
		
		searchFlightDetails = ENC + AppUtils.encrypt(searchFlightDetails + ENC_TIME + DateTimeUtils.convertDateToENCDateString(new Date(), false));
		
		mLink += "\n\n";
		mLink += searchFlightUrl + "&ENC=" + searchFlightDetails + "&ENC_TIME=" + new Date().getTime();
		
		searchFlightUrl = searchFlightUrl + searchFlightDetails;
		
		copyToClipBoard(mLink);
		
		mLink = "";
		
		return searchFlightUrl;
	}
	
	public String getSearchFlightDetails()
	{
		String searchFlightDetails = getOriginDestinationUrl() + getToDestinationUrl() + getJourneyTypeUrl() + getDepartDayUrl() + //
				getDepartMonthUrl() + getReturnDayUrl() + getReturnMonthUrl() + getAdultsQuantityUrl() + getInfantsQuantityUrl() + //
				getStudentsQuantityUrl() + getYouthsQuantityUrl() + getChildrenQuantityUrl() + getGoldenAgesQuantityUrl() + //
				getTypeOfFlightUrl() + getSettings() + getUDID() + getFamilyCredentialsUrl() + tryToGetEmailAndPhoneUrl() + getUserId() + isMobile();
		
		searchFlightDetails = "ENC=" + AppUtils.encrypt(searchFlightDetails + ENC_TIME + DateTimeUtils.convertDateToENCDateString(DateTimeUtils.getCurrentDateByGMT("+03")/*new Date()*/, false));
		return searchFlightDetails;
	}
	
	public String getSearchFlightUrlNoENC()
	{
		String searchFlightUrl = getBaseUrlByFlightType();
		return searchFlightUrl;
	}
	
	private void copyToClipBoard(final String iStr)
	{
		Context context = ElAlApplication.getInstance();
		
		if (ElalPreferenceUtils.getSharedPreferenceBooleanOrFalse(context, ElalPreferenceUtils.SECRET_COUNT) && GlobalVariables.isDebugVersion)
		{
			AppUtils.copyToClipBoard("link", iStr);
			ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), ElalPreferenceUtils.SECRET_COUNT, false);
		}
	}
	
	@NonNull
	private String getUDID()
	{
		return UDID + UserData.getInstance().getUdid();
	}
	
	@NonNull
	public String getUserId()
	{
		return USER_ID + UserData.getInstance().getUserID();
	}
	
	@NonNull
	@Contract(pure = true)
	private String isMobile()
	{
		return "";
	}
	
	private String tryToGetEmailAndPhoneUrl()
	{
		String emailAndPhoneUrl = "";
		
		UserData userData = UserData.getInstance();
		UserObject userObject = userData.getUserObject();
		
		if (!userData.isUserGuest())
		{
			emailAndPhoneUrl += HOME_PHONE;
			emailAndPhoneUrl += userObject.getPhoneNumber();
			emailAndPhoneUrl += EMAIL;
			emailAndPhoneUrl += userObject.getEmail();
		}
		
		return emailAndPhoneUrl;
	}
	
	private String getFamilyCredentialsUrl()
	{
		StringBuilder familyCredentialsUrl = new StringBuilder();
		int count = 1;
		
		for (GetSearchGlobalDataContentUserFamily iMember : mDataQuery.getPassengers())
		{
			if (iMember.isSelectedForSearching())
			{
				String memberId = iMember.getMatmidMemberID() == 0 || iMember.getMatmidMemberID() == -1 ? "" : String.valueOf(iMember.getMatmidMemberID());
				String dob = new SimpleDateFormat(DateTimeUtils.DATE_FORMAT_yyyy_MM_ddHHmmSS, LocaleUtils.getLocale()).format(iMember.getDOB());
				
				familyCredentialsUrl.append(TITLE);
				familyCredentialsUrl.append(String.valueOf(count));
				familyCredentialsUrl.append(EQUAL_SING);
				familyCredentialsUrl.append(iMember.getTitle());
				
				familyCredentialsUrl.append(FIRST_NAME);
				familyCredentialsUrl.append(String.valueOf(count));
				familyCredentialsUrl.append(EQUAL_SING);
				familyCredentialsUrl.append(iMember.getFirstNameEng());
				
				familyCredentialsUrl.append(LAST_NAME);
				familyCredentialsUrl.append(String.valueOf(count));
				familyCredentialsUrl.append(EQUAL_SING);
				familyCredentialsUrl.append(iMember.getLastNameEng());
				
				familyCredentialsUrl.append(DOB);
				familyCredentialsUrl.append(String.valueOf(count));
				familyCredentialsUrl.append(EQUAL_SING);
				familyCredentialsUrl.append(dob);
				
				familyCredentialsUrl.append(MEMBER_NUMBER);
				familyCredentialsUrl.append(String.valueOf(count));
				familyCredentialsUrl.append(EQUAL_SING);
				familyCredentialsUrl.append(memberId);
				
				count++;
			}
		}
		
		return familyCredentialsUrl.toString();
	}
	
	@NonNull
	private String getSettings()
	{
		//set lang code by app settings
		String langCode = "IL";
		eLanguage language = UserData.getInstance().getLanguage();
		
		if (language != null && !language.isHebrew())
		{
			langCode = "EN";
		}
		
		String flightClass = mDataQuery.getFlightClass();
		
		return CABIN + flightClass + SETTINGS_1 + langCode;
	}
	
	@NonNull
	@Contract(pure = true)
	private String getGoldenAgesQuantityUrl()
	{
		return GOLDEN_AGES;
	}
	
	@NonNull
	private String getChildrenQuantityUrl()
	{
		return CHILDREN + String.valueOf(mDataQuery.getSelectedPassengersQuantityByType(ePassengersType.CHILDREN));
	}
	
	@NonNull
	private String getYouthsQuantityUrl()
	{
		int numOfYouth = 0;
		if (mDataQuery.getCurrentTabStatement() != eTabStatement.AWARD_TICKET)
		{
			numOfYouth =  mDataQuery.getSelectedPassengersQuantityByType(ePassengersType.YOUTH) + mDataQuery.getSelectedPassengersQuantityByType(ePassengersType.TEENAGERS);
		}
		return YOUTHS + String.valueOf(numOfYouth);
	}
	
	@NonNull
	@Contract(pure = true)
	private String getStudentsQuantityUrl()
	{
		return STUDENTS;
	}
	
	@NonNull
	private String getAdultsQuantityUrl()
	{
		int numOfAdults = mDataQuery.getSelectedPassengersQuantityByType(ePassengersType.ADULTS);
		if (mDataQuery.getCurrentTabStatement() == eTabStatement.AWARD_TICKET)
		{
			numOfAdults += mDataQuery.getSelectedPassengersQuantityByType(ePassengersType.TEENAGERS);
			numOfAdults += mDataQuery.getSelectedPassengersQuantityByType(ePassengersType.YOUTH);
		}
		
		return ADULTS + String.valueOf(numOfAdults);
	}
	
	@NonNull
	private String getInfantsQuantityUrl()
	{
		return INFANTS + String.valueOf(mDataQuery.getSelectedPassengersQuantityByType(ePassengersType.INFANTS));
	}
	
	private String getReturnMonthUrl()
	{
		String returnMonth = RETURN_MONTH;
		Date date = DateTimeUtils.getDate(mDataQuery.getCurrentLastDateInSelection(), DateTimeUtils.eCalendarUnit.MONTH, -1);
		
		if (date != null)
		{
			String result = new SimpleDateFormat(DateTimeUtils.DATE_FORMAT_MM, LocaleUtils.getLocale()).format(date);
			
			if (result.toLowerCase().equals(LAST_MONTH.toLowerCase()))
			{
				result = FIRST_MONTH;
			}
			
			returnMonth += result;
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.DamagedUrl.getErrorMessage());
		}
		
		return returnMonth;
	}
	
	private String getReturnDayUrl()
	{
		String returnDay = RETURN_DAY;
		
		if (mDataQuery.getCurrentLastDateInSelection() != null)
		{
			returnDay += new SimpleDateFormat(DateTimeUtils.DATE_FORMAT_dd, LocaleUtils.getLocale()).format(mDataQuery.getCurrentLastDateInSelection());
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.DamagedUrl.getErrorMessage());
		}
		
		return returnDay;
	}
	
	private String getDepartMonthUrl()
	{
		String departMonth = DEPART_MONTH;
		Date date = DateTimeUtils.getDate(mDataQuery.getCurrentFirstDateInSelection(), DateTimeUtils.eCalendarUnit.MONTH, -1);
		
		if (date != null)
		{
			String result = new SimpleDateFormat(DateTimeUtils.DATE_FORMAT_MM, LocaleUtils.getLocale()).format(date);
			
			if (result.toLowerCase().equals(LAST_MONTH.toLowerCase()))
			{
				result = FIRST_MONTH;
			}
			
			departMonth += result;
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.DamagedUrl.getErrorMessage());
		}
		
		return departMonth;
		
	}
	
	private String getDepartDayUrl()
	{
		String departDay = DEPART_DAY;
		
		if (mDataQuery.getCurrentFirstDateInSelection() != null)
		{
			departDay += new SimpleDateFormat(DateTimeUtils.DATE_FORMAT_dd, LocaleUtils.getLocale()).format(mDataQuery.getCurrentFirstDateInSelection());
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.DamagedUrl.getErrorMessage());
		}
		
		return departDay;
	}
	
	private String getJourneyTypeUrl()
	{
		String journeyType = JOURNEY_TYPE;
		
		if (mDataQuery.getCurrentTypeOfTrip() != null)
		{
			journeyType += String.valueOf(mDataQuery.getCurrentTypeOfTrip().getTypeForWebView());
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.DamagedUrl.getErrorMessage());
		}
		
		return journeyType;
	}
	
	private String getToDestinationUrl()
	{
		String toDestinationUrl = DESTINATION;
		
		if (mDataQuery.getCurrentToDestination() != null)
		{
			toDestinationUrl += mDataQuery.getCurrentToDestination().getCityCode();
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.DamagedUrl.getErrorMessage());
		}
		
		return toDestinationUrl;
	}
	
	private String getOriginDestinationUrl()
	{
		String originDestinationUrl = ORIGIN;
		
		if (mDataQuery.getCurrentOriginDestination() != null)
		{
			originDestinationUrl += mDataQuery.getCurrentOriginDestination().getCityCode();
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.DamagedUrl.getErrorMessage());
		}
		
		return originDestinationUrl;
	}
	
	private String getTypeOfFlightUrl()
	{
		String typeOfFlightUrl = RETURN_FROM;
		
		if (mDataQuery.getCurrentTypeOfTrip().isReturnFromAnotherDestination())
		{
			typeOfFlightUrl += mDataQuery.getAdditionalCurrentOriginDestination() != null ? mDataQuery.getAdditionalCurrentOriginDestination().getCityCode() : "";
			
			if (mDataQuery.getAdditionalCurrentToDestination() != null)
			{
				typeOfFlightUrl += RETURN_TO;
				typeOfFlightUrl += mDataQuery.getAdditionalCurrentToDestination().getCityCode();
			}
		}
		else
		{
			typeOfFlightUrl += RETURN_TO;
		}
		
		return typeOfFlightUrl;
	}
	
	private String getBaseUrlByFlightType()
	{
		String baseUrl = "";
		
		eTabStatement mCurrentTabStatement = mDataQuery.getCurrentTabStatement();
		String systemId = "";
		
		switch (mCurrentTabStatement)
		{
			case AWARD_TICKET:
			{
				baseUrl += URL_AWARD_TICKET;
				systemId = SYSTEM_ID_4;
				SETTINGS_2 = RequestStringBuilder.UTM_BONUS_TICKET;
				break;
			}
			case FLIGHT_TICKET:
			{
				baseUrl += URL_FLIGHT_TICKET;
				systemId = SYSTEM_ID_1;
				SETTINGS_2 = RequestStringBuilder.UTM_SEARCH_FLIGHT;
				break;
			}
			case POINTS_AND_MONEY:
			{
				baseUrl += URL_POINTS_AND_MONEY_TICKET;
				systemId = SYSTEM_ID_1;
				SETTINGS_2 = RequestStringBuilder.UTM_TICKET_MONEY_POINT;
				break;
			}
			default:
			{
				AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.NoSuchItems.getErrorMessage());
			}
		}
		
		String toCityCode = mDataQuery.getCurrentToDestination().getCityCode();
		String originCityCode = mDataQuery.getCurrentOriginDestination().getCityCode();
		
		if (toCityCode.toLowerCase().contains(NOT_ONLY_ELAL.toLowerCase()) || originCityCode.toLowerCase().contains(NOT_ONLY_ELAL.toLowerCase()))
		{
			systemId = SYSTEM_ID_10;
		}
		
		SETTINGS_2 += systemId;
		
		return baseUrl + SETTINGS_2;
	}
}
