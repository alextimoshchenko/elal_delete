package interfaces;

import java.io.Serializable;

/**
 * Created with care by Shahar Ben-Moshe on 26/02/17.
 */

public interface IAddOrEditCheckListItemListener extends Serializable
{
	void onSuccessfulAdd();
}
