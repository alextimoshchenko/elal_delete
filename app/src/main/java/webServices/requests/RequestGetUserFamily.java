package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestGetUserFamily
{
	@JsonProperty("UserID")
	private int mUserID;
	
	public RequestGetUserFamily(int iUserID)
	{
		mUserID = iUserID;
	}
	
	public RequestGetUserFamily()
	{
	}
}
