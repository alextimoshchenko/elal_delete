package webServices.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with care by Shahar Ben-Moshe on 16/09/15.
 */
public class ResponseIpInfo
{
	@JsonProperty("ip")
	private String mIp;
	@JsonProperty("hostname")
	private String mHostname;
	@JsonProperty("city")
	private String mCity;
	@JsonProperty("country")
	private String mCountry;
	@JsonProperty("loc")
	private String mLoc;
	@JsonProperty("org")
	private String mOrg;
	
	public String getIp()
	{
		return mIp;
	}
}