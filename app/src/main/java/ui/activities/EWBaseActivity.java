package ui.activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.tagmanager.DataLayer;
import com.guardsquare.dexguard.runtime.detection.CertificateChecker;
import com.guardsquare.dexguard.runtime.detection.TamperDetector;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;
import java.util.Map;

import global.AppData;
import global.ElAlApplication;
import global.GlobalVariables;
import global.MyContextWrapper;
import global.UserData;
import global.eLanguage;
import il.co.ewave.elal.R;
import interfaces.IEwOnServiceConnectedListener;
import services.EWSSLService;
import ui.customWidgets.CustomProgressDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import utils.errors.LocalError;
import utils.global.AppUtils;
import utils.global.FragmentSwapper;
import utils.global.googleAnalitics.GoogleAnalyticsManager;
import utils.global.googleAnalitics.googleTagManager.TagManagerImplementation;
import utils.global.googleAnalitics.googleTagManager.TagManagerImplementation.ePushEvents;
import utils.permissions.IPermissionsListener;
import utils.permissions.PermissionManager;
import webServices.controllers.LoginController;
import webServices.push.MyFirebaseMessagingService;
import webServices.requests.RequestGetUnreadNotificationsBadge;
import webServices.responses.ResponseGetUnreadNotificationsBadge;

public abstract class EWBaseActivity extends AppCompatActivity implements IPermissionsListener
{
	private final String TAG = EWBaseActivity.class.getSimpleName();
	protected EWSSLService mService = null;
	protected ServiceConnection mServiceConnection = null;
	protected boolean mServiceConnected = false;
	protected Bundle mSavedInstanceState = null;
	private FragmentSwapper mFragmentSwapper;
	private ArrayList<IEwOnServiceConnectedListener> mEwOnServiceConnectedListenerList = new ArrayList<>();
	//	private ProgressDialog mProgressDialog;
	private GoogleAnalyticsManager mGoogleAnalyticsManager;
	private CustomProgressDialog mCustomProgressDialog;
	private boolean isFirstOpen = true;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		ElAlApplication.getInstance().setUserDefinedLanguage();
		super.onCreate(savedInstanceState);
		
		setCustomScreenLockTime();
		
		CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
		
		mSavedInstanceState = savedInstanceState;
		
		initGoogleAnalyticsManager();
		initServiceConnection();
		initScreen();
		
		//		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		registerReceiver(myReceiver, new IntentFilter(MyFirebaseMessagingService.INTENT_NOTIFICATION));
		overridePendingTransition(R.anim.slide_in, R.anim.fade_to_black);
		
		//For tamper detection
		if (isFirstOpen)
		{
			isFirstOpen = false;
			new Delegate().checkAndInitialize();
		}
	}
	
	private void setCustomScreenLockTime()
	{
		int permissionWriteSettings = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_SETTINGS);

		ArrayList<String> listPermissionsNeeded = new ArrayList<>();
		if (permissionWriteSettings != PackageManager.PERMISSION_GRANTED)
		{
			listPermissionsNeeded.add(Manifest.permission.WRITE_SETTINGS);
			
			PermissionManager.requestApplicationPermissionsIfNeeded(this, listPermissionsNeeded, this);
		}
		else
		{
			android.provider.Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, AppUtils.SCREEN_WAKE_TIME);
		}
	}
	
	@Override
	protected void onSaveInstanceState(final Bundle outState)
	{
		super.onSaveInstanceState(outState);
	}
	
	private void initGoogleAnalyticsManager()
	{
		mGoogleAnalyticsManager = GoogleAnalyticsManager.getAnalyticsManagerObject();
	}
	
	private BroadcastReceiver myReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			handleBroadcastEvent();
		}
	};
	
	private void handleBroadcastEvent()
	{
		getAndUpdateMessageNumber();
	}
	
	public void getAndUpdateMessageNumber()
	{
		if (isServiceConnected())
		{
			final int userId = UserData.getInstance().getUserID();
			
			getService().getController(LoginController.class).GetUnreadNotificationsBadge(new RequestGetUnreadNotificationsBadge(userId), new Response.Listener<ResponseGetUnreadNotificationsBadge>()
			{
				@Override
				public void onResponse(final ResponseGetUnreadNotificationsBadge response)
				{
					AppData.getInstance().setUnReadPushes(response);
					doOnUpdateNumberOfMessages();
				}
			}, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError error)
				{
					AppUtils.printLog(Log.ERROR, TAG, error.getMessage());
				}
			});
		}
	}
	
	private void doOnUpdateNumberOfMessages()
	{
		if (this instanceof EWBaseDrawerActivity)
		{
			int numberOfPushes = AppData.getInstance().getUnReadPushes();
			((EWBaseDrawerActivity) EWBaseActivity.this).setUnreadPushes(numberOfPushes);
			((EWBaseDrawerActivity) EWBaseActivity.this).notifyPushesIndicator();
		}
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		handleBroadcastEvent();
	}
	
	@Override
	protected void attachBaseContext(Context newBase)
	{
		if (UserData.getInstance().getLanguage() != null && !TextUtils.isEmpty(UserData.getInstance().getLanguage().getLanguageCode()) && eLanguage.isLanguageSupported(UserData.getInstance()
		                                                                                                                                                                        .getLanguage()))
		{
			//			super.attachBaseContext(CalligraphyContextWrapper.wrap(MyContextWrapper.wrap(newBase, UserData.getInstance().getLanguage().getLanguageCode())));
		}
		else
		{
			UserData.getInstance().setLanguage(eLanguage.English);
		}
		super.attachBaseContext(CalligraphyContextWrapper.wrap(MyContextWrapper.wrap(newBase, UserData.getInstance().getLanguage().getLanguageCode())));
	}
	
	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		
		if (!isTaskRoot())
		{
			overridePendingTransition(R.anim.slide_in_reverse, R.anim.fade_to_black);
		}
	}
	
	/**
	 * Init Action bar. Call in onCreate after initScreen
	 */
	//    protected abstract void initActionbar();
	@Override
	protected void onStop()
	{
		if (mServiceConnected && mServiceConnection != null)
		{
			unbindService(mServiceConnection);
			mServiceConnected = false;
		}
		
		super.onStop();
	}
	
	@Override
	protected void onDestroy()
	{
		//		super.onDestroy();
		try
		{
			unregisterReceiver(myReceiver);
		}
		catch (Exception iE)
		{
			iE.printStackTrace();
		}
		super.onDestroy();
	}
	
	/**
	 * Service connection initialization.
	 */
	protected void initServiceConnection()
	{
		mServiceConnection = new ServiceConnection()
		{
			@Override
			public void onServiceConnected(ComponentName cn, IBinder binder)
			{
				try
				{
					//					mService = ((EWService.EWServiceBinder) binder).getService();
					mService = ((EWSSLService.EWSSLServiceBinder) binder).getService();
					EWBaseActivity.this.onServiceConnected();
					notifyOnServiceConnectedListenerIfAvailable();
				}
				catch (Exception ignored)
				{
				}
			}
			
			@Override
			public void onServiceDisconnected(ComponentName name)
			{
				AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.ServiceDisconnected.getErrorMessage());
				mServiceConnected = false;
				mService = null;
			}
		};
	}
	
	/**
	 * calls in onCreate after execution of service binding
	 */
	protected abstract void initScreen();
	
	/**
	 * Notify an Activity when EWTService bounded and
	 */
	protected abstract void onServiceConnected();
	
	@Override
	protected void onStart()
	{
		if (mServiceConnection != null)
		{
			Intent intent = new Intent(this, EWSSLService.class);
			mServiceConnected = bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
		}
		
		super.onStart();
	}
	
	/**
	 * @return the mService
	 */
	public EWSSLService getService()
	{
		return mService;
	}
	
	/**
	 * @return the mServiceConnected
	 */
	public boolean isServiceConnected()
	{
		return mServiceConnected && getService() != null;
	}
	
	public boolean isNetworkAvailable()
	{
		return isServiceConnected() && getService().isNetworkAvailable();
	}
	
	@Override
	public void finish()
	{
		finish(RESULT_CANCELED);
	}
	
	public void finish(int resultCode)
	{
		finish(resultCode, null);
	}
	
	public void finish(int resultCode, Intent intent)
	{
		if (intent != null)
		{
			setResult(resultCode, intent);
		}
		else
		{
			setResult(resultCode);
		}
		
		super.finish();
	}
	
	public void setProgressDialog(boolean iShouldShow)
	{
		try
		{
			if (mCustomProgressDialog == null)
			{
				mCustomProgressDialog = new CustomProgressDialog(this);
			}
			
			if (iShouldShow)
			{
				if (!mCustomProgressDialog.isShowing())
				{
					mCustomProgressDialog.show();
				}
			}
			else
			{
				//TODO avishay 06/09/17 allow it after the test
				mCustomProgressDialog.dismiss();
			}
		}
		catch (Exception e)
		{
			AppUtils.printLog(Log.ERROR, TAG, e.getMessage());
		}
	}
	
	public void addEwOnServiceConnectedListener(IEwOnServiceConnectedListener iEwOnServiceConnectedListener)
	{
		if (!mEwOnServiceConnectedListenerList.contains(iEwOnServiceConnectedListener))
		{
			mEwOnServiceConnectedListenerList.add(iEwOnServiceConnectedListener);
		}
		
		notifyOnServiceConnectedListenerIfAvailable();
	}
	
	public void removeEwOnServiceConnectedListener(IEwOnServiceConnectedListener iEwOnServiceConnectedListener)
	{
		mEwOnServiceConnectedListenerList.remove(iEwOnServiceConnectedListener);
	}
	
	private void notifyOnServiceConnectedListenerIfAvailable()
	{
		if (mServiceConnected && mService != null)
		{
			for (IEwOnServiceConnectedListener ewOnServiceConnectedListener : mEwOnServiceConnectedListenerList)
			{
				ewOnServiceConnectedListener.onServiceConnected();
			}
		}
	}
	
	public FragmentSwapper getFragmentSwapper()
	{
		if (mFragmentSwapper == null)
		{
			mFragmentSwapper = new FragmentSwapper(getSupportFragmentManager());
		}
		
		return mFragmentSwapper;
	}
	
	@Override
	protected void onActivityResult(final int iRequestCode, final int iResultCode, final Intent iData)
	{
		if (mFragmentSwapper != null && mFragmentSwapper.getTopFragment() != null)
		{
			mFragmentSwapper.getTopFragment().onActivityResult(iRequestCode, iResultCode, iData);
		}
	}
	
	@Override
	public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults)
	{
		if (!getFragmentSwapper().isFragmentStackEmpty())
		{
			getFragmentSwapper().getTopFragment().onRequestPermissionsResult(requestCode, permissions, grantResults);
		}
		else
		{
			super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		}
	}
	
	public void setGoogleTagManager()
	{
		mGoogleAnalyticsManager.init(this);
	}
	
	public void pushCustomEvent(Map<String, Object> iMap)
	{
		pushCustomEvent(getString(R.string.auto_event), iMap);
	}
	
	public void pushEvent(ePushEvents iPushEvent)
	{
		GoogleAnalyticsManager googleAnalyticsManager = GoogleAnalyticsManager.getAnalyticsManagerObject();
		googleAnalyticsManager.init(this);
		TagManagerImplementation tagManager = googleAnalyticsManager.getTagManager();
		tagManager.pushEvent(iPushEvent);
	}
	
	public void pushCustomEvent(String iKey, Map<String, Object> iMap)
	{
		GoogleAnalyticsManager googleAnalyticsManager = GoogleAnalyticsManager.getAnalyticsManagerObject();
		googleAnalyticsManager.init(this);
		TagManagerImplementation tagManager = googleAnalyticsManager.getTagManager();
		tagManager.pushCustomEvent(iKey, iMap);
	}
	
	public void pushScreenOpenEvent(String iScreenName)
	{
		pushCustomEvent(getString(R.string.open_screen), DataLayer.mapOf(getString(R.string.screen_name), iScreenName));
	}
	
	//TODO ask Tal about the app behaviour if it was tamper.
	
	/**
	 * This utility class performs the tamper detection and creates the Toast.
	 * <p>
	 * We're putting this functionality in a separate class so we can encrypt
	 * it, as an extra layer of protection around the tamper detection and
	 * some essential code. We can't encrypt the activity itself, for
	 * technical reasons, but an inner class or another class are fine.
	 */
	private class Delegate
	{
		void checkAndInitialize()
		{
			// We need a context for most methods.
			final Context context = EWBaseActivity.this;
			
			// You can pick your own value or values for OK,
			// to make the code less predictable.
			final int OK = 1;
			
			// Let the DexGuard runtime library detect whether the apk has
			// been modified or repackaged in any way (with jar, zip,
			// jarsigner, zipalign, or any other tool), after DexGuard has
			// packaged it. The return value is the value of the optional
			// integer argument OK (default=0) if the apk is unchanged.
			int apkChanged = TamperDetector.checkApk(context, OK);
			
			// Let the DexGuard runtime library detect whether the apk has
			// been re-signed with a different certificate, after DexGuard has
			// packaged it.  The return value is the value of the optional
			// integer argument OK (default=0) if the certificate is still
			// the same.
			int certificateChanged = CertificateChecker.checkCertificate(context, OK);
			
			// You can also explicitly pass the MD5 hash of a certificate, if
			// the application is only signed after DexGuard has packaged it.
			// You can print out the MD5 hash of the certificate of your key
			// store with
			//   keytool -list -keystore my.keystore
			//
			// If you are publishing on the Amazon Store, you can find the MD5
			// hash in
			//   Amazon Apps & Games Developer Portal
			//     > Binary File(s) Tab > Settings > My Account.
			//
			// With your MD5 hash, you can then use one of
			//   CertificateChecker.checkCertificate(context,
			//     "FA:F8:0A:CB:26:C9:08:DD:3F:E4:A4:76:1B:37:3E:C1", OK);
			//   CertificateChecker.checkCertificate(context,
			//     "FAF80ACB26C908DD3FE4A4761B373EC1", OK);
			//   CertificateChecker.checkCertificate(context,
			//     0xFAF80ACB, 0x26C908DD, 0x3FE4A476, 0x1B373EC1, OK);
			//   CertificateChecker.checkCertificate(context,
			//     0xFAF80ACB26C908DDL, 0x3FE4A4761B373EC1L, OK);
			//
			// If you specify a string, you should make sure it is encrypted,
			// for good measure.
			
			if (apkChanged == OK && certificateChanged == OK)
			{
				//All is ok
				AppUtils.printLog(Log.DEBUG, TAG, "All is ok");
			}
			else
			{
				String comment;
				
				if (certificateChanged != OK)
				{
					comment = "The certificate is not the expected certificate";
				}
				else
				{
					comment = "The application archive has been modified";
				}
				
				if (GlobalVariables.isTamperDetection)
				{
					Toast.makeText(context, comment, Toast.LENGTH_LONG).show();
					//				In this case we are just need to close the app
					throw new RuntimeException(comment);
				}
			}
		}
	}
	
	@Override
	public void doOnPermissionsGranted(final String[] iPermissions)
	{
		if (PermissionManager.isPermissionInArray(iPermissions, Manifest.permission.CAMERA))
		{
			setCustomScreenLockTime();
		}
	}
	
	@Override
	public void doOnPermissionsDenied(final String[] iPermissions)
	{
	}
}
