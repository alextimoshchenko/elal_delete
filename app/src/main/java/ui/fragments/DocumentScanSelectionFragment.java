package ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import global.GlobalVariables;
import il.co.ewave.elal.R;
import ui.activities.DocumentScanActivity;

import static global.IntentExtras.SCAN_RESULT_RECEIVING_FRAGMENT;

/**
 * Created with care by Shahar Ben-Moshe on 01/01/17.
 */

public class DocumentScanSelectionFragment extends EWBaseFragment
{
	public static Bundle createBundleForFragmentResult(EWBaseFragment iScanResultReceivingFragment)
	{
		Bundle result = new Bundle();
		
		result.putParcelable(SCAN_RESULT_RECEIVING_FRAGMENT, iScanResultReceivingFragment);
		
		return result;
	}
	
	private EWBaseFragment mScanResultReceivingFragment;
	private Button mBtnPassport;
	private Button mBtnDrivingLicense;
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.fragment_document_scan_selection);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		getIntentExtras();
		initReference();
		setListeners();
	}
	
	private void getIntentExtras()
	{
		if (getArguments() != null)
		{
			mScanResultReceivingFragment = getArguments().getParcelable(SCAN_RESULT_RECEIVING_FRAGMENT);
		}
	}
	
	private void initReference()
	{
		this.mBtnPassport = (Button) getActivity().findViewById(R.id.btn_documentScanSelection_Passport);
		this.mBtnDrivingLicense = (Button) getActivity().findViewById(R.id.btn_documentScanSelection_DrivingLicense);
	}
	
	private void setListeners()
	{
		mBtnPassport.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				goToScanDocumentActivityByType(GlobalVariables.OCR_TYPE_PASSPORT);
			}
		});
		
		mBtnDrivingLicense.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				goToScanDocumentActivityByType(GlobalVariables.OCR_TYPE_ISRAELI_DRIVING_LICENSE);
			}
		});
	}
	
	private void goToScanDocumentActivityByType(@GlobalVariables.OcrType int iOcrType)
	{
		DocumentScanActivity.tryStartDocumentScanActivityForResult(getActivity(), mScanResultReceivingFragment, iOcrType);
		tryPopFragment();
	}
}
