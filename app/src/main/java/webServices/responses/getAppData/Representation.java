package webServices.responses.getAppData;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Representation
{
	@JsonProperty("RepresentationNameHeb")
	private String mRepresentationNameHeb;
	@JsonProperty("RepresentationID")
	private int mRepresentationID;
	@JsonProperty("RepresentationNameEng")
	private String mRepresentationNameEng;
	@JsonProperty("CountryCode")
	private String mCountryCode;
	@JsonProperty("RepresentationName")
	private String mRepresentationName;
	
	public String getRepresentationNameHeb()
	{
		return this.mRepresentationNameHeb;
	}
	
	public int getRepresentationID()
	{
		return this.mRepresentationID;
	}
	
	public String getRepresentationNameEng()
	{
		return this.mRepresentationNameEng;
	}
	
	public String getCountryCode()
	{
		return this.mCountryCode;
	}
	
	public String getRepresentationName()
	{
		return this.mRepresentationName;
	}
	
	public Representation()
	{
	}
	
	@Override
	public String toString()
	{
		return mRepresentationName;
	}
}
