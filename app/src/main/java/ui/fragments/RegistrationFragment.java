package ui.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import global.ElAlApplication;
import global.GlobalVariables;
import global.IntentExtras;
import global.UserData;
import global.eLanguage;
import il.co.ewave.elal.R;
import interfaces.ILoginResponse;
import ui.activities.EWBaseActivity;
import ui.activities.EWBaseUserActivity;
import ui.customWidgets.ElalCustomTilLL;
import ui.fragments.details.DetailScreenFragment;
import utils.errors.ErrorsHandler;
import utils.errors.LocalError.eLocalError;
import utils.errors.ServerError;
import utils.global.AppUtils;
import utils.global.CustomTextWatcher;
import utils.global.ElalPreferenceUtils;
import utils.global.LocaleUtils;
import utils.global.googleAnalitics.googleTagManager.TagManagerImplementation.ePushEvents;
import webServices.controllers.LoginController;
import webServices.global.RequestStringBuilder;
import webServices.requests.RequestRegister;
import webServices.responses.ResponseGetRepresentationByIp;
import webServices.responses.ResponseRegister;
import webServices.responses.ResponseUserLogin;
import webServices.responses.responseUserLogin.UserLogin;

import static ui.activities.MainActivity.LOGIN_TYPE_MATMID_CLUB_MEMBER;
import static ui.activities.MainActivity.LOGIN_TYPE_REGISTERED_USER;

public class RegistrationFragment extends EWBaseScrollFragment implements Response.ErrorListener
{
	private static final String TAG = RegistrationFragment.class.getSimpleName();
	private CheckBox mCbAgreeRules;
	private TextView mTvLinkToRules, mTvSkipLayoutHeaderTitle, mTvNameScreenTitle, mTvSkipLayoutLoginMatmid, mTvAgreeRulesError;
	private Button mBtnContinue;
	
	private ImageView mIvNameScreenBack, mIvBackArrow;
	private ElalCustomTilLL mTilPhoneNumber, mTilEmail, mTilPassword, mTilPasswordConfirm;
	private EditText mEtEmail, mEtPhoneNumber, mEtPassword, mEtPasswordConfirm;
	
	private ImageView mIvTooltipPassword;
	private ImageView mIvTooltipPasswordConfirm;
	private ScrollView mSvMainContainer;
	private boolean mPassHasInvalidChars = false;
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_registration_layout);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		initReference();
		setGui();
		setListeners();
		
		setDebugData();
		pushScreenOpenEvent("Create an Account");
	}
	
	private void setDebugData()
	{
		if (GlobalVariables.isDebugVersion)
		{
			mEtEmail.setText("vladimirvladimirov@gmail.com");
			mEtPhoneNumber.setText("0523681927");
			mEtPassword.setText("Aa123456");
			mEtPasswordConfirm.setText("Aa123456");
		}
	}
	
	private void initReference()
	{
		
		mTvAgreeRulesError = getActivity().findViewById(R.id.tv_frag_reg_agree_rules_error);
		mIvBackArrow = getActivity().findViewById(R.id.iv_skip_layout_back_arrow);
		mCbAgreeRules = getActivity().findViewById(R.id.cb_frag_reg);
		mTvLinkToRules = getActivity().findViewById(R.id.tv_frag_reg_link_to_rules);
		mBtnContinue = getActivity().findViewById(R.id.btn_frag_reg_continue);
		
		mSvMainContainer = getActivity().findViewById(R.id.sv_main_container);
		
		mTvSkipLayoutHeaderTitle = getActivity().findViewById(R.id.tv_header_skip_layout_title);
		mTvNameScreenTitle = getActivity().findViewById(R.id.tv_name_screen_title);
		mIvNameScreenBack = getActivity().findViewById(R.id.iv_name_screen_back_arrow);
		mTvSkipLayoutLoginMatmid = getActivity().findViewById(R.id.tv_skip_layout_skip);
		
		
		mTilEmail = getActivity().findViewById(R.id.til_frag_reg_email);
		mTilEmail.init(getString(R.string.email_asterisk));
		mTilEmail.setInputTypeEmail();
		mEtEmail = mTilEmail.getEditText();
		
		mTilPhoneNumber = getActivity().findViewById(R.id.til_frag_reg_phone_number);
		mTilPhoneNumber.init(getString(R.string.mobile_phone_number));
		mTilPhoneNumber.setInputTypePhone();
		mEtPhoneNumber = mTilPhoneNumber.getEditText();
		
		//Password
		mTilPassword = getActivity().findViewById(R.id.til_frag_reg_password);
		mTilPassword.init(getResources().getText(R.string.password_asterisk));
		mTilPassword.setInputTypePassword();
		mEtPassword = mTilPassword.getEditText();
		
		mIvTooltipPassword = getActivity().findViewById(R.id.iv_tooltip_password);
		
		//Password Confirm
		mTilPasswordConfirm = getActivity().findViewById(R.id.til_frag_reg_password_confirm);
		mTilPasswordConfirm.init(getString(R.string.confirm_password_asterisk));
		mTilPasswordConfirm.setInputTypePassword();
		mEtPasswordConfirm = mTilPasswordConfirm.getEditText();
		mEtPasswordConfirm.setImeOptions(EditorInfo.IME_ACTION_DONE);
		
		mIvTooltipPasswordConfirm = getActivity().findViewById(R.id.iv_tooltip_password_confirm);
	}
	
	private void setGui()
	{
		mTvLinkToRules.setPaintFlags(mTvLinkToRules.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
		mTvSkipLayoutLoginMatmid.setText(getString(R.string.login_title));
		mTvSkipLayoutHeaderTitle.setText(getString(R.string.login_matmid));
		mTvNameScreenTitle.setText(getActivity().getResources().getString(R.string.create_an_account));
		mIvNameScreenBack.setVisibility(View.GONE);
	}
	
	private void showAgreeRulesError(boolean iState)
	{
		mTvAgreeRulesError.setVisibility(iState ? View.VISIBLE : View.INVISIBLE);
	}
	
	private void setListeners()
	{
		mIvBackArrow.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				//				tryPopFragment();
				goToHomeFragment();
			}
		});
		
		mCbAgreeRules.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked)
			{
				showAgreeRulesError(false);
			}
		});
		
		
		mIvTooltipPassword.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View iV)
			{
				showToolTip(iV, getString(R.string.frag_reg_password_tooltip) );
			}
		});
		
		mIvTooltipPasswordConfirm.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View iV)
			{
				showToolTip(iV, getString(R.string.frag_reg_password_tooltip) );
			}
		});
		
		mBtnContinue.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				hideKeyboard();
				doOnContinue();
				
				//event 8
				((EWBaseActivity) getActivity()).pushEvent(ePushEvents.REGISTER_CUSTOMER_);
			}
		});
		mTvSkipLayoutLoginMatmid.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnMatmidLogin();
			}
		});
		
		mTvLinkToRules.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				doOnReadRules();
			}
		});
		
		mEtEmail.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				showErrorEmail(false);
			}
		});
		
		mEtEmail.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus)
				{
					if (!isEmailValid() && mEtEmail.getText().toString().length() > 0)
					{
						showErrorEmail(true);
					}
				}
			}
		});
		
		mEtPhoneNumber.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				showErrorPhoneNumber(false);
			}
		});
		
		mEtPhoneNumber.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus)
				{
					if (!isPhoneNumberValid() && mEtPhoneNumber.getText().toString().length() > 0)
					{
						showErrorPhoneNumber(true);
					}
				}
			}
		});
		
		mEtPassword.addTextChangedListener(new CustomTextWatcher()
		{
			int lastCharIndex = 0;
			
			@Override
			public void onTextChanged(final CharSequence s, final int start, final int before, final int count)
			{
			
			}
			
			@Override
			public void afterTextChanged(final Editable s)
			{
				showErrorPassword(false);
				
				if (s != null && s.length() > 0)
				{
					if (s.length() > lastCharIndex && !AppUtils.checkValidChar(s.charAt(s.length() - 1) + ""))
					{
						mPassHasInvalidChars = true;
						showErrorPassword(true);
						mEtPassword.setFilters(new InputFilter[] {new InputFilter.LengthFilter(s.length())});
					}
					else
					{
						mPassHasInvalidChars = false;
						showErrorPassword(false);
						mEtPassword.setFilters(new InputFilter[] {new InputFilter.LengthFilter(15)});
					}
					lastCharIndex = s.length();
				}
				else
				{
					mPassHasInvalidChars = false;
					lastCharIndex = 0;
					showErrorPassword(false);
					mEtPassword.setFilters(new InputFilter[] {new InputFilter.LengthFilter(15)});
				}
			}
		});
		
		mEtPassword.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus)
				{
					if (!isPasswordValid() && mEtPassword.getText().toString().length() > 0)
					{
						showErrorPassword(true);
					}
				}
			}
		});
		
		mEtPasswordConfirm.addTextChangedListener(new CustomTextWatcher()
		{
			@Override
			public void afterTextChanged(final Editable s)
			{
				showErrorConfirmPassword(false);
			}
		});
		
		mEtPasswordConfirm.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(final View v, final boolean hasFocus)
			{
				if (!hasFocus)
				{
					if (!isConfirmPasswordValid() && mEtPasswordConfirm.getText().toString().length() > 0)
					{
						showErrorConfirmPassword(true);
					}
				}
			}
		});
	}
	
	private void doOnReadRules()
	{
		Bundle bundle = new Bundle();
		bundle.putString(IntentExtras.URL, RequestStringBuilder.getElalTermsOfUseUrl());
		tryGoToFragmentByFragmentClass(WebViewFragment.class, bundle, true);
	}
	
	private void doOnMatmidLogin()
	{
		ElalPreferenceUtils.setSharedPreference(getContext(), IntentExtras.LAST_SELECTED_LOGIN_TYPE, LOGIN_TYPE_MATMID_CLUB_MEMBER);
		
		Bundle bundle = new Bundle();
		bundle.putBoolean(IntentExtras.LOGIN_AS_MATMID, true);
		
		tryGoToFragmentByFragmentClass(LoginFragment.class, bundle, true);
		
		((EWBaseActivity) getActivity()).pushEvent(ePushEvents.CLUB_MEMBER);
	}
	
	private void doOnContinue()
	{
		if (!isFieldsValid())
		{
			return;
		}
		
		AppUtils.hideKeyboard(getActivity(), getView());
		trySetProgressDialog(true);
		
		doOnGetRepresentationById();
	}
	
	private void doOnGetRepresentationById()
	{
		if (isServiceConnected())
		{
			getService().getController(LoginController.class).GetRepresentationByIp(new Response.Listener<ResponseGetRepresentationByIp>()
			{
				@Override
				public void onResponse(final ResponseGetRepresentationByIp iResponse)
				{
					
					if (iResponse != null && iResponse.getContent() != null && iResponse.getContent().size() > 0)
					{
						UserData.getInstance().setUserRepresentation(iResponse.getContent().get(0).getRepresentationId());
					}
					
					doOnGetRegistration();
				}
			}, RegistrationFragment.this);
		}
	}
	
	private void doOnGetRegistration()
	{
		final String userPass = mEtPassword.getText().toString();
		final String userEmail = mEtEmail.getText().toString();
		
		if (isServiceConnected())
		{
			getService().getController(LoginController.class).Register(new RequestRegister(userEmail, mEtPhoneNumber.getText().toString(), userPass), new Response.Listener<ResponseRegister>()
			{
				@Override
				public void onResponse(final ResponseRegister iResponse)
				{
					trySetProgressDialog(false);
					ElalPreferenceUtils.setSharedPreference(getActivity(), IntentExtras.LOGGEDIN_TYPE, LOGIN_TYPE_REGISTERED_USER);
					
					if (iResponse != null && iResponse.getContent() != null)
					{
						UserData.getInstance().setEmailAndPassword(mEtEmail.getText().toString(), mEtPassword.getText().toString());
						UserData.getInstance().setPhoneNumber(mEtPhoneNumber.getText().toString());
						UserData.getInstance().setUserId(iResponse.getContent().getUserID());
						
						//TODO avishay 27/06/17 Tal told me if user just register the email&password won't be saved. check it with Tal.
						//						UserData.getInstance().saveUserData();
						doOnOpenLetsContinueDialog();
					}
				}
			}, RegistrationFragment.this);
		}
	}
	
	private void doOnOpenLetsContinueDialog()
	{
		
		AppUtils.setSawReminderDialogDetails(true);
		AlertDialog dialog = AppUtils.createSimpleMessageDialog(getActivity(), getString(R.string.thanks_for_signing_up), getString(R.string.few_more_details), getString(R.string.let_me_continue), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, final int which)
			{
				Bundle bundle = new Bundle();
				bundle.putSerializable(IntentExtras.IS_REGISTRATION, true);
				tryGoToFragmentByFragmentClass(DetailScreenFragment.class, bundle, false);
				((EWBaseActivity) getActivity()).pushEvent(ePushEvents.MORE_DETAILS_LETS_CONTINUE);
			}
		}, getString(R.string.remind_me_later), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, final int which)
			{
				ElalPreferenceUtils.setSharedPreference(getContext(), AppUtils.getRemindMeDetailsKey(UserData.getInstance().getUserID()), true);
				goToLogin();
				((EWBaseActivity) getActivity()).pushEvent(ePushEvents.MORE_DETAILS_REMIND_ME_LATER);
			}
		}, getString(R.string.skip_underline), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(final DialogInterface dialog, final int which)
			{
				//				doOnSkipClick();
				goToLogin();
				((EWBaseActivity) getActivity()).pushEvent(ePushEvents.MORE_DETAILS_SKIP);
			}
		});
		
		dialog.setCancelable(false);
		dialog.show();
	}
	
	private void doOnSkipClick()
	{
		ElalPreferenceUtils.setSharedPreference(ElAlApplication.getInstance(), AppUtils.getRemindMeDetailsKey(UserData.getInstance().getUserID()), true);
		goToLogin();
	}
	
	private void goToLogin()
	{
		if (getActivity() != null && getActivity() instanceof EWBaseUserActivity)
		{
			trySetProgressDialog(true);
			
			UserData data = UserData.getInstance();
			String email = data.getEmail();
			String pass = data.getPassword();
			
			((EWBaseUserActivity) getActivity()).performRegisteredUserLogin(email, pass, true, new Response.Listener<ResponseUserLogin>()
			{
				@Override
				public void onResponse(final ResponseUserLogin iResponse)
				{
					if (iResponse != null && iResponse.getContent() != null)
					{
						doOnSuccessfulRegisteredUserLogin(iResponse);
					}
					else
					{
						AppUtils.printLog(Log.ERROR, TAG, eLocalError.GeneralError.getErrorMessage());
					}
				}
				
			}, RegistrationFragment.this);
		}
	}
	
	private void doOnSuccessfulRegisteredUserLogin(final ILoginResponse iResponse)
	{
		trySetProgressDialog(false);
		
		UserData.getInstance().setEmailAndPassword(mEtEmail.getText().toString(), mEtPassword.getText().toString());
		eLanguage userLanguageInServer = eLanguage.getLanguageByCodeOrDefault("", eLanguage.English);
		
		if (iResponse.getContent() instanceof UserLogin)
		{
			UserData.getInstance().setUserId(((UserLogin) iResponse.getContent()).getUserID());
			UserData.getInstance().setUserObject(((UserLogin) iResponse.getContent()).getUserObject());
			userLanguageInServer = eLanguage.getLanguageByCodeOrDefault(((UserLogin) iResponse.getContent()).getUserObject().getLanguageID(), eLanguage.English);
		}
		
		//			UserData.getInstance().saveUserData();
		
		if (userLanguageInServer != UserData.getInstance().getLanguage())
		{
			UserData.getInstance().setLanguage(userLanguageInServer);
			//				UserData.getInstance().saveUserData();
			LocaleUtils.applyLocale(ElAlApplication.getInstance(), LocaleUtils.convertLanguageCodeToLocaleOrNull(userLanguageInServer.getLanguageCode()));
			
			AlertDialog dialog = AppUtils.createSimpleMessageDialog(getActivity(), getString(R.string.restart_after_language_change), null, null, getString(R.string.close), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(final DialogInterface dialog, final int which)
				{
					tryPopFragment();
				}
			});
			dialog.setCancelable(false);
			dialog.show();
		}
		else
		{
			goToHomeFragment();
		}
	}
	
	private void goToHomeFragment()
	{
		if (getActivity() != null && getActivity() instanceof EWBaseUserActivity)
		{
			((EWBaseUserActivity) getActivity()).loadHomeFragment();
		}
	}
	
	private boolean isEmailValid()
	{
		return AppUtils.isEmailValid(mEtEmail.getText().toString());
	}
	
	private boolean isPhoneNumberValid()
	{
		return AppUtils.isPhoneNumberValid(mEtPhoneNumber.getText().toString());
	}
	
	private boolean isPasswordValid()
	{
		return AppUtils.isPasswordStrong(mEtPassword.getText().toString(), false) && !mPassHasInvalidChars;
	}
	
	private boolean isFieldsValid()
	{
		boolean result = true;
		
		if (!mCbAgreeRules.isChecked())
		{
			result = false;
			showAgreeRulesError(true);
			focusOnView(mSvMainContainer, mTvAgreeRulesError);
		}
		if (!isConfirmPasswordValid())
		{
			result = false;
			showErrorConfirmPassword(true);
			focusOnView(mSvMainContainer, mTilPasswordConfirm);
		}
		if (!isPasswordValid())
		{
			result = false;
			showErrorPassword(true);
			focusOnView(mSvMainContainer, mTilPassword);
		}
		if (!isPhoneNumberValid())
		{
			result = false;
			showErrorPhoneNumber(true);
			focusOnView(mSvMainContainer, mTilPhoneNumber);
		}
		if (!isEmailValid())
		{
			result = false;
			showErrorEmail(true);
			focusOnView(mSvMainContainer, mTilEmail);
		}
		return result;
	}
	
	private boolean isConfirmPasswordValid()
	{
		return !TextUtils.isEmpty(mEtPasswordConfirm.getText().toString()) && mEtPassword.getText().toString().equals(mEtPasswordConfirm.getText().toString());
	}
	
	private void showToolTip(View iV, String iText)
	{
		AppUtils.showToolTip(getContext(), iV, iText);
	}
	
	private void showErrorEmail(boolean state)
	{
		showErrorEmail(state, null);
	}
	
	private void showErrorPhoneNumber(boolean state)
	{
		mTilPhoneNumber.setError(state, state ? getString(R.string.mobile_phone_number_incorrect) : null);
	}
	
	private void showErrorConfirmPassword(boolean state)
	{
		mTilPasswordConfirm.setError(state, state ? getString(R.string.password_not_match) : null);
		//		mEtPasswordConfirm.getBackground().mutate().setColorFilter(getResources().getColor(state ? R.color.red : R.color.ElalDarkBlue), PorterDuff.Mode.SRC_ATOP);
		//		mTvErrorPassConfirm.setVisibility(state ? View.VISIBLE : View.INVISIBLE);
	}
	
	private void showErrorPassword(boolean state)
	{
		mTilPassword.setError(state, state ? getString(R.string.invalid_password) : null);
		//		mEtPassword.getBackground().mutate().setColorFilter(getResources().getColor(state ? R.color.red : R.color.ElalDarkBlue), PorterDuff.Mode.SRC_ATOP);
		//		mTvErrorPass.setVisibility(state ? View.VISIBLE : View.INVISIBLE);
	}
	
	@Override
	public void onErrorResponse(final VolleyError error)
	{
		doOnError(error);
		trySetProgressDialog(false);
		AppUtils.printLog(Log.ERROR, TAG, error.getLocalizedMessage());
	}
	
	private void doOnError(final VolleyError iError)
	{
		if (iError != null)
		{
			try
			{
				if (((ServerError) iError).getErrorCode() == ServerError.eServerError.EmailAlreadyExist.getErrorCode())
				{
					focusOnView(mSvMainContainer, mTilEmail);
					showErrorEmail(true, ServerError.eServerError.EmailAlreadyExist.getErrorMessage());
				}
				else
				{
					ErrorsHandler.tryShowServiceErrorDialog(iError, getActivity());
				}
			}
			catch (Exception iE)
			{
				ErrorsHandler.tryShowServiceErrorDialog(iError, getActivity());
			}
			
		}
	}
	
	private void showErrorEmail(boolean state, String iErrorMessage)
	{
		if (state)
		{
			if (!TextUtils.isEmpty(iErrorMessage))
			{
				mTilEmail.setError(true, iErrorMessage);
			}
			else
			{
				mTilEmail.setError(true, getString(R.string.email_not_match));
			}
		}
		else
		{
			mTilEmail.setError(false, null);
			
		}
	}
	
	//	private final void focusOnView(final View iView)
	//	{
	//		mSvMainContainer.post(new Runnable()
	//		{
	//			@Override
	//			public void run()
	//			{
	//				mSvMainContainer.smoothScrollTo(0, iView.getTop());
	//			}
	//		});
	//	}
}
