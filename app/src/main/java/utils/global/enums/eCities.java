package utils.global.enums;

/**
 * Created with care by Aleksey Timoshchenko on 8/18/17.
 */
public enum  eCities
{
	TLV("tlv");
	
	private String mCountryCode;
	
	eCities(String iCountryCode)
	{
		mCountryCode = iCountryCode;
	}
	
	public String getCountryCode()
	{
		return mCountryCode;
	}
}
