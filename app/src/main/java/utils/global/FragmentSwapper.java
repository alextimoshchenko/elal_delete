package utils.global;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import java.util.List;

import utils.managers.TabsBackStackManager;

/**
 * Created with care by Shahar Ben-Moshe on 27-Dec-2015.
 */
public class FragmentSwapper
{
	private FragmentManager mFragmentManager = null;
	
	public FragmentSwapper(FragmentManager fragmentManager)
	{
		mFragmentManager = fragmentManager;
	}
	
	public Fragment swapToFragment(Class<? extends Fragment> fragmentClass, Bundle arguments, int containerResourceId, boolean isShouldaddToBackStack)
	{
		return swapToFragment(fragmentClass, arguments, containerResourceId, isShouldaddToBackStack, true);
	}
	
	public Fragment swapToFragmentWithoutAnimation(Class<? extends Fragment> fragmentClass, Bundle arguments, int containerResourceId, boolean isShouldaddToBackStack)
	{
		return swapToFragment(fragmentClass, arguments, containerResourceId, isShouldaddToBackStack, false);
	}
	
	public Fragment swapToFragment(Class<? extends Fragment> fragmentClass, Bundle arguments, int containerResourceId, boolean isShouldaddToBackStack, boolean isAnimationEnabled)
	{
		final String fragmentTag = getFragmentTag(fragmentClass);
		
		// try to
		Fragment newFragment = mFragmentManager.findFragmentByTag(fragmentTag);
		FragmentTransaction mFt = mFragmentManager.beginTransaction();
		
		// if (newFragment == null)
		{
			try
			{
				newFragment = fragmentClass.newInstance();
				// pass the arguments to the fragment
				// note that you can't call setArguments if the fragment is
				// attached!
				// the last constrain might cause weird behavior in case we
				// would like to send new arguments. Be aware!
				newFragment.setArguments(arguments);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	/*
	 * if (isAnimationEnabled) {
	 * mFt.setCustomAnimations(R.anim.tab_right_to_left_in_animation,
	 * R.anim.tab_right_to_left_out_animation,
	 * R.anim.tab_left_to_right_in_animation,
	 * R.anim.tab_left_to_right_out_animation); }
	 */
		mFt.replace(containerResourceId, newFragment, fragmentTag);
		if (isShouldaddToBackStack)
		{
			mFt.addToBackStack(fragmentTag);
		}
		try
		{
			mFt.commit();
			
			mFragmentManager.executePendingTransactions();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return newFragment;
	}
	
	public void printFragmentsStack()
	{
		@SuppressLint("RestrictedApi") List<Fragment> fragments = mFragmentManager.getFragments();
		
		if (fragments != null)
		{
			Log.i("ui/fragments", "==========================");
			
			for (int i = 0 ; i < fragments.size() ; i++)
			{
				Log.d("ui/fragments", i + " - " + (fragments.get(i) != null ? fragments.get(i).getClass().getSimpleName() : "null"));
			}
			
			Log.i("ui/fragments", "==========================");
		}
	}
	
	public void refreshFragment(Fragment fragment)
	{
		//fragment.setArguments(bundle);
		mFragmentManager.beginTransaction().detach(fragment).attach(fragment).commit();
	}
	
	public void clearFragments()
	{
		if (mFragmentManager != null && mFragmentManager.getBackStackEntryCount() > 0)
		{
			FragmentManager.BackStackEntry first = mFragmentManager.getBackStackEntryAt(0);
			mFragmentManager.popBackStackImmediate(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
			mFragmentManager.executePendingTransactions();
		}
	}
	
	public void clearStack()
	{
		if (mFragmentManager != null && mFragmentManager.getBackStackEntryCount() > 0)
		{
			//Here we are removing all the fragment that are shown here
			if (mFragmentManager.getFragments() != null && mFragmentManager.getFragments().size() > 0)
			{
				for (int i = 0 ; i < mFragmentManager.getFragments().size() ; i++)
				{
					Fragment mFragment = mFragmentManager.getFragments().get(i);
					if (mFragment != null)
					{
						mFragmentManager.beginTransaction().remove(mFragment).commit();
					}
				}
			}
			
			//Here we are clearing back stack fragment entries
			int backStackEntry = mFragmentManager.getBackStackEntryCount();
			if (backStackEntry > 0)
			{
				for (int i = 0 ; i < backStackEntry ; i++)
				{
					mFragmentManager.popBackStackImmediate(mFragmentManager.getBackStackEntryAt(i).getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
					mFragmentManager.executePendingTransactions();
					
//					mFragmentManager.popBackStackImmediate();
				}
			}
			
		}
	}
	
	public void clearFragmentsToPosition(int iPosition)
	{
		if (mFragmentManager.getBackStackEntryCount() > 0 && iPosition >= 0 && mFragmentManager.getBackStackEntryCount() > iPosition)
		{
			FragmentManager.BackStackEntry first = mFragmentManager.getBackStackEntryAt(iPosition);
			mFragmentManager.popBackStackImmediate(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
		}
	}
	
	public boolean isFragmentStackEmpty()
	{
		return mFragmentManager.getBackStackEntryCount() <= 0;
	}
	
	public boolean isFragmentStackMaxOne()
	{
		return mFragmentManager.getBackStackEntryCount() <= 1;
	}
	
	private boolean isTopFragmentOfTypeTabStack()
	{
		boolean result = false;
		
		Fragment topFragment = getTopFragment();
		
		if (topFragment != null && topFragment instanceof TabsBackStackManager.ITabsBackStackManagerHandler)
		{
			result = true;
		}
		
		return result;
	}
	
	public boolean isTopFragmentOfTypeTabStackMaxOne()
	{
		boolean result = true;
		
		if (isTopFragmentOfTypeTabStack())
		{
			result = ((TabsBackStackManager.ITabsBackStackManagerHandler) getTopFragment()).isFirstInTab();
		}
		
		return result;
	}
	
	public void popFragment()
	{
		mFragmentManager.popBackStack();
	}
	
	public Fragment findFragmentByTag(String tag)
	{
		return mFragmentManager.findFragmentByTag(tag);
	}
	
	public Fragment findFragmentById(int id)
	{
		return mFragmentManager.findFragmentById(id);
	}
	
	private static String getFragmentTag(Class<? extends Fragment> baseFragmentClass)
	{
		return baseFragmentClass.getSimpleName();
	}
	
	/**
	 * Finds the top fragment that inherits from the given class in the ui.fragments stack.
	 *
	 * @param iClass The fragment class to find inheritance from.
	 *
	 * @return The position, or -1 if not found.
	 */
	private int getTopFragmentExtendsClassPosition(Class<? extends Fragment> iClass)
	{
		int result = -1;
		
		@SuppressLint("RestrictedApi") List<Fragment> fragmentsInStack = mFragmentManager.getFragments();
		
		if (fragmentsInStack != null)
		{
			for (int i = fragmentsInStack.size() - 1 ; i >= 0 ; i--)
			{
				if (fragmentsInStack.get(i) != null && iClass.isAssignableFrom(fragmentsInStack.get(i).getClass()))
				{
					result = i;
					break;
				}
			}
		}
		return result;
	}
	
	/**
	 * Finds if top fragment in stack is instance of given class.
	 *
	 * @param iFragmentClass The fragment class to check instance of.
	 *
	 * @return true if top fragment in stack is instance of given class.
	 */
	public boolean isTopFragmentIsInstanceOfClass(Class<? extends Fragment> iFragmentClass)
	{
		boolean result = false;
		
		@SuppressLint("RestrictedApi") List<Fragment> fragmentsInStack = mFragmentManager.getFragments();
		
		if (fragmentsInStack != null)
		{
			for (int i = fragmentsInStack.size() - 1 ; i >= 0 ; i--)
			{
				Fragment currentFragment = fragmentsInStack.get(i);
				
				if (currentFragment != null)
				{
					result = iFragmentClass.isInstance(currentFragment);
					break;
				}
			}
		}
		return result;
	}
	
	@SuppressLint("RestrictedApi")
	public Fragment getTopFragment()
	{
		Fragment result = null;
		
		try
		{
			result = mFragmentManager.getFragments().get(getTopFragmentExtendsClassPosition(Fragment.class)/* - 1*/);
		}
		catch (Exception ignore)
		{
		}
		
		return result;
	}
}
