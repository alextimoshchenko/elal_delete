package webServices.responses.responseGetUserActivePNRs;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Avishay.Peretz on 22/06/2017.
 */
public class PnrSeat implements Parcelable
{
	
	@JsonProperty("PNR")
	private String mPNR;
	@JsonProperty("TicketNumber")
	private String mTicketNumber;
	@JsonProperty("FlightNumber")
	private String mFlightNumber;
	@JsonProperty("HasEligibleSeats")
	private boolean mHasEligibleSeats;
	@JsonProperty("HasSeats")
	private boolean mHasSeats;
	@JsonProperty("CategoryId")
	private int mCategoryId;
	@JsonProperty("SubText")
	private String mSubText;
	@JsonProperty("Icon")
	private String mIcon;
	@JsonProperty("URL")
	private String mURL;
	@JsonProperty("SeatNumber")
	private String mSeatNumber;
	
	
	public PnrSeat()
	{
	}
	
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeString(this.mPNR);
		dest.writeString(this.mTicketNumber);
		dest.writeString(this.mFlightNumber);
		dest.writeByte(this.mHasEligibleSeats ? (byte) 1 : (byte) 0);
		dest.writeByte(this.mHasSeats ? (byte) 1 : (byte) 0);
		dest.writeInt(this.mCategoryId);
		dest.writeString(this.mSubText);
		dest.writeString(this.mIcon);
		dest.writeString(this.mURL);
		dest.writeString(this.mSeatNumber);
	}
	
	protected PnrSeat(Parcel in)
	{
		this.mPNR = in.readString();
		this.mTicketNumber = in.readString();
		this.mFlightNumber = in.readString();
		this.mHasEligibleSeats = in.readByte() != 0;
		this.mHasSeats = in.readByte() != 0;
		this.mCategoryId = in.readInt();
		this.mSubText = in.readString();
		this.mIcon = in.readString();
		this.mURL = in.readString();
		this.mSeatNumber = in.readString();
	}
	
	public static final Parcelable.Creator<PnrSeat> CREATOR = new Parcelable.Creator<PnrSeat>()
	{
		@Override
		public PnrSeat createFromParcel(Parcel source)
		{
			return new PnrSeat(source);
		}
		
		@Override
		public PnrSeat[] newArray(int size)
		{
			return new PnrSeat[size];
		}
	};
	
	public String getPNR()
	{
		return mPNR;
	}
	
	public String getTicketNumber()
	{
		return mTicketNumber;
	}
	
	public String getFlightNumber()
	{
		return mFlightNumber;
	}
	
	public boolean isHasEligibleSeats()
	{
		return mHasEligibleSeats;
	}
	
	public boolean isHasSeats()
	{
		return mHasSeats;
	}
	
	public int getCategoryId()
	{
		return mCategoryId;
	}
	
	public String getSubText()
	{
		return mSubText;
	}
	
	public String getIcon()
	{
		return mIcon;
	}
	
	public String getURL()
	{
		return mURL;
	}
	
	public String getSeatNumber()
	{
		return mSeatNumber;
	}
	
	public static Creator<PnrSeat> getCREATOR()
	{
		return CREATOR;
	}
}
