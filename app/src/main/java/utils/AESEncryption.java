package utils;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


/**
 * Created by erline.katz on 24/08/2017.
 */

public class AESEncryption
{
	private static final String RIJNDAEL = "AES";
	private static String KEY = "27323A0825226DDD316881852610DACB81210355C3117DAD83EF5EE9E8602915";
	
	private static final String AES_MODE = "AES/ECB/PKCS7Padding";
	private static final String CHARSET = "UTF-8";//
	
	
//	static
//	{
//		Security.addProvider(new cryptix.provider.Cryptix());
//	}
	
	
	public AESEncryption() throws java.security.NoSuchProviderException, java.security.NoSuchAlgorithmException, NoSuchPaddingException
	{}
	
	private static SecretKeySpec generateKey() throws NoSuchAlgorithmException, UnsupportedEncodingException {
		return new SecretKeySpec(new BigInteger(KEY,16).toByteArray(), RIJNDAEL);
	}
	
	/**
	 * Encrypt and encode message using 256-bit AES with key generated from password.
	 *
	 *
	
	 * @param message the thing you want to encrypt assumed String UTF-8
	 * @return Base64 encoded CipherText
	 * @throws GeneralSecurityException if problems occur during encryption
	 */
	public String encrypt(String message) throws GeneralSecurityException
	{
		try
		{
			final SecretKeySpec key = generateKey();
			byte[] cipherText = encrypt(key, null, message.getBytes(CHARSET));

			return bytesToHex(cipherText);
		}
		catch (UnsupportedEncodingException e)
		{
			throw new GeneralSecurityException(e);
		}
	}
	
	
	private static byte[] hexStringToByteArray(String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
					+ Character.digit(s.charAt(i+1), 16));
		}
		return data;
	}
	
	private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
	private static String bytesToHex(byte[] bytes)
	{
		char[] hexChars = new char[bytes.length * 2];
		for ( int j = 0; j < bytes.length; j++ )
		{
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}
	
	
	/**
	 * More flexible AES decrypt that doesn't encode
	 *
	 * @param key AES key typically 128, 192 or 256 bit
	 * @param iv Initiation Vector
	 * @param decodedCipherText in bytes (assumed it's already been decoded)
	 * @return Decrypted message cipher text (not encoded)
	 * @throws GeneralSecurityException if something goes wrong during encryption
	 */
	private static byte[] decrypt(final SecretKeySpec key, final byte[] iv, final byte[] decodedCipherText) throws GeneralSecurityException
	{
		final Cipher cipher = Cipher.getInstance(AES_MODE);
		IvParameterSpec ivSpec = new IvParameterSpec(iv);
		cipher.init(Cipher.DECRYPT_MODE, key);//, ivSpec
		byte[] decryptedBytes = cipher.doFinal(decodedCipherText);
		return decryptedBytes;
	}
	
	public String decrypt(String base64EncodedCipherText) throws GeneralSecurityException
	{
		try
		{
			final SecretKeySpec key = generateKey();
			byte[] decodedCipherText = hexStringToByteArray(base64EncodedCipherText);
			byte[] decryptedBytes = decrypt(key, "".getBytes(), decodedCipherText);
			String message = new String(decryptedBytes, CHARSET);
			return message;
		}
		catch (UnsupportedEncodingException e)
		{
			throw new GeneralSecurityException(e);
		}
	}
	
	/**
	 * More flexible AES encrypt that doesn't encode
	 * @param key AES key typically 128, 192 or 256 bit
	 * @param iv Initiation Vector
	 * @param message in bytes (assumed it's already been decoded)
	 * @return Encrypted cipher text (not encoded)
	 * @throws GeneralSecurityException if something goes wrong during encryption
	 */
	private static byte[] encrypt(final SecretKeySpec key, final byte[] iv, final byte[] message)
			throws GeneralSecurityException
	{
		final Cipher cipher = Cipher.getInstance(AES_MODE);
//		IvParameterSpec ivSpec = new IvParameterSpec(iv);
		cipher.init(Cipher.ENCRYPT_MODE, key);//, ivSpec
		byte[] cipherText = cipher.doFinal(message);
		return cipherText;
	}
	
	
}
