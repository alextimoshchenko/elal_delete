package webServices.responses.responseGetUserPNRs;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Date;

public class GetUserPNRsContent
{
	@JsonProperty("PNRCheckins")
	private ArrayList<GetUserPNRsContentPNRCheckins> mPNRCheckins;
	@JsonProperty("IsActive")
	private String mIsActive;
	@JsonProperty("PNRSeats")
	private ArrayList<GetUserPNRsContentPNRSeats> mPNRSeats;
	@JsonProperty("IsGroupPnr")
	private String mIsGroupPnr;
	@JsonProperty("PNRFlights")
	private ArrayList<GetUserPNRsContentPNRFlights> mPNRFlights;
	@JsonProperty("CreateDate")
	private Date mCreateDate;
	@JsonProperty("CurrencyCode")
	private String mCurrencyCode;
	@JsonProperty("LastModified")
	private String mLastModified;
	@JsonProperty("TicketingDate")
	private String mTicketingDate;
	@JsonProperty("PNR")
	private String mPNR;
	@JsonProperty("PNRTickets")
	private ArrayList<GetUserPNRsContentPNRTickets> mPNRTickets;
	@JsonProperty("TotalFarePaid")
	private String mTotalFarePaid;
	@JsonProperty("LastName")
	private String mLastName;
	
	public ArrayList<GetUserPNRsContentPNRCheckins> getPNRCheckins()
	{
		return mPNRCheckins;
	}
	
	public String getIsActive()
	{
		return mIsActive;
	}
	
	public ArrayList<GetUserPNRsContentPNRSeats> getPNRSeats()
	{
		return mPNRSeats;
	}
	
	public String getIsGroupPnr()
	{
		return mIsGroupPnr;
	}
	
	public ArrayList<GetUserPNRsContentPNRFlights> getPNRFlights()
	{
		return mPNRFlights;
	}
	
	public Date getCreateDate()
	{
		return mCreateDate;
	}
	
	public String getCurrencyCode()
	{
		return mCurrencyCode;
	}
	
	public String getLastModified()
	{
		return mLastModified;
	}
	
	public String getTicketingDate()
	{
		return mTicketingDate;
	}
	
	public String getPNR()
	{
		return mPNR;
	}
	
	public ArrayList<GetUserPNRsContentPNRTickets> getPNRTickets()
	{
		return mPNRTickets;
	}
	
	public String getTotalFarePaid()
	{
		return mTotalFarePaid;
	}
	
	public String getLastName()
	{
		return mLastName;
	}
	
	public GetUserPNRsContent()
	{
	}
}
