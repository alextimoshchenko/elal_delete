package ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import global.IntentExtras;
import il.co.ewave.elal.R;
import interfaces.IAddFlightListener;
import utils.errors.ErrorsHandler;
import utils.errors.ServerError;
import webServices.controllers.MainController;
import webServices.global.RequestStringBuilder;
import webServices.requests.RequestPairGroupPNRToUser;
import webServices.requests.RequestRetrievePNR;
import webServices.responses.ResponsePairGroupPnrToUser;
import webServices.responses.ResponseRetrievePNR;

/**
 * Created with care by Shahar Ben-Moshe on 10/01/17.
 */

public class AddNewFlightFragmentOld extends EWBaseFragment implements Response.ErrorListener
{
	private Button mBtnPerformCheckIn;
	private Button mBtnSaveBoardingPass;
	private EditText mEtOrderNumber;
	private EditText mEtFamilyName;
	private EditText mEtTicketNumber;
	private TextView mTvValidation;
	private Button mBtnContinue;
	private TextView mTvCancelCheckIn;
	private LinearLayout llAddNewFlightCenterView;
	private LinearLayout llFlightTicketNumberCenterView;
	private IAddFlightListener mAddFlightListener;
	private boolean mShouldShowTicketNumberCenterView;
	private boolean mResult = false;
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_add_new_flight_layout_old);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		getIntentExtras();
		initReference();
		setListeners();
		setCenterViewGui();
		setErrorMessage("");
	}
	
	private void getIntentExtras()
	{
		if (getArguments() != null)
		{
			mAddFlightListener = (IAddFlightListener) getArguments().getSerializable(IntentExtras.ADD_FLIGHT_LISTENER);
		}
	}
	
	private void initReference()
	{
		mBtnPerformCheckIn = (Button) getActivity().findViewById(R.id.btn_addNewFlight_PerformCheckIn);
		mBtnSaveBoardingPass = (Button) getActivity().findViewById(R.id.btn_addNewFlight_SaveBoardingPass);
		llAddNewFlightCenterView = (LinearLayout) getActivity().findViewById(R.id.ll_addNewFlight_AddFlight);
		llFlightTicketNumberCenterView = (LinearLayout) getActivity().findViewById(R.id.ll_addNewFlight_GroupPnr);
		mEtOrderNumber = (EditText) getActivity().findViewById(R.id.et_addNewFlight_OrderNumber);
		mEtFamilyName = (EditText) getActivity().findViewById(R.id.et_addNewFlight_FamilyName);
		mEtTicketNumber = (EditText) getActivity().findViewById(R.id.et_addNewFlight_FlightTicketNumber);
		mTvValidation = (TextView) getActivity().findViewById(R.id.tv_addNewFlight_validation);
		mBtnContinue = (Button) getActivity().findViewById(R.id.btn_addNewFlight_Continue);
		mTvCancelCheckIn = (TextView) getActivity().findViewById(R.id.tv_addNewFlight_CancelCheckIn);
	}
	
	private void setListeners()
	{
		mBtnPerformCheckIn.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				onPerformCheckInClick();
			}
		});
		
		mBtnSaveBoardingPass.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				onSaveBoardingPassClick();
			}
		});
		
		mBtnContinue.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				onContinueClick();
			}
		});
		
		mTvCancelCheckIn.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(final View v)
			{
				onCancelCheckInClick();
			}
		});
	}
	
	private void setCenterViewGui()
	{
		llAddNewFlightCenterView.setVisibility(mShouldShowTicketNumberCenterView ? View.GONE : View.VISIBLE);
		llFlightTicketNumberCenterView.setVisibility(mShouldShowTicketNumberCenterView ? View.VISIBLE : View.GONE);
	}
	
	private void onPerformCheckInClick()
	{
		tryGoToWebFragment(RequestStringBuilder.getCheckInUrlByLanguage());
	}
	
	private void onSaveBoardingPassClick()
	{
		tryGoToWebFragment(RequestStringBuilder.getSaveBoardingPassUrlByLanguage());
	}
	
	private void onContinueClick()
	{
		setErrorMessage("");
		
		if (mShouldShowTicketNumberCenterView)
		{
			if (!TextUtils.isEmpty(mEtTicketNumber.getText().toString()))
			{
				trySetProgressDialog(true);
				getService().getController(MainController.class)
				            .PairGroupPnrToUser(new RequestPairGroupPNRToUser(mEtOrderNumber.getText().toString().toUpperCase(), mEtTicketNumber.getText()
				                                                                                                                                .toString()), new Response.Listener<ResponsePairGroupPnrToUser>()
				            {
					            @Override
					            public void onResponse(final ResponsePairGroupPnrToUser response)
					            {
						            setSuccessfulResult();
					            }
				            }, this);
			}
		}
		else
		{
			if (isDataValidAndShowErrorMessageIfNeeded() && isServiceConnected())
			{
				trySetProgressDialog(true);
				getService().getController(MainController.class)
				            .RetrievePnr(new RequestRetrievePNR(mEtOrderNumber.getText().toString().toUpperCase(), mEtFamilyName.getText().toString()), new Response.Listener<ResponseRetrievePNR>()
				            {
					            @Override
					            public void onResponse(final ResponseRetrievePNR response)
					            {
						            onRetrievePnrResponse(response);
					            }
				            }, this);
			}
		}
	}
	
	private boolean isDataValidAndShowErrorMessageIfNeeded()
	{
		boolean result = false;
		
		if (TextUtils.isEmpty(mEtOrderNumber.getText().toString()))
		{
			setErrorMessage(getString(R.string.please_enter_order_number));
		}
		else if (TextUtils.isEmpty(mEtFamilyName.getText().toString()))
		{
			setErrorMessage(getString(R.string.please_enter_family_name));
		}
		else
		{
			setErrorMessage("");
			result = true;
		}
		
		return result;
	}
	
	private void setErrorMessage(final String iTextToShow)
	{
		mTvValidation.setText(iTextToShow == null ? "" : iTextToShow);
	}
	
	private void onRetrievePnrResponse(final ResponseRetrievePNR iResponse)
	{
		if (iResponse != null && iResponse.getContent() != null && iResponse.getContent().isGroupPNR())
		{
			trySetProgressDialog(false);
			switchToPairGroupPnrToUserView();
		}
		else
		{
			setSuccessfulResult();
		}
	}
	
	private void onCancelCheckInClick()
	{
		tryGoToWebFragment(RequestStringBuilder.getCancelCheckInUrlByLanguage());
	}
	
	private void setSuccessfulResult()
	{
		trySetProgressDialog(false);
		mResult = true;
		tryPopFragment();
	}
	
	@Override
	public void onDestroy()
	{
		trySetAddFlightResult();
		
		super.onDestroy();
	}
	
	private void trySetAddFlightResult()
	{
		if (mAddFlightListener != null)
		{
			mAddFlightListener.addFlightListener(mResult);
		}
	}
	
	private void switchToPairGroupPnrToUserView()
	{
		mShouldShowTicketNumberCenterView = true;
		setCenterViewGui();
	}
	
	@Override
	public void onErrorResponse(final VolleyError error)
	{
		if (error != null && error instanceof ServerError && ((utils.errors.ServerError) error).getErrorCode() == utils.errors.ServerError.eServerError.LognetPNRNotFound.getErrorCode())
		{
			trySetProgressDialog(false);
			setErrorMessage(((utils.errors.ServerError) error).getErrorMessage());
		}
		else
		{
			ErrorsHandler.tryShowServiceErrorDialog(error, getActivity());
		}
	}
}
