package ui.fragments.myFlights;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TabHost;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.NetworkImageView;
import com.google.android.gms.tagmanager.DataLayer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Objects;

import global.ElAlApplication;
import global.IntentExtras;
import global.UserData;
import il.co.ewave.elal.R;
import interfaces.IDestinationListener;
import ui.activities.EWBaseActivity;
import ui.activities.EWBaseDrawerActivity;
import ui.activities.MyFlightsActivity;
import ui.adapters.MyTripFlightsAdapter;
import ui.customWidgets.ChangeDirectionTableRow;
import ui.customWidgets.CustomLinearLayoutManager;
import ui.customWidgets.FlightStatusIndicator;
import ui.fragments.EWBaseFragment;
import ui.fragments.MyDocumentsFragment;
import ui.fragments.ToDoFragment;
import ui.fragments.ToKnowFragment;
import ui.fragments.ToSeeFragment;
import ui.fragments.UsefulInfoFragment;
import utils.errors.ErrorsHandler;
import utils.errors.LocalError;
import utils.global.AppUtils;
import utils.global.DateTimeUtils;
import utils.global.FlightUtils;
import utils.global.googleAnalitics.googleTagManager.TagManagerImplementation;
import utils.global.myFlightsObjects.MyFlightsObject;
import utils.managers.TabsBackStackManager;
import webServices.controllers.MainController;
import webServices.global.RequestStringBuilder;
import webServices.global.ePnrFlightType;
import webServices.requests.RequestGetAppContents;
import webServices.requests.RequestGetWeather;
import webServices.responses.ResponseGetAppContents;
import webServices.responses.ResponseGetPnrByNum;
import webServices.responses.ResponseGetUserCheckList;
import webServices.responses.ResponseGetWeather;
import webServices.responses.UserCheckList.UserCheckList;
import webServices.responses.getAppData.AppContent;
import webServices.responses.getMultiplePNRDestinations.DestinationListItem;
import webServices.responses.getMultiplePNRDestinations.GetMultiplePNRDestinations;
import webServices.responses.getWeather.Weather;
import webServices.responses.responseGetUserActivePNRs.PnrFlight;
import webServices.responses.responseGetUserActivePNRs.PnrTicket;
import webServices.responses.responseGetUserActivePNRs.UserActivePnr;

import static utils.global.AppUtils.eFlightStatus.AfterBackToOrigination;
import static utils.global.AppUtils.eFlightStatus.DepartedNotArrivedToDestination;
import static utils.global.AppUtils.eFlightStatus.LessThan48HoursToDeparture;
import static utils.global.AppUtils.eFlightStatus.LessThanDayToDeparture;
import static utils.global.AppUtils.eFlightStatus.LessThanDayToOrigination;
import static utils.global.AppUtils.eFlightStatus.MoreThanDayToOrigination;

/**
 * Created by Avishay.Peretz on 05/07/2017.
 */

public class MyFlightFragmentNew2 extends EWBaseFragment implements TabsBackStackManager.ITabsBackStackManagerHandler, TabsBackStackManager.IOnTabChangeListener, Response.ErrorListener, IDestinationListener
{
	private static final String TAG = MyFlightFragmentNew2.class.getSimpleName();
	private static final int DETAILS_EXPAND_COLLAPSE_ANIMATION_SPEED = 750;
	private static final int SCROLL_DURATION = 800;
	
	private RecyclerView mRvTripFlights;
	private CustomLinearLayoutManager mRvLayoutManager;
	private FrameLayout mFlMainContainer;
	private ScrollView mSvRoot;
	private FrameLayout mFlTopContainer;
	private TabHost mTabHost;
	private FrameLayout mFlTabsContainer;
	private TabsBackStackManager mTabsBackStackManager;
	private NetworkImageView mNivTopRootBackground;
	private FlightStatusIndicator mBtnFlightStatusReady;
	private FlightStatusIndicator mBtnFlightStatusAtDestination;
	private FlightStatusIndicator mBtnFlightStatusMyTrip;
	private TextView mTvTimeTitle;
	private TextView mTvTimeLeftCounterHours, mTvTimeLeftCounterMinutes, mTvTimeLeftCounterSeconds;
	private View mLlTimeLeftCounter;
	private TextView mTvOrigin, mTvOriginBeyond;
	private ImageView mIvClose;
	//	private ImageView mIvPlane, mIvPlaneBeyond;
	private TextView mTvDestination, mTvDestinationBeyond;
	private TextView mTvDepartureDateTime, mTvDepartureDateTimeBeyond;
	private TextView mTvFlightDuration, mTvFlightDurationBeyond;
	private LinearLayout mLilFlightDuration, mLilFlightDurationBeyond;
	private TextView mTvArrivalDateTime, mTvArrivalDateTimeBeyond;
	private TextView mTvBoardingTime;
	private TextView mTvGate;
	private Button mBtnCheckIn;
	private TextView mTvFlightDetailsNumber, mTvFlightDetailsNumberBeyond;
	private Button mBtnFlightDetails;
	private View mLilCheckIn;
	private View mLlFlightBeyond;
	private View mLlFlightBoarding;
	private View mLlFlightLayover;
	private TextView mTvFlightLayover;
	private boolean mShowDestinationTabs;
	private ImageView mIvBackArrow;
	private MyTripFlightsAdapter mMyTripFlightsAdapter;
	private ArrayList<DestinationListItem> mDestinationsList = new ArrayList<>();
	//	private ExpandCollapseAnimation mExpandCollapseAnimation;
	private UserActivePnr mUserActivePnr;
	private ArrayList<AppContent> mAppContent;
	private AppUtils.eFlightStatus mFlightStatus;
	private UserCheckList mUserCheckList;
	private Weather mWeather;
	private boolean mShouldScroll = false;
	private boolean mDidRequestData = false;
	//	private PnrFlight mFirstFlight, nLastFlight;
	private DestinationListItem mCurrentInspectedFlight;
	private boolean mMyFlightVisible = true;
	private PnrFlight mCurrentFlight;
	private String mCurrentFlightNumber;
	private LinearLayout mRlRefreshPageContainer;
	private Button mBtnRefreshPage;
	private LinearLayout mLilFlightContainer;
	private ChangeDirectionTableRow mCdtTerminalBeyondRow, mCdtTerminalRow;
	private TextView mTvOriginTerminal, mTvDestinationTerminal, mTvOriginTerminalBeyond, mTvDestinationTerminalBeyond;
	
	public static Bundle createBundleForFragment(@NonNull final UserActivePnr iUserActivePnr, GetMultiplePNRDestinations iDestinationsList)
	{
		Bundle result = new Bundle();
		
		result.putParcelable(IntentExtras.USER_ACTIVE_PNR, iUserActivePnr);
		if (iDestinationsList != null)
		{
			result.putParcelable(IntentExtras.PNR_DESTINATIONS, iDestinationsList);
		}
		
		return result;
	}
	
	@Override
	public void onCreate(@Nullable final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_my_flight_layout_new2);
	}
	
	@Override
	public void onViewCreatedOnce(final View view, @Nullable final Bundle savedInstanceState)
	{
		getIntentExtras();
		setFlightStatus();
		initReference();
		setStatusBarItems();
		
		//		initTabs();
		
		initGui();
		setViewByStatus(view);
		
		//		Log.d(TAG, "called init tabs from onViewCreatedOnce");
		//		initTabs();
		
		setListeners();
		mSvRoot.requestFocus(View.FOCUS_UP);
		mSvRoot.fullScroll(View.FOCUS_UP);
		
		pushScreenOpenEvent("Manage Flight");
	}
	
	@Override
	public void onServiceConnected()
	{
		getDataIfNeeded();
	}
	
	private void checkConnectionTo2000()
	{
		//		if (mCurrentFlight != null && mCurrentFlight.isError())
		//		{
		//			mLlTimeLeftCounter.setVisibility(View.GONE);
		//
		//			showRefreshPageDialog(true);
		//		}
		
		
		String errorFlightNumber = mUserActivePnr.getErrorFlightNumber();
		if (!TextUtils.isEmpty(errorFlightNumber))
		{
			mLlTimeLeftCounter.setVisibility(View.GONE);
			
			showRefreshPageDialog(true, errorFlightNumber);
		}
		
	}
	
	private void showRefreshPageDialog(boolean iShowRefreshPageDialog)
	{
		if (iShowRefreshPageDialog)
		{
			mRlRefreshPageContainer.setVisibility(View.VISIBLE);
			mBtnRefreshPage.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					getPnrByNum(mUserActivePnr.getPnr(), mCurrentFlightNumber);
				}
			});
			mTabHost.setClickable(false);
		}
		else
		{
			mRlRefreshPageContainer.setVisibility(View.GONE);
			mTabHost.setClickable(true);
		}
	}
	
	private void showRefreshPageDialog(boolean iShowRefreshPageDialog, final String iErrorFlightNumber)
	{
		if (iShowRefreshPageDialog)
		{
			mRlRefreshPageContainer.setVisibility(View.VISIBLE);
			mBtnRefreshPage.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					getPnrByNum(mUserActivePnr.getPnr(), iErrorFlightNumber);
				}
			});
			mTabHost.setClickable(false);
		}
		else
		{
			mRlRefreshPageContainer.setVisibility(View.GONE);
			mTabHost.setClickable(true);
		}
	}
	
	private void setStatusBarItems()
	{
		mBtnFlightStatusAtDestination.setText(R.string.arrived_at_destination);
		mBtnFlightStatusAtDestination.setIndicator(FlightStatusIndicator.DISABLED);
		
		mBtnFlightStatusReady.setText(R.string.ready);
		mBtnFlightStatusReady.setIndicator(FlightStatusIndicator.DISABLED);
		
		mBtnFlightStatusMyTrip.setText(R.string.my_trip);
		mBtnFlightStatusMyTrip.setIndicator(FlightStatusIndicator.ACTIVE);
	}
	
	private void setViewByStatus(View view)
	{
		String currentActivePnrFromSplash = ((MyFlightsActivity) getActivity()).getCurrentActiveFlightPnr();
		
		if (isMultipleTrip() && !(currentActivePnrFromSplash != null && !TextUtils.isEmpty(currentActivePnrFromSplash)))
		{
			setMyTripLayout(view);
		}
		else
		{
			setMyFlightLayout(view);
		}
	}
	
	private void setFlightStatus()
	{
		mFlightStatus = FlightUtils.getFlightStatusByDestination(mUserActivePnr);
	}
	
	private void getIntentExtras()
	{
		if (getArguments() != null)
		{
			mUserActivePnr = MyFlightsObject.getInstance().getCurrentUserActivePnr();
			
			GetMultiplePNRDestinations multiplePNRDestinations = MyFlightsObject.getInstance().getCurrentPnrDestinations();
			if (multiplePNRDestinations != null)
			{
				mDestinationsList = multiplePNRDestinations.getmPNRDestinationsList();
				setCurrentFlightAndGetPosition();
			}
		}
	}
	
	private int setCurrentFlightAndGetPosition()
	{
		if (mUserActivePnr != null && mDestinationsList != null && !mDestinationsList.isEmpty())
		{
			if (mUserActivePnr.getIsMultiple() && mDestinationsList != null && mDestinationsList.size() > 0)
			{
				for (int i = 0 ; i < mDestinationsList.size() ; i++)
				{
					if (mDestinationsList.get(i).isCurrentFlight())
					{
						mCurrentInspectedFlight = mDestinationsList.get(i);
						mCurrentInspectedFlight.setmFlightType(mUserActivePnr.getFlightDirectionByFlightNum(mCurrentInspectedFlight.getmDestination().getFlightNumber()));
						return i;
					}
				}
			}
			else
			{
				mCurrentFlightNumber = mUserActivePnr.getCurrentFlightNum();
				if (mDestinationsList != null)
				{
					for (int i = 0 ; i < mDestinationsList.size() ; i++)
					{
						try
						{
							if (mDestinationsList != null && mDestinationsList.get(i) != null && mDestinationsList.get(i).getmDestination() != null && mDestinationsList.get(i)
							                                                                                                                                            .getmDestination()
							                                                                                                                                            .getFlightNumber()
							                                                                                                                                            .equals(mCurrentFlightNumber) || (mDestinationsList
									.get(i) != null && mDestinationsList.get(i).getmVia() != null && mDestinationsList.get(i).getmVia().getFlightNumber().equals(mCurrentFlightNumber)))
							{
								mCurrentInspectedFlight = mDestinationsList.get(i);
								mCurrentInspectedFlight.setmFlightType(mUserActivePnr.getFlightDirectionByFlightNum(mCurrentInspectedFlight.getmDestination().getFlightNumber()));
								return i;
							}
						}
						catch (Exception e)
						{
							Log.d("setCurrentFlight", "no flight in " + i);
							e.printStackTrace();
						}
					}
				}
			}
			
			mCurrentInspectedFlight = mDestinationsList.get(0);
			mCurrentInspectedFlight.setmFlightType(mUserActivePnr.getFlightDirectionByFlightNum(mCurrentInspectedFlight.getmDestination().getFlightNumber()));
		}
		return 0;
	}
	
	private void initFlightReferences(View lilMyFlightView)
	{
		mNivTopRootBackground = getActivity().findViewById(R.id.niv_myFlightTop_RootBackground);
		
		mLilFlightContainer = lilMyFlightView.findViewById(R.id.lilFlightContainer);
		mLlTimeLeftCounter = lilMyFlightView.findViewById(R.id.ll_myFlightTop_TimeLeftCounter);
		mTvOrigin = lilMyFlightView.findViewById(R.id.tv_myFlightTop_Origin);
		mTvOriginBeyond = lilMyFlightView.findViewById(R.id.tv_myFlightTop_Origin_beyond);
		mTvDestination = lilMyFlightView.findViewById(R.id.tv_myFlightTop_Destination);
		mTvDestinationBeyond = lilMyFlightView.findViewById(R.id.tv_myFlightTop_Destination_beyond);
		mTvDepartureDateTime = lilMyFlightView.findViewById(R.id.tv_myFlightTop_DepartureDateTime);
		mTvDepartureDateTimeBeyond = lilMyFlightView.findViewById(R.id.tv_myFlightTop_DepartureDateTime_beyond);
		mTvFlightDuration = lilMyFlightView.findViewById(R.id.tv_myFlightTop_FlightDuration);
		mLilFlightDuration = lilMyFlightView.findViewById(R.id.lil_flightDuration);
		mTvFlightDurationBeyond = lilMyFlightView.findViewById(R.id.tv_myFlightTop_FlightDuration_beyond);
		mLilFlightDurationBeyond = lilMyFlightView.findViewById(R.id.lil_flightDuration_beyond);
		mTvArrivalDateTime = lilMyFlightView.findViewById(R.id.tv_myFlightTop_ArrivalDateTime);
		mTvArrivalDateTimeBeyond = lilMyFlightView.findViewById(R.id.tv_myFlightTop_ArrivalDateTime_beyond);
		mTvBoardingTime = lilMyFlightView.findViewById(R.id.tv_myFlightTop_BoardingTime);
		mTvGate = lilMyFlightView.findViewById(R.id.tv_myFlightTop_Gate);
		mBtnCheckIn = lilMyFlightView.findViewById(R.id.btn_myFlightTop_CheckIn);
		mLilCheckIn = lilMyFlightView.findViewById(R.id.ll_myFlightTop_CheckIn);
		mTvFlightDetailsNumber = lilMyFlightView.findViewById(R.id.tv_myFlightTop_FlightDetailsNumber);
		mTvFlightDetailsNumberBeyond = lilMyFlightView.findViewById(R.id.tv_myFlightTop_FlightDetailsNumber_beyond);
		mBtnFlightDetails = lilMyFlightView.findViewById(R.id.btn_myFlightTop_FlightDetails);
		mLlFlightBeyond = lilMyFlightView.findViewById(R.id.ll_myFlightTop_Flight_beyond);
		mLlFlightBoarding = lilMyFlightView.findViewById(R.id.ll_myFlightTop_boarding);
		mLlFlightLayover = lilMyFlightView.findViewById(R.id.ll_myFlightTop_Flight_layover);
		mTvFlightLayover = lilMyFlightView.findViewById(R.id.tv_myFlightTop_Flight_layover);
		mTvTimeTitle = lilMyFlightView.findViewById(R.id.tv_myFlightTop_TimeTitle);
		mTvTimeLeftCounterMinutes = lilMyFlightView.findViewById(R.id.tv_myFlightTop_TimeLeftCounter_minutes);
		
		mCdtTerminalBeyondRow = lilMyFlightView.findViewById(R.id.cdt_TerminalBeyond_row);
		mCdtTerminalRow = lilMyFlightView.findViewById(R.id.cdt_Terminal_row);
		
		mTvOriginTerminal = lilMyFlightView.findViewById(R.id.tv_myFlightTop_OriginTerminal);
		mTvDestinationTerminal = lilMyFlightView.findViewById(R.id.tv_myFlightTop_DestinationTerminal);
		mTvOriginTerminalBeyond = lilMyFlightView.findViewById(R.id.tv_myFlightTop_OriginTerminal_beyond);
		mTvDestinationTerminalBeyond = lilMyFlightView.findViewById(R.id.tv_myFlightTop_DestinationTerminal_beyond);
		
		
		if (AppUtils.isDefaultLocaleRTL())
		{
			mTvTimeLeftCounterHours = lilMyFlightView.findViewById(R.id.tv_myFlightTop_TimeLeftCounter_3);
			mTvTimeLeftCounterSeconds = lilMyFlightView.findViewById(R.id.tv_myFlightTop_TimeLeftCounter_1);
		}
		else
		{
			mTvTimeLeftCounterHours = lilMyFlightView.findViewById(R.id.tv_myFlightTop_TimeLeftCounter_1);
			mTvTimeLeftCounterSeconds = lilMyFlightView.findViewById(R.id.tv_myFlightTop_TimeLeftCounter_3);
		}
		
	}
	
	private void initReference()
	{
		mSvRoot = getActivity().findViewById(R.id.sv_myFlight_Root_new);
		mFlTopContainer = getActivity().findViewById(R.id.fl_myFlight_TopContainer);
		mTabHost = getActivity().findViewById(android.R.id.tabhost);
		mFlTabsContainer = getActivity().findViewById(R.id.fl_myFlight_TabsContainer);
		
		mRlRefreshPageContainer = getActivity().findViewById(R.id.rl_refreshPage_container);
		mBtnRefreshPage = getActivity().findViewById(R.id.btn_refresh);
		
		
		mNivTopRootBackground = getActivity().findViewById(R.id.niv_myFlightTop_RootBackground);
		
		mBtnFlightStatusMyTrip = getActivity().findViewById(R.id.btn_myFlightTop_FlightStatusMyTrip);
		mBtnFlightStatusReady = getActivity().findViewById(R.id.btn_myFlightTop_FlightStatusReady);
		mBtnFlightStatusAtDestination = getActivity().findViewById(R.id.btn_myFlightTop_FlightStatusAtDestination);
		mIvClose = getActivity().findViewById(R.id.iv_myFlightTop_Close);
		
		mIvBackArrow = getActivity().findViewById(R.id.iv_name_screen_back_arrow);
		mFlMainContainer = getActivity().findViewById(R.id.fl_myFlight_MainContainer);
	}
	
	private void setListeners()
	{
		if (mBtnCheckIn != null)
		{
			mBtnCheckIn.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(final View v)
				{
					onCheckinClick();
				}
			});
		}
		
		if (mBtnFlightDetails != null)
		{
			mBtnFlightDetails.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(final View v)
				{
					onFlightDetailsClick();
				}
			});
		}
		
		if (mIvBackArrow != null)
		{
			mIvBackArrow.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					//					onBackPressed();
					doOnBackPressed();
				}
			});
		}
		
		if (mIvClose != null)
		{
			mIvClose.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					Log.d("mIvClose", "clicked mIvClose");
					doOnBackPressed();
				}
			});
		}
		
		if (isMultipleTrip())
		{
			setStatusMyTripOnclick();
		}
		
		
		if (AppUtils.isFlightStatusAtDestination(mFlightStatus))
		{
			if (mFlightStatus != AppUtils.eFlightStatus.AfterBackToOrigination)
			{
				setBtnFlightStatusReadyOnclick();
			}
			else
			{
				mBtnFlightStatusReady.setOnClickListener(null);
			}
		}
		else
		{
			mBtnFlightStatusAtDestination.setOnClickListener(null);
			mBtnFlightStatusReady.setOnClickListener(null);
		}
		
		
	}
	
	private void doOnBackPressed()
	{
		if (isMultipleTrip() && mMyFlightVisible)
		{
			//						setMyTripLayout(getView());
			OnBtnFlightStatusMyTripClick();
		}
		else
		{
			tryPopFragment();
		}
	}
	
	private void OnBtnFlightStatusMyTripClick()
	{
		mShowDestinationTabs = false;
		mShouldScroll = false;
		mTabsBackStackManager = null;
		
		setButtonEnabled(mBtnFlightStatusMyTrip);
		setButtonClickable(mBtnFlightStatusReady);
		
		if (AppUtils.isFlightStatusAtDestination(mFlightStatus) && !(isMultipleTrip() && mFlightStatus == MoreThanDayToOrigination && isReturnFlight()))
		{
			setButtonClickable(mBtnFlightStatusAtDestination);
		}
		else
		{
			setButtonDisabled(mBtnFlightStatusAtDestination);
		}
		
		if (mMyFlightVisible)
		{
			setMyTripLayout(getView());
		}
	}
	
	private void setMyFlightLayout(View view)
	{
		mMyFlightVisible = true;
		if (mFlMainContainer == null)
		{
			mFlMainContainer = view.findViewById(R.id.fl_myFlight_MainContainer);
		}
		final LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		LinearLayout lilMyFlightView = (LinearLayout) inflater.inflate(R.layout.my_flight_layout, null, false);
		mFlMainContainer.removeAllViewsInLayout();
		mFlMainContainer.addView(lilMyFlightView);
		
		initFlightReferences(lilMyFlightView);
		
		if (mUserActivePnr != null)
		{
			setFlightData();
		}
		
		setListeners();
		if (AppUtils.isFlightStatusAtDestination(mFlightStatus))
		{
			if (mUserActivePnr.getIsMultiple() && mFlightStatus == AppUtils.eFlightStatus.MoreThanDayToOrigination && isReturnFlight())
			{
				OnBtnFlightStatusReadyClick();
			}
			else
			{
				OnBtnFlightStatusAtDestinationClick();
			}
		}
		else
		{
			OnBtnFlightStatusReadyClick();
		}
	}
	
	private boolean isReturnFlight()
	{
		String flightNum = mCurrentFlightNumber;
		
		if (flightNum == null || flightNum.isEmpty())
		{
			flightNum = mUserActivePnr.getCurrentFlightNum();
		}
		
		//		Log.d("DirectionByFlightNum", "flightNum for isReturnFlight: " + flightNum);
		
		ePnrFlightType flightDirection = mUserActivePnr.getFlightDirectionByFlightNum(flightNum);
		return flightDirection == ePnrFlightType.RETURN;
		//		return mUserActivePnr.getReturnPNRFlights() != null && !mUserActivePnr.getReturnPNRFlights().isEmpty();
	}
	
	private void setMyTripLayout(View view)
	{
		includeTabsHeight(false);
		showTabs(false);
		
		mMyFlightVisible = false;
		mFlMainContainer = view.findViewById(R.id.fl_myFlight_MainContainer);
		final LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		LinearLayout lilMyTripView = (LinearLayout) inflater.inflate(R.layout.my_trip_layout, null, false);
		mFlMainContainer.removeAllViewsInLayout();
		mFlMainContainer.addView(lilMyTripView);
		
		initTripReferences(lilMyTripView);
		setMyTripListiners();
		setMyTripRecyclerAdapter();
		onScrollToCurrentItem(setCurrentFlightAndGetPosition());
	}
	
	private void setMyTripListiners()
	{
		if (AppUtils.isFlightStatusAtDestination(mFlightStatus) && !(isMultipleTrip() && mFlightStatus == MoreThanDayToOrigination && isReturnFlight()))
		{
			setBtnFlightStatusAtDestinationOnclick();
		}
		setBtnFlightStatusReadyOnclick();
	}
	
	private void setMyTripRecyclerAdapter()
	{
		mRvLayoutManager = new CustomLinearLayoutManager(getContext());
		mRvTripFlights.setLayoutManager(mRvLayoutManager);
		if (mMyTripFlightsAdapter == null)
		{
			mMyTripFlightsAdapter = new MyTripFlightsAdapter(mDestinationsList, getContext(), MyFlightFragmentNew2.this);
		}
		mRvTripFlights.setAdapter(mMyTripFlightsAdapter);
		mRvTripFlights.setNestedScrollingEnabled(true);
	}
	
	private void initTripReferences(View lilMyTripView)
	{
		mRvTripFlights = lilMyTripView.findViewById(R.id.rv_myTrip_Flights);
	}
	
	private void OnBtnFlightStatusReadyClick()
	{
		if (!mMyFlightVisible)
		{
			setMyFlightLayout(getView());
		}
		
		includeTabsHeight(true);
		showTabs(true);
		mShowDestinationTabs = false;
		mShouldScroll = false;
		Log.d(TAG, "called init tabs from OnBtnFlightStatusReadyClick");
		initTabs();
		
		if (isMultipleTrip())
		{
			setButtonClickable(mBtnFlightStatusMyTrip);
			setStatusMyTripOnclick();
		}
		
		setButtonEnabled(mBtnFlightStatusReady);
		
		
		/**START**/
		if (AppUtils.isFlightStatusAtDestination(mFlightStatus))
		{
			if (/*mUserActivePnr.getIsMultiple()*/
				/*&& mFlightStatus == AppUtils.eFlightStatus.MoreThanDayToOrigination && */
					!isReturnFlight())
			{
				setFlightDetailsVisibility(View.INVISIBLE);
			}
			else
			{
				setFlightDetailsVisibility(View.VISIBLE);
			}
		}
		else
		{
			setFlightDetailsVisibility(View.VISIBLE);
		}
		/***END**/
		
		
		if (AppUtils.isFlightStatusAtDestination(mFlightStatus) && !(isMultipleTrip() && mFlightStatus == MoreThanDayToOrigination && isReturnFlight()))
		{
			setButtonClickable(mBtnFlightStatusAtDestination);
			setBtnFlightStatusAtDestinationOnclick();
		}
		else
		{
			setButtonDisabled(mBtnFlightStatusAtDestination);
			mBtnFlightStatusAtDestination.setOnClickListener(null);
		}
		
		trySetProgressDialog(false);
		
		String activePnr = UserData.getInstance().getActivePnr();
		
		if (mShowDestinationTabs)
		{
			//event 63
			((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight - At destination", "Action", "At destination >> " + activePnr + " >> Lobby", "Label", "-"));
		}
		else
		{
			//event 38
			((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight" + TagManagerImplementation.TIME_SCOPE, "Action", TagManagerImplementation.TIME_SCOPE + activePnr + ">> Lobby", "Label", "-"));
		}
	}
	
	private void setFlightDetailsVisibility(int iVisibility)
	{
		// NOTE avishay 10.4.18 : for MARCH version, always show the flight details
//		if (mLilFlightContainer != null)
//		{
//			mLilFlightContainer.setVisibility(iVisibility);
//		}
//		if (mBtnFlightDetails != null)
//		{
//			mBtnFlightDetails.setVisibility(iVisibility);
//		}
	}
	
	private void setBtnFlightStatusAtDestinationOnclick()
	{
		mBtnFlightStatusAtDestination.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				OnBtnFlightStatusAtDestinationClick();
			}
		});
	}
	
	private void OnBtnFlightStatusAtDestinationClick()
	{
		if (!mMyFlightVisible)
		{
			setMyFlightLayout(getView());
		}
		
		includeTabsHeight(true);
		showTabs(true);
		mShowDestinationTabs = true;
		mShouldScroll = false;
		Log.d(TAG, "called init tabs from OnBtnFlightStatusAtDestinationClick");
		initTabs();
		
		setButtonEnabled(mBtnFlightStatusAtDestination);
		setFlightDetailsVisibility(View.INVISIBLE);
		
		if (isMultipleTrip())
		{
			setButtonClickable(mBtnFlightStatusMyTrip);
		}
		
		if (mFlightStatus != AfterBackToOrigination)
		{
			setButtonClickable(mBtnFlightStatusReady);
			setBtnFlightStatusReadyOnclick();
		}
		else
		{
			setButtonDisabled(mBtnFlightStatusReady);
			mBtnFlightStatusReady.setOnClickListener(null);
		}
		
		
		setStatusMyTripOnclick();
	}
	
	private void setStatusMyTripOnclick()
	{
		mBtnFlightStatusMyTrip.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				OnBtnFlightStatusMyTripClick();
			}
		});
	}
	
	private void setBtnFlightStatusReadyOnclick()
	{
		mBtnFlightStatusReady.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				OnBtnFlightStatusReadyClick();
			}
		});
	}
	
	private void setButtonClickable(FlightStatusIndicator iBtnView)
	{
		iBtnView.setEnabled(true);
		iBtnView.setIndicator(FlightStatusIndicator.AVAILABLE);
	}
	
	private void setButtonDisabled(FlightStatusIndicator iBtnView)
	{
		iBtnView.setEnabled(false);
		iBtnView.setIndicator(FlightStatusIndicator.DISABLED);
	}
	
	private void setButtonEnabled(FlightStatusIndicator iBtnView)
	{
		iBtnView.setEnabled(true);
		iBtnView.setIndicator(FlightStatusIndicator.ACTIVE);
	}
	
	private void setBtnFlightStatusAtDestinationEnabled()
	{
		setButtonEnabled(mBtnFlightStatusAtDestination);
		setButtonClickable(mBtnFlightStatusReady);
		setButtonClickable(mBtnFlightStatusMyTrip);
	}
	
	private void onCheckinClick()
	{
		tryGoToWebFragment(RequestStringBuilder.getCheckinUrlWithParamsByLanguage(mUserActivePnr.getPnr(), mUserActivePnr.getPassengers().get(0), mUserActivePnr.isGroupPnr()));
		
		//event 39
		String activePnr = UserData.getInstance().getActivePnr();
		((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight" + TagManagerImplementation.TIME_SCOPE, "Action", TagManagerImplementation.TIME_SCOPE + activePnr + ">> Lobby", "Label", "Online " + "check in"));
	}
	
	private void onFlightDetailsClick()
	{
		tryGoToFragmentByFragmentClass(FlightDetailsFragment.class, FlightDetailsFragment.createBundleForFragment(mUserActivePnr, mCurrentInspectedFlight), true);
		
		String activePnr = UserData.getInstance().getActivePnr();
		
		if (mShowDestinationTabs)
		{
			//event 64
			((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight - At destination", "Action", "At destination >> " + activePnr + " >> Lobby", "Label", "More about the flight"));
		}
		else
		{
			//event 40
			((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight" + TagManagerImplementation.TIME_SCOPE, "Action", TagManagerImplementation.TIME_SCOPE + activePnr + ">> Lobby", "Label", "More " + "about the " + "flight"));
		}
	}
	
	private void initTabs()
	{
		mShouldScroll = false;
		
		ArrayList<TabsBackStackManager.TabItem> tabItems = new ArrayList<>();
		
		int defaultPosition = 0;
		mTabHost.clearFocus();
		
		String iata = mUserActivePnr.getCurrentFlightDestination();
		
		Bundle bundle;
		bundle = null;
		
		try
		{
			if (mShowDestinationTabs)
			{
				if (mCurrentFlight != null && mCurrentFlight.geteNumDirection() == ePnrFlightType.RETURN)
				{
					if (mAppContent != null)
					{
						bundle = ToKnowFragment.createBundleForFragment(mAppContent);
					}
					
					//One time I got the eception here java.lang.NullPointerException: Attempt to invoke virtual method 'java.lang.String android.content.Context.getString(int)' on a null object reference
					//				at ui.fragments.myFlights.MyFlightFragmentNew2.initTabs(MyFlightFragmentNew2.java:750)
					
					try
					{
						tabItems.add(new TabsBackStackManager.TabItem(getContext().getString(R.string.to_know), ToKnowFragment.class, bundle, R.mipmap.b_icon_1));
					}
					catch (NullPointerException ignore)
					{
					}
				}
				else
				{
					bundle = UsefulInfoFragment.createBundleForFragment(iata, mWeather, mCurrentFlight, mUserActivePnr.getCurrentFlightDestinationName(), mUserActivePnr.getCurrentFlightDestinationInfo());
					tabItems.add(new TabsBackStackManager.TabItem(getContext().getString(R.string.useful_info), UsefulInfoFragment.class, bundle, R.mipmap.b_icon_1));
				}
				
				bundle = null;
				
				if (!isDepartureFlight() && mUserActivePnr.getReturnPNRFlights() != null && !mUserActivePnr.getReturnPNRFlights().isEmpty())
				{
					iata = mUserActivePnr.getCurrentItemFlightAtPositionDepartureOrReurnOrNull(0, ePnrFlightType.RETURN).getOrigination();
				}
				if (!TextUtils.isEmpty(iata))
				{
					bundle = ToSeeFragment.createBundleForFragment(iata);
				}
				
				try
				{
					tabItems.add(new TabsBackStackManager.TabItem(getContext().getString(R.string.to_see), ToSeeFragment.class, bundle, R.mipmap.b_icon_3));
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				
				
				bundle = MyDocumentsFragment.createBundleForFragment(mUserActivePnr, true, getFlightsCount());
				tabItems.add(new TabsBackStackManager.TabItem(getContext().getString(R.string.to_save), MyDocumentsFragment.class, bundle, R.mipmap.icon_to_save, true));
			}
			else
			{
				if (mAppContent != null)
				{
					bundle = ToKnowFragment.createBundleForFragment(mAppContent);
				}
				
				tabItems.add(new TabsBackStackManager.TabItem(getContext().getString(R.string.to_know), ToKnowFragment.class, bundle, R.mipmap.b_icon_1));
				
				
				bundle = null;
				
				//			if (!isReturnFlight() || isDepartureFlight() || mFlightStatus == MoreThanDayToOrigination)
				//			if (!isReturnFlight() || isDepartureFlight())
				if (!isReturnFlight())
				{
					if (mUserActivePnr != null && mUserCheckList != null)
					{
						bundle = ToDoFragment.createBundleForFragment(mUserActivePnr, mUserCheckList);
					}
					tabItems.add(new TabsBackStackManager.TabItem(getContext().getString(R.string.to_do), ToDoFragment.class, bundle, R.mipmap.b_icon_2));
				}
				
				bundle = null;
				
				if (!isDepartureFlight() && mUserActivePnr.getDeparturePNRFlights() != null && !mUserActivePnr.getDeparturePNRFlights().isEmpty())
				{
					iata = mUserActivePnr.getCurrentItemFlightAtPositionDepartureOrReurnOrNull(0, ePnrFlightType.RETURN).getOrigination();
				}
				if (!TextUtils.isEmpty(iata))
				{
					bundle = ToSeeFragment.createBundleForFragment(iata);
				}
				tabItems.add(new TabsBackStackManager.TabItem(getActivity()/*getContext()*/.getString(R.string.to_see), ToSeeFragment.class, bundle, R.mipmap.b_icon_3));
				
				bundle = MyDocumentsFragment.createBundleForFragment(mUserActivePnr, true, getFlightsCount());
				tabItems.add(new TabsBackStackManager.TabItem(getActivity()/*getContext()*/.getString(R.string.to_save), MyDocumentsFragment.class, bundle, R.mipmap.icon_to_save, true));
			}
			
			if (AppUtils.isDefaultLocaleRTL())
			{
				Collections.reverse(tabItems);
				defaultPosition = tabItems.size() - 1;
			}
			
			mTabHost.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener()
			{
				
				@Override
				public void onViewAttachedToWindow(View v)
				{
					mTabHost.getViewTreeObserver().removeOnTouchModeChangeListener(mTabHost);
				}
				
				@Override
				public void onViewDetachedFromWindow(View v)
				{
				}
			});
			
			if (!isAdded())
			{
				return;
			}
			
			if (mTabsBackStackManager != null)
			{
				Log.d(TAG, "doing updateTabsBackStackManager");
				mTabsBackStackManager.updateTabsBackStackManager(getActivity(), mFlTabsContainer, mTabHost, tabItems, getChildFragmentManager(), defaultPosition, this, mFlTopContainer, getContext(), isMultipleTrip());
			}
			else
			{
				Log.d(TAG, "doing new TabsBackStackManager");
				mTabsBackStackManager = new TabsBackStackManager(getActivity(), mFlTabsContainer, mTabHost, tabItems, getChildFragmentManager(), defaultPosition, this, mFlTopContainer, getContext());
			}
			
			mSvRoot.requestFocus(View.FOCUS_UP);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private int getFlightsCount()
	{
		int counter = 0;
		if (mDestinationsList != null && isMultipleTrip())
		{
			for (DestinationListItem item : mDestinationsList)
			{
				if (item.getmVia() != null)
				{
					counter++;
				}
				counter++;
			}
			
			return counter;
		}
		else
		{
			return mUserActivePnr.getDeparturePNRFlights().size() + mUserActivePnr.getReturnPNRFlights().size();
		}
	}
	
	private void getDataIfNeeded()
	{
		if (isServiceConnected() && !mDidRequestData)
		{
			getAppContent();
			GetWeather();
			getUserCheckList();
			
			mDidRequestData = true;
		}
	}
	
	private void doOnDataReceived()
	{
		if (isAllDataReceivedFromWebServices())
		{
			mShowDestinationTabs = AppUtils.isFlightStatusAtDestination(mFlightStatus);
			trySetProgressDialog(false);
			Log.d(TAG, "called init tabs from doOnDataReceived");
			initTabs();
		}
	}
	
	private boolean isAllDataReceivedFromWebServices()
	{
		return mAppContent != null && mWeather != null && mUserCheckList != null;
	}
	
	private void getAppContent()
	{
		if (isServiceConnected() && (mAppContent == null || mAppContent.isEmpty()) && mUserActivePnr != null)
		{
			trySetProgressDialog(true);
			
			//			String flightNumber = mCurrentFlight != null ? AppUtils.nullSafeCheckString(mCurrentFlight.getFlightNumber()) : "";
			String flightNumber = mCurrentFlightNumber;
			//			if (isMultipleTrip() && mCurrentInspectedFlight != null)
			if (flightNumber == null && mCurrentInspectedFlight != null)
			{
				flightNumber = mCurrentInspectedFlight.getmDestination().getFlightNumber();
			}
			
			getService().getController(MainController.class).getGetAppContents(new RequestGetAppContents(mUserActivePnr.getPnr(), flightNumber), new Response.Listener<ResponseGetAppContents>()
			{
				@Override
				public void onResponse(final ResponseGetAppContents iResponse)
				{
					trySetProgressDialog(false);
					if (iResponse != null)
					{
						mAppContent = iResponse.getContent();
					}
					
					initAppContentIfNeeded();
					doOnDataReceived();
				}
			}, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError error)
				{
					trySetProgressDialog(false);
					initAppContentIfNeeded();
					
					//avishay
					doOnDataReceived();
				}
			});
		}
	}
	
	private void initAppContentIfNeeded()
	{
		if (mAppContent == null)
		{
			mAppContent = new ArrayList<>();
		}
		
	}
	
	private void GetWeather()
	{
		if (isServiceConnected() && mUserActivePnr != null /* && !AppUtils.isFlightStatusAtDestination(mFlightStatus)*/)
		{
			trySetProgressDialog(true);
			
			getService().getController(MainController.class)
			            .GetWeather(new RequestGetWeather(UserData.getInstance().getUserID(), mUserActivePnr.getCurrentFlightDestination()), new Response.Listener<ResponseGetWeather>()
			            {
				            @Override
				            public void onResponse(final ResponseGetWeather response)
				            {
					            if (response != null)
					            {
						            mWeather = response.getContent();
					            }
					
					            initWeatherIfNeeded();
					            doOnDataReceived();
				            }
			            }, new Response.ErrorListener()
			            {
				            @Override
				            public void onErrorResponse(final VolleyError error)
				            {
					            initWeatherIfNeeded();
					            doOnDataReceived();
				            }
			            });
		}
	}
	
	private void initWeatherIfNeeded()
	{
		if (mWeather == null)
		{
			mWeather = new Weather();
		}
	}
	
	private void getUserCheckList()
	{
		if (isServiceConnected() && mUserActivePnr != null)
		{
			trySetProgressDialog(true);
			
			getService().getController(MainController.class).getUserCheckList(mUserActivePnr.generateUserCheckListRequest(), new Response.Listener<ResponseGetUserCheckList>()
			{
				@Override
				public void onResponse(final ResponseGetUserCheckList iResponse)
				{
					if (iResponse != null)
					{
						Integer timeDifference = DateTimeUtils.HOURS_OF_A_DAY * 30;
						if (mUserActivePnr != null)
						{
							PnrFlight pnrFlight = mUserActivePnr.getCurrentItemFlightAtPositionDepartureOrReurnOrNull(0, ePnrFlightType.DEPARTURE);
							
							Date departureDate = DateTimeUtils.convertDateToDateIgnoreGMT(pnrFlight.getDepartureDate());
							Date currentDate = DateTimeUtils.getCurrentDateByGMT(pnrFlight.getmDepartureTimezoneOffset());
							
							timeDifference = DateTimeUtils.getTimeDifferenceInHoursOrNull(currentDate, departureDate);
						}
						mUserCheckList = iResponse.getContent().orderListByTimes(timeDifference);
					}
					
					initUserCheckListIfNeeded();
					doOnDataReceived();
					trySetProgressDialog(false);
					
				}
			}, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError error)
				{
					trySetProgressDialog(false);
					initUserCheckListIfNeeded();
					doOnDataReceived();
				}
			});
		}
		else
		{
			initUserCheckListIfNeeded();
			doOnDataReceived();
		}
	}
	
	private void initUserCheckListIfNeeded()
	{
		if (mUserCheckList == null)
		{
			mUserCheckList = new UserCheckList();
		}
	}
	
	private void initGui()
	{
		setFlightHeadersByFlightStatus();
		
		trySetProgressDialog(false);
	}
	
	private void includeTabsHeight(final boolean includeTabs)
	{
		new Handler().post(new Runnable()
		{
			@Override
			public void run()
			{
				Point mWindowPoint = new Point();
				if (getActivity() != null)
				{
					getActivity().getWindow().getWindowManager().getDefaultDisplay().getSize(mWindowPoint);
					int containerHeightWithoutTabs = mSvRoot.getHeight();
					if (includeTabs)
					{
						containerHeightWithoutTabs = (int) (mSvRoot.getHeight() - AppUtils.resolveActionBarHeight(ElAlApplication.getInstance()));
					}
					int indicatorArrowHeight = (int) AppUtils.resolveDrawableHeight(getActivity(), R.mipmap.arrow_down_blue);
					
					mFlTopContainer.setLayoutParams(new LinearLayout.LayoutParams(mSvRoot.getWidth(), containerHeightWithoutTabs + indicatorArrowHeight));
					mFlTabsContainer.setLayoutParams(new LinearLayout.LayoutParams(mSvRoot.getWidth(), containerHeightWithoutTabs));
				}
				//								resolveExpandableLayout();
			}
		});
	}
	
	private void setFlightHeadersByFlightStatus()
	{
		if (mUserActivePnr.getIsMultiple() && isMultipleTrip())
		{
			mBtnFlightStatusMyTrip.setVisibility(View.VISIBLE);
			setButtonEnabled(mBtnFlightStatusMyTrip);
			setButtonClickable(mBtnFlightStatusReady);
			
			if (AppUtils.isFlightStatusAtDestination(mFlightStatus))
			{
				setButtonClickable(mBtnFlightStatusAtDestination);
			}
			else
			{
				setButtonDisabled(mBtnFlightStatusAtDestination);
			}
		}
		else
		{
			mBtnFlightStatusMyTrip.setVisibility(View.GONE);
			
			if (AppUtils.isFlightStatusAtDestination(mFlightStatus))
			{
				setBtnFlightStatusAtDestinationEnabled();
			}
			else
			{
				mBtnFlightStatusAtDestination.setEnabled(false);
				setButtonEnabled(mBtnFlightStatusReady);
				setButtonDisabled(mBtnFlightStatusAtDestination);
			}
		}
	}
	
	private void setFlightData()
	{
		mNivTopRootBackground.setDefaultImageResId(R.mipmap.background_flight_management);
		mNivTopRootBackground.setErrorImageResId(R.mipmap.background_flight_management);
		
		mNivTopRootBackground.addOnLayoutChangeListener(new View.OnLayoutChangeListener()
		{
			@Override
			public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8)
			{
				// the layout of the logo view changes at least twice: When it is added
				// to the parent and when the image has been loaded. We are only interested
				// in the second case and to find that case, we do this if statement
				
				if (mNivTopRootBackground.getDrawable() != null)
				{
					trySetProgressDialog(false);
				}
			}
		});
		
		PnrFlight destination = null;
		PnrFlight viaFlight = null;
		
		mCurrentFlight = mUserActivePnr.getCurrentFlight();
		if (mCurrentFlight != null)
		{
			mCurrentFlightNumber = mCurrentFlight.getFlightNumber();
			
			if (mCurrentFlight != null && mCurrentFlight.geteNumDirection() == ePnrFlightType.DEPARTURE)
			{
				if (mUserActivePnr.getDeparturePNRFlights() != null && mUserActivePnr.getDeparturePNRFlights().size() > 1)
				{
					//				nLastFlight = mUserActivePnr.getLastDeparturePNRFlightOrNull();
					viaFlight = mUserActivePnr.getCurrentItemFlightAtPositionDepartureOrReurnOrNull(0, ePnrFlightType.DEPARTURE);
					destination = mUserActivePnr.getCurrentItemFlightAtPositionDepartureOrReurnOrNull(1, ePnrFlightType.DEPARTURE);
				}
				else
				{
					destination = mUserActivePnr.getCurrentItemFlightAtPositionDepartureOrReurnOrNull(0, ePnrFlightType.DEPARTURE);
				}
			}
			else
			{
				if (isReturnFlightWithConnection())
				{
					//				nLastFlight = mUserActivePnr.getLastReturnPNRFlightOrNull();
					viaFlight = mUserActivePnr.getCurrentItemFlightAtPositionDepartureOrReurnOrNull(0, ePnrFlightType.RETURN);
					destination = mUserActivePnr.getCurrentItemFlightAtPositionDepartureOrReurnOrNull(1, ePnrFlightType.RETURN);
				}
				else
				{
					destination = mUserActivePnr.getCurrentItemFlightAtPositionDepartureOrReurnOrNull(0, ePnrFlightType.RETURN);
				}
			}
		}
		
		
		if (!AppUtils.isFlightStatusAtDestination(mFlightStatus))
		{
			//			mFirstFlight = mUserActivePnr.getCurrentItemFlightAtPositionDepartureOrReurnOrNull(0, ePnrFlightType.DEPARTURE);
			
			PnrTicket pnrTicket = mUserActivePnr.getDeparturePNRTicketsMap().entrySet().iterator().hasNext() ?
			                      mUserActivePnr.getDeparturePNRTicketsMap().entrySet().iterator().next().getValue() :
			                      null;
			if (pnrTicket != null && pnrTicket.isCheckinButtonValid())
			{
				getActivity().findViewById(R.id.ll_myFlightTop_CheckIn)
				             .setVisibility(mFlightStatus == LessThan48HoursToDeparture || mFlightStatus == LessThanDayToDeparture /*||mFlightStatus == LessThanDayToOrigination*/ ? View.VISIBLE : View.GONE);
			}
			else
			{
				getActivity().findViewById(R.id.ll_myFlightTop_CheckIn).setVisibility(View.GONE);
			}
		}
		else
		{
			//			mFirstFlight = mUserActivePnr.getCurrentItemFlightAtPositionDepartureOrReurnOrNull(0, ePnrFlightType.RETURN);
			//			destination = mUserActivePnr.getCurrentItemFlightAtPositionDepartureOrReurnOrNull(0, ePnrFlightType.RETURN);
			
			
			PnrTicket pnrTicket = mUserActivePnr.getReturnPNRTicketsMap().entrySet().iterator().hasNext() ? mUserActivePnr.getReturnPNRTicketsMap().entrySet().iterator().next().getValue() : null;
			if (pnrTicket != null && pnrTicket.isCheckinButtonValid())
			{
				getActivity().findViewById(R.id.ll_myFlightTop_CheckIn)
				             .setVisibility(mFlightStatus == LessThan48HoursToDeparture || mFlightStatus == LessThanDayToDeparture ? View.VISIBLE : View.GONE);
			}
			else
			{
				getActivity().findViewById(R.id.ll_myFlightTop_CheckIn).setVisibility(View.GONE);
			}
		}
		
		
		//		GetPNRFlightsContent destination = mCurrentInspectedFlight.getmDestination();
		//		GetPNRFlightsContent viaFlight = mCurrentInspectedFlight.getmVia();
		
		// set first leg of flight depending on existence of viaDestination.
		if (destination != null)
		{
			mCurrentFlightNumber = destination.getFlightNumber();
			
			try
			{
				String imgUrl = getBackgroundImageUrl(mUserActivePnr.getFlightByNumber(mCurrentFlightNumber));
				imgUrl = imgUrl.replace(" ", "%20");
				
				//				trySetProgressDialog(true);
				mNivTopRootBackground.setImageUrl(imgUrl, getService().getImageLoader());
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			
			mTvTimeTitle.setText(getTimeTitleByFlightStatus());
			setTimeLeftCounterByFlightStatus();
			mTvOrigin.setText(viaFlight != null ? viaFlight.getOrigination() : destination.getOrigination());
			mTvDestination.setText(viaFlight != null ? viaFlight.getDestination() : destination.getDestination());
			mTvDepartureDateTime.setText(setDateTimeText(viaFlight != null ? viaFlight.getDepartureDate() : destination.getDepartureDate()));
			
			String departureTerminalStr = viaFlight != null ? viaFlight.getDepartTerminal() : destination.getDepartTerminal();
			String arrivalTerminalStr = viaFlight != null ? viaFlight.getArraivalTerminal() : destination.getArraivalTerminal();
			
			if ((departureTerminalStr == null || departureTerminalStr.isEmpty()) && (arrivalTerminalStr == null || arrivalTerminalStr.isEmpty()))
			{
				mCdtTerminalRow.setVisibility(View.GONE);
			}
			else
			{
				mCdtTerminalRow.setVisibility(View.VISIBLE);
				
				mTvOriginTerminal.setText(departureTerminalStr != null && !departureTerminalStr.isEmpty() ? getResources().getString(R.string.terminal) + " " + departureTerminalStr : "");
				mTvDestinationTerminal.setText(arrivalTerminalStr != null && !arrivalTerminalStr.isEmpty() ? getResources().getString(R.string.terminal) + " " + arrivalTerminalStr : "");
			}
			
			mTvFlightDuration.setText(viaFlight != null ? viaFlight.getFlightDuration() : destination.getFlightDuration());
			if (!(mTvFlightDuration.getText() != null && !TextUtils.isEmpty(mTvFlightDuration.getText())))
			{
				mLilFlightDuration.setVisibility(View.INVISIBLE);
			}
			
			mTvArrivalDateTime.setText(setDateTimeText(viaFlight != null ? viaFlight.getArrivalDate() : destination.getArrivalDate()));
			
			
			mTvFlightDetailsNumber.setText(viaFlight != null ? viaFlight.getFlightNumber() : destination.getFlightNumber());
			
			if (mLilCheckIn != null)
			{
				mLilCheckIn.setVisibility(checkCheckInAvailable() ? View.VISIBLE : View.GONE);
			}
			
			PnrFlight flight = mUserActivePnr.getFlightByNumber(viaFlight != null ? viaFlight.getFlightNumber() : destination.getFlightNumber());
			
			mLlFlightBoarding.setVisibility(shouldShowBoardingLil(flight) ? View.VISIBLE : View.GONE);
			
			if (flight != null && flight.getBoardingDate() != null)
			{
				//				mTvBoardingTime.setText(setTimeText(flight.getBoardingDate()));
				mTvBoardingTime.setText(setTimeText(flight.getBoardingDate()) + " " + getString(R.string.estimated));
			}
			else
			{
				mTvBoardingTime.setText("--");
			}
			
			if (flight != null && !TextUtils.isEmpty(flight.getPit()))
			{
				mTvGate.setText(flight.getPit());
			}
			else
			{
				mTvGate.setText("--");
			}
		}
		else
		{
			mTvTimeTitle.setText("");
			mTvTimeLeftCounterHours.setText("");
			mTvTimeLeftCounterMinutes.setText("");
			mTvTimeLeftCounterSeconds.setText("");
			mLlTimeLeftCounter.setVisibility(View.GONE);
			mTvOrigin.setText("");
			mTvDestination.setText("");
			mTvDepartureDateTime.setText("");
			mTvFlightDuration.setText("");
			mTvArrivalDateTime.setText("");
			mTvBoardingTime.setText("");
			mTvGate.setText("");
			mTvFlightDetailsNumber.setText("");
			
			mLlFlightBoarding.setVisibility(View.GONE);
			mLlFlightLayover.setVisibility(View.GONE);
		}
		
		if (viaFlight != null && destination != null)
		{
			mLlFlightBeyond.setVisibility(View.VISIBLE);
			mTvOriginBeyond.setText(viaFlight.getDestination());
			mTvDestinationBeyond.setText(destination.getDestination());
			mTvDepartureDateTimeBeyond.setText(setDateTimeText(destination.getDepartureDate()));
			
			String departureTerminalStr = destination.getDepartTerminal();
			mTvOriginTerminalBeyond.setText(!TextUtils.isEmpty(departureTerminalStr) ? getResources().getString(R.string.terminal) + " " + departureTerminalStr : "");
			
			mTvFlightDurationBeyond.setText(destination.getFlightDuration());
			mTvArrivalDateTimeBeyond.setText(setDateTimeText(destination.getArrivalDate()));
			
			String arrivalTerminalStr = destination.getArraivalTerminal();
			mTvDestinationTerminalBeyond.setText(!TextUtils.isEmpty(arrivalTerminalStr) ? getResources().getString(R.string.terminal) + " " + arrivalTerminalStr : "");
			
			mTvFlightDetailsNumberBeyond.setText(destination.getFlightNumber());
			
			setTimeLeftBeyond(destination.getDepartureDate(), viaFlight.getArrivalDate());
		}
		else
		{
			mLlFlightBeyond.setVisibility(View.GONE);
			mTvOriginBeyond.setText("");
			mTvDestinationBeyond.setText("");
			mTvDepartureDateTimeBeyond.setText("");
			mTvOriginTerminalBeyond.setText("");
			mTvFlightDurationBeyond.setText("");
			mTvArrivalDateTimeBeyond.setText("");
			mTvDestinationTerminalBeyond.setText("");
			mTvFlightDetailsNumberBeyond.setText("");
			mLlFlightLayover.setVisibility(View.GONE);
		}
		
		if (!(mTvFlightDurationBeyond.getText() != null && !TextUtils.isEmpty(mTvFlightDurationBeyond.getText())))
		{
			mLilFlightDurationBeyond.setVisibility(View.INVISIBLE);
		}
		
		checkConnectionTo2000();
	}
	
	private boolean shouldShowBoardingLil(PnrFlight flight)
	{
		return (mFlightStatus == LessThan48HoursToDeparture || mFlightStatus == LessThanDayToDeparture || mFlightStatus == LessThanDayToOrigination) && flight != null && (flight.getBoardingDate() != null || (flight
				.getPit() != null && !TextUtils.isEmpty(flight.getPit())));
		//		|| mFlightStatus == LessThanDayToDeparture
		//		return mFlightStatus == LessThan48HoursToDeparture || mFlightStatus == DepartedNotArrivedToDestination || mFlightStatus == LessThanDayToOrigination || mFlightStatus == AfterBackToOrigination
		//				|| flight == null || (flight.getBoardingDate() == null && (flight.getPit() == null || TextUtils.isEmpty(flight.getPit())));
	}
	
	@Override
	public void onErrorResponse(final VolleyError error)
	{
		ErrorsHandler.tryShowServiceErrorDialog(error, getActivity());
	}
	
	private boolean isDepartureFlight()
	{
		return mUserActivePnr.getDeparturePNRFlights() != null && !mUserActivePnr.getDeparturePNRFlights().isEmpty();
	}
	
	private boolean isReturnFlightWithConnection()
	{
		return mUserActivePnr.getReturnPNRFlights() != null && !mUserActivePnr.getReturnPNRFlights().isEmpty() && mUserActivePnr.getReturnPNRFlights().size() > 1;
	}
	
	private boolean checkCheckInAvailable()
	{
		MyFlightsObject myFlightsObject = MyFlightsObject.getInstance();
		if (myFlightsObject != null && mCurrentInspectedFlight != null && myFlightsObject.getCurrentUserActivePnr() != null && myFlightsObject.getCurrentUserActivePnr()
		                                                                                                                                      .getPNRCheckins() != null && (mFlightStatus == LessThan48HoursToDeparture || mFlightStatus == LessThanDayToDeparture || mFlightStatus == LessThanDayToOrigination))
		{
			PnrTicket ticket = myFlightsObject.getCurrentUserActivePnr()
			                                  .getPNRTicketByFlightNum(mUserActivePnr.getLastName(), mCurrentInspectedFlight.getmVia() != null ?
			                                                                                         mCurrentInspectedFlight.getmVia().getFlightNumber() :
			                                                                                         mCurrentInspectedFlight.getmDestination().getFlightNumber());
			return ticket != null && ticket.isCheckinButtonValid();
		}
		else
		{
			return false;
		}
	}
	
	private String getBackgroundImageUrl(PnrFlight iFlight)
	{
		if (iFlight != null)
		{
			String imgUrl = mUserActivePnr.getDestinationImageByFlightNumber(iFlight.getFlightNumber());
			if (imgUrl != null && !TextUtils.isEmpty(imgUrl))
			{
				return RequestStringBuilder.getDestinationImageUrl(imgUrl);
			}
			else
			{
				return RequestStringBuilder.getDestinationImageUrl(iFlight.getDestinationImage());
			}
		}
		return "";
	}
	
	private void setTimeLeftBeyond(final Date iFirstFlightDate, final Date iLastFlightDate)
	{
		long timeDifference = DateTimeUtils.getTimeDifferenceInMilliseconds(iLastFlightDate, iFirstFlightDate);
		if (timeDifference > 0)
		{
			mLlFlightLayover.setVisibility(View.VISIBLE);
			mTvFlightLayover.setText(DateTimeUtils.convertDDeltaInMillisecondsToTimeStringWithoutSeconds(timeDifference));
		}
		else
		{
			mLlFlightLayover.setVisibility(View.GONE);
		}
	}
	
	private void setTimeLeftCounterByFlightStatus()
	{
		mTvTimeLeftCounterHours.setText("");
		mTvTimeLeftCounterMinutes.setText("");
		mTvTimeLeftCounterSeconds.setText("");
		mLlTimeLeftCounter.setVisibility(View.GONE);
		
		if (mFlightStatus == LessThan48HoursToDeparture || mFlightStatus == LessThanDayToDeparture || mFlightStatus == LessThanDayToOrigination && mCurrentInspectedFlight != null && mCurrentInspectedFlight
				.getmDestination() != null)
		{
			Date departureDate = new Date();
			if (mCurrentFlight != null)
			{
				departureDate = mCurrentFlight.getDepartureDate();
			}
			else if (mCurrentInspectedFlight != null)
			{
				departureDate = mCurrentInspectedFlight.getmVia() != null ? mCurrentInspectedFlight.getmVia().getDepartureDate() : mCurrentInspectedFlight.getmDestination().getDepartureDate();
			}
			//TODO avishay 16/1/18 HERE!
			//			long ts = System.currentTimeMillis();
			//			Date current = new Date(ts);
			Date current = DateTimeUtils.getCurrentDateByGMT(mCurrentFlight.getmDepartureTimezoneOffset());
			
			departureDate = DateTimeUtils.convertDateToDateIgnoreGMT(departureDate);
			
			Log.d(TAG, "Counter currentDate: " + current);
			Log.d(TAG, "Counter departureDate: " + departureDate);
			
			long timeDifference = DateTimeUtils.getTimeDifferenceInMilliseconds(current, departureDate);
			
			if (timeDifference > 0)
			{
				mLlTimeLeftCounter.setVisibility(View.VISIBLE);
				new CountDownTimer(timeDifference, DateTimeUtils.SECOND_IN_MILLISECONDS)
				{
					@Override
					public void onTick(final long iNewDeltaInMilliseconds)
					{
						tryUpdateCounter(mTvTimeLeftCounterHours, iNewDeltaInMilliseconds);
					}
					
					@Override
					public void onFinish()
					{
						tryUpdateCounter(mTvTimeLeftCounterHours, 0);
					}
				}.start();
			}
		}
	}
	
	private void tryUpdateCounter(final TextView iTvTimeLeftCounter, final long iNewValue)
	{
		//		iTvTimeLeftCounter.setText(DateTimeUtils.convertDDeltaInMillisecondsToTimeString(iNewValue));
		String[] timeArray = DateTimeUtils.convertDDeltaInMillisecondsToTimeStringArray(iNewValue);
		
		mTvTimeLeftCounterHours.setText(timeArray[0]);
		mTvTimeLeftCounterMinutes.setText(timeArray[1]);
		mTvTimeLeftCounterSeconds.setText(timeArray[2]);
	}
	
	private String getTimeTitleByFlightStatus()
	{
		String result;
		switch (mFlightStatus)
		{
			case MoreThan48HoursToDeparture:
				PnrFlight currentFlight = mUserActivePnr.getCurrentFlight();
				
				result = getString(R.string.nearly_there) + " " + getDestinationCityName();
				
				if (currentFlight != null)
				{
					Date current = DateTimeUtils.getCurrentDateByGMT(currentFlight/*mCurrentFlight*/.getmDepartureTimezoneOffset());
					Date departureDate = DateTimeUtils.convertDateToDateIgnoreGMT(currentFlight.getDepartureDate());
					
					Integer timeDifferenceInDays = DateTimeUtils.getTimeDifferenceInDaysOrNull(current, departureDate);
					
					//					Integer timeDifferenceInDays = DateTimeUtils.getTimeDifferenceInDaysOrNull(current, new Date(currentFlight.getDepartureDate().getTime()));
					
					if (timeDifferenceInDays != null && timeDifferenceInDays > 1)
					{
						result = timeDifferenceInDays == null ? "" : getString(R.string.days_left_to_go, String.valueOf(timeDifferenceInDays), getDestinationCityName());
						
						if (timeDifferenceInDays <= 10)
						{
							result = getString(R.string.days_to_go, String.valueOf(timeDifferenceInDays), getDestinationCityName());
						}
					}
				}
				
				break;
			case LessThan48HoursToDeparture:
				result = getString(R.string.nearly_there) + " " + getDestinationCityName();
				break;
			case LessThanDayToDeparture:
				result = getString(R.string.nearly_there) + " " + getDestinationCityName();
				break;
			case DepartedNotArrivedToDestination:
				result = getString(R.string.nearly_there) + " " + getDestinationCityName();
				break;
			case MoreThanDayToOrigination:
				
				if (isMultipleTrip())
				{
					PnrFlight selectedFlight = mUserActivePnr.getCurrentFlight();
					if (selectedFlight != null)
					{
						Date current = DateTimeUtils.getCurrentDateByGMT(selectedFlight/*mCurrentFlight*/.getmDepartureTimezoneOffset());
						Date departureDate = DateTimeUtils.convertDateToDateIgnoreGMT(selectedFlight.getDepartureDate());
						
						Integer timeDifferenceInDays = DateTimeUtils.getTimeDifferenceInDaysOrNull(current, departureDate);
						
						//						Integer timeDifferenceInDays = DateTimeUtils.getTimeDifferenceInDaysOrNull(current, new Date(selectedFlight.getDepartureDate().getTime()));
						
						if (timeDifferenceInDays != null && timeDifferenceInDays > 1)
						{
							result = timeDifferenceInDays == null ? "" : getString(R.string.days_left_to_go, String.valueOf(timeDifferenceInDays), getDestinationCityName());
							
							if (timeDifferenceInDays <= 10)
							{
								result = getString(R.string.days_to_go, String.valueOf(timeDifferenceInDays), getDestinationCityName());
							}
						}
						else
						{
							if (isReturnFlight())
							{
								result = getString(R.string.ready_fly_back);
							}
							else
							{
								result = getString(R.string.enjoy_in, String.valueOf(getDestinationCityName()));
							}
						}
					}
					else
					{
						if (mUserActivePnr.getDeparturePNRFlights() != null && !mUserActivePnr.getDeparturePNRFlights().isEmpty()) // ePnrFlightType.DEPARTURE
						{
							result = getString(R.string.enjoy_in, String.valueOf(getDestinationCityName()));
						}
						else // ePnrFlightType.RETURN
						{
							result = getString(R.string.enjoy_in, String.valueOf(getOriginationCityName()));
						}
					}
				}
				else
				{
					if (mUserActivePnr.getDeparturePNRFlights() != null && !mUserActivePnr.getDeparturePNRFlights().isEmpty()) // ePnrFlightType.DEPARTURE
					{
						//					result = getString(R.string.enjoy)  + " " + getDestinationCityName();
						result = getString(R.string.enjoy_in, String.valueOf(getDestinationCityName()));
					}
					else // ePnrFlightType.RETURN
					{
						//					result = getString(R.string.enjoy) + " " + getOriginationCityName();
						result = getString(R.string.enjoy_in, String.valueOf(getOriginationCityName()));
					}
				}
				break;
			case LessThanDayToOrigination:
				result = getString(R.string.ready_fly_back);
				break;
			case AfterBackToOrigination:
				result = getString(R.string.welcome_back);
				break;
			case NoToOrigination:
				//				result = getString(R.string.enjoy) + getDestinationCityName();
				result = getString(R.string.enjoy_in, String.valueOf(getDestinationCityName()));
				break;
			case Error:
			default:
				result = "";
				break;
		}
		
		return result;
	}
	
	private String getDestinationCityName()
	{
		if (mUserActivePnr.getDeparturePNRFlights() != null && !mUserActivePnr.getDeparturePNRFlights().isEmpty())
		{
			if (mUserActivePnr.getDeparturePNRFlights().size() > 1)
			{
				return mUserActivePnr.getDeparturePNRFlights().get(1).getDestinationCityName();
			}
			else
			{
				return mUserActivePnr.getDeparturePNRFlights().get(0).getDestinationCityName();
			}
		}
		else if (mUserActivePnr.getReturnPNRFlights() != null && !mUserActivePnr.getReturnPNRFlights().isEmpty())
		{
			if (mUserActivePnr.getReturnPNRFlights().size() > 1)
			{
				return mUserActivePnr.getReturnPNRFlights().get(1).getDestinationCityName();
			}
			else
			{
				return mUserActivePnr.getReturnPNRFlights().get(0).getDestinationCityName();
			}
		}
		else
		{
			return "";
		}
		
	}
	
	private String getOriginationCityName()
	{
		if (mCurrentFlight != null)
		{
			return mCurrentFlight.getOriginationCityName();
		}
		else
		{
			return "";
		}
	}
	
	private String setDateTimeText(final Date iDate)
	{
		String result = "";
		
		if (iDate != null)
		{
			result = DateTimeUtils.convertDateToDayMonthHourMinuteString(iDate);
		}
		
		return result;
	}
	
	private String setTimeText(final Date iDate)
	{
		String result = "";
		
		if (iDate != null)
		{
			result = DateTimeUtils.convertDateToHourMinuteString(iDate);
		}
		
		return result;
	}
	
	@Override
	public void pushFragmentToCurrentTab(final EWBaseFragment iFragment, final boolean iShouldAnimate, final boolean iShouldAdd)
	{
		mTabsBackStackManager.pushFragment(TabsBackStackManager.CURRENT_TAB, iFragment, iShouldAnimate, iShouldAdd);
	}
	
	@Override
	public void popFragment()
	{
		if (mTabsBackStackManager != null)
		{
			mTabsBackStackManager.popFragment();
		}
		else
		{
			tryPopFragment();
		}
	}
	
	@Override
	public boolean isFirstInTab()
	{
		boolean result;
		
		if (isTabsContainerFullyVisible())
		{
			if (mTabsBackStackManager != null && mTabsBackStackManager.isCurrentTabBackStackEmpty())
			{
				scrollByVisibility();
			}
			
			result = false;
		}
		else
		{
			result = true;
		}
		
		return result;
	}
	
	@Override
	public void onTabRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults)
	{
		mTabsBackStackManager.onTabRequestPermissionsResult(requestCode, permissions, grantResults);
	}
	
	private boolean isTabsContainerFullyVisible()
	{
		boolean result = false;
		
		Rect visibleRect = AppUtils.getVisibleRectOfView(mFlTabsContainer);
		
		if (visibleRect != null)
		{
			result = (visibleRect.bottom - visibleRect.top) == (mFlTabsContainer.getBottom() - mFlTabsContainer.getTop());
			result &= visibleRect.bottom <= getActivity().getWindow().getWindowManager().getDefaultDisplay().getHeight();
		}
		
		return result;
	}
	
	private void scrollByVisibility()
	{
		if (mShouldScroll)
		{
			//			mSvRoot.fullScroll(isTabsContainerFullyVisible() ? View.FOCUS_UP : View.FOCUS_DOWN);
			
			ObjectAnimator anim = ObjectAnimator.ofInt(mSvRoot, "scrollY", isTabsContainerFullyVisible() ? mSvRoot.getTop() : mSvRoot.getBottom());
			anim.setDuration(SCROLL_DURATION);
			anim.start();
		}
		else
		{
			mShouldScroll = true;
		}
	}
	
	@Override
	public void onFinishInit()
	{
		mSvRoot.requestFocus(View.FOCUS_UP);
		mShouldScroll = true;
	}
	
	@Override
	public void onTabSelected(final String iTabName)
	{
		try
		{
			((EWBaseDrawerActivity) getActivity()).setShouldExit(false);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		if (mShouldScroll)
		{
			//			mSvRoot.fullScroll(View.FOCUS_DOWN);
			ObjectAnimator anim = ObjectAnimator.ofInt(mSvRoot, "scrollY", mSvRoot.getBottom());
			anim.setDuration(SCROLL_DURATION);
			anim.start();
		}
		
		if (isAdded())
		{
			pushAnalyticsEvent(iTabName);
		}
	}
	
	private void pushAnalyticsEvent(final String iTabName)
	{
		String activePnr = UserData.getInstance().getActivePnr();
		
		String saveTab = getString(R.string.to_save);
		String doTab = getString(R.string.to_do);
		String seeTab = getString(R.string.to_see);
		String knowTab = getString(R.string.to_know);
		
		if (mShowDestinationTabs)
		{
			if (iTabName.equals(saveTab))
			{
				//event 67
				((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight - At destination", "Action", "At destination >> " + activePnr + " >> Lobby", "Label", "Save"));
			}
			else if (iTabName.equals(knowTab))
			{
				//event 65
				((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight - At destination", "Action", "At destination >> " + activePnr + " >> Lobby", "Label", "Useful information"));
			}
			else if (iTabName.equals(seeTab))
			{
				//event 66
				((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight - At destination", "Action", "At destination >> " + activePnr + " >> Lobby", "Label", "Experience"));
			}
		}
		else
		{
			if (iTabName.equals(saveTab))
			{
				//event 44
				((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight" + TagManagerImplementation.TIME_SCOPE, "Action", TagManagerImplementation.TIME_SCOPE + activePnr + " >> Lobby", "Label", "Save"));
			}
			else if (iTabName.equals(doTab))
			{
				//event 42
				((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight" + TagManagerImplementation.TIME_SCOPE, "Action", TagManagerImplementation.TIME_SCOPE + activePnr + " >> Lobby", "Label", "Remember"));
			}
			else if (iTabName.equals(seeTab))
			{
				//event 43
				((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight" + TagManagerImplementation.TIME_SCOPE, "Action", TagManagerImplementation.TIME_SCOPE + activePnr + " >> Lobby", "Label", "Experience"));
			}
			else if (iTabName.equals(knowTab))
			{
				//event 41
				((EWBaseActivity) getActivity()).pushCustomEvent(DataLayer.mapOf("Category", "Manage Flight" + TagManagerImplementation.TIME_SCOPE, "Action", TagManagerImplementation.TIME_SCOPE + activePnr + " >> Lobby", "Label", "Know"));
			}
		}
	}
	
	@Override
	public void onTabUnselected(final String iTabName)
	{
	}
	
	@Override
	public void onTabReselected(final String iTabName)
	{
		scrollByVisibility();
	}
	
	@Override
	public void onTabStackAdd(final String iTabName)
	{
	}
	
	@Override
	public void onTabStackPop(final String iTabName)
	{
	}
	
	@Override
	public void onActivityResult(int iRequestCode, int iResultCode, Intent iData)
	{
		mTabsBackStackManager.onActivityResult(iRequestCode, iResultCode, iData);
	}
	
	private boolean isMultipleTrip()
	{
		//		return mDestinationsList != null && !mDestinationsList.isEmpty() && mUserActivePnr.getIsMultiple() ? mUserActivePnr.getIsMultiple() : (mDestinationsList != null && mDestinationsList.size() > 2);
		return mUserActivePnr.getIsMultiple() && mDestinationsList != null && !mDestinationsList.isEmpty();
	}
	
	private void showTabs(boolean iTabsShouldShow)
	{
		if (mTabHost != null)
		{
			if (iTabsShouldShow)
			{
				mTabHost.setVisibility(View.VISIBLE);
			}
			else
			{
				mTabHost.setVisibility(View.GONE);
			}
		}
	}
	
	@Override
	public void onDestinationSelected(DestinationListItem iDestinationListItem)
	{
		mCurrentInspectedFlight = iDestinationListItem;
		mCurrentFlightNumber = mCurrentInspectedFlight.getmDestination().getFlightNumber();
		mCurrentInspectedFlight.setmFlightType(mUserActivePnr.getFlightDirectionByFlightNum(mCurrentFlightNumber));
		
		mAppContent = null;
		
		getPnrByNum(mUserActivePnr.getPnr(), mCurrentFlightNumber);
	}
	
	@Override
	public void onScrollToCurrentItem(final int position)
	{
		Handler handler = new Handler();
		handler.postDelayed(new Runnable()
		{
			public void run()
			{
				try
				{
					doScrollToPosition(position);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				
			}
		}, DETAILS_EXPAND_COLLAPSE_ANIMATION_SPEED);
	}
	
	public void getPnrByNum(String pnr, String flightNumber)
	{
		if (isServiceConnected() && (isResumed() || isVisible()))
		{
			showRefreshPageDialog(false);
			trySetProgressDialog(true);
			((MyFlightsActivity) getActivity()).GetPNRByNum(new Response.Listener<ResponseGetPnrByNum>()
			{
				@Override
				public void onResponse(final ResponseGetPnrByNum response)
				{
					handleResponseGetPnrByNum(response);
				}
			}, this, pnr, flightNumber);
		}
		else
		{
			AppUtils.printLog(Log.ERROR, TAG, LocalError.eLocalError.GeneralError.getErrorMessage());
		}
	}
	
	private void handleResponseGetPnrByNum(ResponseGetPnrByNum response)
	{
		if (response != null && response.getContent() != null)
		{
			trySetProgressDialog(false);
			UserActivePnr activePnr = response.getContent();
			MyFlightsObject myFlightsObject = MyFlightsObject.getInstance();
			myFlightsObject.setCurrentUserActivePnr(activePnr);
			
			mUserActivePnr = activePnr;
			mDidRequestData = false;
			
			mCurrentFlight = response.getContent().getCurrentFlight();
			
			getDataIfNeeded();
			setFlightStatus();
			setMyFlightLayout(getView());
		}
	}
	
	private void doScrollToPosition(int position)
	{
		if (mRvLayoutManager != null)
		{
			RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(getContext())
			{
				@Override
				protected int getVerticalSnapPreference()
				{
					return LinearSmoothScroller.SNAP_TO_START;
				}
			};
			
			if (position > 0)
			{
				smoothScroller.setTargetPosition(position - 1);
			}
			else
			{
				smoothScroller.setTargetPosition(0);
			}
			mRvLayoutManager.startSmoothScroll(smoothScroller);
		}
	}
}

