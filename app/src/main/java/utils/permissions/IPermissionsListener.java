package utils.permissions;

/**
 * Created with care by Shahar Ben-Moshe on 11/12/16.
 */

public interface IPermissionsListener
{
//    void doOnPermissionExplanationNeeded(Activity iActivity, String iPermission);
    
    void doOnPermissionsGranted(String[] iPermissions);
    
    void doOnPermissionsDenied(String[] iPermissions);
}
