package global;

import android.text.TextUtils;

/**
 * Created with care by Shahar Ben-Moshe on 04/01/17.
 */
public enum eLanguage
{
	Hebrew(1, "HE"),
	English(2, "EN");
	//	HEBREW(3, "IW");
	
	private int mLanguageId;
	private String mLanguageCode;
	
	eLanguage(final int iLanguageId, final String iLanguageCode)
	{
		mLanguageId = iLanguageId;
		mLanguageCode = iLanguageCode;
	}
	
	public int getLanguageId()
	{
		return mLanguageId;
	}
	
	public String getLanguageCode()
	{
		return mLanguageCode;
	}
	
	
	public void setLanguageCode(final String iLanguageCode)
	{
		mLanguageCode = iLanguageCode;
	}
	
	public static eLanguage getLanguageByCodeOrDefault(final String iLanguageCode, final eLanguage iLanguage)
	{
		eLanguage result = iLanguage;
		//		/***TODO avishay 10/09/17 just fot test**/
		//		String languageCode = iLanguageCode;
		//		if (iLanguageCode.equalsIgnoreCase("he"))
		//		{
		//			languageCode = "iw";
		//		}
		//		/***/
		for (eLanguage language : values())
		{
			if (language != null && !TextUtils.isEmpty(language.getLanguageCode())&& language.getLanguageCode().equalsIgnoreCase(iLanguageCode/*languageCode*/))
			{
				result = language;
				break;
			}
		}
		
		return result;
	}
	
	public boolean isHebrew()
	{
		return this == Hebrew;
	}
	
	public boolean isEnglish()
	{
		return this == English;
	}
	
	public static boolean isLanguageSupported(final eLanguage iLanguage)
	{
		for (eLanguage el : eLanguage.values())
		{
			if (el == iLanguage)
			{
				return true;
			}
		}
		return false;
	}
}