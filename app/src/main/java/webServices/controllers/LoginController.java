package webServices.controllers;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import global.AppData;
import global.UserData;
import utils.errors.LocalError.eLocalError;
import utils.global.AppUtils;
import webServices.global.JacksonMatmidLoginRequest;
import webServices.global.JacksonRequest;
import webServices.global.RequestStringBuilder;
import webServices.global.eRequestItem;
import webServices.requests.RequestChangePassword;
import webServices.requests.RequestCheckAppVersion;
import webServices.requests.RequestGetAppData;
import webServices.requests.RequestGetGroupPNRsData;
import webServices.requests.RequestGetHomeScreenContent;
import webServices.requests.RequestGetRepresentationByIP;
import webServices.requests.RequestGetSideMenuItems;
import webServices.requests.RequestGetUnreadNotificationsBadge;
import webServices.requests.RequestGetUserFamily;
import webServices.requests.RequestLogout;
import webServices.requests.RequestPromo;
import webServices.requests.RequestRegister;
import webServices.requests.RequestResetPassword;
import webServices.requests.RequestSetDevice;
import webServices.requests.RequestSetMatmidUserFamily;
import webServices.requests.RequestSetRegularUserFamily;
import webServices.requests.RequestSetUserDetails;
import webServices.requests.RequestSetUserMatmidDetails;
import webServices.requests.RequestUserLogin;
import webServices.responses.ResponseChangePassword;
import webServices.responses.ResponseCheckAppVersion;
import webServices.responses.ResponseGetAppData;
import webServices.responses.ResponseGetGroupPNRs;
import webServices.responses.ResponseGetHomeScreenContent;
import webServices.responses.ResponseGetRepresentationByIp;
import webServices.responses.ResponseGetSideMenuItems;
import webServices.responses.ResponseGetUnreadNotificationsBadge;
import webServices.responses.ResponseGetUserFamily;
import webServices.responses.ResponseIpInfo;
import webServices.responses.ResponseLogout;
import webServices.responses.ResponseMatmidLogin;
import webServices.responses.ResponsePromo;
import webServices.responses.ResponseRegister;
import webServices.responses.ResponseResetPassword;
import webServices.responses.ResponseSetDevice;
import webServices.responses.ResponseSetUserDetails;
import webServices.responses.ResponseSetUserFamily.ResponseSetMatmidUserFamily;
import webServices.responses.ResponseSetUserFamily.ResponseSetRegularUserFamily;
import webServices.responses.ResponseSetUserMatmidDetails;
import webServices.responses.ResponseUserLogin;
import webServices.responses.getMobileCountryCodes.ResponseGetMobileCountryCodes;

/**
 * Created with care by Shahar Ben-Moshe on 13/12/16.
 */
public class LoginController extends AController implements MizdamenInterfave
{
	private static final int CONNECTION_TIME_OUT = 3000;
	private static final String TAG = LoginController.class.getSimpleName();
	
	public void getPromoByUdid(Response.Listener<ResponsePromo> iListener, Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponsePromo> requestPromo = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getPromoUrl(), new RequestPromo(), ResponsePromo.class, iListener, iErrorListener);
			requestPromo.setRetryPolicy(new DefaultRetryPolicy(CONNECTION_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			requestPromo.setSecretHashHeader(requestPromo.getBody());
			getRequestQueue().add(requestPromo);
		}
	}
	
	public void CheckAppVersion(Response.Listener<ResponseCheckAppVersion> iListener, Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseCheckAppVersion> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getCheckAppVersionUrl(), new RequestCheckAppVersion(), ResponseCheckAppVersion.class, iListener, iErrorListener);
			request.setRetryPolicy(new DefaultRetryPolicy(CONNECTION_TIME_OUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void getAppData(final RequestGetAppData iRequest, final Response.Listener<ResponseGetAppData> AppDataListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && AppDataListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseGetAppData> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getGetAppDataUrl(), iRequest, ResponseGetAppData.class, AppDataListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void Register(final RequestRegister iRequest, final Response.Listener<ResponseRegister> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseRegister> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getRegisterUrl(), iRequest, ResponseRegister.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void LoginAsGuest(final Response.Listener<ResponseUserLogin> iListener, final Response.ErrorListener iErrorListener)
	{
		LoginAsUser(null, null, false, iListener, iErrorListener);
	}
	
	public void LoginAsUser(final String iEmail, final String iPassword, final boolean iRememberMe, final Response.Listener<ResponseUserLogin> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			getIpAddress(new Response.Listener<ResponseIpInfo>()
			{
				@Override
				public void onResponse(final ResponseIpInfo response)
				{
					String ipRequest = response == null ? "" : response.getIp();
					RequestUserLogin requestUserLogin = new RequestUserLogin(iEmail, iPassword, iRememberMe, ipRequest);
					
					JacksonRequest<ResponseUserLogin> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getLoginUrl(), requestUserLogin, ResponseUserLogin.class, new Response.Listener<ResponseUserLogin>()
					{
						@Override
						public void onResponse(final ResponseUserLogin iResponseUserLogin)
						{
							if (iResponseUserLogin != null && iResponseUserLogin.getContent() != null)
							{
								UserData.getInstance().setUserId(iResponseUserLogin.getContent().getUserID());
								UserData.getInstance().setGuestUserId(iResponseUserLogin.getContent().getUserID());
//								getHomeScreenContent(null, null);
								
								final int userId = iResponseUserLogin.getContent().getUserID();
								String sessionId = iResponseUserLogin.getContent().getUserObject().getSessionID();
								String matmidMemberId = iResponseUserLogin.getContent().getUserObject().getMatmidMemberID();
								
								getSideMenuItems(new RequestGetSideMenuItems(userId, sessionId, matmidMemberId), new Response.Listener<ResponseGetSideMenuItems>()
								{
									@Override
									public void onResponse(final ResponseGetSideMenuItems iResponseGetSideMenuItems)
									{
										AppData.getInstance().setSideMenuData(iResponseGetSideMenuItems);
										
										//Get notification badge
										GetUnreadNotificationsBadge(new RequestGetUnreadNotificationsBadge(userId), new Response.Listener<ResponseGetUnreadNotificationsBadge>()
										{
											@Override
											public void onResponse(final ResponseGetUnreadNotificationsBadge response)
											{
												AppData.getInstance().setUnReadPushes(response);
												iListener.onResponse(iResponseUserLogin);
											}
										}, iErrorListener);
									}
								}, iErrorListener);
							}
							else
							{
								AppUtils.printLog(Log.DEBUG, TAG, eLocalError.GeneralConnectionError.getErrorMessage());
							}
						}
					}, iErrorListener);
					//					request.setHeaders(RequestStringBuilder.createPasswordHeader(iPassword));
					request.setSecretHashHeader(request.getBody());
					getRequestQueue().add(request);
				}
			}, iErrorListener);
		}
		else
		{
			AppUtils.printLog(Log.DEBUG, TAG, eLocalError.GeneralConnectionError.getErrorMessage());
		}
	}
	
	
//	public void getMobileCountryCodes(final Response.Listener<ResponseGetMobileCountryCodes> countryCodesListener, final Response.ErrorListener iErrorListener)
//	{
//		if (!isRequestQueueNull() && countryCodesListener != null && iErrorListener != null)
//		{
//			JacksonRequest<ResponseGetMobileCountryCodes> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getMobileCountryCodesUrl(), new Object(), ResponseGetMobileCountryCodes.class, countryCodesListener, iErrorListener);
//			request.setSecretHashHeader(request.getBody());
//			getRequestQueue().add(request);
//		}
//	}
	
	public void getHomeScreenContent(final Response.Listener<ResponseGetHomeScreenContent> iResponseListener, final Response.ErrorListener iErrorListener)
	{
		String sessionId = "";
		String matmidMemberId = "";
		if (UserData.getInstance().getUserObject() != null)
		{
			if (!TextUtils.isEmpty(UserData.getInstance().getUserObject().getSessionID()))
			{
				sessionId = UserData.getInstance().getUserObject().getSessionID();
			}
			if (!TextUtils.isEmpty(UserData.getInstance().getUserObject().getMatmidMemberID()))
			{
				matmidMemberId = UserData.getInstance().getUserObject().getMatmidMemberID();
			}
		}
		
		Response.Listener<ResponseGetHomeScreenContent> responseListener;
		Response.ErrorListener errorListener;
		
		if (iResponseListener != null)
		{
			responseListener = iResponseListener;
		}
		else
		{
			responseListener = new Response.Listener<ResponseGetHomeScreenContent>()
			{
				@Override
				public void onResponse(final ResponseGetHomeScreenContent iResponse)
				{
					
					if (iResponse != null && iResponse.getContent() != null)
					{
						AppData.getInstance().setHomeItems(iResponse.getContent());
					}
				}
			};
		}
		if (iErrorListener != null)
		{
			errorListener = iErrorListener;
		}
		else
		{
			errorListener = new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(final VolleyError error)
				{
					
					
				}
			};
		}
		
		GetHomeScreenContent(new RequestGetHomeScreenContent(UserData.getInstance().isUserGuest() ?
		                                                     UserData.getInstance().getGuestUserId() :
		                                                     UserData.getInstance().getUserID(), sessionId, matmidMemberId), responseListener, errorListener);
		
	}
	
	
	private void GetHomeScreenContent(final RequestGetHomeScreenContent iRequest, final Response.Listener<ResponseGetHomeScreenContent> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseGetHomeScreenContent> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getGetHomeScreenContentUrl(), iRequest, ResponseGetHomeScreenContent.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void GetGroupPnrsData(final RequestGetGroupPNRsData iRequest, final Response.Listener<ResponseGetGroupPNRs> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseGetGroupPNRs> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getGetGroupPNRsDataUrl(), iRequest, ResponseGetGroupPNRs.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	private void handleMatmidLoginUrlResponse(final ResponseGetSideMenuItems iResponseGetSideMenuItems, final ResponseMatmidLogin iResponseMatmidLogin,
	                                          final Response.Listener<ResponseMatmidLogin> iListener, final Response.ErrorListener iErrorListener)
	{
		AppData.getInstance().setSideMenuData(iResponseGetSideMenuItems);
		final int userId = iResponseMatmidLogin.getContent().getUserID();
		
		
		//Get notification badge
		GetUnreadNotificationsBadge(new RequestGetUnreadNotificationsBadge(userId), new Response.Listener<ResponseGetUnreadNotificationsBadge>()
		{
			@Override
			public void onResponse(final ResponseGetUnreadNotificationsBadge response)
			{
				AppData.getInstance().setUnReadPushes(response);
				
				if (iListener != null)
				{
					iListener.onResponse(iResponseMatmidLogin);
				}
			}
		}, new Response.ErrorListener()
		{
			@Override
			public void onErrorResponse(final VolleyError error)
			{
				if (iErrorListener != null)
				{
					iErrorListener.onErrorResponse(error);
				}
			}
		});
	}
	
	public void SetUserDetails(final RequestSetUserDetails iRequest, final Response.Listener<ResponseSetUserDetails> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseSetUserDetails> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getSetUserDetailsUrl(), iRequest, ResponseSetUserDetails.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	
	public void SetUserMatmidDetails(final RequestSetUserMatmidDetails iRequest, final Response.Listener<ResponseSetUserMatmidDetails> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonMatmidLoginRequest<ResponseSetUserMatmidDetails> request = new JacksonMatmidLoginRequest<>(Request.Method.POST, RequestStringBuilder.getSetUserMatmidDetailsUrl(), iRequest, null, ResponseSetUserMatmidDetails.class, iListener, iErrorListener);
			//			if(TextUtils.isEmpty(RequestStringBuilder.getEnvironmentName())){//Environment = PROD
			//				HashUtilsManager.handleMatmidJsonIfNeed(request.getBody());
			//			}
			request.setSecretHashHeader(eRequestItem.MatmidUpdateMemberData, request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void SetDevice(final RequestSetDevice iRequest, final Response.Listener<ResponseSetDevice> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseSetDevice> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getSetDeviceUrl(), iRequest, ResponseSetDevice.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void ResetPassword(final RequestResetPassword iRequest, final Response.Listener<ResponseResetPassword> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseResetPassword> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getResetPasswordUrl(), iRequest, ResponseResetPassword.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void ChangePassword(final RequestChangePassword iRequest, final Response.Listener<ResponseChangePassword> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseChangePassword> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getChangePasswordUrl(), iRequest, ResponseChangePassword.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void GetRepresentationByIp(final Response.Listener<ResponseGetRepresentationByIp> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			getIpAddress(new Response.Listener<ResponseIpInfo>()
			{
				@Override
				public void onResponse(final ResponseIpInfo response)
				{
					String ipAddress = response != null ? response.getIp() : null;
					
					JacksonRequest<ResponseGetRepresentationByIp> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getGetRepresentationByIpUrl(), new RequestGetRepresentationByIP(ipAddress), ResponseGetRepresentationByIp.class, iListener, iErrorListener);
					request.setSecretHashHeader(request.getBody());
					getRequestQueue().add(request);
				}
			}, iErrorListener);
		}
	}
	
	public void SetRegularUserFamily(final RequestSetRegularUserFamily iRequest, final Response.Listener<ResponseSetRegularUserFamily> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseSetRegularUserFamily> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getSetRegularUserFamilyUrl(), iRequest, ResponseSetRegularUserFamily.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void SetMatmidUserFamily(final RequestSetMatmidUserFamily iRequest, final Response.Listener<ResponseSetMatmidUserFamily> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonMatmidLoginRequest<ResponseSetMatmidUserFamily> request = new JacksonMatmidLoginRequest<>(Request.Method.POST, RequestStringBuilder.getSetMatmidUserFamilyUrl(), iRequest, null, ResponseSetMatmidUserFamily.class, iListener, iErrorListener);
			request.setSecretHashHeader(eRequestItem.MatmidSetFamilyMembers, request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void GetUserFamily(final RequestGetUserFamily iRequest, final Response.Listener<ResponseGetUserFamily> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseGetUserFamily> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getUserFamilyUrl(), iRequest, ResponseGetUserFamily.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	private void getSideMenuItems(final RequestGetSideMenuItems iRequest, final Response.Listener<ResponseGetSideMenuItems> iListener, final Response.ErrorListener errorListener)
	{
		if (!isRequestQueueNull() && iListener != null && errorListener != null)
		{
			JacksonRequest<ResponseGetSideMenuItems> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getGetSideMenuItemsUrl(), iRequest, ResponseGetSideMenuItems.class, iListener, errorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void GetUnreadNotificationsBadge(final RequestGetUnreadNotificationsBadge iRequest, final Response.Listener<ResponseGetUnreadNotificationsBadge> iListener,
	                                        final Response.ErrorListener errorListener)
	{
		if (!isRequestQueueNull() && iListener != null && errorListener != null)
		{
			JacksonRequest<ResponseGetUnreadNotificationsBadge> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getUnreadNotificationsBadge(), iRequest, ResponseGetUnreadNotificationsBadge.class, iListener, errorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void Logout(final RequestLogout iRequest, final Response.Listener<ResponseLogout> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseLogout> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getLogoutUrl(), iRequest, ResponseLogout.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
}
