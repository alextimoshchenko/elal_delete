package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestSearchFlightFamily
{
	@JsonProperty("UserID")
	private int mUserID;
	@JsonProperty("FamilyID")
	private int mFamilyID;
	
	public RequestSearchFlightFamily(int iuserID, int iFamilyID)
	{
		mUserID = iuserID;
		mFamilyID = iFamilyID;
	}
	
	public RequestSearchFlightFamily()
	{
	}
}
