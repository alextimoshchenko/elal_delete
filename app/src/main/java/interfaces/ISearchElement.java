package interfaces;

public interface ISearchElement
{
	String getElementNameEn();
	
	String getElementNameHe();
	
	String getAliasText();
}
