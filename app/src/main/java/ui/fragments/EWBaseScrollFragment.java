package ui.fragments;

import android.view.View;
import android.widget.ScrollView;

import ui.customWidgets.CustomScrollView;

/**
 * Created by Avishay.Peretz on 28/06/2017.
 */

public class EWBaseScrollFragment extends EWBaseFragment
{
	
	public final void focusOnView(final ScrollView iScrollView, final View iView)
	{
		iScrollView.post(new Runnable()
		{
			@Override
			public void run()
			{
				iScrollView.smoothScrollTo(0, iView.getTop());
			}
		});
	}
	
	
	public final void focusOnView(final CustomScrollView iCustomScrollView, final View iView)
	{
		iCustomScrollView.post(new Runnable()
		{
			@Override
			public void run()
			{
				iCustomScrollView.smoothScrollTo(0, iView.getTop());
			}
		});
	}
	
	public final void focusOnViewBottomView(final CustomScrollView iCustomScrollView, final View iView)
	{
		iCustomScrollView.post(new Runnable()
		{
			@Override
			public void run()
			{
				int[] location=new int[2];
				iView.getLocationOnScreen(location);
				iCustomScrollView.smoothScrollTo(0, location[1]);
			}
		});
	}
}
