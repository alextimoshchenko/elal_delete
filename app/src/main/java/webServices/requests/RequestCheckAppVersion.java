package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.GlobalVariables;

/**
 * Created with care by Shahar Ben-Moshe on 13/12/16.
 */

public class RequestCheckAppVersion
{
	@JsonProperty("OSID")
	private int mOSID = GlobalVariables.OS_ANDROID;
	
	public RequestCheckAppVersion(int iOSID)
	{
		mOSID = iOSID;
	}
	
	public RequestCheckAppVersion()
	{
	}
}
