package utils.global.myFlightsObjects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import webServices.global.ePnrFlightType;
import webServices.responses.getMultiplePNRDestinations.GetMultiplePNRDestinations;
import webServices.responses.responseGetUserActivePNRs.PnrFlight;
import webServices.responses.responseGetUserActivePNRs.UserActivePnr;

/**
 * Created with care by Alexey.T on 14/08/2017.
 */
public class MyFlightsObject
{
	private static final String TAG = MyFlightsObject.class.getSimpleName();
	
	private static MyFlightsObject mObject;
	
	//Global Data
	private ArrayList<UserActivePnr> mUserActivePnrList = new ArrayList<>();
	
	//Current date
	private UserActivePnr mCurrentUserActivePnr = new UserActivePnr();
//	private UserActivePnr mCurrentActivePnrData = new UserActivePnr();
	private GetMultiplePNRDestinations mCurrentPnrDestinations = new GetMultiplePNRDestinations();
	
	
	private MyFlightsObject()
	{
	}
	
	public ArrayList<UserActivePnr> getUserActivePnrList()
	{
		return mUserActivePnrList;
	}
	
	public GetMultiplePNRDestinations getCurrentPnrDestinations()
	{
		return mCurrentPnrDestinations;
	}
	
	public void setCurrentPnrDestinations(final GetMultiplePNRDestinations iCurrentPnrDestinations)
	{
		mCurrentPnrDestinations = iCurrentPnrDestinations;
	}
	
	public void setUserActivePnrList(final ArrayList<UserActivePnr> iUserActivePnrList)
	{
		mUserActivePnrList = iUserActivePnrList;
		
		sortByDatePnrList();
		
		if (iUserActivePnrList != null && !iUserActivePnrList.isEmpty())
		{
			mCurrentUserActivePnr = iUserActivePnrList.get(0);
		}
	}
	
	public void sortByDatePnrList()
	{
		Collections.sort(mUserActivePnrList, new Comparator<UserActivePnr>()
		{
			@Override
			public int compare(final UserActivePnr o1, final UserActivePnr o2)
			{
				int result;
				
				Date date1 = o1 != null && o1.getCurrentItemFlightAtPositionOrNull(0, ePnrFlightType.DEPARTURE) != null ?
				             o1.getCurrentItemFlightAtPositionOrNull(0, ePnrFlightType.DEPARTURE).getDepartureDate() :
				             null;
				Date date2 = o2 != null && o2.getCurrentItemFlightAtPositionOrNull(0, ePnrFlightType.DEPARTURE) != null ?
				             o2.getCurrentItemFlightAtPositionOrNull(0, ePnrFlightType.DEPARTURE).getDepartureDate() :
				             null;
				
				if (date1 == null)
				{
					result = 1;
				}
				else if (date2 == null)
				{
					result = -1;
				}
				else
				{
					result = date1.compareTo(date2);
				}
				
				return result;
			}
		});
	}
	
	public UserActivePnr getCurrentUserActivePnr()
	{
		return mCurrentUserActivePnr;
	}
	
	public void setCurrentUserActivePnr(final UserActivePnr iCurrentUserActivePnr)
	{
		mCurrentUserActivePnr = iCurrentUserActivePnr;
	}
	
	public void setCurrentUserActivePnrByIndex(int iIndex)
	{
		if (iIndex >= 0 && !mUserActivePnrList.isEmpty() && mUserActivePnrList.size() >= iIndex)
		{
			if (iIndex == mUserActivePnrList.size())
			{
				iIndex = 0;
			}
			
			setCurrentUserActivePnr(mUserActivePnrList.get(iIndex));
		}
	}
	
	public static MyFlightsObject getInstance()
	{
		if (mObject == null)
		{
			mObject = new MyFlightsObject();
		}
		
		return mObject;
	}
	
	public PnrFlight getCurrentPnrFlight()
	{
		PnrFlight result = new PnrFlight();
		
		ArrayList<PnrFlight> pnrFlightsList = new ArrayList<>();
		pnrFlightsList.addAll(mCurrentUserActivePnr.getDeparturePNRFlights());
		
		if (!pnrFlightsList.isEmpty())
		{
			result = pnrFlightsList.get(pnrFlightsList.size() - 1);
		}
		
		return result;
	}
	
	public void clearObject()
	{
				mObject = null;
	}
	
	public boolean isUserActivePnrExist(final UserActivePnr iUserActivePnr)
	{
		if (mUserActivePnrList!= null)
		{
			int size = mUserActivePnrList.size();
			for (int i = 0 ; i < size ; i++)
			{
				if (mUserActivePnrList.get(i).getPnr().equalsIgnoreCase(iUserActivePnr.getPnr()))
				{
					return true;
				}
			}
		}
		return false;
	}
	
	//	public void setCurrentActivePnrData(UserActivePnr activePnr)
//	{
//		mCurrentActivePnrData = activePnr;
//	}
//
//	public UserActivePnr getCurrentActivePnrData()
//	{
//		return mCurrentActivePnrData;
//	}
}
