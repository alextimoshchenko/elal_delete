package interfaces;

import webServices.responses.getMultiplePNRDestinations.DestinationListItem;
import webServices.responses.responseGetUserActivePNRs.UserActivePnr;

/**
 * Created by erline.katz on 14/08/2017.
 */

public interface IDestinationListener
{
	void onDestinationSelected(final DestinationListItem iDestinationListItem);
	void onScrollToCurrentItem(final int position);
}
