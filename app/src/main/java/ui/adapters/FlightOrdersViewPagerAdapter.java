package ui.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;

import il.co.ewave.elal.R;
import utils.global.AppUtils;
import utils.global.DateTimeUtils;
import webServices.global.ePnrFlightType;
import webServices.responses.responseGetUserActivePNRs.PnrFlight;
import webServices.responses.responseGetUserActivePNRs.UserActivePnr;

/**
 * Created by erline.katz on 04/09/2017.
 */

public class FlightOrdersViewPagerAdapter extends PagerAdapter
{
	private static String TAG = "FlightOrdersViewPagerAdapter";
	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private UserActivePnr mUserActivePnr;
	private ArrayList<PnrFlight> mFlights = new ArrayList<>();
	
	private TextView mTvOrigin;
	private TextView mTvDestination;
	private TextView mOriginName;
	private TextView mTvFlightNumber;
	private TextView mTvDestinationName;
	private TextView mTvFlightDate;
	private TextView mTvDeparture;
	private TextView mTvArrival;
	private TextView mTvStatus;
	private RecyclerView mRvPassengers;
	private TextView mTvOriginTerminal;
	private TextView mTvDestinationTerminal;
	private TextView mTvClass;
	private LinearLayout mLilRoot;
	
	public FlightOrdersViewPagerAdapter(Context context, UserActivePnr iUserActivePnr)
	{
		this.mContext = context;
		if (mContext != null)
		{
			mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}
		getFlightsList(iUserActivePnr);
		this.mUserActivePnr = iUserActivePnr;
	}
	
	private void getFlightsList(UserActivePnr iFlightOrders)
	{
		if (mFlights != null)
		{
			if (!mFlights.isEmpty())
			{
				mFlights.clear();
			}
			mFlights.addAll(iFlightOrders.getDeparturePNRFlights());
			mFlights.addAll(iFlightOrders.getReturnPNRFlights());
		}
	}
	
	@Override
	public int getCount()
	{
		return mFlights.size();
	}
	
	@Override
	public boolean isViewFromObject(View view, Object object)
	{
		return view == ((LinearLayout) object);
	}
	
	@Override
	public Object instantiateItem(ViewGroup container, int position)
	{
		View itemView = mLayoutInflater.inflate(R.layout.flight_order_layout, container, false);
		
		initReference(itemView);
		setData(position);
		flipViewIfRtl();
		setPassengersAdapter(position);
		
		container.addView(itemView);
		return itemView;
	}
	
	private void flipViewIfRtl()
	{
		if (AppUtils.isDefaultLocaleRTL())
		{
			mLilRoot.setRotationY(180);
		}
	}
	
	private void setPassengersAdapter(int position)
	{
		LinearLayoutManager llm = new LinearLayoutManager(mContext);
		llm.setOrientation(LinearLayoutManager.VERTICAL);
		mRvPassengers.setLayoutManager(llm);
		
		mRvPassengers.setAdapter(new FlightOrderPassengersAdapter(mUserActivePnr.getPassengersByFlightNum(mFlights.get(position).getFlightNumber()), mUserActivePnr.getPnr(), mContext));
	}
	
	private void setData(int position)
	{
		PnrFlight currentFlight = mFlights.get(position);
		
		if (currentFlight != null)
		{
			if (mTvOrigin != null)
			{
				mTvOrigin.setText(currentFlight.getOrigination());
			}
			mOriginName.setText(currentFlight.getOriginationCityName());
			mTvOriginTerminal.setText(TextUtils.isEmpty(currentFlight.getDepartTerminal()) ? " " : mContext.getResources().getString(R.string.terminal) + " " + currentFlight.getDepartTerminal());
			
			mTvDestination.setText(currentFlight.getDestination());
			mTvDestinationName.setText(currentFlight.getDestinationCityName());
			mTvDestinationTerminal.setText(TextUtils.isEmpty(currentFlight.getArraivalTerminal()) ? " " : mContext.getResources().getString(R.string.terminal) + " " + currentFlight.getArraivalTerminal());
			
			mTvFlightNumber.setText(currentFlight.getFlightNumber());
			
			mTvFlightDate.setText(DateTimeUtils.convertDateToSlashSeparatedDayMonthYearString(currentFlight.getDepartureDate()));
			mTvDeparture.setText(DateTimeUtils.convertDateToHourMinuteString(currentFlight.getDepartureDate()));
			mTvArrival.setText(DateTimeUtils.convertDateToHourMinuteString(currentFlight.getArrivalDate()));
			
			mTvClass.setText(currentFlight.getClassOfService());
//			mTvStatus.setText(currentFlight.getFlightStatus());
		}
		else
		{
			mTvOrigin.setText("");
			mTvDestination.setText("");
			mOriginName.setText("");
			mTvFlightNumber.setText("");
			mTvDestinationName.setText("");
			mTvFlightDate.setText("");
			mTvDeparture.setText("");
			mTvArrival.setText("");
			mTvClass.setText("");
			mTvOriginTerminal.setText("");
			mTvDestinationTerminal.setText("");
		}
	}
	
	private void initReference(View itemView)
	{
		mLilRoot = (LinearLayout) itemView.findViewById(R.id.lil_flightOrder_Root);
		mTvOrigin = (TextView) itemView.findViewById(R.id.tv_flightDetails_Origin);
		mTvDestination = (TextView) itemView.findViewById(R.id.tv_flightDetails_Destination);
		mOriginName = (TextView) itemView.findViewById(R.id.tv_flightDetails_OriginName);
		mTvFlightNumber = (TextView) itemView.findViewById(R.id.tv_flightDetails_FlightNumber);
		mTvDestinationName = (TextView) itemView.findViewById(R.id.tv_flightDetails_DestinationName);
		mTvFlightDate = (TextView) itemView.findViewById(R.id.tv_flightDetails_FlightDate);
		mTvDeparture = (TextView) itemView.findViewById(R.id.tv_flightDetails_Departure);
		mTvArrival = (TextView) itemView.findViewById(R.id.tv_flightDetails_Arrival);
		mTvStatus = (TextView) itemView.findViewById(R.id.tv_flightDetails_Status);
		mRvPassengers = (RecyclerView) itemView.findViewById(R.id.rv_flightDetails_Passengers);
		mTvOriginTerminal = (TextView) itemView.findViewById(R.id.tv_flightDetails_OriginTerminal);
		mTvDestinationTerminal = (TextView) itemView.findViewById(R.id.tv_flightDetails_DestinationTerminal);
		mTvClass = (TextView) itemView.findViewById(R.id.tv_flightDetails_Class);
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object)
	{
		container.removeView((LinearLayout) object);
	}
	
	@Override
	public int getItemPosition(Object item)
	{
		return POSITION_NONE;
	}
}