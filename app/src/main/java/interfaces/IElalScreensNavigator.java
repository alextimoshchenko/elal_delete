package interfaces;

import android.app.Activity;
import android.os.Bundle;

import ui.fragments.EWBaseFragment;

/**
 * Created with care by Shahar Ben-Moshe on 08/01/17.
 */

public interface IElalScreensNavigator
{
	void goToScreenById(final int iScreenId, final Bundle iBundle);
	
	void goToScreenById(final int iScreenId, final Class iOriginActivity, final Bundle iBundle);
	
	void goToWebFragment(final String iUrl);
	
	void goToFragmentByFragmentClass(Class<? extends EWBaseFragment> iFragmentClass, final Bundle iBundle, final boolean iAddToBackStack);
}
