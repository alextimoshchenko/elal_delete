package webServices.controllers;

import com.android.volley.Request;
import com.android.volley.Response;

import webServices.global.JacksonRequest;
import webServices.global.RequestStringBuilder;
import webServices.requests.RequestClearUserHistory;
import webServices.requests.RequestGetSearchGlobalData;
import webServices.requests.RequestGetSearchHistory;
import webServices.requests.RequestSearchDepartureCities;
import webServices.requests.RequestSearchDestinationCities;
import webServices.requests.RequestSearchFlight;
import webServices.responses.ResponseClearUserHistory;
import webServices.responses.ResponseGetSearchGlobalData;
import webServices.responses.ResponseGetSearchHistory;
import webServices.responses.ResponseSearchFlight;
import webServices.responses.getSearchDepartureCities.ResponseSearchDepartureCities;
import webServices.responses.getSearchDestiationCities.ResponseSearchDestinationCities;

/**
 * Created with care by Alexey.T on 06/07/2017.
 */
public class SearchFlightController extends AController implements MizdamenInterfave
{
	public void GetSearchDepartureCities(final RequestSearchDepartureCities iRequest, final Response.Listener<ResponseSearchDepartureCities> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseSearchDepartureCities> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getSearchDepartureCitiesUrl(), iRequest, ResponseSearchDepartureCities.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void GetSearchDestinationCities(final RequestSearchDestinationCities iRequest, final Response.Listener<ResponseSearchDestinationCities> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseSearchDestinationCities> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getSearchDestinationCitiesUrl(), iRequest, ResponseSearchDestinationCities.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void SearchFlight(final RequestSearchFlight iRequest, final Response.Listener<ResponseSearchFlight> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseSearchFlight> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getSearchFlightUrl(), iRequest, ResponseSearchFlight.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void GetSearchGlobalData(final RequestGetSearchGlobalData iRequest, final Response.Listener<ResponseGetSearchGlobalData> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseGetSearchGlobalData> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getSearchGlobalDataUrl(), iRequest, ResponseGetSearchGlobalData.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void GetSearchHistory(final RequestGetSearchHistory iRequest, final Response.Listener<ResponseGetSearchHistory> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseGetSearchHistory> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getSearchHistoryUrl(), iRequest, ResponseGetSearchHistory.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
	
	public void ClearUserHistory(final int iUserId, final Response.Listener<ResponseClearUserHistory> iListener, final Response.ErrorListener iErrorListener)
	{
		if (!isRequestQueueNull() && iListener != null && iErrorListener != null)
		{
			JacksonRequest<ResponseClearUserHistory> request = new JacksonRequest<>(Request.Method.POST, RequestStringBuilder.getClearUserHistoryUrl(), new RequestClearUserHistory(iUserId), ResponseClearUserHistory.class, iListener, iErrorListener);
			request.setSecretHashHeader(request.getBody());
			getRequestQueue().add(request);
		}
	}
}
