package webServices.push;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Created with care by Shahar Ben-Moshe on 8/2/15.
 */
public class PushObject
{
	public enum eLinkMode
	{
		Error,
		InApp,
		Url,
		Store;
		
		@JsonValue
		private int getOrdinalValue()
		{
			return this.ordinal();
		}
		
		@JsonCreator
		public static eLinkMode getLinkTypeByOrdinalOrErrorDefault(int iLinkModId)
		{
			return iLinkModId >= 0 && iLinkModId < eLinkMode.values().length ? eLinkMode.values()[iLinkModId] : Error;
		}
		
		public boolean isError()
		{
			return this == Error;
		}
	}
	
	@JsonProperty("Message")
	private String mMessageText;
	@JsonProperty("p1")
	private eLinkMode mLinkMode = eLinkMode.InApp;
	@JsonProperty("p2")
	private String mUrl;
	@JsonProperty("p3")
	private String mParam3;
	@JsonProperty("p4")
	private String mParam4;
	@JsonProperty("p5")
	private String mParam5;
	
	public PushObject()
	{
	}
	
	public PushObject(final String iMessageText)
	{
		mMessageText = iMessageText;
	}
	
	public String getMessageText()
	{
		return mMessageText;
	}
	
	eLinkMode getLinkMode()
	{
		return mLinkMode;
	}
	
	public String getUrl()
	{
		return mUrl;
	}
}