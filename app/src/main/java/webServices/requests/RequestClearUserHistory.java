package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestClearUserHistory
{
	@JsonProperty("UserID")
	private int mUserID;
	
	public RequestClearUserHistory(int iUserID)
	{
		this.mUserID = iUserID;
	}
}
