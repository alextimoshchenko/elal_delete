package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.UserData;

/**
 * Created with care by Shahar Ben-Moshe on 20/02/17.
 */

public class RequestGetToSeeLinkByDestination
{
//	@JsonProperty("UserId")
//	private int mUserId;
	
	@JsonProperty("DestinationIATA")
	private String mDestinationIata;
	
	@JsonProperty("LanguageCode")
	private String mLanguageCode = UserData.getInstance().getLanguage().getLanguageCode();
	
	public RequestGetToSeeLinkByDestination(final int iUserId, final String iDestinationIata)
	{
//		mUserId = iUserId;
		mDestinationIata = iDestinationIata;
	}
}
