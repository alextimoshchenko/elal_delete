package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with care by Shahar Ben-Moshe on 21/02/17.
 */

public class RequestDoCheckList
{
	@JsonProperty("UserID")
	private int mUserID;
	@JsonProperty("ItemID")
	private int mItemID;
	@JsonProperty("PNR")
	private String mPnr;
	@JsonProperty("IsDone")
	private boolean mIsDone;
	
	public RequestDoCheckList(final int iUserID, final int iItemID, final String iPnr, final boolean iIsDone)
	{
		mUserID = iUserID;
		mItemID = iItemID;
		mPnr = iPnr;
		mIsDone = iIsDone;
	}
}
