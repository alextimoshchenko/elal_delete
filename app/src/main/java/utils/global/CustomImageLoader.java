package utils.global;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageRequest;

import java.util.HashMap;
import java.util.Map;

import webServices.global.JacksonRequest;

/**
 * Created by erline.katz on 19/09/2017.
 */

public class CustomImageLoader extends ImageLoader
{
//	private Context mContext;
	
	public CustomImageLoader(RequestQueue queue, ImageCache imageCache)
	{
		super(queue, imageCache);
//		mContext = aContext;
	}
	
//	@Override
//	protected Request<Bitmap> makeImageRequest(String requestUrl, int maxWidth, int maxHeight, ImageView.ScaleType scaleType, final String cacheKey)
//	{
////		return super.makeImageRequest(requestUrl, maxWidth, maxHeight, scaleType, cacheKey);
//
//		return new ImageRequest(requestUrl, new Response.Listener<Bitmap>() {
//			@Override
//			public void onResponse(Bitmap response) {
//				onGetImageSuccess(cacheKey, response);
//			}
//		}, maxWidth, maxHeight, scaleType, Bitmap.Config.RGB_565, new Response.ErrorListener() {
//			@Override
//			public void onErrorResponse(VolleyError error) {
//				onGetImageError(cacheKey, error);
//			}
//		}){
//			@Override
//			public Map<String, String> getHeaders() throws AuthFailureError
//			{
//				HashMap<String, String> params = new HashMap<String, String>();
//				params.put(JacksonRequest.USER_AGENT, JacksonRequest.USER_AGENT_CONTENT);
//				return params;
//			}
//		};
//	}
	
	@Override
		protected Request<Bitmap> makeImageRequest(String requestUrl, int maxWidth, int maxHeight, ImageView.ScaleType scaleType, final String cacheKey)
		{
	//		return super.makeImageRequest(requestUrl, maxWidth, maxHeight, scaleType, cacheKey);
			
			return new ImageRequest(requestUrl, new Response.Listener<Bitmap>() {
				@Override
				public void onResponse(Bitmap response) {
					onGetImageSuccess(cacheKey, response);
				}
			}, maxWidth, maxHeight, Bitmap.Config.RGB_565, new Response.ErrorListener()
			{
				@Override
				public void onErrorResponse(VolleyError error) {
					onGetImageError(cacheKey, error);
				}
			}){
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError
				{
					HashMap<String, String> params = new HashMap<String, String>();
					params.put(JacksonRequest.USER_AGENT, JacksonRequest.USER_AGENT_CONTENT);
					return params;
				}
			};
		}
	
}
