package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.UserData;

/**
 * Created with care by Shahar Ben-Moshe on 24/01/17.
 */

public class RequestGetUserDestinations
{
	@JsonProperty("UserID")
	private int mUserID;
	
	@JsonProperty("LanguageCode")
	private String mLanguageCode = UserData.getInstance().getLanguage().getLanguageCode();
	
	public RequestGetUserDestinations(final int iUserID)
	{
		mUserID = iUserID;
	}
}
