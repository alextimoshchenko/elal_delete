package webServices.responses.responseGetUserActivePNRs;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Date;

import utils.global.dbObjects.Document;

/**
 * Created with care by Shahar Ben-Moshe on 12/02/17.
 */

public class Passenger implements Parcelable
{
	@JsonProperty("TicketNumber")
	private String mTicketNumber;
	
	@JsonProperty("Title")
	private String mTitle;
	
	@JsonProperty("FirstName")
	private String mFirstName;
	
	@JsonProperty("LastName")
	private String mLastName;
	
	@JsonProperty("Seat")
	private String mSeat;
	
	@JsonProperty("Meal")
	private String mMeal;
	
	@JsonProperty("isExpanded")
	private boolean isExpanded;
	
	private String mFlightNumber;
	
	public Passenger(final String iTicketNumber, final String iTitle, final String iFirstName, final String iLastName, final String iSeat, final String iMeal, final String iFlightNumber)
	{
		mTicketNumber = iTicketNumber;
		mTitle = iTitle;
		mFirstName = iFirstName;
		mLastName = iLastName;
		mSeat = iSeat;
		mMeal = iMeal;
		mFlightNumber = iFlightNumber;
	}
	
	public static final Creator<Passenger> CREATOR = new Creator<Passenger>()
	{
		@Override
		public Passenger createFromParcel(Parcel in)
		{
			return new Passenger(in);
		}
		
		@Override
		public Passenger[] newArray(int size)
		{
			return new Passenger[size];
		}
	};
	
	public String getTicketNumber()
	{
		return mTicketNumber;
	}
	
	public String getTitle()
	{
		return mTitle;
	}
	
	public String getFirstName()
	{
		return mFirstName;
	}
	
	public String getLastName()
	{
		return mLastName;
	}
	
	public String getSeat()
	{
		if (mSeat != null && !TextUtils.isEmpty(mSeat))
		{
			return mSeat;
		}
		else
		{
			return "-";
		}
	}
	
	public String getMeal()
	{
		if (mMeal != null && !TextUtils.isEmpty(mMeal))
		{
			return mMeal;
		}
		else
		{
			return " ";
		}
	}
	
	public boolean isExpanded()
	{
		return isExpanded;
	}
	
	public void reverseExpand()
	{
		isExpanded = !isExpanded;
	}
	
	public String getmFlightNumber()
	{
		return mFlightNumber;
	}
	
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeString(this.mTicketNumber);
		dest.writeString(this.mTitle);
		dest.writeString(this.mFirstName);
		dest.writeString(this.mLastName);
		dest.writeString(this.mSeat);
		dest.writeString(this.mMeal);
		dest.writeString(this.mFlightNumber);
		dest.writeByte(this.isExpanded ? (byte) 1 : (byte) 0);
	}
	
	protected Passenger(Parcel in)
	{
		this.mTicketNumber = in.readString();
		this.mTitle = in.readString();
		this.mFirstName = in.readString();
		this.mLastName = in.readString();
		this.mSeat = in.readString();
		this.mMeal = in.readString();
		this.mFlightNumber = in.readString();
		this.isExpanded = in.readByte() != 0;
	}
}
