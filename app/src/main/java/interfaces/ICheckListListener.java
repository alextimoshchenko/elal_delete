package interfaces;

import webServices.responses.UserCheckList.CheckListItem;

/**
 * Created with care by Shahar Ben-Moshe on 21/02/17.
 */

public interface ICheckListListener
{
	void onCheckChanged(final CheckListItem iCheckListItem);
	
	void onLink(CheckListItem iCheckListItem);
	
	void onEdit(CheckListItem iCheckListItem, int iParentAdapterPosition, int iChildAdapterPosition);
	
	void onDelete(final CheckListItem iCheckListItem, final int iParentPosition, final int iChildPosition);
	
	void onOptionClicked();
}
