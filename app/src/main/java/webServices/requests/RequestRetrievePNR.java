package webServices.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

import global.UserData;

/**
 * Created by Aviv.Kabeli on 15/12/2016.
 */

public class RequestRetrievePNR
{
	@JsonProperty("PNRNum")
	private String mPNRNum;
	@JsonProperty("LastName")
	private String mLastName;
	@JsonProperty("UDID")
	private String mUDID = UserData.getInstance().getUdid();
	
	public RequestRetrievePNR(String iPNRNum, String iLastName)
	{
		this.mPNRNum = iPNRNum;
		this.mLastName = iLastName;
	}
	
	public RequestRetrievePNR()
	{
	}
}
