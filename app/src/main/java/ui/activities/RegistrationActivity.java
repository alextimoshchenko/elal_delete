package ui.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import global.UserData;
import global.eLanguage;
import il.co.ewave.elal.R;
import ui.fragments.EWBaseFragment;
import ui.fragments.RegistrationMainTabsFragment;

public class RegistrationActivity extends EWBaseActivity
{
	private LinearLayout mLlToolbar;
	private FrameLayout mFlContainer;
	
	@Override
	protected void initScreen()
	{
		setContentView(R.layout.activity_registration_layout);
		
		initReference();
		initToolbar();
		loadRegistrationFragment();
	}
	
	private void initReference()
	{
		mLlToolbar = (LinearLayout) findViewById(R.id.ll_registration_Toolbar);
		mFlContainer = (FrameLayout) findViewById(R.id.fl_registration_Container);
	}
	
	private void initToolbar()
	{
		if (mLlToolbar != null)
		{
			mLlToolbar.findViewById(R.id.img_actionBar_drawer).setVisibility(View.GONE);
			((ImageView) mLlToolbar.findViewById(R.id.img_actionBar_logo)).setImageResource(UserData.getInstance().getLanguage() == eLanguage.Hebrew ? R.mipmap.elal_logo_he : R.mipmap.elal_logo_en);
		}
	}
	
	private void loadRegistrationFragment()
	{
		getFragmentSwapper().swapToFragment(RegistrationMainTabsFragment.class, null, mFlContainer.getId(), false, false);
	}
	
	public void loadFragmentToContainer(Class<? extends EWBaseFragment> iFragmentClass, final Bundle iBundle)
	{
		getFragmentSwapper().swapToFragment(iFragmentClass, iBundle, mFlContainer.getId(), true, true);
	}
	
	@Override
	protected void onServiceConnected()
	{
	}
}