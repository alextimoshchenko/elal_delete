package utils.global.enums;

/**
 * Created with care by Alexey.T on 10/07/2017.
 * <p>
 * TODO: Add a class header comment!
 */
public enum eIntention
{
	INCREASE,
	DECREASE;
	
	public boolean isIncrease()
	{
		return this == INCREASE;
	}
	
	public boolean isDecrease()
	{
		return this == DECREASE;
	}
}
