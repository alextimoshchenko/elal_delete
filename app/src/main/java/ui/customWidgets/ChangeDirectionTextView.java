package ui.customWidgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;

import utils.global.AppUtils;

/**
 * Created by Avishay.Peretz on 24/07/2017.
 */

public class ChangeDirectionTextView extends android.support.v7.widget.AppCompatTextView
{
	
	public ChangeDirectionTextView(final Context context)
	{
		super(context);
	}
	
	public ChangeDirectionTextView(final Context context, final AttributeSet attrs)
	{
		super(context, attrs);
	}
	
	public ChangeDirectionTextView(final Context context, final AttributeSet attrs, final int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
	}
	
	
	@SuppressLint("ObsoleteSdkInt")
	@Override
	protected void onFinishInflate()
	{
		super.onFinishInflate();
		
		boolean isRtl = !isInEditMode() && AppUtils.isDefaultLocaleRTL();
		
		this.setGravity(isRtl ? Gravity.RIGHT : Gravity.LEFT);
	}
	
}
